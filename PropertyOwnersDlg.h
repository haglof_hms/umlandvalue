#pragma once
#include "Resource.h"

// CPropertyOwnersDlg dialog

class CPropertyOwnersDlg : public CDialog
{
	DECLARE_DYNAMIC(CPropertyOwnersDlg)

	BOOL m_bInitialized;
	// Setup language filename; 051214 p�d
	CString m_sLangFN;

	CButton m_wndOKBtn;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLblObjName;
	CMyExtStatic m_wndLblPropName;
	CMyExtStatic m_wndLblPropNum;

	CString m_sObjectName;
	CString m_sPropName;
	CString m_sPropNum;
	int m_nPropId;
	int m_nObjId;

	//#5010 PH 20160613
	int currentOwnerId;
	CButton m_wndBtnAddOwner;
	CButton m_wndBtnRemoveOwner;

	//Lagt till knapp f�r fastighetsgenv�g 20111014 J� Feature #2452
	CXTButton m_wndBtnPropShortCut;

	CMyReportCtrl m_wndReport;

	CUMLandValueDB *m_pDB;
	//CTransaction_elv_properties m_recProperty;
	vecTransactionContacts m_vecContacts;
	vecTransactionPropOwners m_vecPropertyOwners;	// .. to get info on is contact; 090302 p�d
	CTransaction_prop_owners m_recPropertyOwners;

	CImageList m_ilIcons;
	void getPropertyOwners();

	void populateData();

	void setupReport(void);
public:
	CPropertyOwnersDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPropertyOwnersDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG8 };

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

	void setObjectAndPropertyInfo(LPCTSTR obj_name,CTransaction_elv_properties &rec)
	{
		m_sObjectName = obj_name;
		m_sPropName = rec.getPropName();
		m_sPropNum = rec.getPropNumber();
		m_nPropId = rec.getPropID_pk();
	}

	void setObjectAndPropertyInfo(LPCTSTR obj_name,LPCTSTR prop_name,LPCTSTR prop_number,int prop_id)
	{
		m_sObjectName = obj_name;
		m_sPropName = prop_name;
		m_sPropNum = prop_number;
		m_nPropId = prop_id;
	}

	void setObjectId(int id) { m_nObjId=id; }

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
 	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_MSG
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnPropshortcut();
	afx_msg void OnBnClickedAddOwnerButton();
	afx_msg void OnBnClickedRemoveOwnerButton();
};
