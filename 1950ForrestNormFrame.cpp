
#include "StdAfx.h"
#include <mapi.h>
#include "IMapi.h"
#include "libxl.h"


#include "1950ForrestNormFrame.h"
#include "MDI1950ForrestNormFormView.h"
#include "LoggMessageDlg.h"
#include "SelectContractorDlg.h"
#include "SearchPropertiesDlg.h"
#include "AddKrPerM3Dlg.h"
#include "ChangePropStatusDlg.h"
#include "HandleSetupFilesDlg.h"
#include "ReplaceTemplateDlg.h"
#include "CheckBoxMessageDlg.h"
#include "PropertyStatusFormView.h"
#include "XBrowseForFolder.h"
#include "P30TemplateFormView.h"
#include "HigherCostsFormView.h"
#include "DocumentTmplFormView.h"
#include "ResLangFileReader.h"
#include "AddOtherCostsDlg.h"
#include "HtmlMsgDlg.h"
#include "ProgressDlg2.h"
#include "HandleLogBookDlg.h"
#include "ObjectSelListFormView.h"
#include "ImportExcelDlg.h"
#include "PropertyOwnersDlg.h"

#include "SpeciesFormView.h"
#include "Resource.h"

using namespace libxl;

static int nShowObjectStatusFlag = 0;	//new #3385	anv�nds f�r att s�tta checkbox framf�r valt visningsalternativ, default visas endast p�g�ende


////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

BEGIN_MESSAGE_MAP(CACBox, CComboBox)
	//{{AFX_MSG_MAP(CMyExtCBox)
	ON_WM_DESTROY()
	ON_CONTROL_REFLECT_EX(CBN_SELCHANGE, OnCBoxChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CACBox::CACBox()
{
	m_fnt1 = new CFont();
}

void CACBox::OnDestroy()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
  
	CComboBox::OnDestroy();
}

void CACBox::setLanguageFN(LPCTSTR lng_fn)
{
	m_sLangFN = lng_fn;
}

void CACBox::setSTDReportsInCBox(LPCTSTR shell_data_file,LPCTSTR add_to,int index)
{
	CString sStr;
	if (getSTDReports(shell_data_file,add_to,index,m_vecReports))
	{
		ResetContent();
		for (UINT i = 0;i < m_vecReports.size();i++)
		{
			sStr = m_vecReports[i].getCaption();
			// Check if there's a Caption for this report
			// item. If not try to read the String in language-
			// file, for this Suite/Module; 070410 p�d
			if (sStr.IsEmpty())
			{
				sStr = getLangStr(m_vecReports[i].getStrID());
			}
			AddString(sStr);

		}	// for (UINT i = 0;i < m_vecReports.size();i++)
	}	// if (getSTDReports(m_vecReports))
}

BOOL CACBox::OnCBoxChange()
{
	CMDI1950ForrestNormFrame *pForm = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pForm != NULL)
	{
		pForm->SendMessage(MSG_IN_SUITE,GetCurSel());
	}
	return FALSE;
}

void CACBox::SetLblFont(int size,int weight,LPCTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

CString CACBox::getLangStr(int id)
{
	CString sStr;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sStr = xml->str(id);
		}
		delete xml;
	}
	return sStr;
}

//////////////////////////////////////////////////////////////////////////
// CMyPropGridItemBool derived from CXTPPropertyGridItemBool
class CMyPropGridItemBool_1 : public CXTPPropertyGridItemBool
{
public:
	CMyPropGridItemBool_1(const CString true_text,
											const CString false_text,
											const CString& cap,
											BOOL bValue) : CXTPPropertyGridItemBool(cap,bValue)
	{
		SetTrueFalseText((true_text),(false_text));
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
//
IMPLEMENT_XTP_CONTROL( CMyControlPopup, CXTPControlPopup)

CMyControlPopup::CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
}

CMyControlPopup::~CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
}

BOOL CMyControlPopup::OnSetPopup(BOOL bPopup)
{
	CMenu menu;
	HMENU sub_menu;
	short nIdx = -1;
	// Make sure there's any menuitems to add; 080425 p�d
	if (bPopup && vecMenuItems.size() > 0)
	{
		menu.CreatePopupMenu();
		for (UINT i = 0;i < vecMenuItems.size();i++)
		{
			_menu_items item = vecMenuItems[i];
			// create menu items
			if (item.m_nID > 0)		
			{
				menu.AppendMenu(MF_STRING, item.m_nID, item.m_sText);

				if(item.m_nID == ID_TOOLS_EXPORT_PROPERTIES || 
					item.m_nID== ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ERBJ ||
					item.m_nID== ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_VP ||
					item.m_nID== ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ELLEVIO ||
					item.m_nID== ID_TOOLS_EXPORT_PROPERTIES_XML ||
					item.m_nID== ID_TOOLS_EXPORT_PROPERTIES_JSON)
				{
					menu.EnableMenuItem(item.m_nID,MF_BYPOSITION | MF_ENABLED);
				}
				else
				{

					if (!isEnabled(item.m_sText,&nIdx))
					{
						menu.EnableMenuItem(item.m_nID,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
					}
				}
			}
			else if (item.m_nID == 0)		
			{
				menu.AppendMenu(MF_SEPARATOR, -1, _T(""));
			}
			else if (item.m_nID < 0 && vecSubMenuItems.size() > 0 && item.m_nSubMenuID > 0) // Setup a Sub-menu
			{
				if (vecSubMenuItems.size() > 0)
				{
					sub_menu = CreatePopupMenu();
					for (UINT i1 = 0;i1 < vecSubMenuItems.size();i1++)
					{
						_menu_items sub_item = vecSubMenuItems[i1];
						if (sub_item.m_nSubMenuID == item.m_nSubMenuID)
						{
							// create sub menu item(s)
							AppendMenu(sub_menu,MF_STRING, sub_item.m_nID, sub_item.m_sText);
						}
						else if (sub_item.m_nID == 0)
						{
							AppendMenu(sub_menu,MF_SEPARATOR, -1, _T(""));
						}
					}
					menu.AppendMenu(MF_POPUP, (UINT)sub_menu, item.m_sText);					
					if (isEnabled(item.m_sText,&nIdx))
						menu.EnableMenuItem(nIdx,MF_BYPOSITION | MF_ENABLED);
					else
						menu.EnableMenuItem(nIdx,MF_BYPOSITION | MF_DISABLED | MF_GRAYED);
				}
			}	// for (UINT i = 0;i < vecSubMenuItems.size();i++)
		}	//for (UINT i = 0;i < vecMenuItems.size();i++)

		//new #3385 enable/disable submenus, finns s�kert b�ttre s�tt
		for (UINT i = 0;i < m_vecMenuItemsEnabled.size();i++)
		{
			if(m_vecMenuItemsEnabled[i].m_bEnabled)
			{
				setSubMenuEnabled(m_vecMenuItemsEnabled[i].m_nItemIndex,&menu);
				setSubMenuChecked(m_vecMenuItemsEnabled[i].m_nItemIndex,&menu);
			}
		}
					
		SetCommandBar(&menu);
		menu.DestroyMenu();
	}
	return CXTPControlPopup::OnSetPopup(bPopup);
}

BOOL CMyControlPopup::isEnabled(LPCTSTR item_text,short *idx)
{
	CString sTmp(item_text);
	*idx = -1;
	if (m_vecMenuItemsEnabled.size() == 0) return FALSE;

	for (UINT i = 0;i < m_vecMenuItemsEnabled.size();i++)
	{
		if (sTmp.Compare(m_vecMenuItemsEnabled[i].m_sItemName) == 0)
		{
			*idx = m_vecMenuItemsEnabled[i].m_nItemIndex;
			return m_vecMenuItemsEnabled[i].m_bEnabled;
		}
	}
	return FALSE;
}

//#3385
void CMyControlPopup::setSubMenuEnabled(int idx, CMenu *menu)
{	
	CMenu* hSub = NULL;
	
	hSub = menu->GetSubMenu(idx);
	if(hSub)
	{
		for(UINT k=0; k<hSub->GetMenuItemCount(); k++)
		{
			for(UINT i=0; i<m_vecSubMenuItemsEnabled.size(); i++)
			{
				if(m_vecSubMenuItemsEnabled[i].m_nId == idx)
				{
					if(m_vecSubMenuItemsEnabled[i].m_nItemIndex == hSub->GetMenuItemID(k))
					{
						if(m_vecSubMenuItemsEnabled[i].m_bEnabled == false)
							hSub->EnableMenuItem(k,MF_BYPOSITION | MF_DISABLED | MF_GRAYED);
						else
							hSub->EnableMenuItem(k,MF_BYPOSITION | MF_ENABLED);
					}
				}
			}
		}
	}
}

//#3385
//s�tt checkbox framf�r aktuellt visningsalternativ
void CMyControlPopup::setSubMenuChecked(int idx, CMenu *menu)
{	
	CMenu* hSub = NULL;
	
	if(idx == 16)	//objekt status, kanske dumt att ha h�rdkodat index h�r?
	{
		hSub = menu->GetSubMenu(idx);
		if(hSub)
		{
			for(UINT k=0; k<hSub->GetMenuItemCount(); k++)
			{
				if(hSub->GetMenuItemID(k) == ID_TOOLS_SHOW_OBJ_ONGOING)	//visa p�g�ende
				{
					if(nShowObjectStatusFlag == 0)
						hSub->CheckMenuItem(k,MF_CHECKED|MF_BYPOSITION);
					else
						hSub->CheckMenuItem(k,MF_UNCHECKED|MF_BYPOSITION);
				}
				else if(hSub->GetMenuItemID(k) == ID_TOOLS_SHOW_OBJ_FINISHED)	//visa slutf�rda
				{
					if(nShowObjectStatusFlag == 1)
						hSub->CheckMenuItem(k,MF_CHECKED|MF_BYPOSITION);
					else
						hSub->CheckMenuItem(k,MF_UNCHECKED|MF_BYPOSITION);
				}
				else if(hSub->GetMenuItemID(k) == ID_TOOLS_SHOW_OBJ_ALL)	//visa alla
				{
					if(nShowObjectStatusFlag == 2)
						hSub->CheckMenuItem(k,MF_CHECKED|MF_BYPOSITION);
					else
						hSub->CheckMenuItem(k,MF_UNCHECKED|MF_BYPOSITION);
				}
			}
		}
	}
}

void CMyControlPopup::addMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setSubMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecSubMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setExcludedItems(vecMenuItemsEnabled &vec)
{
	if (vec.size() > 0)
	{
		m_vecMenuItemsEnabled.clear();
		m_vecMenuItemsEnabled = vec;
	}	// if (vec.size() > 0)
}

//new #3385
void CMyControlPopup::setExcludedSubMenuItems(vecSubMenuItemsEnabled &vec)
{
	if (vec.size() > 0)
	{
		m_vecSubMenuItemsEnabled.clear();
		m_vecSubMenuItemsEnabled = vec;
	}	// if (vec.size() > 0)
}

////////////////////////////////////////////////////////////////////////////////////////////////

class CCustomItemChilds::CCustomItemChildsPad : public CXTPPropertyGridItemDouble
{
public:
	CCustomItemChildsPad(CString strCaption, double *bind_to) 
		: CXTPPropertyGridItemDouble(strCaption,0,_T("%.1f"),bind_to),m_fValue(*bind_to)
	{
	}
	virtual void OnValueChanged(CString strValue)
	{
		SetValue(strValue);

		CCustomItemChilds* pParent = ((CCustomItemChilds*)m_pParent);
		m_fValue = GetDouble();
		pParent->OnValueChanged(pParent->ValueToString(pParent->m_fBindTo1,pParent->m_fBindTo2));
	}
	double &m_fValue;
};


CCustomItemChilds::CCustomItemChilds(CString strCaption,CString childCap1,CString childCap2,double *bind_to1,double *bind_to2 )
	: CXTPPropertyGridItem(strCaption),	m_fBindTo1(*bind_to1),m_fBindTo2(*bind_to2)

{
	m_sChildCap1 = childCap1;
	m_sChildCap2 = childCap2;
	m_nFlags = 0;
	m_sFormat = _T("%.1f/%.1f");
}

void CCustomItemChilds::OnAddChildItem()
{
	m_itemWidth1 = (CCustomItemChildsPad*)AddChildItem(new CCustomItemChildsPad((m_sChildCap1), &m_fBindTo1));
	m_itemWidth2 = (CCustomItemChildsPad*)AddChildItem(new CCustomItemChildsPad((m_sChildCap2), &m_fBindTo2));

	UpdateChilds();
}

void CCustomItemChilds::UpdateChilds()
{
	m_itemWidth1->SetDouble(m_fBindTo1);
	m_itemWidth2->SetDouble(m_fBindTo2);
 }

void CCustomItemChilds::setValues(double *bind_to1,double *bind_to2)
	
{
	m_fBindTo1 = *bind_to1;
	m_fBindTo2 = *bind_to2;
	SetValue(m_strValue);
	CXTPPropertyGridItem::OnValueChanged(ValueToString(m_fBindTo1,m_fBindTo2));
}

void CCustomItemChilds::SetValue(CString strValue)
{
	CXTPPropertyGridItem::SetValue(strValue);
	UpdateChilds();
}

void CCustomItemChilds::SetReadOnly(BOOL bReadOnly /* = TRUE */)
{
	CXTPPropertyGridItem::SetReadOnly(bReadOnly);
	m_itemWidth1->SetReadOnly(bReadOnly);
	m_itemWidth2->SetReadOnly(bReadOnly);
}

CString CCustomItemChilds::ValueToString(double val1,double val2)
{
	CString str;
	str.Format(m_sFormat, val1,val2);
	return str;
}


//////////////////////////////////////////////////////////////////////////
// CMyPropGridItemBool derived from CXTPPropertyGridItemBool
class CMyPropGridItemBool : public CXTPPropertyGridItemBool
{
public:
};
//////////////////////////////////////////////////////////////////////////
// CMyPropGridItemText derived from CXTPPropertyGridItemBool
class CMyPropGridItem : public CXTPPropertyGridItem
{
public:
};

////////////////////////////////////////////////////////////////////////////////////////////////

class CCustomItemChilds_2::CMyPropGridItemBool : public CXTPPropertyGridItemBool
{
public:
	CMyPropGridItemBool(CString strCaption, BOOL *bind_to) 
		: CXTPPropertyGridItemBool(strCaption,*bind_to),m_bValue(*bind_to)
	{
	}
	virtual void OnValueChanged(CString strValue)
	{
		SetValue(strValue);

		CCustomItemChilds_2* pParent = ((CCustomItemChilds_2*)m_pParent);
		m_bValue = GetBool();
		pParent->OnValueChanged(pParent->ValueToString(pParent->m_bBindTo1,
																									 pParent->m_bBindTo2,
																									 pParent->m_bBindTo3,
																									 pParent->m_bBindTo4,
																									 pParent->m_sBindTo5));
	}
	BOOL &m_bValue;
};


class CCustomItemChilds_2::CMyPropGridItem : public CXTPPropertyGridItem
{
public:
	CMyPropGridItem(CString strCaption, CString* bind_to) 
		: CXTPPropertyGridItem(strCaption,(*bind_to),bind_to),m_sValue(*bind_to)
	{
	}
	virtual void OnValueChanged(CString strValue)
	{
		SetValue(strValue);

		CCustomItemChilds_2* pParent = ((CCustomItemChilds_2*)m_pParent);
		m_sValue = GetValue();
		pParent->OnValueChanged(pParent->ValueToString(pParent->m_bBindTo1,
																									 pParent->m_bBindTo2,
																									 pParent->m_bBindTo3,
																									 pParent->m_bBindTo4,
																									 pParent->m_sBindTo5));
	}
	CString m_sValue;
};


CCustomItemChilds_2::CCustomItemChilds_2(CString strCaption,
																				 CString childCap1,
																				 CString childCap2,
																				 CString childCap3,
																				 CString childCap4,
																				 CString childCap5,
																				 BOOL *bind_to1,
																				 BOOL *bind_to2,
																				 BOOL *bind_to3,
																				 BOOL *bind_to4,
																				 CString *bind_to5,
																				 CString bool_yes,
																				 CString bool_no)
	: CXTPPropertyGridItem(strCaption),	m_bBindTo1(*bind_to1),
																			m_bBindTo2(*bind_to2),
																			m_bBindTo3(*bind_to3),
																			m_bBindTo4(*bind_to4),
																			m_sBindTo5(*bind_to5)

{
	m_sChildCap1 = childCap1;
	m_sChildCap2 = childCap2;
	m_sChildCap3 = childCap3;
	m_sChildCap4 = childCap4;
	m_sChildCap5 = childCap5;
	m_nFlags = 0;
	m_sFormat = _T("%s;%s;%s;%s;%s");
	m_sBoolYes = bool_yes;
	m_sBoolNo = bool_no;

}

void CCustomItemChilds_2::OnAddChildItem()
{
	m_item1 = (CMyPropGridItemBool*)AddChildItem(new CMyPropGridItemBool((m_sChildCap1), &m_bBindTo1));
	m_item1->SetTrueFalseText((m_sBoolYes),(m_sBoolNo));
	m_item2 = (CMyPropGridItemBool*)AddChildItem(new CMyPropGridItemBool((m_sChildCap2), &m_bBindTo2));
	m_item2->SetTrueFalseText((m_sBoolYes),(m_sBoolNo));
	m_item3 = (CMyPropGridItemBool*)AddChildItem(new CMyPropGridItemBool((m_sChildCap3), &m_bBindTo3));
	m_item3->SetTrueFalseText((m_sBoolYes),(m_sBoolNo));
	m_item4 = (CMyPropGridItemBool*)AddChildItem(new CMyPropGridItemBool((m_sChildCap4), &m_bBindTo4));
	m_item4->SetTrueFalseText((m_sBoolYes),(m_sBoolNo));
	m_item5 = (CMyPropGridItem*)AddChildItem(new CMyPropGridItem((m_sChildCap5), &m_sBindTo5));
	m_item5->SetMask(_T("T00"),_T("T__"));

	UpdateChilds();
}

void CCustomItemChilds_2::UpdateChilds()
{
	m_item1->SetBool(m_bBindTo1);
	m_item2->SetBool(m_bBindTo2);
	m_item3->SetBool(m_bBindTo3);
	m_item4->SetBool(m_bBindTo4);
	m_item5->SetValue(m_sBindTo5);
}

void CCustomItemChilds_2::setValues(BOOL *bind_to1,BOOL *bind_to2,BOOL *bind_to3,BOOL *bind_to4,CString *bind_to5)
	
{
	m_bBindTo1 = *bind_to1;
	m_bBindTo2 = *bind_to2;
	m_bBindTo3 = *bind_to3;
	m_bBindTo4 = *bind_to4;
	m_sBindTo5 = *bind_to5;
	SetValue(m_strValue);
	CXTPPropertyGridItem::OnValueChanged(ValueToString(m_bBindTo1,m_bBindTo2,m_bBindTo3,m_bBindTo4,m_sBindTo5));
}

void CCustomItemChilds_2::SetValue(CString strValue)
{
	CXTPPropertyGridItem::SetValue(strValue);
	UpdateChilds();
}

CString CCustomItemChilds_2::ValueToString(BOOL val1,BOOL val2,BOOL val3,BOOL val4,CString val5)
{
	CString str;
	str.Format(m_sFormat, (val1 ? (m_sBoolYes.Left(1)):(m_sBoolNo.Left(1))),
												(val2 ? (m_sBoolYes.Left(1)):(m_sBoolNo.Left(1))),
												(val3 ? (m_sBoolYes.Left(1)):(m_sBoolNo.Left(1))),
												(val4 ? (m_sBoolYes.Left(1)):(m_sBoolNo.Left(1))),
												val5);
	return str;
}

//////////////////////////////////////////////////////////////////////////
// CInplaceButton

BEGIN_MESSAGE_MAP(CInplaceButton, CXTButton)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
END_MESSAGE_MAP()

void CInplaceButton::OnClicked()
{
	m_pItem->OnValueChanged(m_pItem->GetValue());
}

//////////////////////////////////////////////////////////////////////////
// CCustomItemButton

CCustomItemButton::CCustomItemButton(CString strCaption,CWnd *parent)
	: CXTPPropertyGridItem(strCaption)
{
	m_wndButton.m_pItem = this;
	m_nFlags = 0;
	m_pParent = parent;

	m_wndButton.SetBitmap(CSize(16,11),IDB_BITMAP2);
	m_wndButton.SetXButtonStyle( BS_XT_WINXP_COMPAT );

}

void CCustomItemButton::OnValueChanged(CString strValue)
{

	// Direct to CMDI1950ForrestNormFrame class; 080205 p�d
	CMDI1950ForrestNormFrame *pView = dynamic_cast<CMDI1950ForrestNormFrame *>(m_pParent);
	if (pView != NULL)
	{
		pView->changeInventoryDirectory();
	}

	CXTPPropertyGridItem::OnValueChanged(strValue);
}

void CCustomItemButton::CreateButton()
{
	if (IsVisible())
	{	
		CRect rc;
		rc = GetValueRect();
		rc.left = rc.right - 22; //rc.Height();

		if (!m_wndButton.m_hWnd)
		{
			m_wndButton.Create(_T(""), WS_CHILD|BS_NOTIFY|WS_VISIBLE, rc, (CWnd*)m_pGrid, 100);
//			m_wndButton.SetTheme(new CXTButtonThemeOfficeXP(TRUE));
		}
		
		m_wndButton.MoveWindow(rc);
		m_wndButton.Invalidate(FALSE);
	}
	else
	{
		m_wndButton.DestroyWindow();
	}
}

void CCustomItemButton::SetVisible(BOOL bVisible)
{
	CXTPPropertyGridItem::SetVisible(bVisible);
	CreateButton();
}

BOOL CCustomItemButton::OnDrawItemValue(CDC& /*dc*/, CRect /*rcValue*/)
{
	CreateButton();
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc
/*
IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
	// TODO: add one-time construction code here

}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands
*/

/////////////////////////////////////////////////////////////////////////////
// CMDI1950ForrestNormFrame

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
/*
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
*/
};

IMPLEMENT_DYNCREATE(CMDI1950ForrestNormFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDI1950ForrestNormFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDI1950ForrestNormFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)

	// HMS-76 20220128 J� L�gg till knapp f�r export menyer	
	//ON_COMMAND(ID_BUTTON_EXPORT, OnExportPropData)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_EXPORT, OnUpdateExportTBtn)

	ON_COMMAND(ID_TBTN_CONTRACTOR, OnContractorTBtn)
	// Changed to Import INV/HXL files
	ON_COMMAND(ID_TBTN_ADD_PROPERTY, OnCreateFormINVDataOnObjectAndProperties)

	ON_COMMAND(ID_TBTN_CALCULATE, OnMenuCalculateTBtn)
	ON_COMMAND(ID_TBTN_UPDATE, OnUpdatePropertiesTBtn)
	ON_COMMAND(ID_TBBTN_HANDLE_DOCS, OnHandleDocumentTBtn)
	ON_COMMAND(ID_TBTN_PRINTOUT, OnPrintOutTBtn)
	ON_COMMAND(ID_TBTN_GIS, OnGotoGIS)

	ON_UPDATE_COMMAND_UI(ID_TBTN_GIS, OnUpdateGotoGIS)
	ON_UPDATE_COMMAND_UI(ID_TBTN_CONTRACTOR, OnUpdateContractorTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD_PROPERTY, OnUpdateImportPropertiesTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_CALCULATE, OnUpdateCalculateTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_UPDATE, OnUpdateUpdateTBtn)
	//ON_UPDATE_COMMAND_UI(ID_TBBTN_REMOVE_PROPERTY, OnUpdateRemoveTBtn)

	ON_UPDATE_COMMAND_UI(ID_TBBTN_TOOLS, OnUpdateToolTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_HANDLE_DOCS, OnUpdateExtDocTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_PRINTOUT, OnUpdatePrintOutTBtn)

	// "Fastigheter"
	ON_COMMAND(ID_TOOLS_DO_PROP_GROUPINGS, OnGroupPropertiesOnLandOwners)
	ON_COMMAND(ID_TOOLS_DO_PROP_GROUPINGS2, OnGroupPropertiesOnPropertyNumbers)
	ON_COMMAND(ID_TOOLS_DEL_PROP_GROUPING, OnDelPropertyGroupings)
	ON_COMMAND(ID_TOOLS_ADD_PROPERTY, OnImportPropertiesTBtn)
	ON_COMMAND(ID_TOOLS_ADD_PROPERTY_FROM_FILE, OnImportPropertiesFromFileTBtn)
	ON_COMMAND(ID_TOOLS_REMOVE_PROPERTY, OnRemovePropertyTBtn)
	ON_COMMAND(ID_TOOLS_REMOVE_PROPERTY2, OnRemovePropertyTBtn2)
	ON_COMMAND(ID_TOOLS_EXPORT_PROPERTIES, OnExportPropertiesTBtn)
	ON_COMMAND(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ERBJ, OnExportPropertiesExcelrapportErsErbjTBtn)
	ON_COMMAND(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_VP, OnExportPropertiesExcelrapportVpTBtn)
	ON_COMMAND(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ELLEVIO, ExportPropertiesExcelrapportEllevio2025_01_22)


	ON_COMMAND(ID_TOOLS_EXPORT_PROPERTIES_XML, OnExportPropertiesXmlTBtn)
   ON_COMMAND(ID_TOOLS_EXPORT_PROPERTIES_JSON, OnExportPropertiesJsonTBtn)
	
	ON_COMMAND(ID_TOOLS_LOG_BOOK_SETTINGS, OnLogBookSettingsTBtn)

	// "Mark�gare
	ON_COMMAND(ID_TOOLS_PROPERTY_OWNER_LIST,OnListPropertyOwners)

	// "Setup-filer"
	ON_COMMAND(ID_TOOLS_CALIPER_SETUP_FILE, OnCreateSetupFileForCaliper)
	ON_COMMAND(ID_TOOLS_SEND_TO_DP, OnSendSetupToCaliper)
	ON_COMMAND(ID_TOOLS_SEND_TO_DPII, OnSendSetupToDPII)
	ON_COMMAND(ID_TOOLS_SEND_TO_EMAIL, OnSendSetupByEMail)

	// "Ta emot fil"
	ON_COMMAND(ID_TOOLS_RECIVE_INV_DATA_DP, OnReciveINVDataFromCaliperDP)
	ON_COMMAND(ID_TOOLS_RECIVE_INV_DATA_DPII, OnReciveINVDataFromCaliperDPII)

	// "V�rderingsbest�nd"
	ON_COMMAND(ID_TOOLS_ADD_NEW_VSTAND, OnAddNewVStandToAvtiveProperty)
	ON_COMMAND(ID_TOOLS_EVAL_FROM_CRUISE, OnCreateEvaluationFromCruise)
	ON_COMMAND(ID_TOOLS_ALL_EVAL_CRUISE, OnCreateAllEvaluationsFromCruise)
	ON_COMMAND(ID_TOOLS_SAVE_CALC_VSTAND, OnSaveCalculateVStandInAvtiveProperty)
	ON_COMMAND(ID_TOOLS_REMOVE_SEL_VSTAND, OnRemoveCalculateVStandFromAvtiveProperty)

	// "Taxeringsbest�nd"
	ON_COMMAND(ID_TOOLS_CALC_NEW_STAND, OnCalculateManuallyEnteredCruises)
	ON_COMMAND(ID_TOOLS_MATCH_PROP, OnMatchStandsToAvtiveProperty)
	ON_COMMAND(ID_TOOLS_REMOVE_SEL_STAND, OnRemoveSelStandFromProperty)
	ON_COMMAND(ID_TOOLS_MATCH_ALL_PROP, OnMatchStandsToProperties)
	ON_COMMAND(ID_TOOLS_RECALC_KRM3_STANDS, OnRecalculateK3M3ForStands)
	
	ON_COMMAND(ID_TOOLS_OPEN_SEL_STAND,OnOpenSelStand)
	ON_COMMAND(ID_TOOLS_NEW_STAND,OnCreateNewStand)
	ON_COMMAND(ID_TOOLS_SHOW_PRICELIST,OnShowPriceList)
	ON_COMMAND(ID_TOOLS_SHOW_COST_TEMPLATE,OnShowCostTemplate)

	// "Prislistor"
	ON_COMMAND(ID_TOOLS_UPDATE_PRL_TMPL, OnUpdatePricelistTemplate)
//	ON_COMMAND(ID_TOOLS_RECALC_KRM3_STANDS, OnRecalculateK3M3ForStands) Moved to "Taxeringsbes�nd"; 100505 p�d
	ON_COMMAND(ID_TOOLS_PRICELIST_INFO, OnUpdatePricelistInfoTemplate)
	ON_COMMAND(ID_TOOLS_UPDATE_COST_TMPL, OnUpdateCostTemplate)

	// "Metoder f�r Uppdatering"
	ON_COMMAND(ID_TOOLS_UPD_P30, OnUpdateP30)
	ON_COMMAND(ID_TOOLS_UPD_HIGHER_COSTS, OnUpdateHigherCosts)

	// "Batchfunktioner"
	ON_COMMAND(ID_TOOLS_CHANGE_TEMPLATE, OnChangeTemplate)
	ON_COMMAND(ID_TOOLS_CHANGE_STATUS, OnChangeStatusForStands)
	ON_COMMAND(ID_TOOLS_ADD_OTHER_COSTS, OnAddOtherCosts)
	ON_COMMAND(ID_TOOLS_GENERATE_FROM_EXCEL, OnCreateExcelData)

	// Splitter bar; set orientation"
	ON_COMMAND(ID_TOOLS_CHANGE_SPLITBAR_ORIENTATION, OnChangeSplitBarOrientation)

	//#3385 Hantera objektstatus
	ON_COMMAND(ID_TOOLS_SET_OBJ_FINISHED, OnSetObjectFinished)
	ON_COMMAND(ID_TOOLS_SET_OBJ_ONGOING, OnSetObjectOngoing)
	ON_COMMAND(ID_TOOLS_SHOW_OBJ_ONGOING, OnShowObjectOngoing)
	ON_COMMAND(ID_TOOLS_SHOW_OBJ_FINISHED, OnShowObjectFinished)
	ON_COMMAND(ID_TOOLS_SHOW_OBJ_ALL, OnShowObjectAll)

	ON_XTP_CREATECONTROL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CMDI1950ForrestNormFrame construction/destruction

XTPDockingPanePaintTheme CMDI1950ForrestNormFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CMDI1950ForrestNormFrame::CMDI1950ForrestNormFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bConnected = TRUE;
	m_pDB = NULL;
	m_bEnableContractorTBtn = TRUE;
	m_bEnableUpdateTBtn = TRUE;
	m_bEnableImportPropertiesTBtn = FALSE;
	m_bEnableCalculateTBtn = FALSE;
	m_bEnableRemoveTBtn = FALSE;
	m_bEnableToolTBtn = FALSE;
	m_bEnableExportTBtn = FALSE;
	m_bEnableExtDocTBtn = FALSE;
	m_bEnablePrintOutTBtn = FALSE;
	m_bGISInstalled = FALSE;

	m_bDoCheckPricelist = TRUE;
	m_fBindToDiameterClass = 0.0;
	m_fBindToObjLength = 0.0;
	m_fBindToCurrentWidth1 = 0.0;
	m_fBindToCurrentWidth2 = 0.0;
	m_fBindToAddedWidth1 = 0.0;
	m_fBindToAddedWidth2 = 0.0;
	m_fBindToParallelWidth = 0.0;
	m_fBindToPriceDev1 = 100.0;
	m_fBindToPriceDev2 = 100.0;

	m_bBindToIsVAT = TRUE;						// "Moms (rotpost)"
	m_bBindToIsVoluntaryDeal = TRUE;	// "Frivillig uppg�relse"
	m_bBindToIsHigherCosts = TRUE;		// "F�rdyrad avverkning"
	m_bBindToIsOtherCompens = TRUE;		// "Annan ers�ttning"
	m_bBindToIsGrot = TRUE;						// "Grot"

	m_pToolsPopup = NULL;

	m_bAlreadySaidOkToClose = FALSE;

	m_bInitReports = FALSE;

	m_bDoCompleteReCalc = TRUE;	// Set to do complete recalc as default; 090525 p�d

	m_dForceCompleteReCalc = FALSE;

	m_dNeedToRecalcAll = TRUE;

	m_nProdIDRecalc = -1;

	m_nDoCacluateAction = 0;	// => run saveObject in method isCalculationdataOK; 091202 p�d	

	m_bObjectsIsFinished = FALSE;	//new #3385
}

CMDI1950ForrestNormFrame::~CMDI1950ForrestNormFrame()
{
}

void CMDI1950ForrestNormFrame::OnDestroy(void)
{
	m_vecExchangeFunc.clear();
	m_vecForrestNormFunc.clear();
	m_vecTransactionPricelist.clear();
	m_vecTransaction_costtempl.clear();

	m_vecExcludedItemsOnObjectInfoTabSelected.clear();
	m_vecExcludedItemsOnPropertyTabSelected.clear();
	m_vecExcludedSubMenuItems.clear();	//new #3385

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_1950_FORREST_NORM_ENTRY_KEY);
	SavePlacement(this, csBuf);

	m_bFirstOpen = TRUE;

	if (m_wndProgressDlg.GetSafeHwnd())
		m_wndProgressDlg.DestroyWindow();

	if (m_pDB != NULL)
		delete m_pDB;

}

void CMDI1950ForrestNormFrame::OnClose(void)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Save state of docking pane(s)
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save((REG_WP_1950_FORREST_NORM_ENTRY_LAYOUT_KEY));

	if (m_bAlreadySaidOkToClose)
		CMDIChildWnd::OnClose();
	else	if (okToClose())
		CMDIChildWnd::OnClose();
}


void CMDI1950ForrestNormFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}


int CMDI1950ForrestNormFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	EnableDocking(CBRS_ALIGN_ANY);

	// Send message to HMSShell, please send back the DB connection; 070430 p�d
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

		// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg.Create(this, IDD_FIELDCHOOSER1,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS_DLG))
	{
		return -1;      // fail to create
	}
	// Initialize dialog bar m_wndFieldChooser evaluated; 090610 p�d
	if (!m_wndFieldChooserDlg_evalue.Create(this, IDD_FIELDCHOOSER_EVALUE,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS_DLG_EVALUE))
	{
		return -1;      // fail to create
	}
		// Initialize dialog bar m_wndFieldChooser cruised; 090610 p�d
	if (!m_wndFieldChooserDlg_cruise.Create(this, IDD_FIELDCHOOSER_CRUISE,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS_DLG_CRUISE))
	{
		return -1;      // fail to create
	}

	// docking for field chooser
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			if (m_wndFieldChooserDlg.GetSafeHwnd())
			{
				m_wndFieldChooserDlg.SetWindowTextW(xml.str(IDS_STRING4400));
			}
			if (m_wndFieldChooserDlg_evalue.GetSafeHwnd())
			{
				m_wndFieldChooserDlg_evalue.SetWindowTextW(xml.str(IDS_STRING4401));
			}
			if (m_wndFieldChooserDlg_cruise.GetSafeHwnd())
			{
				m_wndFieldChooserDlg_cruise.SetWindowTextW(xml.str(IDS_STRING4402));
			}
		}
		xml.clean();
	}

	// Check if GIS module is installed (this will be used to enabled/disable toolbar button)
	m_bIsGISModule = fileExists(getSuitesDir() + _T("UMGIS.dll"));

	// Don't show fieldchooser at this stadge; 070220 p�d
	m_wndFieldChooserDlg.EnableDocking(0);
	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	m_wndFieldChooserDlg_evalue.EnableDocking(0);
	ShowControlBar(&m_wndFieldChooserDlg_evalue, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg_evalue, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	m_wndFieldChooserDlg_cruise.EnableDocking(0);
	ShowControlBar(&m_wndFieldChooserDlg_cruise, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg_cruise, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	int iIndex = m_wndStatusBar.GetPaneCount();

	// Create the progress window control; 100203 p�d
	if (!m_wndProgressDlg.Create(CProgressDlg::IDD,this))
	{
		TRACE0("Failed to create progress window control.\n");
		return -1;
	}

	// Initialize the pane info and add the control.
	int nIndex = m_wndStatusBar.CommandToIndex(ID_INDICATOR_PROG);
	ASSERT (nIndex != -1);

	RECT rect;
	GetWindowRect(&rect);
	m_wndStatusBar.SetPaneWidth(0, 70);
	m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS); //m_wndStatusBar.GetPaneStyle(0) | SBPS_NOBORDERS);
	m_wndStatusBar.SetPaneWidth(1, 300);
	m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH | SBPS_NORMAL); //m_wndStatusBar.GetPaneStyle(1) | SBPS_STRETCH | SBPS_NORMAL);
	m_wndStatusBar.SetPaneWidth(2, 100);
	m_wndStatusBar.SetPaneStyle(2, m_wndStatusBar.GetPaneStyle(3));
	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);
	m_wndToolBar.EnableDocking(xtpFlagStretched);

	if (m_wndInfoDlgBar.GetSafeHwnd() == NULL)
	{
		if (!m_wndInfoDlgBar.Create(this, IDD_DIALOGBAR, CBRS_TOP /*| CBRS_SIZE_DYNAMIC*/, 777))
			return -1;
	}
	m_wndInfoDlgBar.EnableDocking(CBRS_ALIGN_ANY);

	// control bars objects have been created and docked.
	m_paneManager.InstallDockingPanes(this);

	CXTPDockingPane *paneProp2 = m_paneManager.CreatePane(ID_SETTINGS_PANE, CRect(0, 0,210, 120), xtpPaneDockLeft);
	paneProp2->SetOptions( xtpPaneNoCloseable );

	m_paneManager.SetTheme(m_themeCurrent);

	setLanguage();

	setStatusBarText(0,m_sStatus);
	setStatusBarText(1,m_sStatusOK);
	// Load the previous state for docking panes.
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if (layoutNormal.Load((REG_WP_1950_FORREST_NORM_ENTRY_LAYOUT_KEY)))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}

	m_bFirstOpen = TRUE;

	// Get pricelists 
	getPricelistsFromDB();
	// Get costs
	getCostTemplateFromDB();

	nShowObjectStatusFlag = 0;	//new #3385 h�ller reda p� vilket visa alt. som �r aktivt, default visa endast p�g�ende

	return 0; // creation ok
}

BOOL CMDI1950ForrestNormFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

void CMDI1950ForrestNormFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDI1950ForrestNormFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

//	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	if(bShow && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_1950_FORREST_NORM_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDI1950ForrestNormFrame::OnSetFocus(CWnd* pWnd)
{
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and save data; 080409 p�d
		pObjView = pFrame->getObjectFormView();
		if (pObjView != NULL)
		{
			pObjView->doSetNavigationButtons();
			// Need to release ref. pointers; 080520 p�d
			pObjView = NULL;
		}
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}

		// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
		// with ID_LPARAM_COMMAND2.
		// The return message holds a _user_msg structure, collected in the
		// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	CMDIChildWnd::OnSetFocus(pWnd);
}

int CMDI1950ForrestNormFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{

	if(lpCreateControl->nID == ID_BUTTON_EXPORT)
	{
		CMyControlPopup	*pToolsPopup = new CMyControlPopup();
		pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES,(xml.str(IDS_STRING22647)),1);			
				pToolsPopup->addMenuIDAndText();	// Add separator
				pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ERBJ,(xml.str(IDS_STRING226470)),2);				
				pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_VP,(xml.str(IDS_STRING226471)),2);				
				pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ELLEVIO,(xml.str(IDS_STRING226475)),2);
				pToolsPopup->addMenuIDAndText();	// Add separator
				pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_XML,(xml.str(IDS_STRING226472)),3);				
				pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_JSON,(xml.str(IDS_STRING226473)),3);				
				xml.clean();
			}
		}
		lpCreateControl->pControl = pToolsPopup;

		return TRUE;
	}

	if (lpCreateControl->nID == ID_TBBTN_TOOLS)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(0,xml.str(IDS_STRING22640),false));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(2,xml.str(IDS_STRING22660),false));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(4,xml.str(IDS_STRING22664),true));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(6,xml.str(IDS_STRING22641),false));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(8,xml.str(IDS_STRING22642),false));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(10,xml.str(IDS_STRING22686),true));		// Batchfunktioner
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(12,xml.str(IDS_STRING2800),true));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(14,xml.str(IDS_STRING22671),false));
				m_vecExcludedItemsOnObjectInfoTabSelected.push_back(CMenuItemsEnabled(16,xml.str(IDS_STRING22678),true));	//new #3385 OBS! Om �ndrar fr�n id 16 s� �ndra �ven i setExcludedItems och  void CMyControlPopup::setSubMenuChecked(int idx, CMenu *menu)

				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(0,xml.str(IDS_STRING22640),true));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(2,xml.str(IDS_STRING22660),true));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(4,xml.str(IDS_STRING22664),true));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(6,xml.str(IDS_STRING22641),true));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(8,xml.str(IDS_STRING22642),true));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(10,xml.str(IDS_STRING22686),true));			// Batchfunktioner
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(12,xml.str(IDS_STRING2800),false));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(14,xml.str(IDS_STRING22671),true));
				m_vecExcludedItemsOnPropertyTabSelected.push_back(CMenuItemsEnabled(16,xml.str(IDS_STRING22678),false));	//new #3385

				m_pToolsPopup = new CMyControlPopup();
				m_pToolsPopup->SetStyle(xtpButtonIcon);
				m_pToolsPopup->setExcludedItems(m_vecExcludedItemsOnObjectInfoTabSelected);

				//R�knare f�r ordningen i menyn
				int menuOrder=1;

				//#3385 s�tt submeny alternativen f�r objektsstatus
				setSubMenuItemsExcluded(m_recActiveObject.getObjStatus());
				
				
				// "Submeny: Fastigheter"; 090323 p�d
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_DO_PROP_GROUPINGS,xml.str(IDS_STRING22654)+L" " + xml.str(IDS_STRING226540),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_DO_PROP_GROUPINGS2,xml.str(IDS_STRING22654)+L" " + xml.str(IDS_STRING226541),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_DEL_PROP_GROUPING,xml.str(IDS_STRING22670),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_CHANGE_STATUS,xml.str(IDS_STRING22659),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_ADD_PROPERTY,xml.str(IDS_STRING2260),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_ADD_PROPERTY_FROM_FILE,xml.str(IDS_STRING22601),menuOrder); //#5008 PH 20160615
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_REMOVE_PROPERTY,xml.str(IDS_STRING2264) + L"\tCtrl+Delete",menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_REMOVE_PROPERTY2,xml.str(IDS_STRING22644) + L"\tCtrl+Shift+Delete",menuOrder);
				

				m_pToolsPopup->setSubMenuIDAndText();				
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES,xml.str(IDS_STRING22647),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ERBJ,xml.str(IDS_STRING226470),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_VP,xml.str(IDS_STRING226471),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ELLEVIO,xml.str(IDS_STRING226475),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_XML,xml.str(IDS_STRING226472),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EXPORT_PROPERTIES_JSON,xml.str(IDS_STRING226473),menuOrder);


				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_LOG_BOOK_SETTINGS,xml.str(IDS_STRING22800),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22640),menuOrder);

				// Add separator
				m_pToolsPopup->addMenuIDAndText();

				//#5009 "Submeny: Mark�gare"; 160609 PH
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_PROPERTY_OWNER_LIST,xml.str(IDS_STRING33112),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING33111),menuOrder);
				
				// Add separator
				m_pToolsPopup->addMenuIDAndText();	

				// "Submeny: Hantera setup-filer"; 090323 p�d
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_CALIPER_SETUP_FILE,xml.str(IDS_STRING22661),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SEND_TO_DP,xml.str(IDS_STRING22662),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SEND_TO_DPII,xml.str(IDS_STRING22692),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SEND_TO_EMAIL,xml.str(IDS_STRING22663),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22660),menuOrder);

				// Add separator
				m_pToolsPopup->addMenuIDAndText();

				// Add separator
				m_pToolsPopup->addMenuIDAndText();

				// "Submeny: Ta emot fil"
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_RECIVE_INV_DATA_DP,xml.str(IDS_STRING22690),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_RECIVE_INV_DATA_DPII,xml.str(IDS_STRING22691),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22664),menuOrder);
				
				// Add separator
				m_pToolsPopup->addMenuIDAndText();

				// "Submeny: V�rderingsbest�nd"; 090323 p�d
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_ADD_NEW_VSTAND,xml.str(IDS_STRING22651),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EVAL_FROM_CRUISE,xml.str(IDS_STRING22666),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_ALL_EVAL_CRUISE,xml.str(IDS_STRING22669),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SAVE_CALC_VSTAND,xml.str(IDS_STRING22652),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_REMOVE_SEL_VSTAND,xml.str(IDS_STRING22653),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22641),menuOrder);

				// Add separator
				m_pToolsPopup->addMenuIDAndText();

				// "Submeny: Taxeringsbest�nd"; 090323 p�d
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_CALC_NEW_STAND,xml.str(IDS_STRING22649),menuOrder);	// Added 090911 p�d
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_MATCH_PROP,xml.str(IDS_STRING22650),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_REMOVE_SEL_STAND,xml.str(IDS_STRING22657),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_OPEN_SEL_STAND,xml.str(IDS_STRING22672),menuOrder);//�ppna markerat best�nd
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_NEW_STAND,xml.str(IDS_STRING22648),menuOrder);//Nytt Taxeringsbest�nd (Manuell inl�sning)

				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_EVAL_FROM_CRUISE,xml.str(IDS_STRING22666),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SHOW_PRICELIST,xml.str(IDS_STRING22707),menuOrder);//Visa prislista
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SHOW_COST_TEMPLATE,xml.str(IDS_STRING22708),menuOrder);//Visa kostnadmall
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22642),menuOrder);

				// Add separator
				m_pToolsPopup->addMenuIDAndText();

				// "Submeny: Batchfunktioner"; 090918 p�d
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_CHANGE_TEMPLATE,xml.str(IDS_STRING22679),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_CHANGE_STATUS,xml.str(IDS_STRING22659),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_ADD_OTHER_COSTS,xml.str(IDS_STRING4233),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_GENERATE_FROM_EXCEL,xml.str(IDS_STRING22706),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22686),menuOrder);

				// Add separator
				m_pToolsPopup->addMenuIDAndText();	// Add separator

				// "Submeny: Uppdatera"; 100303 p�d
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_UPD_P30,xml.str(IDS_STRING2801),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_UPD_HIGHER_COSTS,xml.str(IDS_STRING2802),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING2800),menuOrder);


				// Add separator
				m_pToolsPopup->addMenuIDAndText();	// Add separator

				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CHANGE_SPLITBAR_ORIENTATION,(xml.str(IDS_STRING22671)));

				//#3385
				// Add separator
				m_pToolsPopup->addMenuIDAndText();	// Add separator

				// "Submeny: Objekt";
				menuOrder++;
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SET_OBJ_FINISHED,xml.str(IDS_STRING1045),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SET_OBJ_ONGOING,xml.str(IDS_STRING1046),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText();
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SHOW_OBJ_ONGOING,xml.str(IDS_STRING1047),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SHOW_OBJ_FINISHED,xml.str(IDS_STRING1048),menuOrder);
				m_pToolsPopup->setSubMenuIDAndText(ID_TOOLS_SHOW_OBJ_ALL,xml.str(IDS_STRING1049),menuOrder);
				m_pToolsPopup->addMenuIDAndText(-menuOrder,xml.str(IDS_STRING22678),menuOrder);

				m_sVoluntaryDealLocal = xml.str(IDS_STRING3014);
				m_sVoluntaryDealRegional = xml.str(IDS_STRING3036);
				m_sVoluntaryDealSwedishEnergy = xml.str(IDS_STRING3037);
				m_sVoluntaryDealCurrency = xml.str(IDS_STRING3038);

				xml.clean();
			}
		}

		lpCreateControl->pControl = m_pToolsPopup;

		return TRUE;
	}
	if (lpCreateControl->nID == ID_TBTN_COMBO)
	{

		CRect rCombo;
		if (!m_cbPrintOuts.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_TBTN_COMBO) )
		{
			AfxMessageBox(_T("ERROR:\nCMDIStandEntryFormFrame::OnCreateControl"));
		}
		else
		{
			m_cbPrintOuts.SetOwner(this);
			GetWindowRect(&rCombo);
			m_cbPrintOuts.SetLblFont(15,FW_NORMAL);     
			m_cbPrintOuts.MoveWindow(0, 3, 200, 20);

	    CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cbPrintOuts);
      pCB->SetFlags(xtpFlagRightAlign);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
      lpCreateControl->pControl = pCB;
		}
		return TRUE;
	}

	return FALSE;

}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDI1950ForrestNormFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW_SELLIST,m_sLangFN,lParam);
	}
	else if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		if (!m_bInitReports)
		{

			// The return message holds a _user_msg structure, collected in the
			// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
			// In this case the szFileName item in _user_msg structure holds
			// the path and filename of the ShellData file used and the szArgStr
			// holds the Suite/UserModule name; 070410 p�d
			_user_msg *msg = (_user_msg*)lParam;
			if (msg != NULL)
			{
				m_sShellDataFile = msg->getFileName();
				//int nIndex = msg->getIndex();
				m_nShellDataIndex = 5200; // explicit set Identifer, macth id in ShellData file; 090212 p�d
				m_cbPrintOuts.setLanguageFN(m_sLangFN);
				getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);
				m_cbPrintOuts.setSTDReportsInCBox(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex);
				int nObjID = getActiveObject()->getObjID_pk();
				m_cbPrintOuts.EnableWindow(nObjID > -1);
			}	// if (msg != NULL)
			m_bInitReports = TRUE;
		} // if (!m_bInitReports) 
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 1)	// Identifer for InData module
			{
				// Force total recalculate; 090922 p�d
				m_dForceCompleteReCalc = true;
				// Do not save object in isCalculationdataOK() 091202 p�d
				m_nDoCacluateAction = 1;	
				// Calculate data; 081114 p�d
				if( !OnCalculateTBtn() )
				{
					// #4643: uppdatera vald fastighet om fastighetsfliken �r aktiv
					CMDI1950ForrestNormFormView *pView = NULL;
					CPropertiesFormView *pPropView = NULL;
					if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
					{
						if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
						{
							pPropView->setActivePropertyInReport();
						}
					}
					pView = NULL;
					pPropView = NULL;
				}

				// Save object in isCalculationdataOK() 091202 p�d
				m_nDoCacluateAction = 0;	
				// Reset not total recalculate; 090922 p�d
				m_dForceCompleteReCalc = false;
			}	// if (msg->getValue1() == 1)
			else if (msg->getValue1() == 11)	// Identifer for GIS module
			{
				CObjectFormView *pObjView = NULL;
				CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
				if (pFrame != NULL)
				{
					// Try to get the ObjectView, and save data; 080409 p�d
					pObjView = pFrame->getObjectFormView();
					if (pObjView != NULL)
					{
						pObjView->doPopulateLastObject(msg->getValue2());
						// Need to release ref. pointers; 080520 p�d
						pObjView = NULL;
					}
					// Need to release ref. pointers; 080520 p�d
					pFrame = NULL;
				}
			}
			else if (msg->getValue1() == 5200)	// Identifer for UMLandValue; 090917 p�d
			{
				// Create the progress window control; 100203 p�d
				CProgressDlg2 prosDlg;
				if (!prosDlg.Create(CProgressDlg2::IDD,this))
				{
					TRACE0("Failed to create progress window control.\n");
				}
				if (prosDlg.GetSafeHwnd())
				{
					prosDlg.ShowWindow(SW_NORMAL);
					prosDlg.setProgressPos(30);
				}

				// Changed from "doMatchStandsToAvtiveProperty(true)" to "doMatchStandsToAvtiveProperty(false)"; 090924 p�d
				if (doMatchStandsToAvtiveProperty(false))
				{
					if (prosDlg.GetSafeHwnd())
					{
						prosDlg.setProgressPos(60);
					}
					OnCalculateManuallyEnteredCruises();
					if (prosDlg.GetSafeHwnd())
					{
						prosDlg.setProgressPos(90);
					}
					doCreateEvaluationFromCruise(true);	// false = auto adding of "Taxeringsbest�nd" to "V�rdering"; 090924 p�d
					if (prosDlg.GetSafeHwnd())
					{
						prosDlg.ShowWindow(SW_HIDE);
						DeleteObject(prosDlg);
					}

				}
			}
			else if (msg->getValue1() == 123)	// Identifer for Forrest suite External Docs; 090508 p�d
			{
				// We need to update data in Evaluation and Cruise, in case the entered
				// data from INV-file, have been changed; 090508 p�d
				CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
				if (pFormView != NULL)
				{
					CPropertiesFormView *pProp = pFormView->getPropertiesFormView();
					if (pProp != NULL)
					{
						pProp->populateProperties();
						// Need to release ref. pointers; 090508 p�d
						pProp = NULL;
					}
					// Need to release ref. pointers; 090508 p�d
					pFormView = NULL;
				}
			}
			else if(msg->getValue1() == 1234) //Identifier for Forrest suite import data #5008 PH 20160615
			{
				CMDI1950ForrestNormFormView *pView = NULL;
				CPropertiesFormView *pPropView = NULL;
				if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
				{
					if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
					{
						pPropView->populateProperties();
					}
				}
				pView = NULL;
				pPropView = NULL;
			}
			else if(msg->getValue1() == 12345) //Identifier for Forrest suite Add contact #5009 PH 20160616
			{
				if(m_pDB!=NULL) {
					int propId=msg->getValue2();
					LPCTSTR objName;
					LPCTSTR propName;
					LPCTSTR propNumber;
					int objId;
					m_pDB->getPropertyInfoForDialog(propId,objName,propName,propNumber,objId);

					CPropertyOwnersDlg *pDlg = new CPropertyOwnersDlg();
					if (pDlg != NULL)
					{
						pDlg->setDBConnection(m_pDB);
						pDlg->setObjectAndPropertyInfo(objName,propName,propNumber,propId);
						pDlg->setObjectId(objId);
						pDlg->DoModal();
						delete pDlg;
					}

				}
			}

		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x03 )
	{
		// 090331 PeterL
		// TODO: skapa nytt objekt med fastigheterna i arrayen (f�rsta int anger antalet fastigheter)
		// TODO: skicka tillbaka objektid p� det skapade objektet

		CObjectFormView *pObjView = NULL;
		CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pFrame != NULL)
		{
			// Try to get the ObjectView, and save data; 080409 p�d
			pObjView = pFrame->getObjectFormView();
			if (pObjView != NULL)
			{
				pObjView->createObjectFromGIS((int*)lParam);
				// Need to release ref. pointers; 080520 p�d
				pObjView = NULL;
			}
			// Need to release ref. pointers; 080520 p�d
			pFrame = NULL;
		}

	}
	else if(wParam == ID_WPARAM_VALUE_FROM + 0x04 ) //Identifier for Forest Suite shortcut Open obj and property
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			//Load object and set property to active
			CObjectFormView *pObjView = NULL;
			CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
			if (pFrame != NULL)
			{
				// Try to get the ObjectView, and save data; 080409 p�d
				pObjView = pFrame->getObjectFormView();
				if (pObjView != NULL)
				{
					pObjView->doPopulateObjectAndProperty(msg->getValue1(),msg->getValue2());
					// Need to release ref. pointers; 080520 p�d
					pObjView = NULL;
				}
				// Need to release ref. pointers; 080520 p�d
				pFrame = NULL;
			}
		}
	}
	else if (wParam == ID_EXECUTE_ITEM)
	{
		// Recalculate Object; 090130 p�d
		OnCalculateTBtn();
	}
	else
	{
		CDocument *pDoc = NULL;
		CView *pView = getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			pDoc = pView->GetDocument();
			if (pDoc != NULL)
			{
				POSITION pos = pDoc->GetFirstViewPosition();
				while (pos != NULL)
				{
					CView *pView1 = pDoc->GetNextView(pos);
					pView1->SendMessage(MSG_IN_SUITE,wParam,lParam);
				}	// while (pos != NULL)
			}	// if (pDoc != NULL)
		}
	}
	return 0L;
}

LRESULT CMDI1950ForrestNormFrame::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	int nObjID = getActiveObject()->getObjID_pk();

	m_nSelPrintOut = (int)wParam;

	m_bEnablePrintOutTBtn = (m_nSelPrintOut > -1 && nObjID > -1);
	m_cbPrintOuts.EnableWindow(nObjID > -1);

	return 0L;
}


LRESULT CMDI1950ForrestNormFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		// get a pointer to the docking pane being shown.
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;
		if (!pPane->IsValid())
		{
			if (pPane->GetID() == ID_SETTINGS_PANE)
			{
				if (!m_wndSettingsGrid.m_hWnd)
				{
					m_wndSettingsGrid.Create(CRect(0, 0, 0, 0), this, 1001);
					m_wndSettingsGrid.SetOwner(this);
					m_wndSettingsGrid.ShowHelp( FALSE );
					m_wndSettingsGrid.SetTheme(xtpGridThemeOffice2003);
					m_wndSettingsGrid.SetViewDivider(0.55);
					pPane->Attach(&m_wndSettingsGrid);				
				}
				if (m_wndSettingsGrid.m_hWnd)
				{
					CXTPPropertyGridItem* pSettings  = NULL;
					//===============================================================================
					// CATEGORY; Rotpost
				// Commented out 100226 p�d
				//pSettings  = m_wndSettingsGrid.AddCategory((m_sSettingGridCap1));
					// Item: Exchange (Utbyte,Prislista); 080115 p�d
					//setupTypeOfExchangefunctionsInPropertyGrid(pSettings);
					// Item: Prislista; 090918 p�d
					//setupExchangefunctionsInPropertyGrid(pSettings);
					// Item: Costs; 080115 p�d
					//setupTypeOfCostsInPropertyGrid(pSettings);
					// Item: The rest; 080408 p�d
				// Commented out 100226 p�d
				//setupOtherInfoInPropertyGrid(pSettings);
					// Expand category; 080409 p�d
				// Commented out 100226 p�d
 				//pSettings->Expand();

					//===============================================================================
					// CATEGORY; Intr�ng
					pSettings  = m_wndSettingsGrid.AddCategory((m_sSettingGridCap2));
					// Item: 				
					setupTypeOfInfringInPropertyGrid(pSettings);
					// Item: 				
					setupTypeOfInfringNormInPropertyGrid(pSettings);
					// Item: 				
					setupTypeOfHighCostTablesInPropertyGrid(pSettings);
					// Item: 				
//					setupLatitudeAndAltitudeInPropertyGrid(pSettings);
//					setupVFallHighCostDataInPropertyGrid(pSettings);
					// Item: 				
					setupTypeOfMiscInfringInPropertyGrid(pSettings);
					// Expand category; 080409 p�d
					pSettings->Expand();

					//===============================================================================
					// CATEGORY; "Frivillig uppg�relse"
					pSettings  = m_wndSettingsGrid.AddCategory((m_sVoluntaryDeal));
					// Item: 				
					setupWoluntaryDealSettingsInPropertyGrid(pSettings);
					// Expand category; 080409 p�d
					pSettings->Expand();

					//===============================================================================
					// CATEGORY; Misc. settings
					pSettings  = m_wndSettingsGrid.AddCategory((m_sSettingGridCap3));
					// Item: 				
					setupMiscSettingsInPropertyGrid(pSettings);
					// Expand category; 080409 p�d
					pSettings->Expand();

					//===============================================================================
					// CATEGORY; Settings for reports
					pSettings  = m_wndSettingsGrid.AddCategory((m_sSettingGridCap4));

					setupReportSettingsInPropertyGrid(pSettings);
					// Expand category; 080409 p�d
					pSettings->Expand();

					// Populate settings here, to make sure the first items data
					// is displayed in the Settings PropertyGrid; 080401 p�d
					setPopulateSettings();

				}
			}
		} // if (!pPane->IsValid())

		return TRUE; // handled
	}	// if (wParam == XTP_DPN_SHOWWINDOW)
	return FALSE;

}

LRESULT CMDI1950ForrestNormFrame::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	CString S;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;

	if (wParam == XTP_PGN_ITEMVALUE_CHANGED)
	{
/*
		if (pItem->GetID() == ID_EXCH_FUNC_CB)
		{
			setupExchangefunctionsInPropertyGrid(pItem);
		}

		if (pItem->GetID() == ID_PRL_FUNC_CB)
		{
			// Set private data members, on selecting a pricelist; 080408 p�d
			getSelectedPricelistInformation(pItem);
			// Only do update on Active object; 080408 p�d
			saveObject_prl(pItem);
		}
		else if (pItem->GetID() == ID_COST_TMPL)
		{
			getSelectedCostInformation(pItem);
			// Only do update on Active object; 080408 p�d
			saveObject_cost(pItem);
		}
*/
		if (pItem->GetID() == ID_TYPEOF_INFR)
		{
			setBroadening();
			// Only do update on Active object; 080408 p�d
			saveObject_typeof_infr(pItem); 
		}
		else if (pItem->GetID() == ID_TYPEOF_HIGH_COST)
		{
			// Only do update on Active object; 080408 p�d
			saveObject_high_cost(pItem);
		}
		else if (pItem->GetID() == ID_FORREST_NORM)
		{
			getSelectedNormInformation(pItem);
			// Only do update on Active object; 080408 p�d
			saveObject_norm(pItem);
			// Add P30-pricelists to PropertyGrid, depending on 
			// selected type of ForrestNorm; 090417 p�d
			setupP30PricelistsInPropertyGrid(pItem,true);
			// Add GrowthAreat PropertyGrid, depending on
			// selected type of ForrestNorm; 090511 p�d
			setupGrowthAreaInPropertyGrid(pItem,true);
		}
		else if (pItem->GetID() == ID_P30_PRICE_CB)
		{		
			CObjectFormView *pObjView = NULL;
			CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
			if (pFrame != NULL)
			{
				BOOL bIsNewItem = (pFrame->getAction() == NEW_ITEM);
				pFrame->setAction(UPD_ITEM);
				getSelectedP30Information(pItem);
				BOOL bRet = setGrowthAreaOnSelectedP30Price();
				// Only do update on Active object; 080408 p�d
				saveObject_p30(pItem,!bRet);
				// Only do update on Active object; 080408 p�d
				if (bRet) 
					saveObject_growth_area(pItem,false);

				// Try to get the ObjectView, and save data; 080409 p�d
				/*if ((pObjView = pFrame->getObjectFormView()) != NULL && !bIsNewItem)
				{
					pObjView->doPopulateLastObject(m_recActiveObject.getObjID_pk());
				}

				// Try to get the ObjectView, and save data; 080409 p�d
				if ((pObjView = pFrame->getObjectFormView()) != NULL && bIsNewItem)
				{
						pObjView->doPopulateFromLastAddedObject();
				}*/
				OnCalculateTBtn();
			}
		}
		else if (pItem->GetID() == ID_GROWTH_AREA_CB)
		{		
			getSelectedGrowthAreaInformation(pItem);
			// Only do update on Active object; 080408 p�d
			saveObject_growth_area(pItem);
		}
/*	COMMENTED OUT, NOT USED; 090528 p�d
		else if (pItem->GetID() == ID_LATITUDE || 
						 pItem->GetID() == ID_ALTITUDE)
		{
			saveObject_latitude_altitude(pItem);
		}
		// Added 2009-05-29 P�D
		// Not used 2009-05-30 P�D (PL)
		else if (pItem->GetID() == ID_VFALL_BREAK || 
						 pItem->GetID() == ID_VFALL_FACTOR)
		{
			saveObject_vfall_break_factor(pItem);
		}
*/
		// Added 2009-05-30 P�D
		else if (pItem->GetID() == ID_CURRENT_WIDTH)
		{
			saveObject_curr_width(pItem);
		}		
		else if (pItem->GetID() == ID_ADDED_WIDTH)
		{
			saveObject_added_widths(pItem);
		}		
		else if (pItem->GetID() == ID_PARALLEL_WIDTH)
		{
			saveObject_parallel_width(pItem);
			updateCurrentWidth();
		}
		else if (pItem->GetID() == ID_OBJ_LENGTH)
		{
			saveObject_length(pItem);
		}		
		else if (pItem->GetID() == ID_DCLS)
		{
			// Only do update on Active object; 080408 p�d
			saveObject_dcls(pItem);
		}
		else if (pItem->GetID() == ID_RECALCULATE_BY)
		{
			// Only do update on Active object; 080408 p�d
			saveObject_recalc_by(pItem);
		}
		else if (pItem->GetID() == ID_VAT)
		{
			saveObject_pricebase_max_percent(pItem);
		}
		//Lagt till Id f�r "moms p� rotpostvariabel" h�r s� att eventuellt f�r�ndrad ja nej p� den ocks� sparas undan
		//20120531 J� #3116
		else if (pItem->GetID() == ID_ID_VOLUNTARY_DEAL || 
						 pItem->GetID() == ID_ID_HIGHER_COSTS || 
						 pItem->GetID() == ID_ID_OTHER_COMP ||
						 pItem->GetID() == ID_IS_GROT ||
						 pItem->GetID() == ID_IS_VAT)
		{
			saveObject_include_exclude(pItem);
		}
		else if (pItem->GetID() == ID_PRICE_DEVIATION)
		{
			saveObject_price_deviation(pItem);
		}
		else if (pItem->GetID() == ID_DATA_DIR)
		{
			// Update searchpath for Object inventory directory in
			// m_wndSettingsGrid; 080205 p�d
			m_sBindToDataDirectory = getRegisterObjectInventoryDir();
			m_wndSettingsGrid.Refresh();
			m_wndSettingsGrid.UpdateWindow();
		}
		else if (pItem->GetID() == ID_TYPE_OF_NET)
		{
			saveObject_type_of_net(pItem);

			CMDI1950ForrestNormFormView *pView = NULL;
			CPropertiesFormView *pPropView = NULL;
			if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
			{
				pPropView = (CPropertiesFormView*)pView->getPropertiesFormView();
			}

			// Change percentages based on selected policy
 			if (getTypeOfNet() == TYPEOFNET_SVENSKAKRAFTNAT)
			{
				// Values based on policy from "Svenska Kraftn�t"
				m_fBindToPercentOfPriceBase	= 10.0;
				m_fBindToCurrPercent		= 15.0;
				m_fBindToMaxPercent			= 20.0;

				if (pPropView)
				{
					pPropView->changeVoluntaryDealCaption(m_sVoluntaryDealRegional + _T(" ") + m_sVoluntaryDealCurrency);
				}
			}
			else if (getTypeOfNet() == TYPEOFNET_SWEDISHENERGY)
			{
				// Values based on policy from "Svensk Energi"
				m_fBindToPercentOfPriceBase	= 3.0;
				m_fBindToCurrPercent		= 20.0;
				m_fBindToMaxPercent			= 20.0;

				if (pPropView)
				{
					pPropView->changeVoluntaryDealCaption(m_sVoluntaryDealSwedishEnergy + _T(" ") + m_sVoluntaryDealCurrency);
				}
			}
			else if(getTypeOfNet()==TYPEOFNET_SWEDISHENERGY_2019) //HMS-75 20220208 J� Ny s�rskild ers svensk energi 2019
			{
				// Values based on policy from "Svensk Energi 2019"
				m_fBindToPercentOfPriceBase	= 5.0;
				m_fBindToCurrPercent		= 20.0;
				m_fBindToMaxPercent			= 20.0;

				if (pPropView)
				{
					pPropView->changeVoluntaryDealCaption(m_sVoluntaryDealSwedishEnergy + _T(" ") + m_sVoluntaryDealCurrency);
				}			
			}
			else
			{
				// Old local net
				if (pPropView)
				{
					pPropView->changeVoluntaryDealCaption(m_sVoluntaryDealLocal + _T(" ") + m_sVoluntaryDealCurrency);
				}
			}
			m_wndSettingsGrid.Refresh();
			saveObject_pricebase_max_percent(NULL);

			// Recalc
			OnCalculateTBtn();
		}	// else if (pItem->GetID() == ID_TYPE_OF_NET)
		else if (pItem->GetID() == ID_PRICE_BASE || 
						 pItem->GetID() == ID_MAX_PERCENT || 
						 pItem->GetID() == ID_CURRENT_PERCENT ||
						 pItem->GetID() == ID_PERCENT_OF_BASE_PRICE // #4420 20150803 J�
						 )// ||
						 
						 //pItem->GetID() == ID_VAT)
		{
			saveObject_pricebase_max_percent(pItem);
		}	// 
	}

	return 0;
}

// Annan kostnad; Added 2011-08-22 p�d
// R�knar �ven om v�rde f�r "Intr�ng"
void CMDI1950ForrestNormFrame::recalcOtherCosts()
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->recalulateAllOtherCosts();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

/*
LRESULT CMDI1950ForrestNormFrame::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}
*/

// CMDI1950ForrestNormFrame diagnostics

#ifdef _DEBUG
void CMDI1950ForrestNormFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDI1950ForrestNormFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDI1950ForrestNormFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_1950_FORREST_NORM_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_1950_FORREST_NORM_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDI1950ForrestNormFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// PRIVATE

void CMDI1950ForrestNormFrame::setLanguage(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{

			m_sPropGridCap1 = (xml->str(IDS_STRING2020));
			m_sPropGridCap2 = (xml->str(IDS_STRING2021));
			m_sPropGridCap3 = (xml->str(IDS_STRING2022));

			m_sSettingGridCap1 = (xml->str(IDS_STRING224));
			m_sSettingGridCap2 = (xml->str(IDS_STRING225));
			m_sSettingGridCap3 = (xml->str(IDS_STRING2330));
			m_sSettingGridCap4 = (xml->str(IDS_STRING423));

			m_sYes = (xml->str(IDS_STRING110));
			m_sNo  = (xml->str(IDS_STRING111));

			m_sOK = (xml->str(IDS_STRING22695));
			m_sCancelDlg = (xml->str(IDS_STRING22696));

			m_sChangeTemplateMsg1.Format(_T("%s\n%s\n\n%s\n"),
				xml->str(IDS_STRING22681),
				xml->str(IDS_STRING22682),
				xml->str(IDS_STRING22683));

			m_sChangeTemplateMsg2 =	xml->str(IDS_STRING22684);

			m_sMissingFileForPrintOutMsg = xml->str(IDS_STRING22685);

			m_sCalculateRotpost = (xml->str(IDS_STRING2240));
			m_sPricelist = (xml->str(IDS_STRING2241));
			m_sCosts = (xml->str(IDS_STRING2242));
			m_sDiameterClass = (xml->str(IDS_STRING280));
			m_sSoderbergs = (xml->str(IDS_STRING281));
			m_sIsAtCoast = (xml->str(IDS_STRING2810));
			m_sIsSouthEast = (xml->str(IDS_STRING2811));
			m_sIsRegion5 = (xml->str(IDS_STRING2812));
			m_sIsPartOfPlot = (xml->str(IDS_STRING2813));
			m_sSIH100_pine = (xml->str(IDS_STRING2814));

			m_sTypeOfInfring = xml->str(IDS_STRING214);
			m_sForrestNorm = (xml->str(IDS_STRING2250));
			m_sP30Price = (xml->str(IDS_STRING2210));
			m_sGrowthArea = (xml->str(IDS_STRING207));
			m_sLatitude = (xml->str(IDS_STRING216));
			m_sAltitude = (xml->str(IDS_STRING217));
			m_sHighCost = (xml->str(IDS_STRING2211));
			m_sTakeCareOfPercent = (xml->str(IDS_STRING2201));
			m_sCorrectionFactor = (xml->str(IDS_STRING2205));
			m_sLengthOfObject = (xml->str(IDS_STRING2082));
			m_sCurrentWidth = (xml->str(IDS_STRING208));
			m_sCurrentWidth1 = (xml->str(IDS_STRING2080));
			m_sCurrentWidth2 = (xml->str(IDS_STRING2081));
			m_sParallelWidth = (xml->str(IDS_STRING2083));
			m_sAddedWidth = (xml->str(IDS_STRING209));
			m_sAddedWidth1 = (xml->str(IDS_STRING2090));
			m_sAddedWidth2 = (xml->str(IDS_STRING2091));
			m_sTypeOfNet  = (xml->str(IDS_STRING212));
			m_sPriceDeviation = (xml->str(IDS_STRING218));
			m_sPriceDeviation1 = (xml->str(IDS_STRING2180));
			m_sPriceDeviation2 = (xml->str(IDS_STRING2181));
			m_sVFallBreak = (xml->str(IDS_STRING4300));
			m_sVFallFactor = (xml->str(IDS_STRING4301));
			m_sInfrPercent = (xml->str(IDS_STRING220));

			m_sDataDirectory = (xml->str(IDS_STRING2331));
			m_sActiveDataDirectory = (xml->str(IDS_STRING2332));
			m_sCurrPercent = (xml->str(IDS_STRING2192));	// "Nuvarande procentsats"
			m_sVAT = (xml->str(IDS_STRING2193));	// "Moms"

			m_sPriceBase = (xml->str(IDS_STRING2190));		// "Prisbasbelopp"
			m_sMaxPerc = (xml->str(IDS_STRING2191));			// "Max procent"
			m_sPercOfPriceBase = (xml->str(IDS_STRING2194));			// "Procent av prisbasbelopp"	#4420 20150803 J�

			m_sVATForRotpost = (xml->str(IDS_STRING4230));		// "Inkludera/Exkludera i MOMS f�r rotpost"
			m_sVoluntaryDeal = (xml->str(IDS_STRING4231));		// "Inkludera/Exkludera i Frivillig uppg�relse"
			m_sHigherCosts = (xml->str(IDS_STRING4232));			// "Inkludera/Exkludera i F�rdyrad avverkning"
			m_sOtherCompensation = (xml->str(IDS_STRING4233));// "Inkludera/Exkludera i Annan ers�ttning"
			m_sGROT = (xml->str(IDS_STRING4234));// "Inkludera/Exkludera i GROT"
			
			m_sStatus = (xml->str(IDS_STRING2270));
			m_sStatusOK = (xml->str(IDS_STRING2271));
			m_sStatusWorking = (xml->str(IDS_STRING2272));

			m_sMsgReplaceUpdTemplate = (xml->str(IDS_STRING290));
			m_sMsgReCalcUpdCruise = (xml->str(IDS_STRING291));
			m_sMsgReCalcUpdEval = (xml->str(IDS_STRING292));
			m_sMsgFinishOff = (xml->str(IDS_STRING293));
			m_sMsgProperty = (xml->str(IDS_STRING294));
			m_sMsgStand = (xml->str(IDS_STRING295));
			m_sMsgObject = (xml->str(IDS_STRING296));

			m_sSearchDlgTitle = (xml->str(IDS_STRING2410));

			m_sLoggMsg1.Format(_T("%s\n%s\n\n%s\n\n"),
				(xml->str(IDS_STRING2285)),
				(xml->str(IDS_STRING2286)),
				(xml->str(IDS_STRING2287)));
			
			m_sRestrictionOnReCalc.Format(_T("%s\n%s\n\n"),
				(xml->str(IDS_STRING2288)),
				(xml->str(IDS_STRING2289)));
			m_sDoRecalculation = xml->str(IDS_STRING2294);

			m_sMsgCouldNotSaveObject= xml->str(IDS_STRING2296);
			
			m_sDoCompleteReCalc = xml->str(IDS_STRING6100);
/*
			m_sMsgCreateEvaluetionsFromStands.Format(_T("%s\n\n%s\n"),
						(xml->str(IDS_STRING3208)),
						(xml->str(IDS_STRING3209)));
*/
			m_sMsgCreateEvaluetionsFromStands.Format(_T("%s\n\n"),
						xml->str(IDS_STRING3208));

			m_sMsgNoEvaluationStandsOnRandTrees.Format(_T("%s\n%s\n\n"),
						(xml->str(IDS_STRING3220)),
						(xml->str(IDS_STRING3221)));

/*
			m_sLoggMsg2.Format("%s\r\n\r\n%s %s\r\n\r\n%s\r\n%s\r\n%s\r\n\r\n",
				(xml->str(IDS_STRING2280)),
				(xml->str(IDS_STRING2281)),
				getDateTime(),
				(xml->str(IDS_STRING2282)),
				(xml->str(IDS_STRING2283)),
				(xml->str(IDS_STRING2284)));

			
*/
			m_sLogErrorHeader.Format(_T("%s\r\n\r\n%s %s\r\n\r\n"),
				(xml->str(IDS_STRING3443)),
				(xml->str(IDS_STRING3444)),
				getDateTime());

			m_sLogErrorMsgSoderberg.Format(_T("%s\r\n"),
				(xml->str(IDS_STRING3448)));

			m_sLogErrorMsg2.Format(_T("%s\r\n%s\r\n\r\n"),
					(xml->str(IDS_STRING3445)),
					(xml->str(IDS_STRING3447)));

			m_sPricelistCompareMsg.Format(_T("%s\n%s\n%s\n%s\n\n%s\n\n"),
					(xml->str(IDS_STRING52170)),
					(xml->str(IDS_STRING52171)),
					(xml->str(IDS_STRING52172)),
					(xml->str(IDS_STRING52173)),
					(xml->str(IDS_STRING52174)));

			m_sRecalcVStandMsg.Format(_T("%s\n%s\n%s\n\n"),
					(xml->str(IDS_STRING1200)),
					(xml->str(IDS_STRING1201)),
					(xml->str(IDS_STRING1202)));

			m_sLogRecalcMsg	= (xml->str(IDS_STRING3500));

			m_sMsgCap	= (xml->str(IDS_STRING229));
			// Added 2008-06-18 P�D
			m_sMsgEMailSubject = (xml->str(IDS_STRING58267));
			m_sMsgEMailText = (xml->str(IDS_STRING58268));
			
			m_sMsgUpdP30 = xml->str(IDS_STRING2803);
			m_sMsgUpdHigherCosts = xml->str(IDS_STRING2804);
			m_sMsgP30NotFound = xml->str(IDS_STRING6325);
			m_sMsgHigherCostsNotFound = xml->str(IDS_STRING6326);

			m_sMsgNoObjects = (xml->str(IDS_STRING5903));
			m_sMsgNoProperties = (xml->str(IDS_STRING5904));
			m_sMsgNoProperties_eval.Format(_T("%s\n%s\n"),
				xml->str(IDS_STRING22673),
				xml->str(IDS_STRING22674));
			m_sMsgNoProperties_cruise.Format(_T("%s\n%s\n"),
				xml->str(IDS_STRING22673),
				xml->str(IDS_STRING22675));

			m_sMsgNoPropertiesToCalc1 = xml->str(IDS_STRING2293);
			m_sMsgNoPropertiesToCalc2 = xml->str(IDS_STRING2295);

			m_sMsgBarkFuncErrSoderbergs= xml->str(IDS_STRING2297);
			m_sMsgHgtFuncErrSoderbergs= xml->str(IDS_STRING2298);
			m_sHeightForH25Unreasonable = xml->str(IDS_STRING2299);	//_T("F�ljande best�nd har tr�dh�jd(er) som �r utanf�r omr�det (%d-%dm) f�r H25: ");
			m_sHeightForH25Unreasonable2 = xml->str(IDS_STRING22991);	//"(tr�dslag och antal inom parentes)"


			m_sMsgPrintOut1 = xml->str(IDS_STRING4500);
			m_sMsgPrintOut2 = xml->str(IDS_STRING4501);
			m_sMsgPrintOut3 = xml->str(IDS_STRING4502);
			m_sMsgPrintOut4 = xml->str(IDS_STRING4510);
			m_sMsgPrintOut5 = xml->str(IDS_STRING4511);
			m_sMsgPrintOut6 = xml->str(IDS_STRING4512);
			m_sMsgPrintOut7 = xml->str(IDS_STRING4513);
			m_sMsgPrintOut8 = xml->str(IDS_STRING4514);
			m_sMsgPrintOut9 = xml->str(IDS_STRING4515);

			m_sLogAddOtherCompMsg = (xml->str(IDS_STRING3501));

			m_sMsgGrowthAreaError.Format(_T("%s\n\n%s\n\n%s\n\n- %s\n\n- %s\n\n"),
				xml->str(IDS_STRING5509),
				xml->str(IDS_STRING5505),
				xml->str(IDS_STRING5506),
				xml->str(IDS_STRING5507),
				xml->str(IDS_STRING5508));

			m_sLogCap = (xml->str(IDS_STRING2300));
			m_sCancel = (xml->str(IDS_STRING2301));
			m_sPrintOut = (xml->str(IDS_STRING2302));
			m_sSaveToFile = (xml->str(IDS_STRING2303));
			// "Typ av intr�ng"; 080326 p�d
			m_arrTypeOfNet.Add((xml->str(IDS_STRING2120)));
			m_arrTypeOfNet.Add((xml->str(IDS_STRING2121)));
			m_arrTypeOfNet.Add((xml->str(IDS_STRING2122)));
			m_arrTypeOfNet.Add((xml->str(IDS_STRING2123)));

			//#3385
			m_sSetAsFinished = xml->str(IDS_STRING1170);
			m_sSetAsOngoing = xml->str(IDS_STRING1171);

			// m_wndInfoDlgBar information dialog bar; 080428 p�d
			if (m_wndInfoDlgBar.GetSafeHwnd())
			{
				m_wndInfoDlgBar.setObjID(xml->str(IDS_STRING1149));
				m_wndInfoDlgBar.setTimeStamp(xml->str(IDS_STRING11490));
				m_wndInfoDlgBar.setObjCap((xml->str(IDS_STRING1150)));
				m_wndInfoDlgBar.setPropCap((xml->str(IDS_STRING1151)),(xml->str(IDS_STRING1165)) );
			}

			CXTPDockingPane* pSetProp = GetDockingPaneManager()->FindPane(ID_SETTINGS_PANE);
			ASSERT(pSetProp);
			if (pSetProp) 
			{
				pSetProp->SetTitle((xml->str(IDS_STRING223)));
				//pSetProp->Hide();
			}

			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{
				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR1)
						{		
							setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_CONTRACTOR,_T(""),FALSE); //xml->str(IDS_STRING2263));	Hide 100226 p�d
							setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_CONTRACTOR,_T(""),FALSE);	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_CALCULATE,xml->str(IDS_STRING2261));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_IMPORT,xml->str(IDS_STRING22665));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(4),RES_TB_CONTRACTOR,_T(""),FALSE);	//

							setToolbarBtnIcon(sTBResFN,p->GetAt(5),RES_TB_EXPORT,xml->str(IDS_STRING226474));	//

							setToolbarBtnIcon(sTBResFN,p->GetAt(6),RES_TB_TOOLS,xml->str(IDS_STRING2265));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(7),RES_TB_PAPER_CLIP,xml->str(IDS_STRING2266));	//
							pCtrl = p->GetAt(8);
							pCtrl->SetStyle(xtpButtonCaption);
							pCtrl->SetCaption((xml->str(IDS_STRING4250)));
							setToolbarBtnIcon(sTBResFN,p->GetAt(10),RES_TB_PRINT,xml->str(IDS_STRING4251));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(11),RES_TB_GIS,xml->str(IDS_STRING2269));	//
							



						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())
			}	// if (fileExists(sTBResFN))

		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
}

void CMDI1950ForrestNormFrame::clearSettingsPropertyGrid(void)
{
	//Nollar vissa inst�llningsvariabler #3500, 20121203 J�
	m_sBindToHighCostSelected = _T("");
	m_sBindToP30Selected = _T("");
	m_sBindToGrowthAreaSelected= _T("");
	m_sBindToTypeOfInfr= _T("");
	m_fBindToAddedWidth1 = 0.0;
	m_fBindToAddedWidth2= 0.0;
	m_fBindToParallelWidth = 0.0;

	m_sBinToExchPricelistStr = _T("");
	m_sBindToPricelistSelected = _T("");
	m_sBindToCosts = _T("");
	m_sBindToForrestNorm = _T("");
	m_sBindToTypeOfNet = _T("");
	m_fBindToTakeCareOfPercent = 0.0;
	m_fBindToCorrectionFactor = 0.0;
	m_fBindToDiameterClass = 0.0;
	m_fBindToObjLength = 0.0;
	m_fBindToCurrentWidth1 = 0.0;
	m_fBindToCurrentWidth2 = 0.0;
	m_bBindToIsVAT = TRUE;						// "Moms (rotpost)"
	m_bBindToIsVoluntaryDeal = TRUE;	// "Frivillig uppg�relse"
	m_bBindToIsHigherCosts = TRUE;		// "F�rdyrad avverkning"
	m_bBindToIsOtherCompens = TRUE;		// "Annan ers�ttning"
	m_bBindToIsGrot = TRUE;						// "Grot"
	// Uppr�kning procent p� Intr�ng; 100706 p�d
	m_fBindToInfrPercent = 0.0;

/*
	CCustomItemChilds* pCurrentWidth = (CCustomItemChilds*)m_wndSettingsGrid.FindItem(ID_CURRENT_WIDTH);
	if (pCurrentWidth != NULL)
		pCurrentWidth->setValues(&m_fBindToCurrentWidth1,&m_fBindToCurrentWidth2);
*/
	setBroadening();

	m_fBindToPriceDev1 = 100.0;	// "I skogsgatan"
	m_fBindToPriceDev2 = 100.0;	// "Kanttr�d"
	CCustomItemChilds* pPriceDev = (CCustomItemChilds*)m_wndSettingsGrid.FindItem(ID_PRICE_DEVIATION);
	if (pPriceDev != NULL)
		pPriceDev->setValues(&m_fBindToPriceDev1,&m_fBindToPriceDev2);

	m_bBindToIsAtCoast = m_bBindToIsSouthEast = m_bBindToIsRegion5 = m_bBindToIsPartOfPlot = FALSE;
	m_sBindToSIH100_pine = _T("");
	CCustomItemChilds_2* pSoderbergs = (CCustomItemChilds_2*)m_wndSettingsGrid.FindItem(ID_SODERBERGS);
	if (pSoderbergs != NULL)
		pSoderbergs->setValues(&m_bBindToIsAtCoast,&m_bBindToIsSouthEast,&m_bBindToIsRegion5,&m_bBindToIsPartOfPlot,&m_sBindToSIH100_pine);

	m_fBindToPriceBase = 0.0;
	m_fBindToMaxPercent = 0.0;
	m_fBindToCurrPercent = 0.0;
	m_fBindToPercentOfPriceBase = 0.0;
	m_fBindToVAT = 0.0;

	m_wndSettingsGrid.Refresh();
}

void CMDI1950ForrestNormFrame::enablePropertyGrid(BOOL enable)
{
	CXTPDockingPane* pSetProp = GetDockingPaneManager()->FindPane(ID_SETTINGS_PANE);
	if (pSetProp != NULL) 
	{
		if (enable) pSetProp->SetEnabled(xtpPaneEnabled);
		else pSetProp->SetEnabled(xtpPaneDisabled);
	}
}

void CMDI1950ForrestNormFrame::setupTypeOfExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItem *pExchangeItem = pItem->AddChildItem(new CXTPPropertyGridItem((m_sCalculateRotpost)));
	if (pExchangeItem != NULL)
	{
		pExchangeItem->BindToString(&m_sBinToExchPricelistStr);
		pExchangeItem->SetReadOnly();
		pExchangeItem->SetID(ID_EXCH_FUNC_CB);	// Set ID in PropertyGrid; 070413 p�d
	}
}

// This function is called from OnGridNotify
// Actaully adds pricelists for selected exchange functions; 080115 p�d
void CMDI1950ForrestNormFrame::setupExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItem *pPrlTmpl = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem(m_sPricelist));
	if (pPrlTmpl != NULL)
	{
		pPrlTmpl->BindToString(&m_sBindToPricelistSelected);
		pPrlTmpl->SetReadOnly();
		pPrlTmpl->SetID(ID_PRL_FUNC_CB);
	}

}

// This function is called from OnDockingPaneNotify
void CMDI1950ForrestNormFrame::setupTypeOfCostsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItem *pCostTmpl = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem((m_sCosts)));
	pCostTmpl->BindToString(&m_sBindToCosts);
	pCostTmpl->SetReadOnly();
	pCostTmpl->SetID(ID_COST_TMPL);

}

// This function is called from OnDockingPaneNotify
void CMDI1950ForrestNormFrame::setupOtherInfoInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItemDouble *pDCLS = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sDiameterClass),0.0,_T("%.1f")));
	pDCLS->SetID(ID_DCLS);
	pDCLS->BindToDouble(&m_fBindToDiameterClass);

	CCustomItemChilds_2* pSoderbergs = (CCustomItemChilds_2*)pItem->AddChildItem(new CCustomItemChilds_2((m_sSoderbergs),
		(m_sIsAtCoast),
		(m_sIsSouthEast),
		(m_sIsRegion5),
		(m_sIsPartOfPlot),
		(m_sSIH100_pine),
		&m_bBindToIsAtCoast,
		&m_bBindToIsSouthEast,
		&m_bBindToIsRegion5,
		&m_bBindToIsPartOfPlot,
		&m_sBindToSIH100_pine,
		m_sYes,
		m_sNo));
	pSoderbergs->SetID(ID_SODERBERGS);
	pSoderbergs->setValues(&m_bBindToIsAtCoast,&m_bBindToIsSouthEast,&m_bBindToIsRegion5,&m_bBindToIsPartOfPlot,&m_sBindToSIH100_pine);
}


// This function is called from OnDockingPaneNotify
void CMDI1950ForrestNormFrame::setupTypeOfInfringInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	vecUCFunctions funcs;
	CXTPPropertyGridItem *pTypeOfInfr = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem((m_sTypeOfInfring)));
	pTypeOfInfr->BindToString(&m_sBindToTypeOfInfr);
	getForrestNormTypeOfInfring(funcs);
	if (funcs.size() > 0)
	{
		for (UINT i = 0;i < funcs.size();i++)
		{
			pTypeOfInfr->GetConstraints()->AddConstraint(funcs[i].getName(),i);
		}
	}
	pTypeOfInfr->SetFlags(xtpGridItemHasComboButton);
	pTypeOfInfr->SetConstraintEdit( FALSE );
	pTypeOfInfr->SetID(ID_TYPEOF_INFR);
}

// This function is called from OnDockingPaneNotify
void CMDI1950ForrestNormFrame::setupTypeOfInfringNormInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	// "Lista skogsnormer; 090417 p�d
	UCFunctions flist;
	CXTPPropertyGridItem *pForrestNorm = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem((m_sForrestNorm)));
	pForrestNorm->BindToString(&m_sBindToForrestNorm);
	getForrestNormFunctions(m_vecForrestNormFunc);
	if (m_vecForrestNormFunc.size() > 0)
	{
		for (UINT i = 0;i < m_vecForrestNormFunc.size();i++)
		{
			flist = m_vecForrestNormFunc[i];
			pForrestNorm->GetConstraints()->AddConstraint((flist.getName()),i);
		}
	}
	pForrestNorm->SetFlags(xtpGridItemHasComboButton);
	pForrestNorm->SetConstraintEdit( FALSE );
	pForrestNorm->SetID(ID_FORREST_NORM);

	setupP30PricelistsInPropertyGrid(pForrestNorm);
	// Added 2009-05-11 P�D
	setupGrowthAreaInPropertyGrid(pForrestNorm);
}

void CMDI1950ForrestNormFrame::setupP30PricelistsInPropertyGrid(CXTPPropertyGridItem *pItem,bool reset)
{
	CString sInfo,S;
	int nIndex = -1;
	CXTPPropertyGridItem *pP30Item = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (!pItem->HasChilds())
	{
		pP30Item = pItem->AddChildItem(new CXTPPropertyGridItem(m_sP30Price));
		pP30Item->BindToString(&m_sBindToP30Selected);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pP30Item = pGridChilds->GetAt(0);
			if (reset) pP30Item->GetConstraints()->SetCurrent(-1);
			m_sBindToP30Selected = _T("");
			pItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pP30Item != NULL)
	{
		if (nIndex == 0) getObjectP30TemplatesFromDB(TEMPLATE_P30);
		else if (nIndex == 1)	getObjectP30TemplatesFromDB(TEMPLATE_P30_NEW_NORM);
		else if (nIndex == 2)	getObjectP30TemplatesFromDB(TEMPLATE_P30_2018_NORM); // #1 20180321 J� Lista bara 2018 p30 prislistor
		pItem->Expand();
		pP30Item->GetConstraints()->RemoveAll();
		// Add pricelists for Exchange calculation model selected; 070430 p�d
		if (m_vecObjectP30Template.size() > 0)
		{
			for (UINT i = 0;i < m_vecObjectP30Template.size();i++)
			{
				CTransaction_template tmpl = m_vecObjectP30Template[i];
				sInfo.Format(_T("%s"),tmpl.getTemplateName());
				pP30Item->GetConstraints()->AddConstraint(sInfo,i);
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			//m_sBindToP30Selected = m_recActiveObject.getObjP30Name();
		}	// if (m_vecVolFuncList.size() > 0)

		pP30Item->SetFlags(xtpGridItemHasComboButton);
		pP30Item->SetConstraintEdit( FALSE );
		pP30Item->SetID(ID_P30_PRICE_CB);
	} // 	if (pP30Item != NULL)
}

void CMDI1950ForrestNormFrame::setupGrowthAreaInPropertyGrid(CXTPPropertyGridItem *pItem,bool reset)
{
	CString sInfo,sP30Name,S;
	int nIndex = -1;
	CXTPPropertyGridItem *pGrowthAreaItem = NULL;
	CXTPPropertyGridItems *pGridChilds = NULL;

	CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
	if (pItemConstraints != NULL)
	{
		nIndex = pItemConstraints->GetCurrent();
	} //	if (pItemConstraints != NULL)
	if (pItem->GetChilds()->GetCount() == 1)
//	if (!pItem->HasChilds())
	{
		pGrowthAreaItem = pItem->AddChildItem(new CXTPPropertyGridItem(m_sGrowthArea));
		pGrowthAreaItem->BindToString(&m_sBindToGrowthAreaSelected);
	}	// 	if (!pItem->HasChilds())
	else
	{
		pGridChilds = pItem->GetChilds();
		if (pGridChilds != NULL)
		{
			pGrowthAreaItem = pGridChilds->GetAt(1);
			if (reset) pGrowthAreaItem->GetConstraints()->SetCurrent(-1);
			m_sBindToGrowthAreaSelected = _T("");
			pGrowthAreaItem->GetGrid()->Refresh();
		}	// if (pGridChilds != NULL)
	} // else

	if (pGrowthAreaItem != NULL)
	{
		pItem->Expand();
		pGrowthAreaItem->GetConstraints()->RemoveAll();
		if (nIndex == 0) // 1950:�rs norm
		{
			for (short i = 1;i <= 6;i++)
			{
				sInfo.Format(_T("%d"),i);
				pGrowthAreaItem->GetConstraints()->AddConstraint(sInfo,i);
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			pGrowthAreaItem->SetReadOnly(0); //#4521 20151012 J�
		}
		else if (nIndex == 1) // Skogsnormen ver1.1
		{
			for (short i = 1;i <= 5;i++)
			{
				sInfo.Format(_T("%d"),i);
				pGrowthAreaItem->GetConstraints()->AddConstraint(sInfo,i);
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			pGrowthAreaItem->SetReadOnly(1);//#4521 20151012 J�
		}
		else if (nIndex == 2) // 2018 �rs norm
		{
			for (short i = 1;i <= 5;i++)
			{
				sInfo.Format(_T("%d"),i);
				pGrowthAreaItem->GetConstraints()->AddConstraint(sInfo,i);
			}	// for (UINT i = 0;i < m_vecVolFuncList.size();i++)
			pGrowthAreaItem->SetReadOnly(1);
		}

		pGrowthAreaItem->SetFlags(xtpGridItemHasComboButton);
		pGrowthAreaItem->SetConstraintEdit( FALSE );
		pGrowthAreaItem->SetID(ID_GROWTH_AREA_CB);
	} // 	if (pP30Item != NULL)
}

void CMDI1950ForrestNormFrame::setupTypeOfHighCostTablesInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItem *pTypeOfHighCost = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem((m_sHighCost)));
	pTypeOfHighCost->BindToString(&m_sBindToHighCostSelected);
	getHighCostTemplateFromDB();
	if (vecTemplate_high_cost.size() > 0)
	{
		for (UINT i = 0;i < vecTemplate_high_cost.size();i++)
		{
			pTypeOfHighCost->GetConstraints()->AddConstraint(vecTemplate_high_cost[i].getTemplateName(),i);
		}
	}
	pTypeOfHighCost->SetFlags(xtpGridItemHasComboButton);
	pTypeOfHighCost->SetConstraintEdit( FALSE );
	pTypeOfHighCost->SetID(ID_TYPEOF_HIGH_COST);
}

void CMDI1950ForrestNormFrame::setupLatitudeAndAltitudeInPropertyGrid(CXTPPropertyGridItem *pItem)
{
/*
	// "Latitude"; 090511 p�d
	CXTPPropertyGridItemNumber *pLatitude = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber(m_sLatitude));
	pLatitude->SetID(ID_LATITUDE);
	pLatitude->BindToNumber(&m_nBindToLatitude);

	// "Altitude"; 090511 p�d
	CXTPPropertyGridItemNumber *pAltitude = (CXTPPropertyGridItemNumber*)pItem->AddChildItem(new CXTPPropertyGridItemNumber(m_sAltitude));
	pAltitude->SetID(ID_ALTITUDE);
	pAltitude->BindToNumber(&m_nBindToAltitude);
*/
	
}

void CMDI1950ForrestNormFrame::setupVFallHighCostDataInPropertyGrid(CXTPPropertyGridItem *pItem)
{

	// "Brytv�rde f�rdyrad avverkning"; 090528 p�d
	CXTPPropertyGridItemDouble *pVFallBreak = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sVFallBreak,0.0,_T("%.1f")));
	pVFallBreak->SetID(ID_VFALL_BREAK);
	pVFallBreak->BindToDouble(&m_fBindToVFallBreak);

	// "Faktor f�rdyrad avverkning"; 090528 p�d
	CXTPPropertyGridItemDouble *pVFallFactor = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble(m_sVFallFactor,0.0,_T("%.1f")));
	pVFallFactor->SetID(ID_VFALL_FACTOR);
	pVFallFactor->BindToDouble(&m_fBindToVFallFactor);
	
}

void CMDI1950ForrestNormFrame::setupTypeOfMiscInfringInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CXTPPropertyGridItemDouble *pTakeCareOfPercent = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sTakeCareOfPercent),0.0,_T("%.1f")));
	pTakeCareOfPercent->SetID(ID_TAKE_CARE_OF_PERCENT);
	pTakeCareOfPercent->BindToDouble(&m_fBindToTakeCareOfPercent);

/*	Commanted out 2009-03-20 (Not used PL)
	CXTPPropertyGridItemDouble *pCorrectionFactor = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sCorrectionFactor),0.0,_T("%.1f")));
	pCorrectionFactor->SetID(ID_CORRECTION_FACTOR);
	pCorrectionFactor->BindToDouble(&m_fBindToCorrectionFactor);
*/

	// Length
	CXTPPropertyGridItemDouble *pObjWidth = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sLengthOfObject),0.0,_T("%.1f")));
	pObjWidth->SetID(ID_OBJ_LENGTH);
	pObjWidth->BindToDouble(&m_fBindToObjLength);

	// Parallel width
	CXTPPropertyGridItemDouble *pParallelWidth = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sParallelWidth),0.0,_T("%.1f")));
	pParallelWidth->SetID(ID_PARALLEL_WIDTH);
	pParallelWidth->BindToDouble(&m_fBindToParallelWidth);

	// Current width
	CXTPPropertyGridItemDouble *pCurrentWidth = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sCurrentWidth),0.0,_T("%.1f")));
	pCurrentWidth->SetID(ID_CURRENT_WIDTH);
	pCurrentWidth->BindToDouble(&m_fBindToCurrentWidth1);

	// Added width
	CCustomItemChilds* pAddedWidth = (CCustomItemChilds*)pItem->AddChildItem(new CCustomItemChilds((m_sAddedWidth),
																																																 (m_sAddedWidth1),
																																																 (m_sAddedWidth2),
																																																 &m_fBindToAddedWidth1,
																																																 &m_fBindToAddedWidth2));
	pAddedWidth->SetID(ID_ADDED_WIDTH);

	setBroadening();

//****************************************************
	// "Uppr�kning av intr�ng (%)"; 100615 p�d
	CXTPPropertyGridItemDouble *pRecalcBy = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sInfrPercent),0.0,_T("%.0f")));
	pRecalcBy->SetID(ID_RECALCULATE_BY);
	pRecalcBy->BindToDouble(&m_fBindToInfrPercent);
//****************************************************/

/*
	// Added width
	CCustomItemChilds* pPriceDiv = (CCustomItemChilds*)pItem->AddChildItem(new CCustomItemChilds(_T(m_sPriceDeviation),
																																																 _T(m_sPriceDeviation1),
																																																 _T(m_sPriceDeviation2),
																																																 &m_fBindToPriceDev1,
																																																 &m_fBindToPriceDev2));
	pPriceDiv->SetID(ID_PRICE_DEVIATION);
	pPriceDiv->setValues(&m_fBindToPriceDev1,&m_fBindToPriceDev2);
*/
}

void CMDI1950ForrestNormFrame::setupWoluntaryDealSettingsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	// "Ers�ttningspolicy"
	CXTPPropertyGridItem *pTypeOfNet = (CXTPPropertyGridItem*)pItem->AddChildItem(new CXTPPropertyGridItem((m_sTypeOfNet)));
	pTypeOfNet->SetFlags(xtpGridItemHasComboButton);
	pTypeOfNet->SetConstraintEdit( FALSE );
	pTypeOfNet->SetID(ID_TYPE_OF_NET);
	pTypeOfNet->BindToString(&m_sBindToTypeOfNet);
	if (m_arrTypeOfNet.GetCount() > 0)
	{
		for (int i = 0;i < m_arrTypeOfNet.GetCount();i++)
		{
			pTypeOfNet->GetConstraints()->AddConstraint((m_arrTypeOfNet[i]),i);
		}
	}

	// "Max procent"; 080326 p�d
	CXTPPropertyGridItemDouble *pMaxPerc = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sMaxPerc),0.0,_T("%.1f")));
	pMaxPerc->SetID(ID_MAX_PERCENT);
	pMaxPerc->BindToDouble(&m_fBindToMaxPercent);
	
	// "Nuvarande procentsats"
	CXTPPropertyGridItemDouble *pCurrPerc = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sCurrPercent),0.0,_T("%.1f")));
	pCurrPerc->SetID(ID_CURRENT_PERCENT);
	pCurrPerc->BindToDouble(&m_fBindToCurrPercent);

	// Procent av prisbasbelopp #4420 20150803 J�
	CXTPPropertyGridItemDouble *pPercOfBasePrice = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sPercOfPriceBase),0.0,_T("%.1f")));
	pPercOfBasePrice->SetID(ID_PERCENT_OF_BASE_PRICE);
	pPercOfBasePrice->BindToDouble(&m_fBindToPercentOfPriceBase);	
	

}

void CMDI1950ForrestNormFrame::setupMiscSettingsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CCustomItemButton *pDataDir = (CCustomItemButton*)pItem->AddChildItem(new CCustomItemButton((m_sDataDirectory),this));
	pDataDir->SetID(ID_DATA_DIR);
	pDataDir->SetReadOnly();
	pDataDir->BindToString(&m_sBindToDataDirectory);
	m_sBindToDataDirectory = getRegisterObjectInventoryDir();

	// "Prisbasbelopp"; 080326 p�d
	CXTPPropertyGridItemDouble *pPriceBase = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sPriceBase),0.0,_T("%.1f")));
	pPriceBase->SetID(ID_PRICE_BASE);
	pPriceBase->BindToDouble(&m_fBindToPriceBase);

	// "Moms"
	CXTPPropertyGridItemDouble *pVAT = (CXTPPropertyGridItemDouble*)pItem->AddChildItem(new CXTPPropertyGridItemDouble((m_sVAT),0.0,_T("%.1f")));
	pVAT->SetID(ID_VAT);
	pVAT->BindToDouble(&m_fBindToVAT);

}

void CMDI1950ForrestNormFrame::setupReportSettingsInPropertyGrid(CXTPPropertyGridItem *pItem)
{
	CMyPropGridItemBool_1 *pIsVAT = (CMyPropGridItemBool_1*)pItem->AddChildItem(new CMyPropGridItemBool_1((m_sYes),(m_sNo),m_sVATForRotpost,TRUE));
	pIsVAT->SetID(ID_IS_VAT);
	pIsVAT->BindToBool(&m_bBindToIsVAT);

	CMyPropGridItemBool_1 *pIsVoluntaryDeal = (CMyPropGridItemBool_1*)pItem->AddChildItem(new CMyPropGridItemBool_1((m_sYes),(m_sNo),m_sVoluntaryDeal,TRUE));
	pIsVoluntaryDeal->SetID(ID_ID_VOLUNTARY_DEAL);
	pIsVoluntaryDeal->BindToBool(&m_bBindToIsVoluntaryDeal);

	CMyPropGridItemBool_1 *pIsHigherCosts = (CMyPropGridItemBool_1*)pItem->AddChildItem(new CMyPropGridItemBool_1((m_sYes),(m_sNo),m_sHigherCosts,TRUE));
	pIsHigherCosts->SetID(ID_ID_HIGHER_COSTS);
	pIsHigherCosts->BindToBool(&m_bBindToIsHigherCosts);

	CMyPropGridItemBool_1 *pIsOtherComp = (CMyPropGridItemBool_1*)pItem->AddChildItem(new CMyPropGridItemBool_1((m_sYes),(m_sNo),m_sOtherCompensation,TRUE));
	pIsOtherComp->SetID(ID_ID_OTHER_COMP);
	pIsOtherComp->BindToBool(&m_bBindToIsOtherCompens);

	CMyPropGridItemBool_1 *pIsGROT = (CMyPropGridItemBool_1*)pItem->AddChildItem(new CMyPropGridItemBool_1((m_sYes),(m_sNo),m_sGROT,TRUE));
	pIsGROT->SetID(ID_IS_GROT);
	pIsGROT->BindToBool(&m_bBindToIsGrot);

}

void CMDI1950ForrestNormFrame::setPopulateSettings(void)
{
	CString S;
	int nIndex = -1;
	UCFunctions flist;
	CXTPPropertyGridItem *pItem = NULL;

	//---------------------------------------------------------------------------------------
	// get exchange functions from UCCalculate.dll modules in
	// ..\Module directory; 070413 p�d
	getExchangeFunctions(m_vecExchangeFunc);
	for (UINT j = 0;j < m_vecExchangeFunc.size();j++)
	{
		flist = m_vecExchangeFunc[j];
		// Check if we can find the typeof in m_recTraktMiscData
		// to match the typeof in flist; 070430 p�d
		if (m_recActiveObject.getObjTypeOfPricelist() == flist.getIdentifer())
		{
			m_sBinToExchPricelistStr = flist.getName();
			nIndex = j;
			break;
		}
	}	// for (UINT j = 0;j < func_list.size();j++)
/*
	// Also setup list of Pricelists in PropertyGrid; 080328 p�d
	pItem = m_wndSettingsGrid.FindItem(ID_EXCH_FUNC_CB);
	if (pItem != NULL)
	{
		pItem->GetConstraints()->SetCurrent(nIndex);
		setupExchangefunctionsInPropertyGrid(pItem);
	}	// if (pItem != NULL)
	pItem = NULL;
*/
	// Set name of pricelist selectes to BindTo..; 080331 p�d
	m_sBindToPricelistSelected = m_recActiveObject.getObjNameOfPricelist();
	//---------------------------------------------------------------------------------------

	m_sBindToCosts = m_recActiveObject.getObjNameOfCosts();

	vecUCFunctions funcs;
	getForrestNormTypeOfInfring(funcs);
	if (_tstoi(m_recActiveObject.getObjTypeOfInfring()) >= 0 && _tstoi(m_recActiveObject.getObjTypeOfInfring()) < funcs.size())
	{
		m_sBindToTypeOfInfr = funcs[_tstoi(m_recActiveObject.getObjTypeOfInfring())].getName();
	}
	//---------------------------------------------------------------------------------------
	if (m_recActiveObject.getObjUseNorm().CompareNoCase(_T("Skogsnorm 2009")) == 0 || m_recActiveObject.getObjUseNorm().IsEmpty())
		m_sBindToForrestNorm = STRING_2009_FORREST_NORM;
	else if (m_recActiveObject.getObjUseNorm().CompareNoCase(_T("Skogsnormen ver1")) == 0 || m_recActiveObject.getObjUseNorm().IsEmpty())
		m_sBindToForrestNorm = STRING_2009_FORREST_NORM;
	else
		m_sBindToForrestNorm = m_recActiveObject.getObjUseNorm();

	// Also setup list of P30-prices in PropertyGrid; 090417 p�d
	pItem = m_wndSettingsGrid.FindItem(ID_FORREST_NORM);
	if (pItem != NULL)
	{
		/*
		if (m_recActiveObject.getObjUseNormID() == 4000 || m_recActiveObject.getObjUseNormID() == 0)
			pItem->GetConstraints()->SetCurrent(0);
		else if (m_recActiveObject.getObjUseNormID() == 4001 || m_recActiveObject.getObjUseNormID() == 1)
			pItem->GetConstraints()->SetCurrent(1);
		*/

		pItem->GetConstraints()->SetCurrent(m_recActiveObject.getObjUseNormID());

		setupP30PricelistsInPropertyGrid(pItem);
		setupGrowthAreaInPropertyGrid(pItem);

	}
	pItem = NULL;
	m_sBindToP30Selected = m_recActiveObject.getObjP30Name();
	m_sBindToGrowthAreaSelected = m_recActiveObject.getObjGrowthArea();

	//---------------------------------------------------------------------------------------

	m_sBindToHighCostSelected = m_recActiveObject.getObjHCostName();	// 090420 p�d

	// Not used 2009-05-28 P�D
	m_nBindToLatitude = 0; //m_recActiveObject.getObjLatitude();
	m_nBindToAltitude = 0; //m_recActiveObject.getObjAltitude();

// NOT USED 2009-05-30 P�D (PL)
//	m_fBindToVFallBreak = m_recActiveObject.getObjVFallBreak();
//	m_fBindToVFallFactor = m_recActiveObject.getObjVFallFactor();

	/*
	// Added 090218 p�d
	if (m_vecForrestNormFunc.size() > 0)
	{
		for (UINT i = 0;i < m_vecForrestNormFunc.size();i++)
		{
			flist = m_vecForrestNormFunc[i];
			if (m_sBindToForrestNorm.CompareNoCase(flist.getName()) == 0)
			{
				m_nNormTypeOf = flist.getIdentifer();
				// We'll set range for growth area; 090218 p�d
				CObjectFormView *pObjView = NULL;
				CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
				if (pFrame != NULL)
				{
					// Try to get the ObjectView, and save data; 080409 p�d
					pObjView = pFrame->getObjectFormView();
					if (pObjView != NULL)
					{
						if (m_nNormTypeOf == 4000)	// "1950:�rs ..."
							pObjView->setRangeForGrowthArea(1,6);
						else if (m_nNormTypeOf == 4001)	// "Ny skogsnorm ..."
							pObjView->setRangeForGrowthArea(1,5);

						// Need to release ref. pointers; 080520 p�d
						pObjView = NULL;
					}
					// Need to release ref. pointers; 080520 p�d
					pFrame = NULL;
				}
				break;
			}	// if (m_sBindToForrestNorm.CompareNoCase(flist.getName()) == 0)
		}	// for (UINT i = 0;i < m_vecForrestNormFunc.size();i++)
	}	// if (m_vecForrestNormFunc.size() > 0)
*/
	if (m_recActiveObject.getObjTypeOfNet() >= 0 && m_recActiveObject.getObjTypeOfNet() < m_arrTypeOfNet.GetCount())
	{
		m_sBindToTypeOfNet = m_arrTypeOfNet[m_recActiveObject.getObjTypeOfNet()];
	}

	m_fBindToTakeCareOfPercent = m_recActiveObject.getObjTakeCareOfPerc();
	m_fBindToCorrectionFactor = m_recActiveObject.getObjCorrFactor();
	m_fBindToObjLength = m_recActiveObject.getObjLength();
	m_fBindToCurrentWidth1 = m_recActiveObject.getObjPresentWidth1();
	m_fBindToCurrentWidth2 = m_recActiveObject.getObjPresentWidth2();	// For now equals 0.0; 080627 p�d
	m_fBindToAddedWidth1 = m_recActiveObject.getObjAddedWidth1();
	m_fBindToAddedWidth2 = m_recActiveObject.getObjAddedWidth2();
	m_fBindToParallelWidth = m_recActiveObject.getObjParallelWidth();
	setBroadening();

	m_fBindToPriceDev1 = m_recActiveObject.getObjPriceDiv1();
	m_fBindToPriceDev2 = m_recActiveObject.getObjPriceDiv2();
	CCustomItemChilds* pPriceDev = (CCustomItemChilds*)m_wndSettingsGrid.FindItem(ID_PRICE_DEVIATION);
	if (pPriceDev != NULL)
		pPriceDev->setValues(&m_fBindToPriceDev1,&m_fBindToPriceDev2);

	m_fBindToDiameterClass = m_recActiveObject.getObjDCLS();

	m_fBindToInfrPercent =  m_recActiveObject.getObjRecalcBy();

	CString sSoderbergs(m_recActiveObject.getObjExtraInfo());
	CStringArray arrStr;
	if (sSoderbergs.Right(1).Compare(_T(";")) != 0)	sSoderbergs += _T(";");
	SplitString(sSoderbergs,_T(";"),arrStr);
	// Set default values first; 080408 p�d
	m_bBindToIsAtCoast = m_bBindToIsSouthEast = m_bBindToIsRegion5 = 	m_bBindToIsPartOfPlot = FALSE;
	if (arrStr.GetCount() == 5)
	{
		m_bBindToIsAtCoast = (arrStr.GetAt(0) == _T("1") ? TRUE:FALSE);
		m_bBindToIsSouthEast = (arrStr.GetAt(1) == _T("1") ? TRUE:FALSE); 
		m_bBindToIsRegion5 = (arrStr.GetAt(2) == _T("1") ? TRUE:FALSE); 
		m_bBindToIsPartOfPlot = (arrStr.GetAt(3) == _T("1") ? TRUE:FALSE);
		m_sBindToSIH100_pine = (arrStr.GetAt(4));
	}
	CCustomItemChilds_2* pSoderbergs = (CCustomItemChilds_2*)m_wndSettingsGrid.FindItem(ID_SODERBERGS);
	if (pSoderbergs != NULL)
		pSoderbergs->setValues(&m_bBindToIsAtCoast,&m_bBindToIsSouthEast,&m_bBindToIsRegion5,&m_bBindToIsPartOfPlot,&m_sBindToSIH100_pine);

	m_fBindToPriceBase = m_recActiveObject.getObjPriceBase();
	m_fBindToMaxPercent = m_recActiveObject.getObjMaxPercent();
	m_fBindToCurrPercent = m_recActiveObject.getObjPercent();
	m_fBindToPercentOfPriceBase = m_recActiveObject.getObjPercentOfPriceBase();								//#4420 20150803 J�
	m_fBindToVAT = m_recActiveObject.getObjVAT();

	m_bBindToIsVAT = (m_recActiveObject.getObjIsVATIncluded() == 1 ? TRUE : FALSE);							// "Moms (rotpost)"
	m_bBindToIsVoluntaryDeal = (m_recActiveObject.getObjIsVoluntaryDealIncluded() == 1 ? TRUE : FALSE);		// "Frivillig uppg�relse"
	m_bBindToIsHigherCosts = (m_recActiveObject.getObjIsHigherCostsIncluded() == 1 ? TRUE : FALSE);			// "F�rdyrad avverkning"
	m_bBindToIsOtherCompens = (m_recActiveObject.getObjIsOtherCompensIncluded() == 1 ? TRUE : FALSE);		// "Annan ers�ttning"
	m_bBindToIsGrot = (m_recActiveObject.getObjIsGrotIncluded() == 1 ? TRUE : FALSE);						// "Grot"


	m_wndSettingsGrid.Refresh();

	m_wndInfoDlgBar.setObjectRecord(m_recActiveObject,FALSE);

	enablePropertyGrid(m_recActiveObject.getObjID_pk() > -1);

	
}

//#3385
void CMDI1950ForrestNormFrame::setSubMenuItemsExcluded(int obj_status)
{
	BOOL bIsAnyFinished=FALSE, bIsAnyOngoing=FALSE;

	if(m_pToolsPopup == NULL)
		return;

	bIsAnyFinished=getIsAnyObjectFinished();	//titta om det finns n�got objekt satt som slutf�rt
	bIsAnyOngoing=getIsAnyObjectOngoing();		//titta om det finns n�got objekt satt som p�g�ende

	m_vecExcludedSubMenuItems.clear();	
	
	//s�tt status alternativen
	if(obj_status == 0)	//aktuellt objekt har status p�g�ende
	{
		if(bIsAnyOngoing==FALSE)
			m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_FINISHED,false));
		else
			m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_FINISHED,true));
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_ONGOING,false));
	}
	else if(obj_status == 1)	//aktuellt objekt har status slutf�rt
	{
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_FINISHED,false));
		if(bIsAnyFinished==FALSE)
			m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_ONGOING,false));
		else
			m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_ONGOING,true));
	}
	else	//status ej satt
	{
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_FINISHED,false));
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SET_OBJ_ONGOING,false));
	}

	//s�tt visa alternativen
	if(bIsAnyOngoing)
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SHOW_OBJ_ONGOING,true));
	else
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SHOW_OBJ_ONGOING,false));

	if(bIsAnyFinished)
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SHOW_OBJ_FINISHED,true));
	else
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SHOW_OBJ_FINISHED,false));

	if(bIsAnyOngoing==FALSE && bIsAnyFinished==FALSE)	//finns varken p�g�ende eller slutf�rda
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SHOW_OBJ_ALL,false));
	else
		m_vecExcludedSubMenuItems.push_back(CSubMenuItemsEnabled(16,ID_TOOLS_SHOW_OBJ_ALL,true));

	m_pToolsPopup->setExcludedSubMenuItems(m_vecExcludedSubMenuItems);

}

void CMDI1950ForrestNormFrame::setExcludedToolItems(int tab_selected)
{
	switch (tab_selected)
	{
		case TAB_GENERAL_DATA :
			m_pToolsPopup->setExcludedItems(m_vecExcludedItemsOnObjectInfoTabSelected);
		break;
		case TAB_PROPERTIES :
			m_pToolsPopup->setExcludedItems(m_vecExcludedItemsOnPropertyTabSelected);
		break;
	};
}

int CMDI1950ForrestNormFrame::getForrestNormIndex(void)
{
	UCFunctions flist;
	getForrestNormFunctions(m_vecForrestNormFunc);
	if (m_vecForrestNormFunc.size() > 0)
	{
		for (UINT i = 0;i < m_vecForrestNormFunc.size();i++)
		{
			flist = m_vecForrestNormFunc[i];
			if (m_sBindToForrestNorm.CompareNoCase(flist.getName()) == 0)
				return i;
		}
	}
	m_sBindToForrestNorm.Empty();	// Clear this datamamber also; 081107 p�d
	return -1;	// No selection
}


int CMDI1950ForrestNormFrame::getTypeOfNet(void)					
{ 
	int nRet = -1;
	if (m_arrTypeOfNet.GetSize() > 0)
	{
		for (int i = 0;i < m_arrTypeOfNet.GetSize();i++)
		{
			if (m_sBindToTypeOfNet.CompareNoCase(m_arrTypeOfNet.GetAt(i)) == 0)
			{
				nRet = i;
				break;
			}
		}
	}
	return nRet; 

}

// Create a semicolon limited string, based
// on data entered for "S�derbergs"; 080408 p�d
CString CMDI1950ForrestNormFrame::getExtraInfo(void)				
{
	CString sExtraInfo;
	sExtraInfo.Format(_T("%d;%d;%d;%d;%s"),
		(m_bBindToIsAtCoast ? 1:0),
		(m_bBindToIsSouthEast ? 1:0),
		(m_bBindToIsRegion5 ? 1:0),
		(m_bBindToIsPartOfPlot ? 1:0),
		m_sBindToSIH100_pine);	// "Tall S�derbergs funktion"
	return sExtraInfo;
}

BOOL CMDI1950ForrestNormFrame::getTraktTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getTemplates(m_vecTraktTemplate,ID_TEMPLATE_TRAKT);	// Only valid Templates; 080407 p�d
			return TRUE;
		}
	}
	return FALSE;
}

// Pricelist set in prn_pricelist_table
void CMDI1950ForrestNormFrame::getPricelistsFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecTransactionPricelist.clear();
			m_bConnected = m_pDB->getPricelists(m_vecTransactionPricelist);
		}
	}	
}

void CMDI1950ForrestNormFrame::getCostTemplateFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecTransaction_costtempl.clear();
			m_bConnected = m_pDB->getCosts(m_vecTransaction_costtempl,COST_TYPE_2);
		}
	}
}

void CMDI1950ForrestNormFrame::getHighCostTemplateFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getObjectTemplates(vecTemplate_high_cost,TEMPLATE_HCOST);
	}
}

void CMDI1950ForrestNormFrame::getObjectP30TemplatesFromDB(short type_id)
{
	if (m_pDB != NULL)
	{
			m_pDB->getObjectTemplates(m_vecObjectP30Template,type_id);	// Only valid Templates; 080407 p�d
	}
}

void CMDI1950ForrestNormFrame::getObjectCruisesNotCalculated(void)	
{
	if (m_pDB != NULL)
	{
		m_pDB->getObjectCruisesNotCalculatedForObject(m_recActiveObject.getObjID_pk(),m_vecElvCruiseNotCalc);
	}
}

BOOL CMDI1950ForrestNormFrame::calculateThisCruise(CTransaction_elv_cruise& rec)
{
	if (rec.getECruType() == CRUISE_TYPE_2) return FALSE;
	// Do a complete recalculation, no matter what ...; 090525 p�d
	if (m_bDoCompleteReCalc) return TRUE;
	// No cruisees to calculate, already calculated; 090525 p�d
	if (m_vecElvCruiseNotCalc.size() == 0) return FALSE;

	// Cruisees to calculate; 090525 p�d
	if (m_vecElvCruiseNotCalc.size() > 0)
	{
		for (UINT i = 0;i < m_vecElvCruiseNotCalc.size();i++)
		{
			if (rec.getECruID_pk() == m_vecElvCruiseNotCalc[i].getECruID_pk() &&
					rec.getECruObjectID_pk() == m_vecElvCruiseNotCalc[i].getECruObjectID_pk() &&
					rec.getECruPropID_pk() == m_vecElvCruiseNotCalc[i].getECruPropID_pk() &&
					rec.getECruType() == CRUISE_TYPE_1)
				return TRUE;
		}
	}

	return FALSE;
}

BOOL CMDI1950ForrestNormFrame::okToClose(void)
{
	BOOL bIsOKToClose = FALSE;

	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and save data; 080409 p�d
		pObjView = pFrame->getObjectFormView();
		if (pObjView != NULL)
		{
			bIsOKToClose = pObjView->doSaveObject(1);
			// Need to release ref. pointers; 080520 p�d
			pObjView = NULL;
		}
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}

	return bIsOKToClose;
}

void CMDI1950ForrestNormFrame::getSelectedPricelistInformation(CXTPPropertyGridItem *pItem)
{
	if (pItem != NULL)
	{
		CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
		if (pItemConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pItemConstraints->GetConstraintAt(pItemConstraints->GetCurrent());
			if (pConstraint != NULL)
				m_nPriceListTypeOf = (int)pConstraint->m_dwData;
			else
				m_nPriceListTypeOf = -10;
		}

		// Load most resent information on "prislistor"; 080918 p�d
		getPricelistsFromDB();

		// Just make sure that index is within limits; 080319 p�d
		if (m_vecTransactionPricelist.size() > 0 &&
				!m_sBindToPricelistSelected.IsEmpty())
		{
			for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
			{
				CTransaction_pricelist rec = m_vecTransactionPricelist[i];
				if (m_sBindToPricelistSelected.Compare(rec.getName()) == 0 &&
						rec.getTypeOf() == m_nPriceListTypeOf)
				{
					m_sPriceListXML = rec.getPricelistFile();
					break;
				}	// if (m_sBindToPricelistSelected.Compare(rec.getName()) == 0)
			}	// for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
		}
	}	// if (pItem != NULL)
}


void CMDI1950ForrestNormFrame::getSelectedCostInformation(CXTPPropertyGridItem *pItem)
{
	if (pItem != NULL)
	{

		CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
		if (pItemConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pItemConstraints->GetConstraintAt(pItemConstraints->GetCurrent());
			if (pConstraint != NULL)
				m_nCostsTypeOf = (int)pConstraint->m_dwData;
			else
				m_nCostsTypeOf = -10;
		}

		// Load most resent information on "kostnadsmallar"; 080918 p�d
		getCostTemplateFromDB();
	
		// Just make sure that index is within limits; 080319 p�d
		if (m_vecTransaction_costtempl.size() > 0 &&
				!m_sBindToCosts.IsEmpty())
		{
			for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
			{
				CTransaction_costtempl rec = m_vecTransaction_costtempl[i];
				if (m_sBindToCosts.CompareNoCase(rec.getTemplateName()) == 0 &&
						rec.getTypeOf() == m_nCostsTypeOf)
				{
					m_sCostsXML = rec.getTemplateFile();
					//AfxMessageBox("getSelectedCostInformation\n" + rec.getTemplateName() + "\n" + m_sCostsXML);
					break;
				}	// if (m_sBindToPricelistSelected.Compare(rec.getName()) == 0)
			}	// for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
		}
	}	// if (pItem != NULL)
}

void CMDI1950ForrestNormFrame::getSelectedNormInformation(CXTPPropertyGridItem *pItem)
{
	int nIndex = -1;
	if (pItem != NULL)
	{
		CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
		if (pItemConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pItemConstraints->GetConstraintAt(pItemConstraints->GetCurrent());
			if (pConstraint != NULL)
				nIndex = (int)pConstraint->m_dwData;
			else
				nIndex = -1;
		}
/*
		// Just make sure that index is within limits; 080319 p�d
		if (nIndex >= 0 && nIndex < m_vecForrestNormFunc.size())
		{
			UCFunctions rec = m_vecForrestNormFunc[nIndex];
			m_nNormTypeOf = rec.getIdentifer();	// Identifer of Forrest Norm; 080506 p�d
			// We'll set range for growth area; 090218 p�d
			CObjectFormView *pObjView = NULL;
			CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
			if (pFrame != NULL)
			{
				// Try to get the ObjectView, and save data; 080409 p�d
				pObjView = pFrame->getObjectFormView();
				if (pObjView != NULL)
				{
					if (m_nNormTypeOf == 4000)	// "1950:�rs ..."
					{
						pObjView->setRangeForGrowthArea(1,6);
					}
					else if (m_nNormTypeOf == 4001)	// "Ny skogsnorm ..."
					{
						pObjView->setRangeForGrowthArea(1,5);
						if (pObjView->getGrowthArea() > 5)
							pObjView->setGrowthArea(_T(""));
					}

					// Need to release ref. pointers; 080520 p�d
					pObjView = NULL;
				}
				// Need to release ref. pointers; 080520 p�d
				pFrame = NULL;
			}
		}
*/
	}	// if (pItem != NULL)
}

void CMDI1950ForrestNormFrame::getSelectedP30Information(CXTPPropertyGridItem *pItem)
{
	m_nP30SelIndex = -1;
	if (pItem != NULL)
	{
		CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
		if (pItemConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pItemConstraints->GetConstraintAt(pItemConstraints->GetCurrent());
			if (pConstraint != NULL)
			{
				m_nP30SelIndex = (int)pConstraint->m_dwData;
			}
		}
	}	// if (pItem != NULL)
}

void CMDI1950ForrestNormFrame::getSelectedGrowthAreaInformation(CXTPPropertyGridItem *pItem)
{
	m_nGrowthAreaSelIndex = -1;
	if (pItem != NULL)
	{
		CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
		if (pItemConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pItemConstraints->GetConstraintAt(pItemConstraints->GetCurrent());
			if (pConstraint != NULL)
			{
				m_nGrowthAreaSelIndex = (int)pConstraint->m_dwData;
			}
		}
	}	// if (pItem != NULL)
}

BOOL CMDI1950ForrestNormFrame::setGrowthAreaOnSelectedP30Price(void)
{

	// Kontrollera vilken skogsnorm som valts. Endast den nya skogsnormen har
	// P30-prislistor som �r kopplade till tillv�xtomr�de; 100915 p�d
	CXTPPropertyGridItem* pNormItem = m_wndSettingsGrid.FindItem(ID_FORREST_NORM);
	if (pNormItem != NULL)
	{
		if (pNormItem->GetConstraints()->GetCurrent() == 0) return FALSE;
	}

	CString sName;
	int nAreaIndex;
	vecObjectTemplate_p30_nn_table vecP30_nn;
	CTransaction_template recTmpl;
	TemplateParser parser;
	if (m_vecObjectP30Template.size() > 0 && 
			m_nP30SelIndex > -1 && 
			m_nP30SelIndex < m_vecObjectP30Template.size())
	{	
		recTmpl = m_vecObjectP30Template[m_nP30SelIndex];
		CXTPPropertyGridItem* pGrowthArea = m_wndSettingsGrid.FindItem(ID_GROWTH_AREA_CB);
		if (pGrowthArea != NULL)
		{
			if (parser.LoadFromBuffer(recTmpl.getTemplateFile()))
			{
				CXTPPropertyGridItemConstraints *pGrowthAreaConstraints = pGrowthArea->GetConstraints();
				parser.getObjTmplP30_nn(vecP30_nn,sName,&nAreaIndex);
				if (pGrowthAreaConstraints != NULL)
				{
					switch (nAreaIndex)
					{
						case 0 : m_sBindToGrowthAreaSelected = L"1"; pGrowthAreaConstraints->SetCurrent(0); m_nGrowthAreaSelIndex = 0; break;
						case 1 : m_sBindToGrowthAreaSelected = L"2"; pGrowthAreaConstraints->SetCurrent(1); m_nGrowthAreaSelIndex = 1; break;
						case 2 : m_sBindToGrowthAreaSelected = L"3"; pGrowthAreaConstraints->SetCurrent(2);	m_nGrowthAreaSelIndex = 2; break;
						case 3 :
						case 4 : m_sBindToGrowthAreaSelected = L"4"; pGrowthAreaConstraints->SetCurrent(3);	m_nGrowthAreaSelIndex = 3; break;
						case 5 : m_sBindToGrowthAreaSelected = L"5"; pGrowthAreaConstraints->SetCurrent(4);	m_nGrowthAreaSelIndex = 4; break;
					};
					pGrowthArea->GetGrid()->Refresh();
				}	// if (pGrowthAreaConstraints != NULL)
			}	// if (parser.LoadFromBuffer(recTmpl.getTemplateFile()))
		}	// if (pGrowthArea != NULL)
	}
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_prl(CXTPPropertyGridItem *pItem)
{
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			bIsObjUpdates = m_pDB->updObject_prl(nObjID,m_sBindToPricelistSelected,m_nPriceListTypeOf,m_sPriceListXML);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)

	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
	if (bIsObjUpdates)
			OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_cost(CXTPPropertyGridItem *pItem)
{
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			bIsObjUpdates = m_pDB->updObject_cost(nObjID,m_sBindToCosts,m_nCostsTypeOf,m_sCostsXML);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)

	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	
	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
	if (bIsObjUpdates)
			OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_typeof_infr(CXTPPropertyGridItem *pItem)
{
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;

	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pItem != NULL && pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 090403 p�d
		pObjView = pFrame->getObjectFormView();

		if (pObjView != NULL)
		{
			// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 090403 p�d
			// COMMENTED OUT 110627 P�D; No need to do this check. Just save
			//if (pFrame->getAction() == UPD_ITEM)
			//{
				nObjID = pObjView->getActiveObjectID_pk();
				// Only update the active Object; 090403 p�d
				m_sBindToTypeOfInfrIndex.Format(_T("%d"),getTypeOfInfrIndex());
				bIsObjUpdates = m_pDB->updObject_typeof_infr(nObjID,m_sBindToTypeOfInfrIndex);
				// On change, repopulate object; 090403 p�d
				//pObjView->doPopulateData(pObjView->getObjIndex());
				
				// Also update cached value; #5082 160815 PeterL
				CTransaction_elv_object *pObj = getActiveObject();
				if (pObj)
				{
					pObj->setObjTypeOfInfring(m_sBindToTypeOfInfrIndex);
				}

			//}	// if (pFrame->getAction() == UPD_ITEM)
			// Need to release ref. pointers; 090403 p�d
			pObjView = NULL;
		}	// if (pObjView != NULL)
		
		// Need to release ref. pointers; 090403 p�d
		pFrame = NULL;

		// If we have an update on object, i think we should
		// also recaluclate; 080918 p�d
		if (bIsObjUpdates)
				OnCalculateTBtn();

	}	// if (pItem != NULL)

	return TRUE;
}

void CMDI1950ForrestNormFrame::updateCurrentWidth()
{
	CXTPPropertyGridItemDouble* pCurrentWidth = (CXTPPropertyGridItemDouble*)m_wndSettingsGrid.FindItem(ID_CURRENT_WIDTH);
	if( pCurrentWidth )
	{
		pCurrentWidth->SetReadOnly(getTypeOfInfrIndex() == 0 && !(m_fBindToParallelWidth > 0)); // Nybyggnation
	}
}

/************************************************************************/
/* Sets if the broadening values is active or not depending on what type*/ 
/* of infringem that is used.                                           */
/*                                                                      */
/* @return BOOL - TRUE if the infringem is set to use widening values.  */
/************************************************************************/
BOOL CMDI1950ForrestNormFrame::setBroadening()
{
	BOOL isBroadening=FALSE;
	BOOL isNewConstruction=FALSE;
	//#1972 mathias; 110317 Only activate "Breddning values" when "Breddning" is the active selection.
	//otherwize ressett to 0, and deactive them.
	CCustomItemChilds* pAddedWidth = (CCustomItemChilds*)m_wndSettingsGrid.FindItem(ID_ADDED_WIDTH);
	CXTPPropertyGridItemDouble* pParallelWidth = (CXTPPropertyGridItemDouble*)m_wndSettingsGrid.FindItem(ID_PARALLEL_WIDTH);

	if (pAddedWidth != NULL)
	{
		//Index values is used because string values is taken from database instead of language XML files.
		//TODO: #2036 Change the strings to id for the language xml files, in that case this can be changed.
		int index=getTypeOfInfrIndex();
		isBroadening=(index==1);

		if(!isBroadening)
		{
			m_fBindToAddedWidth1=0.0;
			m_fBindToAddedWidth2=0.0;
		}
		pAddedWidth->setValues(&m_fBindToAddedWidth1,&m_fBindToAddedWidth2);
		pAddedWidth->SetReadOnly(!isBroadening);
	}

	if (pParallelWidth)
	{
		int index=getTypeOfInfrIndex();
		isNewConstruction=(index==0);

		if(!isNewConstruction)
		{
			pParallelWidth->SetDouble(0.0);
		}
		pParallelWidth->SetReadOnly(!isNewConstruction);
	}

	updateCurrentWidth();

	return isBroadening;
}

/************************************************************************/
/* Gets the active infringem index as a integer.                        */
/*                                                                      */
/* @return -1 if no one is active otherwize the positive index number.  */
/************************************************************************/
int CMDI1950ForrestNormFrame::getTypeOfInfrIndex()
{
	int nTypeOfInfrIdx=-1;
	vecUCFunctions funcs;
	getForrestNormTypeOfInfring(funcs);
	if (funcs.size() > 0)
	{
		for (UINT i = 0;i < funcs.size();i++)
		{
			if (m_sBindToTypeOfInfr.CompareNoCase(funcs[i].getName()) == 0)
			{
				nTypeOfInfrIdx = i;
				break;
			}
		}
	}
	return nTypeOfInfrIdx;
}

BOOL CMDI1950ForrestNormFrame::saveObject_high_cost(CXTPPropertyGridItem *pItem)
{
	CTransaction_template rec = CTransaction_template();
	int nTypeOfHCostIdx = -1;
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;

	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pItem != NULL && pFrame != NULL)
	{

		CXTPPropertyGridItemConstraints *pItemConstraints = pItem->GetConstraints();
		if (pItemConstraints != NULL)
		{
			CXTPPropertyGridItemConstraint *pConstraint = pItemConstraints->GetConstraintAt(pItemConstraints->GetCurrent());
			if (pConstraint != NULL)
				nTypeOfHCostIdx = (int)pConstraint->m_dwData;
		}

		// Try to get the ObjectView, and collect data; 090403 p�d
		pObjView = pFrame->getObjectFormView();

		if (pObjView != NULL)
		{
			// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 090403 p�d
			if (pFrame->getAction() == UPD_ITEM)
			{
				// Reload Cost tables; 090602 p�d			
				getHighCostTemplateFromDB();

				nObjID = pObjView->getActiveObjectID_pk();
				if (nTypeOfHCostIdx >= 0 && nTypeOfHCostIdx < vecTemplate_high_cost.size())
					rec = vecTemplate_high_cost[nTypeOfHCostIdx];

				bIsObjUpdates = m_pDB->updObject_hcost(nObjID,rec.getTemplateName(),rec.getTypeOf(),rec.getTemplateFile());
				// On change, repopulate object; 090403 p�d
				//pObjView->doPopulateData(pObjView->getObjIndex());
				pObjView->doPopulateLastObject(nObjID);
			}	// if (pFrame->getAction() == UPD_ITEM)
			// Need to release ref. pointers; 090403 p�d
			pObjView = NULL;
		}	// if (pObjView != NULL)
		
		// Added 090602 p�d
		CPropertiesFormView *pProp = pFrame->getPropertiesFormView();
		if (pProp != NULL)
		{
			// Need to release ref. pointers; 090602 p�d
			pProp = NULL;
		}

		// Need to release ref. pointers; 090403 p�d
		pFrame = NULL;

		// If we have an update on object, i think we should
		// also recaluclate; 080918 p�d
		//if (bIsObjUpdates)
		// OnCalculateTBtn();

	}	// if (pItem != NULL)

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_norm(CXTPPropertyGridItem *pItem)
{
	CString S;
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
//			S.Format("CMDI1950ForrestNormFrame::saveObject_norm\nm_pDB->updObject_norm  m_nNormTypeOf %d",m_nNormTypeOf);
//			AfxMessageBox(S);
			bIsObjUpdates = m_pDB->updObject_norm(nObjID,m_sBindToForrestNorm,m_nNormTypeOf);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
	//if (bIsObjUpdates)
	//		OnCalculateTBtn();

	return TRUE;
}


BOOL CMDI1950ForrestNormFrame::saveObject_p30(CXTPPropertyGridItem *pItem,bool recalc)
{
	CString S;
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CTransaction_template tmpl = CTransaction_template();
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			if (m_nP30SelIndex >= 0 && m_nP30SelIndex < m_vecObjectP30Template.size())
				tmpl = m_vecObjectP30Template[m_nP30SelIndex];
			// Only update the active Object; 080408 p�d
			bIsObjUpdates = m_pDB->updObject_p30(nObjID,tmpl.getTemplateName(),tmpl.getTypeOf(),tmpl.getTemplateFile());
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
	if (bIsObjUpdates && recalc)
		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_growth_area(CXTPPropertyGridItem *,bool recalc)
{
	CString sGA,S;
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			sGA.Format(_T("%d"),m_nGrowthAreaSelIndex);
			// Only update the active Object; 080408 p�d
//			bIsObjUpdates = m_pDB->updObject_growth_area(nObjID,sGA);
			bIsObjUpdates = m_pDB->updObject_growth_area(nObjID,m_sBindToGrowthAreaSelected);
			// Reload active object into m_recActiveObject; 100915 p�d
			m_pDB->getObject(nObjID,m_recActiveObject);
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
	if (bIsObjUpdates && recalc)
		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_latitude_altitude(CXTPPropertyGridItem *pItem)
{
	CString sGA,S;
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			bIsObjUpdates = m_pDB->updObject_latitude_altitude(nObjID,m_nBindToLatitude,m_nBindToAltitude);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
//	if (bIsObjUpdates)
//		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_length(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL && m_pDB != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_length(nObjID,m_fBindToObjLength);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
//	if (bIsObjUpdates)
//		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_curr_width(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL && m_pDB != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_curr_width(nObjID,m_fBindToCurrentWidth1);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
//	if (bIsObjUpdates)
//		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_added_widths(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL && m_pDB != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_added_widths(nObjID,m_fBindToAddedWidth1,m_fBindToAddedWidth2);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
//	if (bIsObjUpdates)
//		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_parallel_width(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL && m_pDB != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_parallel_width(nObjID,m_fBindToParallelWidth);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
//	if (bIsObjUpdates)
//		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_vfall_break_factor(CXTPPropertyGridItem *pItem)
{
	CString sGA,S;
	BOOL bIsObjUpdates = FALSE;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			bIsObjUpdates = m_pDB->updObject_vfall_break_factor(nObjID,m_fBindToVFallBreak,m_fBindToVFallFactor);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;

	// If we have an update on object, i think we should
	// also recaluclate; 080918 p�d
//	if (bIsObjUpdates)
//		OnCalculateTBtn();

	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_dcls(CXTPPropertyGridItem *pItem)
{
	CString S;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_dcls(nObjID,m_fBindToDiameterClass);

			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_recalc_by(CXTPPropertyGridItem *pItem)
{
	CString S;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_recalc_by(nObjID,m_fBindToInfrPercent);

			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_include_exclude(CXTPPropertyGridItem *pItem)
{
	CString S;
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		//Remove this check and always save the settings, otherwise the settings will not be saved unless the object has been saved first, concering new objects, 20121130 J� #3499
		//if ( pFrame->getAction() == UPD_ITEM)
		//{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_include_exclude(nObjID,(m_bBindToIsVAT ? 1 : 0),
																							(m_bBindToIsVoluntaryDeal ? 1 : 0),
																							(m_bBindToIsHigherCosts ? 1 : 0),
																							(m_bBindToIsOtherCompens ? 1 : 0),
																							(m_bBindToIsGrot ? 1 : 0));

			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());

		//}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_price_deviation(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_price_div(nObjID,m_fBindToPriceDev1,m_fBindToPriceDev2);
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)
	
	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_type_of_net(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	int nTypeOf = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	// Try to find index of selected "Typ av intr�ng"; 080519 p�d
	for (int i = 0;i < m_arrTypeOfNet.GetCount();i++)
	{
		if (m_sBindToTypeOfNet.CompareNoCase(m_arrTypeOfNet.GetAt(i)) == 0)
		{
			nTypeOf = i;
			break;
		}	// if (m_sBindToTypeOfNet.CompareNoCase(m_arrTypeOfNet.GetAt(i)) == 0)
	}	// for (int i = 0;i < m_arrTypeOfNet.GetCount();i++)
	if (pObjView != NULL && nTypeOf > -1)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		//if (pFrame->getAction() == UPD_ITEM)
		//{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_type_of_net(nObjID,nTypeOf);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		//}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)

	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::saveObject_pricebase_max_percent(CXTPPropertyGridItem *pItem)
{
	int nObjID = -1;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
		if (pFrame->getAction() == UPD_ITEM)
		{
			nObjID = pObjView->getActiveObjectID_pk();
			// Only update the active Object; 080408 p�d
			m_pDB->updObject_price_max_percent(nObjID,m_fBindToPriceBase,m_fBindToMaxPercent,m_fBindToCurrPercent,m_fBindToVAT,m_fBindToPercentOfPriceBase);
			// On change, repopulate object; 080519 p�d
			//pObjView->doPopulateData(pObjView->getObjIndex());
		}	// if (pFrame->getAction() == UPD_ITEM)
		// Need to release ref. pointers; 080520 p�d
		pObjView = NULL;
	}	// if (pObjView != NULL)

	// Need to release ref. pointers; 080520 p�d
	pFrame = NULL;
	return TRUE;
}

// Method for updating information on Stand for Property in Object.
// Update: "Prislista","Kostnadsmall","Diameterklass" and "S�derbergs"; 080521 p�d
BOOL CMDI1950ForrestNormFrame::updateTraktMiscDataPerProperty(vecTransaction_elv_properties& vec)
{
	CString sMsg;
	int nCheckReturn = 1;
	vecTransaction_elv_properties vecELVProperties;
	CTransaction_elv_properties recELVProperty;
	vecTransaction_elv_cruise vecELVCruises;
	CTransaction_elv_cruise recELVCruise;
	CTransaction_elv_object recObject;
	CTransaction_trakt recTrakt;
	CTransaction_elv_object *pObj = getActiveObject();

	doEvents();
	if (m_pDB != NULL && pObj != NULL)
	{
		// Get active object data; 080521 p�d
		m_pDB->getObject(pObj->getObjID_pk(),recObject);

		// Get properties for this Object; 080521 p�d
		// Get ALL properties. Commented out; 091022 p�d
		//m_pDB->getProperties_by_status(pObj->getObjID_pk(),vecELVProperties);
		
		// vec holds selected properties only; 091022 p�d
		vecELVProperties = vec;
	
		// Make sure there's data to work with; 080521 p�d
		if (vecELVProperties.size() > 0)
		{
			m_wndProgressDlg.setProgressRange_props(vecELVProperties.size());

			for (UINT i = 0;i < vecELVProperties.size();i++)
			{
				recELVProperty = vecELVProperties[i];
				
				m_wndProgressDlg.setPropertyName(recELVProperty.getPropName());
			
				doEvents();

				// Open Cruise table for Object and Property; 080520 p�d
				m_pDB->getObjectCruises(recELVProperty.getPropID_pk(),
																recELVProperty.getPropObjectID_pk(),
																vecELVCruises);

				// Check if there's any cruises to work with; 080521 p�d
				if (vecELVCruises.size() > 0)
				{
					for (UINT ii = 0;ii < vecELVCruises.size();ii++)
					{
						recELVCruise = vecELVCruises[ii];					

						if (m_bDoCheckPricelist)
						{
							// Check if the new pricelist holds, at least, the same species
							// as the old pricelist; 080522 p�d
							nCheckReturn = checkPricelistForSpecies(recObject,recELVCruise.getECruID_pk() /* Eq. traktid */);
							if (nCheckReturn ==  -1 /* Don't use this pricelist */)
							{
								vecELVProperties.clear();
								vecELVCruises.clear();
								pObj = NULL;
								m_bDoCheckPricelist = FALSE;
								return FALSE;
							}	// if (!checkPricelistForSpecies(recObject,recELVCruise.getECruID_pk() /* Eq. traktid */))
							else if (nCheckReturn ==  0 /* Use this pricelist */)
							{
								m_bDoCheckPricelist = FALSE;	// Don't do anymore checkin' ; 080522 p�d
							}	// if (nCheckReturn ==  /* Use this pricelist */)
						}	// if (m_bDoCheckPricelist)

						// Clean up; try to mimic what happens in "Update pricelist", in UMEstimate: 080521 p�d
						cleanUpTraktOnUpdatePerProperty(recELVProperty,recObject,recELVCruise.getECruID_pk() /* Eq. traktid */);


						//AfxMessageBox(recObject.getObjNameOfCosts() + "\n" + recObject.getObjCostsXML());

						// Update: "Prislista","Kostnadsmall" and diameterclass; 080521 p�d
						m_pDB->updTraktMiscData(CTransaction_trakt_misc_data(recELVCruise.getECruID_pk(),
																																 recObject.getObjNameOfPricelist(),
																																 recObject.getObjTypeOfPricelist(),
																																 recObject.getObjPricelistXML(),
																																 recObject.getObjNameOfCosts(),
																																 recObject.getObjTypeOfCosts(),
																																 recObject.getObjCostsXML(),
																																 recObject.getObjDCLS(),
																																 _T("")));
						// Update Trakt on "S�derbergs"; 080521 p�d
						m_pDB->updTraktData_soderbergs(recELVCruise.getECruID_pk(),	
																					 m_bBindToIsAtCoast,
																					 m_bBindToIsSouthEast,
																					 m_bBindToIsRegion5,
																					 m_bBindToIsPartOfPlot);

					}	// for (UINT ii = 0;ii < vecELVCruises.size();ii++)
				}	// if (vecELVCruises.size() > 0)
				m_wndProgressDlg.setProgressPos_props(i+1);
			}	// for (UINT i = 0;i < vecELVProperties.size();i++)
		}	// if (vecELVProperties.size() > 0)
	}	// if (m_pDB != NULL && pObj != NULL)

	vecELVProperties.clear();
	vecELVCruises.clear();
	pObj = NULL;

	return TRUE;
}

// We'll try to mimic what we're doin' in UMEstimate on "Update of Pricelist"; 080521 p�d
void CMDI1950ForrestNormFrame::cleanUpTraktOnUpdatePerProperty(CTransaction_elv_properties& prop,CTransaction_elv_object &rec_obj,int trakt_id)
{
	CString sSQLRemoveSpecies;
	CTransaction_species recSpc;
	vecTransactionSpecies vecSpc;

	vecTransactionAssort vecAssort;
	CTransaction_assort recAssort;

	CTransaction_trakt_data recTraktData;
	vecTransactionTraktData vecTraktData;
	BOOL bSpcFound = TRUE;

	//---------------------------------------------------------------------------------------
	// Try to setup an SQL-question, that contains specie in Pricelist.
	// use this information to delete specie in table "esti_trakt_data_table" that
	// doesn't match these specieid(s); 080521 p�d
	xmllitePricelistParser *prlPars = new xmllitePricelistParser();
	if (m_pDB != NULL && prlPars != NULL)
	{

		m_pDB->getTraktData(vecTraktData,trakt_id,STMP_LEN_WITHDRAW);
		if (prlPars->loadStream(rec_obj.getObjPricelistXML()))
		{
			prlPars->getSpeciesInPricelistFile(vecSpc);
			prlPars->getAssortmentPerSpecie(vecAssort);

			if (vecSpc.size() > 0 && vecTraktData.size() > 0)
			{
				// As well as we remove species we need to add species; 080521 p�d
				for (UINT i1 = 0;i1 < vecSpc.size();i1++)	// Species in selectedPricelist; 080521 p�d
				{
					bSpcFound = FALSE;
					recSpc = vecSpc[i1];
					for (UINT i2 = 0;i2 < vecTraktData.size();i2++)
					{
						recTraktData = vecTraktData[i2];
						// Check if speice in pricelist also is in TraktData.
						// If not, add to traktdata; 080521 p�d
						if (recTraktData.getSpecieID() == recSpc.getSpcID())
						{
							bSpcFound = TRUE;
							break;
						}	// if (recTraktData.getSpecieID() == recSpc.getSpcID())

						if (!bSpcFound)
						{

							m_pDB->addTraktData(CTransaction_trakt_data(recSpc.getSpcID(),
																													trakt_id,
																													STMP_LEN_WITHDRAW,
																													recSpc.getSpcID(),
																													recSpc.getSpcName(),
																													0.0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,_T("")));
							m_pDB->addTraktData(CTransaction_trakt_data(recSpc.getSpcID(),
																													trakt_id,
																													STMP_LEN_TO_BE_LEFT,
																													recSpc.getSpcID(),
																													recSpc.getSpcName(),
																													0.0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,_T("")));
							m_pDB->addTraktData(CTransaction_trakt_data(recSpc.getSpcID(),
																													trakt_id,
																													STMP_LEN_WITHDRAW_ROAD,
																													recSpc.getSpcID(),
																													recSpc.getSpcName(),
																													0.0,0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,_T("")));
																												
						}	// if (!bSpcFound)					
					}	// for (UINT i2 = 0;i2 < vecTraktData.size();i2++)
				}	// for (UINT i1 = 0;i1 < vecSpc.size();i1++)
				// Now, we need to reload the TraktData; 080521 p�d
				m_pDB->getTraktData(vecTraktData,trakt_id,STMP_LEN_WITHDRAW);

				//------------------------------------------------------------------------------------------
				sSQLRemoveSpecies = formatData(_T("where tdata_trakt_id=%d and ("),trakt_id);
				for (UINT i = 0;i < vecSpc.size();i++)
				{
					if (vecSpc.size() == 1)
					{
						//Added paranthesis to the end of sql string tdata_spc_id<>%d), 20120124 J� Bug #2781
						sSQLRemoveSpecies += formatData(_T("tdata_spc_id<>%d)"),vecSpc[i].getSpcID());
					}
					else if (vecSpc.size() > 1 && i < vecSpc.size() - 1)
					{
						sSQLRemoveSpecies += formatData(_T("tdata_spc_id<>%d and "),vecSpc[i].getSpcID());
					}
					else 
					{
						sSQLRemoveSpecies += formatData(_T("tdata_spc_id<>%d)"),vecSpc[i].getSpcID());
					}
				}	// for (UINT i = 0;i < vecSpc.size();i++)
				// This method'll remove all species from "esti_trakt_data_table" and it's
				// dependencies "esti_trakt_set_spc_table" and "esti_trakt_spc_assort_table"; 080521 p�d
				m_pDB->removeTraktDataSpecies(trakt_id,sSQLRemoveSpecies);		
				//------------------------------------------------------------------------------------------

				//------------------------------------------------------------------------------------------
				// Remove from "esti_trakt_spc_assort_table"; 080521 p�d
				for (UINT i1 = 0;i1 < vecTraktData.size();i1++)
				{
					recTraktData = vecTraktData[i1];

					m_pDB->removeTraktAss(trakt_id,recTraktData.getTDataID());

				}	// for (UINT i1 = 0;i1 < vecTraktData.size();i1++)
				//------------------------------------------------------------------------------------------

				//------------------------------------------------------------------------------------------
				// Setup assortments form selected pricelist to "esti_trakt_spc_assort_table"; 080521 p�d
				int nCounter = 1;
				for (UINT i1 = 0;i1 < vecTraktData.size();i1++)
				{
					recTraktData = vecTraktData[i1];
					nCounter = 1;
					for (UINT i2 = 0;i2 < vecAssort.size();i2++)
					{
						recAssort = vecAssort[i2];
						if (recAssort.getSpcID() == recTraktData.getSpecieID())
						{
							CTransaction_trakt_ass rec = CTransaction_trakt_ass(nCounter,
																																	trakt_id,
																																	recTraktData.getSpecieID(),
																																	STMP_LEN_WITHDRAW,
																																	recAssort.getAssortName(),
																																	recAssort.getPriceM3fub(),
																																	recAssort.getPriceM3to(),
																																	0.0,0.0,0.0,0.0,_T(""),0.0);
							m_pDB->addTraktAss(rec);
							nCounter++;
						}
					}

				}	// for (UINT i1 = 0;i1 < vecTraktData.size();i1++)
				//------------------------------------------------------------------------------------------


			}	// if (vecSpc.size() > 0)
		}	// if (prlPars->loadStream(rec_obj.getObjPricelistXML()))
		delete prlPars;
	}	// if (m_pDB != NULL && prlPars != NULL)

	//---------------------------------------------------------------------------------------
	vecSpc.clear();
	vecTraktData.clear();
	vecAssort.clear();
}

int CMDI1950ForrestNormFrame::checkPricelistForSpecies(CTransaction_elv_object &rec_obj,int trakt_id)
{
	int nReturn = 1;	// Alles ok; 080522 p�d
	CTransaction_species recSpc_new;
	vecTransactionSpecies vecSpc_new;
	CTransaction_species recSpc_old;
	vecTransactionSpecies vecSpc_old;
	CTransaction_trakt_misc_data recTraktMiscData;
	BOOL bSpcFound = TRUE;

	xmllitePricelistParser *prlPars_new = new xmllitePricelistParser();
	xmllitePricelistParser *prlPars_old = new xmllitePricelistParser();

	if (prlPars_new != NULL && prlPars_old != NULL)
	{

		m_pDB->getTraktMiscData(trakt_id,recTraktMiscData);

		if (prlPars_old->loadStream(recTraktMiscData.getXMLPricelist()))
		{
			prlPars_old->getSpeciesInPricelistFile(vecSpc_old);
		}
		
		if (prlPars_new->loadStream(rec_obj.getObjPricelistXML()))
		{
			prlPars_new->getSpeciesInPricelistFile(vecSpc_new);
			// Start by checkin' the Pricelist species compared to species in old pricelist; 080522 p�d
			if (vecSpc_new.size() > 0 && vecSpc_old.size() > 0)
			{
				// Compare new pricelist to old pricelist; 080522 p�d
				for (UINT i1 = 0;i1 < vecSpc_old.size();i1++)
				{
					bSpcFound = FALSE;
					recSpc_old = vecSpc_old[i1];
					for (UINT i2 = 0;i2 < vecSpc_new.size();i2++)
					{
						recSpc_new = vecSpc_new[i2];

						if (recSpc_new.getSpcID() == recSpc_old.getSpcID())
						{
							bSpcFound = TRUE;
							break;
						}	// if (recSpc.getSpcID() == recSpc_old.getSpcID())
					}	// for (UINT i2 = 0;i2 < vecSpc.size();i2++)

					// Get out an tell user; 080522 p�d
					if (!bSpcFound)	break;
				}	// for (UINT i1 = 0;i1 < vecSpc_old.size();i1++)
			}

			if (!bSpcFound)
			{
				if (::MessageBox(this->GetSafeHwnd(),(m_sPricelistCompareMsg),(m_sMsgCap),MB_ICONASTERISK | MB_DEFBUTTON2 | MB_YESNO) == IDNO)
					nReturn = -1;
				else
					nReturn = 0;
			}
		}
		vecSpc_new.clear();
		vecSpc_old.clear();;

		delete prlPars_old;
		delete prlPars_new;
	}

	return nReturn;
}

// Check that status set on Active Property, allow user to do
// whatever he's about to do; 080623 p�d
BOOL CMDI1950ForrestNormFrame::doesStatusAllowChange(BOOL only_msg)
{
	if (only_msg)
	{
		CCheckBoxMessageDlg *pDlg = new CCheckBoxMessageDlg();
		if (pDlg != NULL)
		{
			// Tell user that there's restriction, depending on
			// Status set for Property; 080623 p�d
			CString sMsg;
			sMsg.Format(_T("%s\n\n%s\n\n"),m_sRestrictionOnReCalc,m_sDoRecalculation);
			pDlg->addMsgText(sMsg);
			pDlg->addCapText(m_sMsgCap);
			pDlg->addCBoxTextAndStatus(m_sDoCompleteReCalc,FALSE);
			pDlg->addOkCancelText(m_sOK,m_sCancelDlg);
			pDlg->forceCompleteReCalc(m_dForceCompleteReCalc);
			pDlg->needToRecalcAll(m_dNeedToRecalcAll);
//			if (::MessageBox(this->GetSafeHwnd(),sMsg,(m_sMsgCap),MB_ICONASTERISK | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
			if (pDlg->DoModal() == IDOK)
			{
				m_bDoCompleteReCalc = pDlg->getDoCompleteReCalc();
				UpdateWindow();
				return TRUE;
			}
			else
			{
				m_bDoCompleteReCalc = TRUE;			
				return FALSE;
			}
		}
	}
	else
	{
		CTransaction_elv_properties *pActiveProp = getActiveProperty();
		if (pActiveProp != NULL)
		{
			if (canWeCalculateThisProp_cached(pActiveProp->getPropStatus())) return TRUE;
			else
			{
				// Tell user that there's restriction, depending on
				// Status set for Property; 080623 p�d
				::MessageBox(this->GetSafeHwnd(),(m_sRestrictionOnReCalc),(m_sMsgCap),MB_ICONASTERISK |  MB_OK);
				UpdateWindow();
				return FALSE;
			}	// else
			pActiveProp = NULL;
		}
	}
	return TRUE;
}

BOOL CMDI1950ForrestNormFrame::isCalculationdataOK(void)
{
	CObjectFormView *pObj = NULL;

	if (m_nDoCacluateAction == 0 && m_nProdIDRecalc == -1)
	{
		// Save Cruise data before running UMInData; 091202 p�d
		CMDI1950ForrestNormFormView *pView = (CMDI1950ForrestNormFormView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{	
			if ((pObj = pView->getObjectFormView()) != NULL)	pObj->saveObject(1);
		}	// if (pView != NULL)
	}

	int nGrowthArea = _tstoi(m_sBindToGrowthAreaSelected);
	if (nGrowthArea < 1 || nGrowthArea > 6)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgGrowthAreaError,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}

	return TRUE;
}

void CMDI1950ForrestNormFrame::changeInventoryDirectory(void)
{
		// We must to create the Inventory directory; 080204 p�d
	CString sObjectDirectory;
	CString sPresentPath = getRegisterObjectInventoryDir();	// Added 110216 p�d
	sObjectDirectory.Format(_T("%s:\n%s"),
		m_sActiveDataDirectory,
		sPresentPath);

	// Buffer for the returned path
	TCHAR szPath[MAX_PATH];

	// Display the dalog and check the return code
	if( XBrowseForFolder(GetSafeHwnd(), sPresentPath, szPath, sizeof(szPath) / sizeof(TCHAR)) )
	{
		// Set Object Inventory directory to Register; 080205 p�d
		setRegisterObjectInventoryDir(szPath);
	}
}

// Select a contractor. Can be based on data entered in form; 080327 p�d
void CMDI1950ForrestNormFrame::OnContractorTBtn(void)
{
	CObjectFormView *pObj = NULL;
	// Create SQL question, based on data entered (or not), by user
	// in fields for contractor data; 080327 p�d
	CMDI1950ForrestNormFormView *pView = (CMDI1950ForrestNormFormView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pObj = pView->getObjectFormView();
	}	// if (pView != NULL)

	CSelectContractorDlg *selectDlg = new CSelectContractorDlg();
	if (selectDlg != NULL)
	{
		selectDlg->setDBConnection(m_pDB);
		selectDlg->setDialogTitle(m_sSearchDlgTitle);
		if (selectDlg->DoModal() == IDOK)
		{
			// Check if there's a contractor selected; 080327 p�d
			if (selectDlg->getIsSelected())
			{
				if (pObj != NULL)
				{
					pObj->setContractorID_pk(selectDlg->getID_pk());
					pObj->setPersOrgNum(selectDlg->getPersOrgNum());
					pObj->setContactPers(selectDlg->getContactPers());
					pObj->setCompany(selectDlg->getCompany());
//					pObj->setAddress(cleanCRLF(selectDlg->getAddress(),L", "));
//					pObj->setPostnum(cleanCRLF(selectDlg->getPostNum(),L" "));
//					pObj->setPostAddress(cleanCRLF(selectDlg->getPostAddress(),L" "));
					pObj->setPhoneWork(selectDlg->getPhoneWork());
					pObj->setPhoneHome(selectDlg->getPhoneHome());
					pObj->setMobile(selectDlg->getMobile());
					pObj->saveObject(0);
				}	// if (pObj != NULL)
			}	// if (selectDlg->getIsContractorSelected())
		}	// if (selectDlg->DoModal() == IDOK)

		delete selectDlg;
	}

	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pObj = NULL;
}


BOOL CMDI1950ForrestNormFrame::isThereOnlyRandTreesInStand(CTransaction_elv_cruise &rec)
{
	BOOL bReturn = TRUE;
	vecTransactionDCLSTree vecTrees;
	CTransaction_dcls_tree recTree;

	if (m_pDB != NULL)
			m_pDB->getDCLSTrees(rec.getECruID_pk() /* TraktID */,vecTrees);
		
	// If there's no trees at all, we'll also return TRUE, i.e. only randtrees; 090330 p�d
	if (vecTrees.size() > 0)
	{
		for (UINT i = 0;i < vecTrees.size();i++)
		{
			recTree = vecTrees[i];
			// Check for "Tr�d i gatan"; 090330 p�d
			if (recTree.getNumOf() > 0)
			{
				bReturn = FALSE;
				break;
			}	// if (recCruise.getECruNumOfTrees() > 0)
		}	// for (UINT i = 0;i < vecELVObjectCruise.size();i++)
		vecTrees.clear();
	}

	return bReturn;

}

// Do a recalculation of properties in this Object; 080121 p�d
void CMDI1950ForrestNormFrame::OnMenuCalculateTBtn()
{
	OnCalculateTBtn();
}

BOOL CMDI1950ForrestNormFrame::OnCalculateTBtn()
{
	// Try to run the doUMForrestNorm
	int nReturnValue = 1;
	int nGrowthArea = 0;

	BOOL bAnythingToCalculate = FALSE;	// = True if status is "ok to calculate"; 100426 p�d

	CString sMsg,S;
	CString sErrorLog;
	CStringArray arrLog;
	CStringArray arrLogBark;
	CStringArray arrLogHgt;
	CStringArray arrErrorLog;
	CStringArray arrTraktErrorLog;
	CStringArray arrTraktSoderbergErrorLog;
	CStringArray arrTraktH25ErrorLog;
	vecTransaction_elv_properties vecELVProperties;
	CTransaction_elv_properties recELVProperty;
	vecTransaction_elv_cruise vecELVCruises;
	CTransaction_elv_cruise recELVCruise;
	CTransaction_trakt recTrakt;
	CTransaction_elv_object pObj = m_recActiveObject; //getActiveObject();

	CTransaction_elv_properties_logbook recLogBook;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;

	doEvents();

	if (!isCalculationdataOK()) return FALSE;

	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		pPropView = (CPropertiesFormView*)pView->getPropertiesFormView();
	}
	if (m_pDB != NULL) // && pObj != NULL)
	{

		// Refresh cached property status table; Optimization by Peter
		canWeCalculateThisProp_cached(0, true);

		// Get a list of Cruises not calculted, 
		// i.e. number of trees = 0; 090525 p�d
		getObjectCruisesNotCalculated();

		m_pDB->getProperties_by_status(pObj.getObjID_pk(),vecELVProperties,m_nProdIDRecalc);
		// We'll do a quick check to see if there's anything
		// to calculate. I.e. Status allowing calculation on at least one Property; 100426 p�d

		if (vecELVProperties.size() > 0)
		{
			for (UINT i = 0;i < vecELVProperties.size();i++)
			{
				if (canWeCalculateThisProp_cached(vecELVProperties[i].getPropStatus()))
				{
					bAnythingToCalculate = TRUE;
					break;
				}
			}
		}

		// Check if there's any data do handle. If not, just leave; 090205 p�d
		if (vecELVProperties.size() == 0) 
		{
			::MessageBox(this->GetSafeHwnd(),m_sMsgNoPropertiesToCalc2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		if (!bAnythingToCalculate) 
		{
			::MessageBox(this->GetSafeHwnd(),m_sMsgNoPropertiesToCalc1,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		// If don't recalculate, return; 090420 p�d
		if (!doesStatusAllowChange(TRUE /* Only show message */)) 
		{
			m_wndProgressDlg.show(SW_HIDE);
			return FALSE;
		}
		// OBS COLLECT INFO. ON TRAKT(S) FROM elv_cruise_table
		//********************************************************
		// This method'll update "esti_trakt_table : S�derbergs"
		// and also "esti_trakt_misc_data_table : Pricelista,Kostnadsmall,Diam.klass"; 080521 p�d

		// Set wait cursor
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));

		// Setup statusbar to tell user, calculating; 081016 p�d		
		setStatusBarText(1,m_sStatusWorking);

		m_bDoCheckPricelist = TRUE;	// Ready do do some checkin' ; 080522 p�d

		// Commented out 2009-10-16 p�d
		// This method'll update ALL trakt adding infrmation on
		// Pricelist etc. to each Trakt. This can cause error, e.g. if
		// pricelist in Object isn't OK and pricelist in Stand-template,
		// used to create Stand(s) is updated; 091016 p�d
		//		if (!updateTraktMiscDataPerProperty())
		//				return;
		//********************************************************

		if (m_wndProgressDlg.GetSafeHwnd()) 
		{

			if (!m_wndProgressDlg.isVisible())
			{
				m_wndProgressDlg.show(SW_SHOW);
				m_wndProgressDlg.setCaption(m_sMsgObject + L" : " + pObj.getObjectName());
				m_wndProgressDlg.setLbl2(m_sMsgProperty);
				m_wndProgressDlg.setLbl3(m_sMsgStand);
			}

			m_wndProgressDlg.setActiveAction(m_sMsgReCalcUpdCruise);

			// Check if m_bDoCompleteReCalc = TRUE
			// If not hide StandInformation; 100203 p�d
			if (!m_bDoCompleteReCalc)
				m_wndProgressDlg.hideStandInformation();
			else
				m_wndProgressDlg.showStandInformation();

			m_wndProgressDlg.UpdateWindow();
		}
		// Make sure there's data to work with; 080505 p�d
		if (vecELVProperties.size() > 0)
		{
			m_wndProgressDlg.setProgressRange_props(vecELVProperties.size());
			for (UINT i = 0;i < vecELVProperties.size();i++)
			{
				recELVProperty = vecELVProperties[i];

				//m_pDB->moveManualCruiseData(recELVProperty.getPropObjectID_pk(),recELVProperty.getPropID_pk()); NOT USED 100504 p�d

				m_wndProgressDlg.setPropertyName(recELVProperty.getPropName());
				m_wndProgressDlg.setProgressPos_props(i+1);
				//if (recELVProperty.getPropStatus() <= STATUS_ADVICED)
				if (canWeCalculateThisProp_cached(recELVProperty.getPropStatus()))
				{
					//Reset the property here before calculations 20120123 Bug #2769
					//Does not reset other costs or status values
					m_pDB->resetPropertyBeforeCalc(recELVProperty.getPropID_pk(),recELVProperty.getPropObjectID_pk());

					// Open Cruise table for Object and Property; 080520 p�d
					m_pDB->getObjectCruises(recELVProperty.getPropID_pk(),
						recELVProperty.getPropObjectID_pk(),
						vecELVCruises);
					// Check if there's any cruises to work with; 080520 p�d
					if (vecELVCruises.size() > 0)
					{
						//------------------------------------------------------------------------
						// Add information to logbook that property been recalculated; 100812 p�d
						recLogBook = CTransaction_elv_properties_logbook(recELVProperty.getPropID_pk(),
							recELVProperty.getPropObjectID_pk(),
							getDateTimeEx2(),
							getUserName().MakeUpper(),
							m_sLogRecalcMsg,
							getDateTime(),
							-1);

						if (m_pDB != NULL && logThis(0)) 
							m_pDB->addPropertyLogBook(recLogBook);

						//------------------------------------------------------------------------

						m_wndProgressDlg.setProgressRange_stands(vecELVCruises.size());
						for (UINT ii = 0;ii < vecELVCruises.size();ii++)
						{
							recELVCruise = vecELVCruises[ii];		
							// Added 2009-05-25 P�D
							// Chack from list of Cruises, in "elv_cruise_table", havin number of trees = 0; 090525 p�d
							if (calculateThisCruise(recELVCruise))
							{								

								// Get Trakt from Property in Object; 080520 p�d
								// "elv_trakt_table"; 090525 p�d
								m_pDB->getTrakt(recELVCruise.getECruID_pk(),recTrakt);

								// Show progress in bar; 100203 p�d
								m_wndProgressDlg.setStandName(recTrakt.getTraktName());
								m_wndProgressDlg.setProgressPos_stands(ii+1);

								// Calculate "Rotpost"; 080505 p�d
								// Funtion declared in "calculation_methods.h" in HMSFuncLib; 080505 p�d
								arrLog.RemoveAll();

								//#4601 Kontroll av s�derb bark o vol 20151021 J�
								arrLogBark.RemoveAll();
								arrLogHgt.RemoveAll();
								nReturnValue=doSoderbergHgtandVolCheckonTrakt(m_dbConnectionData,recTrakt,arrLogBark,arrLogHgt);								
								if(nReturnValue!=1)
								{
									arrTraktSoderbergErrorLog.Add(recELVProperty.getPropName());
									arrTraktSoderbergErrorLog.Add(formatData(_T("%s - %s"),recTrakt.getTraktNum(),recTrakt.getTraktName()));
									if (arrLogBark.GetCount() > 0) //Barkfunktion anv�nds med best�nd�lder >500
									{
										arrTraktSoderbergErrorLog.Add(+_T("\r\n") + m_sMsgBarkFuncErrSoderbergs + _T("\r\n"));
										arrTraktSoderbergErrorLog.Append(arrLogBark);
									}
									if (arrLogHgt.GetCount() > 0) //H�jd anv�nds med best�nd�lder >500
									{
										arrTraktSoderbergErrorLog.Add(+_T("\r\n") + m_sMsgHgtFuncErrSoderbergs + _T("\r\n"));									
										arrTraktSoderbergErrorLog.Append(arrLogHgt);
									}
									arrTraktSoderbergErrorLog.Add(_T("\r\n------------------------------------------------------------------------\r\n"));
								}

								// #4618: Kontrollera om h�jd �r utanf�r H25:s omr�de
								arrLogHgt.RemoveAll();
								nReturnValue = CheckH25MinMax(m_dbConnectionData, recTrakt, arrLogHgt);
								if (nReturnValue == FALSE)
								{
									// visa varning om att det finns provtr�d utanf�r H25s omr�de
									arrTraktH25ErrorLog.Add(recELVProperty.getPropName());
									arrTraktH25ErrorLog.Add(_T("\r\n"));
									arrTraktH25ErrorLog.Add(formatData(_T("%s - %s\r\n"), recTrakt.getTraktNum(), recTrakt.getTraktName()));

									for(int nLoop=0; nLoop<arrLogHgt.GetSize(); nLoop++)
									{
										arrTraktH25ErrorLog.Add(arrLogHgt.GetAt(nLoop));
										arrTraktH25ErrorLog.Add(_T("\r\n"));
									}
									arrTraktH25ErrorLog.Add(_T("------------------------------------------------------------------------\r\n"));
								}



								nReturnValue = doCruisingCalculations(1,FALSE,arrLog,m_bConnected,m_dbConnectionData,recTrakt,true,pObj.getObjID_pk());
								// Do refresh, kick the message-loop in the ass; 100203 p�d
								doEvents();
								// If return = -2, no data for any specie e.g. volumefuntion missing etc; 080521 p�d
								if (nReturnValue == -2)
								{
									arrTraktErrorLog.Add(recELVProperty.getPropName());
									arrTraktErrorLog.Add(formatData(_T("%s - %s"),recTrakt.getTraktNum(),recTrakt.getTraktName()));
									arrTraktErrorLog.Add(_T("------------------------------------------------------------------------"));
								}
								else if (arrLog.GetCount() > 0)
								{
									arrErrorLog.Add(recELVProperty.getPropName());
									arrErrorLog.Append(arrLog);
								}
							}	// if (calculateThisCruise(recELVCruise))
							// Manually entered cruises; 090910 p�d
							else
							{
								if (pPropView != NULL)
								{
									pPropView->calculateStormDry(pObj.getObjID_pk(),vecELVCruises);
								}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
							}
						}	// for (UINT ii = 0;ii < vecELVCruises.size();ii++)
					}	// if (vecELVCruises.size() > 0)
					else
					{
						// Fix for bug #2388; 111006 Peter
						m_pDB->resetProperty(recELVProperty.getPropID_pk(),recELVProperty.getPropObjectID_pk());
					}

					m_wndProgressDlg.setProgressPos_stands(0);
				}	// if (recELVProperty.getPropStatus() <= STATUS_ADVICED)
			}	// for (UINT i = 0;i < vecELVProperties.size();i++)
		}	// if (vecELVProperties.size() > 0)

		m_wndProgressDlg.setProgressPos_props(0);
	}	// if (m_pDB != NULL)

	// Make sure StandInformation is displayed; 100203 p�d
	m_wndProgressDlg.showStandInformation();

	m_wndProgressDlg.setActiveAction(m_sMsgReCalcUpdEval);

	// Do a recalcualtion of "V�rdering", also; 080910 p�d
	if (m_nProdIDRecalc == -1)
		OnCreateAllEvaluationsFromCruise();
	else if (m_nProdIDRecalc > -1)
		OnCreateEvaluationFromCruise();

	//if (!m_bCalculateEvalFromCruise)
	OnSaveCalculateVStands();

	// Try to do a match for the recalculated stands in each property
	// for active object; 080520 p�d
	//*	Commented out 2008-10-28 p�d
	// Uncommented 2008-11-03 p�d
	// We need this method, to match (add data), from
	// stands to object/property. But we're doin' it
	// by checkin' if there's any stands already
	// added to Object (i.e. UMInData()), if so
	// only match those stands; 081103 p�d

	m_wndProgressDlg.setActiveAction(m_sMsgFinishOff);
	m_wndProgressDlg.hideStandInformation();
	if (pPropView != NULL)
	{
		if (m_nProdIDRecalc == -1)
			pPropView->matchStandsPerPropertyForObject(m_wndProgressDlg);
		else 	if (m_nProdIDRecalc > -1)
			pPropView->matchStandsForActiveProperty(FALSE /* No dialog */);

		// Do an update of higher costs for ALL properties; 100303 p�d
		pPropView->recalulateAllHigherCosts();
		pPropView->recalulateAllVouluntaryDeals();
	}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	pView = NULL;
	pPropView = NULL;

	//*/

	// Set default cursor (arrow)
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));

	if (arrErrorLog.GetCount() > 0 || nReturnValue == -2 || sErrorLog.GetLength() > 0 || arrTraktH25ErrorLog.GetCount() > 0 || arrTraktSoderbergErrorLog.GetCount() > 0)
	{
		// show a generic header for the error log
		sErrorLog += m_sLogErrorHeader;

		//#4601
		if(arrTraktSoderbergErrorLog.GetCount()>0)
		{
			sErrorLog+=_T("\r\n------------------------------------------------------------------------\r\n");
			sErrorLog+=m_sLogErrorMsgSoderberg;
			sErrorLog += _T("\r\n");
			//sErrorLog+=getDateTime()+_T("\r\n");
			for (int i = 0;i < arrTraktSoderbergErrorLog.GetCount();i++)
				sErrorLog += arrTraktSoderbergErrorLog.GetAt(i);
			//			sErrorLog+=_T("\r\n------------------------------------------------------------------------\r\n");
		}

		// tree heights outside H25 boundaries
		if(arrTraktH25ErrorLog.GetCount()>0)
		{
			CString csMsg;
			csMsg.Format(m_sHeightForH25Unreasonable, (int)H25_MIN_VALUE, (int)H25_MAX_VALUE);
			sErrorLog+=_T("\r\n------------------------------------------------------------------------\r\n");
			sErrorLog += csMsg;
			sErrorLog += m_sHeightForH25Unreasonable2;
			sErrorLog += _T("\r\n\r\n");

			for (int i = 0;i < arrTraktH25ErrorLog.GetCount();i++)
				sErrorLog += arrTraktH25ErrorLog.GetAt(i);
			sErrorLog+=_T("\r\n");
		}

		if(arrErrorLog.GetCount() > 0 || nReturnValue == -2)
		{
			sErrorLog+=_T("\r\n------------------------------------------------------------------------\r\n");
			sErrorLog += m_sLogErrorMsg2;
			for (int i = 0;i < arrTraktErrorLog.GetCount();i++)
				sErrorLog += arrTraktErrorLog.GetAt(i) + _T("\r\n");

			for (int i = 0;i < arrErrorLog.GetCount();i++)
				sErrorLog += arrErrorLog.GetAt(i) + _T("\r\n");
		}


		CLoggMessageDlg *dlg = new CLoggMessageDlg();
		if (dlg != NULL)
		{
			dlg->setMessage(sErrorLog);
			dlg->setObjID(pObj.getObjID_pk());
			dlg->setupLanguage(m_sLogCap,m_sCancel,m_sPrintOut,m_sSaveToFile,
				formatData(_T("%s%s"),pObj.getObjectName(),LAND_VALUE_ROTPOST_LOG_FILE_NAME));
			dlg->DoModal();

			delete dlg;
		}	// if (dlg != NULL)
	}
	//	pObj = NULL;

	if (m_wndProgressDlg.GetSafeHwnd()) m_wndProgressDlg.show(SW_HIDE);

	setStatusBarText(0,m_sStatus);
	setStatusBarText(1,m_sStatusOK);

	m_nProdIDRecalc = -1;

	return TRUE;
}

// Do a import of properties in "fst_property_table"
// Import can be based on search criterias
// such as: "L�n,Kommun,Socken,Namn p� fastighet eller fastighetsnummer"; 080121 p�d
void CMDI1950ForrestNormFrame::OnImportPropertiesTBtn(void)
{
	CString S;
	int nDlgRet = -1;
	CTransaction_property recSelectedProp;
	vecTransactionProperty vecSelectedProperties;
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CSearchPropertiesDlg *searchDlg = new CSearchPropertiesDlg();
	if (searchDlg != NULL)
	{
		searchDlg->setDBConnection(m_pDB);
		if ( (nDlgRet = searchDlg->DoModal()) == IDOK)
		{
			searchDlg->getSelectedProperties(vecSelectedProperties);
		}
		delete searchDlg;
	}
	if (nDlgRet == IDOK)
	{
		if (vecSelectedProperties.size() > 0)
		{
			if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
			{
				if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
				{
					pPropView->setSelectedProperties(vecSelectedProperties);
				}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
			}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
		}	// if (vecSelectedProperties.size() > 0)
	}	// if (searchDlg->DoModal() == IDOK)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

//#5008 PH 20160615
void CMDI1950ForrestNormFrame::OnImportPropertiesFromFileTBtn(void) {
	CString	sLangFN(getLanguageFN(getLanguageDir(),(FORREST_PROGRAM_NAME),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	CTransaction_elv_object *pObj = getActiveObject();
	showFormView(122,sLangFN);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
		(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("Module122"),_T(""),1,pObj->getObjID_pk(),0));	
}

void CMDI1950ForrestNormFrame::OnUpdatePropertiesTBtn(void)
{
/*
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->populateProperties();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
*/

	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and save data; 080409 p�d
		pObjView = pFrame->getObjectFormView();
		if (pObjView != NULL)
		{
			pObjView->doPopulateLastObject();
			// Need to release ref. pointers; 080520 p�d
			pObjView = NULL;
		}
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}

}

void CMDI1950ForrestNormFrame::OnHandleDocumentTBtn(void)
{
	CXTPTabManagerItem *tabManager = NULL;
	int nTabIndex = -1;	
	CTransaction_elv_object *pObj = getActiveObject();
	CPropertiesFormView *pProp = NULL;
	CTransaction_elv_properties recELVProp;

	// Added 081013 p�d
	if (pObj->getObjID_pk() == -1)
	{
		::MessageBox(this->GetSafeHwnd(),(m_sMsgNoObjects),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
		return;
	}
	// Get Module language filename; 080616 p�d
	CString	sLangFN(getLanguageFN(getLanguageDir(),(FORREST_PROGRAM_NAME),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		tabManager = pFrame->getTabCtrl().getSelectedTabPage();
		if (tabManager != NULL)
		{
			nTabIndex = tabManager->GetIndex();

			switch (nTabIndex)
			{
					case 0 :	// Object tab
						showFormView(123,sLangFN);
						AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
							(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("Module123"),_T(""),pObj->getObjID_pk(),0,0));				
					break;

					case 1 :	// Properties tab
						pProp = pFrame->getPropertiesFormView();
						if (pProp != NULL)
						{						
							if (pProp->isAnyProperties())
							{
								recELVProp = *(pProp->getActivePropertyRecord());
								showFormView(123,sLangFN);
								AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
									(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("Module123"),_T(""),
															recELVProp.getPropID_pk(),0,1));
							}	// if (pProp->isAnyProperties())
							else
								::MessageBox(this->GetSafeHwnd(),(m_sMsgNoProperties),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
						}

					break;
			};

			tabManager = NULL;
		}
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}

}

void CMDI1950ForrestNormFrame::OnPrintOutTBtn(void)
{
	CString sMsg;
	CString sFileExtension;
	CString sArgStr;
	CString sReportPathAndFN;
	int nIdx = m_nSelPrintOut;
	int nObjID = m_recActiveObject.getObjID_pk();
	vecTransactionPropOwners vecPropertyOwners;	
	CTransaction_prop_owners recPropOwners;
	vecTransaction_elv_properties vecProperties;
	CTransaction_elv_properties recElvProps;
	CString sPropInfo;
	vecString vStrProp;
	vecString vStrGID;
	BOOL bFound = FALSE;
	int nMsgReturn = IDOK;

	if (m_pDB)
	{
		m_pDB->getPropertyOwners(vecPropertyOwners);
		m_pDB->getProperties(nObjID,vecProperties);
	}


	getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);

	if (m_vecReports.size() > 0 && nIdx >= 0 && nIdx < m_vecReports.size())
	{
		//********************************************************************************
		// Check which report user selected; 2010-09-17 P�D
		//	If report "Avisering"; check for "Mark�gare"
		//	If report "Utskick"; check for "GruppID" and "Mark�gare"
		CString sReportName(m_vecReports[nIdx].getFileName());
		CString sReport(m_vecReports[nIdx].getCaption());
		vStrProp.clear();
		CHtmlMsgDlg *pMsgDlg = new CHtmlMsgDlg();
		//------------------------------------------------------------------------
		// "Avisering"
		if (sReportName.CompareNoCase(HMS_5206_REPORT) == 0)
		{
			// There are properties but no propertyowners at all. Tell user; 100917 p�d
			if (vecPropertyOwners.size() == 0 && vecProperties.size() > 0)
			{
				if (pMsgDlg)
				{
					pMsgDlg->startHTML();				
					pMsgDlg->addTable();
					pMsgDlg->addTableText(sReport,12,true);
					pMsgDlg->addTableBR();
					pMsgDlg->addTableText(m_sMsgPrintOut1,10,true);
					pMsgDlg->addTableHR();
					pMsgDlg->addTableText(m_sMsgPrintOut3);
					pMsgDlg->endTable();

					nMsgReturn = pMsgDlg->ShowDialog();
				}
			}
			else if (vecPropertyOwners.size() > 0 && vecProperties.size() > 0)
			{
				// Try to match all properties in vecProperties 
				// to a property in m_vecPropertyOwners; 100917 p�d
				for (UINT i1 = 0;i1 < vecProperties.size();i1++)
				{
					recElvProps = vecProperties[i1];
					bFound = FALSE;
					for (UINT i2 = 0;i2 < vecPropertyOwners.size();i2++)
					{
						recPropOwners = vecPropertyOwners[i2];
						if (recPropOwners.getPropID() == recElvProps.getPropID_pk())
						{
							bFound = TRUE;
							break;
						}
					}	// for (UINT i2 = 0;i2 < vecPropertyOwners.size();i2++)
					if (!bFound)
					{
						sPropInfo.Format(L" - %s",recElvProps.getPropName());
						vStrProp.push_back(sPropInfo);
					}
				}	// for (UINT i1 = 0;i1 < vecProperties.size();i1++)
				if (vStrProp.size() > 0)
				{
					if (pMsgDlg)
					{
						pMsgDlg->startHTML();				
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut2,10,true);
						for (UINT i3 = 0;i3 < vStrProp.size();i3++)
							pMsgDlg->addTableText(vStrProp[i3]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut3);
						pMsgDlg->endTable();
	
						nMsgReturn = pMsgDlg->ShowDialog();
					}
				}
			}	// else if (vecPropertyOwners.size() > 0 && vecProperties.size() > 0)
		}	// if (sReportName.CompareNoCase(HMS_5206_REPORT) == 0)
		//------------------------------------------------------------------------
		// "Utskick"
		else if (sReportName.CompareNoCase(HMS_5215_REPORT) == 0)
		{
			bool bShowMsg = false;
			if (vecProperties.size() > 0)
			{
				// Check if there's GruppID set; 100921 p�d
				// I.e. compare number of properties to number of GruppID set
				vStrGID.clear();
				for (UINT i = 0;i < vecProperties.size();i++)
				{
					recElvProps = vecProperties[i];
					// Add proeprty to vector if there's no gruppid; 100921 p�d
					if (recElvProps.getPropGroupID() == L"" || recElvProps.getPropGroupID() == L" " )
					{
						sPropInfo.Format(L" - %s",recElvProps.getPropName());
						vStrGID.push_back(sPropInfo);
					}
				}
				if (pMsgDlg)
				{
					pMsgDlg->startHTML();			
					//-------------------------------------------------------------------------
					// There are properties but no propertyowners at all. Tell user; 100917 p�d
					if (vecPropertyOwners.size() > 0)
					{
						// Try to match all properties in vecProperties 
						// to a property in m_vecPropertyOwners; 100917 p�d
						bFound = FALSE;
						vStrProp.clear();
						for (UINT i1 = 0;i1 < vecProperties.size();i1++)
						{
							recElvProps = vecProperties[i1];
							bFound = FALSE;
							for (UINT i2 = 0;i2 < vecPropertyOwners.size();i2++)
							{
								recPropOwners = vecPropertyOwners[i2];
								if (recPropOwners.getPropID() == recElvProps.getPropID_pk())
								{
									bFound = TRUE;
									break;
								}
							}	// for (UINT i2 = 0;i2 < vecPropertyOwners.size();i2++)
							if (!bFound)
							{
								sPropInfo.Format(L" - %s",recElvProps.getPropName());
								vStrProp.push_back(sPropInfo);
							}
						}	// for (UINT i1 = 0;i1 < vecProperties.size();i1++)
					}	// if (vecPropertyOwners.size() == 0)
					//--------------------------------------------------
					// No propertyowners and no gruppid on any property
					if ((vecProperties.size() == vStrProp.size() || vecPropertyOwners.size() == 0) && vecProperties.size() == vStrGID.size())
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut4,10,true);
						pMsgDlg->addTableText(m_sMsgPrintOut5,10,true);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut8);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}
					//--------------------------------------------------
					// Propertyowners on all properties, but no gruppid 
					// on any property
					if ((vStrProp.size() == 0 && vecPropertyOwners.size() > 0) && vecProperties.size() == vStrGID.size())
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut5,10,true);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut9);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}
					//--------------------------------------------------
					// Propertyowners on all properties, but gruppid 
					// only on some property
					if ((vStrProp.size() == 0 && vecPropertyOwners.size() > 0) && vStrGID.size() < vecProperties.size() && vStrGID.size() > 0)
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut7,10,true);
						pMsgDlg->addTableHR();
						for (UINT i = 0;i < vStrGID.size();i++)
							pMsgDlg->addTableText(vStrGID[i]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut9);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}
					//--------------------------------------------------
					// Propertyowners on some properties, but no gruppid 
					// on any property
					if (vStrProp.size() < vecProperties.size() && vecProperties.size() == vStrGID.size() && vStrProp.size() > 0)
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut5,10,true);
						pMsgDlg->addTableText(m_sMsgPrintOut6,10,true);
						//pMsgDlg->addTableHR();
						for (UINT i = 0;i < vStrProp.size();i++)
							pMsgDlg->addTableText(vStrProp[i]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut8);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}

					//--------------------------------------------------
					// Propertyowners and gruppid only on 
					// some properties
					if (vStrProp.size() < vecProperties.size() && vStrGID.size() < vecProperties.size() && vStrProp.size() > 0 && vStrGID.size() > 0)
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut6,10,true);
						for (UINT i = 0;i < vStrProp.size();i++)
							pMsgDlg->addTableText(vStrProp[i]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut7,10,true);
						for (UINT i = 0;i < vStrGID.size();i++)
							pMsgDlg->addTableText(vStrGID[i]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut8);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}
					//--------------------------------------------------
					// GruppID on all properties but no propertyowners.
					if (vStrGID.size() == 0 && (vStrProp.size() == vecProperties.size() && vecPropertyOwners.size() > 0))
					{
						pMsgDlg->startHTML();				
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut1,10,true);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut3);
						pMsgDlg->endTable();

						nMsgReturn = pMsgDlg->ShowDialog();
					}
					//--------------------------------------------------
					// GruppID on all properties but propertyowners only
					// on some properties
					if (vStrGID.size() == 0 && (vStrProp.size() < vecProperties.size() && vStrProp.size() > 0)) 
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut6,10,true);
						for (UINT i = 0;i < vStrProp.size();i++)
							pMsgDlg->addTableText(vStrProp[i]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut3);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}

				}	// if (pMsgDlg)
			}	// if (vecProperties.size() > 0)	
		} // else if (sReportName.CompareNoCase(HMS_5215_REPORT) == 0)
		//------------------------------------------------------------------------
		// "St�mplingsl�ngd"
		// "St�mplingsl�ngd per Objekt"
		// "St�mplingsl�ngd ut�kad"
		// "Intr�ngsv�rdering"
		// "Ers�ttningserbjudande"
		// "Rotnetto"
		// "Sammandrag"
		// "Objektsredovisning"
		else if (sReportName.CompareNoCase(HMS_5201_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5207_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5217_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5202_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5203_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5205_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5208_REPORT) == 0 ||
						 sReportName.CompareNoCase(HMS_5209_REPORT) == 0)
		{
			bool bShowMsg = false;
			if (vecProperties.size() > 0)
			{
				// Check if there's GruppID set; 100921 p�d
				// I.e. compare number of properties to number of GruppID set
				vStrGID.clear();
				for (UINT i = 0;i < vecProperties.size();i++)
				{
					recElvProps = vecProperties[i];
					// Add proeprty to vector if there's no gruppid; 100921 p�d
					if (recElvProps.getPropGroupID() == L"" || recElvProps.getPropGroupID() == L" " )
					{
						sPropInfo.Format(L" - %s",recElvProps.getPropName());
						vStrGID.push_back(sPropInfo);
					}
				}
				if (pMsgDlg)
				{
					pMsgDlg->startHTML();			
					//--------------------------------------------------
					// No gruppid on any property
					if (vecProperties.size() == vStrGID.size())
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut5,10,true);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut9);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}
					//--------------------------------------------------
					// Gruppid only on some properties
					if (vStrGID.size() < vecProperties.size() && vStrGID.size() > 0)
					{
						pMsgDlg->addTable();
						pMsgDlg->addTableText(sReport,12,true);
						pMsgDlg->addTableBR();
						pMsgDlg->addTableText(m_sMsgPrintOut7,10,true);
						pMsgDlg->addTableHR();
						for (UINT i = 0;i < vStrGID.size();i++)
							pMsgDlg->addTableText(vStrGID[i]);
						pMsgDlg->addTableHR();
						pMsgDlg->addTableText(m_sMsgPrintOut9);
						pMsgDlg->endTable();
						nMsgReturn = pMsgDlg->ShowDialog();
					}
				}	// if (pMsgDlg)
			}	// if (vecProperties.size() > 0)	
		}

		if (pMsgDlg) delete pMsgDlg;

		// Cancel method if user selected "avbryt" in msgdlg; 100921 p�d
		if (nMsgReturn == IDCANCEL) return;
		//********************************************************************************

		// Add ObjectID as argument to Report; 090129 p�d
		sReportPathAndFN.Format(_T("%s%s\\%s"),
														getReportsDir(),
														getLangSet(),
														m_vecReports[nIdx].getFileName());

		if (fileExists(sReportPathAndFN))
		{
			// 081107 p�d
			// Try to get extension of file, to determin' if it's
			// a FastReport or Crystal Report.
			// I.e.
			//	.fr3 = FastReports
			//	.rpt = Crystal Reports
			sFileExtension = sReportPathAndFN.Right(3);

			if (sFileExtension.CompareNoCase(_T("fr3")) == 0)
			{
				sArgStr.Format(_T("%d;"),nObjID);
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
					(LPARAM)&_user_msg(300,	// ID = 333 for CrystalReport, ID = 300 for FastReports
					_T("OpenSuiteEx2"),			// Exported/Imported function
					_T("Reports.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports; 080214 p�d
					(sReportPathAndFN),		// Use this report
					_T(""),
					(sArgStr)));
			}
			else if (sFileExtension.CompareNoCase(_T("rpt")) == 0)
			{
				sArgStr.Format(_T("GruppId - object_id=%d;"),nObjID);
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
					(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
					_T("OpenSuiteEx"),			// Exported/Imported function
					_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports; 080214 p�d
					(sReportPathAndFN),		// Use this report
					(sReportPathAndFN),
					(sArgStr)));
			}
		}	// if (fileExists(sReportPathAndFN))
		else
		{
			sMsg.Format(_T("%s\n\n%s\n"),m_sMissingFileForPrintOutMsg,sReportPathAndFN);
			::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONASTERISK | MB_OK);
		}

	}	// if (nIdx > -1 && nIdx < m_vecReports.size())
}

void CMDI1950ForrestNormFrame::OnRemovePropertyTBtn(void)
{
	RemoveProperties(1);
	/*
	int nObjID = -1;
	//Trycker upp en listview h�r f�r att kunna v�lja flera fastigheter att ta bort. Feature #2456 20111021 J�
	CTransaction_elv_object* pObj=getActiveObject();
	nObjID=pObj->getObjID_pk();
	showFormView(IDD_REPORTVIEW1,m_sLangFN,(LPARAM)nObjID);*/


	/*
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;


	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->removeProperty(1))
			{				
				// Added 080904 p�d
				pPropView->resetCruisingReport();
				pPropView->resetEvaluatedReport();
			}
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;*/
	
}

void CMDI1950ForrestNormFrame::OnRemovePropertyTBtn2(void)
{
	RemoveProperties(2);
	/*
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->removeProperty(2))
			{
				// Added 080904 p�d
				pPropView->resetCruisingReport();
				pPropView->resetEvaluatedReport();
			}
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;*/
}

//Lagt till funktion f�r att ta bort flera fastigheter feature #2456 20111101 J�
BOOL CMDI1950ForrestNormFrame::RemoveProperties(int nRemoveRotpost)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CXTPReportRow *pRow;
	CELVPropertyReportRec *pRec = NULL;
	std::vector<int> vecPropId;
	vecPropId.clear();
	int i=0,i2=0,nRemovedProp=0,nId=0;
	bool bRemoved=false;
	bool bAnsToAll=false;
	bool bYesToAll=false;
	bool bNoToAll=false;

	//Testa att plocka r�tt p� alla markerade fastigheter 
	//--------------------------------------------------------------------------------------------------------------------
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			CXTPReportSelectedRows *pRows = pPropView->m_wndReportProperties.GetSelectedRows();
			if (pRows != NULL)
			{
				for (i=0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						//Fyll p� lista med valda fastighets id:n, dvs de rader som �r markerade
						pRec = (CELVPropertyReportRec*)pRow->GetRecord();					
						vecPropId.push_back(pRec->getPropId());
					}
				}
			}
		}
	}
	if(vecPropId.size()<=0)
		return FALSE;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			CXTPReportSelectedRows *pRows = pPropView->m_wndReportProperties.GetSelectedRows();
			if (pRows != NULL)
			{
				//Avmarkera sedan vald rad						
				pRow->SetSelected(false);
			}
		}
	}
	//S�tt focus p� respektive rad/fastighet som skall tas bort och testa att radera den
	//Detta eftersom metoderna som tar bort en fastighet bygger p� att den raden �r fokuserad
	for(i=0;i<vecPropId.size();i++)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			CXTPReportRows *pRows = pPropView->m_wndReportProperties.GetRows();
			for (i2=0;i2 < pRows->GetCount() ;i2++)
			{
				pRow = pRows->GetAt(i2);
				if (pRow != NULL)
				{
					//Leta efter r�tt fastighetsid bland rapportraderna
					pRec = (CELVPropertyReportRec*)pRow->GetRecord();	
					if (pRec)
					{
						nId=pRec->getPropId();
						if(vecPropId[i]==nId)
						{
							//S�tt fokus och s�tt den fastigheten som aktiv
							pPropView->GetReport()->SetFocusedRow(pRow);
							pPropView->setActivePropertyInReport();
							//pRow->SetSelected(true);
								//Kolla status s� den g�r att radera
								if (!doesStatusAllowChange(FALSE)) 
								{}
								else
								{						
									//Testa att ta bort fastigheten
									if (pPropView->removeProperty(nRemoveRotpost,true,&bAnsToAll,&bYesToAll,&bNoToAll))
									{				
										nRemovedProp=1;
										pPropView->resetCruisingReport();
										pPropView->resetEvaluatedReport();	
										//S�tt i2 till storleken p� listan s� den slutar att g� igenom fastigheterna n�r den har hittat r�tt for (i2=0;i2 < pRows->GetCount() ;i2++)
										i2 = pRows->GetCount();
										//pPropView->resetCruisingReport();
										//pPropView->resetEvaluatedReport();
										pRow->SetSelected(false);
									}
									else
										pRow->SetSelected(true);

								}
							break;
						}
					}
				}
			}
		}
	}
	//Reset om n�gon fastighet �r borttagen
	/*if(nRemovedProp==1)
	{
		pPropView->resetCruisingReport();
		pPropView->resetEvaluatedReport();	
	}*/

	return TRUE;
}

//HMS-135 20250123 J� Formatera �gd andel till procent 
float CMDI1950ForrestNormFrame::FormatShareToPercent(CString cShare)
{
	CString cRet=_T("");
	CString cV1=_T("");
	CString cV2=_T("");
	int nPos=0,nLen=0;
	float fValue=0.0;
	float fValue1=0.0;
	float fValue2=0.0;
	nPos=cShare.Find(_T("%"));
	if(nPos>=0)
	{
		cShare.Replace(_T("%"),_T(""));
		fValue=_tstof(cShare);
		fValue/=100.0;
	}
	else
	{
		nPos=cShare.Find(_T("/"));
		if(nPos>0)
		{
			nLen=cShare.GetLength();
			cV1=cShare.Left(nPos);
			cV2=cShare.Right(nLen-1-nPos);
			fValue1=_tstof(cV1);
			fValue2=_tstof(cV2);
			fValue=fValue1/fValue2;
		}
		else
		{
			fValue=_tstof(cShare);
			if(fValue>1.0)	//Om v�rdet �r st�rre �n 1 och ej procent eller br�k angivet s� anta att man skrivit i procent fast gl�mt att skriva procenttecknet
			{
			fValue/=100.0;
			}
		}
	}
	
	return fValue*100.0;
}

//HMS-30 20200704 J� Formatera �gd andel till decimaltal
CString CMDI1950ForrestNormFrame::FormatShareToDecimal(CString cShare)
{
	CString cRet=_T("");
	CString cV1=_T("");
	CString cV2=_T("");
	int nPos=0,nLen=0;
	float fValue=0.0;
	float fValue1=0.0;
	float fValue2=0.0;
	nPos=cShare.Find(_T("%"));
	if(nPos>=0)
	{
		cShare.Replace(_T("%"),_T(""));
		fValue=_tstof(cShare);
		fValue/=100.0;
	}
	else
	{
		nPos=cShare.Find(_T("/"));
		if(nPos>0)
		{
			nLen=cShare.GetLength();
			cV1=cShare.Left(nPos);
			cV2=cShare.Right(nLen-1-nPos);
			fValue1=_tstof(cV1);
			fValue2=_tstof(cV2);
			fValue=fValue1/fValue2;
		}
		else
		{
			fValue=_tstof(cShare);
			if(fValue>1.0)	//Om v�rdet �r st�rre �n 1 och ej procent eller br�k angivet s� anta att man skrivit i procent fast gl�mt att skriva procenttecknet
			{
			fValue/=100.0;
			}
		}
	}

	cRet.Format(_T("%.3f"),fValue);
	return cRet;
}

void CMDI1950ForrestNormFrame::OnExportPropData(void)
{

}

//HMS-76 20220125 J� Json export av fastighetsdata
void CMDI1950ForrestNormFrame::OnExportPropertiesJsonTBtn(void)
{
	vecTransaction_elv_cruise vecELVObjectCruise;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;
	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;
	CTransaction_elv_properties_other_comp other_erst_rec;
	vecTransaction_elv_properties_other_comp vecOtherComp;
	vecTransaction_property_status m_vecPropStatus;

	vecTransactionPlot vecPlots;
	CTransaction_plot recPlot;
	vecTransactionSampleTree vecSampleTrees;
	CTransaction_sample_tree recSampleTree;
	vecTransactionDCLSTree vecDclsTrees;
	CTransaction_dcls_tree recDclsTree;
	int nNrTrees=0;
	CTransaction_elv_cruise recCruise;
	CTransaction_trakt recTrakt;

	CString cSavePath=_T("");
	CString cXml=_T("");
	CString cTmp=_T("");
	CString cTmp2=_T("");
	CString cTmp3=_T("");
	CString cTagFirst=_T("");
	CString cTagLast=_T("");
	CString cXmlFileName=_T("");
	CString cContact=_T("");
	CStringArray arr;
	int nTypeOfOther=0;
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');

	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);
	m_pDB->getPropertyActionStatus(m_vecPropStatus);

	cSavePath=getRegisterExportXmlJsonDir();

	BOOL bRet = XBrowseForFolder(this->GetSafeHwnd(),
		cSavePath,
		szFolder,
		sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		cSavePath = szFolder;
		if(cSavePath.GetAt(cSavePath.GetLength()-1) != '\\')
			cSavePath += _T("\\");

	}
	else 
		return;

	setRegisterExportXmlJsonDir(cSavePath);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));

		cTmp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		cTmp.Replace(':',';');
		cXmlFileName.Format(_T("%s\\%s.json"),cSavePath,cTmp);		

		cXml+=_T("{\"Object\":{");
		cTmp.Format(_T("\"Name\":\"%s\","),pObj->getObjectName()); cXml+=cTmp;
		cTmp.Format(_T("\"Id\":\"%s\","),pObj->getObjIDNumber()); cXml+=cTmp;
		cTmp.Format(_T("\"Nr\":\"%s\","),pObj->getObjErrandNum()); cXml+=cTmp;
		cTmp.Format(_T("\"Littera\":\"%s\","),pObj->getObjLittra()); cXml+=cTmp;
		//HMS-80 20220304 J� �ndrat fr�n getobjectdate till startdate
		cTmp.Format(_T("\"Date\":\"%s\","),pObj->getObjStartDate()); cXml+=cTmp;
		cTmp.Format(_T("\"Performedby\":\"%s\","),pObj->getObjCreatedBy()); cXml+=cTmp;

		

		cXml+=_T("\"Properties\":[");
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];
			if(i>0)
				cXml+=_T(",{"); //Property
			else
				cXml+=_T("{"); //Property


			// Fastighetsid
			cTmp.Format(_T("\"Id\":%d,"),recProperty.getPropID_pk()); cXml+=cTmp;
			cTmp.Format(_T("\"Name\":\"%s\","),recProperty.getPropName()); cXml+=cTmp;
			cTmp.Format(_T("\"Nr\":\"%s\","),recProperty.getPropNumber()); cXml+=cTmp;
			cTmp.Format(_T("\"Municipal\":\"%s\","),recProperty.getPropMunicipal()); cXml+=cTmp;
			cTmp.Format(_T("\"County\":\"%s\","),recProperty.getPropCounty()); cXml+=cTmp;


			for (UINT i1 = 0;i1 < m_vecPropStatus.size();i1++)
			{
				if (m_vecPropStatus[i1].getPropStatusID_pk() == recProperty.getPropStatus())
				{
					cTmp.Format(_T("\"Status\":\"%s\","),m_vecPropStatus[i1].getPropStatusName()); cXml+=cTmp;
				}
			}
			
			cTmp.Format(_T("\"StatusId\":%d,"),recProperty.getPropStatus()); cXml+=cTmp;

			cTmp.Format(_T("\"TypeOfAction\":%d,"),recProperty.getPropTypeOfAction()); cXml+=cTmp;
			
			cXml+=_T("\"Evaluation\":{");			
			cTmp.Format(_T("\"Landvalue\":%.0f,"),floor(recProperty.getPropLandValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Rotvalue\":%.0f,"),floor(recProperty.getPropRotpost()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Woodvalue\":%.0f,"),floor(recProperty.getPropWoodValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Earlycutvalue\":%.0f,"),floor(recProperty.getPropEarlyCutValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Highcutvalue\":%.0f,"),floor(recProperty.getPropHighCutValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Stormdryvalue\":%.0f,"),floor(recProperty.getPropStromDryValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Bordertreevalue\":%.0f,"),floor(recProperty.getPropRandTreesValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Voluntaryvalue\":%.0f,"),floor(recProperty.getPropVoluntaryDealValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Costvalue\":%.0f,"),floor(recProperty.getPropCostValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Volume\":%.2f"),floor(100.0*(recProperty.getPropM3Sk()+0.005))/100.0); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");
			cTmp.Format(_T("\"Area\":%.2f"),recProperty.getPropAreal()); cTmp.Replace(',','.');  cXml+=cTmp+_T(",");
			cTmp.Format(_T("\"Numtrees\":%d,"),recProperty.getPropNumOfTrees()); cXml+=cTmp;		
			cTmp.Format(_T("\"Forestfuelvalue\":%.0f,"),floor(recProperty.getPropGrotValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("\"Forestfueltons\":%.3f"),floor(1000.0*(recProperty.getPropGrotVolume()+0.0005))/1000.0); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");
			cTmp.Format(_T("\"Forestfuelcost\":%.0f,"),floor(recProperty.getPropGrotCost()+0.5)); cXml+=cTmp;

			cTmp.Format(_T("\"Groupid\":\"%s\","),recProperty.getPropGroupID()); cXml+=cTmp;

			cXml+=_T("\"Othercompensation\":{");
			cTmp.Format(_T("\"Othertotal\":%.0f"),floor(recProperty.getPropOtherCostValue()+0.5)); cXml+=cTmp;
			//H�mta annan ers�ttning 
			m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),recProperty.getPropID_pk(),vecOtherComp);

			if (vecOtherComp.size() > 0)
			{
				for (UINT i = 0;i < vecOtherComp.size();i++)
				{
					other_erst_rec = vecOtherComp[i];
					nTypeOfOther=other_erst_rec.getPropOtherCompTypeOf();
    				cXml+=_T(",");
					switch(nTypeOfOther)
					{
					case 0:	//Fast ers
						cTagFirst=_T("\"Fixed\":{");
						break;
					case 1:	//Fast ers + moms
						cTagFirst=_T("\"FixedVat\":{");
						break;
					case 2:	//Intr�ngsers�ttning
						cTagFirst=_T("\"Intrusion\":{");
						break;
					case 3:	//Kr per m3 gagnvirke
						cTagFirst=_T("\"Merchwood\":{");
						break;
					case 4:	//3. Hinder i �kermark
						cTagFirst=_T("\"Fieldobstacle\":{");
						break;
					case 5:	//4a. Ledning i skogsmark 							
						cTagFirst=_T("\"Forestline\":{");
						break;
					case 6:	//5. �vrigt intr�ng
						cTagFirst=_T("\"Otherintrusion\":{");
						break;
					}

					cXml+=cTagFirst;
					cTmp.Format(_T("\"Description\":\"%s\","),other_erst_rec.getPropOtherCompNote()); cXml+=cTmp;
					cTmp.Format(_T("\"Value\":%.2f"),floor(100.0*(other_erst_rec.getPropOtherCompValue_calc()+0.005))/100.0); cTmp.Replace(',','.');  cXml+=cTmp+_T(",");
					
					
					cTmp.Format(_T("\"Id\":%d"),nTypeOfOther+1); cXml+=cTmp;
					cXml+=_T("}");
				}
			}
			cXml+=_T("}");	// Other comp
			cXml+=_T("}");	// Evaluation	//HMS-96 20230124 Flyttat kommat till ownerstaggen annars komma �ven d� det inte finns owners d� blir syntaxfel i json filen


			vecTransaction_eval_evaluation vecEvals;
			CTransaction_eval_evaluation	recEval;
			// HMS-124 mer info, v�rderingsbest�nd
			m_pDB->getObjectEvaluation(recProperty.getPropID_pk(),recProperty.getPropObjectID_pk(),vecEvals);
			if (vecEvals.size() > 0)
			{
				cXml+=_T(",\"Evaluationstands\":[");


				for (UINT e = 0;e < vecEvals.size();e++)
				{
					recEval=vecEvals[e];
					if(e>0)
						cXml+=_T(",{");
					else
						cXml+=_T("{");

					// V�rderingsbest�nd namn
					cTmp.Format(_T("\"Name\":\"%s\","),recEval.getEValName()); cXml+=cTmp;

					// V�rderingsbest�nd id
					cTmp.Format(_T("\"Id\":%d,"),recEval.getEValID_pk()); cXml+=cTmp;

					// Fastighetsid
					cTmp.Format(_T("\"Propertyid\":%d,"),recCruise.getECruPropID_pk()); cXml+=cTmp;

					// V�rderingsbest�nd areal
					cTmp.Format(_T("\"Areal\":%.3f"),recEval.getEValAreal()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

					// V�rderingsbest�nd �lder
					cTmp.Format(_T("\"Age\":%d,"),recEval.getEValAge()); cXml+=cTmp;

					// V�rderingsbest�nd SI
					cTmp.Format(_T("\"Si\":\"%s\","),recEval.getEValSIH100()); cXml+=cTmp;

					// V�rderingsbest�nd Korrfakt
					cTmp.Format(_T("\"Corrfactor\":%.1f"),recEval.getEValCorrFactor()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

					// V�rderingsbest�nd Markv�rde
					cTmp.Format(_T("\"Landvalue\":%.3f"),recEval.getEValLandValue()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

					// V�rderingsbest�nd Markv�rde per ha
					cTmp.Format(_T("\"Landvalueha\":%.3f"),recEval.getEValLandValue_ha()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

					// V�rderingsbest�nd F�rtidig
					cTmp.Format(_T("\"Precut\":%.3f"),recEval.getEValPreCut()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

					// V�rderingsbest�nd F�rtidig per ha
					cTmp.Format(_T("\"Precutha\":%.3f"),recEval.getEValPreCut_ha()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

					// V�rderingsbest�nd Tallandel
					cTmp.Format(_T("\"Pinepart\":%.0f,"),recEval.getEValPinePart()); cXml+=cTmp;

					// V�rderingsbest�nd Granandel
					cTmp.Format(_T("\"Sprucepart\":%.0f,"),recEval.getEValSprucePart()); cXml+=cTmp;

					// V�rderingsbest�nd Bj�rkandel
					cTmp.Format(_T("\"Birchpart\":%.0f"),recEval.getEValBirchPart()); cXml+=cTmp;

					cXml+=_T("}");
				}
				cXml+=_T("]");	//Evaluations
			}

			// HMS-120 Lista best�nd med info och koordinater			
			// HMS-124 mer info stmpll�ngd, tr�d, ytor
			m_pDB->getObjectCruises(recProperty.getPropID_pk(),recProperty.getPropObjectID_pk(),vecELVObjectCruise);
			if (vecELVObjectCruise.size() > 0)
			{
				cXml+=_T(",\"Trakts\":[");

				for (UINT i = 0;i < vecELVObjectCruise.size();i++)
				{
					if(i>0)
						cXml+=_T(",{");
					else
						cXml+=_T("{");

					recCruise = vecELVObjectCruise[i];					
					//H�mta upp best�ndet fr�n trakt table f�r koordinater
					m_pDB->getTrakt(recCruise.getECruID_pk(),recTrakt);

				
					//Best�ndsnamn
					cTmp.Format(_T("\"Name\":\"%s\","),recCruise.getECruName()); cXml+=cTmp;

					//Best�ndsid
					cTmp.Format(_T("\"Id\":%d,"),recCruise.getECruID_pk()); cXml+=cTmp;

					//Fastighetsid
					cTmp.Format(_T("\"Propertyid\":%d,"),recCruise.getECruPropID_pk()); cXml+=cTmp;

					// Best�nd SI
					cTmp.Format(_T("\"Si\":\"%s\","),recTrakt.getTraktSIH100()); cXml+=cTmp;

					// Best�nd �lder
					cTmp.Format(_T("\"Age\":%d,"),recTrakt.getTraktAge()); cXml+=cTmp;

					// Best�nd Areal
					cTmp.Format(_T("\"Areal\":%.3f"),recTrakt.getTraktAreal()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");


					//Best�ndpunkt,koordinat
					//cTmp.Format(_T("\"Point\":\"%s\""),recTrakt.getTraktPoint()); cXml+=cTmp;
					cTmp.Format(_T("\"geometry\":[{"));	cXml+=cTmp;
					cTmp.Format(_T("\"type\": \"Point\","));	cXml+=cTmp;
					cTmp.Format(_T("\"coordinates\":"));	cXml+=cTmp;
					cTmp.Format(_T("[%s]"),recTrakt.getTraktPoint()); cXml+=cTmp;
					cTmp.Format(_T("},"));cXml+=cTmp;
				

					//Best�ndskoordinater
					//cTmp.Format(_T("\"Coordinates\":\"%s\","),recTrakt.getTraktCoordinates()); cXml+=cTmp;
					cTmp.Format(_T("{"));	cXml+=cTmp;
					cTmp.Format(_T("\"type\": \"Polygon\","));	cXml+=cTmp;
					cTmp.Format(_T("\"coordinates\":"));	cXml+=cTmp;					
					cTmp.Format(_T("[[%s]]"),recTrakt.getTraktCoordinates()); 
					cTmp.Replace(_T(", "),_T(","));
					cTmp.Replace(_T("\n"),_T("],["));
					cXml+=cTmp;
					
					
					cTmp.Format(_T("}]")); cXml+=cTmp;


					// St�mplingsl�ngd tr�d i gatan
					// HMS-124 mer info			
					nNrTrees=0;
					vecDclsTrees.clear();
					m_pDB->getDCLSTreesforJsonExport(recCruise.getECruID_pk(),vecDclsTrees);

					if(vecDclsTrees.size()>0)
					{
						cXml+=_T(",\"TreeTable\":[");
						for (UINT d = 0;d < vecDclsTrees.size();d++)
						{
							recDclsTree=vecDclsTrees[d];

							if(nNrTrees>0)
								cXml+=_T(",{");
							else
								cXml+=_T("{");

							cTmp.Format(_T("\"Speciesname\":\"%s\","),recDclsTree.getSpcName()); cXml+=cTmp;

							cTmp.Format(_T("\"Speciesid\":%d,"),recDclsTree.getSpcID()); cXml+=cTmp;

							cTmp.Format(_T("\"From\":%.0f,"),recDclsTree.getDCLS_from()); cXml+=cTmp;

							cTmp.Format(_T("\"Numtrees\":%d"),recDclsTree.getNumOf()-recDclsTree.getNumOfRandTrees()); cXml+=cTmp;

							cXml+=_T("}"); //Dcsl
							nNrTrees++;
						}
						cXml+=_T("]");	//Treetable
					}							


					// St�mplingsl�ngd kanttr�d
					// HMS-124 mer info			
					if(vecDclsTrees.size()>0)
					{
						nNrTrees=0;
						cXml+=_T(",\"TreeHazardTable\":[");
						for (UINT d = 0;d < vecDclsTrees.size();d++)
						{
							recDclsTree=vecDclsTrees[d];							

							if(nNrTrees>0)
								cXml+=_T(",{");
							else
								cXml+=_T("{");

							cTmp.Format(_T("\"Speciesname\":\"%s\","),recDclsTree.getSpcName()); cXml+=cTmp;

							cTmp.Format(_T("\"Speciesid\":%d,"),recDclsTree.getSpcID()); cXml+=cTmp;

							cTmp.Format(_T("\"From\":%.0f,"),recDclsTree.getDCLS_from()); cXml+=cTmp;

							cTmp.Format(_T("\"Numtrees\":%d"),recDclsTree.getNumOfRandTrees()); cXml+=cTmp;

							cXml+=_T("}"); //Dcls
							nNrTrees++;
						}
						cXml+=_T("]");	//TreeHazardtable
					}


					// Ytor
					// HMS-124 mer info, ytor
					int nPlotnum=0;
					vecPlots.clear();
					m_pDB->getPlots(recCruise.getECruID_pk(),vecPlots);
					if (vecPlots.size() > 0)
					{
						cXml+=_T(",\"Plots\":[");

						for (UINT p = 0;p < vecPlots.size();p++)
						{
							recPlot= vecPlots[p];
							if(nPlotnum>0)
								cXml+=_T(",{");
							else
								cXml+=_T("{");

							//Fastighetsid
							cTmp.Format(_T("\"Propertyid\":%d,"),recCruise.getECruPropID_pk()); cXml+=cTmp;

							//Best�ndsid
							cTmp.Format(_T("\"Traktid\":%d,"),recPlot.getTraktID()); cXml+=cTmp;

							//Ytid
							cTmp.Format(_T("\"Id\":%d,"),recPlot.getPlotID()); cXml+=cTmp;

							//Ytnamn
							cTmp.Format(_T("\"Name\":\"%s\","),recPlot.getPlotName()); cXml+=cTmp;

							//Ytarea
							cTmp.Format(_T("\"Area\":%.0f,"),recPlot.getArea()); cXml+=cTmp;

							//Ytpunkt,koordinat
							//cTmp.Format(_T("\"Point\":\"%s\""),recPlot.getCoord()); cXml+=cTmp;
							cTmp.Format(_T("\"geometry\": {"));	cXml+=cTmp;
							cTmp.Format(_T("\"type\": \"Point\","));	cXml+=cTmp;
							cTmp.Format(_T("\"coordinates\":"));	cXml+=cTmp;
							cTmp.Format(_T("[%s]"),recPlot.getCoord()); cXml+=cTmp;
							cTmp.Format(_T("}"));cXml+=cTmp;



							cXml+=_T("}"); //Plot
							nPlotnum++;
						}
						cXml+=_T("]");	//Plots
					}

					// Tr�d
					// HMS-124 mer info
					nNrTrees=0;
					vecSampleTrees.clear();
					m_pDB->getLandValueTrees(recCruise.getECruID_pk(),-1,vecSampleTrees);
					//Skriv ut tr�d med koordinat
					if(vecSampleTrees.size()>0)
					{
						cXml+=_T(",\"Trees\":[");
						for (UINT t = 0;t < vecSampleTrees.size();t++)
						{
							recSampleTree=vecSampleTrees[t];
							if(recSampleTree.getCoord().GetLength()>2)
							{

								if(nNrTrees>0)
									cXml+=_T(",{");
								else
									cXml+=_T("{");

								//Fastighetsid
								cTmp.Format(_T("\"Propertyid\":%d,"),recCruise.getECruPropID_pk()); cXml+=cTmp;

								//Best�ndsid
								cTmp.Format(_T("\"Traktid\":%d,"),recSampleTree.getTraktID()); cXml+=cTmp;

								//Ytid
								cTmp.Format(_T("\"Plotid\":%d,"),recSampleTree.getPlotID()); cXml+=cTmp;

								//Tr�did
								cTmp.Format(_T("\"Treeid\":%d,"),recSampleTree.getTreeID()); cXml+=cTmp;

								//Spec id
								cTmp.Format(_T("\"Specid\":%d,"),recSampleTree.getSpcID()); cXml+=cTmp;

								//Spec name
								cTmp.Format(_T("\"Specname\":\"%s\","),recSampleTree.getSpcName()); cXml+=cTmp;

								//Koordinat
								//cTmp.Format(_T("\"Point\":\"%s\","),recSampleTree.getCoord()); cXml+=cTmp;
								cTmp.Format(_T("\"geometry\": {"));	cXml+=cTmp;
								cTmp.Format(_T("\"type\": \"Point\","));	cXml+=cTmp;
								cTmp.Format(_T("\"coordinates\":"));	cXml+=cTmp;
								cTmp.Format(_T("[%s]"),recSampleTree.getCoord());cXml+=cTmp;
								cTmp.Format(_T("},"));cXml+=cTmp;

								//Dbh
								cTmp.Format(_T("\"Dbh\":%.0f,"),recSampleTree.getDBH()); cXml+=cTmp;

								//H�jd
								cTmp.Format(_T("\"Hgt\":%.0f,"),recSampleTree.getHgt()); cXml+=cTmp;

								//Typ
								cTmp.Format(_T("\"Type\":%d,"),recSampleTree.getTreeType()); cXml+=cTmp;

								//�lder
								cTmp.Format(_T("\"Age\":%d,"),recSampleTree.getAge()); cXml+=cTmp;

								//Bonitet
								cTmp.Format(_T("\"Si\":\"%s\","),recSampleTree.getBonitet()); cXml+=cTmp;

								//m3sk
								cTmp.Format(_T("\"M3sk\":%.3f"),recSampleTree.getM3Sk()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

								//m3ub
								cTmp.Format(_T("\"M3ub\":%.3f"),recSampleTree.getM3Ub()); cTmp.Replace(',','.'); cXml+=cTmp+_T(",");

								//m3fub
								cTmp.Format(_T("\"M3fub\":%.3f"),recSampleTree.getM3Fub()); cTmp.Replace(',','.'); cXml+=cTmp;

								cXml+=_T("}"); //Tree
								nNrTrees++;
							}
						}
						cXml+=_T("]");	//Trees						
					}			
					cXml+=_T("}");
				}

				cXml+=_T("]");	//Trakts
			}


			// H�nta kontakter och �garinfo
			m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
			m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

			if (vecContacts.size() > 0)
			{
				cXml+=_T(",\"Owners\":["); //HMS-96 20230124 Flyttat kommat till ownerstaggen annars komma �ven d� det inte finns owners d� blir syntaxfel i json filen
				//Loopa och skriv ut alla kontakter
				for (UINT i = 0;i < vecContacts.size();i++)
				{
					if(i>0)
						cXml+=_T(",{");
					else
						cXml+=_T("{");
					recContact = vecContacts[i];
					//Skriv bara ut 7 kontakter
					if (vecPropertyOwners.size() > 0)
					{								
						for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
						{											
							recPropertyOwner = vecPropertyOwners[i1];									
							if (recPropertyOwner.getContactID() == recContact.getID())
							{
								cContact=_T("false");
								if(recPropertyOwner.getIsContact() == 1)
									cContact=_T("true");;
								cTmp.Format(_T("\"Contact\":\"%s\","),cContact); cXml+=cTmp;
								cTmp.Format(_T("\"Name\":\"%s\","),recContact.getName()); cXml+=cTmp;
								cTmp.Format(_T("\"Adress\":\"%s\","),recContact.getAddress()); cXml+=cTmp;
								cTmp.Format(_T("\"Zip\":\"%s\","),recContact.getPostNum()); cXml+=cTmp;
								cTmp.Format(_T("\"City\":\"%s\","),recContact.getPostAddress()); cXml+=cTmp;
								cTmp.Format(_T("\"Phonehome\":\"%s\","),recContact.getPhoneHome()); cXml+=cTmp;
								cTmp.Format(_T("\"Phonework\":\"%s\","),recContact.getPhoneWork()); cXml+=cTmp;
								cTmp.Format(_T("\"Email\":\"%s\","),recContact.getEMail()); cXml+=cTmp;
								cTmp.Format(_T("\"Share    \":\"%s\","),FormatShareToDecimal(recContact.getOwnerShare())); cXml+=cTmp;
								cTmp.Format(_T("\"Sharetext\":\"%s\","),recContact.getOwnerShare()); cXml+=cTmp;

								cTmp.Format(_T("\"Orgnr\":\"%s\","),recContact.getPNR_ORGNR()); cXml+=cTmp;


								
								Split1(recContact.getCoAddress(),_T("\n"),arr);						
								cTmp3=_T("");
								for(int i=0;i<arr.GetSize();i++)
								{
									cTmp2=arr.GetAt(i);
									if(i>0)
									{
										if(cTmp2.GetLength()>0)
											cTmp3+=_T(",\"")+cTmp2+_T("\"");
									}
									else
									{
									cTmp3+=_T("\"")+cTmp2+_T("\"");
									}
								}
								cTmp.Format(_T("\"COAdress\":[%s],"),cTmp3); 
								cXml+=cTmp;

		



								cTmp.Format(_T("\"Vat\":\"%s\","),recContact.getVATNumber()); cXml+=cTmp;
								cTmp.Format(_T("\"Mobilephone\":\"%s\","),recContact.getMobile()); cXml+=cTmp;
								cTmp.Format(_T("\"Bankgiro\":\"%s\","),recContact.getBankgiro()); cXml+=cTmp;
								cTmp.Format(_T("\"Plusgiro\":\"%s\","),recContact.getPlusgiro()); cXml+=cTmp;
								cTmp.Format(_T("\"Bankacount\":\"%s\","),recContact.getBankkonto()); cXml+=cTmp;
								cTmp.Format(_T("\"SuplNr\":\"%s\""),recContact.getLevnummer()); cXml+=cTmp;

							}
						}										
					}
					cXml+=_T("}");	//Owner
				}
				cXml+=_T("]");	//Owners


			}			
			cXml+=_T("}"); //Property
		}
		cXml+=_T("]"); //Properties
		cXml+=_T("}");	//Object
		cXml+=_T("}");	//End


		cXml.Remove('\r');
		cXml.Replace('\n',' ');
		

		if(fileExists(cXmlFileName))
		{
			DeleteFile(cXmlFileName);
		}	// if (fileExists(sNameAndPath))

		//CStdioFile file;		
		// We can call Close() explicitly, but the destructor would have
		// also closed the file for us. Note that there's no need to
		// call the CloseHandle() on the handle returned by the API because
		// MFC will close it for us.		

		/*if (file.Open(cXmlFileName,CFile::modeCreate | CFile::modeWrite)) // | CStdioFileEx::modeWriteUnicode))
		{
			file.WriteString(cXml);
			file.Close();
		}*/
		CStdioFileEx file2;
		if (file2.Open(cXmlFileName,CFile::modeCreate | CFile::modeWrite)) // | CStdioFileEx::modeWriteUnicode))
		{
			file2.SetCodePage(CP_UTF8);
			file2.WriteString(cXml);
			file2.Close();
		}
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna exportmapp?"),_T("�ppna exportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}

	}

}

void CMDI1950ForrestNormFrame::Split1(const CString strIn, const CString delim, CStringArray &a) 
{
	int position = 0;
	CString strToken;

	a.RemoveAll();

	strToken = strIn.Tokenize(delim, position);
	a.Add(strToken);
	while(!strToken.IsEmpty()) {
		strToken = strIn.Tokenize(delim, position);
		a.Add(strToken);
	}
}


//HMS-76 20220125 J� Xml export av fastighetsdata
void CMDI1950ForrestNormFrame::OnExportPropertiesXmlTBtn(void)
{
	vecTransaction_elv_cruise vecELVObjectCruise;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;
	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;
	CTransaction_elv_properties_other_comp other_erst_rec;
	vecTransaction_elv_properties_other_comp vecOtherComp;
	vecTransaction_property_status m_vecPropStatus;

	vecTransactionPlot vecPlots;
	CTransaction_plot recPlot;
	vecTransactionSampleTree vecSampleTrees;
	CTransaction_sample_tree recSampleTree;
	vecTransactionDCLSTree vecDclsTrees;
	CTransaction_dcls_tree recDclsTree;
	int nNrTrees=0;
	CTransaction_elv_cruise recCruise;
	CTransaction_trakt recTrakt;

	CString cSavePath=_T("");
	CString cXml=_T("");
	CString cTmp=_T("");
	CString cTagFirst=_T("");
	CString cTagLast=_T("");
	CString cXmlFileName=_T("");
	int nContact=0;
	int nTypeOfOther=0;
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');

	

	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);
	m_pDB->getPropertyActionStatus(m_vecPropStatus);

	//�ppna mappv�ljare f�r att v�lja vart xml filerna skall sparas
	cSavePath=getRegisterExportXmlJsonDir();

	BOOL bRet = XBrowseForFolder(this->GetSafeHwnd(),
		cSavePath,
		szFolder,
		sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		cSavePath = szFolder;
		if(cSavePath.GetAt(cSavePath.GetLength()-1) != '\\')
			cSavePath += _T("\\");

	}
	else 
		return;

	setRegisterExportXmlJsonDir(cSavePath);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));

		cTmp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		cTmp.Replace(':',';');
		cXmlFileName.Format(_T("%s\\%s.xml"),cSavePath,cTmp);		

		cXml=_T("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>");
		cXml+=_T("<Object>");
		cTmp.Format(_T("<Name>%s</Name>"),pObj->getObjectName()); cXml+=cTmp;
		cTmp.Format(_T("<Id>%s</Id>"),pObj->getObjIDNumber()); cXml+=cTmp;
		cTmp.Format(_T("<Nr>%s</Nr>"),pObj->getObjErrandNum()); cXml+=cTmp;
		cTmp.Format(_T("<Littera>%s</Littera>"),pObj->getObjLittra()); cXml+=cTmp;
		//HMS-80 20220304 J� �ndrat fr�n getobjectdate till startdate
		//cTmp.Format(_T("<Date>%s</Date>"),pObj->getObjDate()); cXml+=cTmp;
		cTmp.Format(_T("<Date>%s</Date>"),pObj->getObjStartDate()); cXml+=cTmp;
		cTmp.Format(_T("<Performedby>%s</Performedby>"),pObj->getObjCreatedBy()); cXml+=cTmp;

		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];
			cXml+=_T("<Property>");

			cTmp.Format(_T("<Name>%s</Name>"),recProperty.getPropName()); cXml+=cTmp;
			cTmp.Format(_T("<Nr>%s</Nr>"),recProperty.getPropNumber()); cXml+=cTmp;
			cTmp.Format(_T("<Municipal>%s</Municipal>"),recProperty.getPropMunicipal()); cXml+=cTmp;
			cTmp.Format(_T("<County>%s</County>"),recProperty.getPropCounty()); cXml+=cTmp;

			for (UINT i1 = 0;i1 < m_vecPropStatus.size();i1++)
			{
				if (m_vecPropStatus[i1].getPropStatusID_pk() == recProperty.getPropStatus())
				{					
					cTmp.Format(_T("<Status>%s</Status>"),m_vecPropStatus[i1].getPropStatusName()); cXml+=cTmp;
				}
			}
			//cTmp.Format(_T("<Status>%s</Status>"),m_vecPropStatus[recProperty.getPropStatus()].getPropStatusName()); cXml+=cTmp;
			cTmp.Format(_T("<StatusId>%d</StatusId>"),recProperty.getPropStatus()); cXml+=cTmp;
			cTmp.Format(_T("<TypeOfAction>%d</TypeOfAction>"),recProperty.getPropTypeOfAction()); cXml+=cTmp;

			cXml+=_T("<Evaluation>");
			cTmp.Format(_T("<Landvalue>%.0f</Landvalue>"),floor(recProperty.getPropLandValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Rotvalue>%.0f</Rotvalue>"),floor(recProperty.getPropRotpost()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Woodvalue>%.0f</Woodvalue>"),floor(recProperty.getPropWoodValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Earlycutvalue>%.0f</Earlycutvalue>"),floor(recProperty.getPropEarlyCutValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Highcutvalue>%.0f</Highcutvalue>"),floor(recProperty.getPropHighCutValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Stormdryvalue>%.0f</Stormdryvalue>"),floor(recProperty.getPropStromDryValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Bordertreevalue>%.0f</Bordertreevalue>"),floor(recProperty.getPropRandTreesValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Voluntaryvalue>%.0f</Voluntaryvalue>"),floor(recProperty.getPropVoluntaryDealValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Costvalue>%.0f</Costvalue>"),floor(recProperty.getPropCostValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Volume>%.2f</Volume>"),floor(100.0*(recProperty.getPropM3Sk()+0.005))/100.0); cXml+=cTmp;
						

			cTmp.Format(_T("<Area>%.2f</Area>"),recProperty.getPropAreal()); cXml+=cTmp;
			cTmp.Format(_T("<Numtrees>%d</Numtrees>"),recProperty.getPropNumOfTrees()); cXml+=cTmp;				

			cTmp.Format(_T("<Forestfuelvalue>%.0f</Forestfuelvalue>"),floor(recProperty.getPropGrotValue()+0.5)); cXml+=cTmp;
			cTmp.Format(_T("<Forestfueltons>%.3f</Forestfueltons>"),floor(1000.0*(recProperty.getPropGrotVolume()+0.0005))/1000.0); cXml+=cTmp;
			cTmp.Format(_T("<Forestfuelcost>%.0f</Forestfuelcost>"),floor(recProperty.getPropGrotCost()+0.5)); cXml+=cTmp;


			cTmp.Format(_T("<Groupid>%s</Groupid>"),recProperty.getPropGroupID()); cXml+=cTmp;

			cXml+=_T("<Other>");
			cTmp.Format(_T("<Othertotal>%.0f</Othertotal>"),floor(recProperty.getPropOtherCostValue()+0.5)); cXml+=cTmp;
			//H�mta annan ers�ttning 
			m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),recProperty.getPropID_pk(),vecOtherComp);

			if (vecOtherComp.size() > 0)
			{
				for (UINT i = 0;i < vecOtherComp.size();i++)
				{

					other_erst_rec = vecOtherComp[i];
					nTypeOfOther=other_erst_rec.getPropOtherCompTypeOf();

					switch(nTypeOfOther)
					{
					case 0:	//Fast ers
						cTagFirst=_T("<Fixed>");
						cTagLast=_T("</Fixed>");
						break;
					case 1:	//Fast ers + moms
						cTagFirst=_T("<FixedVat>");
						cTagLast=_T("</FixedVat>");
						break;
					case 2:	//Intr�ngsers�ttning
						cTagFirst=_T("<Intrusion>");
						cTagLast=_T("</Intrusion>");
						break;
					case 3:	//Kr per m3 gagnvirke
						cTagFirst=_T("<Merchwood>");
						cTagLast=_T("</Merchwood>");
						break;
					case 4:	//3. Hinder i �kermark
						cTagFirst=_T("<Fieldobstacle>");
						cTagLast=_T("</Fieldobstacle>");								
						break;
					case 5:	//4a. Ledning i skogsmark 							
						cTagFirst=_T("<Forestline>");
						cTagLast=_T("</Forestline>");	
						break;
					case 6:	//5. �vrigt intr�ng
						cTagFirst=_T("<Otherintrusion>");
						cTagLast=_T("</Otherintrusion>");
						break;
					}

					cXml+=cTagFirst;
					cTmp.Format(_T("<Description>%s</Description>"),other_erst_rec.getPropOtherCompNote()); cXml+=cTmp;
					cTmp.Format(_T("<Value>%.2f</Value>"),floor(100.0*(other_erst_rec.getPropOtherCompValue_calc()+0.005))/100.0); cXml+=cTmp;


					cTmp.Format(_T("<Id>%d</Id>"),nTypeOfOther+1); cXml+=cTmp;
					cXml+=cTagLast;
				}
			}
			cXml+=_T("</Other>");
			cXml+=_T("</Evaluation>");

			// HMS-124 mer info, v�rderingsbest�nd
			vecTransaction_eval_evaluation vecEvals;
			CTransaction_eval_evaluation	recEval;
			m_pDB->getObjectEvaluation(recProperty.getPropID_pk(),recProperty.getPropObjectID_pk(),vecEvals);
			if (vecEvals.size() > 0)
			{
				cXml+=_T("<Evaluationstands>");
				for (UINT e = 0;e < vecEvals.size();e++)
				{
					cXml+=_T("<Evaluationstand>");
					// V�rderingsbest�nd namn
					cTmp.Format(_T("<Name>%s</Name>"),recEval.getEValName()); cXml+=cTmp;

					// V�rderingsbest�nd id
					cTmp.Format(_T("<Id>%d</Id>"),recEval.getEValID_pk()); cXml+=cTmp;

					// Fastighetsid
					cTmp.Format(_T("<Propertyid>%d</Propertyid>"),recCruise.getECruPropID_pk()); cXml+=cTmp;

					// V�rderingsbest�nd areal
					cTmp.Format(_T("<Areal>%.3f</Areal>"),recEval.getEValAreal()); cTmp.Replace(',','.'); 

					// V�rderingsbest�nd �lder
					cTmp.Format(_T("<Age>%d</Age>"),recEval.getEValAge()); cXml+=cTmp;

					// V�rderingsbest�nd SI
					cTmp.Format(_T("<Si>%s</Si>"),recEval.getEValSIH100()); cXml+=cTmp;

					// V�rderingsbest�nd Korrfakt
					cTmp.Format(_T("<Corrfactor>%.1f</Corrfactor>"),recEval.getEValCorrFactor()); cTmp.Replace(',','.'); 

					// V�rderingsbest�nd Markv�rde
					cTmp.Format(_T("<Landvalue>%.3f</Landvalue>"),recEval.getEValLandValue()); cTmp.Replace(',','.'); 

					// V�rderingsbest�nd Markv�rde per ha
					cTmp.Format(_T("<Landvalueha>%.3f</Landvalueha>"),recEval.getEValLandValue_ha()); cTmp.Replace(',','.'); 

					// V�rderingsbest�nd F�rtidig
					cTmp.Format(_T("<Precut>%.3f</Precut>"),recEval.getEValPreCut()); cTmp.Replace(',','.'); 

					// V�rderingsbest�nd F�rtidig per ha
					cTmp.Format(_T("<Precutha>%.3f</Precutha>"),recEval.getEValPreCut_ha()); cTmp.Replace(',','.'); 

					// V�rderingsbest�nd Tallandel
					cTmp.Format(_T("<Pinepart>%.0f</Pinepart>"),recEval.getEValPinePart()); cXml+=cTmp;

					// V�rderingsbest�nd Granandel
					cTmp.Format(_T("<Sprucepart>%.0f</Sprucepart>"),recEval.getEValSprucePart()); cXml+=cTmp;

					// V�rderingsbest�nd Bj�rkandel
					cTmp.Format(_T("<Birchpart>%.0f</Birchpart>"),recEval.getEValBirchPart()); cXml+=cTmp;
					cXml+=_T("</Evaluationstand>");
				}
				cXml+=_T("</Evaluationstands>");
			}

			// HMS-120 Lista best�nd med info och koordinater			
			// HMS-124 mer info stmpll�ngd, tr�d, ytor
			m_pDB->getObjectCruises(recProperty.getPropID_pk(),recProperty.getPropObjectID_pk(),vecELVObjectCruise);
			if (vecELVObjectCruise.size() > 0)
			{
				cXml+=_T("<Trakts>");

				for (UINT i = 0;i < vecELVObjectCruise.size();i++)
				{
					cXml+=_T("<Trakt>");

					recCruise = vecELVObjectCruise[i];					
					//H�mta upp best�ndet fr�n trakt table f�r koordinater
					m_pDB->getTrakt(recCruise.getECruID_pk(),recTrakt);

				
					//Best�ndsnamn
					cTmp.Format(_T("<Name>%s</Name>"),recCruise.getECruName()); cXml+=cTmp;

					//Best�ndsid
					cTmp.Format(_T("<Id>%d</Id>"),recCruise.getECruID_pk()); cXml+=cTmp;

					//Fastighetsid
					cTmp.Format(_T("<Propertyid>%d</Propertyid>"),recCruise.getECruPropID_pk()); cXml+=cTmp;

					// Best�nd SI
					cTmp.Format(_T("<Si>%s</Si>"),recTrakt.getTraktSIH100()); cXml+=cTmp;

					// Best�nd �lder
					cTmp.Format(_T("<Age>%d</Age>"),recTrakt.getTraktAge()); cXml+=cTmp;

					// Best�nd Areal
					cTmp.Format(_T("<Areal>%.3f</Areal>"),recTrakt.getTraktAreal()); cXml+=cTmp;

					//Best�ndpunkt,koordinat
					//cTmp.Format(_T("<Point>\"%s\""),recTrakt.getTraktPoint()); cXml+=cTmp;
					cTmp.Format(_T("<Coordinate>"));	cXml+=cTmp;
					cTmp.Format(_T("%s"),recTrakt.getTraktPoint()); cXml+=cTmp;
					cTmp.Format(_T("</Coordinate>"));	cXml+=cTmp;
				

					//Best�ndskoordinater
					//cTmp.Format(_T("<Coordinates>\"%s\","),recTrakt.getTraktCoordinates()); cXml+=cTmp;
					cTmp.Format(_T("<Coordinates>"));	cXml+=cTmp;					
					cTmp.Format(_T("%s"),recTrakt.getTraktCoordinates()); 
					cTmp.Format(_T("</Coordinates>"));	cXml+=cTmp;					
					
					// St�mplingsl�ngd tr�d i gatan
					// HMS-124 mer info			
					nNrTrees=0;
					vecDclsTrees.clear();
					m_pDB->getDCLSTreesforJsonExport(recCruise.getECruID_pk(),vecDclsTrees);

					if(vecDclsTrees.size()>0)
					{
						cXml+=_T("<TreeTable>");
						for (UINT d = 0;d < vecDclsTrees.size();d++)
						{
							recDclsTree=vecDclsTrees[d];
							cXml+=_T("<Dcls>"); //Dcls

							cTmp.Format(_T("<Speciesname>%s</Speciesname>"),recDclsTree.getSpcName()); cXml+=cTmp;

							cTmp.Format(_T("<Speciesid>%d</Speciesid>"),recDclsTree.getSpcID()); cXml+=cTmp;

							cTmp.Format(_T("<From>%.0f</From>"),recDclsTree.getDCLS_from()); cXml+=cTmp;

							cTmp.Format(_T("<Numtrees>%d</Numtrees>"),recDclsTree.getNumOf()-recDclsTree.getNumOfRandTrees()); cXml+=cTmp;

							cXml+=_T("</Dcls>"); //Dcls
							nNrTrees++;
						}
						cXml+=_T("</TreeTable>");
					}							


					// St�mplingsl�ngd kanttr�d
					// HMS-124 mer info			
					if(vecDclsTrees.size()>0)
					{
						nNrTrees=0;
						cXml+=_T("<TreeHazardTable>");
						for (UINT d = 0;d < vecDclsTrees.size();d++)
						{
							recDclsTree=vecDclsTrees[d];							

							cXml+=_T("<Dcls>"); //Dcls

							cTmp.Format(_T("<Speciesname>%s</Speciesname>"),recDclsTree.getSpcName()); cXml+=cTmp;

							cTmp.Format(_T("<Speciesid>%d</Speciesid>"),recDclsTree.getSpcID()); cXml+=cTmp;

							cTmp.Format(_T("<From>%.0f</From>"),recDclsTree.getDCLS_from()); cXml+=cTmp;

							cTmp.Format(_T("<Numtrees>%d</Numtrees>"),recDclsTree.getNumOfRandTrees()); cXml+=cTmp;

							cXml+=_T("</Dcls>"); //Dcls
							nNrTrees++;
						}
						cXml+=_T("</TreeHazardTable>");
					}


					// Ytor
					// HMS-124 mer info, ytor
					int nPlotnum=0;
					vecPlots.clear();
					m_pDB->getPlots(recCruise.getECruID_pk(),vecPlots);
					if (vecPlots.size() > 0)
					{
						cXml+=_T("<Plots>");

						for (UINT p = 0;p < vecPlots.size();p++)
						{
							recPlot= vecPlots[p];
							cXml+=_T("<Plot>");

							//Fastighetsid
							cTmp.Format(_T("<Propertyid>%d</Propertyid>"),recCruise.getECruPropID_pk()); cXml+=cTmp;

							//Best�ndsid
							cTmp.Format(_T("<Traktid>%d</Traktid>"),recPlot.getTraktID()); cXml+=cTmp;

							//Ytid
							cTmp.Format(_T("<Id>%d</Id>"),recPlot.getPlotID()); cXml+=cTmp;

							//Ytnamn
							cTmp.Format(_T("<Name>%s</Name>"),recPlot.getPlotName()); cXml+=cTmp;

							//Ytarea
							cTmp.Format(_T("<Area>%.0f</Area>"),recPlot.getArea()); cXml+=cTmp;

							//Ytpunkt,koordinat
							//cTmp.Format(_T("<Point>\"%s\""),recPlot.getCoord()); cXml+=cTmp;
							cTmp.Format(_T("<Coordinate>"));	cXml+=cTmp;
							cTmp.Format(_T("%s"),recPlot.getCoord()); cXml+=cTmp;
							cTmp.Format(_T("</Coordinate>"));	cXml+=cTmp;



							cXml+=_T("</Plot>"); //Plot
							nPlotnum++;
						}
						cXml+=_T("</Plots>"); //Plots
					}

					// Tr�d
					// HMS-124 mer info
					nNrTrees=0;
					vecSampleTrees.clear();
					m_pDB->getLandValueTrees(recCruise.getECruID_pk(),-1,vecSampleTrees);
					//Skriv ut tr�d med koordinat
					if(vecSampleTrees.size()>0)
					{
						cXml+=_T("<Trees>");
						for (UINT t = 0;t < vecSampleTrees.size();t++)
						{
							recSampleTree=vecSampleTrees[t];
							if(recSampleTree.getCoord().GetLength()>2)
							{
								cXml+=_T("<Tree>"); //Tree

								//Fastighetsid
								cTmp.Format(_T("<Propertyid>%d</Propertyid>"),recCruise.getECruPropID_pk()); cXml+=cTmp;

								//Best�ndsid
								cTmp.Format(_T("<Traktid>%d</Traktid>"),recSampleTree.getTraktID()); cXml+=cTmp;

								//Ytid
								cTmp.Format(_T("<Plotid>%d</Plotid>"),recSampleTree.getPlotID()); cXml+=cTmp;

								//Tr�did
								cTmp.Format(_T("<Treeid>%d</Treeid>"),recSampleTree.getTreeID()); cXml+=cTmp;

								//Spec id
								cTmp.Format(_T("<Specid>%d</Specid>"),recSampleTree.getSpcID()); cXml+=cTmp;

								//Spec name
								cTmp.Format(_T("<Specname>%s</Specname>"),recSampleTree.getSpcName()); cXml+=cTmp;

								//Koordinat
								//cTmp.Format(_T("<Point>\"%s\","),recSampleTree.getCoord()); cXml+=cTmp;
								cTmp.Format(_T("<Coordinate>"));	cXml+=cTmp;
								cTmp.Format(_T("%s"),recSampleTree.getCoord());cXml+=cTmp;
								cTmp.Format(_T("</Coordinate>"));	cXml+=cTmp;

								//Dbh
								cTmp.Format(_T("<Dbh>%.0f</Dbh>"),recSampleTree.getDBH()); cXml+=cTmp;

								//H�jd
								cTmp.Format(_T("<Hgt>%.0f</Hgt>"),recSampleTree.getHgt()); cXml+=cTmp;

								//Typ
								cTmp.Format(_T("<Type>%d</Type>"),recSampleTree.getTreeType()); cXml+=cTmp;

								//�lder
								cTmp.Format(_T("<Age>%d</Age>"),recSampleTree.getAge()); cXml+=cTmp;

								//Bonitet
								cTmp.Format(_T("<Si>%s</Si>"),recSampleTree.getBonitet()); cXml+=cTmp;

								//m3sk
								cTmp.Format(_T("<M3sk>%.3f</M3sk>"),recSampleTree.getM3Sk()); cTmp.Replace(',','.'); 

								//m3ub
								cTmp.Format(_T("<M3ub>%.3f</M3ub>"),recSampleTree.getM3Ub()); cTmp.Replace(',','.'); 

								//m3fub
								cTmp.Format(_T("<M3fub>%.3f</M3fub>"),recSampleTree.getM3Fub()); cTmp.Replace(',','.'); cXml+=cTmp;

								cXml+=_T("</Tree>"); //Tree
								nNrTrees++;
							}
						}
						cXml+=_T("</Trees>");	//Trees						
					}			
					cXml+=_T("</Trakt>");
				}

				cXml+=_T("</Trakts>");
			}

			
			// H�nta kontakter och �garinfo
			m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
			m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

			if (vecContacts.size() > 0)
			{
				//Loopa och skriv ut alla kontakter
				for (UINT i = 0;i < vecContacts.size();i++)
				{
					cXml+=_T("<Owner>");
					recContact = vecContacts[i];
					//Skriv bara ut 7 kontakter
					if (vecPropertyOwners.size() > 0)
					{								
						for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
						{											
							recPropertyOwner = vecPropertyOwners[i1];									
							if (recPropertyOwner.getContactID() == recContact.getID())
							{
								nContact=0;
								if(recPropertyOwner.getIsContact() == 1)
									nContact=1;
								cTmp.Format(_T("<Contact>%d</Contact>"),nContact); cXml+=cTmp;
								cTmp.Format(_T("<Name>%s</Name>"),recContact.getName()); cXml+=cTmp;
								cTmp.Format(_T("<Adress>%s</Adress>"),recContact.getAddress()); cXml+=cTmp;
								cTmp.Format(_T("<Zip>%s</Zip>"),recContact.getPostNum()); cXml+=cTmp;
								cTmp.Format(_T("<City>%s</City>"),recContact.getPostAddress()); cXml+=cTmp;
								cTmp.Format(_T("<Phonehome>%s</Phonehome>"),recContact.getPhoneHome()); cXml+=cTmp;
								cTmp.Format(_T("<Phonework>%s</Phonework>"),recContact.getPhoneWork()); cXml+=cTmp;
								cTmp.Format(_T("<Email>%s</Email>"),recContact.getEMail()); cXml+=cTmp;
								cTmp.Format(_T("<Share>%s</Share>"),FormatShareToDecimal(recContact.getOwnerShare())); cXml+=cTmp;
								cTmp.Format(_T("<Sharetext>%s</Sharetext>"),recContact.getOwnerShare()); cXml+=cTmp;


								cTmp.Format(_T("<Orgnr>%s</Orgnr>"),recContact.getPNR_ORGNR()); cXml+=cTmp;
								cTmp.Format(_T("<COAdress>%s</COAdress>"),recContact.getCoAddress()); cXml+=cTmp;
								cTmp.Format(_T("<Vat>%s</Vat>"),recContact.getVATNumber()); cXml+=cTmp;
								cTmp.Format(_T("<Mobilephone>%s</Mobilephone>"),recContact.getMobile()); cXml+=cTmp;
								cTmp.Format(_T("<Bankgiro>%s</Bankgiro>"),recContact.getBankgiro()); cXml+=cTmp;
								cTmp.Format(_T("<Plusgiro>%s</Plusgiro>"),recContact.getPlusgiro()); cXml+=cTmp;
								cTmp.Format(_T("<Bankaccount>%s</Bankaccount>"),recContact.getBankkonto()); cXml+=cTmp;
								cTmp.Format(_T("<SuplNr>%s</SuplNr>"),recContact.getLevnummer()); cXml+=cTmp;

							}
						}										
					}
					cXml+=_T("</Owner>");
				}
			}			
			cXml+=_T("</Property>");
		}
		cXml+=_T("</Object>");

		if(fileExists(cXmlFileName))
		{
			DeleteFile(cXmlFileName);
		}	// if (fileExists(sNameAndPath))

		
		// We can call Close() explicitly, but the destructor would have
		// also closed the file for us. Note that there's no need to
		// call the CloseHandle() on the handle returned by the API because
		// MFC will close it for us.
		/*CStdioFile file;
		if (file.Open(cXmlFileName,CFile::modeCreate | CFile::modeWrite | CStdioFileEx::normal)) // | CStdioFileEx::modeWriteUnicode))
		{
			file.WriteString(cXml);
			file.Close();
		}*/
		CStdioFileEx file2;
		if (file2.Open(cXmlFileName,CFile::modeCreate | CFile::modeWrite )) // | CStdioFileEx::modeWriteUnicode))
		{
			file2.WriteString(cXml);
			file2.Close();
		}
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna exportmapp?"),_T("�ppna exportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}

}

//-----------------------------------------------------------------------------------------------------------------------------------------
// Kontrollera version av V�rderingsprotokoll excel fil
//-----------------------------------------------------------------------------------------------------------------------------------------

int CMDI1950ForrestNormFrame::CheckVersionOfExcelVp(void)
{
	int nVersion=VP_EXCELVERSION_20230125;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cVersion=_T("");
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_VP);
	const TCHAR* tFileName = (LPCTSTR) cReportFileName;
	Book* xlsxBook = xlCreateXMLBook();
	if(xlsxBook)
	{
		if(xlsxBook->load(tFileName))
		{
			//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
			xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
			Sheet* sheet3 = xlsxBook->getSheet(0);
			if(sheet3) 
			{   								
				// H�mta upp versionsnummer
				cVersion=sheet3->readStr(2,8);
				//HMS-98 Ny VP version, samma layout som 20220113
				if(cVersion.Find(_T("2023"))>0)
				{
							nVersion=VP_EXCELVERSION_20230125;
				}
				else
				{
					if(cVersion.Find(_T("2022"))>0)
					{
						if(cVersion.Find(_T("2022.01.19"))>0)
							nVersion=VP_EXCELVERSION_20220119;
						if(cVersion.Find(_T("2022.01.13"))>0)
							nVersion=VP_EXCELVERSION_20220113;
					}
					else
					{
						if(cVersion.Find(_T("2021"))>0)
							nVersion=VP_EXCELVERSION_2021;				
					}
				}
			}
		}
	}
	return nVersion;
}


int CMDI1950ForrestNormFrame::CheckVersionOfExcelErsErbj(void)
{
	int nVersion=ERSERBJ_EXCELVERSION_2023;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cVersion=_T("");
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_ERS_ERBJ);
	const TCHAR* tFileName = (LPCTSTR) cReportFileName;
	Book* xlsxBook = xlCreateXMLBook();
	if(xlsxBook)
	{
		if(xlsxBook->load(tFileName))
		{
			//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
			xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
			Sheet* sheet3 = xlsxBook->getSheet(0);
			if(sheet3) 
			{   								
				// H�mta upp versionsnummer
				cVersion=sheet3->readStr(1,7);
				//HMS-98 Ny VP version, samma layout som 20220113
				if(cVersion.Find(_T("2019"))>0)
				{
					nVersion=ERSERBJ_EXCELVERSION_2019;
				}
			}
		}
		cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_ERS_ERBJ_XLTX);
		tFileName = (LPCTSTR) cReportFileName;
		if(xlsxBook->load(tFileName))
		{
			//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
			xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
			Sheet* sheet3 = xlsxBook->getSheet(0);
			if(sheet3) 
			{   								
				cVersion=sheet3->readStr(1,8);
				if(cVersion.Find(_T("2023"))>0)
				{
					nVersion=ERSERBJ_EXCELVERSION_2023;
				}

			}
		}

	}
	return nVersion;
}
void CMDI1950ForrestNormFrame::OnExportPropertiesExcelrapportErsErbjTBtn(void)
{
	int nVersion=CheckVersionOfExcelErsErbj();
	ExportPropertiesExcelrapportErsErbj(nVersion);
}

void CMDI1950ForrestNormFrame::OnExportPropertiesExcelrapportVpTBtn(void)
{
	int nVersion=CheckVersionOfExcelVp();
	switch(nVersion)
	{
	case VP_EXCELVERSION_2020:
		ExportPropertiesExcelrapportVp_2020();
		break;
	case VP_EXCELVERSION_2021:
		ExportPropertiesExcelrapportVp_2021();	
		break;
	case VP_EXCELVERSION_20220113:	// Energif�retagens version
		ExportPropertiesExcelrapportVp_2022_01_13();	
		break;
	case VP_EXCELVERSION_20220119:	// Karins eon version
		ExportPropertiesExcelrapportVp_2022_01_19();	
		break;
		//HMS-98 Ny VP version, samma layout som 20220113
	case VP_EXCELVERSION_20230125:
		ExportPropertiesExcelrapportVp_2022_01_13();	
		break;
	}
}

//HMS-135 Export till ellevio excel, per mark�gare
void CMDI1950ForrestNormFrame::ExportPropertiesExcelrapportEllevio2025_01_22(void)
{
	int nRow=0,nTypeOfOther=0,nCountOwners=0,nAskToOpenFolder=0;
	//CFileDialog dlg(TRUE,_T("*.xlsx"),NULL,OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,_T("EXCEL (*.xls)|*.xls|EXCEL2 (*.xlsx)|*.xlsx||"),this);
	CString cErr=_T("");
	CString cTemp=_T("");
	CString cExcPropFileName=_T("");
	CString cKontaktAdress=_T("");
	CString cName=_T("");
	CString cAdress=_T("");
	CString cVersion=_T("");
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;

	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;

	vecTransaction_elv_properties_other_comp vecOtherComp;
	CTransaction_elv_properties_other_comp other_erst_rec;
	BOOL bExistsDir = FALSE;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cSavePath=_T("");
	CString sObjMapp=_T("");
	
	sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
	sObjMapp = checkDirectoryName(sObjMapp);


	cSavePath.Format(_T("%s\\%s"),getObjectDirectory(sObjMapp),DIRECTORYNAME_EXC_REPORTS);
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_ELLEVIO);
	

	double fErs�ttningHinder�ker=0.0;
	CString cErs�ttningHinder�ker=_T("");
	int nRowErs�ttningHinder�ker=24;

	double fErs�ttningLedningIskogsmark=0.0;
	CString cErs�ttningLedningIskogsmark=_T("");
	int nRowErs�ttningLedningIskogsmark=29;
	
	double fErs�ttning�vrig=0.0,dGrotErs=0.0;
	CString cErs�ttning�vrig=_T("");
	int nRowErs�ttning�vrig=37;
	int nPrintedOwnerAtHeader=0;


	const TCHAR* tFileName = (LPCTSTR) cReportFileName;
	const TCHAR* tSheetName = _T("");
	CString cAndel=_T("");
	CString cValue=_T("");
	

	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];

			// H�nta kontakter och �garinfo
			m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
			m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

			if (vecContacts.size() > 0)
			{
				//Loopa och skriv ut alla kontakter
				for (UINT i = 0;i < vecContacts.size();i++)
				{
					recContact = vecContacts[i];
					//Skriv bara ut 7 kontakter
					if (vecPropertyOwners.size() > 0 && nCountOwners<=7)
					{								
						for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
						{											
							recPropertyOwner = vecPropertyOwners[i1];									
							if (recPropertyOwner.getContactID() == recContact.getID())
							{
								cTemp.Format(_T("%s"),recProperty.getPropName());
								cTemp.Replace(':',';');
								cExcPropFileName.Format(_T("%s\\%s-%s.xlsm"),cSavePath,cTemp,recContact.getName());		

								Book* xlsxBook = xlCreateXMLBook();
								if(xlsxBook)
								{
									if(xlsxBook->load(tFileName))
									{
										//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
										xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
										nAskToOpenFolder=1;


										int nNumSheets=xlsxBook->sheetCount();
										int nIndex=0;
										for(nIndex=0;nIndex<nNumSheets;nIndex++)
										{
											tSheetName=xlsxBook->getSheetName(nIndex);
											if(_tcscmp(tSheetName,_T("Skog"))==0)
											{
												// Skriv ut v�rden

												Sheet* sheet1 = xlsxBook->getSheet(nIndex);
												// Markv�rde
												cValue.Format(_T("%.0f"),floor(recProperty.getPropLandValue()+0.5));
												sheet1->writeStr(9,1,cValue);
												// F�rtidig
												cValue.Format(_T("%.0f"),floor(recProperty.getPropEarlyCutValue()+0.5));
												sheet1->writeStr(11,1,cValue);
												// Kanttr�d
												cValue.Format(_T("%.0f"),floor(recProperty.getPropRandTreesValue()+0.5));
												sheet1->writeStr(13,1,cValue);
												// Storm o tork
												cValue.Format(_T("%.0f"),floor(recProperty.getPropStromDryValue()+0.5));
												sheet1->writeStr(15,1,cValue);
												// F�rdyrad
												cValue.Format(_T("%.0f"),floor(recProperty.getPropHighCutValue()+0.5));
												sheet1->writeStr(17,1,cValue);
											}
											else
											{
												if(_tcscmp(tSheetName,_T("Sammandrag"))==0)
												{
													Sheet* sheet2 = xlsxBook->getSheet(nIndex);

													// Objektsinformation
												
													// Skriv ut �garinformation
													// V�rderare = utf�rd av																										
													sheet2->writeStr(9,4,pObj->getObjCreatedBy());
													// Ledning = Littera
													sheet2->writeStr(10,4,pObj->getObjLittra());
													//Projektnr = Id
													sheet2->writeStr(11,4,pObj->getObjIDNumber());
													//Koncessionsnr = �rendenummer
													//sheet2->writeStr(12,8,pObj->getObjErrandNum());

													// Fastighet
													sheet2->writeStr(2,4,recProperty.getPropName());
												
													cName.Format(_T("%s"),recContact.getName());
													cAdress.Format(_T(",%s,%s %s"),recContact.getAddress(),recContact.getPostNum(),recContact.getPostAddress());
													cKontaktAdress.Format(_T("%s%s"),cName,cAdress);
													
													// �gare
													sheet2->writeStr(3,4,cName);
													// Adress
													sheet2->writeStr(4,4,recContact.getAddress());
													// Postnummer
													sheet2->writeStr(6,4,recContact.getPostNum());
													// Postort
													sheet2->writeStr(6,13,recContact.getPostAddress());
													
													// Andel
													cAndel.Format(_T("%.1f%%"),FormatShareToPercent(recContact.getOwnerShare()));
													sheet2->writeStr(8,4,cAndel);
													
													
												}
											}
										}


										//Check if directory exists
										bExistsDir = isDirectory(cSavePath);
										if(!bExistsDir)
										{
											createDirectory(cSavePath);						
										}
										xlsxBook->save((LPCTSTR)cExcPropFileName);
									}
								}
								xlsxBook->release();
								//Fr�ga om �ppna mapp med rapporter i utforskaren						

							}
						}
					}
				}
			}
		}
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}

	if(nAskToOpenFolder==1)
	{
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna rapportmapp?"),_T("�ppna rapportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}

}


// V�rderingsprotokoll
// Energif�retagens version utan eon text
void CMDI1950ForrestNormFrame::ExportPropertiesExcelrapportVp_2022_01_13(void)
{
	int nRow=0,nTypeOfOther=0,nCountOwners=0,nAskToOpenFolder=0;
	//CFileDialog dlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,L"EXCEL *.xls;*.xlsx",this);
	CFileDialog dlg(TRUE,_T("*.xlsx"),NULL,OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,_T("EXCEL (*.xls)|*.xls|EXCEL2 (*.xlsx)|*.xlsx||"),this);
	CString cErr=_T("");
	CString cTemp=_T("");
	CString cExcPropFileName=_T("");
	CString cKontaktAdress=_T("");
	CString cName=_T("");
	CString cAdress=_T("");
	CString cVersion=_T("");
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;

	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;

	vecTransaction_elv_properties_other_comp vecOtherComp;
	CTransaction_elv_properties_other_comp other_erst_rec;
	BOOL bExistsDir = FALSE;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cSavePath=_T("");
	CString sObjMapp=_T("");
	
	sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
	sObjMapp = checkDirectoryName(sObjMapp);


	cSavePath.Format(_T("%s\\%s"),getObjectDirectory(sObjMapp),DIRECTORYNAME_EXC_REPORTS);
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_VP);
	

	double fErs�ttningHinder�ker=0.0;
	CString cErs�ttningHinder�ker=_T("");
	int nRowErs�ttningHinder�ker=24;

	double fErs�ttningLedningIskogsmark=0.0;
	CString cErs�ttningLedningIskogsmark=_T("");
	int nRowErs�ttningLedningIskogsmark=29;
	
	double fErs�ttning�vrig=0.0,dGrotErs=0.0;
	CString cErs�ttning�vrig=_T("");
	int nRowErs�ttning�vrig=37;
	int nPrintedOwnerAtHeader=0;


	const TCHAR* tFileName = (LPCTSTR) cReportFileName;



	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];
			cTemp.Format(_T("%s"),recProperty.getPropName());
			cTemp.Replace(':',';');
			cExcPropFileName.Format(_T("%s\\%s-Vp.xlsx"),cSavePath,cTemp);		

			Book* xlsxBook = xlCreateXMLBook();
			if(xlsxBook)
			{
				if(xlsxBook->load(tFileName))
				{
					//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
					xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
					nAskToOpenFolder=1;
					//-----------------------------------------------------------------------------------------------------------------------------------------
					// V�rderingsprotokoll
					//-----------------------------------------------------------------------------------------------------------------------------------------
					Sheet* sheet3 = xlsxBook->getSheet(0);
					if(sheet3) 
					{   								
						// H�mta upp versionsnummer
						cVersion=sheet3->readStr(2,8);
						// G�r n�gon sorts versionskontroll ???

						// Projektnummer= ObjId	
						// HMS-65 20201123 Korrigerar till att visa �rendenummer
						//sheet3->writeStr(3,8,pObj->getObjErrandNum());
						// HMS-76 20220209 Korrigerar till att visa littera J�
						sheet3->writeStr(3,8,pObj->getObjLittra());
						// Ledning= Objnamn
						sheet3->writeStr(4,8,pObj->getObjectName());
						// Koncessionsl�pnr=littera
						//sheet3->writeStr(5,8,pObj->getObjLittra());

						// Fastighetsbeteckning
						sheet3->writeStr(3,3,recProperty.getPropName());
						// Kommun
						sheet3->writeStr(4,3,recProperty.getPropMunicipal());
						// Fastighetsnummer
						sheet3->writeStr(5,3,recProperty.getPropNumber());

						// Ers�tting Rotnetto HMS-115
						if(pObj->getObjIsGrotIncluded()==1)
							dGrotErs=recProperty.getPropGrotValue()-recProperty.getPropGrotCost();
						else
							dGrotErs=0.0;

						sheet3->writeNum(34,5,floor(recProperty.getPropRotpost()+dGrotErs+0.5));
					}

					fErs�ttningLedningIskogsmark=recProperty.getPropLandValue()+recProperty.getPropEarlyCutValue()+recProperty.getPropRandTreesValue()+recProperty.getPropStromDryValue();


					nRowErs�ttningHinder�ker=24;
					nRowErs�ttningLedningIskogsmark=29;
					nRowErs�ttning�vrig=37;
					// Ers�ttning 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,_T("Ledning i skogsmark"));
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));		
						nRowErs�ttningLedningIskogsmark++;
					}
					
					fErs�ttningLedningIskogsmark=0.0;
					cErs�ttningLedningIskogsmark=_T("");
					
					fErs�ttningHinder�ker=0.0;
					cErs�ttningHinder�ker=_T("");

					fErs�ttning�vrig=0.0;
					cErs�ttning�vrig=_T("");


					//H�mta annan ers�ttning av typ 
					// 4=ers�ttning f�r hinder i �kermark	(3)
					// 5=ers�ttning f�r ledning i skogsmark (4a)
					// 6=ers�ttning f�r �vrigt intr�ng		(5)
					m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),recProperty.getPropID_pk(),vecOtherComp);
				
					if (vecOtherComp.size() > 0)
					{
						for (UINT i = 0;i < vecOtherComp.size();i++)
						{
							
							other_erst_rec = vecOtherComp[i];
							nTypeOfOther=other_erst_rec.getPropOtherCompTypeOf();
							// 4=ers�ttning f�r hinder i �kermark	(3)
							if(nTypeOfOther==4)
							{
																//HMS-58 20200630 J�
								if(nRowErs�ttningHinder�ker>24)
								{
									fErs�ttningHinder�ker+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningHinder�ker.GetLength()<1)
										cErs�ttningHinder�ker=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningHinder�ker,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));
									nRowErs�ttningHinder�ker++;
								}
							}
							// 5=ers�ttning f�r ledning i skogsmark (4a)
							else if(nTypeOfOther==5)
							{

								//HMS-58 20200630 J�
								if(nRowErs�ttningLedningIskogsmark>29)
								{
									fErs�ttningLedningIskogsmark+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningLedningIskogsmark.GetLength()<1)
										cErs�ttningLedningIskogsmark=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		

									nRowErs�ttningLedningIskogsmark++;
								}


							}
							// 6=ers�ttning f�r �vrigt intr�ng		(5)
							else if(nTypeOfOther==6)
							{
								//HMS-58 20200630 J�
								if(nRowErs�ttning�vrig>37)
								{
									fErs�ttning�vrig+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttning�vrig.GetLength()<1)
										cErs�ttning�vrig=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttning�vrig,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttning�vrig,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		
									nRowErs�ttning�vrig++;
								}
							}
						}
					}

					//HMS-58 20200630 J� 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,cErs�ttningLedningIskogsmark);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));
					}
					if(fErs�ttningHinder�ker>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningHinder�ker,1,cErs�ttningHinder�ker);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(fErs�ttningHinder�ker+0.5));
					}
					if(fErs�ttning�vrig>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttning�vrig,1,cErs�ttning�vrig);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttning�vrig,9,floor(fErs�ttning�vrig+0.5));
					}


					// H�nta kontakter och �garinfo
					m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
					m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

					
					nRow=62;
					nCountOwners=0;
					nPrintedOwnerAtHeader=0;
					if (vecContacts.size() > 0)
					{
						//Loopa och skriv ut alla kontakter
						for (UINT i = 0;i < vecContacts.size();i++)
						{
							recContact = vecContacts[i];
							//Skriv bara ut 7 kontakter
							if (vecPropertyOwners.size() > 0 && nCountOwners<=7)
							{								
								for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
								{											
									recPropertyOwner = vecPropertyOwners[i1];									
									if (recPropertyOwner.getContactID() == recContact.getID())
									{
										nCountOwners+=1;
										// Skriv ut �gare som �r kontakt
										if(recPropertyOwner.getIsContact() == 1)
										{											
											if(nPrintedOwnerAtHeader==0)
											{
												cName.Format(_T("%s"),recContact.getName());
												cAdress.Format(_T(",%s,%s %s"),recContact.getAddress(),recContact.getPostNum(),recContact.getPostAddress());
												cKontaktAdress.Format(_T("%s%s"),cName,cAdress);
												//Kontaktperson & Adress
												sheet3->writeStr(6,3,cKontaktAdress);
												nPrintedOwnerAtHeader=1;
											}

										}
										//HMS-76 ut med personnummer 20220209 J�
										cName.Format(_T("%s %s"),recContact.getName(),recContact.getPNR_ORGNR());
										sheet3->writeStr(nRow,1,cName);
										//sheet3->writeStr(nRow,1,recContact.getName());
										// �gd Andel
										sheet3->writeStr(nRow,4,FormatShareToDecimal(recContact.getOwnerShare()));
										if(nCountOwners>1)	
											nRow+=5;
										else
											nRow+=7;
									}
								}										
							}
						}
					}
					
					//Check if directory exists
					bExistsDir = isDirectory(cSavePath);
					if(!bExistsDir)
					{
						createDirectory(cSavePath);						
					}
					xlsxBook->save((LPCTSTR)cExcPropFileName);
				}
			}
			xlsxBook->release();
			//Fr�ga om �ppna mapp med rapporter i utforskaren						
		}
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}

	if(nAskToOpenFolder==1)
	{
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna rapportmapp?"),_T("�ppna rapportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}


}


//HMS-76 20220204 J� Ny version av V�rderingsprotokoll
// Karins eon version av vp
void CMDI1950ForrestNormFrame::ExportPropertiesExcelrapportVp_2022_01_19(void)
{
	int nRow=0,nTypeOfOther=0,nCountOwners=0,nAskToOpenFolder=0;
	//CFileDialog dlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,L"EXCEL *.xls;*.xlsx",this);
	CFileDialog dlg(TRUE,_T("*.xlsx"),NULL,OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,_T("EXCEL (*.xls)|*.xls|EXCEL2 (*.xlsx)|*.xlsx||"),this);
	CString cErr=_T("");
	CString cTemp=_T("");
	CString cExcPropFileName=_T("");
	CString cKontaktAdress=_T("");
	CString cName=_T("");
	CString cAdress=_T("");
	CString cVersion=_T("");
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;

	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;

	vecTransaction_elv_properties_other_comp vecOtherComp;
	CTransaction_elv_properties_other_comp other_erst_rec;
	BOOL bExistsDir = FALSE;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cSavePath=_T("");
	CString sObjMapp=_T("");
	
	sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
	sObjMapp = checkDirectoryName(sObjMapp);


	cSavePath.Format(_T("%s\\%s"),getObjectDirectory(sObjMapp),DIRECTORYNAME_EXC_REPORTS);
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_VP);
	

	double fErs�ttningHinder�ker=0.0;
	CString cErs�ttningHinder�ker=_T("");
	int nRowErs�ttningHinder�ker=24;

	double fErs�ttningLedningIskogsmark=0.0;
	CString cErs�ttningLedningIskogsmark=_T("");
	int nRowErs�ttningLedningIskogsmark=29;
	
	double fErs�ttning�vrig=0.0,dGrotErs=0.0;
	CString cErs�ttning�vrig=_T("");
	int nRowErs�ttning�vrig=37;
	int nPrintedOwnerAtHeader=0;


	const TCHAR* tFileName = (LPCTSTR) cReportFileName;



	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];
			cTemp.Format(_T("%s"),recProperty.getPropName());
			cTemp.Replace(':',';');
			cExcPropFileName.Format(_T("%s\\%s-Vp.xlsx"),cSavePath,cTemp);		

			Book* xlsxBook = xlCreateXMLBook();
			if(xlsxBook)
			{
				if(xlsxBook->load(tFileName))
				{
					//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
					xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
					nAskToOpenFolder=1;
					//-----------------------------------------------------------------------------------------------------------------------------------------
					// V�rderingsprotokoll
					//-----------------------------------------------------------------------------------------------------------------------------------------
					Sheet* sheet3 = xlsxBook->getSheet(0);
					if(sheet3) 
					{   								
						// H�mta upp versionsnummer
						cVersion=sheet3->readStr(2,8);
						// G�r n�gon sorts versionskontroll ???

						// Projektnummer= ObjId	
						// HMS-65 20201123 Korrigerar till att visa �rendenummer
						//sheet3->writeStr(3,8,pObj->getObjErrandNum());
						// HMS-76 20220209 Korrigerar till att visa littera J�
						sheet3->writeStr(3,8,pObj->getObjLittra());
						// Ledning= Objnamn
						sheet3->writeStr(4,8,pObj->getObjectName());
						// Koncessionsl�pnr=littera
						//sheet3->writeStr(5,8,pObj->getObjLittra());

						// Fastighetsbeteckning
						sheet3->writeStr(3,3,recProperty.getPropName());
						// Kommun
						sheet3->writeStr(4,3,recProperty.getPropMunicipal());
						// Fastighetsnummer
						sheet3->writeStr(5,3,recProperty.getPropNumber());

						// Ers�tting Rotnetto HMS-115
						if(pObj->getObjIsGrotIncluded()==1)
							dGrotErs=recProperty.getPropGrotValue()-recProperty.getPropGrotCost();
						else
							dGrotErs=0.0;
						sheet3->writeNum(34,5,floor(recProperty.getPropRotpost()+dGrotErs+0.5));
					}

					fErs�ttningLedningIskogsmark=recProperty.getPropLandValue()+recProperty.getPropEarlyCutValue()+recProperty.getPropRandTreesValue()+recProperty.getPropStromDryValue();


					nRowErs�ttningHinder�ker=24;
					nRowErs�ttningLedningIskogsmark=29;
					nRowErs�ttning�vrig=37;
					// Ers�ttning 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,_T("Ledning i skogsmark"));
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));		
						nRowErs�ttningLedningIskogsmark++;
					}
					
					fErs�ttningLedningIskogsmark=0.0;
					cErs�ttningLedningIskogsmark=_T("");
					
					fErs�ttningHinder�ker=0.0;
					cErs�ttningHinder�ker=_T("");

					fErs�ttning�vrig=0.0;
					cErs�ttning�vrig=_T("");


					//H�mta annan ers�ttning av typ 
					// 4=ers�ttning f�r hinder i �kermark	(3)
					// 5=ers�ttning f�r ledning i skogsmark (4a)
					// 6=ers�ttning f�r �vrigt intr�ng		(5)
					m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),recProperty.getPropID_pk(),vecOtherComp);
				
					if (vecOtherComp.size() > 0)
					{
						for (UINT i = 0;i < vecOtherComp.size();i++)
						{
							
							other_erst_rec = vecOtherComp[i];
							nTypeOfOther=other_erst_rec.getPropOtherCompTypeOf();
							// 4=ers�ttning f�r hinder i �kermark	(3)
							if(nTypeOfOther==4)
							{
																//HMS-58 20200630 J�
								if(nRowErs�ttningHinder�ker>24)
								{
									fErs�ttningHinder�ker+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningHinder�ker.GetLength()<1)
										cErs�ttningHinder�ker=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningHinder�ker,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));
									nRowErs�ttningHinder�ker++;
								}
							}
							// 5=ers�ttning f�r ledning i skogsmark (4a)
							else if(nTypeOfOther==5)
							{

								//HMS-58 20200630 J�
								if(nRowErs�ttningLedningIskogsmark>29)
								{
									fErs�ttningLedningIskogsmark+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningLedningIskogsmark.GetLength()<1)
										cErs�ttningLedningIskogsmark=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		

									nRowErs�ttningLedningIskogsmark++;
								}


							}
							// 6=ers�ttning f�r �vrigt intr�ng		(5)
							else if(nTypeOfOther==6)
							{
								//HMS-58 20200630 J�
								if(nRowErs�ttning�vrig>37)
								{
									fErs�ttning�vrig+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttning�vrig.GetLength()<1)
										cErs�ttning�vrig=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttning�vrig,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttning�vrig,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		
									nRowErs�ttning�vrig++;
								}
							}
						}
					}

					//HMS-58 20200630 J� 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,cErs�ttningLedningIskogsmark);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));
					}
					if(fErs�ttningHinder�ker>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningHinder�ker,1,cErs�ttningHinder�ker);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(fErs�ttningHinder�ker+0.5));
					}
					if(fErs�ttning�vrig>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttning�vrig,1,cErs�ttning�vrig);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttning�vrig,9,floor(fErs�ttning�vrig+0.5));
					}


					// H�nta kontakter och �garinfo
					m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
					m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

					nRow=61;
					nCountOwners=0;
					nPrintedOwnerAtHeader=0;
					if (vecContacts.size() > 0)
					{
						//Loopa och skriv ut alla kontakter
						for (UINT i = 0;i < vecContacts.size();i++)
						{
							recContact = vecContacts[i];
							//Skriv bara ut 7 kontakter
							if (vecPropertyOwners.size() > 0 && nCountOwners<=7)
							{								
								for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
								{											
									recPropertyOwner = vecPropertyOwners[i1];									
									if (recPropertyOwner.getContactID() == recContact.getID())
									{
										nCountOwners+=1;
										// Skriv ut �gare som �r kontakt
										if(recPropertyOwner.getIsContact() == 1)
										{											
											if(nPrintedOwnerAtHeader==0)
											{
												cName.Format(_T("%s"),recContact.getName());
												cAdress.Format(_T(",%s,%s %s"),recContact.getAddress(),recContact.getPostNum(),recContact.getPostAddress());
												cKontaktAdress.Format(_T("%s%s"),cName,cAdress);
												//Kontaktperson & Adress
												sheet3->writeStr(6,3,cKontaktAdress);
												nPrintedOwnerAtHeader=1;
											}

										}
										//HMS-76 ut med personnummer 20220209 J�
										cName.Format(_T("%s %s"),recContact.getName(),recContact.getPNR_ORGNR());
										sheet3->writeStr(nRow,1,cName);
										//sheet3->writeStr(nRow,1,recContact.getName());
										// �gd Andel
										sheet3->writeStr(nRow,4,FormatShareToDecimal(recContact.getOwnerShare()));
										if(nCountOwners>1)	
											nRow+=6;
										else
											nRow+=7;
									}
								}										
							}
						}
					}
					//Check if directory exists
					bExistsDir = isDirectory(cSavePath);
					if(!bExistsDir)
					{
						createDirectory(cSavePath);						
					}
					xlsxBook->save((LPCTSTR)cExcPropFileName);
				}
			}
			xlsxBook->release();
			//Fr�ga om �ppna mapp med rapporter i utforskaren						
		}
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}

	if(nAskToOpenFolder==1)
	{
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna rapportmapp?"),_T("�ppna rapportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}


}


//HMS-30 20200704 J� V�rderingsprotokoll version 2020
void CMDI1950ForrestNormFrame::ExportPropertiesExcelrapportVp_2020(void)
{
	int nRow=0,nTypeOfOther=0,nCountOwners=0,nAskToOpenFolder=0;
	//CFileDialog dlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,L"EXCEL *.xls;*.xlsx",this);
	CFileDialog dlg(TRUE,_T("*.xlsx"),NULL,OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,_T("EXCEL (*.xls)|*.xls|EXCEL2 (*.xlsx)|*.xlsx||"),this);
	CString cErr=_T("");
	CString cTemp=_T("");
	CString cExcPropFileName=_T("");
	CString cKontaktAdress=_T("");
	CString cName=_T("");
	CString cAdress=_T("");
	CString cVersion=_T("");
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;

	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;

	vecTransaction_elv_properties_other_comp vecOtherComp;
	CTransaction_elv_properties_other_comp other_erst_rec;
	BOOL bExistsDir = FALSE;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cSavePath=_T("");
	CString sObjMapp=_T("");
	
	sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
	sObjMapp = checkDirectoryName(sObjMapp);


	cSavePath.Format(_T("%s\\%s"),getObjectDirectory(sObjMapp),DIRECTORYNAME_EXC_REPORTS);
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_VP);
	

	double fErs�ttningHinder�ker=0.0;
	CString cErs�ttningHinder�ker=_T("");
	int nRowErs�ttningHinder�ker=24;

	double fErs�ttningLedningIskogsmark=0.0;
	CString cErs�ttningLedningIskogsmark=_T("");
	int nRowErs�ttningLedningIskogsmark=29;
	
	double fErs�ttning�vrig=0.0,dGrotErs=0.0;
	CString cErs�ttning�vrig=_T("");
	int nRowErs�ttning�vrig=37;
	int nPrintedOwnerAtHeader=0;


	const TCHAR* tFileName = (LPCTSTR) cReportFileName;



	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];
			cTemp.Format(_T("%s"),recProperty.getPropName());
			cTemp.Replace(':',';');
			cExcPropFileName.Format(_T("%s\\%s-Vp.xlsx"),cSavePath,cTemp);		

			Book* xlsxBook = xlCreateXMLBook();
			if(xlsxBook)
			{
				if(xlsxBook->load(tFileName))
				{
					//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
					xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
					nAskToOpenFolder=1;
					//-----------------------------------------------------------------------------------------------------------------------------------------
					// V�rderingsprotokoll
					//-----------------------------------------------------------------------------------------------------------------------------------------
					Sheet* sheet3 = xlsxBook->getSheet(0);
					if(sheet3) 
					{   								
						// H�mta upp versionsnummer
						cVersion=sheet3->readStr(2,8);
						// G�r n�gon sorts versionskontroll ???

						// Projektnummer= ObjId	
						// HMS-65 20201123 Korrigerar till att visa �rendenummer
						//sheet3->writeStr(3,8,pObj->getObjErrandNum());
						// HMS-76 20220209 Korrigerar till att visa littera J�
						sheet3->writeStr(3,8,pObj->getObjLittra());
						// Ledning= Objnamn
						sheet3->writeStr(4,8,pObj->getObjectName());
						// Koncessionsl�pnr=littera
						//sheet3->writeStr(5,8,pObj->getObjLittra());

						// Fastighetsbeteckning
						sheet3->writeStr(3,3,recProperty.getPropName());
						// Kommun
						sheet3->writeStr(4,3,recProperty.getPropMunicipal());
						// Fastighetsnummer
						sheet3->writeStr(5,3,recProperty.getPropNumber());

						// Ers�tting Rotnetto HMS-115
						if(pObj->getObjIsGrotIncluded()==1)
							dGrotErs=recProperty.getPropGrotValue()-recProperty.getPropGrotCost();
						else
							dGrotErs=0.0;
						sheet3->writeNum(34,5,floor(recProperty.getPropRotpost()+dGrotErs+0.5));
					}

					fErs�ttningLedningIskogsmark=recProperty.getPropLandValue()+recProperty.getPropEarlyCutValue()+recProperty.getPropRandTreesValue()+recProperty.getPropStromDryValue();


					nRowErs�ttningHinder�ker=24;
					nRowErs�ttningLedningIskogsmark=29;
					nRowErs�ttning�vrig=37;
					// Ers�ttning 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,_T("Ledning i skogsmark"));
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));		
						nRowErs�ttningLedningIskogsmark++;
					}
					
					fErs�ttningLedningIskogsmark=0.0;
					cErs�ttningLedningIskogsmark=_T("");
					
					fErs�ttningHinder�ker=0.0;
					cErs�ttningHinder�ker=_T("");

					fErs�ttning�vrig=0.0;
					cErs�ttning�vrig=_T("");


					//H�mta annan ers�ttning av typ 
					// 4=ers�ttning f�r hinder i �kermark	(3)
					// 5=ers�ttning f�r ledning i skogsmark (4a)
					// 6=ers�ttning f�r �vrigt intr�ng		(5)
					m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),recProperty.getPropID_pk(),vecOtherComp);
				
					if (vecOtherComp.size() > 0)
					{
						for (UINT i = 0;i < vecOtherComp.size();i++)
						{
							
							other_erst_rec = vecOtherComp[i];
							nTypeOfOther=other_erst_rec.getPropOtherCompTypeOf();
							// 4=ers�ttning f�r hinder i �kermark	(3)
							if(nTypeOfOther==4)
							{
																//HMS-58 20200630 J�
								if(nRowErs�ttningHinder�ker>24)
								{
									fErs�ttningHinder�ker+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningHinder�ker.GetLength()<1)
										cErs�ttningHinder�ker=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningHinder�ker,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));
									nRowErs�ttningHinder�ker++;
								}
							}
							// 5=ers�ttning f�r ledning i skogsmark (4a)
							else if(nTypeOfOther==5)
							{

								//HMS-58 20200630 J�
								if(nRowErs�ttningLedningIskogsmark>29)
								{
									fErs�ttningLedningIskogsmark+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningLedningIskogsmark.GetLength()<1)
										cErs�ttningLedningIskogsmark=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		

									nRowErs�ttningLedningIskogsmark++;
								}


							}
							// 6=ers�ttning f�r �vrigt intr�ng		(5)
							else if(nTypeOfOther==6)
							{
								//HMS-58 20200630 J�
								if(nRowErs�ttning�vrig>37)
								{
									fErs�ttning�vrig+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttning�vrig.GetLength()<1)
										cErs�ttning�vrig=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttning�vrig,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttning�vrig,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		
									nRowErs�ttning�vrig++;
								}
							}
						}
					}

					//HMS-58 20200630 J� 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,cErs�ttningLedningIskogsmark);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));
					}
					if(fErs�ttningHinder�ker>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningHinder�ker,1,cErs�ttningHinder�ker);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(fErs�ttningHinder�ker+0.5));
					}
					if(fErs�ttning�vrig>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttning�vrig,1,cErs�ttning�vrig);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttning�vrig,9,floor(fErs�ttning�vrig+0.5));
					}


					// H�nta kontakter och �garinfo
					m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
					m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

					nRow=62;
					nCountOwners=0;
					nPrintedOwnerAtHeader=0;
					if (vecContacts.size() > 0)
					{
						//Loopa och skriv ut alla kontakter
						for (UINT i = 0;i < vecContacts.size();i++)
						{
							recContact = vecContacts[i];
							//Skriv bara ut 7 kontakter
							if (vecPropertyOwners.size() > 0 && nCountOwners<=7)
							{								
								for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
								{											
									recPropertyOwner = vecPropertyOwners[i1];									
									if (recPropertyOwner.getContactID() == recContact.getID())
									{
										nCountOwners+=1;
										// Skriv ut �gare som �r kontakt
										if(recPropertyOwner.getIsContact() == 1)
										{											
											if(nPrintedOwnerAtHeader==0)
											{
												cName.Format(_T("%s"),recContact.getName());
												cAdress.Format(_T(",%s,%s %s"),recContact.getAddress(),recContact.getPostNum(),recContact.getPostAddress());
												cKontaktAdress.Format(_T("%s%s"),cName,cAdress);
												//Kontaktperson & Adress
												sheet3->writeStr(6,3,cKontaktAdress);
												nPrintedOwnerAtHeader=1;
											}

										}
										//HMS-76 ut med personnummer 20220209 J�
										cName.Format(_T("%s %s"),recContact.getName(),recContact.getPNR_ORGNR());
										sheet3->writeStr(nRow,1,cName);
										//sheet3->writeStr(nRow,1,recContact.getName());
										// �gd Andel
										sheet3->writeStr(nRow,4,FormatShareToDecimal(recContact.getOwnerShare()));
										if(nCountOwners>1)	
											nRow+=5;
										else
											nRow+=6;
									}
								}										
							}
						}
					}
					//Check if directory exists
					bExistsDir = isDirectory(cSavePath);
					if(!bExistsDir)
					{
						createDirectory(cSavePath);						
					}
					xlsxBook->save((LPCTSTR)cExcPropFileName);
				}
			}
			xlsxBook->release();
			//Fr�ga om �ppna mapp med rapporter i utforskaren						
		}
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}

	if(nAskToOpenFolder==1)
	{
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna rapportmapp?"),_T("�ppna rapportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}
}

//HMS-30 20200704 J� V�rderingsprotokoll version 2020
void CMDI1950ForrestNormFrame::ExportPropertiesExcelrapportVp_2021(void)
{
	int nRow=0,nTypeOfOther=0,nCountOwners=0,nAskToOpenFolder=0;
	//CFileDialog dlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,L"EXCEL *.xls;*.xlsx",this);
	CFileDialog dlg(TRUE,_T("*.xlsx"),NULL,OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,_T("EXCEL (*.xls)|*.xls|EXCEL2 (*.xlsx)|*.xlsx||"),this);
	CString cErr=_T("");
	CString cTemp=_T("");
	CString cExcPropFileName=_T("");
	CString cKontaktAdress=_T("");
	CString cName=_T("");
	CString cAdress=_T("");
	CString cVersion=_T("");
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;

	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;

	vecTransaction_elv_properties_other_comp vecOtherComp;
	CTransaction_elv_properties_other_comp other_erst_rec;
	BOOL bExistsDir = FALSE;
	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cSavePath=_T("");
	CString sObjMapp=_T("");
	
	sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
	sObjMapp = checkDirectoryName(sObjMapp);


	cSavePath.Format(_T("%s\\%s"),getObjectDirectory(sObjMapp),DIRECTORYNAME_EXC_REPORTS);
	cReportsDir=getReportsDir();
	cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_VP);
	

	double fErs�ttningHinder�ker=0.0;
	CString cErs�ttningHinder�ker=_T("");
	int nRowErs�ttningHinder�ker=24;

	double fErs�ttningLedningIskogsmark=0.0;
	CString cErs�ttningLedningIskogsmark=_T("");
	int nRowErs�ttningLedningIskogsmark=29;
	
	double fErs�ttning�vrig=0.0,dGrotErs=0.0;
	CString cErs�ttning�vrig=_T("");
	int nRowErs�ttning�vrig=37;
	int nPrintedOwnerAtHeader=0;


	const TCHAR* tFileName = (LPCTSTR) cReportFileName;



	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];
			cTemp.Format(_T("%s"),recProperty.getPropName());
			cTemp.Replace(':',';');
			cExcPropFileName.Format(_T("%s\\%s-Vp.xlsx"),cSavePath,cTemp);		

			Book* xlsxBook = xlCreateXMLBook();
			if(xlsxBook)
			{
				if(xlsxBook->load(tFileName))
				{
					//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
					xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
					nAskToOpenFolder=1;
					//-----------------------------------------------------------------------------------------------------------------------------------------
					// V�rderingsprotokoll
					//-----------------------------------------------------------------------------------------------------------------------------------------
					Sheet* sheet3 = xlsxBook->getSheet(0);
					if(sheet3) 
					{   								
						// H�mta upp versionsnummer
						cVersion=sheet3->readStr(2,8);
						// G�r n�gon sorts versionskontroll ???

						// Projektnummer= ObjId	
						// HMS-65 20201123 Korrigerar till att visa �rendenummer
						//sheet3->writeStr(3,8,pObj->getObjErrandNum());
						// HMS-76 20220209 Korrigerar till att visa littera J�
						sheet3->writeStr(3,8,pObj->getObjLittra());
						// Ledning= Objnamn
						sheet3->writeStr(4,8,pObj->getObjectName());
						// Koncessionsl�pnr=littera
						//sheet3->writeStr(5,8,pObj->getObjLittra());

						// Fastighetsbeteckning
						sheet3->writeStr(3,3,recProperty.getPropName());
						// Kommun
						sheet3->writeStr(4,3,recProperty.getPropMunicipal());
						// Fastighetsnummer
						sheet3->writeStr(5,3,recProperty.getPropNumber());

						// Ers�tting Rotnetto HMS-115
						if(pObj->getObjIsGrotIncluded()==1)
							dGrotErs=recProperty.getPropGrotValue()-recProperty.getPropGrotCost();
						else
							dGrotErs=0.0;
						sheet3->writeNum(34,5,floor(recProperty.getPropRotpost()+dGrotErs+0.5));
					}

					fErs�ttningLedningIskogsmark=recProperty.getPropLandValue()+recProperty.getPropEarlyCutValue()+recProperty.getPropRandTreesValue()+recProperty.getPropStromDryValue();


					nRowErs�ttningHinder�ker=24;
					nRowErs�ttningLedningIskogsmark=29;
					nRowErs�ttning�vrig=37;
					// Ers�ttning 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,_T("Ledning i skogsmark"));
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));		
						nRowErs�ttningLedningIskogsmark++;
					}
					
					fErs�ttningLedningIskogsmark=0.0;
					cErs�ttningLedningIskogsmark=_T("");
					
					fErs�ttningHinder�ker=0.0;
					cErs�ttningHinder�ker=_T("");

					fErs�ttning�vrig=0.0;
					cErs�ttning�vrig=_T("");


					//H�mta annan ers�ttning av typ 
					// 4=ers�ttning f�r hinder i �kermark	(3)
					// 5=ers�ttning f�r ledning i skogsmark (4a)
					// 6=ers�ttning f�r �vrigt intr�ng		(5)
					m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),recProperty.getPropID_pk(),vecOtherComp);
				
					if (vecOtherComp.size() > 0)
					{
						for (UINT i = 0;i < vecOtherComp.size();i++)
						{
							
							other_erst_rec = vecOtherComp[i];
							nTypeOfOther=other_erst_rec.getPropOtherCompTypeOf();
							// 4=ers�ttning f�r hinder i �kermark	(3)
							if(nTypeOfOther==4)
							{
																//HMS-58 20200630 J�
								if(nRowErs�ttningHinder�ker>24)
								{
									fErs�ttningHinder�ker+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningHinder�ker.GetLength()<1)
										cErs�ttningHinder�ker=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningHinder�ker,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));
									nRowErs�ttningHinder�ker++;
								}
							}
							// 5=ers�ttning f�r ledning i skogsmark (4a)
							else if(nTypeOfOther==5)
							{

								//HMS-58 20200630 J�
								if(nRowErs�ttningLedningIskogsmark>29)
								{
									fErs�ttningLedningIskogsmark+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttningLedningIskogsmark.GetLength()<1)
										cErs�ttningLedningIskogsmark=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		

									nRowErs�ttningLedningIskogsmark++;
								}


							}
							// 6=ers�ttning f�r �vrigt intr�ng		(5)
							else if(nTypeOfOther==6)
							{
								//HMS-58 20200630 J�
								if(nRowErs�ttning�vrig>37)
								{
									fErs�ttning�vrig+=other_erst_rec.getPropOtherCompValue_calc();
									if(cErs�ttning�vrig.GetLength()<1)
										cErs�ttning�vrig=other_erst_rec.getPropOtherCompNote();
								}
								else
								{
									// Beskrivning
									sheet3->writeStr(nRowErs�ttning�vrig,1,other_erst_rec.getPropOtherCompNote());
									// Ers�ttning
									sheet3->writeNum(nRowErs�ttning�vrig,9,floor(other_erst_rec.getPropOtherCompValue_calc()+0.5));		
									nRowErs�ttning�vrig++;
								}
							}
						}
					}

					//HMS-58 20200630 J� 4a
					if(fErs�ttningLedningIskogsmark>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningLedningIskogsmark,1,cErs�ttningLedningIskogsmark);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningLedningIskogsmark,9,floor(fErs�ttningLedningIskogsmark+0.5));
					}
					if(fErs�ttningHinder�ker>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttningHinder�ker,1,cErs�ttningHinder�ker);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttningHinder�ker,9,floor(fErs�ttningHinder�ker+0.5));
					}
					if(fErs�ttning�vrig>0.0)
					{
						// Beskrivning
						sheet3->writeStr(nRowErs�ttning�vrig,1,cErs�ttning�vrig);
						// Ers�ttning
						sheet3->writeNum(nRowErs�ttning�vrig,9,floor(fErs�ttning�vrig+0.5));
					}


					// H�nta kontakter och �garinfo
					m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
					m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

					nRow=61;
					nCountOwners=0;
					nPrintedOwnerAtHeader=0;
					if (vecContacts.size() > 0)
					{
						//Loopa och skriv ut alla kontakter
						for (UINT i = 0;i < vecContacts.size();i++)
						{
							recContact = vecContacts[i];
							//Skriv bara ut 7 kontakter
							if (vecPropertyOwners.size() > 0 && nCountOwners<=7)
							{								
								for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
								{											
									recPropertyOwner = vecPropertyOwners[i1];									
									if (recPropertyOwner.getContactID() == recContact.getID())
									{
										nCountOwners+=1;
										// Skriv ut �gare som �r kontakt
										if(recPropertyOwner.getIsContact() == 1)
										{											
											if(nPrintedOwnerAtHeader==0)
											{
												cName.Format(_T("%s"),recContact.getName());
												cAdress.Format(_T(",%s,%s %s"),recContact.getAddress(),recContact.getPostNum(),recContact.getPostAddress());
												cKontaktAdress.Format(_T("%s%s"),cName,cAdress);
												//Kontaktperson & Adress
												sheet3->writeStr(6,3,cKontaktAdress);
												nPrintedOwnerAtHeader=1;
											}

										}
										//HMS-76 ut med personnummer 20220209 J�
										cName.Format(_T("%s %s"),recContact.getName(),recContact.getPNR_ORGNR());
										sheet3->writeStr(nRow,1,cName);
										//sheet3->writeStr(nRow,1,recContact.getName());
										// �gd Andel
										sheet3->writeStr(nRow,4,FormatShareToDecimal(recContact.getOwnerShare()));
										if(nCountOwners>1)	
											nRow+=6;
										else
											nRow+=7;
									}
								}										
							}
						}
					}
					//Check if directory exists
					bExistsDir = isDirectory(cSavePath);
					if(!bExistsDir)
					{
						createDirectory(cSavePath);						
					}
					xlsxBook->save((LPCTSTR)cExcPropFileName);
				}
			}
			xlsxBook->release();
			//Fr�ga om �ppna mapp med rapporter i utforskaren						
		}
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}

	if(nAskToOpenFolder==1)
	{
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna rapportmapp?"),_T("�ppna rapportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}
}


//HMS-30 20200704 J� Ers�ttningserbjudande f�rs�ljning, sheet1, samt tillvaratagande egen regi sheet 2.
void CMDI1950ForrestNormFrame::ExportPropertiesExcelrapportErsErbj(int nVersion)
{
	int nRow=0,nCountOwners=0,nAskToOpenFolder=0,nIngenErsattning=0,nPrintedContactAtHeader=0;
	int nAddCol=0;
	//CFileDialog dlg(TRUE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,L"EXCEL *.xls;*.xlsx",this);
	CFileDialog dlg(TRUE,_T("*.xlsx"),NULL,OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,_T("EXCEL (*.xls)|*.xls|EXCEL2 (*.xlsx)|*.xlsx||"),this);
	CString cErr=_T("");
	CString cTemp=_T("");
	CString cVersion=_T("");
	CString cExcPropFileName=_T("");
	//CString cKontaktAdress=_T("");
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties recProperty;

	vecTransaction_elv_properties vecELV_properties;
	vecTransactionPropOwners vecPropertyOwners;
	vecTransactionContacts vecContacts;
	CTransaction_contacts recContact;
	CTransaction_prop_owners recPropertyOwner;

	CString cReportsDir=_T("");
	CString cReportFileName=_T("");
	CString cSavePath=_T("");
	CString sObjMapp=_T("");
	CString cName=_T("");
	CString cAdress=_T("");
	CString cKontaktAdress=_T("");

	sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
	sObjMapp = checkDirectoryName(sObjMapp);

	cSavePath.Format(_T("%s\\%s"),getObjectDirectory(sObjMapp),DIRECTORYNAME_EXC_REPORTS);			
	
	cReportsDir=getReportsDir();


	switch(nVersion)
	{
	case ERSERBJ_EXCELVERSION_2019:
		cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_ERS_ERBJ);
		nAddCol=0;
		break;
	case ERSERBJ_EXCELVERSION_2023:
		cReportFileName.Format(_T("%s%s"),cReportsDir,FILENAME_EXC_ERS_ERBJ_XLTX);
		nAddCol=1;
		break;
	}


	
	BOOL bExistsDir = FALSE;
	const TCHAR* tFileName = (LPCTSTR) cReportFileName;

    const wchar_t* fonts[] = {L"Arial", L"Arial Black", L"Comic Sans MS", L"Courier New",
                              L"Impact", L"Times New Roman", L"Verdana"};




	m_pDB->getProperties(pObj->getObjID_pk(),vecELV_properties);

	if (vecELV_properties.size() > 0)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		for (UINT i = 0;i < vecELV_properties.size();i++)
		{
			recProperty=vecELV_properties[i];

			nIngenErsattning=0;
			//Kolla om ers�ttning, om rotnetto =0 ingen rapport
			if(recProperty.getPropRotpost()<=0.0 && recProperty.getPropEarlyCutValue()<=0)
			nIngenErsattning=1;

			if(nIngenErsattning==1)
			{}
			else
			{

				cTemp.Format(_T("%s"),recProperty.getPropName());
				cTemp.Replace(':',';');
				Book* xlsxBook = xlCreateXMLBook();
				switch(nVersion)
				{
				case ERSERBJ_EXCELVERSION_2019:
					cExcPropFileName.Format(_T("%s\\%s-Ers.xlsx"),cSavePath,cTemp);					
					break;
				case ERSERBJ_EXCELVERSION_2023:
					cExcPropFileName.Format(_T("%s\\%s-Ers.xlsx"),cSavePath,cTemp);			
					xlsxBook->setTemplate(true);
					break;
				}



				
				
				if(xlsxBook)
				{
					if(xlsxBook->load(tFileName))
					{
						//xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
						xlsxBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");

						//-----------------------------------------------------------------------------------------------------------------------------------------
						// Ers�ttningsergbjudande F�rs�ljning
						//-----------------------------------------------------------------------------------------------------------------------------------------
						Sheet* sheetSale = xlsxBook->getSheet(0);
						if(sheetSale) 
						{   				
							nAskToOpenFolder=1;
							// H�mta upp versionsnummer
							//cVersion=sheetSale->readStr(1,7);
							// G�r n�gon versionskontroll ???	

							// Projektnummer= ObjId			
							// HMS-65 20201111 Korrigerar til att visa �rendenummer
							//sheetSale->writeStr(2,7,pObj->getObjErrandNum());
							//HMS-76 20220209 Visa littera ist�llet
							sheetSale->writeStr(2,7+nAddCol,pObj->getObjLittra());
							// Ledning= Objnamn
							sheetSale->writeStr(3,7+nAddCol,pObj->getObjectName());
							// Koncessionsl�pnr=littera
							// HMS-65 20201119 Korrigerar till att ej visa koncessionsnr
							//sheetSale->writeStr(4,7,pObj->getObjLittra());

							// Fastighetsbeteckning
							sheetSale->writeStr(2,2+nAddCol,recProperty.getPropName());
							// Kommun
							sheetSale->writeStr(3,2+nAddCol,recProperty.getPropMunicipal());
							// Fastighetsnummer
							sheetSale->writeStr(4,2+nAddCol,recProperty.getPropNumber());
							// Antal tr�d
							sheetSale->writeNum(14,5+nAddCol,recProperty.getPropNumOfTrees());
							// Volym m3sk
							sheetSale->writeNum(14,7+nAddCol,recProperty.getPropM3Sk());
							// Ers�tting Rotnetto
							sheetSale->writeNum(15,8+nAddCol,floor(recProperty.getPropRotpost()+0.5));
							// Moms
							sheetSale->writeNum(16,8+nAddCol,floor( (recProperty.getPropRotpost()*(pObj->getObjVAT()/100.0))+0.5 ));
						}

						// H�nta kontakter och �garinfo
						m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
						m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

						nRow=35;
						nCountOwners=0;
						nPrintedContactAtHeader=0;
						if (vecContacts.size() > 0)
						{
							for (UINT i = 0;i < vecContacts.size();i++)
							{
								recContact = vecContacts[i];
								if (vecPropertyOwners.size() > 0  && nCountOwners<=3)
								{
									// Skriv ut alla �gare
									for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
									{											
										recPropertyOwner = vecPropertyOwners[i1];
										if (recPropertyOwner.getContactID() == recContact.getID())
										{
											cName.Format(_T("%s"),recContact.getName());
											nCountOwners+=1;
											// Kolla om satt som kontakt
											if(recPropertyOwner.getIsContact() == 1)
											{																								
												// HMS-65 20201111 skriv ut namn och adress
												if(nPrintedContactAtHeader==0)
												{
													cAdress.Format(_T("%s %s %s"),recContact.getAddress(),recContact.getPostNum(),recContact.getPostAddress());
													cKontaktAdress.Format(_T("%s \n%s")
														,cName
														,cAdress);
													// Kontrollera om antal tecken p� n�gon av de tre adressraderna �r mer �n 21 i s� fall �ndra font
													if((cName.GetLength()>21 || cAdress.GetLength()>21)/* && nVersion==ERSERBJ_EXCELVERSION_2019*/)
													{
														Format* format = sheetSale->cellFormat(5,2+nAddCol);
														format->font()->setSize(8);	
														format->setShrinkToFit(true);
														sheetSale->writeStr(5,2+nAddCol, cKontaktAdress, format);
													}
													else
													{
														// Kontaktperson & Adress
														sheetSale->writeStr(5,2+nAddCol,cKontaktAdress);
													}
													nPrintedContactAtHeader=1;
												}
											}
											sheetSale->writeStr(nRow,0+nAddCol,cName);
											//Personnummer
											// HMS-65 20201111 skriv ut personnummer
											cName.Format(_T("%s"),recContact.getPNR_ORGNR());
											Format* format = sheetSale->cellFormat(nRow,3);
											sheetSale->writeStr(nRow,3+nAddCol,cName);
											// �gd Andel
											sheetSale->writeStr(nRow+2,0+nAddCol,FormatShareToDecimal(recContact.getOwnerShare()));
											if(nCountOwners>1) 
											{
												if(nRow==42)
													nRow+=9;
												else
													nRow+=7;
											}
											else
												nRow+=7;	
										}
									}
								}
							}
						}

						//-----------------------------------------------------------------------------------------------------------------------------------------
						// Ers�ttningsergbjudande Tillvaratagande sheet nr 2
						//-----------------------------------------------------------------------------------------------------------------------------------------
						Sheet* sheetTillv = xlsxBook->getSheet(1);
						if(sheetTillv) 
						{   		
							nAskToOpenFolder=1;
							// Projektnummer= ObjId			
							// HMS-65 20201111 Korrigerar til att visa �rendenummer
							sheetTillv->writeStr(2,7+nAddCol,pObj->getObjErrandNum());
							// Ledning= Objnamn
							sheetTillv->writeStr(3,7+nAddCol,pObj->getObjectName());
							// Koncessionsl�pnr=littera
							// HMS-65 20201119 Korrigerar till att ej visa koncessionsnr
							//sheetTillv->writeStr(4,7,pObj->getObjLittra());

							// Fastighetsbeteckning
							sheetTillv->writeStr(2,2+nAddCol,recProperty.getPropName());
							// Kommun
							sheetTillv->writeStr(3,2+nAddCol,recProperty.getPropMunicipal());
							// Fastighetsnummer
							sheetTillv->writeStr(4,2+nAddCol,recProperty.getPropNumber());
							// Antal tr�d
							sheetTillv->writeNum(14,7+nAddCol,recProperty.getPropNumOfTrees());
							// Ers�tting F�rdyrad avverkning
							// HMS-65 20201111 Korrigerat ers�ttning till f�rdyrad
							//sheetTillv->writeNum(14,8,floor(recProperty.getPropEarlyCutValue()+0.5));
							sheetTillv->writeNum(14,8+nAddCol,floor(recProperty.getPropHighCutValue()+0.5));
						}

						// H�nta kontakter och �garinfo
						m_pDB->getPropertyOwnersForProperty(recProperty.getPropID_pk(),vecPropertyOwners);
						m_pDB->getPropertyOwners(recProperty.getPropID_pk(),vecContacts);

						nRow=28;
						nCountOwners=0;
						nPrintedContactAtHeader=0;
						if (vecContacts.size() > 0)
						{
							for (UINT i = 0;i < vecContacts.size();i++)
							{
								recContact = vecContacts[i];
								if (vecPropertyOwners.size() > 0 && nCountOwners<=3)
								{
									// Skriv ut en �gare, den som �r satt som kontakt
									for (UINT i1 = 0;i1 < vecPropertyOwners.size();i1++)
									{											
										recPropertyOwner = vecPropertyOwners[i1];
										if (recPropertyOwner.getContactID() == recContact.getID())
										{
											cName.Format(_T("%s"),recContact.getName());
											nCountOwners+=1;
											// Kolla om satt som kontakt
											if(recPropertyOwner.getIsContact() == 1)
											{																								
												if(nPrintedContactAtHeader==0)
												{
													// HMS-65 20201111 skriv ut namn och adress
													cAdress.Format(_T("%s %s %s"),recContact.getAddress()
														,recContact.getPostNum()
														,recContact.getPostAddress());
													cKontaktAdress.Format(_T("%s \n%s")
														,cName
														,cAdress);
													// Kontrollera om antal tecken p� n�gon av de tre adressraderna �r mer �n 21 i s� fall �ndra font
													if((cName.GetLength()>21 || cAdress.GetLength()>21) /*&& nVersion==ERSERBJ_EXCELVERSION_2019*/)
													{
														Format* format = sheetTillv->cellFormat(5,2+nAddCol);											
														format->font()->setSize(8);												
														format->setShrinkToFit(true);
														sheetTillv->writeStr(5,2+nAddCol, cKontaktAdress, format);												
													}
													else
													{
														// Kontaktperson & Adress
														sheetTillv->writeStr(5,2+nAddCol,cKontaktAdress);
													}
													nPrintedContactAtHeader=1;
												}
											}
											// Kontrollera om antal tecken p� n�gon av de tre adressraderna �r mer �n 27 i s� fall �ndra font till 8 ist�llet f�r 10
											if(cName.GetLength()>27 /*&& nVersion==ERSERBJ_EXCELVERSION_2019*/)
											{
												Format* format = sheetTillv->cellFormat(nRow,0+nAddCol);
												format->font()->setSize(8);			
												format->setShrinkToFit(true);
												sheetTillv->writeStr(nRow,0+nAddCol, cName, format);
											}
											else
											{
												sheetTillv->writeStr(nRow,0+nAddCol,cName);
											}
											// �gd Andel
											sheetTillv->writeStr(nRow,3+nAddCol,FormatShareToDecimal(recContact.getOwnerShare()));
											nRow+=5;
										}
									}									
								}
							}
						}
						//-----------------------------------------------------------------------------------------------------------------------------------------				

						//Check if directory exists
						bExistsDir = isDirectory(cSavePath);
						if(!bExistsDir)
						{
							createDirectory(cSavePath);
						}
						xlsxBook->save((LPCTSTR)cExcPropFileName);
					}
				}
				xlsxBook->release();
			}
		}
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	}
	if(nAskToOpenFolder==1)
	{
		if (::MessageBox(this->GetSafeHwnd(),_T("�ppna rapportmapp?"),_T("�ppna rapportmapp"),MB_ICONQUESTION | MB_YESNOCANCEL) == IDYES)
		{
			ShellExecute(NULL, _T("open"),cSavePath, NULL, NULL, SW_SHOWDEFAULT);
		}
	}
}

void CMDI1950ForrestNormFrame::OnExportPropertiesTBtn(void)
{
	RLFReader xml;
	int f[4];
	vecTransaction_elv_properties vec;
	CTransaction_elv_object *pObj = getActiveObject();
	CString	sFileName = pObj->getObjectName() + L"_" + pObj->getObjIDNumber();

	if (!fileExists(m_sLangFN) || pObj == NULL) return;

	CFileDialog dlg(FALSE,L".xls",sFileName,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL *.xls|*.xls|");
	if (dlg.DoModal() != IDOK) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			Book* book = xlCreateBook();
			if(book)
			{
				// Set registraton-key
				//book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
				//book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
				  book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
				f[0] = book->addCustomNumFormat(L"0.0");
				f[1] = book->addCustomNumFormat(L"0.00");
				f[2] = book->addCustomNumFormat(L"0.000");
				f[3] = book->addCustomNumFormat(L"0");

				libxl::Font* font = book->addFont();
        font->setSize(8);   

				Format* fmt[6];
				fmt[0] = book->addFormat();
				fmt[0]->setNumFormat(f[0]);
				fmt[0]->setFont(font);
				
				fmt[1] = book->addFormat();
				fmt[1]->setNumFormat(f[1]);
				fmt[1]->setFont(font);
				
				fmt[2] = book->addFormat();
				fmt[2]->setNumFormat(f[2]);
				fmt[2]->setFont(font);
				
				fmt[3] = book->addFormat();
				fmt[3]->setWrap();
				fmt[3]->setShrinkToFit();
				fmt[3]->setFont(font);
				
				fmt[4] = book->addFormat();
				fmt[4]->setNumFormat(f[3]);
				fmt[4]->setFont(font);
				
				fmt[5] = book->addFormat();
				fmt[5]->setWrap();
				fmt[5]->setShrinkToFit();
				fmt[5]->setAlignH(libxl::ALIGNH_RIGHT);
				fmt[5]->setFont(font);
				
				Sheet* sheet = book->addSheet(L"Sheet1");
				if(sheet)
				{
					if (xml.Load(m_sLangFN))
					{
						sheet->setCol(0,3,15);				
						sheet->setCol(4,25,10);				
						// Add header information
						// Objektname
						sheet->writeStr(1, 0,xml.str(IDS_STRING204));
						sheet->writeStr(1, 1,pObj->getObjectName());
						// Objekt id
						sheet->writeStr(2, 0,xml.str(IDS_STRING205));
						sheet->writeStr(2, 1,pObj->getObjIDNumber());

						// Add property information

						// Column headers
						//sheet->writeStr(5, 0,xml.str(IDS_STRING106),fmt[3]);
						//sheet->writeStr(5, 1,xml.str(IDS_STRING107),fmt[3]);
						// Status
						sheet->writeStr(5, 0,xml.str(IDS_STRING3017),fmt[3]);
						// L�n
						sheet->writeStr(5, 1,xml.str(IDS_STRING2311),fmt[3]);
						// Kommun
						sheet->writeStr(5, 2,xml.str(IDS_STRING2312),fmt[3]);
						// Fastighetsnamn
						sheet->writeStr(5, 3,xml.str(IDS_STRING3000),fmt[3]);
						// Fastighetsnummer
						sheet->writeStr(5, 4,xml.str(IDS_STRING3001),fmt[3]);
						// Volym (m3sk)
						sheet->writeStr(5, 5,xml.str(IDS_STRING3002),fmt[5]);
						// Areal (ha)
						sheet->writeStr(5, 6,xml.str(IDS_STRING3003),fmt[5]);
						// Antal tr�d
						sheet->writeStr(5, 7,xml.str(IDS_STRING3004),fmt[5]);
						// Antal taxerade
						sheet->writeStr(5, 8,xml.str(IDS_STRING3005),fmt[5]);
						// Volym gagnvirke (m3fub)
						sheet->writeStr(5, 9,xml.str(IDS_STRING3006),fmt[5]);
						// Virkesv�rde (kr)
						sheet->writeStr(5, 10,xml.str(IDS_STRING3007),fmt[5]);
						// Summa kostnad (kr)
						sheet->writeStr(5, 11,xml.str(IDS_STRING3008),fmt[5]);
						// ROTNETTO (kr)
						sheet->writeStr(5, 12,xml.str(IDS_STRING3009),fmt[5]);
						// Markv�rde (kr)
						sheet->writeStr(5, 13,xml.str(IDS_STRING3010),fmt[5]);
						// F�rtidig avverkning (kr)
						sheet->writeStr(5, 14,xml.str(IDS_STRING3011),fmt[5]);
						// Storm- och torkskador (kr)
						sheet->writeStr(5, 15,xml.str(IDS_STRING3012),fmt[5]);
						// Kanttr�d (kr)
						sheet->writeStr(5, 16,xml.str(IDS_STRING3013),fmt[5]);
						// Frivillig uppg�relse (kr)
						sheet->writeStr(5, 17,xml.str(IDS_STRING3014),fmt[5]);
						// F�rdyrad avverkning (kr)
						sheet->writeStr(5, 18,xml.str(IDS_STRING3015),fmt[5]);
						// Annan ers�ttning (kr)
						sheet->writeStr(5, 19,xml.str(IDS_STRING3016),fmt[5]);
						// Gruppid
						// sheet->writeStr(5, 20,xml.str(IDS_STRING3018),fmt[3]);
						
						// #HMS-91 Tillvaratagande l�gg till tillvaratagande info 20220825 J�
						sheet->writeStr(5, 20,xml.str(IDS_STRING3019),fmt[3]);
						
						// Sorteringsordning
						// sheet->writeStr(5, 22,xml.str(IDS_STRING3020),fmt[3]);
						// Skogsbr�nsle (ton TS)
						sheet->writeStr(5, 21,xml.str(IDS_STRING3021),fmt[5]);
						// Skogsbr�nsle v�rde (kr)
						sheet->writeStr(5, 22,xml.str(IDS_STRING3022),fmt[5]);
						// Skogsbr�nsle kostnad (kr)
						sheet->writeStr(5, 23,xml.str(IDS_STRING3023),fmt[5]);

						pPropView->getPropertiesInReport(sheet,fmt);

					}	// if (xml.Load(m_sLangFN))
				}	// if(sheet)

				if(book->save(dlg.GetPathName())) 
				{
					/*	Uncomment to open excel after saving file
						if (::MessageBox(GetSafeHwnd(),m_sMsgOpenEXCEL,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
							::ShellExecute(NULL, L"open", dlg.GetFolderPath()+L"\\"+dlg.GetFileName(), NULL, NULL, SW_SHOW);        
					*/
				}
				else
				{
					CString sMsg = L"";
					sMsg.Format(L"%S",book->errorMessage());
					AfxMessageBox(sMsg);
				}

				book->release();		

			}	// if(book)
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)

}

void CMDI1950ForrestNormFrame::OnLogBookSettingsTBtn(void)
{

	CHandleLogBookDlg *pDlg = new CHandleLogBookDlg();
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
		}

		delete pDlg;
	}
}

void CMDI1950ForrestNormFrame::OnUpdatePricelistTemplate(void)
{

	BOOL bIsObjUpdates = FALSE;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj == NULL) return;
	// Load most resent information on "prislistor"; 080918 p�d
	getPricelistsFromDB();

	// Just make sure that index is within limits; 080319 p�d
	if (m_vecTransactionPricelist.size() > 0 &&
			!m_sBindToPricelistSelected.IsEmpty())
	{
		for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
		{
			CTransaction_pricelist rec = m_vecTransactionPricelist[i];
			if (m_sBindToPricelistSelected.Compare(rec.getName()) == 0 &&
				 rec.getTypeOf() == pObj->getObjTypeOfPricelist())
			{
				// Only update the active Object; 080408 p�d
				bIsObjUpdates = m_pDB->updObject_prl(pObj->getObjID_pk(),m_sBindToPricelistSelected,rec.getTypeOf(),rec.getPricelistFile());
				break;
			}	// if (m_sBindToPricelistSelected.Compare(rec.getName()) == 0)
		}	// for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
	}
	// If we have an update ob object, i think we should
	// also recaluclate; 080918 p�d
	if (bIsObjUpdates)
			OnCalculateTBtn();
}

void CMDI1950ForrestNormFrame::OnUpdatePricelistInfoTemplate(void)
{
	showFormView(IDD_FORMVIEW10,m_sLangFN,NULL);
}

void CMDI1950ForrestNormFrame::OnUpdateP30(void)
{
	BOOL bUpdated = FALSE;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj == NULL) return;

	if (::MessageBox(this->GetSafeHwnd(),m_sMsgUpdP30,m_sMsgCap,MB_ICONASTERISK | MB_DEFBUTTON1 | MB_YESNO) == IDYES)
	{
		CString sP30Name(pObj->getObjP30Name());
		int nTypeOf = pObj->getObjP30TypeOf();

		if (pObj->getObjP30TypeOf() == TEMPLATE_P30) getObjectP30TemplatesFromDB(TEMPLATE_P30);
		else if (pObj->getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)	getObjectP30TemplatesFromDB(TEMPLATE_P30_NEW_NORM);
		else if (pObj->getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)	getObjectP30TemplatesFromDB(TEMPLATE_P30_2018_NORM);


		// Try to match P30 in Object to P30 in template table; 100303 p�d
		for (UINT i = 0;i < m_vecObjectP30Template.size();i++)
		{
			if (m_vecObjectP30Template[i].getTemplateName().Compare(sP30Name) == 0 && nTypeOf == m_vecObjectP30Template[i].getTypeOf())
			{
				if (m_pDB) 
				{
					if (m_pDB->updObject_p30(pObj->getObjID_pk(),sP30Name,nTypeOf,m_vecObjectP30Template[i].getTemplateFile()))
					{
						if ((pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
						{
							// Try to get the ObjectView; 100303 p�d
							if ((pObjView = pFrame->getObjectFormView()) != NULL)
							{
								// On change, repopulate object; 100303 p�d
								pObjView->doPopulateData(pObjView->getObjIndex());
								pObjView = NULL;
							}	// if (pObjView != NULL)					
						}	// if (pFrame != NULL)
						// Need to release ref. pointers; 080520 p�d
						pFrame = NULL;
						// Tell user to recalculate; 100303 p�d
						OnCalculateTBtn();
						bUpdated = TRUE;

					}	// if (m_pDB->updObject_p30(pObj->getObjID_pk(),sP30Name,nTypeOf,m_vecObjectP30Template[i].getTemplateFile()))
				}	// if (m_pDB) 
			}	// if (m_vecObjectP30Template[i].Compare(sP30Name) == 0 && nTypeOf == m_vecObjectP30Template[i].getTypeOf())
		}	// for (UINT i = 0;i < m_vecObjectP30Template.size();i++)

		// Check if template was found; 111122 Peter, redmine #2496
		if( !bUpdated )
		{
			::MessageBox(this->GetSafeHwnd(),m_sMsgP30NotFound,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}
	}	// if (::MessageBox(this->GetSafeHwnd(),m_sMsgUpdP30,m_sMsgCap,MB_ICONASTERISK | MB_DEFBUTTON1 | MB_YESNO) == IDYES)
}

void CMDI1950ForrestNormFrame::OnUpdateHigherCosts(void)
{
	BOOL bUpdated = FALSE;
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = NULL;
	CPropertiesFormView *pProp = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj == NULL) return;

	if (::MessageBox(this->GetSafeHwnd(),m_sMsgUpdHigherCosts,m_sMsgCap,MB_ICONASTERISK | MB_DEFBUTTON1 | MB_YESNO) == IDYES)
	{
		CString sHigherCostsName(pObj->getObjHCostName());
		short nTypeOf = pObj->getObjHCostTypeOf();

		// Reload Cost tables; 100303 p�d			
		getHighCostTemplateFromDB();
		// Try to match P30 in Object to P30 in template table; 100303 p�d
		for (UINT i = 0;i < vecTemplate_high_cost.size();i++)
		{
			if (vecTemplate_high_cost[i].getTemplateName().Compare(sHigherCostsName) == 0 && nTypeOf == vecTemplate_high_cost[i].getTypeOf())
			{
				if (m_pDB) 
				{
					if (m_pDB->updObject_hcost(pObj->getObjID_pk(),sHigherCostsName,nTypeOf,vecTemplate_high_cost[i].getTemplateFile()))
					{
						if ((pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
						{
							// Try to get the ObjectView; 100303 p�d
							if ((pObjView = pFrame->getObjectFormView()) != NULL)
							{
								// On change, repopulate object; 100303 p�d
								pObjView->doPopulateData(pObjView->getObjIndex());
								pObjView = NULL;
							}	// if (pObjView != NULL)	

							if ((pProp = pFrame->getPropertiesFormView()) != NULL)
							{
								pProp->recalulateAllHigherCosts();
								// Need to release ref. pointers; 090508 p�d
								pProp = NULL;
							}
						}	// if (pFrame != NULL)
						// Need to release ref. pointers; 080520 p�d
						pFrame = NULL;
						bUpdated = TRUE;
					}	// if (m_pDB->updObject_p30(pObj->getObjID_pk(),sP30Name,nTypeOf,m_vecObjectP30Template[i].getTemplateFile()))
				}	// if (m_pDB) 
			}	// if (m_vecObjectP30Template[i].Compare(sP30Name) == 0 && nTypeOf == m_vecObjectP30Template[i].getTypeOf())
		}	// for (UINT i = 0;i < m_vecObjectP30Template.size();i++)

		// Check if template was found; 111122 Peter, redmine #2496
		if( !bUpdated )
		{
			::MessageBox(this->GetSafeHwnd(),m_sMsgHigherCostsNotFound,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}
	}	// if (::MessageBox(this->GetSafeHwnd(),m_sMsgUpdP30,m_sMsgCap,MB_ICONASTERISK | MB_DEFBUTTON1 | MB_YESNO) == IDYES)
}

void CMDI1950ForrestNormFrame::OnChangeSplitBarOrientation(void)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->setSplitBar();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnUpdateCostTemplate(void)
{
	BOOL bIsObjUpdates = FALSE;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj == NULL) return;
	// Load most resent information on "kostnadsmallar"; 080918 p�d
	getCostTemplateFromDB();

	// Just make sure that index is within limits; 080319 p�d
	if (m_vecTransaction_costtempl.size() > 0 &&
			!m_sBindToCosts.IsEmpty())
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			CTransaction_costtempl rec = m_vecTransaction_costtempl[i];
			if (m_sBindToCosts.CompareNoCase(rec.getTemplateName()) == 0 &&
					rec.getTypeOf() == pObj->getObjTypeOfCosts())
			{
				//AfxMessageBox("OnUpdateCostTemplate\n" + rec.getTemplateName() + "\n" + m_sCostsXML);
				// Only update the active Object; 080408 p�d
				bIsObjUpdates = m_pDB->updObject_cost(pObj->getObjID_pk(),m_sBindToCosts,rec.getTypeOf(),rec.getTemplateFile());
				break;
			}	// if (m_sBindToPricelistSelected.Compare(rec.getName()) == 0)
		}	// for (UINT i = 0;i < m_vecTransactionPricelist.size();i++)
	}
	// If we have an update ob object, i think we should
	// also recaluclate; 080918 p�d
	if (bIsObjUpdates)
			OnCalculateTBtn();

}


void CMDI1950ForrestNormFrame::OnChangeTemplate(void)
{
	CString S;

	TemplateParser pars;
	CTransaction_template recTmpl;
	// OBJECT
	TCHAR szPrlName[127];
	CString sPrlXML;
	int nPrlID,nPrlTypeOf;
	CTransaction_pricelist recPricelist;
	vecTransactionPricelist vecPricelists;
	BOOL bPricelistFound = FALSE;

	TCHAR szCostName[127];
	int nCostID;
	CTransaction_costtempl recCost;
	vecTransaction_costtempl vecCosts;
	BOOL bCostFound = FALSE;

	std::map<int,BOOL> mapSpcOK;
	CString sData,sSpcMsg;
	BOOL sSpcMissmatch = FALSE;
	// STAND(S)
	vecTransactionSpecies vecSpeciesInTmpl;
	CTransaction_species recSpecieInTmpl;
	// Present species in Pricelist used; 090622 p�d
	vecTransactionSpecies vecSpeciesInTmpl_active;
	CTransaction_species recSpecieInTmpl_active;

	vecTransaction_elv_properties vecProps;
	CTransaction_elv_properties recProps;

	vecTransaction_elv_cruise vecCruises;
	CTransaction_elv_cruise recCruises;

	vecUCFunctionList vecFuncList;
	UCFunctionList recFuncList;

	vecTransactionTraktSetSpc vecTraktSetSpc;
	CTransaction_trakt_set_spc recTraktSetSpc;

	xmllitePricelistParser *parsPRL = new xmllitePricelistParser();
	if (parsPRL == NULL) return;

	getTraktTemplatesFromDB();
	CReplaceTemplateDlg *pDlg = new CReplaceTemplateDlg();
	if (pDlg == NULL) return;


	// Get information on present pricelist species, to check
	// that user selected a stand-template (prcicelist) with at least
	// the same species as before; 090622 p�d
	if (parsPRL->loadStream(m_recActiveObject.getObjPricelistXML()))
		parsPRL->getSpeciesInPricelistFile(vecSpeciesInTmpl_active);
	delete parsPRL;

	pDlg->setObjectName(m_recActiveObject.getObjectName(),m_recActiveObject.getObjID_pk());
	pDlg->setTemplates(m_vecTraktTemplate);
	pDlg->setDBConnection(m_pDB);
	if (pDlg->DoModal() == IDOK)
	{
		if (!m_wndProgressDlg.isVisible())
		{
			m_wndProgressDlg.show(SW_SHOW);
			m_wndProgressDlg.setCaption(m_sMsgObject + L" : " + m_recActiveObject.getObjectName());
			m_wndProgressDlg.setLbl2(m_sMsgProperty);
			m_wndProgressDlg.setLbl3(m_sMsgStand);
			m_wndProgressDlg.hideStandInformation();
		}

		m_wndProgressDlg.setActiveAction(m_sMsgReplaceUpdTemplate);
	
		setStatusBarText(1,m_sStatusWorking);

		doEvents();

		recTmpl = pDlg->getSelectedTemplate();
		// Vector containing selected properties; 090918 p�d
		vecProps = pDlg->getPropertiesToChangeTemplate();

		// I think we'll use methods created just for this purpose, to change
		// settings for Object and Stand(s); 090617 p�d
		if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
		{
			// Species in new pricelist; 090622 p�d
			pars.getSpeciesInTemplateFile(vecSpeciesInTmpl);

			//**********************************************************************
			// COMPARE SPECIES IN PRESENT AND NEW PRICELIST; 2009-09-28 p�d
			//**********************************************************************
			if (vecSpeciesInTmpl_active.size() > 0 && vecSpeciesInTmpl.size() > 0)
			{
				for (UINT i1 = 0;i1 < vecSpeciesInTmpl_active.size();i1++)
				{
					recSpecieInTmpl_active = vecSpeciesInTmpl_active[i1];
					mapSpcOK[recSpecieInTmpl_active.getSpcID()] = FALSE;
				}	// for (UINT i1 = 0;i1 < getSpeciesInTemplateFile_active.size();i1++)
				
				for (UINT i2 = 0;i2 < vecSpeciesInTmpl.size();i2++)
				{
					recSpecieInTmpl = vecSpeciesInTmpl[i2];
					mapSpcOK[recSpecieInTmpl.getSpcID()] = TRUE;
				}	// for (UINT i2 = 0;i2 < getSpeciesInTemplateFile.size();i2++)

				sSpcMsg.Empty();
				for (UINT i3 = 0;i3 < vecSpeciesInTmpl_active.size();i3++)
				{
					recSpecieInTmpl_active = vecSpeciesInTmpl_active[i3];
					if (!mapSpcOK[recSpecieInTmpl_active.getSpcID()])
					{
						sData.Format(_T("- %s\n"),recSpecieInTmpl_active.getSpcName());
						sSpcMsg += sData;
						sSpcMissmatch = TRUE;
					}	// if (!mapSpcOK[recSpecieInTmpl_active.getSpcID])
				}	// for (UINT i3 = 0;i3 < vecSpeciesInTmpl_active.size();i3++)

				if (sSpcMissmatch)
				{
					sData = m_sChangeTemplateMsg1 + sSpcMsg + _T("\n\n") + m_sChangeTemplateMsg2;
					if (::MessageBox(this->GetSafeHwnd(),sData,m_sMsgCap,MB_ICONASTERISK | MB_DEFBUTTON2 | MB_YESNO) == IDNO)
					{
						m_wndProgressDlg.show(SW_HIDE);
						return;
					}
				}
			}	// if (getSpeciesInTemplateFile.size() > 0 && getSpeciesInTemplateFile_active.size() > 0)


			//**********************************************************************
			// UPDATE INFORMATION IN OBJECT; 2009-09-28 p�d
			//**********************************************************************

			//-------------------------------------------------------------------------------------------		
			// Update pricelist; 090617 p�d
			m_pDB->getPricelists(vecPricelists);

			pars.getTemplatePricelist(&nPrlID,szPrlName);
			pars.getTemplatePricelistTypeOf(&nPrlTypeOf);
			// Try to seekout the Pricelist-template in vecPricelists and to get
			// the XML-file; 080409 p�d
			for (UINT i = 0;i < vecPricelists.size();i++)
			{
				recPricelist = vecPricelists[i];
				if (recPricelist.getID() == nPrlID &&
						recPricelist.getTypeOf() == nPrlTypeOf &&
						recPricelist.getName().Compare(szPrlName) == 0)
				{
					bPricelistFound = TRUE;
					break;
				}	// if (recCost.getTypeOf() == nCostType &&
			}	// for (UINT i = 0;i < vecCost.size();i++)


			if (bPricelistFound)
			{
				// Save data for Pricelist; 090617 p�d
				// OBS! Only do update of the active object; 090617 p�d
				m_pDB->updObject_prl(m_recActiveObject.getObjID_pk(),recPricelist.getName(),recPricelist.getTypeOf(),recPricelist.getPricelistFile());
			}
			//-------------------------------------------------------------------------------------------		
			// Update costs; 090617 p�d
			m_pDB->getCosts(vecCosts);

			pars.getTemplateCostsTmpl(&nCostID,szCostName);
			// Try to seekout the Cost-template in vecCost and to get
			// the XML-file; 090617 p�d
			for (UINT i = 0;i < vecCosts.size();i++)
			{
				recCost = vecCosts[i];
				if (recCost.getID() == nCostID &&
						recCost.getTemplateName().Compare(szCostName) == 0)
				{
					bCostFound = TRUE;
					break;
				}	// if (recCost.getTypeOf() == nCostType &&
			}	// for (UINT i = 0;i < vecCost.size();i++)
			if (bCostFound)
			{
				// Save data for Costs; 090617 p�d
				// OBS! Only do update of the active object; 090617 p�d
				m_pDB->updObject_cost(m_recActiveObject.getObjID_pk(),recCost.getTemplateName(),recCost.getTypeOf(),recCost.getTemplateFile());
			}

			//H�mta in korrekt s�derbergsinformation h�r och mellanlandar dessa p� objektet f�r att sedan kunna uppdatera best�ndets s�derbergsinst�llningar med dessa
			//Bug #2653 20111213 J�
			//Prislista och s�derbergs etc verkar mellanlanda i objektet av n�gon anledning vilket g�r det knepigt att f�lja d� byta best�ndsmall 
			//knappast skall beh�va inbegripa objektet och dess data, men nu fungerar det s� pga kvarleva fr�n b�rjan av programmet d� 
			//prislisteinfo etc l�g p� objektet.
			GetSoderbergFromNewTemplate(pars,&m_bBindToIsAtCoast,&m_bBindToIsSouthEast,&m_bBindToIsRegion5,&m_bBindToIsPartOfPlot,&m_sBindToSIH100_pine);
		}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))


		



		//**********************************************************************
		//	UPDATE INFORMATION IN TRAKTS FOR THIS OBJECT AND 
		//	CONNECTED PROPERTIES; 2009-09-28 p�d
		//**********************************************************************
		updateTraktMiscDataPerProperty(vecProps);

		CMDI1950ForrestNormFormView *pView = NULL;
		CPropertiesFormView *pPropView = NULL;
		if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
		{
			if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
			{
				if (pPropView->updateAllTraktTemplates(recTmpl,vecProps))
				{
					// Reload Active object into m_recActiveObject record; 090623 p�d
					m_pDB->getObject(m_recActiveObject.getObjID_pk(),m_recActiveObject);
					// Reset data in Settings; 090928 p�d
					setPopulateSettings();
					// Force recalculation; 090928 p�d
					m_dForceCompleteReCalc = true;
					OnCalculateTBtn();
					// Unfore recalculation; 090928 p�d
					m_dForceCompleteReCalc = false;
				}	// if (pPropView->updateAllTraktTemplates(recTmpl.getTemplateFile(),vecProps))
			}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
		pView = NULL;
		pPropView = NULL;
	}

	delete pDlg;
}

void CMDI1950ForrestNormFrame::GetSoderbergFromNewTemplate(TemplateParser pars,BOOL *bBindToIsAtCoast,BOOL *bBindToIsSouthEast,BOOL *bBindToIsRegion5,BOOL *bBindToIsPartOfPlot,CString *sBindToSIH100_pine)
{
	TCHAR szSI_H100_Pine[127];
	pars.getIsAtCoast(bBindToIsAtCoast);
	pars.getIsSouthEast(bBindToIsSouthEast);
	pars.getIsRegion5(bBindToIsRegion5);
	pars.getPartOfPlot(bBindToIsPartOfPlot);	
	pars.getSIH100_Pine(szSI_H100_Pine);
	*sBindToSIH100_pine=szSI_H100_Pine;
}

void CMDI1950ForrestNormFrame::OnAddOtherCosts(void)
{
	int nSelCompType,nIndex;
	double fCompValue = 0.0;
	double fCompCalc = 0.0;
	double fSumOtherComp = 0.0;
	CString sCompNotes;
	vecTransaction_elv_properties vecProps;
	CTransaction_elv_properties recProp;
	CTransaction_elv_properties_other_comp recOtherComp;

	CAddOtherCostsDlg *pDlg = new CAddOtherCostsDlg();
	if (pDlg)
	{
		pDlg->setObject(m_recActiveObject);
		pDlg->setDBConnection(m_pDB);

		if (pDlg->DoModal() == IDOK)
		{
			vecProps = pDlg->getSelectedProperties();
			nSelCompType = pDlg->getSelectedCostType();
			fCompValue = pDlg->getOtherCompValue();
			fCompCalc = pDlg->getOtherCompCalc();
			sCompNotes = pDlg->getOtherCompNotes();

			if (vecProps.size() > 0)
			{
				for (UINT i = 0;i < vecProps.size();i++)
				{
					recProp = vecProps[i];
					
					// Get Next index for Object/Property in table; 100614 p�d
					if (m_pDB)
					{
						nIndex = m_pDB->getPropertyOtherCompNextIndex(recProp.getPropObjectID_pk(),recProp.getPropID_pk());
						if(nSelCompType == 3)// #4083 20140625 J� m�ste ber�kna ers�ttning eftersom den s�tts per m3 gagnvirke per fastighet
						{
							fCompCalc=fCompValue*recProp.getPropWoodVolume();
						}

						fSumOtherComp = recProp.getPropOtherCostValue() + fCompCalc;
						recOtherComp = CTransaction_elv_properties_other_comp(nIndex,
																																	recProp.getPropObjectID_pk(),
																																	recProp.getPropID_pk(),
																																	fCompValue,
																																	fCompCalc,
																																	sCompNotes,
																																	nSelCompType);
	
						m_pDB->addPropertyOtherComp(recOtherComp);
						m_pDB->updObjPropTraktSUM_5(recProp.getPropID_pk(),recProp.getPropObjectID_pk(),fSumOtherComp);
						if (logThis(1))
						{
							//------------------------------------------------------------------------
							// Add information to logbook that other compensation been added; 100812 p�d
							m_pDB->addPropertyLogBook(CTransaction_elv_properties_logbook(recProp.getPropID_pk(),
																																						recProp.getPropObjectID_pk(),
																																						getDateTimeEx2(),
																																						getUserName().MakeUpper(),
																																						m_sLogAddOtherCompMsg + L": " + sCompNotes,
																																						getDateTime(),
																																						-2));

						}
					}	// if (m_pDB)
				}	// for (UINT i = 0;i < vecProps.size();i++)

				// We need to update data in Evaluation and Cruise, in case the entered
				// data from INV-file, have been changed; 090508 p�d
				CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
				if (pFormView != NULL)
				{
					CPropertiesFormView *pProp = pFormView->getPropertiesFormView();
					if (pProp != NULL)
					{
						pProp->populateProperties();
						// Need to release ref. pointers; 090508 p�d
						pProp = NULL;
					}
					// Need to release ref. pointers; 090508 p�d
					pFormView = NULL;
				}
			}	// if (vecProps.size() > 0)
		}	// if (pDlg->DoModal() == IDOK)

		delete pDlg;
	}
}

void CMDI1950ForrestNormFrame::OnGotoGIS(void)
{
	// Bring up GIS; 090929 p�d
	CString sLangFile(getLanguageFN(getLanguageDir(), _T("UMGIS"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV));
	showFormView(888, sLangFile);
	// Return information to GIS-module; 090929 p�d
	CView *pViewGIS = getFormViewByID(888);	// 888 = Identifer for GIS-module; 090924 p�d
	if( pViewGIS )
	{
		pViewGIS->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
													(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("Module888"),_T(""),
													GISMSG_OBJECT,m_recActiveObject.getObjID_pk(),0));

	}
}

void CMDI1950ForrestNormFrame::OnCalculateManuallyEnteredCruises(void)
{


	// Check if data for calculation's ok; 090311 p�d
	if (!isCalculationdataOK()) return;

	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->calculateStormDry();
			pPropView->matchStandsPerPropertyForObject(m_wndProgressDlg);
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	pView = NULL;
	pPropView = NULL;
}



void CMDI1950ForrestNormFrame::OnMatchStandsToAvtiveProperty(void)
{
	doMatchStandsToAvtiveProperty();
}

BOOL CMDI1950ForrestNormFrame::doMatchStandsToAvtiveProperty(bool use_dlg)
{
	BOOL bReturn = TRUE;
	m_nDoCacluateAction = 1;	// => don't run saveObject in method isCalculationdataOK; 101118 p�d
	// Check if data for calculation's ok; 090311 p�d
	if (!isCalculationdataOK()) return FALSE;
	m_nDoCacluateAction = 0;	// => run saveObject in method isCalculationdataOK; 091202 p�d

	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return FALSE;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{

			if (pPropView->getNumOfProperties() > 0)
			{
				//********************************************************
				// This method'll update "esti_trakt_table : S�derbergs"
				// and also "esti_trakt_nisc_data_table : Pricelista,Kostnadsmall,Diam.klass"; 080521 p�d
				// updateTraktMiscDataPerProperty();
				//********************************************************
				if (bReturn = pPropView->matchStandsForActiveProperty(use_dlg))
				{
					// Do an update of higher costs for ALL properties; 100303 p�d
					pPropView->recalulateAllHigherCosts();
				}
			}
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_cruise,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;

	return bReturn;
}

void CMDI1950ForrestNormFrame::OnMatchStandsToProperties(void)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->matchAllStandsPerPropertyForObject();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)

	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnRemoveSelStandFromProperty(void)
{
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->getNumOfProperties() > 0)
			{
				pPropView->removeStandInProperty();
				pPropView->recalulateAllHigherCosts();
			}
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_cruise,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnOpenSelStand(void) {

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CCruisingData * cView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if((cView = (CCruisingData *) pPropView->getCruisingView()) != NULL) {
				cView->openStand();
			}	
		}
	}
	cView = NULL;
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnCreateNewStand(void) { 
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CCruisingData * cView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if((cView = (CCruisingData *) pPropView->getCruisingView()) != NULL) {
				cView->addNewStand();
			}	
		}
	}
	cView = NULL;
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnShowPriceList(void) {
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CCruisingData * cView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if((cView = (CCruisingData *) pPropView->getCruisingView()) != NULL) {
				cView->openPricelist();
			}	
		}
	}
	cView = NULL;
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnShowCostTemplate(void) { 
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CCruisingData * cView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if((cView = (CCruisingData *) pPropView->getCruisingView()) != NULL) {
				cView->openCostPricelist();
			}	
		}
	}
	cView = NULL;
	pView = NULL;
	pPropView = NULL;
}


void CMDI1950ForrestNormFrame::OnAddNewVStandToAvtiveProperty(void)
{
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->getNumOfProperties() > 0)
				pPropView->addNewVStandToActivePropertyWithDialog();
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_eval,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnSaveCalculateVStandInAvtiveProperty()
{
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->getNumOfProperties() > 0)
			{
				// Just give the user a little info. before running the Recalc; 080923 p�d
				if (m_bShowMsg_OnSaveCalculateVStandInAvtiveProperty) 
					::MessageBox(this->GetSafeHwnd(),(m_sRecalcVStandMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
				pPropView->saveAndCalculateVStandInActiveProperty(true);
			}
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_eval,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnSaveCalculateVStands(void)
{
	// Check status of property; 080623 p�d
	//if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->getNumOfProperties() > 0)
				pPropView->saveAndCalculateVStands(m_wndProgressDlg);
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_eval,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnRemoveCalculateVStandFromAvtiveProperty(void)
{
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->getNumOfProperties() > 0)
			{
				// Save if any changes has been made to evaluation stands so that those changes are not overwritten with old DB values 
				// #4353 20150803 J�
				CObjectFormView *pObjView = NULL;
				CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
				BOOL bIsOKToClose=FALSE;
				if (pFrame != NULL)
				{
					// Try to get the ObjectView, and save data; 080409 p�d
					pObjView = pFrame->getObjectFormView();
					if (pObjView != NULL)
					{
						bIsOKToClose = pObjView->doSaveObject(1);
						// Need to release ref. pointers; 080520 p�d
						pObjView = NULL;
					}
					// Need to release ref. pointers; 080520 p�d
					pFrame = NULL;
				}
				if(bIsOKToClose)
				pPropView->removeCalculateVStandFromActiveProperty();
				else
				::MessageBox(this->GetSafeHwnd(),m_sMsgCouldNotSaveObject,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			}
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_eval,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnGroupPropertiesOnLandOwners(void)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->groupPropertiesOnLandOwners();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnGroupPropertiesOnPropertyNumbers(void)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->groupPropertiesOnPropertyNumbers();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	// Need to release ref. pointers; 080520 p�d
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnSavePropertyGroupings(void)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->savePropertyGroupings();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnDelPropertyGroupings(void)
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->removePropertyGroupings();
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	pView = NULL;
	pPropView = NULL;
}

//Added 2016-06-09 PH
void CMDI1950ForrestNormFrame::OnListPropertyOwners(void) {
	//List property owners
	CString sLangFN(getLanguageFN(getLanguageDir(),_T("Forrest"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	
	showFormView(116,sLangFN);
	CTransaction_elv_object *pObj = getActiveObject();
	int id = pObj->getObjID_pk();

	LPARAM lp = (LPARAM)&_doc_identifer_msg(_T("ReportView1"),_T("ReportView1"),_T(""),3 /* 3 = Identifer for UMLandVlaue module */,id,0);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x03,lp);
}

void CMDI1950ForrestNormFrame::OnRecalculateK3M3ForStands(void)
{
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CString S;
	double fTimberVolume = 0.0;
	double fTimberValue = 0.0;
	double fTimberCost = 0.0;
	double fRotNetto = 0.0;
	CStringArray sarrLog;
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CTransaction_elv_cruise_id recCruiseIDs;
	vecTransaction_elv_cruise_id vecCruiseIDs;
	vecTransaction_elv_ass_stands_obj vec;
	CTransaction_elv_properties m_recELV_properties;
	vecTransaction_elv_properties m_vecELV_properties;
	CTransaction_elv_object *pObj = getActiveObject();
	int nPriceSetIn = 2;	// Added 090615 p�d
	CAddKrPerM3Dlg *pDlg = new CAddKrPerM3Dlg();
	if (pDlg != NULL && pObj != NULL)
	{
		// Get ID:s for Trakts for this Object; 080612 p�d
		m_pDB->getObjectAllCruiseStands(pObj->getObjID_pk(),vecCruiseIDs);
		// Get Properties for this Object; 080612 p�d
		m_pDB->getProperties_by_status(pObj->getObjID_pk(),m_vecELV_properties);

		pDlg->setDBConnection(m_pDB);
		pDlg->setObjectID(pObj->getObjID_pk());
		if (pDlg->DoModal() == IDOK)
		{
			pDlg->getReturnData(vec);

			xmllitePricelistParser *parsPRL = new xmllitePricelistParser();
			if (parsPRL)
			{
				if (parsPRL->loadStream(pObj->getObjPricelistXML()))
				{
					parsPRL->getHeaderPriceIn(&nPriceSetIn);
				}
				delete parsPRL;
			}
			// Run recacluation of kr/m3
			for (UINT i1 = 0;i1 < vec.size();i1++)
			{
				m_pDB->calculateK3PerM3OnObjectPropertyStands(pObj->getObjID_pk(),vec[i1].getKrPerM3(),vec[i1].getAssortName(),nPriceSetIn);
			}	// for (UINT i1 = 0;i1 < vec.size();i1++)
			if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
			{
				if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
				{
					if (vecCruiseIDs.size() > 0)
					{
						for (UINT i2 = 0;i2 < vecCruiseIDs.size();i2++)
						{
							recCruiseIDs = vecCruiseIDs[i2];
							calcRotpostForTrakt(m_bConnected,m_dbConnectionData,recCruiseIDs.getTraktID());
							// Get new calculated Rotpost (Value); 080613 p�d
							m_pDB->getObjPropTrakt_rotpost(recCruiseIDs.getTraktID(),&fTimberVolume,&fTimberValue,&fTimberCost,&fRotNetto);	
							// Update "elv_cruise_table" on rotpost (value); 080613 p�d
							m_pDB->updObjectCruise_rotpost(recCruiseIDs.getTraktID(),recCruiseIDs.getPropID(),recCruiseIDs.getObjID(),
																						 fTimberVolume,fTimberValue,fTimberCost,fRotNetto);
						}	// }	// for (UINT i2 = 0;i2 < vecCruiseIDs.size();i2++)
					}	// if (vecStandIndexes.size() > 0)

					if (m_vecELV_properties.size() > 0)
					{
						for (UINT i3 = 0;i3 < m_vecELV_properties.size();i3++)
						{
							m_recELV_properties = m_vecELV_properties[i3];
							// Update SUM data on Rotpost for each Property in DB; 080613 p�d
							m_pDB->getObjPropTraktSUM_rotpost(m_recELV_properties.getPropObjectID_pk(),
																								m_recELV_properties.getPropID_pk(),
																								&fTimberVolume,&fTimberValue,&fTimberCost,&fRotNetto);
							m_pDB->updObjPropTraktSUM_6(m_recELV_properties.getPropID_pk(),
																					m_recELV_properties.getPropObjectID_pk(),
																					fTimberVolume,fTimberValue,fTimberCost,fRotNetto);
						}	// for (UINT i3 = 0;i3 < m_vecELV_properties.size();i3++)

						// Populate data; 080613 p�d
						// OBS! We'll populate the first property; 080613 p�d
						m_recELV_properties = m_vecELV_properties[0];	// <-- Points to first Property; 080613 p�d
						pPropView->getCruisingView()->populateReport(pPropView->getFocusedPropertyID(),
																												 m_recELV_properties.getPropObjectID_pk(),
																												 m_recELV_properties.getPropStatus(),
																												 pObj->getObjUseNormID());

						// Repopulate properties; 080613 p�d	
						pPropView->populateProperties();
					}	// if (m_vecELV_properties.size() > 0)
				}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
			}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)			
		}	// if (pDlg.DoModal() == IDOK)

		pObj = NULL;
		pView = NULL;
		pPropView = NULL;
		delete pDlg;
	}
}

void CMDI1950ForrestNormFrame::OnChangeStatusForStands(void)
{
	vecTransaction_elv_properties vec;
	CString sStatusSet;
	int m_nStatusIndex;
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	CChangePropStatusDlg *pDlg = new CChangePropStatusDlg();
	CTransaction_elv_object *pObj = getActiveObject();
	if (pDlg != NULL && pObj != NULL)
	{
		pDlg->setDBConnection(m_pDB);
		pDlg->setObjectID(pObj->getObjID_pk());
		if (pDlg->DoModal() == IDOK)
		{
			pDlg->getReturnData(vec,sStatusSet,&m_nStatusIndex);
			if (vec.size() > 0)
			{
				for (UINT i = 0;i < vec.size();i++)
				{
					m_pDB->updPropertyStatus(vec[i].getPropID_pk(),vec[i].getPropObjectID_pk(),m_nStatusIndex);
					// I think we could add a note to the logbook here; 080604 p�d
					CTransaction_elv_properties_logbook recLogBook = CTransaction_elv_properties_logbook(vec[i].getPropID_pk(),
																																															 vec[i].getPropObjectID_pk(),
																																															 getDateTimeEx2(),
																																															 getUserName().MakeUpper() ,
																																															 sStatusSet,
																																															 _T(""),
																																															 m_nStatusIndex);
					if (m_pDB != NULL && logThis(2))
						m_pDB->addPropertyLogBook(recLogBook);

				}	// for (UINT i = 0;i < vec.size();i++)
			}	// if (vec.size() > 0)
		}	// if (pDlg->DoModal() == IDOK)
		delete pDlg;
		// Repopulate
		if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
		{
			if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
			{
				// Repopulate properties; 080613 p�d	
				pPropView->populateProperties();
			}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)			

	}	// if (pDlg != NULL)

	pObj = NULL;
	pView = NULL;
	pPropView = NULL;

}

void CMDI1950ForrestNormFrame::OnCreateSetupFileForCaliper(void)
{
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		// Make sure we have a directory, to add setup-files to; 080616 p�d
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		doCreateObjectDataDirectory(sObjMapp,SETUP_DATA_DIR);

		CHandleSetupFilesDlg *pDlg = new CHandleSetupFilesDlg();
		if (pDlg != NULL)
		{
			pDlg->setDBConnection(m_pDB);
			pDlg->setObjectID(pObj->getObjID_pk());
			pDlg->setPricelistXML(pObj->getObjPricelistXML());
			if (pDlg->DoModal() == IDOK)
			{
			}
			delete pDlg;
		}	// if (pDlg != NULL)

		pObj = NULL;
	}
}

void CMDI1950ForrestNormFrame::OnSendSetupToCaliper(void)
{
	CString m_sSetupDataDir;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		m_sSetupDataDir = getObjectSetupDirectory(sObjMapp) + formatData(_T("\\%s"),SETUP_FILE_WC);
		CFileDialog dlg(TRUE,
										(SETUP_FILE_DEF_EXT),
										m_sSetupDataDir,
										OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_EXPLORER,
										_T("Setup (*.sup)|*.sup|"));
		if (dlg.DoModal() == IDOK)
		{
			UpdateWindow();

//			AfxMessageBox("Transfer to caliper not implemented : 2008-06-18");
			// Tell the Communication-suite to receive files in inventory-dir; 080616 p�d
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, 
				(LPARAM)&_user_msg(401, _T("OpenSuiteEx"), _T("Communication.dll"), _T(""), dlg.GetPathName(), _T("")));
		}
		pObj = NULL;
	}

}

void CMDI1950ForrestNormFrame::OnSendSetupToDPII(void)
{
	CString m_sSetupDataDir;
	CString sObjMapp;
	CString sCaliperPath = _T("DATA\\ESTIM.KRFT\\");
	CString sDest;
	CTransaction_elv_object *pObj = getActiveObject();
	DWORD dwSerial, dwMaxComp, dwFlags, dwSize;
	TCHAR szVolume[MAX_PATH], szName[MAX_PATH], szFS[MAX_PATH], szPath[MAX_PATH] = {'\0'};
	RLFReader *xml = new RLFReader;
	if (xml->Load(m_sLangFN))
	{
		if (pObj != NULL)
		{
			// Go through devices, look for DPII		
			HANDLE hVolume = FindFirstVolume(szVolume, sizeof(szVolume)/sizeof(TCHAR));		
			while(1)		
			{		
					if( hVolume != INVALID_HANDLE_VALUE )		
					{		
							if( GetVolumeInformation(szVolume, szName, sizeof(szName)/sizeof(TCHAR), &dwSerial, &dwMaxComp, &dwFlags, szFS, sizeof(szFS)/sizeof(TCHAR)) )		
							{		
									if( _tcscmp(szName, _T("DPII")) == 0  )		
									{		
											GetVolumePathNamesForVolumeName(szVolume, szPath, sizeof(szPath)/sizeof(TCHAR), &dwSize);		
											break;		
									}		
							}		
					}		
			
					if( !FindNextVolume(hVolume, szVolume, sizeof(szVolume)/sizeof(TCHAR)) )		
					{		
							break;		
					}		
			}

			// Copy setup file to caliper
			if( szPath[0] )
			{
				sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
				sObjMapp = checkDirectoryName(sObjMapp);
				m_sSetupDataDir = getObjectSetupDirectory(sObjMapp) + formatData(_T("\\%s"),SETUP_FILE_WC);
				CFileDialog dlg(TRUE,
												(SETUP_FILE_DEF_EXT),
												m_sSetupDataDir,
												OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_EXPLORER,
												_T("Setup (*.sup)|*.sup|"));
				if (dlg.DoModal() == IDOK)
				{
					UpdateWindow();

					sDest.Format(_T("%s%s%s"), szPath, sCaliperPath, dlg.GetFileName());
					CopyFile(dlg.GetPathName(), sDest, FALSE);
				}
				pObj = NULL;
			}
			else
			{
				AfxMessageBox(xml->str(IDS_STRING4309), MB_ICONINFORMATION);
			}
		}
	}
}

void CMDI1950ForrestNormFrame::OnSendSetupByEMail(void)
{
	CIMapi mail;
	CString sMsgText;
	CString m_sSetupDataDir;
	CString m_sSelectedSetupFiles;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		m_sSetupDataDir = getObjectSetupDirectory(sObjMapp) + formatData(_T("\\%s"),SETUP_FILE_WC);
		CFileDialog dlg(TRUE,
										(SETUP_FILE_DEF_EXT),
										m_sSetupDataDir,
										OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT | OFN_EXPLORER,
										_T("Setup (*.sup)|*.sup|"));
		if (dlg.DoModal() == IDOK)
		{
			UpdateWindow();
			if (mail.Error() == 0) //IMAPI_SUCCESS)
			{
				// Subject (�mne)
				mail.Subject(m_sMsgEMailSubject);

				// Make sure the file actually exists
				POSITION pos = dlg.GetStartPosition();
				while (pos)
				{
					m_sSelectedSetupFiles = dlg.GetNextPathName(pos);
					// Attached file(s)
					mail.Attach(m_sSelectedSetupFiles);
				}	// while (pos)

				// "Br�dtext"
				sMsgText.Format(_T("%s %s"),m_sMsgEMailText,getDBDateTime());
				mail.Text((sMsgText));
				mail.Send();
				
			}	// if (mail.Error() == 0)
			else
			{
				// Do something appropriate to the error...
				AfxMessageBox(_T("No E-Mail client")); //_T("No email-client installed!"));
			}
		}	// if (dlg.DoModal() == IDOK)

		pObj = NULL;
	}
}

void CMDI1950ForrestNormFrame::OnReciveINVDataFromCaliperDP(void)
{
	CString m_sInvDataDir;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		doCreateObjectDataDirectory(sObjMapp,INVENTORY_DIR);

		m_sInvDataDir = getObjectInventoryDirectory(sObjMapp);
		// Tell the Communication-suite to receive files in inventory-dir; 080616 p�d
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, 
			(LPARAM)&_user_msg(402, _T("OpenSuiteEx"), _T("Communication.dll"), _T(""), m_sInvDataDir, _T("")));

		pObj = NULL;
	}
}

static int CALLBACK BrowseCallbackProc(HWND hwnd,UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	// If the BFFM_INITIALIZED message is received
	// set the path to the start path.
	switch (uMsg)
	{
		case BFFM_INITIALIZED:
		{
			if (NULL != lpData)
			{
				SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
			}
		}
	}

	return 0; // The function should always return 0.
}

void CMDI1950ForrestNormFrame::OnReciveINVDataFromCaliperDPII(void)
{
	TCHAR szVolume[MAX_PATH], szName[MAX_PATH], szFS[MAX_PATH], szPath[MAX_PATH] = {'\0'}, szDest[MAX_PATH];
	TCHAR szData1[] = _T("DATA\\ESTIM.KRFT\\");
	TCHAR szData2[] = _T("DATA\\ESTIM PRO\\");
	DWORD dwSerial, dwMaxComp, dwFlags, dwSize;
	BOOL bFound = FALSE;
	RLFReader *xml = new RLFReader;
	
	if (!xml->Load(m_sLangFN))
		return;

	// Go through devices, look for DPII
	HANDLE hVolume = FindFirstVolume(szVolume, sizeof(szVolume)/sizeof(TCHAR));
	while(1)
	{
		if( hVolume != INVALID_HANDLE_VALUE )
		{
			if( GetVolumeInformation(szVolume, szName, sizeof(szName)/sizeof(TCHAR), &dwSerial, &dwMaxComp, &dwFlags, szFS, sizeof(szFS)/sizeof(TCHAR)) )
			{
				if( _tcscmp(szName, _T("DPII")) == 0  )
				{
					GetVolumePathNamesForVolumeName(szVolume, szPath, sizeof(szPath)/sizeof(TCHAR), &dwSize);
					break;
				}
			}
		}

		if( !FindNextVolume(hVolume, szVolume, sizeof(szVolume)/sizeof(TCHAR)) )
		{
			break;
		}
	}

	if( szPath[0] )
	{
		// Check if files exist on caliper
		CString strSrc, strDest;
		strSrc.Format(_T("%s%s*.HXL"), szPath, szData1);
		WIN32_FIND_DATA ffd;
		HANDLE hFile = FindFirstFile( strSrc, &ffd );
		while(1) // First folder
		{
			if( hFile != INVALID_HANDLE_VALUE && !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
			{
				bFound = TRUE;
				break;
			}

			if( !FindNextFile(hFile, &ffd) )
			{
				break;
			}
		}
		if( !bFound ) // Second folder
		{
			strSrc.Format(_T("%s%s*.HXL"), szPath, szData2);
			HANDLE hFile = FindFirstFile( strSrc, &ffd );
			while(1)
			{
				if( hFile != INVALID_HANDLE_VALUE && !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
				{
					bFound = TRUE;
					break;
				}

				if( !FindNextFile(hFile, &ffd) )
				{
					break;
				}
			}
		}

		// Move files to dir chosen by user
		if( bFound )
		{
			BOOL bFound = FALSE;
			BROWSEINFO bi = {0};
			bi.lpszTitle = xml->str(IDS_STRING4307);
			CString sObjMapp; sObjMapp.Format(_T("%s_%s"),getActiveObject()->getObjectName(),getActiveObject()->getObjIDNumber());
			CString m_sInvDataDir = getObjectInventoryDirectory(sObjMapp);
			bi.lpfn = BrowseCallbackProc;
			bi.lParam = (LPARAM)m_sInvDataDir.GetBuffer();
			LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
			if( pidl )
			{
				SHGetPathFromIDList(pidl, szDest);

				// First folder
				strSrc.Format(_T("%s%s*.HXL"), szPath, szData1);
				hFile = FindFirstFile( strSrc, &ffd );
				while(1)
				{
					if( hFile != INVALID_HANDLE_VALUE && !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
					{
						strSrc.Format(_T("%s%s%s"), szPath, szData1, ffd.cFileName);
						strDest.Format(_T("%s\\%s"), szDest, ffd.cFileName);
						DeleteFile(strDest);
						MoveFile(strSrc, strDest);
					}

					if( !FindNextFile(hFile, &ffd) )
					{
						break;
					}
				}

				// Second folder
				strSrc.Format(_T("%s%s*.HXL"), szPath, szData2);
				hFile = FindFirstFile( strSrc, &ffd );
				while(1)
				{
					if( hFile != INVALID_HANDLE_VALUE && !(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
					{
						strSrc.Format(_T("%s%s%s"), szPath, szData2, ffd.cFileName);
						strDest.Format(_T("%s\\%s"), szDest, ffd.cFileName);
						DeleteFile(strDest);
						MoveFile(strSrc, strDest);
					}

					if( !FindNextFile(hFile, &ffd) )
					{
						break;
					}
				}

				// Write path to registry for UMIndata
				regSetStr(REG_ROOT, _T("UMIndata"), _T("LastImportDir"), szDest);

				// Bring up import dialog
				OnCreateFormINVDataOnObjectAndProperties();
			}
		}
		else
		{
			AfxMessageBox(xml->str(IDS_STRING4308), MB_OK|MB_ICONINFORMATION);
		}
	}
	else
	{
		AfxMessageBox(xml->str(IDS_STRING4309), MB_OK|MB_ICONINFORMATION);
	}
}

void CMDI1950ForrestNormFrame::OnCreateFormINVDataOnObjectAndProperties(void)
{
	// Check status of property; 080623 p�d
	//if (!doesStatusAllowChange(FALSE)) return;

	CString sLangFN;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	CObjectFormView *pObjView = NULL;

	if (pObj != NULL)
	{

		// Save Cruise data before running UMInData; 091202 p�d
		CMDI1950ForrestNormFormView *pView = (CMDI1950ForrestNormFormView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{	
			if ((pObjView = pView->getObjectFormView()) != NULL)	
			{
				pObjView->saveObject(1);
			}
		}	// if (pView != NULL)
		// Jusr do this here also. In case the inv-directory haven't
		// already been created; 080616 p�d
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		doCreateObjectDataDirectory(sObjMapp,INVENTORY_DIR);
		// Get Module language filename; 080616 p�d
		sLangFN = getLanguageFN(getLanguageDir(),(UMINDATA_PROGRAM_NAME),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		showFormView(866,sLangFN);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
			(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("Module866"),_T("Module5200"),0,pObj->getObjID_pk(),0));

		pObj = NULL;
	}
}

void CMDI1950ForrestNormFrame::OnCreateExcelData(void)
{
	CFileDialog dlgFile (TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("*.xls, *.xlsx|*.xls; *.xlsx||"), this);
	if (dlgFile.DoModal() == IDOK)
	{
		CImportExcelDlg dlg(dlgFile.GetPathName(), m_pDB);
		if (dlg.DoModal() == IDOK)
		{
			// Write path to registry for UMIndata
			regSetStr(REG_ROOT, _T("UMIndata"), _T("LastImportDir"), dlg.GetOutputPath());

			// Bring up import dialog
			OnCreateFormINVDataOnObjectAndProperties();
		}
	}
}

void CMDI1950ForrestNormFrame::OnCreateEvaluationFromCruise()
{
	doCreateEvaluationFromCruise();
}

void CMDI1950ForrestNormFrame::doCreateEvaluationFromCruise(bool show_msg)
{
	m_bCalculateEvalFromCruise = TRUE;
	// Check status of property; 080623 p�d
	if (!doesStatusAllowChange(FALSE)) return;

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->getNumOfProperties() > 0)
			{
				/*
				if (pPropView->isThereOnlyRandTrees())
				{
					// Tell user that stands only includes rand-trees, i.e. NO EVALUATIONSTANDS; 090330 p�d
					::MessageBox(this->GetSafeHwnd(),(m_sMsgNoEvaluationStandsOnRandTrees),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
					// Remove Eavlauation-stands in Cruise isn't valid; 090330 p�d
					pPropView->removeAllCalculateVStands(m_wndProgressDlg);
					pPropView->populateProperties();
					pPropView->setActivePropertyInReport();
					m_bCalculateEvalFromCruise = FALSE;
					pView = NULL;
					pPropView = NULL;
					return;
				}
				*/

				// Ask user if he realy want to do this; 080916 p�d
				if (show_msg)
				{
					if (::MessageBox(this->GetSafeHwnd(),(m_sMsgCreateEvaluetionsFromStands),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
					{
						m_bCalculateEvalFromCruise = FALSE;
					}
					else
					{
						// First; we'll remove the VStands already entered; 080924 p�d
						pPropView->removeAllCalculateVStandsFromActiveProperty();
						// Second; create evaluationstands from cruised stands; 080924 p�d
						pPropView->calculateEvaluationStandFromCruise();
					}
				}
				else
				{
					// First; we'll remove the VStands already entered; 080924 p�d
					pPropView->removeAllCalculateVStandsFromActiveProperty();
					// Second; create evaluationstands from cruised stands; 080924 p�d
					pPropView->calculateEvaluationStandFromCruise();
				}
			}
			else
				::MessageBox(this->GetSafeHwnd(),m_sMsgNoProperties_cruise,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	pView = NULL;
	pPropView = NULL;
}

void CMDI1950ForrestNormFrame::OnCreateAllEvaluationsFromCruise()
{
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	m_bCalculateEvalFromCruise = TRUE;
	// Check status of property; 080623 p�d
//	if (!doesStatusAllowChange(FALSE)) return;

	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			if (pPropView->isThereOnlyRandTrees())
			{
				// Tell user that stands only includes rand-trees, i.e. NO EVALUATIONSTANDS; 090330 p�d
				::MessageBox(this->GetSafeHwnd(),(m_sMsgNoEvaluationStandsOnRandTrees),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
				// Remove Eavlauation-stands in Cruise isn-t valid; 090330 p�d
				pPropView->removeAllCalculateVStands(m_wndProgressDlg);
				// Second; create evaluationstands from cruised stands; 081022 p�d
				pPropView->calculateAllEvaluationStandsFromCruises(m_wndProgressDlg);
				m_bCalculateEvalFromCruise = FALSE;
				pView = NULL;
				pPropView = NULL;
				return;
			}
		}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
	}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgCreateEvaluetionsFromStands),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
	{
		m_bCalculateEvalFromCruise = FALSE;
		pView = NULL;
		pPropView = NULL;
		return;
	}

	if (pView != NULL && pPropView != NULL)
	{
		// First; we'll remove the VStands already entered; 081022 p�d
		pPropView->removeAllCalculateVStands(m_wndProgressDlg);
		// Second; create evaluationstands from cruised stands; 081022 p�d
		pPropView->calculateAllEvaluationStandsFromCruises(m_wndProgressDlg);	
		pView = NULL;
		pPropView = NULL;
	}	// if (pView != NULL && pPropView!= NULL)

}

//#3385 s�tt objektets status
void CMDI1950ForrestNormFrame::setObjectStatus(int status)
{
	CObjectFormView *pObjView = NULL;
	CObjectSelListFormView* pListView = NULL;

	if(status == 1)
	{
		if(::MessageBox(this->GetSafeHwnd(),m_sSetAsFinished,m_sMsgCap,MB_ICONQUESTION|MB_YESNO) != IDYES)
			return;
	}
	else if(status == 0)
	{
		if(::MessageBox(this->GetSafeHwnd(),m_sSetAsOngoing,m_sMsgCap,MB_ICONQUESTION|MB_YESNO) != IDYES)
			return;
	}
	else
		return;

	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and save data; 080409 p�d
		pObjView = pFrame->getObjectFormView();
		if (pObjView != NULL)
		{
			pObjView->setObjectStatus(status);
			// Need to release ref. pointers; 080520 p�d
			pObjView = NULL;

			//g�r en refresh p� listv�ljaren ifall den �r uppe
			pListView = (CObjectSelListFormView*)getFormViewByID(IDD_FORMVIEW_SELLIST);
			if(pListView)
			{
				pListView->Refresh();
				pListView = NULL;
			}
		}
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}
}

//#3385 s�tt objektets status till slutf�rt
void CMDI1950ForrestNormFrame::OnSetObjectFinished(void)
{
	setObjectStatus(1);
}

//#3385 s�tt objektets status till p�g�ende
void CMDI1950ForrestNormFrame::OnSetObjectOngoing(void)
{
	setObjectStatus(0);
}

//3385 visa objekt
void CMDI1950ForrestNormFrame::showObjectStatus(int status)
{
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	CObjectSelListFormView* pListView = NULL;

	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and save data; 080409 p�d
		pObjView = pFrame->getObjectFormView();
		if (pObjView != NULL)
		{
			pObjView->setShowObjectStatus(status);
			// Need to release ref. pointers; 080520 p�d
			pObjView = NULL;

			nShowObjectStatusFlag = status;	//s�tt flagga f�r vad som visas
		}
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;

		//g�r en refresh p� listv�ljaren ifall den �r uppe
		pListView = (CObjectSelListFormView*)getFormViewByID(IDD_FORMVIEW_SELLIST);
		if(pListView)
		{
			pListView->Refresh();
			pListView = NULL;
		}
	}	
}

//#3385 visa endast p�g�ende objekt
void CMDI1950ForrestNormFrame::OnShowObjectOngoing(void)
{
	showObjectStatus(0);
}

//#3385 visa endast slutf�rda objekt
void CMDI1950ForrestNormFrame::OnShowObjectFinished(void)
{
	showObjectStatus(1);
}

//#3385 visa alla objekt
void CMDI1950ForrestNormFrame::OnShowObjectAll(void)
{
	showObjectStatus(2);
}

//#3386 Kolla om det finns n�got objekt satt som slutf�rd
BOOL CMDI1950ForrestNormFrame::getIsAnyObjectFinished(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			return m_pDB->isAnyObjectFinished();
		}
	}
	return FALSE;
}

//#3386 Kolla om det finns n�got objekt satt som p�g�ende
BOOL CMDI1950ForrestNormFrame::getIsAnyObjectOngoing(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			return m_pDB->isAnyObjectOngoing();
		}
	}
	return FALSE;
}


// Enable/Disable toolbar buttons; 080121 p�d

void CMDI1950ForrestNormFrame::OnUpdateGotoGIS(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bGISInstalled && m_bIsGISModule);
}

void CMDI1950ForrestNormFrame::OnUpdateContractorTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableContractorTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdateImportPropertiesTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableImportPropertiesTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdateCalculateTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableCalculateTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdateUpdateTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableUpdateTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdateRemoveTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableRemoveTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdateExportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableExportTBtn);
}


void CMDI1950ForrestNormFrame::OnUpdateToolTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdateExtDocTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableExtDocTBtn);
}

void CMDI1950ForrestNormFrame::OnUpdatePrintOutTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnablePrintOutTBtn);
}



/////////////////////////////////////////////////////////////////////////////
// CMDI1950ForrestNormListFrame


IMPLEMENT_DYNCREATE(CMDI1950ForrestNormListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDI1950ForrestNormListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDI1950ForrestNormListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDI1950ForrestNormListFrame::CMDI1950ForrestNormListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDI1950ForrestNormListFrame::~CMDI1950ForrestNormListFrame()
{
}

void CMDI1950ForrestNormListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_1950_FORREST_NORM_LIST_ENTRY_KEY);
	SavePlacement(this, csBuf);
}

int CMDI1950ForrestNormListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDI1950ForrestNormListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDI1950ForrestNormListFrame diagnostics

#ifdef _DEBUG
void CMDI1950ForrestNormListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDI1950ForrestNormListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDI1950ForrestNormListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDI1950ForrestNormListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDI1950ForrestNormListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_1950_FORREST_NORM_LIST_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDI1950ForrestNormListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDI1950ForrestNormListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_1950_FORREST_NORM_LIST_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_1950_FORREST_NORM_LIST_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDI1950ForrestNormListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDI1950ForrestNormListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDI1950ForrestNormListFrame::setLanguage()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIP30TemplateFrame

IMPLEMENT_DYNCREATE(CMDIP30TemplateFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIP30TemplateFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIP30TemplateFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()

	ON_COMMAND(ID_TBTN_ADD_SPC, OnAddSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD_SPC, OnUpdateSpcTBtnClick)

	ON_COMMAND(ID_TBTN_DEL_SPC, OnRemoveSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DEL_SPC, OnUpdateRemoveSpcTBtnClick)

	ON_COMMAND(ID_TBTN_IMPORT, OnImportTBtnClick)
	//ON_UPDATE_COMMAND_UI(ID_TBTN_IMPORT, OnUpdateImportTBtnClick)

	ON_COMMAND(ID_TBTN_EXPORT, OnExportTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtnClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIP30TemplateFrame::CMDIP30TemplateFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableTBtns = FALSE;
	m_bAlreadySaidOkToClose = FALSE;
}

CMDIP30TemplateFrame::~CMDIP30TemplateFrame()
{
}

void CMDIP30TemplateFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_TEMPLATE_ENTRY_KEY);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnDestroy();

}

void CMDIP30TemplateFrame::OnClose(void)
{
	BOOL bDoClose = FALSE;

	bDoClose = m_bAlreadySaidOkToClose || okToClose();

	if (bDoClose)
	{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

		// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		CP30TemplateFormView *pView = (CP30TemplateFormView*)getFormViewByID(IDD_FORMVIEW2);
		if (pView != NULL)
		{
			pView->saveP30Template(-1);
			// Need to release ref. pointers; 080520 p�d
			pView = NULL;
		}	// if (pView)
		CMDIChildWnd::OnClose();
	}
}

int CMDIP30TemplateFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;

	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	CString sTBResFN = getToolBarResourceFN();
	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.EnableDocking(xtpFlagStretched);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{
				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR2)
						{		
							setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_ADD,xml->str(IDS_STRING2267));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_REMOVE,xml->str(IDS_STRING2268));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_IMPORT,xml->str(IDS_STRING22699));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_EXPORT,xml->str(IDS_STRING22700));	//
						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())
			}	// if (fileExists(sTBResFN))
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CMDIP30TemplateFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

BOOL CMDIP30TemplateFrame::okToClose(void)
{
	BOOL bIsOKToClose = FALSE;

	CObjectFormView *pObjView = NULL;
	CP30TemplateFormView *pFrame = (CP30TemplateFormView *)getFormViewByID(IDD_FORMVIEW2);
	if (pFrame != NULL)
	{
		bIsOKToClose = pFrame->saveP30Template(2);
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}

	return bIsOKToClose;
}

BOOL CMDIP30TemplateFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIP30TemplateFrame diagnostics

#ifdef _DEBUG
void CMDIP30TemplateFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIP30TemplateFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDIP30TemplateFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIP30TemplateFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_TEMPLATE_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIP30TemplateFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CP30TemplateFormView *pView = (CP30TemplateFormView*)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}


	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIP30TemplateFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIP30TemplateFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIP30TemplateFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW4,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}

	return 0L;
}

// MY METHODS
void CMDIP30TemplateFrame::setLanguage()
{
}

void CMDIP30TemplateFrame::OnAddSpecieTBtnClick(void)
{
	CP30TemplateFormView *pView = (CP30TemplateFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->addSpecieToReport();
		pView = NULL;
	}
}

void CMDIP30TemplateFrame::OnRemoveSpecieTBtnClick(void)
{
	CP30TemplateFormView *pView = (CP30TemplateFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->removeSelectedSpecie();
		pView = NULL;
	}
}

void CMDIP30TemplateFrame::OnImportTBtnClick(void)
{
	CP30TemplateFormView *pView = (CP30TemplateFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->importP30Table();
		pView = NULL;
	}
}

void CMDIP30TemplateFrame::OnExportTBtnClick(void)
{
	CP30TemplateFormView *pView = (CP30TemplateFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->exportP30Table();
		pView = NULL;
	}
}


void CMDIP30TemplateFrame::OnUpdateSpcTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30TemplateFrame::OnUpdateRemoveSpcTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30TemplateFrame::OnUpdateImportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30TemplateFrame::OnUpdateExportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}


/////////////////////////////////////////////////////////////////////////////
// CMDIP30NewNormTemplateFrame

IMPLEMENT_DYNCREATE(CMDIP30NewNormTemplateFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIP30NewNormTemplateFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIP30NewNormTemplateFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()

	ON_COMMAND(ID_TBTN_ADD_SPC, OnAddSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD_SPC, OnUpdateSpcTBtnClick)

	ON_COMMAND(ID_TBTN_DEL_SPC, OnRemoveSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DEL_SPC, OnUpdateRemoveSpcTBtnClick)

	ON_COMMAND(ID_TBTN_IMPORT, OnImportTBtnClick)
	//ON_UPDATE_COMMAND_UI(ID_TBTN_IMPORT, OnUpdateImportTBtnClick)

	ON_COMMAND(ID_TBTN_EXPORT, OnExportTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtnClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIP30NewNormTemplateFrame::CMDIP30NewNormTemplateFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableTBtns = FALSE;
	m_bAlreadySaidOkToClose = FALSE;
}

CMDIP30NewNormTemplateFrame::~CMDIP30NewNormTemplateFrame()
{
}

void CMDIP30NewNormTemplateFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_NEW_NORM_TEMPLATE_ENTRY_KEY);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnDestroy();

}

void CMDIP30NewNormTemplateFrame::OnClose(void)
{

	BOOL bDoClose = FALSE;

	bDoClose = m_bAlreadySaidOkToClose || okToClose();

	if (bDoClose)
	{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

		// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		CP30NewNormTemplateFormView *pView = (CP30NewNormTemplateFormView*)getFormViewByID(IDD_FORMVIEW11);
		if (pView != NULL)
		{
			pView->saveP30NewNormTemplate(-1);
			// Need to release ref. pointers; 080520 p�d
			pView = NULL;
		}	// if (pView)
		CMDIChildWnd::OnClose();
	}

}

int CMDIP30NewNormTemplateFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;

	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	CString sTBResFN = getToolBarResourceFN();
	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.EnableDocking(xtpFlagStretched);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{
				m_sMainWindowCaption = xml->str(IDD_FORMVIEW11);
				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR2)
						{		

							setToolbarBtnIcon(sTBResFN,p->GetAt(0),-1,_T(""),FALSE);	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(1),-1,_T(""),FALSE);	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_IMPORT,xml->str(IDS_STRING22699));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_EXPORT,xml->str(IDS_STRING22700));	//
						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())
			}	// if (fileExists(sTBResFN))
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CMDIP30NewNormTemplateFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

BOOL CMDIP30NewNormTemplateFrame::okToClose(void)
{
	BOOL bIsOKToClose = FALSE;

	CObjectFormView *pObjView = NULL;
	CP30NewNormTemplateFormView *pFrame = (CP30NewNormTemplateFormView *)getFormViewByID(IDD_FORMVIEW11);
	if (pFrame != NULL)
	{
		bIsOKToClose = pFrame->saveP30NewNormTemplate(2);
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}
	return bIsOKToClose;
}

BOOL CMDIP30NewNormTemplateFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIP30NewNormTemplateFrame diagnostics

#ifdef _DEBUG
void CMDIP30NewNormTemplateFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIP30NewNormTemplateFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDIP30NewNormTemplateFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);

	// For some reason, we need to set MainWindow caption here. Because when one of the
	// P30 add windows has focus, the MainWindow caption dissapears on movin' to another
	// module, e.g. Esitmate etc; 091117 p�d
	SetWindowText(m_sMainWindowCaption);
}

// load the placement in OnShowWindow()
void CMDIP30NewNormTemplateFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_NEW_NORM_TEMPLATE_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIP30NewNormTemplateFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CP30NewNormTemplateFormView *pView = (CP30NewNormTemplateFormView*)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIP30NewNormTemplateFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIP30NewNormTemplateFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIP30NewNormTemplateFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW12,m_sLangFN,lParam);
	}
	else
	{
		CView *pView = NULL;
		if ((pView = getFormViewByID(IDD_FORMVIEW11)) != NULL)
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
	}

	return 0L;
}

// MY METHODS
void CMDIP30NewNormTemplateFrame::setLanguage()
{
}

void CMDIP30NewNormTemplateFrame::OnAddSpecieTBtnClick(void)
{
}

void CMDIP30NewNormTemplateFrame::OnRemoveSpecieTBtnClick(void)
{
}

void CMDIP30NewNormTemplateFrame::OnImportTBtnClick(void)
{
	CP30NewNormTemplateFormView *pView = (CP30NewNormTemplateFormView*)getFormViewByID(IDD_FORMVIEW11);
	if (pView != NULL)
	{
		pView->importP30Table();
		pView = NULL;
	}
}

void CMDIP30NewNormTemplateFrame::OnExportTBtnClick(void)
{
	CP30NewNormTemplateFormView *pView = (CP30NewNormTemplateFormView*)getFormViewByID(IDD_FORMVIEW11);
	if (pView != NULL)
	{
		pView->exportP30Table();
		pView = NULL;
	}
}

void CMDIP30NewNormTemplateFrame::OnUpdateSpcTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30NewNormTemplateFrame::OnUpdateRemoveSpcTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30NewNormTemplateFrame::OnUpdateImportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30NewNormTemplateFrame::OnUpdateExportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

/////////////////////////////////////////////////////////////////////////////
// CMDIHigherCostsFrame

IMPLEMENT_DYNCREATE(CMDIHigherCostsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIHigherCostsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIHigherCostsFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_TBTN_ADD_SPC, OnAddRowTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD_SPC, OnUpdateAddTBtnClick)

	ON_COMMAND(ID_TBTN_DEL_SPC, OnRemoveRowTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DEL_SPC, OnUpdateRemoveRowTBtnClick)

	ON_COMMAND(ID_TBTN_IMPORT, OnImportTBtnClick)
	//ON_UPDATE_COMMAND_UI(ID_TBTN_IMPORT, OnUpdateImportTBtnClick)

	ON_COMMAND(ID_TBTN_EXPORT, OnExportTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtnClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIHigherCostsFrame::CMDIHigherCostsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableTBtns0 = FALSE;
	m_bEnableTBtns1 = FALSE;
}

CMDIHigherCostsFrame::~CMDIHigherCostsFrame()
{
}

void CMDIHigherCostsFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_HIGHER_COSTS_ENTRY_KEY);
	SavePlacement(this, csBuf);
}

void CMDIHigherCostsFrame::OnClose(void)
{
	BOOL bDoClose = FALSE;

	bDoClose = m_bAlreadySaidOkToClose || okToClose();

	if (bDoClose)
	{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

		// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		CHigherCostsFormView *pView = (CHigherCostsFormView*)getFormViewByID(IDD_FORMVIEW3);
		if (pView != NULL)
		{
			pView->saveHCostTemplate(-1);
			// Need to release ref. pointers; 080520 p�d
			pView = NULL;
		}

		CMDIChildWnd::OnClose();
	}
}

int CMDIHigherCostsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;

	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

//	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	CString sTBResFN = getToolBarResourceFN();
	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.EnableDocking(xtpFlagStretched);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{
				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR2)
						{		
							setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_ADD,xml->str(IDS_STRING22697));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_REMOVE,xml->str(IDS_STRING22698));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_IMPORT,xml->str(IDS_STRING22701));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_EXPORT,xml->str(IDS_STRING22702));	//
						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())
			}	// if (fileExists(sTBResFN))
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CMDIHigherCostsFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

BOOL CMDIHigherCostsFrame::okToClose(void)
{
	BOOL bIsOKToClose = FALSE;

	CHigherCostsFormView *pFrame = (CHigherCostsFormView *)getFormViewByID(IDD_FORMVIEW3);
	if (pFrame != NULL)
	{
		bIsOKToClose = pFrame->saveHCostTemplate(2);
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}

	return bIsOKToClose;
}

BOOL CMDIHigherCostsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIHigherCostsFrame diagnostics

#ifdef _DEBUG
void CMDIHigherCostsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIHigherCostsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIHigherCostsFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDIHigherCostsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIHigherCostsFrame::OnUpdateAddTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns0);
}

void CMDIHigherCostsFrame::OnUpdateRemoveRowTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns0);
}

void CMDIHigherCostsFrame::OnUpdateImportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns1);
}

void CMDIHigherCostsFrame::OnUpdateExportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns1);
}

// load the placement in OnShowWindow()
void CMDIHigherCostsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_HIGHER_COSTS_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIHigherCostsFrame::OnSetFocus(CWnd *wnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CHigherCostsFormView *pView = (CHigherCostsFormView*)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);
}

void CMDIHigherCostsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_HIGHER_COSTS_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_HIGHER_COSTS_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIHigherCostsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIHigherCostsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW5,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}

	return 0L;
}

// MY METHODS
void CMDIHigherCostsFrame::setLanguage()
{
}

void CMDIHigherCostsFrame::OnAddRowTBtnClick(void)
{
	CHigherCostsFormView *pView = (CHigherCostsFormView*)getFormViewByID(IDD_FORMVIEW3);
	if (pView != NULL)
	{
		pView->addRowToReport();
		pView = NULL;
	}
}

void CMDIHigherCostsFrame::OnRemoveRowTBtnClick(void)
{
	CHigherCostsFormView *pView = (CHigherCostsFormView*)getFormViewByID(IDD_FORMVIEW3);
	if (pView != NULL)
	{
//		pView->removeLastRowFromReport();
		pView->removeFocusedRowFromReport();	// #3357
		pView = NULL;
	}
}

void CMDIHigherCostsFrame::OnImportTBtnClick(void)
{
	CHigherCostsFormView *pView = (CHigherCostsFormView*)getFormViewByID(IDD_FORMVIEW3);
	if (pView != NULL)
	{
		pView->importHighCostTable();
		pView = NULL;
	}
}

void CMDIHigherCostsFrame::OnExportTBtnClick(void)
{
	CHigherCostsFormView *pView = (CHigherCostsFormView*)getFormViewByID(IDD_FORMVIEW3);
	if (pView != NULL)
	{
		pView->exportHighCostTable();
		pView = NULL;
	}
}



/////////////////////////////////////////////////////////////////////////////
// CMDITemplateListFrame

IMPLEMENT_DYNCREATE(CMDITemplateListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITemplateListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITemplateListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDITemplateListFrame::CMDITemplateListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDITemplateListFrame::~CMDITemplateListFrame()
{
}

void CMDITemplateListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_TEMPLATE_LIST_KEY);
	SavePlacement(this, csBuf);
}

int CMDITemplateListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDITemplateListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDITemplateListFrame diagnostics

#ifdef _DEBUG
void CMDITemplateListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITemplateListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDITemplateListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDITemplateListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_TEMPLATE_LIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDITemplateListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDITemplateListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_LIST_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_LIST_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITemplateListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITemplateListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDITemplateListFrame::setLanguage()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDITemplate2ListFrame

IMPLEMENT_DYNCREATE(CMDITemplate2ListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITemplate2ListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITemplate2ListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDITemplate2ListFrame::CMDITemplate2ListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDITemplate2ListFrame::~CMDITemplate2ListFrame()
{
}

void CMDITemplate2ListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_TEMPLATE_LIST_KEY);
	SavePlacement(this, csBuf);
}

int CMDITemplate2ListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDITemplate2ListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDITemplate2ListFrame diagnostics

#ifdef _DEBUG
void CMDITemplate2ListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITemplate2ListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDITemplate2ListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDITemplate2ListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_TEMPLATE_LIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDITemplate2ListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDITemplate2ListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_LIST_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_LIST_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITemplate2ListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITemplate2ListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDITemplate2ListFrame::setLanguage()
{
}


/////////////////////////////////////////////////////////////////////////////
// CMDITemplate3ListFrame

IMPLEMENT_DYNCREATE(CMDITemplate3ListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITemplate3ListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITemplate3ListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDITemplate3ListFrame::CMDITemplate3ListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDITemplate3ListFrame::~CMDITemplate3ListFrame()
{
}

void CMDITemplate3ListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_NN_TEMPLATE_LIST_KEY);
	SavePlacement(this, csBuf);
}

int CMDITemplate3ListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDITemplate3ListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDITemplate3ListFrame diagnostics

#ifdef _DEBUG
void CMDITemplate3ListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITemplate3ListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDITemplate3ListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDITemplate3ListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_NN_TEMPLATE_LIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDITemplate3ListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDITemplate3ListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_LIST_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_LIST_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITemplate3ListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITemplate3ListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDITemplate3ListFrame::setLanguage()
{
}


//---------------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CMDITemplate3ListFrame

IMPLEMENT_DYNCREATE(CMDITemplate2018ListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITemplate2018ListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITemplate2018ListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDITemplate2018ListFrame::CMDITemplate2018ListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDITemplate2018ListFrame::~CMDITemplate2018ListFrame()
{
}

void CMDITemplate2018ListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_2018_TEMPLATE_LIST_KEY);
	SavePlacement(this, csBuf);
}

int CMDITemplate2018ListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDITemplate2018ListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDITemplate2018ListFrame diagnostics

#ifdef _DEBUG
void CMDITemplate2018ListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITemplate2018ListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDITemplate2018ListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDITemplate2018ListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_2018_TEMPLATE_LIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDITemplate2018ListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDITemplate2018ListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_LIST_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_LIST_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITemplate2018ListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITemplate2018ListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDITemplate2018ListFrame::setLanguage()
{
}

//---------------------------------------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// CDocumentTmplFrame

IMPLEMENT_DYNCREATE(CDocumentTmplFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CDocumentTmplFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CDocumentTmplFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CDocumentTmplFrame::CDocumentTmplFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CDocumentTmplFrame::~CDocumentTmplFrame()
{
}

void CDocumentTmplFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_DOC_TMPL_ENTRY_KEY);
	SavePlacement(this, csBuf);
}

void CDocumentTmplFrame::OnClose(void)
{
	CDocumentTmplFormView *pView = (CDocumentTmplFormView*)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		if (pView->doSaveDocument(2))
		{
			// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

			// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
			// Need to release ref. pointers; 080520 p�d
			CMDIChildWnd::OnClose();
		}
		pView = NULL;
	}
}

int CDocumentTmplFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 090817 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR4);
	m_wndToolBar.EnableDocking(xtpFlagStretched);

//	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	///////////////////////////////////////////////////////////////////////////
	// Setup icons in toolbar and assign a tooltip
	if (fileExists(m_sLangFN))
	{

		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					CXTPControl *pCtrl = NULL;
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR4)
					{		
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip((xml.str(IDS_STRING6200)));
	
						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip((xml.str(IDS_STRING6201)));
						//pCtrl->SetVisible(FALSE);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip((xml.str(IDS_STRING6202)));

						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip((xml.str(IDS_STRING6203)));

						pCtrl = p->GetAt(4);
						pCtrl->SetTooltip((xml.str(IDS_STRING6211)));

						pCtrl = p->GetAt(5);
						pCtrl->SetTooltip((xml.str(IDS_STRING6212)));

						pCtrl = p->GetAt(6);
						pCtrl->SetTooltip((xml.str(IDS_STRING6213)));

						pCtrl = p->GetAt(7);
						pCtrl->SetTooltip((xml.str(IDS_STRING6214)));

						pCtrl = p->GetAt(8);
						pCtrl->SetTooltip((xml.str(IDS_STRING6215)));

						pCtrl = p->GetAt(9);
						pCtrl->SetTooltip((xml.str(IDS_STRING6204)));

						pCtrl = p->GetAt(10);
						pCtrl->SetTooltip((xml.str(IDS_STRING6205)));

						pCtrl = p->GetAt(11);
						pCtrl->SetTooltip((xml.str(IDS_STRING6206)));

						pCtrl = p->GetAt(12);
						pCtrl->SetTooltip((xml.str(IDS_STRING6207)));

						pCtrl = p->GetAt(13);
						pCtrl->SetTooltip((xml.str(IDS_STRING6208)));

						pCtrl = p->GetAt(14);
						pCtrl->SetTooltip((xml.str(IDS_STRING6209)));

						pCtrl = p->GetAt(15);
						pCtrl->SetTooltip((xml.str(IDS_STRING6210)));
	
					}	// if (nBarID == IDR_TOOLBAR2)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(sLangFN))
	}	// if (fileExists(m_sLangFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CDocumentTmplFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CDocumentTmplFrame diagnostics

#ifdef _DEBUG
void CDocumentTmplFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CDocumentTmplFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CDocumentTmplFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CDocumentTmplFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CDocumentTmplFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_DOC_TMPL_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CDocumentTmplFrame::OnSetFocus(CWnd *wnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CDocumentTmplFormView *pView = (CDocumentTmplFormView*)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);
}

void CDocumentTmplFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_DOC_TMPL_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_DOC_TMPL_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CDocumentTmplFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CDocumentTmplFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW9,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}

	return 0L;
}

// MY METHODS
void CDocumentTmplFrame::setLanguage()
{
}


/////////////////////////////////////////////////////////////////////////////
// CDocumentTmplListFrame


IMPLEMENT_DYNCREATE(CDocumentTmplListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CDocumentTmplListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CDocumentTmplListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CDocumentTmplListFrame::CDocumentTmplListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CDocumentTmplListFrame::~CDocumentTmplListFrame()
{
}

void CDocumentTmplListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_DOC_TMPL_ENTRY_LIST_KEY);
	SavePlacement(this, csBuf);
}

int CDocumentTmplListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CDocumentTmplListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CDocumentTmplListFrame diagnostics

#ifdef _DEBUG
void CDocumentTmplListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CDocumentTmplListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CDocumentTmplListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CDocumentTmplListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CDocumentTmplListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_DOC_TMPL_ENTRY_LIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CDocumentTmplListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CDocumentTmplListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_DOC_TMPL_LIST_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_DOC_TMPL_LIST_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CDocumentTmplListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CDocumentTmplListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CDocumentTmplListFrame::setLanguage()
{
}


/////////////////////////////////////////////////////////////////////////////
// CPropertyStatusFrame


IMPLEMENT_DYNCREATE(CPropertyStatusFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPropertyStatusFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPropertyStatusFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPropertyStatusFrame::CPropertyStatusFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CPropertyStatusFrame::~CPropertyStatusFrame()
{
}

void CPropertyStatusFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_PROP_STATUS_KEY);
	SavePlacement(this, csBuf);
}

int CPropertyStatusFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CPropertyStatusFrame::OnClose(void)
{
	CPropertyStatusFormView *pView = (CPropertyStatusFormView*)getFormViewByID(IDD_FORMVIEW13);
	if (pView != NULL)
	{
		pView->doSavePropActionStatus();
	}
	CMDIChildWnd::OnClose();
}


BOOL CPropertyStatusFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertyStatusFrame diagnostics

#ifdef _DEBUG
void CPropertyStatusFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPropertyStatusFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CPropertyStatusFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CPropertyStatusFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CPropertyStatusFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_PROP_STATUS_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPropertyStatusFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, enable/disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CPropertyStatusFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROP_STATUS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROP_STATUS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPropertyStatusFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPropertyStatusFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CPropertyStatusFrame::setLanguage()
{
}



//--------------------------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CMDIP30Norm2018TemplateFrame

IMPLEMENT_DYNCREATE(CMDIP30Norm2018TemplateFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIP30Norm2018TemplateFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIP30NewNormTemplateFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()

	ON_COMMAND(ID_TBTN_ADD_SPC, OnAddSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD_SPC, OnUpdateSpcTBtnClick)

	ON_COMMAND(ID_TBTN_DEL_SPC, OnRemoveSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DEL_SPC, OnUpdateRemoveSpcTBtnClick)

	ON_COMMAND(ID_TBTN_IMPORT, OnImportTBtnClick)
	//ON_UPDATE_COMMAND_UI(ID_TBTN_IMPORT, OnUpdateImportTBtnClick)

	ON_COMMAND(ID_TBTN_EXPORT, OnExportTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtnClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIP30Norm2018TemplateFrame::CMDIP30Norm2018TemplateFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableTBtns = FALSE;
	m_bAlreadySaidOkToClose = FALSE;
}

CMDIP30Norm2018TemplateFrame::~CMDIP30Norm2018TemplateFrame()
{
}

void CMDIP30Norm2018TemplateFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_NORM_2018_TEMPLATE_ENTRY_KEY);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnDestroy();

}

void CMDIP30Norm2018TemplateFrame::OnClose(void)
{

	BOOL bDoClose = FALSE;

	bDoClose = m_bAlreadySaidOkToClose || okToClose();

	if (bDoClose)
	{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

		// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		CP30Norm2018TemplateFormView *pView = (CP30Norm2018TemplateFormView*)getFormViewByID(IDD_FORMVIEW14);
		if (pView != NULL)
		{
			pView->saveP30Norm2018Template(-1);
			// Need to release ref. pointers; 080520 p�d
			pView = NULL;
		}	// if (pView)
		CMDIChildWnd::OnClose();
	}

}

int CMDIP30Norm2018TemplateFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;

	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	CString sTBResFN = getToolBarResourceFN();
	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.EnableDocking(xtpFlagStretched);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{
				m_sMainWindowCaption = xml->str(IDD_FORMVIEW14);
				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR2)
						{		

							setToolbarBtnIcon(sTBResFN,p->GetAt(0),-1,_T(""),FALSE);	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(1),-1,_T(""),FALSE);	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_IMPORT,xml->str(IDS_STRING22699));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_EXPORT,xml->str(IDS_STRING22700));	//
						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())
			}	// if (fileExists(sTBResFN))
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CMDIP30Norm2018TemplateFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

BOOL CMDIP30Norm2018TemplateFrame::okToClose(void)
{
	BOOL bIsOKToClose = FALSE;

	CObjectFormView *pObjView = NULL;
	CP30Norm2018TemplateFormView *pFrame = (CP30Norm2018TemplateFormView *)getFormViewByID(IDD_FORMVIEW14);
	if (pFrame != NULL)
	{
		bIsOKToClose = pFrame->saveP30Norm2018Template(2);
		// Need to release ref. pointers; 080520 p�d
		pFrame = NULL;
	}
	return bIsOKToClose;
}

BOOL CMDIP30Norm2018TemplateFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIP30NewNormTemplateFrame diagnostics

#ifdef _DEBUG
void CMDIP30Norm2018TemplateFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIP30Norm2018TemplateFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

// PRIVATE
void CMDIP30Norm2018TemplateFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);

	// For some reason, we need to set MainWindow caption here. Because when one of the
	// P30 add windows has focus, the MainWindow caption dissapears on movin' to another
	// module, e.g. Esitmate etc; 091117 p�d
	SetWindowText(m_sMainWindowCaption);
}

// load the placement in OnShowWindow()
void CMDIP30Norm2018TemplateFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_P30_NORM_2018_TEMPLATE_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIP30Norm2018TemplateFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CP30Norm2018TemplateFormView *pView = (CP30Norm2018TemplateFormView*)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIP30Norm2018TemplateFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_P30_TEMPLATE_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_P30_TEMPLATE_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIP30Norm2018TemplateFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIP30Norm2018TemplateFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW15,m_sLangFN,lParam);
	}
	else
	{
		CView *pView = NULL;
		if ((pView = getFormViewByID(IDD_FORMVIEW14)) != NULL)
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
	}

	return 0L;
}

// MY METHODS
void CMDIP30Norm2018TemplateFrame::setLanguage()
{
}

void CMDIP30Norm2018TemplateFrame::OnAddSpecieTBtnClick(void)
{
}

void CMDIP30Norm2018TemplateFrame::OnRemoveSpecieTBtnClick(void)
{
}

void CMDIP30Norm2018TemplateFrame::OnImportTBtnClick(void)
{
	CP30Norm2018TemplateFormView *pView = (CP30Norm2018TemplateFormView*)getFormViewByID(IDD_FORMVIEW14);
	if (pView != NULL)
	{
		pView->importP30Table();
		pView = NULL;
	}
}

void CMDIP30Norm2018TemplateFrame::OnExportTBtnClick(void)
{
	CP30Norm2018TemplateFormView *pView = (CP30Norm2018TemplateFormView*)getFormViewByID(IDD_FORMVIEW14);
	if (pView != NULL)
	{
		pView->exportP30Table();
		pView = NULL;
	}
}

void CMDIP30Norm2018TemplateFrame::OnUpdateSpcTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30Norm2018TemplateFrame::OnUpdateRemoveSpcTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30Norm2018TemplateFrame::OnUpdateImportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

void CMDIP30Norm2018TemplateFrame::OnUpdateExportTBtnClick(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBtns);
}

//--------------------------------------------------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// CMDISpeciesFrame

IMPLEMENT_DYNCREATE(CMDISpeciesFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CMDISpeciesFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CMDISpeciesFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDISpeciesFrame::CMDISpeciesFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDISpeciesFrame::~CMDISpeciesFrame()
{
}

void CMDISpeciesFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SPECIEFRAME_KEY);
	SavePlacement(this, csBuf);

	CChildFrameBase::OnDestroy();
}

BOOL CMDISpeciesFrame::DestroyWindow()
{
	return CChildFrameBase::DestroyWindow();
}


int CMDISpeciesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CChildFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING205),
						xml.str(IDS_STRING206));
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDISpeciesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CChildFrameBase::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDISpeciesFrame::OnClose(void)
{

	CSpeciesFormView *pView = (CSpeciesFormView *)getFormViewByID(IDD_FORMVIEW_P30SPEC_SET);
	if (pView)
	{
//		if (pView->getIsDirty())
//		{
// Commented out (PL); 070402 p�d
//			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//			{
					pView->saveSpecies();
//			}	// if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CChildFrameBase::OnClose();
}

// CMDISpeciesFrame diagnostics

#ifdef _DEBUG
void CMDISpeciesFrame::AssertValid() const
{
	CChildFrameBase::AssertValid();
}

void CMDISpeciesFrame::Dump(CDumpContext& dc) const
{
	CChildFrameBase::Dump(dc);
}

#endif //_DEBUG


void CMDISpeciesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SPECIES;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SPECIES;

	CChildFrameBase::OnGetMinMaxInfo(lpMMI);
}

void CMDISpeciesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CChildFrameBase::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
 
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
	{
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
	}
}

void CMDISpeciesFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

// load the placement in OnShowWindow()
void CMDISpeciesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CChildFrameBase::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SPECIEFRAME_KEY);
		LoadPlacement(this, csBuf);
  }


}

void CMDISpeciesFrame::OnSize(UINT nType,int cx,int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDISpeciesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}




/*
/////////////////////////////////////////////////////////////////////////////
// CPropertyRemoveSelListFrame

IMPLEMENT_DYNCREATE(CPropertyRemoveSelListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPropertyRemoveSelListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPropertyRemoveSelListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPropertyRemoveSelListFrame::CPropertyRemoveSelListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	//LOGFONT lfIcon;
	//VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	//VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CPropertyRemoveSelListFrame::~CPropertyRemoveSelListFrame()
{
}

void CPropertyRemoveSelListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_REMOVE_SELLIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CPropertyRemoveSelListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	//m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	//m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	//EnableDocking(CBRS_ALIGN_ANY);

	// docking for field chooser
	//m_wndFieldChooser.EnableDocking(0);

	//ShowControlBar(&m_wndFieldChooser, FALSE, FALSE);
	//FloatControlBar(&m_wndFieldChooser, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CPropertyRemoveSelListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_REMOVE_SELLIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPropertyRemoveSelListFrame::OnSetFocus(CWnd* wnd)
{
		// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(wnd);
}

BOOL CPropertyRemoveSelListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertyRemoveSelListFrame diagnostics

#ifdef _DEBUG
void CPropertyRemoveSelListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPropertyRemoveSelListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CPropertyRemoveSelListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROPERTY_REMOVE_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROPERTY_REMOVE_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPropertyRemoveSelListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPropertyRemoveSelListFrame::OnPaint()
{
	
	CMDIChildWnd::OnPaint();
}

void CPropertyRemoveSelListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPropertyRemoveSelListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}*/



