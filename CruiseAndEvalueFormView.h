#pragma once

#include "Resource.h"

// Class derived from CXTPReportView

// CEvaluatedData;	"V�rdering"

class CEvaluatedData : public CXTPReportView
{
	DECLARE_DYNCREATE(CEvaluatedData)

	BOOL m_bInitialized;

	CString m_sLangFN;

	CString m_sFieldChooser;
	CString m_sGroupBy;

	mapString m_mapArealCalculatorDlgText;

	CUMLandValueDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	CStringArray m_arrTypeOfVStand;	// "V�rdering", "V�rdering startar" etc.
	CStringArray m_arrTypeOfLand;		// "Skogsmark","Kalmark" etc,


	CStringArray m_arrReducMark;		// "Tomt/inget","�verlappande Mark", "Tillf�lligt utnyttjande",

	short m_nPropertyStatus;

	CXTPReportSubListControl m_wndSubList;

	bool m_bLoadReportOK;

	int popPropId;
	int popObjId;
	short popPropStatus;
	short popNormType;

protected:
	void LoadReportState(void);
	void SaveReportState(void);
	void ChangeAreal(CXTPReportRow *pRow,double fAreal);
	void setupReport(void);

	// Add both "typ av v�rdering" and "type av mark; skogsmark,kalmark"
	void addConstraints(void);

	int findTypeOfVStandIndex(LPCTSTR vstand);
	int findTypeOfLandIndex(LPCTSTR land);
	int findTypeOfReduceradMark(LPCTSTR reducmarktext);

	vecTransaction_eval_evaluation vecELVObjectEvaluated;
	void getObjectEvaluatedFromDB(int prop_id,int obj_id);

public:
	CEvaluatedData();
	
	void populateReport(int prop_id,int obj_id,short prop_status,short norm_type);
	void populateReport() { populateReport(popPropId,popObjId,popPropStatus,popNormType); }

	void addNewRecordToReport(void);
	void addNewRecordToReportWithDialog(void);

	//void saveEvalData(void); Tagit bort, g�r �nd� ingenting 20200428 J�
	void saveEvalData(double volume);
	void removeSelectedEvaluation(short prop_status,short norm_type);
	void removeEvaluations(void);
	void setDataInReport(CString best_num,CString best_name,CString si,CString tgl,double areal,double volume,int age,double corr_fac,BOOL do_update,double volumeha,int sida,BOOL bTillfUtnyttj);

	void getSelectedEvaluated(CTransaction_eval_evaluation& rec,CString& si,int* age,double* pine_perc,double* spruce_perc,double *birch_perc);
	inline void getEvaluated(vecTransaction_eval_evaluation& vec) { vec = vecELVObjectEvaluated; }
	inline void resetReport(void)	{		GetReportCtrl().ResetContent();	}
	inline int getNumOfEvalues(void)	{		return GetReportCtrl().GetRows()->GetCount();	}


	void setManuallyEnteredCorrFactor(CTransaction_eval_evaluation& rec,double corr_factor);

	//{{AFX_VIRTUAL(CCruisingData)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCruisingData)
	afx_msg	void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
//	afx_msg void OnReportConstraintChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportHeaderColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

////////////////////////////////////////////////////////////////////////////////////////////
// CCruisingData;	"Taxerade"

struct _struct_reduce_rotpost
{
	int nTraktID;
	BOOL bIsYes;
	BOOL bDontUseSampleTrees;
};

typedef std::vector<_struct_reduce_rotpost> vecReduceRotpost;

class CCruisingData : public CXTPReportView
{
	DECLARE_DYNCREATE(CCruisingData)

//private:
	BOOL m_bInitialized;

	CString m_sLangFN;

	CStringArray m_sarrYesNo;

	CString m_sWidthErrMsg;
	CString m_sSideNumberMsg;
	CString m_sEvalueUpdateMsg;

	CString m_sFieldChooser;
	CString m_sGroupBy;

	CImageList m_ilIcons;

	CUMLandValueDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	short m_nPropertyStatus;

	CStringArray m_arrTypeOfStand;	// "Taxerad best�nd", "Manuellt inl�st"

	CXTPReportSubListControl m_wndSubList;
protected:
	void LoadReportState(void);
	void SaveReportState(void);

	void setupReport(void);

	CTransaction_elv_cruise m_recELVCruise;
	vecTransaction_elv_cruise vecELVObjectCruise;
	void getObjectCruiseFromDB(int prop_id,int obj_id);


	int findTypeOfStandIndex(LPCTSTR stand);

	// Add both "typ av cruise"
	void addConstraints(void);



public:
	CCruisingData();

	void populateReport(int prop_id,int obj_id,short prop_status,short norm_type);
	BOOL removeSelectedCruise(short prop_status,short norm_type);
	void getIsDoReduceRotpost(vecReduceRotpost&);
	void saveCruisingWSide(void);
	void saveCruisingWidth(void);
	void saveCruiseData(void);

	void openPricelist();	// #4554
	void openCostPricelist();	// #4554

	void openStand(void);
	void addNewStand(void);

	BOOL getFocusedCruise(CTransaction_elv_cruise &rec);
	inline void getCruises(vecTransaction_elv_cruise &vec)	{	vec = vecELVObjectCruise;	}
	inline void resetReport(void)	{		GetReportCtrl().ResetContent();	}
	inline int getNumOfCruises(void)	{		return GetReportCtrl().GetRows()->GetCount();	}
	
	void setManuallyEnteredCalcData(CTransaction_elv_cruise rec,double spruce_mix,double st_value,double st_volume,
		double fStormTorkInfo_SpruceMix,
		double fStormTorkInfo_SumM3Sk_inside,
		double fStormTorkInfo_AvgPriceFactor,
		double fStormTorkInfo_TakeCareOfPerc,
		double fStormTorkInfo_PineP30Price,
		double fStormTorkInfo_SpruceP30Price,
		double fStormTorkInfo_BirchP30Price,
		double fStormTorkInfo_CompensationLevel,
		double fStormTorkInfo_Andel_Pine,
		double fStormTorkInfo_Andel_Spruce,
		double fStormTorkInfo_Andel_Birch,
		double fStormTorkInfo_WideningFactor);

	//{{AFX_VIRTUAL(CCruisingData)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCruisingData)
	afx_msg	void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportHeaderColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};





