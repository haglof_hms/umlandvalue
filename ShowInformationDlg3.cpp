// ShowInformationDlg3.cpp : implementation file
//

#include "stdafx.h"
#include "ShowInformationDlg3.h"


// CShowInformationDlg3 dialog

IMPLEMENT_DYNAMIC(CShowInformationDlg3, CDialog)



BEGIN_MESSAGE_MAP(CShowInformationDlg3, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CShowInformationDlg3::CShowInformationDlg3(CWnd* pParent /*=NULL*/)
	: CDialog(CShowInformationDlg3::IDD, pParent)
{
	m_pDB = NULL;

	m_bInitialized = FALSE;
	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);
}

CShowInformationDlg3::~CShowInformationDlg3()
{
}

void CShowInformationDlg3::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();

	m_xml.clean();

	CDialog::OnDestroy();
}

void CShowInformationDlg3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// CShowInformationDlg3 message handlers

BOOL CShowInformationDlg3::OnInitDialog()
{
	CDialog::OnInitDialog();
	CWnd *pBtn = NULL;

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			m_xml.Load(m_sLangFN);

			SetWindowText((m_xml.str(IDS_STRING6300)));
			if ((pBtn = GetDlgItem(IDCANCEL)) != NULL)
				pBtn->SetWindowText((m_xml.str(IDS_STRING4245)));
		}
		m_bInitialized = TRUE;
	}		// if (!m_bInitialized)



	return TRUE;
}


// CShowInformationDlg message handlers

void CShowInformationDlg3::OnPaint()
{
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

//	clip.DeflateRect(1,1);
	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-30,10,10);

	showInformation(pDC);

	CDialog::OnPaint();

}


// PRIVATE

void CShowInformationDlg3::showInformation(CDC *dc)
{
	CString S;
	int nLen = 0;
	TEXTMETRIC tm;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_contacts recContact;

	dc->SelectObject(&m_fnt1);
	dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

	if (pObj && m_pDB)
	{
		m_pDB->getContact(pObj->getObjContractorID(),recContact);
	}

	// Pers.nr/Org.nr
	dc->TextOut(10, tm.tmHeight+10,m_xml.str(IDS_STRING2400)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2400).GetLength());
	// Kontaktperson
	dc->TextOut(10, 2*tm.tmHeight+10,m_xml.str(IDS_STRING2401)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2401).GetLength());
	// F�retag
	dc->TextOut(10, 3*tm.tmHeight+10,m_xml.str(IDS_STRING2402)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2402).GetLength());
	// Adress
	dc->TextOut(10, 4*tm.tmHeight+10,m_xml.str(IDS_STRING2403)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2403).GetLength());
	// Postnummer
	dc->TextOut(10, 7*tm.tmHeight+10,m_xml.str(IDS_STRING2404)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2404).GetLength());
	// Postadress
	dc->TextOut(10, 8*tm.tmHeight+10,m_xml.str(IDS_STRING2405)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2405).GetLength());
	// Tlf arb
	dc->TextOut(10, 9*tm.tmHeight+10,m_xml.str(IDS_STRING2406)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2406).GetLength());
	// Tlf hem
	dc->TextOut(10, 10*tm.tmHeight+10,m_xml.str(IDS_STRING2407)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2407).GetLength());
	// Tlf mobil
	dc->TextOut(10, 11*tm.tmHeight+10,m_xml.str(IDS_STRING2408)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2408).GetLength());
	// Faxnummer
	dc->TextOut(10, 12*tm.tmHeight+10,m_xml.str(IDS_STRING2409)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2409).GetLength());
	// Region
	dc->TextOut(10, 13*tm.tmHeight+10,m_xml.str(IDS_STRING2419)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2419).GetLength());
	// Land
	dc->TextOut(10, 14*tm.tmHeight+10,m_xml.str(IDS_STRING2420)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2420).GetLength());
	// Moms.nummer
	dc->TextOut(10, 15*tm.tmHeight+10,m_xml.str(IDS_STRING2421)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2421).GetLength());
	// E-mail
	dc->TextOut(10, 16*tm.tmHeight+10,m_xml.str(IDS_STRING2422)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2422).GetLength());
	// Hemsida
	dc->TextOut(10, 17*tm.tmHeight+10,m_xml.str(IDS_STRING2423)+L":");
	nLen = max(nLen,m_xml.str(IDS_STRING2423).GetLength());
	// Noteringar
	dc->TextOut(10, 18*tm.tmHeight+10,m_xml.str(IDS_STRING1040)+L"...");


	dc->SelectObject(&m_fnt2);
	dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

	// Pers.nr/Org.nr
	dc->TextOut(nLen*tm.tmAveCharWidth+10, tm.tmHeight+10,recContact.getPNR_ORGNR());
	// Kontaktperson
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 2*tm.tmHeight+10,recContact.getName());
	// F�retag
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 3*tm.tmHeight+10,recContact.getCompany());
	// Adress
	dc->DrawText(recContact.getAddress(),CRect(nLen*tm.tmAveCharWidth+10,4*tm.tmHeight+10,200+nLen*tm.tmAveCharWidth, 6*tm.tmHeight+10),DT_WORDBREAK);
	// Postnummer
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 7*tm.tmHeight+10,recContact.getPostNum());
	// Postadress
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 8*tm.tmHeight+10,cleanCRLF(recContact.getPostAddress(),L", "));
	// Tlf arb
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 9*tm.tmHeight+10,cleanCRLF(recContact.getPhoneWork(),L" "));
	// Tlf hem
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 10*tm.tmHeight+10,cleanCRLF(recContact.getPhoneHome(),L" "));
	// Tlf mobil
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 11*tm.tmHeight+10,recContact.getMobile());
	// Faxnummer
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 12*tm.tmHeight+10,recContact.getFaxNumber());
	// Region
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 13*tm.tmHeight+10,recContact.getCounty());
	// Land
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 14*tm.tmHeight+10,recContact.getCountry());
	// Moms.nummer
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 15*tm.tmHeight+10,recContact.getVATNumber());
	// E-mail
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 16*tm.tmHeight+10,recContact.getEMail());
	// Hemsida
	dc->TextOut(nLen*tm.tmAveCharWidth+10, 17*tm.tmHeight+10,recContact.getWebSite());
	// Noteringar
	dc->DrawText(recContact.getNotes(),CRect(10,19*tm.tmHeight+10,200+nLen*tm.tmAveCharWidth, 23*tm.tmHeight+10),DT_WORDBREAK);

}
