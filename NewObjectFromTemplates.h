#pragma once

#include "Resource.h"

// CNewObjectFromTemplates dialog

class CNewObjectFromTemplates : public CDialog
{
	DECLARE_DYNAMIC(CNewObjectFromTemplates)

	BOOL m_bIsSelectionOK;

	// Setup language filename; 080407 p�d
	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
//	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;

	CMyExtEdit m_wndObjName;
	CMyExtEdit m_wndObjNumber;
	CMyExtEdit m_wndObjGArea;

//	CComboBox m_wndCBoxTraktTemplates;
	CComboBox m_wndCBoxForestNorm;
	CMyComboBox m_wndCBoxP30Templates;
	CComboBox m_wndCBoxHCostTemplates;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	//CTransaction_template m_recSelectedTemplate;
	vecTransactionTemplate m_vecTraktTemplate;
	CTransaction_template m_recSelectedP30Template;
	vecTransactionTemplate m_vecObjectP30Template;
	vecTransactionTemplate m_vecObjectP30TemplateNN;
	vecTransactionTemplate m_vecObjectP30Template2018;
	CTransaction_template m_recSelectedHCostTemplate;
	vecTransactionTemplate m_vecObjectHCostTemplate;
	int m_nForestNormIndex;
	int m_nGrowthArea;

	vecTransaction_elv_object m_vecObjects;

	CString m_sMsgCap;
	CString m_sMsgNoSelection;
	CString m_sMsgNameAndID;

	CString m_sGrowthAreaLbl;

	vecUCFunctions m_vecForrestNormFunc;

	void setupComboboxes(void);

	void setupP30CBox(int type_of);

	int m_nIndexCBox1;
	int m_nIndexCBox2;
	int m_nIndexCBox3;
	int m_nIndexCBox4;
	CString m_sObjName;
	CString m_sObjNumber;

	int m_nSelectedForstNorm;

	void setRangeForGrowthArea(int from,int to)
	{
		CString sMsg;
		sMsg.Format(_T("%s (%d-%d)"),m_sGrowthAreaLbl,from,to);
		m_wndObjGArea.SetRange(from,to);
		m_wndLbl6.SetWindowText(sMsg);
	}

public:
	CNewObjectFromTemplates(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNewObjectFromTemplates();

// Dialog Data
	enum { IDD = IDD_DIALOG5 };

	//======================================================================
	// Trakt-template
	void setTraktTemplates(vecTransactionTemplate& v)
	{
		m_vecTraktTemplate = v;
	}

//	CTransaction_template& getSelectedTemplate(void)	{	return m_recSelectedTemplate;	}

	//======================================================================
	// P30-template
	void setObjectP30Templates(vecTransactionTemplate& v)
	{
		m_vecObjectP30Template = v;
	}
	void setObjectP30TemplatesNN(vecTransactionTemplate& v)
	{
		m_vecObjectP30TemplateNN = v;
	}
	void setObjectP30Templates2018(vecTransactionTemplate& v)
	{
		m_vecObjectP30Template2018 = v;
	}

	CTransaction_template& getSelectedP30Template(void)
	{
		return m_recSelectedP30Template;
	}

	//======================================================================
	// "F�rdyrad avverkning" -template
	void setObjectHCostTemplates(vecTransactionTemplate& v)
	{
		m_vecObjectHCostTemplate = v;
	}

	void setObjects(vecTransaction_elv_object& v)
	{
		m_vecObjects = v;
	}

	CTransaction_template& getSelectedHCostTemplate(void)
	{
		return m_recSelectedHCostTemplate;
	}

	CString getObjectName(void)
	{
		return m_sObjName;
	}

	CString getObjectNumber(void)
	{
		return m_sObjNumber;
	}

	int getObjectForestNormIndex(void)
	{
		return m_nForestNormIndex;
	}
	
	int getObjectGrowthArea(void)
	{
		return m_nGrowthArea;
	}

protected:
	//{{AFX_VIRTUAL(CNewObjectFromTemplates)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnObjName();
	afx_msg void OnBnObjNumber();
	afx_msg void OnCbnSelchangeCombo2();
	afx_msg void OnCbnSelchangeCombo3();
	afx_msg void OnCbnSelchangeCombo4();

	afx_msg void OnBnClickedOk();
};
