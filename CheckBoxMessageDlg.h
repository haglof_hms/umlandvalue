#pragma once

#include "Resource.h"

// CCheckBoxMessageDlg dialog

class CCheckBoxMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CCheckBoxMessageDlg)

	CString m_sLangFN;

	CString m_sCaption;
	CString m_sText;
	CString m_sCBoxText;
	CString m_sOK;
	CString m_sCancel;

	CMyExtStatic m_wndLbl1;
	CButton m_wndCheckBox;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	CStatic	m_wndPicture;

	bool m_bCompleteRecalc;
	bool m_bCompleteRecalcForced;
	bool m_bNeedToRecalcAll;
public:
	CCheckBoxMessageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCheckBoxMessageDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG20 };

	void addCapText(LPCTSTR text);
	void addMsgText(LPCTSTR text);
	void addCBoxTextAndStatus(LPCTSTR text,BOOL cb_status);
	void addOkCancelText(LPCTSTR ok_text,LPCTSTR cancel_text);

	void forceCompleteReCalc(bool v)	{ m_bCompleteRecalcForced = v; }
	void needToRecalcAll(bool v)	{ m_bNeedToRecalcAll = v; }

	bool getDoCompleteReCalc(void)	{ return m_bCompleteRecalc; }
protected:
	//{{AFX_VIRTUAL(CSelectContractorDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
