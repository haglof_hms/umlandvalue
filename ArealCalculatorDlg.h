#pragma once

#include "Resource.h"

// CArealCalculatorDlg dialog

class CArealCalculatorDlg : public CDialog
{
	DECLARE_DYNAMIC(CArealCalculatorDlg)

	CString m_sCaption;
	CString m_sLbl1;
	CString m_sLbl2;
	CString m_sLbl3;
	CString m_sLbl4;
	CString m_sLbl5;
	CString m_sBtnOK;
	CString m_sBtnCancel;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl5;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit4;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	mapString m_mapDlgText;

	double m_fAreal;

public:
	CArealCalculatorDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CArealCalculatorDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG10 };

	void setDlgText(mapString& text)
	{
		m_mapDlgText = text;
	}

	double getAreal(void)	{ return m_fAreal; }
protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditCalc1();
	afx_msg void OnEnChangeEditCalc2();
};
