#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////////
// CAssortsStandsObjRec

class CAssortsStandsObjRec : public CXTPReportRecord
{
	//private:
	CTransaction_elv_ass_stands_obj proAssStandsObj;
protected:

public:
	CAssortsStandsObjRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz0dec));
	}

	CAssortsStandsObjRec(UINT index,CTransaction_elv_ass_stands_obj data)	 
	{
		m_nIndex = index;
		proAssStandsObj = data;
		AddItem(new CTextItem((data.getAssortName())));
		AddItem(new CFloatItem(data.getKrPerM3(),sz0dec));
		
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		return ((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
	CTransaction_elv_ass_stands_obj &getRecord(void)
	{
		return proAssStandsObj;
	}

};

// CAddKrPerM3Dlg dialog

class CAddKrPerM3Dlg : public CDialog
{
	DECLARE_DYNAMIC(CAddKrPerM3Dlg)

	// Setup language filename; 080429 p�d
	CString m_sLangFN;

	CMyReportCtrl m_wndReport;

	CButton m_wndReset;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	int m_nObjID;

	CUMLandValueDB *m_pDB;
	void setupReport(void);

	vecTransaction_elv_ass_stands_obj vecAssStandsObj;
	vecTransaction_elv_ass_stands_obj vecAssStandsObj_return;
	void getAssortmentData(void);
public:
	CAddKrPerM3Dlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddKrPerM3Dlg();

	void setDBConnection(CUMLandValueDB *db)	{	m_pDB = db;	}
	void setObjectID(int id)		{ m_nObjID = id; }

	void getReturnData(vecTransaction_elv_ass_stands_obj& vec)		
	{
		vec = vecAssStandsObj_return;
	}

	void populateData(void);

// Dialog Data
	enum { IDD = IDD_DIALOG15 };

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
};
