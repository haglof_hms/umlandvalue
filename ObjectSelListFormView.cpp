// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDI1950ForrestNormFormView.h"

#include "ObjectSelListFormView.h"

#include "XTPPreviewView.h"

/////////////////////////////////////////////////////////////////////////////
// CObjectReportFilterEditControl

IMPLEMENT_DYNCREATE(CObjectReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CObjectReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CObjectReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CObjectSelListFormView

IMPLEMENT_DYNCREATE(CObjectSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CObjectSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
//	ON_COMMAND(ID_TBBTN_COLUMNS, OnShowFieldChooser)
//	ON_COMMAND(ID_TBBTN1, OnShowFieldFilter)
//	ON_COMMAND(ID_TBBTN2, OnShowFieldFilterOff)
//	ON_COMMAND(ID_TBBTN3, OnPrintPreview)
//	ON_COMMAND(ID_TBBTN4, OnRefresh)
END_MESSAGE_MAP()

CObjectSelListFormView::CObjectSelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
//	m_nSelectedColumn = -1;
}

CObjectSelListFormView::~CObjectSelListFormView()
{
}

void CObjectSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
/*
	CTraktSelectListFrame* pWnd = (CTraktSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST1, &pWnd->m_wndFieldChooserDlg);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if (m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14,FW_BOLD);
	}
*/
	vecUCFunctions funcs;
	getForrestNormTypeOfInfring(funcs);
	if (funcs.size() > 0)
	{
		for (UINT i = 0;i < funcs.size();i++)
			m_mapTypeOfInfr[i] = funcs[i].getName();
	}

	getObject();
	setupReport();

	LoadReportState();

}

BOOL CObjectSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CObjectSelListFormView::OnDestroy()
{
	m_vecObjectData.clear();
	// Try to clear records on exit (for memory deallocation); 080215 p�d
	GetReportCtrl().ResetContent();

	if (m_pDB != NULL)
		delete m_pDB;

	SaveReportState();

	CXTPReportView::OnDestroy();	
}

BOOL CObjectSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CObjectSelListFormView diagnostics

#ifdef _DEBUG
void CObjectSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CObjectSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CObjectSelListFormView message handlers

// CObjectSelListFormView message handlers
void CObjectSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CObjectSelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CObjectSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);
	// Add these 3 lines to add scrollbars for View; 070319 p�d

	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
	GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
	GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
/*
			m_sGroupByThisField	= (xml->str(IDS_STRING230));
				m_sGroupByBox				= (xml->str(IDS_STRING231));
				m_sFieldChooser			= (xml->str(IDS_STRING232));

				m_sFilterOn					= (xml->str(IDS_STRING233));
*/

					// "Typ av intr�ng"; 080326 p�d
					m_arrTypeOfNet.Add((xml->str(IDS_STRING2120)));
					m_arrTypeOfNet.Add((xml->str(IDS_STRING2121)));

					//#3385 Status p� objektet
					m_arrObjectStatus.Add((xml->str(IDS_STRING1043)));
					m_arrObjectStatus.Add((xml->str(IDS_STRING1044)));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING204)), 80));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING205)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING206)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING207)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING214)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING212)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING216)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING217)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING203)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING211)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml->str(IDS_STRING210)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml->str(IDS_STRING1040)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					//#3385
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml->str(IDS_STRING1042)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment( DT_CENTER );

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
					GetReportCtrl().GetReportHeader()->AllowColumnReorder(FALSE);
					GetReportCtrl().GetReportHeader()->AllowColumnResize( TRUE );
					GetReportCtrl().GetReportHeader()->AllowColumnSort( TRUE );
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().AllowEdit(FALSE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

					populateReport();		

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CObjectSelListFormView::populateReport(void)
{
	int nObjectID = -1,nTypeOfInfr = -1;
	CString sTypeOfNet, sObjectStatus;
	CXTPReportRecord *pRec = NULL;
	GetReportCtrl().GetRecords()->RemoveAll();
	CTransaction_elv_object *pObject = getActiveObject();
	if (pObject != NULL)
	{
		nObjectID = pObject->getObjID_pk();
	}
	for (UINT i = 0;i < m_vecObjectData.size();i++)
	{
		CTransaction_elv_object data = m_vecObjectData[i];
		
		if (data.getObjTypeOfNet() >= 0 && data.getObjTypeOfNet() < m_arrTypeOfNet.GetCount())
			sTypeOfNet = m_arrTypeOfNet[data.getObjTypeOfNet()];
		nTypeOfInfr = _tstoi(data.getObjTypeOfInfring());

		//#3385
		if (data.getObjStatus() >= 0 && data.getObjStatus() < m_arrObjectStatus.GetCount())
			sObjectStatus = m_arrObjectStatus[data.getObjStatus()];

		if (data.getObjID_pk() == nObjectID)
			pRec = GetReportCtrl().AddRecord(new CObjectSelListReportRec(i,data,sTypeOfNet,m_mapTypeOfInfr[nTypeOfInfr],sObjectStatus));	//Changed #3385
		else
			GetReportCtrl().AddRecord(new CObjectSelListReportRec(i,data,sTypeOfNet,m_mapTypeOfInfr[nTypeOfInfr],sObjectStatus));	//Changed #3385
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	if (pRec != NULL)
	{
		CXTPReportRow *pRow = GetReportCtrl().GetRows()->Find(pRec);
		if (pRow != NULL)
		{
			GetReportCtrl().SetFocusedRow(pRow);
		}	// if (pRow != NULL)
	}	// if (pRec != NULL)
	// Need to release ref. pointers; 080520 p�d
	pObject = NULL;
}

void CObjectSelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CMDI1950ForrestNormFormView *pFrame = NULL;
	CObjectFormView *pObjView = NULL;
	CObjectSelListReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		pRec = (CObjectSelListReportRec*)pItemNotify->pItem->GetRecord();
		pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pFrame != NULL)
		{
			// Try to get the ObjectView; 080401 p�d
			pObjView = pFrame->getObjectFormView();
			if (pObjView != NULL)
			{
				pObjView->doPopulateData(pRec->getIndex());
			}
		}
	}	// if (pItemNotify->pRow)
	// Need to release ref. pointers; 080520 p�d
	pObjView = NULL;
	pFrame = NULL;
	pRec = NULL;
}

void CObjectSelListFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

void CObjectSelListFormView::OnShowFieldChooser()
{
/*
	CTraktSelectListFrame* pWnd = (CTraktSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}	// if (pWnd != NULL)
*/
}

void CObjectSelListFormView::OnShowFieldFilter()
{
/*
	CTraktSelectListFrame* pWnd = (CTraktSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		{
			CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
			CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
			int nColumn = pColumn->GetIndex();
			if (pCols && nColumn < pCols->GetCount())
			{
				for (int i = 0;i < pCols->GetCount();i++)
				{
					pCols->GetAt(i)->SetFiltrable( i == nColumn );
				}	// for (int i = 0;i < pCols->GetCount();i++)
			}	// if (pCols && nColumn < pCols->GetCount())
			m_wndLbl.SetWindowText(m_sFilterOn + " :");
			m_wndLbl1.SetWindowText(pColumn->GetCaption());
		}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != "");
	}
*/
}

void CObjectSelListFormView::OnShowFieldFilterOff()
{
/*
	GetReportCtrl().SetFilterText("");
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText("");
	CTraktSelectListFrame* pWnd = (CTraktSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}	// if (pWnd != NULL)
*/
}

void CObjectSelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
/*
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}
*/
}

void CObjectSelListFormView::OnPrintPreview()
{
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

}

//new #3385
void CObjectSelListFormView::Refresh()
{
	OnRefresh();
}

void CObjectSelListFormView::OnRefresh()
{
	getObject();
	populateReport();
}

// CObjectSelListFormView message handlers

void CObjectSelListFormView::getObject(void)
{
	//changed #3385
	CMDI1950ForrestNormFormView *pFrame = NULL;
	CObjectFormView *pObjView = NULL;
	int nShow = -1;
	pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pFrame != NULL)
		{
			// Try to get the ObjectView; 080401 p�d
			pObjView = pFrame->getObjectFormView();
			if (pObjView != NULL)
			{
				nShow = pObjView->getShowObjects();
			}
		}
	// Need to release ref. pointers; 080520 p�d
	pObjView = NULL;
	pFrame = NULL;

	if(nShow != -1)
	{
		if (m_bConnected)	
		{
			if (m_pDB != NULL)
			{
				m_pDB->getObjectWithStatus(m_vecObjectData,nShow);
			}	// if (m_pDB != NULL)
		}
	}
}

void CObjectSelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_OBJECT_SELLEIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
/*
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString(_T(REG_WP_TRAKT_SELLEIST_REPORT_KEY), _T("FilterText"), "");
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(_T(REG_WP_TRAKT_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
*/
}

void CObjectSelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_OBJECT_SELLEIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
/*
	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(_T(REG_WP_TRAKT_SELLEIST_REPORT_KEY), _T("FilterText"), sFilterText);

	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt(_T(REG_WP_TRAKT_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);
*/
}

