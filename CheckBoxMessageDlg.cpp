// CheckBoxMessageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CheckBoxMessageDlg.h"


// CCheckBoxMessageDlg dialog

IMPLEMENT_DYNAMIC(CCheckBoxMessageDlg, CDialog)

BEGIN_MESSAGE_MAP(CCheckBoxMessageDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCheckBoxMessageDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CCheckBoxMessageDlg::CCheckBoxMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCheckBoxMessageDlg::IDD, pParent)
{
	m_bCompleteRecalcForced = false;
	m_bNeedToRecalcAll = true;
}

CCheckBoxMessageDlg::~CCheckBoxMessageDlg()
{
}

void CCheckBoxMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCheckBoxMessageDlg)
	DDX_Control(pDX, IDC_LBL20_1, m_wndLbl1);

	DDX_Control(pDX, IDC_CHECK20_1, m_wndCheckBox);

	DDX_Control(pDX, IDC_PICTURE20_1, m_wndPicture);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP


}

BOOL CCheckBoxMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowTextW(m_sCaption);

	m_wndLbl1.SetWindowTextW(m_sText);
	m_wndLbl1.SetLblFont(14,FW_BOLD);

	m_wndCheckBox.SetWindowTextW(m_sCBoxText);
	if (m_bCompleteRecalcForced)
	{
		m_wndCheckBox.SetCheck(true);
		m_wndCheckBox.EnableWindow(false);
	}
	else
	{
		m_wndCheckBox.EnableWindow(true);
		m_wndCheckBox.SetCheck(m_bCompleteRecalc);
	}

	if (!m_bNeedToRecalcAll)
	{
		m_wndCheckBox.SetCheck(false);
		m_wndCheckBox.EnableWindow(false);
	}

	m_wndBtnOK.SetWindowTextW(m_sOK);
	m_wndBtnCancel.SetWindowTextW(m_sCancel);

	m_wndPicture.SetIcon(::LoadIcon(NULL,IDI_EXCLAMATION));

	return TRUE;
}

// CCheckBoxMessageDlg message handlers

// PUBLIC
void CCheckBoxMessageDlg::addCapText(LPCTSTR text)
{
	m_sCaption = text;
}

void CCheckBoxMessageDlg::addMsgText(LPCTSTR text)
{
	m_sText = text;
}

void CCheckBoxMessageDlg::addCBoxTextAndStatus(LPCTSTR text,BOOL cb_status)
{
	m_sCBoxText = text;

	m_bCompleteRecalc = cb_status;
}

void CCheckBoxMessageDlg::addOkCancelText(LPCTSTR ok_text,LPCTSTR cancel_text)
{
	m_sOK = ok_text;
	m_sCancel = cancel_text;
}

void CCheckBoxMessageDlg::OnBnClickedOk()
{
	m_bCompleteRecalc = (m_wndCheckBox.GetCheck() == 1);
	OnOK();
}
