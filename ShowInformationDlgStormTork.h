#pragma once
#include "Resource.h"
#include "ResLangFileReader.h"

// CShowInformationDlgStormTork dialog
// HMS-50 2020-02-12 J�
class CShowInformationDlgStormTork : public CDialog
{
	DECLARE_DYNAMIC(CShowInformationDlgStormTork)

	BOOL m_bInitialized;
	CString m_sLangFN;
	RLFReader m_xml;


	CTransaction_elv_cruise m_ElvCruise;

	CFont m_fnt1;
	CFont m_fnt2;

	CUMLandValueDB *m_pDB;
	int m_nNormId;

	void showStormTork(CDC *dc);
	void setCruise(CTransaction_elv_cruise &cruise)
	{	
	m_ElvCruise=CTransaction_elv_cruise(cruise);
	}
	void setNorm(int nId)
	{
	m_nNormId=nId;
	}

public:
	CShowInformationDlgStormTork(CWnd* pParent = NULL);   // standard constructor
	virtual ~CShowInformationDlgStormTork();	

// Dialog Data
	enum { IDD = IDD_DIALOG_STORMTORK };

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
