#pragma once

#include "Resource.h"

// CHigherCostsFormView form view

class CHigherCostsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CHigherCostsFormView)

	BOOL m_bInitialized;

	BOOL m_bDataEnabled;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgNoTemplateName;
	CString m_sMsgNoTemplateName2;
	CString m_sMsgRemoveTemplate;
	CString m_sMsgNoTemplatesRegistered;
	CString m_sMsgCharError;

	CXTResizeGroupBox m_wndGroupHCost_1;
	CXTResizeGroupBox m_wndGroupHCost_2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;

	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;

	CButton m_wndRBUseTable;
	CButton m_wndRBUseBreakAndFactor;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	vecTransactionTemplate m_vecTransactionTemplate;
	CTransaction_template m_recActiveTemplate;
	void getObjectTemplatesFromDB(void);

	int m_nDBIndex;
	void populateData(int);

	void setupPopulationOfTemplate(BOOL setup_dbindex);	// E.g. after save; 080402 p�d

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void newHCostTemplate();
	void removeHCostTemplate();

	void setEnableData(BOOL enable);

	enum enumAction { TEMPLATE_NONE,
										TEMPLATE_NEW,
										TEMPLATE_OPEN } m_enumAction;

protected:
	CHigherCostsFormView();           // protected constructor used by dynamic creation
	virtual ~CHigherCostsFormView();

	CMyReportCtrl m_wndReport;
	BOOL setupReport(void);
public:
	enum { IDD = IDD_FORMVIEW3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	// Public, so we can run it from MDIChildWnd (CMDIHigherCostsFrame::OnClose(void)); 080404 p�d
	BOOL saveHCostTemplate(short action);

	void doPopulateData(int idx)
	{
		m_nDBIndex = idx;
		populateData(m_nDBIndex);
	}

	int getDBIndex(void)
	{
		return m_nDBIndex;
	}

	int getNumOfTemplateItems(void)
	{
		return (int)m_vecTransactionTemplate.size();
	}

	void addRowToReport(void);
	void removeLastRowFromReport(void);
	void removeFocusedRowFromReport(void);
	void importHighCostTable(void);
	void exportHighCostTable(void);

	void doSetNavigationBar();

protected:
	//{{AFX_VIRTUAL(CHigherCostsFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDI1950ForrestNormFrame)
	afx_msg void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnReport(void);
	afx_msg void OnSetFocus(CWnd *);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRadioUseTable();
	afx_msg void OnBnClickedRadioUseBreakFactor();
	afx_msg void OnEnChangeEdit1();
};