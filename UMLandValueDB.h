#if !defined(AFX_UMLANDVALUEDB_H)
#define AFX_UMLANDVALUEDB_H

#include "StdAfx.h"

struct searchResult { int ownerId;int propertyId;int objectId;CString ownerName;CString propertyName;CString objectName; };

typedef struct _match_obj_prop
{
	int prop_id;
	int obj_id;
	CString sPropName;
	CString sPropBlock;
	CString sPropUnit;
	CString sObjName;

	_match_obj_prop(void)
	{
		prop_id = -1;
		obj_id = -1;
		sPropName = L"";
		sPropBlock = L"";
		sPropUnit = L"";
		sObjName = L"";
	}
	_match_obj_prop(int _prop_id,int _obj_id,LPCTSTR _prop_name,LPCTSTR _block,LPCTSTR _unit,LPCTSTR _obj_name)
	{
		prop_id = _prop_id;
		obj_id = _obj_id;
		sPropName = _prop_name;
		sPropBlock =_block;
		sPropUnit = _unit;
		sObjName = _obj_name;
	}
} MATCH_OBJ_PROP_STRUCT;

typedef std::vector<MATCH_OBJ_PROP_STRUCT> vecFuncObjProp;

typedef struct _match_cruise_prop
{
	int nStandId;
	int nObjId;
	CString sStandNumber;
	CString sStandName;

	_match_cruise_prop(void)
	{
		nStandId = -1;
		nObjId = -1;
		sStandNumber = L"";
		sStandName = L"";
	}
	_match_cruise_prop(int _stand_id,int _obj_id,LPCTSTR _stand_number,LPCTSTR _stand_name)
	{
		nStandId = _stand_id;
		nObjId = _obj_id;
		sStandNumber = _stand_number;
		sStandName = _stand_name;
	}
} MATCH_CRUISE_PROP_STRUCT;

typedef std::vector<MATCH_CRUISE_PROP_STRUCT> vecFuncCruiseProp;


class CUMLandValueDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
	BOOL traktDataExists(CTransaction_trakt_data &);
	BOOL traktDataExists(int trakt_id);
	BOOL traktSampleTreeExists(CTransaction_sample_tree &);
	BOOL traktDCLSTreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLSTreeExists(int trakt_id);
	BOOL traktDCLS1TreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLS1TreeExists(int trakt_id);
	BOOL traktDCLS2TreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLS2TreeExists(int trakt_id);
	BOOL traktExists(int trakt_id);
	BOOL traktAssTreeExists(CTransaction_tree_assort &);
	BOOL traktAssTreeExists(int trakt_id);
	BOOL traktAssExists(int trakt_id);
	BOOL traktAssExists(int trakt_id,int trakt_data_id);
	BOOL traktAssExists(CTransaction_trakt_ass &);
//	BOOL traktTransExists(int trakt_data_id,int trakt_id);
	BOOL traktTransExists(int trakt_data_id, int trakt_id, int nFrom_id, int nTo_id);

	BOOL traktRotpostExists(CTransaction_trakt_rotpost &);

	BOOL objectExists(CTransaction_elv_object&);
	BOOL objectExists(int);
	BOOL objectTemplExist(CTransaction_template &rec);
	BOOL objectDocumentTemplExist(CTransaction_template &rec);

	BOOL propExists(CTransaction_elv_properties&);
	BOOL propExists(int prop_id,int obj_id);
	BOOL propExists(int obj_id);

	BOOL propStatusExists(int id);

	BOOL cruiseExists(CTransaction_elv_cruise&);
	BOOL cruiseExists(long ecru_id,int prop_id,int obj_id);
	BOOL cruiseExists(int prop_id,int obj_id);

	BOOL evaluationExists(CTransaction_eval_evaluation&);
	BOOL evaluationExists(int eval_id,int prop_id,int obj_id);
	BOOL evaluationExists(int prop_id,int obj_id);

	BOOL cruiseRandTreesExists(CTransaction_elv_cruise_randtrees&);

	BOOL propLogBookExists(CTransaction_elv_properties_logbook&);

	BOOL propOtherCompExists(CTransaction_elv_properties_other_comp&);

	BOOL traktMiscDataExists(CTransaction_trakt_misc_data &rec);

	BOOL traktSetSpcExists(CTransaction_trakt_set_spc &rec);


public:
	CUMLandValueDB(void);
	CUMLandValueDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CUMLandValueDB(DB_CONNECTION_DATA &db_connection, int nConType = 1);

	// External database item.
	BOOL getSpecies(vecTransactionSpecies &);
	
	BOOL updateP30SpeciesInfo(int nSpecId,int nP30SpecId,int nObjId);
	BOOL createP30SpeciesInfo(vecTransactionSpecies &vec,int nObj_Id);
	BOOL getP30SpeciesInfo(vecTransactionSpecies &vec,int nObj_Id);

	BOOL getPricelists(vecTransactionPricelist &);
	BOOL getCosts(vecTransaction_costtempl &vec,int tmpl_type = -1);
	//Lagt till function f�r att plocka alla kostnader fr�n kostnadstabellen oavsett typ
	// Bug #2367 20101011 J�
	BOOL getAllCostTmpls(vecTransaction_costtempl &vec);
	BOOL getProperties(CString sql,vecTransactionProperty &vec);

	BOOL getObjectIndex(vecInt&, int object_status = -1);	//changed #3385
	BOOL getObjectWithStatus(vecTransaction_elv_object&, int status);	//new #3385
	BOOL getObject(int obj_id,CTransaction_elv_object&);
	BOOL getObject(vecTransaction_elv_object&);
	BOOL addObject(CTransaction_elv_object&);
	BOOL updObject(CTransaction_elv_object&);
	BOOL delObject(int obj_id);
	int getLastObjectIdentity(void);	// Added 090210 p�d
	short isObject(CTransaction_elv_object&,CString&,CString&);	// Added 090111 p�d
	BOOL isObjectInDB(int obj_id);	// Added 090922 p�d
	BOOL isObjectInDB_status(int obj_id, int obj_status);	//new #3385
	BOOL objectExists_status(int obj_id, int obj_status);	//new #3385
	BOOL isAnyObjectFinished(void);							//new #3385
	BOOL isAnyObjectOngoing(void);							//new #3385

	BOOL addObject_prl(int obj_id,LPCTSTR prl_name,int prl_typeof,LPCTSTR prl_xml);
	BOOL updObject_prl(int obj_id,LPCTSTR prl_name,int prl_typeof,LPCTSTR prl_xml);

	BOOL addObject_cost(int obj_id,LPCTSTR cost_name,int cost_typeof,LPCTSTR cost_xml);
	BOOL updObject_cost(int obj_id,LPCTSTR cost_name,int cost_typeof,LPCTSTR cost_xml);

	BOOL updObject_price_div(int obj_id,double price_div_1,double price_div_2);

	BOOL updObject_typeof_infr(int obj_id,LPCTSTR typeof_infr);	// Added 2009-04-03 P�D

	BOOL updObject_norm(int obj_id,LPCTSTR norm_name,int norm_typeof);

	BOOL updObject_dcls(int obj_id,double dcls);

	BOOL updObject_length(int obj_id,double obj_length);	// "Objektets totala l�ngd"

	BOOL updObject_curr_width(int obj_id,double preset_width);
	BOOL updObject_added_widths(int obj_id,double added_width1,double added_width2);
	BOOL updObject_parallel_width(int obj_id,double parallel_width);

	BOOL updObject_extra_info(int obj_id,LPCTSTR extra_info);

	BOOL updObject_recalc_by(int obj_id,double percent);

	// Handle P30
	BOOL updObject_p30(int obj_id,LPCTSTR p30_name,int p30_typeof,LPCTSTR p30_xml);
	// Handle HigherCost
	BOOL updObject_hcost(int obj_id,LPCTSTR hcost_name,int hcost_typeof,LPCTSTR hcost_xml);
	// Handle GrowthArea; 090511 p�d
	BOOL updObject_growth_area(int obj_id,LPCTSTR growtharea);
	// Handle Latitude and Altitude; 090511 p�d
	BOOL updObject_latitude_altitude(int obj_id,int lat,int alt);

	// Handle include/exclude data (may be used when creating reports etc.); 080515 p�d
	BOOL updObject_include_exclude(int obj_id,int vat,int voluntary_deal,int higher_costs,int other_comp,int grot);

	BOOL updObject_type_of_net(int obj_id,int type_of);
	// Handle update of "Prisbasbelopp,Max procent och Procent"; 090508 p�d
	BOOL updObject_price_max_percent(int obj_id,double price_base,double max_percent,double percent,double VAT,double fBindToPercentOfPriceBase);
	// Handle update of "VFall Break and Factor f�r f�rdyrat tillvaratagande"; 090528 p�d
	BOOL updObject_vfall_break_factor(int obj_id,double vfall_break,double vfall_factor);

	BOOL updObject_contractor_id(int obj_id,int id);
	BOOL updObject_retadr_id(int obj_id,int id);
	BOOL updObject_logo_id(int obj_id,int logo_id);
	BOOL updObject_status(int obj_id,int status);	//new #3385

	int getLastObjectID(void);

	BOOL matchPropertyInObjects(int obj_id,int prop_id,vecFuncObjProp &vec1,vecFuncCruiseProp &vec2);

	// Handle properties; 080415 p�d
	BOOL getProperties(int,vecTransaction_elv_properties &);
	BOOL getProperties_by_status(int,vecTransaction_elv_properties &,int prop_id  = -1);
	BOOL addProperty(CTransaction_elv_properties &);
	BOOL updProperty(CTransaction_elv_properties &);
	BOOL resetProperty(int prop_id,int obj_id);
	//Added funkction for resetting the property before calculations, 20120120 J� Bug #2769
	BOOL resetPropertyBeforeCalc(int prop_id,int obj_id);
	BOOL delProperty(CTransaction_elv_properties &);

	BOOL updPropertyStatus(int prop_id,int obj_id,int status);
	BOOL updPropertyStatus2(int prop_id,int obj_id,int status);
	BOOL updPropertyGroupIdentity(int prop_id,int obj_id,LPCTSTR group_id);
	BOOL updPropertyTypeOfAction(int prop_id,int obj_id,int type_of_action);
	BOOL updPropertySortOrder(int prop_id,int obj_id,int sort_number);
	BOOL getPropertySortMax(int obj_id,int* sort_number);

	// Handle Property dates; 080529 p�d
	BOOL updPropertyNotifyDate(int obj_id,LPCTSTR landowner_num);
	BOOL updPropertyCompOfferDate(int obj_id,LPCTSTR landowner_num);
	BOOL updPropertyPayDate(int obj_id,LPCTSTR landowner_num);

	BOOL getPropertyOwners(vecTransactionPropOwners &);

	BOOL getPropertyOwners(int prop_id,vecTransactionContacts &);

	// Added 2009-09-14 p�d
	BOOL getPropertyActionStatus(vecTransaction_property_status &);
	BOOL addPropertyActionStatus(CTransaction_property_status &);
	BOOL updPropertyActionStatus(CTransaction_property_status &);
	BOOL delPropertyActionStatus(CTransaction_property_status &);
	BOOL getNextPropertyActionStatus_id(short *);
	BOOL getNextPropertyActionStatus_ordernum(short *);
	BOOL resetPropetyActionStatusID(void);
	BOOL isPropertyActionStatusUsed(int status_id);
	BOOL getPropertyActionStatusUsed(vecTransaction_property_status &,mapBoolean &);

	// Added 2017-01-10 PH
	std::vector<searchResult> getSearchList();

	BOOL getPropertyInfoForDialog(int id,LPCTSTR &objName,LPCTSTR &propName,LPCTSTR &propNumber,int &objId); //#5009 PH 20160616

	// Added 090302 p�d
	BOOL getPropertyOwnersForProperty(int prop_id,vecTransactionPropOwners&);
	// Added 090302 p�d
	BOOL setPropertyOwnersForProperty_IsContact(int prop_id,int contact_id,int is_contact,LPCTSTR share);

	// Handle Property logbook; 080417 p�d
	BOOL getPropertyLogBook(int,int,vecTransaction_elv_properties_logbook &);
	BOOL addPropertyLogBook(CTransaction_elv_properties_logbook &);
	BOOL updPropertyLogBook(CTransaction_elv_properties_logbook &);
	BOOL delPropertyLogBook(CTransaction_elv_properties_logbook &);
	BOOL delPropertyLogBook(int prop_id,int obj_id);
	int getLastPropertyLogBookID(int obj_id);
	int getNumOfEntriesForPropertyInLogBook(int prop_id,int obj_id);
	BOOL getNumOfEntriesForPropertyInLogBook(int obj_id,std::map<int,int>& map);

	// Handle Property Other costs ("Annan ers�ttning"); 080516 p�d
	BOOL getPropertyOtherComp(int,vecTransaction_elv_properties_other_comp &);
	BOOL getPropertyOtherComp(int,int,vecTransaction_elv_properties_other_comp &);
	BOOL addPropertyOtherComp(CTransaction_elv_properties_other_comp &);
	BOOL updPropertyOtherComp(CTransaction_elv_properties_other_comp &);
	BOOL delPropertyOtherComp(int,int);
	BOOL delPropertyOtherComp(int,int,int);
	int getPropertyOtherCompNextIndex(int,int);

	// Update/remove information on connection between Trakt and Property/Object
	BOOL removePropTraktConnection(int trakt_id);

	// Object cruise table; 080506 p�d
	BOOL getObjectCruises(int prop_id,int obj_id,vecTransaction_elv_cruise &);
	BOOL getObjectCruises(int obj_id,vecTransaction_elv_cruise &);
	BOOL getObjectCruises(vecTransaction_elv_cruise &);
	long getObjectCruise_last_id(int obj_id,int prop_id,int cruise_type);
	long getObjectCruise_id_exists(long ecru_id,int obj_id,int prop_id);
	BOOL getObjectCruisesNotCalculatedForObject(int obj_id,vecTransaction_elv_cruise &);
	BOOL addObjectCruise(CTransaction_elv_cruise &);
	BOOL updObjectCruise(CTransaction_elv_cruise &);
	BOOL delObjectCruise(int prop_id,int obj_id,int cruise_type);
	BOOL delObjectCruise(long ecru_id,int prop_id,int obj_id,int cruise_type);

	BOOL getObjectProperty(int prop_id,int obj_id,CTransaction_elv_properties &);

//	BOOL updObjectCruise_use_sample_trees(int ecru_id,int prop_id,int obj_id,int value);
	// Use column "ecru_use_sample_trees", for information on wich side
	// the stand's been mesaured. Use in widning (Breddning ny norm); 080922 p�d
	BOOL updObjectCruise_wside(long ecru_id,int prop_id,int obj_id,int value);
	BOOL updObjectCruise_do_reduce_rotpost(long ecru_id,int prop_id,int obj_id,int value);
	BOOL updObjectCruise_width(long ecru_id,int prop_id,int obj_id,double value);

	BOOL updObjectCruise_randtrees(long ecru_id,int prop_id,int obj_id,double randtrees_volume,double randtrees_value);
	
	BOOL updObjectCruise_storm_and_dry(long ecru_id,int prop_id,int obj_id,double volume,double value,double mix);

	BOOL updObjectCruise_rotpost(long ecru_id,int prop_id,int obj_id,double volume,double value,double cost,double netto);

	BOOL updObjectCruise_use_for_infr(long ecru_id,int prop_id,int obj_id,int use);

	// Move manually entered stands from elv_cruise_table to elv_manual_cruise_table
	// and on success, delete manually entered from elv_cruise_table; 100503 p�d
	//BOOL moveManualCruiseData(int obj_id,int prop_id);

	// Object evaluation table; 080514 p�d
	BOOL checkObjectEvaluationsExist(int obj_id);
	BOOL getObjectEvaluation(int prop_id,int obj_id,vecTransaction_eval_evaluation &);
	CTransaction_eval_evaluation* getObjectEvaluation(int prop_id,int obj_id,int eval_id);
	int getObjectEvaluated_last_id(int obj_id,int prop_id);
	BOOL getObjectEvaluationNotCalculatesForObject(int obj_id,vecTransaction_eval_evaluation &);
	BOOL addObjectEvaluation_entered_data(CTransaction_eval_evaluation &);
	BOOL updObjectEvaluation_entered_data(CTransaction_eval_evaluation &);
	
	BOOL addObjectEvaluation_other_data(CTransaction_eval_evaluation &);
	BOOL updObjectEvaluation_other_data(CTransaction_eval_evaluation &);
	
	BOOL updObjectEvaluation_calculated_data(
		int eval_id,
		int prop_id,
		int obj_id,
		int eval_type,
		double land_value_ha,
		double early_cut_ha,
		double land_value_stand,
		double early_cut_stand,
		double fPreCutInfo_P30_Pine,
		double fPreCutInfo_P30_Spruce,
		double fPreCutInfo_P30_Birch,
		double fPreCutInfo_Andel_Pine,
		double fPreCutInfo_Andel_Spruce,
		double fPreCutInfo_Andel_Birch,
		double fPreCutInfo_CorrFact,
		double fPreCutInfo_Ers_Pine,
		double fPreCutInfo_Ers_Spruce,
		double fPreCutInfo_Ers_Birch,
		double fPreCutInfo_ReducedBy,
		//HMS-48 Info om markv�rde 20200427 J�
		double fMarkInfo_ValuePine,
		double fMarkInfo_ValueSpruce,
		double fMarkInfo_Andel_Pine,
		double fMarkInfo_Andel_Spruce,
		double fMarkInfo_P30_Pine,
		double fMarkInfo_P30_Spruce,
		double fMarkInfo_ReducedBy
		);
	BOOL updObjectEvaluation_calculated_corrfac(int eval_id,int obj_id,int prop_id,double corr_fac);
	BOOL delObjectEvaluation(int eval_id,int obj_id,int prop_id);
	BOOL delObjectEvaluation(int obj_id,int prop_id);
	BOOL delObjectEvaluations(int obj_id);
	BOOL getObjectEvaluation_last_id(int obj_id,int prop_id,int *id);
	BOOL delObjectEvaluation_cruise(int obj_id,int prop_id);

	// Object cruise randtrees table; 080512 p�d
	BOOL getObjectCruiseRandtrees(int cruise_id,int prop_id,int obj_id,vecTransaction_elv_cruise_randtrees &);
	BOOL addObjectCruiseRandtrees(CTransaction_elv_cruise_randtrees &);
	BOOL delObjectCruiseRandtrees(int cruise_id,int prop_id,int obj_id);

	// ObjectTemplate database items; 080402 p�d
	BOOL getObjectTemplates(vecTransactionTemplate &,int tmpl_type);
	BOOL addObjectTemplate(CTransaction_template &);
	BOOL updObjectTemplate(CTransaction_template &);
	BOOL delObjectTemplate(CTransaction_template &);

	// ObjectTemplate database items; 080402 p�d
	BOOL getObjectDocumentTemplates(vecTransactionTemplate &);
	BOOL addObjectDocumentTemplate(CTransaction_template &);
	BOOL updObjectDocumentTemplate(CTransaction_template &);
	BOOL delObjectDocumentTemplate(CTransaction_template &);

	BOOL getTrakt(int trakt_id,CTransaction_trakt &);
	BOOL getTrakts_match_stand(int obj_id,int prop_id,vecTransactionTrakt &);
	BOOL getTrakts_match_stand_by_elv_cruise(int obj_id,int prop_id,vecTransactionTrakt &);
	
	BOOL updTrakts_lat_long_hgt_over_sea(int trakt_id,int latitude,int longitude,int hgt_over_sea);	// Added 100426 p�d

	BOOL updTrakts_use_for_infr(int trakt_id,int use);	// Added 100702 p�d

	BOOL delTrakt(int trakt_id);

	BOOL getPlots(int trakt_id,vecTransactionPlot &);
	BOOL getSampleTrees(int trakt_id,int tree_type,vecTransactionSampleTree &);
	BOOL getDCLSTreesforJsonExport(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLSTrees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLS1Trees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLS2Trees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getTraktSetSpc(int trakt_id,vecTransactionTraktSetSpc &);
	BOOL updTraktMiscData(CTransaction_trakt_misc_data &);	// Added 080507 p�d
	BOOL getTraktMiscData(int trakt_id,CTransaction_trakt_misc_data &);
	BOOL getTraktData(vecTransactionTraktData &,int trakt_id = -1,int data_type = -1);
	BOOL addTraktData(CTransaction_trakt_data &);
	BOOL updTraktData(CTransaction_trakt_data &);
	BOOL updTraktData_soderbergs(int trakt_id,BOOL near_coast,BOOL southeast,BOOL region5,BOOL part_of_plot);
	BOOL updTraktData_h25(int trakt_id,int spc_id,double h25);
	BOOL updTraktData_gcrown(int trakt_id,int spc_id,double gcrown);
	BOOL removeTraktDataSpecies(int trakt_id,LPCTSTR sql);

	BOOL addSampleTrees(CTransaction_sample_tree &);
	BOOL updSampleTrees(CTransaction_sample_tree &);

	BOOL addDCLSTrees(CTransaction_dcls_tree &);
	BOOL updDCLSTrees(CTransaction_dcls_tree &);
	BOOL delDCLSTreesInTrakt(int trakt_id);

	BOOL addDCLS1Trees(CTransaction_dcls_tree &);
	BOOL updDCLS1Trees(CTransaction_dcls_tree &);
	BOOL delDCLS1TreesInTrakt(int trakt_id);

	BOOL addDCLS2Trees(CTransaction_dcls_tree &);
	BOOL updDCLS2Trees(CTransaction_dcls_tree &);
	BOOL delDCLS2TreesInTrakt(int trakt_id);

	BOOL getTraktSpcDistinct(int trakt_id,int type_of,vecInt& vec);
	
	BOOL getTraktLandOwnerNumberDistinct(int obj_id,vecString& vec);

	BOOL getTraktAss(vecTransactionTraktAss &vec,int trakt_id);
	BOOL addTraktAss(CTransaction_trakt_ass &rec);
	BOOL updTraktAss(CTransaction_trakt_ass &rec);
	BOOL addAssTree(CTransaction_tree_assort &);
	BOOL delAssTree(int trakt_id);
	BOOL updTraktAss_prices(int trakt_id,int trakt_data_id,int data_type,int assort_id,double exch_price);
	BOOL resetTraktAss(int trakt_id);
	BOOL removeTraktAss(int trakt_id,int trakt_data_id);

	BOOL getTraktTrans(vecTransactionTraktTrans &vec,int trakt_id);
	BOOL removeTraktTrans(int trakt_data_id,int trakt_id);
	BOOL delTraktTrans(int trakt_id,int spc_id);
	BOOL delTraktTrans(int trakt_id);

	BOOL addRotpost(CTransaction_trakt_rotpost &rec);
	BOOL updRotpost(CTransaction_trakt_rotpost &rec);

	BOOL getLandValueTrees(int trakt_id,int tree_type,vecTransactionSampleTree &vec);
	BOOL getLandValueRandTrees(int trakt_id,vecTransactionSampleTree &vec);

	BOOL getContacts(vecTransactionContacts &);
	BOOL getContacts(CString sql,vecTransactionContacts &);
	BOOL getContacts(CString category,CString sql,vecTransactionContacts &);
	BOOL getContact(int,CTransaction_contacts &);

	BOOL getTemplates(vecTransactionTemplate &vec,int tmpl_type);

	// Method SUM data for object and property from trakt(s). E.g. Areal,rotpost information etc.; 080430 p�d
	BOOL getObjPropTraktSUM_stands_areal(int obj_id,int prop_id,int *,double *);
	BOOL getObjPropTraktSUM_numof_volume(int obj_id,int prop_id,long *,double *);
	BOOL getObjPropTraktSUM_rotpost(int obj_id,int prop_id,double *,double *,double *,double *);
	BOOL getObjPropTraktSUM_storm_dry_randtrees(int obj_id,int prop_id,double *storm_dry_value,double *randtree_value);
	BOOL getObjPropTraktSUM_grot(int obj_id,int prop_id,double *grot_volume,double *grot_value,double *grot_cost);
	BOOL getObjPropTraktSUM_landvalue_precut(int obj_id,int prop_id,double *land_value,double *precut_value);
	BOOL updObjPropTraktSUM_1(int prop_id,int obj_id,
														int numof_stands,double areal,
														long numof_trees,double m3sk,
														double storm_dry_value,double randtrees_value);
	BOOL updObjPropTraktSUM_2(int prop_id,int obj_id,
														double land_value,double precut_value);
	BOOL updObjPropTraktSUM_3(int prop_id,int obj_id,double voluntary_deal_value);
	BOOL updObjPropTraktSUM_4(int prop_id,int obj_id,double high_cost_volume,double higher_costs);
	BOOL updObjPropTraktSUM_5(int prop_id,int obj_id,double other_comp);
	BOOL updObjPropTraktSUM_6(int prop_id,int obj_id,
														double timber_volume,double timber_value,
														double timber_cost,double rot_netto);
	BOOL updObjPropTraktSUM_grot(int prop_id,int obj_id,double grot_volume,double grot_value,double grot_cost);
	BOOL updObjPropTraktSUM_7(int prop_id,int obj_id,double areal);	// Added 2009-09-14 p�d

	// Aggregate data on Trakt to be added to elv_cruise_table; 080506 p�d
	BOOL getObjPropTrakt_numof_volume(int trakt_id,long *,double *);
	BOOL getObjPropTrakt_rotpost(int trakt_id,double *,double *,double *,double *);
	BOOL getObjPropTrakt_m3sk_volumes_per_spc(int trakt_id,double trakt_areal,vecTransaction_elv_m3sk_per_specie& vec);
	// "Summera GROT Volym (ton) och V�rde (kr)
	BOOL getObjProp_grot(int trakt_id,double *sum_grot_volume,double *sum_grot_value,double *sum_grot_cost);

	BOOL getObjPropTraktCruise_randtrees(int cruise_id,int obj_id,int prop_id,double *randtree_volume,double *randtree_value);

	// "Summerar markv�rde,f�rtidig avverkning,storm- och tork och kanttr�d"; 080515 p�d
	BOOL getObjProp_infr(int obj_id,int prop_id,double *sum_landvalue,double *sum_pre_cut,double *sum_storm_dry,double *sum_randtrees);
	

	// "Metoder f�r att matcha mark�gare till fastigheter"; 080516 p�d
	BOOL getObjProp_landowners_per_prop(int obj_id,int prop_id,vecInt& data);

	BOOL getObjProp_landowner_id_numofs_per_property(int prop_id,int obj_id,int *num_of);

	BOOL getObjProp_match_landowner_ids_per_properties(LPCTSTR sSQL,int numof,vecInt&);

	BOOL getObjectAllCruiseStands(int obj_id,vecTransaction_elv_cruise_id &);

	BOOL getTraktsPerLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecInt&);
	BOOL getSLenForTraktsPerLandOwnerNumber(LPCTSTR sql,vecTransaction_elv_slen_data&);
	
	BOOL getSLenMinAndMaxDCLS(LPCTSTR sql,double *min_dcls,double *max_dcls);
	BOOL getSLenSummmarizedData(LPCTSTR sql,vecTransaction_elv_slen_sum_data&);
	BOOL getSLenDiameterclasses(LPCTSTR sql,vecInt&);
	BOOL getPropertiesByLandOwnerNumber(int obj_id,LPCTSTR landowner_number,vecString&);

	BOOL getEvaluationsPerLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_eval_evaluation&);
	BOOL getStormDryForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,double *volume,double *value);
	BOOL getRandTreesForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_elv_cruise_randtrees&);
	BOOL getHighCostForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,double *volume,double *value);
	BOOL getOtherCompForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_elv_properties_other_comp&);
	BOOL getRotpostDataForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,double *gagn_vol,double *value,double *cost,double *netto);
	BOOL getCruisesForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_elv_cruise &);

	BOOL getAssortmentsByTrakts(LPCTSTR sql,vecTransactionTraktAss &vec);

	BOOL calculateK3PerM3OnObjectPropertyStands(int obj_id,double value,LPCTSTR assortment,int price_set_in);

	BOOL getAssortmentsForTraktAndObject(int obj_id,vecTransaction_elv_ass_stands_obj &);

	BOOL getCategories(vecTransactionCategory &vec);
	// Get ALL properties in Database table; 090331 p�d
	BOOL getProperties(vecTransactionProperty &);

	BOOL getNumOfEvaluations(int obj_id,int prop_id,int *num_of);
	BOOL getObjVolDealValues(int obj_id,int *type,double *price_base,double *max_perc,double *perc,double *perc_pricebase);
	//---------------------------------------------------------------------------------------
	// Added 2009-05-09 p�d
	// Methods used e.g. update/change "Best�ndsmall"; 090506 p�d
	BOOL updTraktMiscData_prl(CTransaction_trakt_misc_data &);
	BOOL updTraktMiscData_dcls(CTransaction_trakt_misc_data &);
	BOOL updTraktMiscData_costtmpl(CTransaction_trakt_misc_data &);
	BOOL addTraktTrans(CTransaction_trakt_trans &rec);	// 101208 p�d
	BOOL updTraktTrans(CTransaction_trakt_trans &rec);
	// Use these methods for changing template data; 090622 p�d
	BOOL addTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_volfunc(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_volfunc(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_m3fub_m3to(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_m3fub_m3to(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_grot(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_grot(CTransaction_trakt_set_spc &);
	BOOL addTraktSetSpc_default_h25(CTransaction_trakt_set_spc &);
	BOOL updTraktSetSpc_default_h25(CTransaction_trakt_set_spc &);
	BOOL addObjectLog(CElv_object_log &);	// #3877
	//---------------------------------------------------------------------------------------

	BOOL getExternalDocuments(LPCTSTR tbl_name,int link_id,vecTransaction_external_docs &vec);
	BOOL loadImage(CString path,int id);

	//#4207 J� 20141112
	BOOL GetCategories(vecTransactionSampleTreeCategory &vec);

	//#5010 PH 20160613
	BOOL removePropOwner(int,int);

};


/////////////////////////////////////////////////////////////////////////////////
// CPropertyRemoveReportDataRec

class CPropertyRemoveReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CPropertyRemoveReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CPropertyRemoveReportDataRec(UINT index,CTransaction_property data)	 
	{
		CString sValue;
		m_nIndex = index;
		AddItem(new CTextItem((data.getObjectID())));
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
		AddItem(new CTextItem((data.getParishName())));
		AddItem(new CTextItem((data.getPropertyNum())));
		AddItem(new CTextItem((data.getPropertyName())));
		AddItem(new CTextItem((data.getBlock())));
		AddItem(new CTextItem((data.getUnit())));
		sValue.Format(_T("%.1f"),data.getAreal());
		AddItem(new CTextItem((sValue)));
		sValue.Format(_T("%.1f"),data.getArealMeasured());
		AddItem(new CTextItem((sValue)));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

#endif