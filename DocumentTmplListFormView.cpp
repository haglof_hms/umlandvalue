// DocumentTmplListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DocumentTmplFormView.h"
#include "DocumentTmplListFormView.h"


// CDocumentTmplListFormView

IMPLEMENT_DYNCREATE(CDocumentTmplListFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CDocumentTmplListFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, ID_REPORT_DOC_TEMPLATE, OnReportClick)
END_MESSAGE_MAP()

CDocumentTmplListFormView::CDocumentTmplListFormView()
	: CXTResizeFormView(CDocumentTmplListFormView::IDD)
{
	m_bInitialized = FALSE;
}

CDocumentTmplListFormView::~CDocumentTmplListFormView()
{
}

void CDocumentTmplListFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}
void CDocumentTmplListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndTemplates.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTemplates,1,1,rect.right - 1,rect.bottom - 2);
	}
	
}

BOOL CDocumentTmplListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CDocumentTmplListFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (!	m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		CDocumentTmplFormView *pView = (CDocumentTmplFormView *)getFormViewByID(IDD_FORMVIEW8);
		if (pView)
		{
			m_nDBIndex = pView->getDBIndex();
		}

		if (setupReport())
		{
			getTemplatesFromDB();
			populateData();
		}

		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

BOOL CDocumentTmplListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CDocumentTmplListFormView diagnostics

#ifdef _DEBUG
void CDocumentTmplListFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CDocumentTmplListFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// Protected
void CDocumentTmplListFormView::setupDoPopulate(void)
{
	CDocumentTmplFormView *pView = (CDocumentTmplFormView *)getFormViewByID(IDD_FORMVIEW8);
	if (pView)
	{
		pView->doPouplate(m_nDBIndex);
	}
}

// Create and add PricelistsList reportwindow
BOOL CDocumentTmplListFormView::setupReport(void)
{

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndTemplates.GetSafeHwnd() == 0)
	{
		// Create the Reportcontrol
		if (!m_wndTemplates.Create(this, ID_REPORT_DOC_TEMPLATE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndTemplates.GetSafeHwnd() != NULL)
				{

					m_sarrDocTypes.Add((xml->str(IDS_STRING56050)));
					m_sarrDocTypes.Add((xml->str(IDS_STRING56051)));
					m_sarrDocTypes.Add((xml->str(IDS_STRING56052)));
					m_sarrDocTypes.Add((xml->str(IDS_STRING56053)));
					m_wndTemplates.ShowWindow( SW_NORMAL );
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING5609)), 100));
					pCol->AllowRemove(FALSE);
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING5610)), 100));
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(2, (xml->str(IDS_STRING5611)), 100));
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(3, (xml->str(IDS_STRING5612)), 100));

					m_wndTemplates.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndTemplates.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndTemplates.GetReportHeader()->AllowColumnResize( TRUE );
					m_wndTemplates.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndTemplates.GetReportHeader()->SetAutoColumnSizing( TRUE );
					m_wndTemplates.SetMultipleSelection( FALSE );
					m_wndTemplates.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndTemplates.AllowEdit(FALSE);
					m_wndTemplates.FocusSubItems(TRUE);

					RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
					setResize(&m_wndTemplates,1,1,rect.right - 1,rect.bottom - 2);

				}	// if (m_wndTemplates.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;

	}	// if (fileExists(m_sLangFN))

	return TRUE;

}

BOOL CDocumentTmplListFormView::populateData(void)
{
	CString sTypeOfDocument;
	CTransaction_template tmplSel;
	CXTPReportRecord *pRec = NULL;
	if (m_vecTransactionTemplate.size() > 0)
	{
		if (m_nDBIndex >= 0 && m_nDBIndex < m_vecTransactionTemplate.size())
		{
			tmplSel = m_vecTransactionTemplate[m_nDBIndex];
		}
		for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		{
			CTransaction_template tmpl = m_vecTransactionTemplate[i];
			if (tmpl.getTypeOf() >= 0 && tmpl.getTypeOf() < m_sarrDocTypes.GetCount())
				sTypeOfDocument = m_sarrDocTypes[tmpl.getTypeOf()];
			else
				sTypeOfDocument.Empty();
			if (tmpl.getID() == tmplSel.getID())
			{
				pRec = m_wndTemplates.AddRecord(new CDocumentTmplListReportDataRec(i+1,sTypeOfDocument,tmpl.getTemplateName(),tmpl.getCreatedBy(),tmpl.getCreated()));
			}
			else
			{
				m_wndTemplates.AddRecord(new CDocumentTmplListReportDataRec(i+1,sTypeOfDocument,tmpl.getTemplateName(),tmpl.getCreatedBy(),tmpl.getCreated()));
			}
		}
		m_wndTemplates.Populate();
//		m_wndTemplates.UpdateWindow();
		// Added 081006 p�d
		m_wndTemplates.RedrawControl();
		if (pRec)
		{
			CXTPReportRow *pRow = m_wndTemplates.GetRows()->Find(pRec);
			if (pRow)
			{
				m_wndTemplates.SetFocusedRow(pRow);
			}
		}

		return TRUE;
	}
	return FALSE;
}

void CDocumentTmplListFormView::getTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		CUMLandValueDB *pDB = new CUMLandValueDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_bConnected = pDB->getObjectDocumentTemplates(m_vecTransactionTemplate);
			delete pDB;
		}
	}
}

// CDocumentTmplListFormView message handlers

void CDocumentTmplListFormView::OnReportClick(NMHDR* pNMHDR, LRESULT* pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;

	if (m_wndTemplates.GetSafeHwnd() == NULL)
		return;

	if (pItemNotify->pRow)
	{
		CTemplateListReportDataRec *pRec = (CTemplateListReportDataRec *)pItemNotify->pItem->GetRecord();
		if (pRec)
		{
			m_nDBIndex = pRec->GetIndex();
			setupDoPopulate();
		}

	}
	*pResult = 0;
}
