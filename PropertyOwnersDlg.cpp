// PropertyOwnersDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PropertyOwnersDlg.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CPropertyOwnersDlg dialog

IMPLEMENT_DYNAMIC(CPropertyOwnersDlg, CDialog)


BEGIN_MESSAGE_MAP(CPropertyOwnersDlg, CDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDOK, &CPropertyOwnersDlg::OnBnClickedOk)
	ON_NOTIFY(NM_CLICK, ID_REPORT_PROP_OWNERS, OnReportItemClick)
	//Lagt till knapp f�r fastighetsgenv�g 20111014 J� Feature #2452
	ON_BN_CLICKED(IDC_BTN_PROPSHORTCUT, &CPropertyOwnersDlg::OnBnClickedBtnPropshortcut)
	ON_BN_CLICKED(IDC_ADD_OWNER_BUTTON, &CPropertyOwnersDlg::OnBnClickedAddOwnerButton)
	ON_BN_CLICKED(IDC_REMOVE_OWNER_BUTTON, &CPropertyOwnersDlg::OnBnClickedRemoveOwnerButton)
END_MESSAGE_MAP()

CPropertyOwnersDlg::CPropertyOwnersDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropertyOwnersDlg::IDD, pParent)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
	// #5010 PH 20160613
	currentOwnerId=-1; 
	m_nObjId=-1;
}

CPropertyOwnersDlg::~CPropertyOwnersDlg()
{
		m_ilIcons.DeleteImageList();
}

void CPropertyOwnersDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_OBJ_NAME, m_wndLblObjName);
	DDX_Control(pDX, IDC_PROP_NAME, m_wndLblPropName);
	DDX_Control(pDX, IDC_PROP_NUM, m_wndLblPropNum);
	//Lagt till knapp f�r fastighetsgenv�g 20111014 J� Feature #2452
	DDX_Control(pDX, IDC_BTN_PROPSHORTCUT, m_wndBtnPropShortCut);

	//#5010 PH 20160614
	DDX_Control(pDX, IDC_ADD_OWNER_BUTTON, m_wndBtnAddOwner);
	DDX_Control(pDX, IDC_REMOVE_OWNER_BUTTON, m_wndBtnRemoveOwner);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	//}}AFX_DATA_MAP
}

BOOL CPropertyOwnersDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING3250)));

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING204)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING3203)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING3204)));
			m_wndLblObjName.SetLblFontEx(-1,FW_BOLD);
			m_wndLblPropName.SetLblFontEx(-1,FW_BOLD);
			m_wndLblPropNum.SetLblFontEx(-1,FW_BOLD);

			//#5010 PH 20160614
			m_wndBtnAddOwner.SetWindowText((xml->str(IDS_STRING33113)));
			m_wndBtnRemoveOwner.SetWindowText((xml->str(IDS_STRING33114)));

			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING113)));
		}
		delete xml;
	}
	//Lagt till knapp f�r fastighetsgenv�g 20111014 J� Feature #2452
	m_wndBtnPropShortCut.SetBitmap(CSize(14,14),IDB_BITMAP3);
	setupReport();

	getPropertyOwners();

	populateData();

	m_bInitialized = TRUE;

	return TRUE;
}

void CPropertyOwnersDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);
}


// CPropertyOwnersDlg message handlers


void 	CPropertyOwnersDlg::getPropertyOwners()
{
	if (m_pDB != NULL)
	{
		m_pDB->getPropertyOwners(m_nPropId,m_vecContacts);
		m_pDB->getPropertyOwnersForProperty(m_nPropId,m_vecPropertyOwners);
	}
}

void CPropertyOwnersDlg::populateData()
{
	BOOL bIsCheck;
	CTransaction_contacts rec;
	m_wndLblObjName.SetWindowText((m_sObjectName));
	m_wndLblPropName.SetWindowText((m_sPropName));
	m_wndLblPropNum.SetWindowText((m_sPropNum));

	m_wndReport.ClearReport();

	// Is there any data do display; 080423 p�d
	if (m_vecContacts.size() > 0)
	{
		for (UINT i = 0;i < m_vecContacts.size();i++)
		{
			rec = m_vecContacts[i];

			// Check if Propretyowner also is set as contact; 090302 p�d
			if (m_vecPropertyOwners.size() > 0)
			{
				bIsCheck = FALSE;
				for (UINT i1 = 0;i1 < m_vecPropertyOwners.size();i1++)
				{
					m_recPropertyOwners = m_vecPropertyOwners[i1];
					if (m_recPropertyOwners.getContactID() == rec.getID())
					{
						bIsCheck = (m_recPropertyOwners.getIsContact() == 1);
						break;
					}	// if (m_recPropertyOwners.getContactID() == rec.getID())
				}	// for (UINT i1 = 0;i1 < m_vecPropertyOwners.size();i1++)
			}	// if (m_vecPropertyOwners.size() > 0)

			m_wndReport.AddRecord(new CPropOwnerReportRec(rec.getID(),bIsCheck,rec.getOwnerShare(),rec.getName(),rec.getAddress(),rec.getPostAddress(),rec.getPhoneWork(),rec.getPhoneHome(),rec.getMobile()));
			if(currentOwnerId==-1)
				currentOwnerId=rec.getID();
		}	// for (UINT i = 0;i < m_vecContacts.size();i++)
		
	}	// if (m_vecContacts.size() > 0)
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CPropertyOwnersDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (!m_wndReport.GetSafeHwnd())
	{
		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, ID_REPORT_PROP_OWNERS, TRUE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd())
	{
		VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
		m_ilIcons.Add(&bmp, RGB(255, 0, 255));

		m_wndReport.SetImageList(&m_ilIcons);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
					m_wndReport.ShowWindow( SW_NORMAL );

					// "Kontakt"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING3258)), 50,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_LEFT);
					// "Andel"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING3252)), 50,FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_nMaxLength = 10;
					// "Name"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING3251)), 180));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// "Adress"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING3253)), 120));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// "PostAdress"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING3254)), 150));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// "Tel.arb"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING3255)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// "Tel.hem."
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml.str(IDS_STRING3256)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// "Mobil"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml.str(IDS_STRING3257)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndReport.SetMultipleSelection( FALSE );
					m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport.AllowEdit(TRUE);
					m_wndReport.FocusSubItems(TRUE);
					m_wndReport.AdjustScrollBars();

					// Need to set size of Report control; 051219 p�d
					RECT rect;
					GetClientRect(&rect);
					setResize(GetDlgItem(ID_REPORT_PROP_OWNERS),5,50,rect.right - 10,rect.bottom - 90);
					xml.clean();
			}	// if (m_xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))
	}	// if (m_wndReport.GetSafeHwnd() != NULL)

}

void CPropertyOwnersDlg::OnBnClickedOk()
{
	CPropOwnerReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CPropOwnerReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (m_pDB != NULL)
					m_pDB->setPropertyOwnersForProperty_IsContact(m_nPropId,pRec->getCOntactID(),pRec->getColumnCheck(COLUMN_0),pRec->getColumnText(COLUMN_1));
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)

	}	// if (pRows != NULL)
	OnOK();
}


void CPropertyOwnersDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	int nNumOfEntries = -1;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;



	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	CPropOwnerReportRec *pRec = (CPropOwnerReportRec*)pItemNotify->pItem->GetRecord();
	if(pRec!=NULL)
		currentOwnerId=pRec->getCOntactID();

	//Added check to be able to open a contact through forrest suite feature #2263 J�
	switch (pItemNotify->pColumn->GetItemIndex())
	{
		//Only take care of when the name column has been clicked
	case COLUMN_2 :
		{
			// Do a hit-test; 080513 p�d
			rect = pItemNotify->pColumn->GetRect();		
			pt = pItemNotify->pt;
			// Check if the user clicked on the Icon or not; 080513 p�d
			if (hitTest_X(pt.x,rect.left,13))
			{
				
				if (pRec != NULL)
				{
				//Open contact details for this specific owner
				CString sLangFN(getLanguageFN(getLanguageDir(),_T("Forrest"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
				showFormView(113,sLangFN);
				//Send message to forest suite to open contact form with a specific contact id, feature #2263 J� 20110808
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
					(LPARAM)&_doc_identifer_msg(_T("Module113"),_T("Module113"),_T(""),3 /* 3 = Identifer for UMLandVlaue module */,pRec->getCOntactID(),m_nObjId));
				OnBnClickedOk();
				
				}	// if (pRec != NULL)
			}	// if (hitTest_X(pt.x,rect.left,13))
			break;
		}
	};
}

//Lagt till knapp f�r fastighetsgenv�g 20111014 J� Feature #2452
void CPropertyOwnersDlg::OnBnClickedBtnPropshortcut()
{
	CString sLangFN(getLanguageFN(getLanguageDir(),_T("Forrest"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	showFormView(118,sLangFN);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
		(LPARAM)&_doc_identifer_msg(_T("Module118"),_T("Module118"),_T(""),10 /* 10 = Identifer for UMLandVlaue module */,m_nPropId,m_nObjId));
	OnBnClickedOk();

}

//#5010 PH 20160614
void CPropertyOwnersDlg::OnBnClickedAddOwnerButton()
{
	CString sLangFN(getLanguageFN(getLanguageDir(),_T("Forrest"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	showFormView(113,sLangFN);
	//Send message to forest suite to open new contact form
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
		(LPARAM)&_doc_identifer_msg(_T("Module113"),_T("Module113"),_T(""),4 /* 4 = Identifer for UMLandVlaue module */,m_nPropId,0));
	OnBnClickedOk();
}

//#5010 PH 20160614
void CPropertyOwnersDlg::OnBnClickedRemoveOwnerButton()
{
	RLFReader *xml = new RLFReader;
	if (xml->Load(m_sLangFN)) {
		if (::MessageBox(this->GetSafeHwnd(),xml->str(IDS_STRING2221),xml->str(IDS_STRING2222),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			return;
		int propertyId;
		if(currentOwnerId!=-1) {
			if (m_pDB != NULL) {
				//propertyId=m_recProperty.getPropID_pk();
				m_pDB->removePropOwner(m_nPropId,currentOwnerId);
			}
			currentOwnerId=-1;
			getPropertyOwners();
			populateData();
			
		}
	}
}

