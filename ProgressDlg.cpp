// ProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ProgressDlg.h"


// CProgressDlg dialog

IMPLEMENT_DYNAMIC(CProgressDlg, CDialog)

BEGIN_MESSAGE_MAP(CProgressDlg, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

CProgressDlg::CProgressDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDlg::IDD, pParent)
{

	m_nIsShowDlg = SW_HIDE;

	// initialize progress control.
	m_wndProgress_props.SetRange (0, 100);
	m_wndProgress_props.SetPos (0);
	m_wndProgress_props.SetStep (10);

	m_wndProgress_stand.SetRange (0, 100);
	m_wndProgress_stand.SetPos (0);
	m_wndProgress_stand.SetStep (10);

}

CProgressDlg::~CProgressDlg()
{
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHigherCostsFormView)
	DDX_Control(pDX, IDC_LBL23_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL23_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL23_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL23_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL23_7, m_wndLbl7);

	DDX_Control(pDX, IDC_PROGRESS23_1, m_wndProgress_props);
	DDX_Control(pDX, IDC_PROGRESS23_2, m_wndProgress_stand);
	//}}AFX_DATA_MAP

}


BOOL CProgressDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_brush.CreateSolidBrush(RGB(255, 255, 255)); // color white brush

	m_wndLbl5.SetLblFontEx(-1,FW_BOLD);
	m_wndLbl6.SetLblFontEx(-1,FW_BOLD);
	m_wndLbl7.SetLblFontEx(-1,FW_BOLD);

	// initialize progress controls
	m_wndProgress_props.SetRange (0, 100);
	m_wndProgress_props.SetPos (0);
	m_wndProgress_props.SetStep (10);

	m_wndProgress_stand.SetRange (0, 100);
	m_wndProgress_stand.SetPos (0);
	m_wndProgress_stand.SetStep (10);

	return FALSE;
}

// CProgressDlg message handlers

HBRUSH CProgressDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	return m_brush;
}
