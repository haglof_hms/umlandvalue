#pragma once

#include "Resource.h"


class CContactsReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CContactsReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CContactsReportDataRec(UINT index,CTransaction_contacts data)	 
	{
		m_nIndex = index;
		CString sPhoneNumbers;
		AddItem(new CTextItem((data.getPNR_ORGNR())));
		AddItem(new CTextItem((data.getName())));
		AddItem(new CTextItem((data.getCompany())));
		AddItem(new CTextItem((cleanCRLF(data.getAddress(),L", "))));
		AddItem(new CTextItem((cleanCRLF(data.getPostNum(),L" "))));
		AddItem(new CTextItem((cleanCRLF(data.getPostAddress(),L" "))));
		AddItem(new CTextItem((data.getPhoneWork())));
		AddItem(new CTextItem((data.getPhoneHome())));
		AddItem(new CTextItem((data.getMobile())));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// CSelectContractorDlg dialog

class CSelectContractorDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectContractorDlg)
	CString	m_sLangSet;
	// Setup language filename; 051214 p�d
	CString m_sLangFN;

	CString m_sNoCategory;

	CString m_sTitle;

//	CMyExtStatic m_wndLbl3_1;
	CMyExtStatic m_wndLbl3_2;
	CMyExtStatic m_wndLbl3_3;
	CMyExtStatic m_wndLbl3_4;
	CMyExtStatic m_wndLbl3_5;
	CMyExtStatic m_wndLbl3_6;
	CMyExtStatic m_wndLbl3_7;

	CComboBox m_wndCBox;
	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;

	CMyReportCtrl m_wndReport;
	
	CButton m_wndSearch;
	CButton m_wndClear;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	void setupReport(void);
	vecTransactionContacts m_vecContacts;
	void getContacts(void);
	void populateData(void);

	BOOL m_bIsContractorSelected;
	int m_nID_pk;
	CString m_sPerOrgNum;
	CString m_sContactPers;
	CString m_sCompany;
	CString m_sAddress;
	CString m_sPostNum;
	CString m_sPostAddress;
	CString m_sPhoneWork;
	CString m_sPhoneHome;
	CString m_sMobile;

	CUMLandValueDB *m_pDB;

	vecTransactionCategory m_vecCategories;
	void getCategories(void);
	void addCategoriesToCBox(void);
public:
	CSelectContractorDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectContractorDlg();

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

	void setDialogTitle(LPCTSTR title)
	{
		m_sTitle = title;
	}


	BOOL getIsSelected(void)							{ return m_bIsContractorSelected; }
	int getID_pk(void)										{ return m_nID_pk; }
	CString getPersOrgNum(void)						{ return m_sPerOrgNum; }
	CString getContactPers(void)					{ return m_sContactPers; }
	CString getCompany(void)							{ return m_sCompany; }
	CString getAddress(void)							{ return m_sAddress; }
	CString getPostNum(void)							{ return m_sPostNum; }
	CString getPostAddress(void)					{ return m_sPostAddress; }
	CString getPhoneWork(void)						{ return m_sPhoneWork; }
	CString getPhoneHome(void)						{ return m_sPhoneHome; }
	CString getMobile(void)								{ return m_sMobile; }

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	//{{AFX_VIRTUAL(CSelectContractorDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnSearch();
	afx_msg void OnBnClickedBtnClear();
};
