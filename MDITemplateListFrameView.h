#pragma once


#include "Resource.h"

// CTemplateListFrameView form view

class CTemplateListFrameView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTemplateListFrameView)

	//private:
	BOOL m_bInitialized;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	vecTransactionTemplate m_vecTransactionTemplate;
	void getTemplatesFromDB(void);

	int m_nDBIndex;

	BOOL m_bConnected;
	CUMLandValueDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CMyReportCtrl m_wndTemplates;
//	CXTPReportControl m_wndPricelistsList;

	// Methods
	BOOL setupReport(void);

	BOOL populateData(void);
	
public:
	CTemplateListFrameView();           // protected constructor used by dynamic creation
	virtual ~CTemplateListFrameView();

	enum { IDD = IDD_FORMVIEW4 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void setDBIndex(int idx)
	{
		m_nDBIndex = idx;
	}

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnReportClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


