#pragma once

#include "Resource.h"

#include "ResLangFileReader.h"

#include "MyRichEditCtrl.h"

// CDocumentTmplFormView form view

class CDocumentTmplFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CDocumentTmplFormView)

	//private:
	BOOL m_bInitialized;

	CString m_strFilename;

	RLFReader m_xml;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgSave;
	CString m_sMsgSave2;
	CString m_sMsgRemove;
	CString m_sFileFilter;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CComboBox m_wndCBox1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;

	CStringArray m_sarrDocTypes;

	// Added 2009-08-13 p�d
	CMyRichEditCtrl	m_wndRTF;

	// I have clicked in the window
	bool m_bClick;

	BOOL m_bToolBarEnabled;

	enumACTION m_enumACTION;
	int m_nDBIndex;
  vecTransactionTemplate m_vecTemplate;
	CTransaction_template m_recTemplate;

	void getDocuments(void);
	BOOL saveDocument(short action);
	void removeDocument(void);
	void setNewDocument(void);

	void populateData(void);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	// Added 2009-08-13 P�D
	static DWORD CALLBACK MyStreamOutCallback(DWORD dwCookie,LPBYTE pbBuff, LONG cb, LONG *pcb);

	enum enumAction { TEMPLATE_NONE,
										TEMPLATE_NEW,
										TEMPLATE_OPEN } m_enumAction;

protected:
	CDocumentTmplFormView();           // protected constructor used by dynamic creation
	virtual ~CDocumentTmplFormView();

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL PrintRTF(HWND hwnd, HDC hdc);

	void setEnableWindows(BOOL enable);

public:
	enum { IDD = IDD_FORMVIEW8 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	int getDBIndex(void)	{	return m_nDBIndex; }

	BOOL doSaveDocument(short action)	{ return saveDocument(action); }

	void doPouplate(UINT idx)
	{
		m_nDBIndex = idx;
		populateData();
	}

	void doSetNavigationBar();
protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTBtnOpen();
	afx_msg void OnUpdateTBtnOpen(CCmdUI* pCmdUI);

	afx_msg void OnTBtnPrint();
	afx_msg void OnUpdateTBtnPrint(CCmdUI* pCmdUI);

	afx_msg void OnTBtnFormatFont();
	afx_msg void OnUpdateTBtnFont(CCmdUI* pCmdUI);

	afx_msg void OnTBtnColor();
	afx_msg void OnUpdateTBtnColor(CCmdUI* pCmdUI);

	afx_msg void OnTBtnCut();
	afx_msg void OnUpdateTBtnCut(CCmdUI* pCmdUI);

	afx_msg void OnTBtnCopy();
	afx_msg void OnUpdateTBtnCopy(CCmdUI* pCmdUI);

	afx_msg void OnTBtnPaste();
	afx_msg void OnUpdateTBtnPaste(CCmdUI* pCmdUI);

	afx_msg void OnTBtnUndo();
	afx_msg void OnUpdateTBtnUndo(CCmdUI* pCmdUI);

	afx_msg void OnTBtnRedo();
	afx_msg void OnUpdateTBtnRedo(CCmdUI* pCmdUI);

	afx_msg void OnTBtnBold();
	afx_msg void OnUpdateTBtnBold(CCmdUI* pCmdUI);

	afx_msg void OnTBtnItalic();	
	afx_msg void OnUpdateTBtnItalic(CCmdUI* pCmdUI);

	afx_msg void OnTBtnUnderline();	
	afx_msg void OnUpdateTBtnUnderline(CCmdUI* pCmdUI);

	afx_msg void OnTBtnParaLeft();
	afx_msg void OnUpdateTBtnParaLeft(CCmdUI* pCmdUI);

	afx_msg void OnTBtnParaCenter();
	afx_msg void OnUpdateTBtnParaCenter(CCmdUI* pCmdUI);

	afx_msg void OnTBtnParaRight();
	afx_msg void OnUpdateTBtnParaRight(CCmdUI* pCmdUI);

	afx_msg void OnTBtnParaBullets();
	afx_msg void OnUpdateTBtnParaBullets(CCmdUI* pCmdUI);

	afx_msg void OnCbnSelchangeList81();

};

