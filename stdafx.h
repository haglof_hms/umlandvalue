// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

/*
// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif
*/
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxhtml.h>						// Per tal de poder utilitzar HTML.

#include <SQLAPI.h> // main SQLAPI++ header

#include <vector>
#include <map>

#define _USE_MATH_DEFINES
#include <math.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

//////////////////////////////////////////////////////////////////////
// MY DEFINE FOR ADDING DATA SECUTITY METHODS; 090224 p�d
// UNCOMMENT THIS TO USE IT!
//#define __USE_DATA_SECURITY_METHODS

//////////////////////////////////////////////////////////////////////

// ... in PAD_HMSFuncLib
#include "pad_hms_miscfunc.h"
#include "Elv_object_log.h"	// #3877
#include "coststmplparser.h"	
#include "Calculation_methods.h"
#include "templateparser.h"	
#include "pricelistparser.h"	
#include "pad_xmlshelldatahandler.h"
#include "messagedlg.h"
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"

#include "UMLandValueDB.h"


#define MSG_IN_SUITE				 					(WM_USER + 10)		// This identifer's used to send messages internally
#define ID_SHOWVIEW_MSG								0x9999
#define ID_SHOWVIEW_MSG_UMESTIMATE							0x8116

// Use this root key in my own registry settings; 070628 p�d
const LPCTSTR PROGRAM_NAME						= _T("UMLandValue");				// Used for Languagefile, registry entries etc.; 071107 p�d
const LPCTSTR SHELL_PROGRAM_NAME			= _T("HMSShell");				// Used for Languagefile for Main program (HMSShell.exe); 090119 p�d


// Registry keyes for SplitBar settings; 090122 p�d
const LPCTSTR REG_LANDVALUE_SPLITBAR_ORIENTATION	= _T("SplitBarOrientation");
const LPCTSTR REG_LANDVALUE_PANE_1_SIZE						= _T("SplitBarPane1Size");
const LPCTSTR REG_LANDVALUE_PANE_2_SIZE						= _T("SplitBarPane2Size");
const LPCTSTR REG_LANDVALUE_LAST_OBJECT						= _T("LastObjectID");
const LPCTSTR REG_LANDVALUE_OBJ_IN_DB							= _T("ObjInDB");
const LPCTSTR REG_LANDVALUE_LOG_OBJECT						= _T("LogObject");

// External programs (Modules), used in showFormView
const LPCTSTR UMINDATA_PROGRAM_NAME		= _T("UMInData");				// Used for Languagefile, registry entries etc.; 071107 p�d

// Forrest suite name
const LPCTSTR FORREST_PROGRAM_NAME		= _T("Forrest");				// Used for Languagefile, registry entries etc.; 080925 p�d

#define ID_TABCONTROL_1							0x9001
#define ID_TABCONTROL_2							0x9002
#define ID_TABCONTROL_3							0x9003
#define ID_TABCONTROL_PROP					0x9004
#define ID_TABCONTROL_4							0x9005
#define	ID_TABCONTROL_P30_2018					0x9006

#define ID_REPORT_P30								0x9010
#define ID_REPORT_P30_TEMPL					0x9011
#define ID_REPORT_P30_LIST_TEMPL		0x9012
#define ID_REPORT_HCOST							0x9013
#define ID_REPORT_HCOST_TEMPL				0x9014
#define ID_REPORT_HCOST_LIST_TEMPL	0x9015
#define ID_REPORT_DOC_TEMPLATE			0x9016
#define ID_REPORT_P30_NEW_NORM_TEMPL	0x9017
#define ID_REPORT_P30_NEW_NORM_LIST_TEMPL		0x9018
#define ID_REPORT_P30_NORM_2018_LIST_TEMPL		0x9019


#define ID_REPORT_PROPERTIES				0x9020
#define ID_REPORT_SEARCH_PROPS			0x9021
#define ID_REPORT_LOGBOOK_PROPS			0x9022
#define ID_REPORT_OTHER_COMP				0x9023
#define ID_REPORT_PROP_OWNERS				0x9024
#define ID_REPORT_PROP_STATUS				0x9025

#define ID_INFORMATION_PANE					0x9030
#define ID_SETTINGS_PANE						0x9031

#define ID_EXCH_FUNC_CB							0x9032
#define ID_PRL_FUNC_CB							0x9033
#define ID_COST_TMPL								0x9034
#define ID_FORREST_NORM							0x9035
#define ID_TAKE_CARE_OF_PERCENT			0x9036
#define ID_CORRECTION_FACTOR				0x9037
#define ID_OBJ_LENGTH								0x9038
#define ID_TYPE_OF_NET							0x9041
#define ID_PRICE_DEVIATION					0x9042

#define ID_DATA_DIR									0x9043
#define ID_PRICE_BASE								0x9044
#define ID_MAX_PERCENT							0x9045
#define ID_CURRENT_PERCENT					0x9046
#define ID_VAT											0x9047

#define ID_DCLS											0x9048
#define ID_SODERBERGS								0x9049
#define ID_SIH100_PINE							0x9050

#define ID_SHOW_FIELDCHOOSER				0x9051
#define ID_TBBTN_COLUMNS_DLG				0x9052

#define ID_SHOWVIEW_OBJECT_CONTACTS	0x9021	//Added 2016-06-09 PH

#define ID_INDICATOR_PROG						0x9053

#define IDC_PRINT_PRL								0x9054

#define ID_BTN_FROM_TMPL_RETURN			0x9055

#define ID_TYPEOF_INFR							0x9056	// Added 2009-04-03 P�D

#define ID_P30_PRICE_CB							0x9057

#define ID_TYPEOF_HIGH_COST					0x9058	// Added 2009-04-20 P�D

#define ID_GROWTH_AREA_CB						0x9059

#define ID_LATITUDE									0x9060
#define ID_ALTITUDE									0x9061

#define ID_VFALL_BREAK							0x9062
#define ID_VFALL_FACTOR							0x9063

#define ID_CURRENT_WIDTH						0x9064
#define ID_ADDED_WIDTH							0x9065

#define ID_TBBTN_COLUMNS_DLG_EVALUE	0x9066
#define ID_TBBTN_COLUMNS_DLG_CRUISE	0x9067

#define ID_SHOW_GROUPBOX						0x9068

#define ID_RECALCULATE_BY						0x9069

#define ID_PERCENT_OF_BASE_PRICE				0x9070 // #4420 20150804 J� flyttat ut procent av prisbasbelopp till inst�llningar, frivillig uppg�relseber�kningar

#define ID_PARALLEL_WIDTH						0x9071

#define IDC_SPECIES_REPORT					0x9081

#define ID_TOOLS_ADD_NEW_STAND			0x9098		// Add a manually entered stand; 090907 p�d
#define ID_TOOLS_CALC_NEW_STAND			0x9099		// Calaculate manually entered stand "Storm- och Tork"; 090907 p�d
#define ID_TOOLS_MATCH_PROP					0x9100
#define ID_TOOLS_ADD_NEW_VSTAND			0x9101
#define ID_TOOLS_SAVE_CALC_VSTAND		0x9102
#define ID_TOOLS_REMOVE_SEL_VSTAND	0x9103
#define ID_TOOLS_DO_PROP_GROUPINGS	0x9104
#define ID_TOOLS_SAVE_PROP_GROUPING	0x9105
#define ID_TOOLS_DEL_PROP_GROUPING	0x9106
#define ID_TOOLS_REMOVE_SEL_STAND		0x9107
#define ID_TOOLS_RECALC_KRM3_STANDS	0x9108
#define ID_TOOLS_CHANGE_STATUS			0x9109
#define ID_TOOLS_CALIPER_SETUP_FILE	0x9110
#define ID_TOOLS_SEND_TO_DP				0x9111
#define ID_TOOLS_SEND_TO_EMAIL			0x9112
#define ID_TOOLS_RECIVE_INV_DATA_DP		0x9113
#define ID_TOOLS_CREATE_FROM_INV		0x9114
#define ID_TOOLS_EVAL_FROM_CRUISE		0x9115
#define ID_TOOLS_UPDATE_PRL_TMPL		0x9116
#define ID_TOOLS_UPDATE_COST_TMPL		0x9117
#define ID_TOOLS_ALL_EVAL_CRUISE		0x9118	// Create "v�rderingsbest�nd", for all properties in an Object; 081022 p�d
#define ID_TOOLS_CHANGE_SPLITBAR_ORIENTATION	0x9119
//#define ID_TOOLS_OPEN_SEL_STAND			0x9120
#define ID_TOOLS_PRICELIST_INFO			0x9121
#define ID_TOOLS_ADD_PROPERTY       0x9122
#define ID_TOOLS_REMOVE_PROPERTY    0x9123
#define ID_TOOLS_MATCH_ALL_PROP			0x9124
#define ID_TOOLS_CHANGE_TEMPLATE		0x9125
#define ID_TOOLS_GOTO_GIS						0x9126
#define ID_TOOLS_DO_PROP_SORT_ORDER	0x9127
#define ID_TOOLS_UPD_P30						0x9128
#define ID_TOOLS_UPD_HIGHER_COSTS		0x9129
#define ID_TOOLS_ADD_OTHER_COSTS		0x9130
#define ID_TOOLS_REMOVE_PROPERTY2   0x9131
#define ID_TOOLS_DO_PROP_GROUPINGS2	0x9132
#define ID_TOOLS_EXPORT_PROPERTIES  0x9133
#define ID_TOOLS_PROPERTY_OWNER_LIST	0x9134
#define ID_TOOLS_LOG_BOOK_SETTINGS  0x9135
#define ID_TOOLS_RECIVE_INV_DATA_DPII	0x9136
#define ID_TOOLS_SEND_TO_DPII		0x9137
#define ID_TOOLS_ADD_PROPERTY_FROM_FILE	0x9138

#define ID_TOOLS_OPEN_SEL_STAND_PRICELIST	0x9139	// #4554
#define ID_TOOLS_OPEN_SEL_STAND_COST_PRICELIST	0x9240	// #4554
#define ID_TOOLS_GENERATE_FROM_EXCEL     0x9241

#define ID_TOOLS_OPEN_SEL_STAND			0x9242
#define ID_TOOLS_NEW_STAND				0x9243
#define ID_TOOLS_SHOW_PRICELIST			0x9245
#define ID_TOOLS_SHOW_COST_TEMPLATE		0x9246


const LPCTSTR FILENAME_EXC_ERS_ERBJ		= _T("ErsErbj.xlsx"); //HMS-30 20200608 J�
const LPCTSTR FILENAME_EXC_ERS_ERBJ_XLTX = _T("ErsErbj.xlsx"); //#HMS-98 Sparat om den som xls ist�llet 20230213
const LPCTSTR FILENAME_EXC_VP		 	= _T("Vp.xlsx");	//HMS-30 20200608 J�
const LPCTSTR DIRECTORYNAME_EXC_REPORTS =_T("Reports");	//HMS-30 20200608 J�

const LPCTSTR FILENAME_EXC_ELLEVIO		 	= _T("Ellevio.xlsm");	//HMS-30 20200608 J�

#define ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ERBJ 0x9247	//HMS-30 20200608 J�
#define ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_VP   0x9248	//HMS-30 20200608 J�


#define ID_TOOLS_EXPORT_PROPERTIES_XML   0x9249	//HMS-76 20220125 J�
#define ID_TOOLS_EXPORT_PROPERTIES_JSON  0x9250	//HMS-76 20220125 J�

#define ID_TOOLS_EXPORT_PROPERTIES_EXCELRAPPORT_ELLEVIO  0x9251	//HMS-135 20250123 J�


//#3385
#define ID_TOOLS_SET_OBJ_FINISHED	0x9142
#define ID_TOOLS_SET_OBJ_ONGOING	0x9143
#define ID_TOOLS_SHOW_OBJ_ONGOING	0x9144
#define ID_TOOLS_SHOW_OBJ_FINISHED	0x9145
#define ID_TOOLS_SHOW_OBJ_ALL		0x9146

#define ID_IS_VAT										0x9200
#define ID_ID_VOLUNTARY_DEAL				0x9201
#define ID_ID_HIGHER_COSTS					0x9202
#define ID_ID_OTHER_COMP						0x9203
#define ID_IS_GROT									0x9204

#define ID_MATCHING_STANDS					0x9210
#define ID_KR_PER_M3								0x9211
#define ID_CHANGE_STATUS						0x9212
#define ID_SETUP_FILE								0x9213
#define ID_SEARCH								0x9214

#define ID_LANDOWNERS_LB						0x9220
#define ID_LANDOWNERS_REP						0x9221

#define ID_MAPSPECIESGRID						0x9230
#define ID_MAPTREETYPESGRID						0x9231

#define IDS_STRING10								10

#define IDS_STRING100								100
#define IDS_STRING101								101
#define IDS_STRING102								102
#define IDS_STRING103								103
#define IDS_STRING1040							1040
#define IDS_STRING1041							1041
#define IDS_STRING1042							1042
#define IDS_STRING1043							1043
#define IDS_STRING1044							1044
#define IDS_STRING1045							1045
#define IDS_STRING1046							1046
#define IDS_STRING1047							1047
#define IDS_STRING1048							1048
#define IDS_STRING1049							1049
#define IDS_STRING105								105
#define IDS_STRING1051							1051
#define IDS_STRING106								106
#define IDS_STRING107								107
#define IDS_STRING110								110
#define IDS_STRING111								111
#define IDS_STRING112								112
#define IDS_STRING113								113

#define IDS_STRING1000							1000
#define IDS_STRING1001							1001

#define IDS_STRING114								114
#define IDS_STRING1140							1140
#define IDS_STRING1141							1141
#define IDS_STRING1142							1142
#define IDS_STRING1143							1143
#define IDS_STRING1144							1144
#define IDS_STRING1145							1145

#define IDS_STRING1149							1149
#define IDS_STRING11490							11490
#define IDS_STRING11491							11491
#define IDS_STRING1150							1150
#define IDS_STRING1151							1151

#define IDS_STRING116								116
#define IDS_STRING1160							1160
#define IDS_STRING1161							1161
#define IDS_STRING1162							1162
#define IDS_STRING1163							1163
#define IDS_STRING1164							1164
#define IDS_STRING1165							1165
#define IDS_STRING1166							1166

#define IDS_STRING1170							1170
#define IDS_STRING1171							1171

#define IDS_STRING1200							1200
#define IDS_STRING1201							1201
#define IDS_STRING1202							1202

#define IDS_STRING1300							1300
#define IDS_STRING1301							1301
#define IDS_STRING1302							1302
#define IDS_STRING1303							1303
#define IDS_STRING1304							1304
#define IDS_STRING1305							1305
#define IDS_STRING1306							1306
#define IDS_STRING1307							1307

#define IDS_STRING131								131

#define IDS_STRING1550							1550	// 090309 p�d
#define IDS_STRING1551							1551
#define IDS_STRING1552							1552	
#define IDS_STRING1553							1553
#define IDS_STRING1554							1554
#define IDS_STRING1555							1555
#define IDS_STRING1556							1556
#define IDS_STRING1557							1557
#define IDS_STRING1558							1558
#define IDS_STRING1559							1559
#define IDS_STRING1560							1560
#define IDS_STRING1561							1561	// 090309 p�d

#define IDS_STRING1902							1902	// 090309 p�d

#define IDS_STRING200								200
#define IDS_STRING201								201
#define IDS_STRING2010							2010
#define IDS_STRING2011							2011
#define IDS_STRING202								202
#define IDS_STRING2020							2020
#define IDS_STRING2021							2021
#define IDS_STRING2022							2022
#define IDS_STRING203								203
#define IDS_STRING204								204
#define IDS_STRING205								205
#define IDS_STRING206								206
#define IDS_STRING207								207
#define IDS_STRING208								208
#define IDS_STRING2080							2080
#define IDS_STRING2081							2081
#define IDS_STRING2082							2082
#define IDS_STRING2083							2083
#define IDS_STRING209								209
#define IDS_STRING2090							2090
#define IDS_STRING2091							2091
#define IDS_STRING210								210
#define IDS_STRING211								211
#define IDS_STRING2110							2110
#define IDS_STRING212								212
#define IDS_STRING2120							2120
#define IDS_STRING2121							2121
#define IDS_STRING2122							2122
#define IDS_STRING2123							2123
#define IDS_STRING213								213
#define IDS_STRING214								214
#define IDS_STRING215								215
#define IDS_STRING216								216
#define IDS_STRING217								217
#define IDS_STRING218								218
#define IDS_STRING2180							2180
#define IDS_STRING2181							2181
#define IDS_STRING219								219
#define IDS_STRING2190							2190
#define IDS_STRING2191							2191
#define IDS_STRING2192							2192
#define IDS_STRING2193							2193
#define IDS_STRING2194							2194
#define IDS_STRING220								220
#define IDS_STRING2200							2200
#define IDS_STRING2201							2201
#define IDS_STRING2202							2202
#define IDS_STRING2203							2203
#define IDS_STRING2204							2204
#define IDS_STRING2205							2205
#define IDS_STRING2206							2206
#define IDS_STRING2207							2207
#define IDS_STRING2208							2208
#define IDS_STRING2209							2209
#define IDS_STRING221								221
#define IDS_STRING2210							2210
#define IDS_STRING2211							2211
#define IDS_STRING2212							2212
#define IDS_STRING222								222
#define IDS_STRING2221								2221
#define IDS_STRING2222								2222
#define IDS_STRING223								223
#define IDS_STRING224								224
#define IDS_STRING2240							2240
#define IDS_STRING2241							2241
#define IDS_STRING2242							2242
#define IDS_STRING225								225
#define IDS_STRING2250							2250
#define IDS_STRING2251							2251
#define IDS_STRING2259							2259
#define IDS_STRING2260							2260
#define IDS_STRING22601							22601
#define IDS_STRING2261							2261
#define IDS_STRING2262							2262
#define IDS_STRING2263							2263
#define IDS_STRING2264							2264
#define IDS_STRING22644							22644
#define IDS_STRING22645							22645
#define IDS_STRING22646							22646
#define IDS_STRING22647							22647
#define IDS_STRING226470						226470
#define IDS_STRING226471						226471
#define IDS_STRING226472						226472
#define IDS_STRING226473						226473
#define IDS_STRING226474						226474
#define IDS_STRING226475						226475
#define IDS_STRING22800							22800
#define IDS_STRING2265							2265
#define IDS_STRING2266							2266
#define IDS_STRING2267							2267
#define IDS_STRING2268							2268
#define IDS_STRING2269							2269
#define IDS_STRING22690							22690
#define IDS_STRING22691							22691
#define IDS_STRING22692							22692
#define IDS_STRING22693							22693
#define IDS_STRING22694							22694
#define IDS_STRING22695							22695
#define IDS_STRING22696							22696
#define IDS_STRING22697							22697
#define IDS_STRING22698							22698
#define IDS_STRING22699							22699
#define IDS_STRING22700							22700
#define IDS_STRING22701							22701
#define IDS_STRING22702							22702
#define IDS_STRING22703							22703
#define IDS_STRING22704							22704
#define IDS_STRING22705							22705
#define IDS_STRING22706							22706
#define IDS_STRING22640							22640
#define IDS_STRING22641							22641
#define IDS_STRING22642							22642
#define IDS_STRING22643							22643
#define IDS_STRING33111							33111 //Mark�gare
#define IDS_STRING33112							33112 //Visa mark�gare
#define IDS_STRING33113							33113 //Ta bort
#define IDS_STRING33114							33114 //L�gg till
#define IDS_STRING22648							22648
#define IDS_STRING22649							22649
#define IDS_STRING22650							22650
#define IDS_STRING22651							22651
#define IDS_STRING22652							22652
#define IDS_STRING22653							22653
#define IDS_STRING22654							22654
#define IDS_STRING226540						226540
#define IDS_STRING226541						226541
#define IDS_STRING22655							22655
#define IDS_STRING22656							22656
#define IDS_STRING22657							22657
#define IDS_STRING22658							22658
#define IDS_STRING22659							22659
#define IDS_STRING22660							22660
#define IDS_STRING22661							22661
#define IDS_STRING22662							22662
#define IDS_STRING22663							22663
#define IDS_STRING22664							22664
#define IDS_STRING22665							22665
#define IDS_STRING22666							22666
#define IDS_STRING22667							22667
#define IDS_STRING22668							22668
#define IDS_STRING22669							22669
#define IDS_STRING22670							22670
#define IDS_STRING22671							22671
#define IDS_STRING22672							22672
#define IDS_STRING22673							22673
#define IDS_STRING22674							22674
#define IDS_STRING22675							22675
#define IDS_STRING22676							22676
#define IDS_STRING22677							22677
#define IDS_STRING22678							22678
#define IDS_STRING22679							22679
#define IDS_STRING22680							22680
#define IDS_STRING22681							22681
#define IDS_STRING22682							22682
#define IDS_STRING22683							22683
#define IDS_STRING22684							22684
#define IDS_STRING22685							22685
#define IDS_STRING22686							22686
#define IDS_STRING22687							22687
#define IDS_STRING22688							22688
#define IDS_STRING22689							22689
#define IDS_STRING22690							22690
#define IDS_STRING22691							22691
#define IDS_STRING22692							22692
#define IDS_STRING22693							22693
#define IDS_STRING22694							22694

#define IDS_STRING22707	22707
#define IDS_STRING22708	22708

#define IDS_STRING2270							2270
#define IDS_STRING2271							2271
#define IDS_STRING2272							2272

#define IDS_STRING2280							2280
#define IDS_STRING2281							2281
#define IDS_STRING2282							2282
#define IDS_STRING2283							2283
#define IDS_STRING2284							2284
#define IDS_STRING2285							2285
#define IDS_STRING2286							2286
#define IDS_STRING2287							2287
#define IDS_STRING2288							2288
#define IDS_STRING2289							2289
#define IDS_STRING229							229
#define IDS_STRING2290							2290
#define IDS_STRING2291							2291
#define IDS_STRING2292							2292
#define IDS_STRING2293							2293
#define IDS_STRING2294							2294
#define IDS_STRING2295							2295
#define IDS_STRING2296							2296
#define IDS_STRING2297							2297
#define IDS_STRING2298							2298
#define IDS_STRING2299							2299
#define IDS_STRING22991							22991
#define IDS_STRING2300							2300
#define IDS_STRING2301							2301
#define IDS_STRING2302							2302
#define IDS_STRING2303							2303
#define IDS_STRING2304							2304
#define IDS_STRING2310							2310
#define IDS_STRING2311							2311
#define IDS_STRING2312							2312
#define IDS_STRING2313							2313
#define IDS_STRING2314							2314
#define IDS_STRING2315							2315
#define IDS_STRING2316							2316
#define IDS_STRING2317							2317
#define IDS_STRING2318							2318
#define IDS_STRING2319							2319
#define IDS_STRING2320							2320
#define IDS_STRING2321							2321
#define IDS_STRING2322							2322
#define IDS_STRING2323							2323
#define IDS_STRING2324							2324
#define IDS_STRING2330							2330
#define IDS_STRING2331							2331
#define IDS_STRING2332							2332

#define IDS_STRING236								236
#define IDS_STRING237								237
#define IDS_STRING238								238
#define IDS_STRING239								239
#define IDS_STRING240								240
#define IDS_STRING241								241
#define IDS_STRING242								242
#define IDS_STRING243								243

#define IDS_STRING2400							2400
#define IDS_STRING2401							2401
#define IDS_STRING2402							2402
#define IDS_STRING2403							2403
#define IDS_STRING2404							2404
#define IDS_STRING2405							2405
#define IDS_STRING2406							2406
#define IDS_STRING2407							2407
#define IDS_STRING2408							2408
#define IDS_STRING2409							2409
#define IDS_STRING2410							2410
#define IDS_STRING2411							2411
#define IDS_STRING2412							2412
#define IDS_STRING2413							2413
#define IDS_STRING2414							2414
#define IDS_STRING2415							2415
#define IDS_STRING2416							2416
#define IDS_STRING2417							2417
#define IDS_STRING2418							2418
#define IDS_STRING2419							2419
#define IDS_STRING2420							2420
#define IDS_STRING2421							2421
#define IDS_STRING2422							2422
#define IDS_STRING2423							2423

#define IDS_STRING253	253
#define IDS_STRING254	254
#define IDS_STRING255	255
#define IDS_STRING256	256
#define IDS_STRING257	257
#define IDS_STRING258	258
#define IDS_STRING259	259
#define IDS_STRING260	260
#define IDS_STRING261	261


#define IDS_STRING2500							2500
#define IDS_STRING2501							2501
#define IDS_STRING2502							2502
#define IDS_STRING2503							2503
#define IDS_STRING2504							2504
#define IDS_STRING2505							2505
#define IDS_STRING2506							2506
#define IDS_STRING2507							2507
#define IDS_STRING2508							2508
#define IDS_STRING2509							2509
#define IDS_STRING2510							2510
#define IDS_STRING2511							2511
#define IDS_STRING2512							2512
#define IDS_STRING2513							2513
#define IDS_STRING2514							2514
#define IDS_STRING2515							2515
#define IDS_STRING2516							2516
#define IDS_STRING2517							2517
#define IDS_STRING2518							2518
#define IDS_STRING2519							2519
#define IDS_STRING2520							2520
#define IDS_STRING2521							2521
#define IDS_STRING2522							2522
#define IDS_STRING2523							2523
#define IDS_STRING2524							2524
#define IDS_STRING2525							2525
#define IDS_STRING2526							2526
#define IDS_STRING2527							2527
#define IDS_STRING2528							2528

#define IDS_STRING2600							2600
#define IDS_STRING2601							2601
#define IDS_STRING2602							2602
#define IDS_STRING2603							2603
#define IDS_STRING2604							2604
#define IDS_STRING2605							2605

#define IDS_STRING2610							2610
#define IDS_STRING2611							2611
#define IDS_STRING2612							2612
#define IDS_STRING2613							2613
#define IDS_STRING2614							2614

#define IDS_STRING2620							2620
#define IDS_STRING2621							2621
#define IDS_STRING2622							2622

#define IDS_STRING2700							2700
#define IDS_STRING2701							2701
#define IDS_STRING2702							2702
#define IDS_STRING2703							2703
#define IDS_STRING2704							2704
#define IDS_STRING2705							2705
#define IDS_STRING2706							2706
#define IDS_STRING2707							2707
#define IDS_STRING2708							2708
#define IDS_STRING2709							2709
#define IDS_STRING2710							2710
#define IDS_STRING2711							2711
#define IDS_STRING2712							2712
#define IDS_STRING2713							2713
#define IDS_STRING2714							2714

#define IDS_STRING280								280
#define IDS_STRING281								281
#define IDS_STRING2810							2810
#define IDS_STRING2811							2811
#define IDS_STRING2812							2812
#define IDS_STRING2813							2813
#define IDS_STRING2814							2814

#define IDS_STRING2800							2800
#define IDS_STRING2801							2801
#define IDS_STRING2802							2802
#define IDS_STRING2803							2803
#define IDS_STRING2804							2804

#define IDS_STRING290								290
#define IDS_STRING291								291
#define IDS_STRING292								292
#define IDS_STRING293								293
#define IDS_STRING294								294
#define IDS_STRING295								295
#define IDS_STRING296								296

#define IDS_STRING2900							2900
#define IDS_STRING2901							2901
#define IDS_STRING2902							2902	// OBS! Not defined yet; 080409 p�d
#define IDS_STRING2903							2903
#define IDS_STRING2904							2904
#define IDS_STRING2905							2905

#define IDS_STRING3000							3000
#define IDS_STRING3001							3001
#define IDS_STRING3002							3002
#define IDS_STRING3003							3003
#define IDS_STRING3004							3004
#define IDS_STRING3005							3005
#define IDS_STRING3006							3006
#define IDS_STRING3007							3007
#define IDS_STRING3008							3008
#define IDS_STRING3009							3009
#define IDS_STRING3010							3010
#define IDS_STRING3011							3011
#define IDS_STRING3012							3012
#define IDS_STRING3013							3013
#define IDS_STRING3014							3014
#define IDS_STRING3015							3015
#define IDS_STRING3016							3016
#define IDS_STRING3017							3017
#define IDS_STRING3018							3018
#define IDS_STRING3019							3019
#define IDS_STRING3020							3020
#define IDS_STRING3021							3021
#define IDS_STRING3022							3022
#define IDS_STRING3023							3023
#define IDS_STRING3024							3024
#define IDS_STRING3025							3025

#define IDS_STRING3030							3030
#define IDS_STRING3031							3031
#define IDS_STRING3032							3032
#define IDS_STRING3033							3033
#define IDS_STRING3034							3034
#define IDS_STRING3035							3035
#define IDS_STRING3036							3036
#define IDS_STRING3037							3037
#define IDS_STRING3038							3038
#define IDS_STRING3039							3039

#define IDS_STRING3080							3080

#define IDS_STRING3100							3100
#define IDS_STRING3101							3101
#define IDS_STRING3102							3102
#define IDS_STRING3103							3103
#define IDS_STRING3104							3104
#define IDS_STRING3105							3105
#define IDS_STRING3106							3106
#define IDS_STRING3107							3107

#define IDS_STRING3200							3200
#define IDS_STRING3201							3201
#define IDS_STRING3202							3202
#define IDS_STRING3203							3203
#define IDS_STRING3204							3204
#define IDS_STRING3205							3205
#define IDS_STRING3206							3206
#define IDS_STRING3207							3207
#define IDS_STRING3208							3208
#define IDS_STRING3209							3209
#define IDS_STRING3210							3210
#define IDS_STRING3211							3211
#define IDS_STRING3212							3212

#define IDS_STRING3220							3220
#define IDS_STRING3221							3221

#define IDS_STRING3250							3250
#define IDS_STRING3251							3251
#define IDS_STRING3252							3252
#define IDS_STRING3253							3253
#define IDS_STRING3254							3254
#define IDS_STRING3255							3255
#define IDS_STRING3256							3256
#define IDS_STRING3257							3257
#define IDS_STRING3258							3258 // 090302 p�d

#define IDS_STRING3261							3261
#define IDS_STRING3262							3262
#define IDS_STRING3263							3263
#define IDS_STRING3264							3264
#define IDS_STRING3265							3265
#define IDS_STRING3266							3266
#define IDS_STRING3267							3267
#define IDS_STRING3268							3268
#define IDS_STRING3269							3269
#define IDS_STRING3270							3270
#define IDS_STRING3271							3271
#define IDS_STRING3272							3272
#define IDS_STRING3273							3273
#define IDS_STRING3274							3274
#define IDS_STRING3275							3275
#define IDS_STRING3276							3276
#define IDS_STRING3277							3277
#define IDS_STRING3278							3278

#define IDS_STRING3280							3280
#define IDS_STRING3281							3281
#define IDS_STRING3282							3282
#define IDS_STRING3283							3283
#define IDS_STRING3284							3284
#define IDS_STRING3285							3285
#define IDS_STRING3286							3286
#define IDS_STRING3287							3287
#define IDS_STRING3288							3288
#define IDS_STRING3289							3289
#define IDS_STRING3290							3290
#define IDS_STRING3291							3291
#define IDS_STRING3292							3292
#define IDS_STRING3293							3293
#define IDS_STRING3294							3294
#define IDS_STRING3295							3295
#define IDS_STRING3296							3296
#define IDS_STRING3297							3297
#define IDS_STRING3298							3298
#define IDS_STRING3299							3299
#define IDS_STRING3300							3300
#define IDS_STRING3301							3301
#define IDS_STRING3302							3302
#define IDS_STRING3303							3303
#define IDS_STRING3304							3304
#define IDS_STRING3305							3305
#define IDS_STRING3306							3306
#define IDS_STRING3307							3307
#define IDS_STRING3308							3308
#define IDS_STRING3309							3309
#define IDS_STRING3310							3310

#define IDS_STRING340							340
#define IDS_STRING341							341

#define IDS_STRING3443							3443
#define IDS_STRING3444							3444
#define IDS_STRING3445							3445
#define IDS_STRING3446							3446
#define IDS_STRING3447							3447
#define IDS_STRING3448							3448

#define IDS_STRING3500							3500
#define IDS_STRING3501							3501
#define IDS_STRING3502							3502
#define IDS_STRING3503							3503

#define IDS_STRING4000							4000
#define IDS_STRING4001							4001
#define IDS_STRING4002							4002
#define IDS_STRING4003							4003
#define IDS_STRING4004							4004
#define IDS_STRING4005							4005
#define IDS_STRING4006							4006
#define IDS_STRING4007							4007
#define IDS_STRING4008							4008
#define IDS_STRING4009							4009
#define IDS_STRING4010							4010
#define IDS_STRING4011							4011
#define IDS_STRING4012							4012
#define IDS_STRING40120							40120
#define IDS_STRING4013							4013
#define IDS_STRING4014							4014
#define IDS_STRING4015							4015
#define IDS_STRING4016							4016
#define IDS_STRING4017							4017
#define IDS_STRING4018							4018
#define IDS_STRING4019							4019
#define IDS_STRING4020							4020
#define IDS_STRING4021							4021
#define IDS_STRING4022							4022

#define IDS_STRING4050							4050
#define IDS_STRING4051							4051
#define IDS_STRING4052							4052
#define IDS_STRING4053							4053
#define IDS_STRING4054							4054
#define IDS_STRING40540							40540
#define IDS_STRING4055							4055
#define IDS_STRING4056							4056
#define IDS_STRING4057							4057
#define IDS_STRING4058							4058
#define IDS_STRING4059							4059
#define IDS_STRING4060							4060
#define IDS_STRING4061							4061
#define IDS_STRING4062							4062
#define IDS_STRING4063							4063
#define IDS_STRING4064							4064
#define IDS_STRING4065							4065

#define IDS_STRING40700							40700
#define IDS_STRING40701							40701
#define IDS_STRING40702							40702
#define IDS_STRING40703							40703
#define IDS_STRING40704							40704
#define IDS_STRING40705							40705
#define IDS_STRING40706							40706
#define IDS_STRING40707							40707
#define IDS_STRING40708							40708
#define IDS_STRING40709							40709
#define IDS_STRING40710							40710

#define IDS_STRING4070							4070
#define IDS_STRING4071							4071
#define IDS_STRING4072							4072
#define IDS_STRING4073							4073
#define IDS_STRING4074							4074
#define IDS_STRING4075							4075
#define IDS_STRING4076							4076

#define IDS_STRING4080							4080
#define IDS_STRING4081							4081

#define IDS_STRING410								410
#define IDS_STRING411								411
#define IDS_STRING412								412
#define IDS_STRING4120							4120
#define IDS_STRING4121							4121
#define IDS_STRING4122							4122
#define IDS_STRING4100							4100
#define IDS_STRING4101							4101
#define IDS_STRING4102							4102
#define IDS_STRING4103							4103
#define IDS_STRING4104							4104
#define IDS_STRING4105							4105
#define IDS_STRING4106							4106
#define IDS_STRING4107							4107
#define IDS_STRING4108							4108
#define IDS_STRING4109							4109
#define IDS_STRING4110							4110
#define IDS_STRING4111							4111
#define IDS_STRING4112							4112
#define IDS_STRING4113							4113
#define IDS_STRING4114							4114
#define IDS_STRING4116							4116
#define IDS_STRING4154							4154

#define IDS_STRING4114							4114
#define IDS_STRING4115							4115

#define IDS_STRING4140							4140
#define IDS_STRING4141							4141

#define IDS_STRING4150							4150
#define IDS_STRING4151							4151
#define IDS_STRING4152							4152
#define IDS_STRING4153							4153

#define IDS_STRING4160							4160
#define IDS_STRING4161							4161


#define IDS_STRING420								420
#define IDS_STRING421								421
#define IDS_STRING422								422
#define IDS_STRING4200							4200
#define IDS_STRING4201							4201
#define IDS_STRING4202							4202
#define IDS_STRING4203							4203
#define IDS_STRING4204							4204
#define IDS_STRING4205							4205

#define IDS_STRING42050							42050
#define IDS_STRING42051							42051
#define IDS_STRING42052							42052
#define IDS_STRING42053							42053
#define IDS_STRING42054							42054

#define IDS_STRING423								423
#define IDS_STRING4230							4230
#define IDS_STRING4231							4231
#define IDS_STRING4232							4232
#define IDS_STRING4233							4233
#define IDS_STRING42330							42330
#define IDS_STRING42331							42331
#define IDS_STRING42332							42332
#define IDS_STRING42333							42333
#define IDS_STRING42334							42334
#define IDS_STRING42335							42335
#define IDS_STRING42336							42336
#define IDS_STRING4234							4234

#define IDS_STRING424								424
#define IDS_STRING4240							4240
#define IDS_STRING4241							4241
#define IDS_STRING4242							4242
#define IDS_STRING4243							4243
#define IDS_STRING4244							4244
#define IDS_STRING4245							4245
#define IDS_STRING4246							4246
#define IDS_STRING4247							4247
#define IDS_STRING4248							4248
#define IDS_STRING4249							4249

#define IDS_STRING4250							4250
#define IDS_STRING4251							4251

#define IDS_STRING4300							4300
#define IDS_STRING4301							4301
#define IDS_STRING4302							4302
#define IDS_STRING4303							4303
#define IDS_STRING4304							4304
#define IDS_STRING4305							4305
#define IDS_STRING4306							4306
#define IDS_STRING4307							4307
#define IDS_STRING4308							4308
#define IDS_STRING4309							4309
#define IDS_STRING4310							4310
#define IDS_STRING4311							4311
#define IDS_STRING4400							4400
#define IDS_STRING4401							4401
#define IDS_STRING4402							4402

#define IDS_STRING4500							4500
#define IDS_STRING4501							4501
#define IDS_STRING4502							4502
#define IDS_STRING4510							4510
#define IDS_STRING4511							4511
#define IDS_STRING4512							4512
#define IDS_STRING4513							4513
#define IDS_STRING4514							4514
#define IDS_STRING4515							4515
#define IDS_STRING4516							4516

// Forrestnorm error log text; 080509 p�d
#define IDS_STRING5000							5000
#define IDS_STRING5001							5001
#define IDS_STRING5002							5002
#define IDS_STRING5101							5101
#define IDS_STRING5102							5102
#define IDS_STRING5103							5103
#define IDS_STRING5104							5104
#define IDS_STRING5105							5105
#define IDS_STRING5106							5106
#define IDS_STRING5107							5107

// Show infr. information; 080519 p�d
#define IDS_STRING5210							5210
#define IDS_STRING5211							5211
#define IDS_STRING5212							5212
#define IDS_STRING52130							52130
#define IDS_STRING52131							52131
#define IDS_STRING52132							52132
#define IDS_STRING52133							52133
#define IDS_STRING52134							52134
#define IDS_STRING52135							52135
#define IDS_STRING52136							52136
#define IDS_STRING5214							5214
#define IDS_STRING52140							52140
#define IDS_STRING52141							52141
#define IDS_STRING52142							52142
#define IDS_STRING52143							52143
#define IDS_STRING52144							52144
#define IDS_STRING52145							52145
#define IDS_STRING52146							52146
#define IDS_STRING52147							52147
#define IDS_STRING5215							5215
#define IDS_STRING52150							52150
#define IDS_STRING52151							52151
#define IDS_STRING52152							52152
#define IDS_STRING52153							52153
#define IDS_STRING52154							52154
#define IDS_STRING52155							52155
#define IDS_STRING5216							5216
#define IDS_STRING52160							52160
#define IDS_STRING52161							52161

#define IDS_STRING52170							52170
#define IDS_STRING52171							52171
#define IDS_STRING52172							52172
#define IDS_STRING52173							52173
#define IDS_STRING52174							52174

#define IDS_STRING52180							52180
#define IDS_STRING52181							52181
#define IDS_STRING52182							52182
#define IDS_STRING52183							52183
#define IDS_STRING52184							52184
#define IDS_STRING52185							52185

#define IDS_STRING52186							52186
#define IDS_STRING52187							52187
#define IDS_STRING52188							52188
#define IDS_STRING52189							52189
#define IDS_STRING52190							52190

#define IDS_STRING5300							5300
#define IDS_STRING53000							53000
#define IDS_STRING53001							53001
#define IDS_STRING53002							53002
#define IDS_STRING53003							53003
#define IDS_STRING53004							53004
#define IDS_STRING53005							53005
#define IDS_STRING53006							53006
#define IDS_STRING53007							53007
#define IDS_STRING53008							53008
#define IDS_STRING53009							53009

#define IDS_STRING53010							53010
#define IDS_STRING53011							53011
#define IDS_STRING53012							53012
#define IDS_STRING53013							53013
#define IDS_STRING53014							53014
#define IDS_STRING53015							53015
#define IDS_STRING53016							53016
#define IDS_STRING53017							53017
#define IDS_STRING53018							53018
#define IDS_STRING53019							53019

#define IDS_STRING53020							53020
#define IDS_STRING53021							53021
#define IDS_STRING53022							53022
#define IDS_STRING53023							53023
#define IDS_STRING53024							53024
#define IDS_STRING53025							53025

#define IDS_STRING53030							53030
#define IDS_STRING53031							53031
#define IDS_STRING53032							53032
#define IDS_STRING53033							53033

#define IDS_STRING53040							53040
#define IDS_STRING53041							53041
#define IDS_STRING53042							53042
#define IDS_STRING53043							53043
#define IDS_STRING53044							53044

#define IDS_STRING5400							5400
#define IDS_STRING5401							5401
#define IDS_STRING5402							5402
#define IDS_STRING5403							5403
#define IDS_STRING5404							5404
#define IDS_STRING5405							5405
#define IDS_STRING5410							5410
#define IDS_STRING54100							54100
#define IDS_STRING54101							54101
#define IDS_STRING54102							54102
#define IDS_STRING54103							54103
#define IDS_STRING54104							54104
#define IDS_STRING54105							54105
#define IDS_STRING54106							54106
#define IDS_STRING54107							54107

#define IDS_STRING5411							5411
#define IDS_STRING54110							54110
#define IDS_STRING54111							54111
#define IDS_STRING54112							54112
#define IDS_STRING54113							54113
#define IDS_STRING54114							54114
#define IDS_STRING54115							54115
#define IDS_STRING54116							54116
#define IDS_STRING54117							54117
#define IDS_STRING54118							54118
#define IDS_STRING54119							54119
#define IDS_STRING54120							54120
#define IDS_STRING54121							54121
#define IDS_STRING54122							54122
#define IDS_STRING54123							54123
#define IDS_STRING54124							54124
#define IDS_STRING54125							54125
#define IDS_STRING54126							54126
#define IDS_STRING54127							54127
#define IDS_STRING54128							54128
#define IDS_STRING54129							54129
#define IDS_STRING54130							54130
#define IDS_STRING54131							54131
#define IDS_STRING54132							54132
#define IDS_STRING54133							54133

#define IDS_STRING5420							5420
#define IDS_STRING54200							54200
#define IDS_STRING54201							54201
#define IDS_STRING54202							54202
#define IDS_STRING54203							54203
#define IDS_STRING54204							54204
#define IDS_STRING54205							54205
#define IDS_STRING54206							54206
#define IDS_STRING54207							54207
#define IDS_STRING54208							54208
#define IDS_STRING54209							54209
#define IDS_STRING54210							54210
#define IDS_STRING54211							54211
#define IDS_STRING54212							54212
#define IDS_STRING54213							54213
#define IDS_STRING54214							54214
#define IDS_STRING54215							54215
#define IDS_STRING54216							54216
#define IDS_STRING54217							54217
#define IDS_STRING54218							54218
#define IDS_STRING54219							54219
#define IDS_STRING54220							54220

#define IDS_STRING5500							5500
#define IDS_STRING5501							5501
#define IDS_STRING5502							5502
#define IDS_STRING5503							5503
#define IDS_STRING5504							5504

#define IDS_STRING5505							5505
#define IDS_STRING5506							5506
#define IDS_STRING5507							5507
#define IDS_STRING5508							5508
#define IDS_STRING5509							5509

#define IDS_STRING5600							5600
#define IDS_STRING5601							5601
#define IDS_STRING5602							5602
#define IDS_STRING5603							5603
#define IDS_STRING5604							5604
#define IDS_STRING56050							56050
#define IDS_STRING56051							56051
#define IDS_STRING56052							56052
#define IDS_STRING56053							56053
#define IDS_STRING5606							5606
#define IDS_STRING5607							5607
#define IDS_STRING5608							5608
#define IDS_STRING5609							5609
#define IDS_STRING5610							5610
#define IDS_STRING5611							5611
#define IDS_STRING5612							5612

#define IDS_STRING5700							5700
#define IDS_STRING5701							5701
#define IDS_STRING5702							5702
#define IDS_STRING5703							5703
#define IDS_STRING5704							5704
#define IDS_STRING5705							5705
#define IDS_STRING5706							5706

#define IDS_STRING5800							5800
#define IDS_STRING5801							5801
#define IDS_STRING5802							5802
#define IDS_STRING5803							5803
#define IDS_STRING5804							5804
#define IDS_STRING5805							5805
#define IDS_STRING5806							5806
#define IDS_STRING5807							5807
#define IDS_STRING5808							5808
#define IDS_STRING5809							5809

#define IDS_STRING5820							5820
#define IDS_STRING5821							5821
#define IDS_STRING5822							5822
#define IDS_STRING5823							5823
#define IDS_STRING5824							5824
#define IDS_STRING5825							5825
#define IDS_STRING5826							5826
#define IDS_STRING5827							5827
#define IDS_STRING5828							5828
#define IDS_STRING5829							5829
#define IDS_STRING5830							5830
#define IDS_STRING58260							58260
#define IDS_STRING58261							58261
#define IDS_STRING58262							58262
#define IDS_STRING58263							58263
#define IDS_STRING58264							58264
#define IDS_STRING58265							58265
#define IDS_STRING58266							58266
#define IDS_STRING58267							58267
#define IDS_STRING58268							58268
#define IDS_STRING58269							58269
#define IDS_STRING58270							58270

#define IDS_STRING5900							5900
#define IDS_STRING5901							5901
#define IDS_STRING5902							5902

#define IDS_STRING5903							5903
#define IDS_STRING5904							5904

#define IDS_STRING6000							6000
#define IDS_STRING6001							6001
#define IDS_STRING6002							6002
#define IDS_STRING6003							6003
#define IDS_STRING6004							6004
#define IDS_STRING6005							6005

#define IDS_STRING6006							6006
#define IDS_STRING6007							6007

#define IDS_STRING6008							6008
#define IDS_STRING6009							6009
#define IDS_STRING6010							6010
#define IDS_STRING6011							6011

#define IDS_STRING6100							6100

#define IDS_STRING6200							6200
#define IDS_STRING6201							6201
#define IDS_STRING6202							6202
#define IDS_STRING6203							6203
#define IDS_STRING6204							6204
#define IDS_STRING6205							6205
#define IDS_STRING6206							6206
#define IDS_STRING6207							6207
#define IDS_STRING6208							6208
#define IDS_STRING6209							6209
#define IDS_STRING6210							6210
#define IDS_STRING6211							6211
#define IDS_STRING6212							6212
#define IDS_STRING6213							6213
#define IDS_STRING6214							6214
#define IDS_STRING6215							6215

#define IDS_STRING6300							6300
#define IDS_STRING6310							6310
#define IDS_STRING6311							6311
#define IDS_STRING6312							6312
#define IDS_STRING6313							6313

#define IDS_STRING6314							6314
#define IDS_STRING6315							6315
#define IDS_STRING6316							6316
#define IDS_STRING6317							6317
#define IDS_STRING6318							6318
#define IDS_STRING6319							6319
#define IDS_STRING6320							6320

#define IDS_STRING6321							6321
#define IDS_STRING6322							6322
#define IDS_STRING6323							6323
#define IDS_STRING6324							6324
#define IDS_STRING6325							6325
#define IDS_STRING6326							6326

#define IDS_STRING6400							6400
#define IDS_STRING6401							6401
#define IDS_STRING6402							6402

#define IDS_STRING7000							7000

#define IDS_STRING701							701
#define IDS_STRING702							702
#define IDS_STRING703							703

#define IDS_STRING413							413 //#5029 PH 20160622

#define IDS_STRING8000							8000
#define IDS_STRING8001							8001
#define IDS_STRING8002							8002
#define IDS_STRING8003							8003
#define IDS_STRING8004							8004
#define IDS_STRING8005							8005
#define IDS_STRING8006							8006


#define IDS_STRING8007							8007
#define IDS_STRING8008							8008
#define IDS_STRING8009							8009
#define IDS_STRING8010							8010
#define IDS_STRING8011							8011
#define IDS_STRING8012							8012
#define IDS_STRING8013							8013
#define IDS_STRING8014							8014
#define IDS_STRING8015							8015
#define IDS_STRING8016							8016

#define IDS_STRING900							900
#define IDS_STRING901							901
#define IDS_STRING902							902
#define IDS_STRING903							903

/////////////////////////////////////////////////////////////////////////////////////////////////
// enumrated column values used in:

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18,
	COLUMN_19,
	COLUMN_20,
	COLUMN_21,
	COLUMN_22,
	COLUMN_23,
	COLUMN_24,
	COLUMN_25,
	COLUMN_26,
	COLUMN_27,
	COLUMN_28,
	COLUMN_29
};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated NEW or UPDATE
typedef enum { NEW_ITEM, UPD_ITEM } enumACTION;

//////////////////////////////////////////////////////////////////////////////////////////
// Species in P30 new norm for Pine,Spruce and Birch; 090608 p�d
// All species not in these relations are related to Birch; 090608 p�d (PL)
static int RELATED_TO_PINE[2]		= { 6,7};	// SpecieID's
static int RELATED_TO_SPRUCE[3]	= { 18,17,24};	// SpecieID's

//////////////////////////////////////////////////////////////////////////////////////////
// Buffer
#define BUFFER_SIZE		1024*100		// 100 kb buffer

#define BUFSIZE 4096

//////////////////////////////////////////////////////////////////////////////////////////
#define GISMSG_OBJECT								11


//////////////////////////////////////////////////////////////////////////////////////////
// Tabpage index; 080421 p�d
#define TAB_GENERAL_DATA						0		// "Tab f�r gnerella data"
#define TAB_PROPERTIES							1		// "Tab f�r fastigheter"

#define TAB_EVALUATED								0		// "Tab f�r v�rdering i PropertiesFormView"
#define TAB_CRUISING								1		// "Tab f�r taxering i PropertiesFormView"

//////////////////////////////////////////////////////////////////////////////////////////
// Type of cost template; 080115 p�d
#define COST_TYPE_1									1	// "Kostnader f�r Rotpost"
#define COST_TYPE_2									2	// "Kostnader f�r Rotpost i LandValue"

//////////////////////////////////////////////////////////////////////////////////////////
// Type of Template in "elv_template_table": P30 or "F�rdyrad avverkning"; 080402 p�d
#define TEMPLATE_P30								1	// "P30 prisetabell"
#define TEMPLATE_HCOST								2	// "Tabell f�rdyrad avverkning"
#define TEMPLATE_P30_NEW_NORM					3	// "P30 prisetabell Ny skogsnorm"
#define TEMPLATE_P30_2018_NORM				4	// "P30 prisetabell 2018 skogsnorm" #5385 PH 2017-09-27
		

//////////////////////////////////////////////////////////////////////////////////////////
// Identifies type of template; 080407 p�d
#define ID_TEMPLATE_TRAKT						1

//////////////////////////////////////////////////////////////////////////////////////////
// Type of Cruise; 090907 p�d
#define CRUISE_TYPE_1							0		// "Taxerat best�nd"
#define CRUISE_TYPE_2							1		// "Manuellt inl�st best�nd"

//////////////////////////////////////////////////////////////////////////////////////////
// Type of Evaluation; 090401 p�d
#define EVAL_TYPE_VARDERING						0		// "V�rdering"
#define EVAL_TYPE_FROM_TAXERING					1		// "Taxeringsbest�nd"
#define EVAL_TYPE_ANNAN							2		// "Annan mark, ex. �kermark"
#define EVAL_TYPE_4								3		// "V�rdering slut ber�knas inte"


////////////////////////////////////
// Reducerad mark
#define EVAL_REDUCMARK_INGET					0		// Inget
#define EVAL_REDUCMARK_OVERLAPPANDE				1		// �verlappande mark
#define EVAL_REDUCMARK_TILLFALLIGT				2		// Tillf�lligt utnyttjande
//////////////////////////////////////////////////////////////////////////////////////////
// Deafult diameterclass; 080507 p�d
#define DEF_DIAMCLASS							2.0

//////////////////////////////////////////////////////////////////////////////////////////
// Type of Net
#define TYPEOFNET_LOCAL							0		//Lrf 1996(lokalt n�t)
#define TYPEOFNET_SVENSKAKRAFTNAT				1		// Svenska kraftn�t
#define TYPEOFNET_SWEDISHENERGY					2		// Svensk energi 2014
#define TYPEOFNET_SWEDISHENERGY_2019			3		// Svensk energi 2019

//////////////////////////////////////////////////////////////////////////////////////////
// Type of fonts in CELVObjectSumRec; 080526 p�d
#define FNT_SMALL									0
#define FNT_SMALL_UNDERLINE				1
#define FNT_SMALL_BOLD						2
#define FNT_NORMAL								3
#define FNT_NORMAL_BOLD						4
#define FNT_HEADER								5

//////////////////////////////////////////////////////////////////////////////////////////
// defines for id of selected Report in Summarized data tab; 080527 p�d
#define TAB_RESULT_SLEN_REPORT		0
#define TAB_RESULT_EVAL_REPORT		1
#define TAB_RESULT_NETTO_REPORT		2

/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated NEW or UPDATE
//typedef enum { NEW_ITEM, UPD_ITEM } enumACTION;

//////////////////////////////////////////////////////////////////////////////////////////
// Type of document template
#define DOC_TMPL_ADVICE						0		// "Avisering"
#define DOC_TMPL_SALE_CONDITIONS	1		// "F�rs�ljningsvillkor"
#define DOC_TMPL_TAKT_CARE_COND		2		// "Villkor eget tillvaratagande"


//////////////////////////////////////////////////////////////////////////////////////////
// Defines for Logitems in elv_properties_log_table; 090423 p�d
#define LOG_ITEM_OWNER_ADDED			-1		// A user added log entry
#define LOG_ITEM_WORING1					0			// "Under behandling"
#define LOG_ITEM_WORING2					1			// "Aviserad"
#define LOG_ITEM_WORING3					2			// "Ers�ttningserbjudande"
#define LOG_ITEM_WORING4					3			// "Mark�gare; f�rs�ljning"
#define LOG_ITEM_WORING5					4			// "Mark�gare; egen regi"
#define LOG_ITEM_WORING6					5			// "Utbetalning"
#define LOG_ITEM_WORING7					6			// "KLAR"

//////////////////////////////////////////////////////////////////////////////////////////
// Define for Age determin if the Stand can be used to create a evaluated stand
// based on Stand; 090504 p�d
#define MAX_STAND_AGE_TO_CREATE_EVAL			500
#define MAX_STAND_AGE_TO_CREATE_EVAL_NEW	200

//////////////////////////////////////////////////////////////////////////////////////////
// Procentuell uppr�kning av Intr�ngsv�rdering; 100706 p�d
#define INCREASE_INFR_PERCENT		25.0

//////////////////////////////////////////////////////////////////////////////////////////
// HTM file for Viewing Pricelist; 090903 p�d
const LPCTSTR PRL_VIEW_FN								= _T("tmp_prl_view.htm");

//////////////////////////////////////////////////////////////////////////////////////////
// Tags used in XML-file for P30 pricetable; 080402 p�d
const LPCTSTR XML_FILE_HEADER							= _T("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

const LPCTSTR NODE_P30_TMPL_START					= _T("<obj_tmpl_p30>");
const LPCTSTR NODE_P30_TMPL_END						= _T("</obj_tmpl_p30>");
const LPCTSTR NODE_P30_TMPL_START_DATA			= _T("<p30_tmpl_data name=\"%s\" done_by=\"%s\" note=\"%s\">");
const LPCTSTR NODE_P30_TMPL_DATA						= _T("<p30_data spcid=\"%d\" spc_name=\"%s\" price=\"%.1f\" price_rel=\"%.1f\"/>");
const LPCTSTR NODE_P30_TMPL_END_DATA				= _T("</p30_tmpl_data>");

const LPCTSTR NODE_HCOST_TMPL_START					= _T("<obj_tmpl_hcost>");
const LPCTSTR NODE_HCOST_TMPL_END						= _T("</obj_tmpl_hcost>");

const LPCTSTR NODE_HCOST_TMPL_TYPE_SETTING	= _T("<hcost_tmpl_type type_set=\"%.d\" name=\"%s\"  done_by=\"%s\"/>");
const LPCTSTR NODE_HCOST_TMPL_DATA_BF				= _T("<hcost_tmpl_bf break_value=\"%.0f\" factor=\"%.0f\"/>");

const LPCTSTR NODE_HCOST_TMPL_START_DATA	= _T("<hcost_tmpl_data>");
const LPCTSTR NODE_HCOST_TMPL_DATA				= _T("<hcost_data m3sk=\"%.0f\" price_m3sk=\"%.0f\" base_comp=\"%.0f\" sum_price=\"%.0f\"/>");
const LPCTSTR NODE_HCOST_TMPL_END_DATA		= _T("</hcost_tmpl_data>");

const LPCTSTR NODE_P30NN_START						= _T("<obj_tmpl_p30nn>");
const LPCTSTR NODE_P30NN_END							= _T("</obj_tmpl_p30nn>");
const LPCTSTR NODE_P30NN_NAME							= _T("<name name=\"%s\"/>");
const LPCTSTR NODE_P30NN_DONE_BY					= _T("<done_by name=\"%s\"/>");
const LPCTSTR NODE_P30NN_AREA							= _T("<area name=\"%s\" index=\"%d\"/>");
const LPCTSTR NODE_P30NN_NOTES						= _T("<notes value=\"%s\"/>");
const LPCTSTR NODE_P30NN_SPC_START				= _T("<specie name=\"%s\" id=\"%d\" si=\"%s\">");
const LPCTSTR NODE_P30NN_SPC_END					= _T("</specie>");
const LPCTSTR NODE_P30NN_DATA							= _T("<data from=\"%d\" to=\"%d\" p30=\"%d\" prel=\"%.1f\"/>");
//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d
//const LPCTSTR TOOLBAR_RES_DLL							= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d
const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091014 p�d

const int RES_TB_FILTER						= 39;
const int RES_TB_FILTER_OFF				= 8;
const int RES_TB_IMPORT						= 19;
const int RES_TB_EXPORT						= 9;
const int RES_TB_TOOLS						= 47;
const int RES_TB_CALCULATE				= 46;
const int RES_TB_UPDATE						= 43;
const int RES_TB_CONTRACTOR				= 44;
const int RES_TB_ADD							= 0;
const int RES_TB_REMOVE						= 28;
const int RES_TB_PREVIEW					= 31;
const int RES_TB_PAPER_CLIP				= 13;
const int RES_TB_PRINT						= 36;
const int RES_TB_GIS							= 74;

//////////////////////////////////////////////////////////////////////////////////////////
// Tables in database; 071109 p�d
const LPCTSTR TBL_SPECIES				= _T("fst_species_table");
const LPCTSTR TBL_P30SPECIES_OBJECT		= _T("elv_object_p30_species_table");
const LPCTSTR TBL_PRICELISTS			= _T("prn_pricelist_table");
const LPCTSTR TBL_COSTS_TEMPLATE		= _T("cost_template_table");
const LPCTSTR TBL_PROPERTY				= _T("fst_property_table");
const LPCTSTR TBL_CONTACTS				= _T("fst_contacts_table");
const LPCTSTR TBL_CATEGORY				= _T("fst_category_table");
const LPCTSTR TBL_TEMPLATE				= _T("tmpl_template_table");
const LPCTSTR TBL_PROP_OWNER			= _T("fst_prop_owner_table");
const LPCTSTR TBL_CATEGORY_CONTACTS		= _T("fst_categories_for_contacts_table");
const LPCTSTR TBL_EXTERNAL_DOCS			= _T("fst_external_doc_table");

const LPCTSTR TBL_SAMPLE_TREE_CATEGORY	= _T("esti_sample_trees_category_table");

// LandValue database; 080116 p�d
const LPCTSTR LANDVALUE_TABLES						= _T("Create_elv_tables.sql");
const LPCTSTR TBL_ELV_OBJECT							= _T("elv_object_table");
const LPCTSTR TBL_ELV_OBJECT_LOG							= _T("elv_object_log_table");
const LPCTSTR TBL_ELV_PROP								= _T("elv_properties_table");
const LPCTSTR TBL_ELV_PROP_LOG						= _T("elv_properties_log_table");
const LPCTSTR TBL_ELV_PROP_OTHER_COMP			= _T("elv_properties_other_comp_table");
const LPCTSTR TBL_ELV_EVALUATION					= _T("elv_evaluation_table");
//const LPCTSTR TBL_ELV_CRUISE								= _T("elv_cruise_table");
const LPCTSTR TBL_ELV_CRUISE_RANDTREES		= _T("elv_cruise_randtrees_table");
const LPCTSTR TBL_ELV_TEMPLATE						= _T("elv_template_table");
const LPCTSTR TBL_ELV_DOC_TEMPLATE				= _T("elv_document_template_table");
const LPCTSTR TBL_ELV_PROP_STATUS					= _T("elv_property_status_table");	// Added 2009-09-14 P�D
/*
const LPCTSTR TBL_TRAKT										= "esti_trakt_table";
const LPCTSTR TBL_TRAKT_DATA								= "esti_trakt_data_table";
const LPCTSTR TBL_TRAKT_SPC_ASS						= "esti_trakt_spc_assort_table";
const LPCTSTR TBL_TRAKT_TRANS							= "esti_trakt_trans_assort_table";
const LPCTSTR TBL_TRAKT_SET_SPC						= "esti_trakt_set_spc_table";
const LPCTSTR TBL_TRAKT_MISC_DATA					= "esti_trakt_misc_data_table";
const LPCTSTR TBL_TRAKT_PLOT								= "esti_trakt_plot_table";
const LPCTSTR TBL_TRAKT_SAMPLE_TREES				= "esti_trakt_sample_trees_table";
const LPCTSTR TBL_TRAKT_DCLS_TREES					= "esti_trakt_dcls_trees_table";
const LPCTSTR TBL_TRAKT_DCLS1_TREES				= "esti_trakt_dcls1_trees_table";
const LPCTSTR TBL_TRAKT_DCLS2_TREES				= "esti_trakt_dcls2_trees_table";
const LPCTSTR TBL_TRAKT_TREES_ASSORT				= "esti_trakt_trees_assort_table";
const LPCTSTR TBL_TRAKT_ROTPOST						= "esti_trakt_rotpost_table";
*/

//////////////////////////////////////////////////////////////////////////////////////////
//
const TCHAR sz0dec[] = _T("%.0f");
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");
const TCHAR sz3dec[] = _T("%.3f");


//////////////////////////////////////////////////////////////////////////////////////////
// Name of directory hoding Inventory files (INV); 080204 p�d
//const LPCTSTR OBJECT_INVENTORY_DIR				= _T("Object");
const LPCTSTR INVENTORY_DIR							= _T("inv");
const LPCTSTR SETUP_DATA_DIR						= _T("setup");
const LPCTSTR SETUP_INFO_DIR						= _T("info");

const LPCTSTR SETUP_FILE_DEF_EXT				= _T(".sup");
const LPCTSTR SETUP_FILE_WC							= _T("*.sup");

//////////////////////////////////////////////////////////////////////////////////////////

const LPCTSTR LICENSE_NAME						= _T("License");				// Used for Languagefile for License; 080901 p�d
const LPCTSTR LICENSE_FILE_NAME				= _T("License.dll");		// Used on setting up filename and path; 080901 p�d
const LPCTSTR LICENSE_GET_LIC					= _T("GetLicense");

//////////////////////////////////////////////////////////////////////////////////////////
// Name of reports, for checking "GruppID" and "Mark�gare"; 100917 p�d
const LPCTSTR	HMS_5206_REPORT					= _T("HMS_5206.rpt");	// "Avisering"
const LPCTSTR	HMS_5201_REPORT					= _T("HMS_5201.rpt");	// "St�mplingsl�ngd"
const LPCTSTR	HMS_5207_REPORT					= _T("HMS_5207.rpt");	// "St�mplingsl�ngd per Objekt"
const LPCTSTR	HMS_5217_REPORT					= _T("HMS_5217.rpt");	// "St�mplingsl�ngd ut�kad"
const LPCTSTR	HMS_5202_REPORT					= _T("HMS_5202.rpt");	// "Intr�ngsv�rdering"
const LPCTSTR	HMS_5203_REPORT					= _T("HMS_5203.rpt");	// "Ers�ttningserbjudande"
const LPCTSTR	HMS_5205_REPORT					= _T("HMS_5205.rpt");	// "Rotnetto"
const LPCTSTR	HMS_5208_REPORT					= _T("HMS_5208.rpt");	// "Sammandrag"
const LPCTSTR	HMS_5209_REPORT					= _T("HMS_5209.rpt");	// "Objektsredovisning"
const LPCTSTR	HMS_5215_REPORT					= _T("HMS_5215.rpt");	// "Utskick"

//////////////////////////////////////////////////////////////////////////////////////////
// Register keys

const LPCTSTR REG_1950_FORREST_NORM_ENTRY_KEY						= _T("UMLandValue\\1950ForrestNormEntry\\Placement");

const LPCTSTR REG_1950_FORREST_NORM_LIST_ENTRY_KEY			= _T("UMLandValue\\1950ForrestNormListEntry\\Placement");

const LPCTSTR REG_WP_1950_FORREST_NORM_ENTRY_LAYOUT_KEY	= _T("UMLandValue_Layout_1950ForrestNormEntry");	// 071107 p�d

const LPCTSTR REG_WP_OBJECT_SELLEIST_REPORT_KEY					= _T("UMLandValue\\TraktSelListFrame\\Report");	// 080401 p�d

const LPCTSTR REG_P30_TEMPLATE_ENTRY_KEY								= _T("UMLandValue\\P30TemplateEntry\\Placement");	// 080402 p�d

const LPCTSTR REG_P30_NEW_NORM_TEMPLATE_ENTRY_KEY				= _T("UMLandValue\\P30NewNormTemplateEntry\\Placement");	// 080402 p�d

const LPCTSTR REG_P30_NORM_2018_TEMPLATE_ENTRY_KEY				= _T("UMLandValue\\P30Norm2018TemplateEntry\\Placement");	// 20180320 j�


const LPCTSTR REG_HIGHER_COSTS_ENTRY_KEY								= _T("UMLandValue\\HigherCostsEntry\\Placement");	// 080402 p�d

const LPCTSTR REG_P30_TEMPLATE_LIST_KEY									= _T("UMLandValue\\P30ListEntry\\Placement");	// 080402 p�d

const LPCTSTR REG_P30_NN_TEMPLATE_LIST_KEY							= _T("UMLandValue\\P30NewNormListEntry\\Placement");	// 080402 p�d

const LPCTSTR REG_P30_2018_TEMPLATE_LIST_KEY							= _T("UMLandValue\\P30Norm2018ListEntry\\Placement");	// 180320 j�

const LPCTSTR REG_DOC_TMPL_ENTRY_KEY										= _T("UMLandValue\\DocumentTmplEntry\\Placement");	// 080610 p�d

const LPCTSTR REG_DOC_TMPL_ENTRY_LIST_KEY								= _T("UMLandValue\\DocumentTmplListEntry\\Placement");	// 080610 p�d

const LPCTSTR REG_PROP_STATUS_KEY												= _T("UMLandValue\\PropStatus\\Placement");	// 080610 p�d

const LPCTSTR REG_WP_PROP_REPORT_KEY										= _T("UMLandValue\\PropertyReportFrame\\Report");	// 080419 p�d

const LPCTSTR REG_WP_EVALUATED_KEY											= _T("UMLandValue\\EvaluatedReport\\Report");	// 080428 p�d

const LPCTSTR REG_WP_CRUISING_KEY												= _T("UMLandValue\\CruisingReport\\Report");	// 080428 p�d

const LPCTSTR REG_PRINT_PRICELIST_KEY										= _T("UMLandValue\\PrintPricelists\\Placement");

const LPCTSTR REG_WP_PROP_STATUS_REPORT_KEY							= _T("UMLandValue\\PropertyStatusReportFrame\\Report");	// 090914 p�d

const LPCTSTR REG_WP_SPECIEFRAME_KEY									= _T("UMLandValue\\SpecieFrame\\Placement");

//const LPCTSTR REG_WP_PROPERTY_REMOVE_SELLIST_REPORT_KEY	= _T("Forrest\\PropertyRemoveSelListFrame\\Report");	// 070105 p�d
//const LPCTSTR REG_WP_PROPERTY_REMOVE_SELLIST_FRAME_KEY= _T("Forrest\\PropertyRemoveSelListFrame\\Placement");	// 070117 p�d

const LPCTSTR REG_IMPORT_EXCEL_KEY									= _T("UMLandValue\\ImportExcel");
const LPCTSTR REG_IMPORT_EXCEL_SPECIES_KEY							= _T("UMLandValue\\ImportExcel\\Species");
const LPCTSTR REG_IMPORT_EXCEL_TREETYPES_KEY						= _T("UMLandValue\\ImportExcel\\TreeTypes");
const LPCTSTR REG_IMPORT_EXCEL_VAL2									= _T("Val2");
const LPCTSTR REG_IMPORT_EXCEL_VAL3									= _T("Val3");
const LPCTSTR REG_IMPORT_EXCEL_VAL4									= _T("Val4");
const LPCTSTR REG_IMPORT_EXCEL_VAL5									= _T("Val5");
const LPCTSTR REG_IMPORT_EXCEL_VAL6									= _T("Val6");
const LPCTSTR REG_IMPORT_EXCEL_VAL7									= _T("Val7");
const LPCTSTR REG_IMPORT_EXCEL_VAL8									= _T("Val8");
const LPCTSTR REG_IMPORT_EXCEL_VAL9									= _T("Val9");
const LPCTSTR REG_IMPORT_EXCEL_VAL10								= _T("Val10");
const LPCTSTR REG_IMPORT_EXCEL_VAL11								= _T("Val11");
const LPCTSTR REG_IMPORT_EXCEL_VAL12								= _T("Val12");
const LPCTSTR REG_IMPORT_EXCEL_VAL13								= _T("Val13");
const LPCTSTR REG_IMPORT_EXCEL_VAL14								= _T("Val14");
const LPCTSTR REG_IMPORT_EXCEL_VAL15								= _T("Val15");
const LPCTSTR REG_IMPORT_EXCEL_VAL16								= _T("Val16");
const LPCTSTR REG_IMPORT_EXCEL_TOPPN								= _T("Toppn");
const LPCTSTR REG_IMPORT_EXCEL_FASAVST								= _T("Fasavst");
const LPCTSTR REG_IMPORT_EXCEL_KATEG								= _T("Kateg");
const LPCTSTR REG_IMPORT_EXCEL_FASAVST_UNIT							= _T("FasavstUnit");
const LPCTSTR REG_IMPORT_EXCEL_TOPPN_UNIT							= _T("ToppnUnit");
const LPCTSTR REG_IMPORT_EXCEL_GKHGT								= _T("GkHgt");
const LPCTSTR REG_IMPORT_EXCEL_GKHGT_UNIT							= _T("GkHgtUnit");
const LPCTSTR REG_IMPORT_EXCEL_BARK									= _T("Bark");
const LPCTSTR REG_IMPORT_EXCEL_SIDA									= _T("Sida");
const LPCTSTR REG_IMPORT_EXCEL_AREAL								= _T("Areal");
const LPCTSTR REG_IMPORT_EXCEL_AREAL_UNIT							= _T("ArealUnit");
const LPCTSTR REG_IMPORT_EXCEL_BESTALDER							= _T("Bestalder");
const LPCTSTR REG_IMPORT_EXCEL_BESTLAT								= _T("Bestlat");
const LPCTSTR REG_IMPORT_EXCEL_BESTLONG								= _T("Bestlong");
const LPCTSTR REG_IMPORT_EXCEL_BESTCOORD_UNIT						= _T("BestcoordUnit");
const LPCTSTR REG_IMPORT_EXCEL_BESTBON								= _T("Bestbon");
const LPCTSTR REG_IMPORT_EXCEL_LANGD								= _T("Langd");
const LPCTSTR REG_IMPORT_EXCEL_BREDD								= _T("Bredd");


//////////////////////////////////////////////////////////////////////////////////////////
// Register value for setting Last Object visited; 090922 p�d
const LPCTSTR	REG_LAST_OBJECT_VISITED_KEY									= _T("UMLandValue\\LastObjectVisited");	
const LPCTSTR	REG_LAST_OBJECT_VISITED_ITEM								= _T("ObjectID");	

const LPCTSTR	REG_LAST_OBJECT_VISITED_DB_KEY							= _T("UMLandValue\\LastObjectVisited_In_DB");	
const LPCTSTR	REG_LAST_OBJECT_VISITED_DB_ITEM							= _T("DBName");	

//////////////////////////////////////////////////////////////////////////////////////////
// Filename of logfile
const LPCTSTR LAND_VALUE_LOG_FILE_NAME										= _T("LandValue_Log");
const LPCTSTR LAND_VALUE_MATCH_LOG_FILE_NAME							= _T("_match_log");
const LPCTSTR LAND_VALUE_ROTPOST_LOG_FILE_NAME						= _T("_rotpost_log");

const LPCTSTR SQL_SERVER_SQRIPT_DIR												= _T("Microsoft SQL Server");

//////////////////////////////////////////////////////////////////////////////////////////
// Extensions for Import/Export of P30 and Higher cost tables; 081202 p�d
const LPCTSTR P30_TABLE_EXTENSION							= _T(".h30xl");
const LPCTSTR P30_NN_TABLE_EXTENSION					= _T(".h30xln");
const LPCTSTR P30_2018_TABLE_EXTENSION					= _T(".h2018xln");
const LPCTSTR HCOST_TABLE_EXTENSION						= _T(".hgcxl");

//////////////////////////////////////////////////////////////////////////////////////////
// Added 2009-09-08 p�d
#define LANDVALUE_NORM_FN						_T("UCLandValueNorm.dll")
#define LANDVALUE_CALC_1950					("doStormDryCalc_1950")	// Exported function in UCLandValueNorm
#define LANDVALUE_CALC_2009					("doStormDryCalc_2009")	// Exported function in UCLandValueNorm
#define LANDVALUE_CALC_2018					("doStormDryCalc_2018")	// Exported function in UCLandValueNorm

#define LANDVALUE_CORR_FAC_1950			("doCorrFactorCalc_1950")	// Exported function in UCLandValueNorm
#define LANDVALUE_CORR_FAC_2009			("doCorrFactorCalc_2009")	// Exported function in UCLandValueNorm
#define LANDVALUE_CORR_FAC_2018			("doCorrFactorCalc_2018")	// Exported function in UCLandValueNorm

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of the Window
const int MIN_1950_FORREST_NORM_X_SIZE				= 50; //450;
const int MIN_1950_FORREST_NORM_Y_SIZE				= 50; //250;

const int MIN_1950_FORREST_NORM_LIST_X_SIZE		= 50; //450;
const int MIN_1950_FORREST_NORM_LIST_Y_SIZE		= 20; //250;

const int MIN_P30_TEMPLATE_X_SIZE							= 50; //450;
const int MIN_P30_TEMPLATE_Y_SIZE							= 50; //250;

const int MIN_P30_LIST_TEMPLATE_X_SIZE				= 50; //450;
const int MIN_P30_LIST_TEMPLATE_Y_SIZE				= 20; //250;

const int MIN_HIGHER_COSTS_X_SIZE							= 50; //450;
const int MIN_HIGHER_COSTS_Y_SIZE							= 50; //250;

const int MIN_DOC_TMPL_X_SIZE									= 50; //450;
const int MIN_DOC_TMPL_Y_SIZE									= 50; //250;

const int MIN_DOC_TMPL_LIST_X_SIZE						= 50; //450;
const int MIN_DOC_TMPL_LIST_Y_SIZE						= 20; //250;

const int MIN_X_SIZE_PRICELISTS_LIST					= 450;
const int MIN_Y_SIZE_PRICELISTS_LIST					= 300;

const int MIN_X_SIZE_PROP_STATUS							= 50;
const int MIN_Y_SIZE_PROP_STATUS							= 50;

const int MIN_X_SIZE_PROPERTY_REMOVE_SELLIST		= 585;
const int MIN_Y_SIZE_PROPERTY_REMOVE_SELLIST		= 350;

//////////////////////////////////////////////////////////////////////////////////////////
// Define for property status
/*
#define STATUS_UNDER_DEVELOP	0	// "Under utveckling"

#define STATUS_ADVICED				1	// "Aviserad"
#define STATUS_COMPENS_OFFER	2	// "Ers�ttningserbjudande"
#define STATUS_RETURNED1			3	// "Retur fr�n mark�gare; F�rs�ljning"
#define STATUS_RETURNED2			4	// "Retur fr�n mark�gare; Eget tillvaratagande"
#define STATUS_PAYED					5	// "Utbetalning"
#define STATUS_DONE						6	// "Klar"
*/
// Define color for property status
#define STATUS_COLOR_UNDER_DEVELOP	RGB(  0,  0,  0)	// "Under utveckling"
#define STATUS_COLOR_ADVICED				RGB(244,238,224)	// "Aviserad"
#define STATUS_COLOR_COMPENS_OFFER	RGB(255,165,	0)	// "Ers�ttningserbjudande"
#define STATUS_COLOR_RETURNED1			RGB( 95,158,160)	// "Retur fr�n mark�gare, f�rs�ljning"
#define STATUS_COLOR_RETURNED2			RGB(205, 92, 92)	// "Retur fr�n mark�gare, egen regi"
#define STATUS_COLOR_PAYED					RGB(255,255,	0)	// "Utbetalning"
#define STATUS_COLOR_DONE						RGB(154,205, 50)	// "Klar"

// Define number of Panes (Properties/Cruise and Evaluation)
#define NUMBER_OF_PANES				2
#define PANE_1_INDEX					0
#define PANE_2_INDEX					1

//////////////////////////////////////////////////////////////////////////////////////////
// Colors
#define CYAN					RGB(		0,  255,  255)
#define LTCYAN				RGB(  224,  255,  255)

//////////////////////////////////////////////////////////////////////////////////////////
// Min and Max value for "Prisrelation"; 101006 p�d
#define MIN_PREL_VALUE		0.5
#define MAX_PREL_VALUE		0.8

//////////////////////////////////////////////////////////////////////////////////////////
// Log settings registry
const LPCTSTR LOG_SETTINGS_FILE	= _T("Settings.csv");	// 121211 P�D #3502

//////////////////////////////////////////////////////////////////////////////////////////
/*	Defined in WinUser.h
#define IDI_APPLICATION     MAKEINTRESOURCE(32512)
#define IDI_HAND            MAKEINTRESOURCE(32513)
#define IDI_QUESTION        MAKEINTRESOURCE(32514)
#define IDI_EXCLAMATION     MAKEINTRESOURCE(32515)
#define IDI_ASTERISK        MAKEINTRESOURCE(32516)
*/
//////////////////////////////////////////////////////////////////////////////////////////
// This structure's used to hold information on specie(s) not havin' any functions
// added. I.e. no volume-,bark- or heightfunction; 070626 p�d
// OBS! This structure is also in UMEstimate; 080117 p�d
/*
typedef struct _func_spc_struct
{
	int nSpcID;
	CString sSpcName;
	_func_spc_struct(void)
	{
		nSpcID = -1;
		sSpcName = _T("");
	}
	_func_spc_struct(int spc_id,LPCTSTR spc_name)
	{
		nSpcID = spc_id;
		sSpcName = _T(spc_name);
	}
} FUNC_SPC_STRUCT;

typedef std::vector<FUNC_SPC_STRUCT> vecFuncSpc;
*/
/////////////////////////////////////////////////////////////////////////////

class CIntItem : public CXTPReportRecordItemNumber
{
//private:
	int m_nValue;
public:
	CIntItem(void) : 
			CXTPReportRecordItemNumber(0)
	{
		m_nValue = 0;
	}

	CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
	{
		m_nValue = nValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
	}

	void setIntItem(int value)	
	{ 
		m_nValue = value; 
		SetValue(value);
	}

	int getIntItem(void)	{ return m_nValue; }
};

class CFloatItem : public CXTPReportRecordItemNumber
{
//private:
	double m_fValue;
public:
	CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
			CXTPReportRecordItemNumber(fValue)
	{
		SetFormatString(fmt_str);
		m_fValue = fValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
	{
		m_fValue = (double)_tstof(szText);
		SetValue(m_fValue);

		CXTPReportControl *pCtrl = NULL;
		if (pItemArgs != NULL)
		{
			pCtrl = pItemArgs->pControl;
			if (pCtrl != NULL)
			{
				pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
			}
		}
	}

	void setFloatItem(double value)	
	{ 
		m_fValue = value; 
		SetValue(value);
	}
	double getFloatItem(void)	{ return m_fValue; }

	void setIsBold(void)		{ SetBold();	}

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}

};

class CMinMaxFloatItem : public CXTPReportRecordItemNumber
{
//private:
	double m_fValue;
	double m_fMinValue;
	double m_fMaxValue;
	bool m_bAllOK;
public:
	CMinMaxFloatItem(double fValue,double min_value,double max_value,LPCTSTR fmt_str = sz1dec) : 
			CXTPReportRecordItemNumber(fValue)
	{
		m_bAllOK = true;
		SetFormatString(fmt_str);
		m_fValue = fValue;
		m_fMinValue = min_value;
		m_fMaxValue = max_value;
		if (m_fValue < m_fMinValue || m_fValue > m_fMaxValue) 
			m_bAllOK = false;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
	{
		m_bAllOK = true;
		m_fValue = (double)_tstof(szText);
		if (m_fValue < m_fMinValue || m_fValue > m_fMaxValue) 
			m_bAllOK = false;
		SetValue(m_fValue);

		CXTPReportControl *pCtrl = NULL;
		if (pItemArgs != NULL)
		{
			pCtrl = pItemArgs->pControl;
			if (pCtrl != NULL)
			{
				pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
			}
		}
	}

	void setFloatItem(double value)	
	{ 
		m_bAllOK = true;
		m_fValue = value; 
		if (m_fValue < m_fMinValue || m_fValue > m_fMaxValue) 
			m_bAllOK = false;
		SetValue(m_fValue);
	}
	double getFloatItem(void)	{ return m_fValue; }

	void setIsBold(void)		{ SetBold();	}

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}

	bool getAllOK(void)		{ return 	m_bAllOK; }


};

class CTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:
	CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
	{
		m_sText = sValue;
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_sText = szText;
			SetValue(m_sText);
	}

	CString getTextItem(void)	{ return m_sText; }
	void setTextItem(LPCTSTR text)	
	{ 
		m_sText = text; 
		SetValue(m_sText);
	}
};


class CStrikeOutTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:
	CStrikeOutTextItem(CString sValue) : 
			CXTPReportRecordItemText(sValue)
	{
		m_sText = sValue;
		SetValue(m_sText);
	}

	virtual ~CStrikeOutTextItem(void)	{}

	void GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
	{
		static CFont m_fntStrikeOut;
		if (!m_fntStrikeOut.GetSafeHandle())
		{
			LOGFONT lf;
			pDrawArgs->pControl->GetPaintManager()->m_fontText.GetLogFont(&lf);
			lf.lfStrikeOut = TRUE;

			m_fntStrikeOut.CreateFontIndirect(&lf);
		}

		pItemMetrics->clrForeground = RGB(128, 128, 128);
		pItemMetrics->pFont = &m_fntStrikeOut;		
	}

};

// Extended functions

class CExIntItem : public CXTPReportRecordItemNumber
{
//private:
	int m_nValue;
public:
	CExIntItem(void) : 
			CXTPReportRecordItemNumber(0)
	{
		m_nValue = 0;
	}

	CExIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
	{
		m_nValue = nValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
	}

	int getIntItem(void)		{ return m_nValue; }
	void setIntItem(int v)	{ m_nValue = v; SetValue(m_nValue); }

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
};

class CExIntMsgItem : public CXTPReportRecordItemNumber
{
//private:
	int m_nValue;
	CString m_sMsg;
public:
	CExIntMsgItem(void) : 
			CXTPReportRecordItemNumber(1)
	{
		m_nValue = 1;
		m_sMsg = _T("");
	}

	CExIntMsgItem(int nValue,LPCTSTR msg) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
	{
		m_nValue = nValue;
		m_sMsg = msg;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_nValue = _tstoi(szText);
			if (m_nValue == 1 || m_nValue == 2)
			{
				SetValue(m_nValue);
			}
			else
			{
				m_nValue = 1;
				SetValue(m_nValue);
			}
	}

	int getIntItem(void)		{ return m_nValue; }
	void setIntItem(int v)	{ m_nValue = v; }

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
};

class CExFloatItem : public CXTPReportRecordItemNumber
{
//private:
	double m_fValue;
public:
	CExFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
			CXTPReportRecordItemNumber(fValue)
	{
		SetFormatString(fmt_str);
		m_fValue = fValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
	{
		m_fValue = (double)_tstof(szText);
		SetValue(m_fValue);
		CXTPReportControl *pCtrl = NULL;
		if (pItemArgs != NULL)
		{
			pCtrl = pItemArgs->pControl;
			if (pCtrl != NULL)
			{
				pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
			}
		}
	}

	void setFloatItem(double value)	
	{ 
		m_fValue = value; 
		SetValue(value);
	}
	double getFloatItem(void)	{ return m_fValue; }

	void setIsBold(void)		{ SetBold();	}

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
};

class CExTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:

	CExTextItem(CString sValue);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);
	
	CString getTextItem(void);
	void setTextItem(LPCTSTR text);
	void setIsBold(void);

	void setTextColor(COLORREF rgb_value);
	void setTextBkColor(COLORREF rgb_value);
};

class CExConstraintTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:
	static int m_nConstraintIndex;

	CExConstraintTextItem(CString sValue);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);
	void OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs,CXTPReportRecordItemConstraint* pConstraint);
	
	CString getTextItem(void);
	void setTextItem(LPCTSTR text);
	int getConstraintIndex(void);
	void setIsBold(void);

	void setTextColor(COLORREF rgb_value);
	void setTextBkColor(COLORREF rgb_value);
};

class CExTextH100Item : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
	short m_nNormUsed;
	int m_nConstraintIndex;
public:
	CExTextH100Item(CString sValue,short norm_used) : CXTPReportRecordItemText(sValue)
	{
		m_sText = sValue;
		m_nNormUsed = norm_used;
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
	{
		//m_nConstraintIndex = CExConstraintTextItem::m_nConstraintIndex; // Selected index in a Combobox Constraint; 080515 p�d
		m_sText = szText;
		m_sText.MakeUpper();
		// Only check this if m_nConstraintIndex = 0 ("V�rdering"); 080515 p�d
		//if (m_nConstraintIndex == 0)
		//{
		// Added FCE for adapting to "ny norm, F = Bok,E = Ek, C = Controta"; 081125 p�d
		// Added IMP = Impedimant, for "1950:�rs norm"; 100913 p�d
		if (m_sText.FindOneOf(_T("TGBFCE")) > -1 || m_sText.IsEmpty() || (m_sText.CompareNoCase(L"IMP") == 0 && m_nNormUsed == 0))
				SetValue(m_sText);
		else
		{
			AfxMessageBox(_T("ERROR: Felaktigt H100 - v�rde"));		
			SetValue(_T(""));
		}
		//}	// if (m_nConstraintIndex == 0)
	}

	CString getTextItem(void)	{ return m_sText; }
	void setTextItem(LPCTSTR text)	
	{ 
		m_sText = text; 
		SetValue(m_sText);
	}

	void setIsBold(void)		{ SetBold();	}

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
};


//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying attachments icons.
class CExIconItem : public CXTPReportRecordItem
{
	CString m_sText;
	double m_fValue;
	int m_dec;
public:
	// Constructs record item with the initial value.
	CExIconItem(CString text,int icon_id);
	CExIconItem(int num_of,int icon_id);
	CExIconItem(double value,int dec,int icon_id);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	void setTextItem(CString text)
	{
		m_sText = text;
		SetCaption((m_sText));
	}

	CString getExIconText(void)		{ return m_sText; }
	void setExIconText(LPCTSTR v)	
	{ 
		m_sText = v; 
		SetCaption((m_sText));
	}

	double getExIconFloat(void)		{ return m_fValue; }
	void setExIconFloat(double v)	
	{ 
		TCHAR tmp[50];
		m_fValue = v; 
		_stprintf(tmp, _T("%.*f"), m_dec,m_fValue);
		SetCaption((tmp));
	}
};


//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying attachments icons.
// Also include a check of range of data entered; 101103 p�d
class CExIconItemEx : public CXTPReportRecordItem
{
	CString m_sText;
	double m_fValue;
	int m_dec;
	int m_nMin,m_nMax;
	double m_fMin,m_fMax;
public:
	// Constructs record item with the initial value.
	CExIconItemEx(CString text,int icon_id);
	CExIconItemEx(int num_of,int icon_id,int min,int max);
	CExIconItemEx(double value,int dec,int icon_id,double min,double max);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	void setTextItem(CString text)
	{
		m_sText = text;
		SetCaption((m_sText));
	}

	CString getExIconText(void)		{ return m_sText; }
	void setExIconText(LPCTSTR v)	
	{ 
		m_sText = v; 
		SetCaption((m_sText));
	}

	double getExIconFloat(void)		{ return m_fValue; }
	void setExIconFloat(double v)	
	{ 
		TCHAR tmp[50];
		m_fValue = v; 
		_stprintf(tmp, _T("%.*f"), m_dec,m_fValue);
		SetCaption((tmp));
	}
};

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying attachments icons.
typedef struct _icon_toogle_struct
{
	int _icon_id;
	TCHAR szText[127];

	_icon_toogle_struct(void)	{ _icon_id = 0; _tcscpy(szText,_T("")); }
	_icon_toogle_struct(int icon_id,LPCTSTR text)	{ _icon_id = icon_id; _tcscpy(szText,text); }
} ICON_TOOGLE_STRUCT;

typedef std::vector<ICON_TOOGLE_STRUCT> vecIconToogle;

class CExIconToogleItem : public CXTPReportRecordItem
{
	CString m_sText;
	int m_nSize;
	int m_nToogleValue;
	vecIconToogle m_vecIconToogle;
	enum enumDirection {NEXT,PREV};
	enumDirection m_enumDirection;
public:
	enum enumToogleType {TOOGLE_ONLY_FW,
											 TOOGLE_FW_BW};
	enumToogleType m_enumTOOGLE_TYPE;

	// Constructs record item with the initial value.
	CExIconToogleItem(vecIconToogle&,int default_item = 0,enumToogleType toogle_type=TOOGLE_ONLY_FW);

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}

	virtual CString getExIconToogleText(void)		{ return m_sText; }
	virtual void setExIconToogleText(LPCTSTR v);

	virtual void setToogle(void);
	virtual int getToogle(void)	{ return m_nToogleValue; }
};

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying checkboxes.
class CExCheckItem : public CXTPReportRecordItem
{
	public:
	// Constructs record item with the initial checkbox value.
	CExCheckItem(BOOL bCheck)
	{
		HasCheckbox(TRUE);
		SetChecked(bCheck);
	}

	virtual BOOL getChecked(void)
	{
		return IsChecked()? TRUE: FALSE;
	}

	virtual void setChecked(BOOL bCheck)
	{
		SetChecked(bCheck);
	}
};

BOOL hitTest_X(int hit_x,int limit_x,int w_x);

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying checkboxes.
class CExCheckItemEx : public CXTPReportRecordItem
{
	CStringArray m_sarr;
public:
	// Constructs record item with the initial checkbox value.
	CExCheckItemEx(BOOL bCheck,CStringArray& accepted_as)
	{
		m_sarr.Append(accepted_as);
		HasCheckbox(TRUE);
		SetChecked(bCheck);
		if (m_sarr.GetCount() > 1)
		{
			if (bCheck)	SetCaption(m_sarr.GetAt(0));
			else	SetCaption(m_sarr.GetAt(1));
		}
	}
	
	virtual BOOL getChecked(void)	{	return IsChecked();	}

	virtual void setChecked(BOOL bCheck)	{	SetChecked(bCheck);	}

	virtual void setHitTestOK(BOOL is_ok)	
	{ 
		if (m_sarr.GetCount() > 1)
		{
			if (is_ok)
			{
				if (IsChecked()) SetCaption(m_sarr.GetAt(0));
				else SetCaption(m_sarr.GetAt(1));
			}
			else
			{
				if (IsChecked()) SetChecked(FALSE); 
				else SetChecked(TRUE);
			}
		}	// if (m_sarr.GetCount() > 1)
	}

};


/////////////////////////////////////////////////////////////////////////////
// CP30ReportRec

class CP30ReportRec : public CXTPReportRecord
{
	CTransaction_elv_p30 recP30Data;
	CTransaction_template recTemplate;
protected:

public:

	CP30ReportRec(void)
	{
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz1dec));
	}

	
	CP30ReportRec(CTransaction_elv_p30& recP30)
	{
		CString cSpec=_T("");
		switch(recP30.getP30SpcID())
		{
		case 1:
			cSpec=_T("Tall");
			break;
		case 2:
			cSpec=_T("Gran");
			break;
		default:
			cSpec=_T("L�v");
			break;
		}
		recP30Data = recP30;
		//AddItem(new CTextItem(recP30.getP30SpecieName()));
		AddItem(new CTextItem(cSpec));

		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(recP30.getP30Value(),sz0dec));
		AddItem(new CFloatItem(recP30.getP30PriceRel(),sz1dec));
	}

	/*
	CP30ReportRec(CTransaction_template &recTmpl,CTransaction_elv_p30& recP30)
	{		
		recTemplate = recTmpl;
		recP30Data = recP30;		
		AddItem(new CTextItem(recP30.getP30SpecieName()));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(recP30.getP30Value(),sz0dec));
		AddItem(new CFloatItem(recP30.getP30PriceRel(),sz1dec));
	}*/

	
	CP30ReportRec(CTransaction_template &recTmpl,CTransaction_elv_p30& recP30,LPCTSTR spc_name,LPCTSTR si_from_to,double p30,double price_rel)
	{
		recTemplate = recTmpl;
		recP30Data = recP30;
		CString cSpec=_T("");
		switch(recP30.getP30SpcID())
		{
		case 1:
			cSpec=_T("Tall");
			break;
		case 2:
			cSpec=_T("Gran");
			break;
		default:
			cSpec=_T("L�v");
			break;
		}
		//AddItem(new CTextItem(spc_name));
		AddItem(new CTextItem(cSpec));
		AddItem(new CTextItem(si_from_to));
		AddItem(new CFloatItem(p30,sz0dec));
		AddItem(new CFloatItem(price_rel,sz1dec));
	}
	

	CTransaction_template& getRecTemplate(void)		{ return recTemplate; }
	CTransaction_elv_p30& getRecP30(void)					{ return recP30Data;	}

	double getColumnFloat(int item)		{ return ((CFloatItem*)GetItem(item))->getFloatItem();	}

	void setColumnFloat(int item,double value)	{ ((CFloatItem*)GetItem(item))->setFloatItem(value);	}

	CString getColumnText(int item)	{ return ((CTextItem*)GetItem(item))->getTextItem();	}
	
	void setColumnText(int item,LPCTSTR text)	{ ((CTextItem*)GetItem(item))->setTextItem(text);	}

};



/////////////////////////////////////////////////////////////////////////////
// CP30NewNormReportRec

class CP30NewNormReportRec : public CXTPReportRecord
{
	bool m_bAllOK;
protected:

public:

	CP30NewNormReportRec(void)
	{
		m_bAllOK = true;
		AddItem(new CIntItem());
		AddItem(new CIntItem());
		AddItem(new CIntItem());
		AddItem(new CMinMaxFloatItem(0.0,MIN_PREL_VALUE,MAX_PREL_VALUE,sz1dec));

		this->GetItem(COLUMN_0)->SetEditable(FALSE);	
		this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_1)->SetEditable(FALSE);	
		this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
	}

	CP30NewNormReportRec(int from,int to,int p30,double prel)
	{
		AddItem(new CIntItem(from));
		AddItem(new CIntItem(to));
		AddItem(new CIntItem(p30));
		AddItem(new CMinMaxFloatItem(prel,MIN_PREL_VALUE,MAX_PREL_VALUE,sz1dec));

		this->GetItem(COLUMN_0)->SetEditable(FALSE);	
		this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_1)->SetEditable(FALSE);	
		this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);

	}

	double getColumnFloat(int item)		{ return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	void setColumnFloat(int item,double value)	{ ((CFloatItem*)GetItem(item))->setFloatItem(value);	}

	double getColumnFloatMM(int item)		{ return ((CMinMaxFloatItem*)GetItem(item))->getFloatItem();	}
	void setColumnFloatMM(int item,double value)	{ ((CMinMaxFloatItem*)GetItem(item))->setFloatItem(value);	}

	bool getColumnFloatOK(int item)		{ return ((CMinMaxFloatItem*)GetItem(item))->getAllOK();	}

	int getColumnInt(int item)				{ return ((CIntItem*)GetItem(item))->getIntItem();	}
	void setColumnInt(int item,int v)	{ ((CIntItem*)GetItem(item))->setIntItem(v);	}

};


/////////////////////////////////////////////////////////////////////////////
// CHCostReportRec

class CHCostReportRec : public CXTPReportRecord
{
	//private:
	int nIndex;
	int nObjID;
	CTransaction_template recTemplate;
protected:

	
public:
	CHCostReportRec(void)
	{
		nIndex = -1;
		nObjID = -1;
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CFloatItem(0.0,(sz0dec)));
	}

	CHCostReportRec(int idx,int obj_id,double volume_m3sk,double price,double base_comp,double sum_price)
	{
		nIndex = idx;
		nObjID = obj_id;
		AddItem(new CFloatItem(volume_m3sk,(sz0dec)));
		AddItem(new CFloatItem(price,(sz0dec)));
		AddItem(new CFloatItem(base_comp,(sz0dec)));
		AddItem(new CFloatItem(sum_price,(sz0dec)));
	}
	CHCostReportRec(CTransaction_elv_hcost& rec)
	{
		nIndex = rec.getHCostID_pk();
		nObjID = rec.getHCostObjID_pk();
		AddItem(new CFloatItem(rec.getHCostM3Sk(),(sz0dec)));
		AddItem(new CFloatItem(rec.getHCostPrice(),(sz0dec)));
		AddItem(new CFloatItem(rec.getHCostBaseComp(),(sz0dec)));
		AddItem(new CFloatItem(rec.getHCostSUM(),(sz0dec)));
	}

	CHCostReportRec(CTransaction_template &recTmpl,CTransaction_elv_hcost& recHCost)
	{
		recTemplate = recTmpl;
		nIndex = recHCost.getHCostID_pk();
		nObjID = recHCost.getHCostObjID_pk();
		AddItem(new CFloatItem(recHCost.getHCostM3Sk(),(sz0dec)));
		AddItem(new CFloatItem(recHCost.getHCostPrice(),(sz0dec)));
		AddItem(new CFloatItem(recHCost.getHCostBaseComp(),(sz0dec)));
		AddItem(new CFloatItem(recHCost.getHCostSUM(),(sz0dec)));
	}

	CTransaction_template& getRecTemplate(void)
	{
		return recTemplate;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	void setColumnBold(int num,...)	
	{ 
		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CFloatItem*)GetItem(col))->setIsBold();
		}
		va_end(vl);
	}

	void setColumnTextColor(COLORREF rgb_value,int num,...)	
	{ 
		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CFloatItem*)GetItem(col))->setTextColor(rgb_value);
		}
		va_end(vl);
	}
};



/////////////////////////////////////////////////////////////////////////////
// CTemplateListReportDataRec

class CTemplateListReportDataRec : public CXTPReportRecord
{
	int m_nID;
protected:
public:

	CTemplateListReportDataRec(void)
	{
		m_nID	= -1;	
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTemplateListReportDataRec(int id,LPCTSTR tmpl_name,LPCTSTR done_by,LPCTSTR date)
	{
		m_nID	= id;
		AddItem(new CTextItem(tmpl_name));
		AddItem(new CTextItem(done_by));
		AddItem(new CTextItem(date));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

/////////////////////////////////////////////////////////////////////////////
// CELVPropertyReportRec

class CELVPropertyReportRec : public CXTPReportRecord
{
	//private:
	int nPropID;
	int nObjID;
	CTransaction_elv_properties recELVProp;
protected:

	
public:
	CELVPropertyReportRec(vecIconToogle& vec)
	{
		nPropID = -1;
		nObjID = -1;
		AddItem(new CExIconItem(_T("0"),2));
		AddItem(new CExIconItem(0,6));
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExIconItem(_T(""),3));
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExIntItem(0));
		AddItem(new CExIntItem(0));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExIconItem(0.0,0,5));
		AddItem(new CExIconItem(0.0,0,5));
		AddItem(new CExIconItem(0.0,0,2));
		AddItem(new CExIconItem(_T(""),5));
		AddItem(new CExIconItem(_T(""),15));
		AddItem(new CExIconToogleItem(vec));
		AddItem(new CExIntItem(0));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExTextItem(_T("")));
	}

	CELVPropertyReportRec(int prop_id,int obj_id,LPCTSTR status_txt,LPCTSTR numof_entries,LPCTSTR numof_docs,CTransaction_elv_properties& rec,vecIconToogle& vec)
	{
		recELVProp = rec;
		nPropID = prop_id;
		nObjID = obj_id;
		AddItem(new CExIconItem((numof_entries),2));
		AddItem(new CExIconItem(numof_docs,6));
		AddItem(new CExTextItem((status_txt)));
		AddItem(new CExTextItem((rec.getPropCounty())));
		AddItem(new CExTextItem((rec.getPropMunicipal())));
		AddItem(new CExIconItem((rec.getPropName()),3));
		AddItem(new CExTextItem((rec.getPropNumber())));
		AddItem(new CExFloatItem(rec.getPropM3Sk(),(sz2dec)));
		AddItem(new CExFloatItem(rec.getPropAreal(),(sz2dec)));
		AddItem(new CExIntItem(rec.getPropNumOfTrees()));
		AddItem(new CExFloatItem(rec.getPropNumOfStands(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropWoodVolume(),(sz2dec)));
		AddItem(new CExFloatItem(rec.getPropWoodValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropCostValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropRotpost(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropLandValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropEarlyCutValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropStromDryValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropRandTreesValue(),(sz0dec)));
		AddItem(new CExIconItem(rec.getPropVoluntaryDealValue(),0,5));
		AddItem(new CExIconItem(rec.getPropHighCutValue(),0,5));
		AddItem(new CExIconItem(rec.getPropOtherCostValue(),0,2));
		AddItem(new CExIconItem((rec.getPropGroupID()),5));
		AddItem(new CExIconToogleItem(vec,rec.getPropTypeOfAction()));
		AddItem(new CExIntItem(rec.getPropSortOrder()));
		AddItem(new CExFloatItem(rec.getPropGrotVolume(),(sz3dec)));
		AddItem(new CExFloatItem(rec.getPropGrotValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getPropGrotCost(),(sz0dec)));
		AddItem(new CExTextItem(rec.getSearchID()));
		AddItem(new CExTextItem(rec.getMottagarref()));

	}

	CTransaction_elv_properties& getRecELVProp(void)	{	return recELVProp;	}

	double getColumnFloat(int item)	{ return ((CExFloatItem*)GetItem(item))->getFloatItem(); }

	int getColumnInt(int item)	{ return ((CExIntItem*)GetItem(item))->getIntItem(); }
	void setColumnInt(int item,int v)	{ return ((CExIntItem*)GetItem(item))->setIntItem(v); }

	void setColumnFloat(int item,double value)	{ ((CExFloatItem*)GetItem(item))->setFloatItem(value);	}

	CString getColumnText(int item)	{ return ((CExTextItem*)GetItem(item))->getTextItem();	}
	
	CString getColumnIconText(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconText();	}
	double getColumnIconFloat(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconFloat();	}

	void setColumnText(int item,LPCTSTR value)	{ ((CExIconItem*)GetItem(item))->setTextItem(value);	}

	void setColumnBold(int num,...)	
	{ 

		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CExFloatItem*)GetItem(col))->setIsBold();
		}
		va_end(vl);
	}

	BOOL getColumnCheck(int item)	{		return ((CExCheckItem*)GetItem(item))->getChecked();	}

	void setColumnExCheckItemExHitTest(int item,BOOL is_ok)	{ ((CExCheckItemEx*)GetItem(item))->setHitTestOK(is_ok);	}

	int getColumnToogle(int item)	{ return ((CExIconToogleItem*)GetItem(item))->getToogle();	}
	void setColumnToogle(int item)	{ ((CExIconToogleItem*)GetItem(item))->setToogle();	}
	//////////////////////////////////////////////////////////////////////////////////////////
//	void setColumnTextColor(int status)	
	void setColumnBkgndColor(COLORREF color)	
	{ 
			for (int i = 0;i < GetItemCount();i++)
			{
				((CExIconItem*)GetItem(i))->setTextBkColor(color);

			}	// for (int i = 0;i < GetItemCount();i++)

	}
	//Added function for retreiving object id and property id 20100305
	int getObjId(void){return nObjID;}
	int getPropId(void){return nPropID;}
};

// Added 2009-09-16 p�d
// Check status set for property. Can we do a calculation or not; 090916 p�d
BOOL canWeCalculateThisProp_cached(int status, bool refresh = false);
BOOL canWeCalculateThisProp(int status);
void getPropActionStatusBkgndColor(int status,short* red,short* green,short* blue);

/////////////////////////////////////////////////////////////////////////////
// CELVObjectCruiseReportRec

class CELVObjectCruiseReportRec : public CXTPReportRecord
{
	//private:
	int nTraktID;
	int nPropID;
	int nObjID;
	int nTypeIndex;
	CTransaction_elv_cruise recELVCruise;
protected:

	
public:
	/*
	CELVObjectCruiseReportRec(CStringArray &stand,LPCTSTR def_yn = _T(""))
	{
		nPropID = -1;
		nObjID = -1;
		nTypeIndex = 1;
		if (stand.GetCount() > 1)
		{
			// Settings for manually entered stand; 090907 p�d
			AddItem(new CExConstraintTextItem((stand.GetAt(1))));
		}
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExIconItem(_T(""),8));
		AddItem(new CExIntItem(0));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExTextH100Item(_T(""),-1));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		//HMS-50 2020-02-11 J� ??
		//AddItem(new CExIconItem(0.0,0,5));

		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
//		AddItem(new CExTextItem((def_yn)));
		AddItem(new CExIntItem(0));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
//		AddItem(new CExIconItem(_T(""),4));
		AddItem(new CTextItem(_T("")));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExCheckItem(FALSE));	// Default, NOT CHECKED = use for Infringe

		// Settings for manually entered stand; 090907 p�d
		// "Typ av best�nd"
		this->GetItem(COLUMN_0)->SetEditable(FALSE);	
		this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
		// "Best�ndsnummer"
		this->GetItem(COLUMN_1)->SetEditable(TRUE);	
		this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
		// "Best�ndsnamn"
		this->GetItem(COLUMN_2)->SetEditable(TRUE);	
		this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
		// "Antal tr�d"
		this->GetItem(COLUMN_3)->SetEditable(FALSE);	
		this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
		// "Areal"
		this->GetItem(COLUMN_4)->SetEditable(FALSE);	
		this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
		// "SI"
		this->GetItem(COLUMN_5)->SetEditable(TRUE);	
		this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
		// "Volym"
		this->GetItem(COLUMN_6)->SetEditable(TRUE);	
		this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
		// "Virkesv�rde"
		this->GetItem(COLUMN_7)->SetEditable(FALSE);	
		this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
		// "Kostnader"
		this->GetItem(COLUMN_8)->SetEditable(FALSE);	
		this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
		// "Rotnetto"
		this->GetItem(COLUMN_9)->SetEditable(FALSE);	
		this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
		// "Volym SoT"
		this->GetItem(COLUMN_10)->SetEditable(FALSE);	
		this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
		// "V�rde SoT"
		this->GetItem(COLUMN_11)->SetEditable(FALSE);	
		this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
		// "Summa volym kanttr�d"
		this->GetItem(COLUMN_12)->SetEditable(FALSE);	
		this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
		// "Summa v�rde kanttr�d"
		this->GetItem(COLUMN_13)->SetEditable(FALSE);	
		this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
		// "Sida 1 el. 2, breddning"
		this->GetItem(COLUMN_14)->SetEditable(FALSE);	
		this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
		// "Annan bredd"
		this->GetItem(COLUMN_15)->SetEditable(FALSE);	
		this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
		// "Gran-inblandning"
		this->GetItem(COLUMN_16)->SetEditable(FALSE);	
		this->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
		// "TGL manuellt inl�st"
		this->GetItem(COLUMN_17)->SetEditable(FALSE);	
		this->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
		// "Virkesv�rde/m3fub"
		this->GetItem(COLUMN_18)->SetEditable(FALSE);	
		this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
		// "Grot (ton)"
		this->GetItem(COLUMN_19)->SetEditable(FALSE);	
		this->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
		// "Grot v�rde (kr)"
		this->GetItem(COLUMN_20)->SetEditable(FALSE);	
		this->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
		// "Grot kostnad (kr)"
		this->GetItem(COLUMN_21)->SetEditable(FALSE);	
		this->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
	}
	*/


	CELVObjectCruiseReportRec(int prop_status,CStringArray &stand,CString best_num,CString best_namn,CString bonitet,double areal,double volym,int side,CString tgl)
	{
		nPropID = -1;
		nObjID = -1;
		nTypeIndex = 1;
		if (stand.GetCount() > 1)
		{
			// Settings for manually entered stand; 090907 p�d
			AddItem(new CExConstraintTextItem((stand.GetAt(1))));
		}
		AddItem(new CExTextItem(best_num));
		AddItem(new CExIconItem(best_namn,8));
		AddItem(new CExIntItem(0));
		AddItem(new CExFloatItem(areal,(sz3dec)));
		AddItem(new CExFloatItem(volym,(sz2dec)));
		AddItem(new CExTextH100Item(bonitet,-1));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		
		//AddItem(new CExFloatItem(0.0,(sz0dec)));
		//HMS-50 2020-02-11 J�
		AddItem(new CExIconItem(0.0,0,5));

		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
//		AddItem(new CExTextItem((def_yn)));
		AddItem(new CExIntItem(side));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
//		AddItem(new CExIconItem(_T(""),4));
		AddItem(new CTextItem(tgl));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExFloatItem(0.0,(sz2dec)));
		AddItem(new CExCheckItem(FALSE));	// Default, NOT CHECKED = use for Infringe
		if (canWeCalculateThisProp_cached(prop_status))
		{
				// "Typ av best�nd"
				this->GetItem(COLUMN_0)->SetEditable(FALSE); this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
				// "Best�ndsnummer"
				this->GetItem(COLUMN_1)->SetEditable(FALSE); this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
				// "Best�ndsnamn"
				this->GetItem(COLUMN_2)->SetEditable(FALSE); this->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);
				// "Antal tr�d"
				this->GetItem(COLUMN_3)->SetEditable(FALSE); this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
				// "Areal"
				this->GetItem(COLUMN_4)->SetEditable(FALSE); this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
				// "Volym"
				this->GetItem(COLUMN_5)->SetEditable(FALSE); this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
				// "SI"
				this->GetItem(COLUMN_6)->SetEditable(FALSE); this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
				// "Virkesv�rde"
				this->GetItem(COLUMN_7)->SetEditable(FALSE); this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
				// "Kostnader"
				this->GetItem(COLUMN_8)->SetEditable(FALSE); this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
				// "Rotnetto"
				this->GetItem(COLUMN_9)->SetEditable(FALSE); this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
				// "Volym SoT"
				this->GetItem(COLUMN_10)->SetEditable(FALSE); this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
				// "V�rde SoT"
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
				// "Summa volym kanttr�d"
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
				// "Summa v�rde kanttr�d"
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
				// "Sida 1 el. 2, breddning"
				this->GetItem(COLUMN_14)->SetEditable(FALSE);	this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
				// "Annan bredd"
				this->GetItem(COLUMN_15)->SetEditable(TRUE); this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);
				// "Gran-inblandning"
				this->GetItem(COLUMN_16)->SetEditable(FALSE); this->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
				// "TGL manuellt inl�st"
				this->GetItem(COLUMN_17)->SetEditable(FALSE);	this->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
				// "Virkesv�rde/m3fub"
				this->GetItem(COLUMN_18)->SetEditable(FALSE);	this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
				// "Grot (ton)"
				this->GetItem(COLUMN_19)->SetEditable(FALSE);	this->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
				// "Grot v�rde (kr)"
				this->GetItem(COLUMN_20)->SetEditable(FALSE);	this->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
				// "Grot kostnad (kr)"
				this->GetItem(COLUMN_21)->SetEditable(FALSE);	this->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
				// "Bara virkesv�rde"
				this->GetItem(COLUMN_22)->SetEditable(TRUE);	
				this->GetItem(COLUMN_22)->SetBackgroundColor(WHITE);
		}
		else
		{
			this->GetItem(COLUMN_0)->SetEditable(FALSE);this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_1)->SetEditable(FALSE);	this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_2)->SetEditable(FALSE);	this->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_3)->SetEditable(FALSE);	this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_4)->SetEditable(FALSE);	this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_5)->SetEditable(FALSE);	this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_6)->SetEditable(FALSE);	this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_7)->SetEditable(FALSE);	this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_8)->SetEditable(FALSE);	this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_9)->SetEditable(FALSE);	this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_10)->SetEditable(FALSE);	this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_11)->SetEditable(FALSE);	this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_12)->SetEditable(FALSE);	this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_13)->SetEditable(FALSE);	this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_14)->SetEditable(FALSE);	this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_15)->SetEditable(FALSE);	this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_16)->SetEditable(FALSE);	this->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_17)->SetEditable(FALSE);	this->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_18)->SetEditable(FALSE);	this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_19)->SetEditable(FALSE);	this->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_20)->SetEditable(FALSE);	this->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_21)->SetEditable(FALSE);	this->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_22)->SetEditable(FALSE);	
			this->GetItem(COLUMN_22)->SetBackgroundColor(LTCYAN);
		}

	}


	CELVObjectCruiseReportRec(int trakt_id,int prop_id,int obj_id,short prop_status,short norm_type,CStringArray &stand,CStringArray &s,CTransaction_elv_cruise& rec,LPCTSTR msg)
	{
		recELVCruise = rec;
		nTraktID = trakt_id;
		nPropID = prop_id;
		nObjID = obj_id;
		nTypeIndex = rec.getECruType();
		if (stand.GetCount() > 0)
		{
			if (nTypeIndex >= 0 && nTypeIndex < stand.GetCount())
				AddItem(new CExConstraintTextItem((stand.GetAt(nTypeIndex))));
		}
		AddItem(new CExTextItem((rec.getECruNumber())));
		if (nTypeIndex == CRUISE_TYPE_1 && canWeCalculateThisProp_cached(prop_status))
			AddItem(new CExIconItem(rec.getECruName(),3));
		else
			AddItem(new CExIconItem(rec.getECruName(),3));
		
		AddItem(new CExIntItem(rec.getECruNumOfTrees()));
		AddItem(new CExFloatItem(rec.getECruAreal(),(sz3dec)));
		AddItem(new CExFloatItem(rec.getECruM3Sk(),(sz2dec)));
		AddItem(new CExTextH100Item(rec.getECruSI(),rec.getECruUseForInfr()));
		AddItem(new CExFloatItem(rec.getECruTimberValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getECruTimberCost(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getECruNetto(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getECruStormDryVol(),(sz2dec)));

		//AddItem(new CExFloatItem(rec.getECruStormDryValue(),(sz0dec)));
		//HMS-50 2020-02-11 J�
		AddItem(new CExIconItem(rec.getECruStormDryValue(),0,5));

		AddItem(new CExFloatItem(rec.getECruRandTreesVol(),(sz2dec)));
		AddItem(new CExFloatItem(rec.getECruRandTreesValue(),(sz2dec)));
		//AddItem(new CExTextItem((s.GetAt(rec.getECruDoReduceRotpost()))));
		if (rec.getECruUseSampleTrees() > 0)
			AddItem(new CExIntItem(rec.getECruUseSampleTrees()));
		else
			AddItem(new CExIntItem(1));

		AddItem(new CExFloatItem(rec.getECruWidth(),(sz1dec)));
		if (rec.gteECruStormDrySpruceMix() < 1.0)
			AddItem(new CExFloatItem(rec.gteECruStormDrySpruceMix()*100.0,(sz1dec)));
		else
			AddItem(new CExFloatItem(rec.gteECruStormDrySpruceMix(),(sz1dec)));
/*	Commanted out 2012-12-05 P�D #3380
		if (nTypeIndex == CRUISE_TYPE_2)
			AddItem(new CExIconItem(rec.getECruTGL(),4));
		else
			AddItem(new CExIconItem(_T(""),8));
*/
		// Added 2012-12-05 P�D #3380
		AddItem(new CTextItem(rec.getECruTGL()));
		
		if (rec.getECruTimberValue() > 0.0 && rec.getECruTimberVol() > 0.0)
			AddItem(new CExFloatItem(rec.getECruTimberValue()/rec.getECruTimberVol(),(sz0dec)));
		else
			AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(rec.getECruGrotVolume(),(sz3dec)));
		AddItem(new CExFloatItem(rec.getECruGrotValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getECruGrotCost(),(sz0dec)));
		AddItem(new CExCheckItem( (rec.getECruUseForInfr() == 0 ? FALSE:TRUE)  ));

		if (canWeCalculateThisProp_cached(prop_status))
		{
			// Added 2009-06-26 P�D
			if (rec.getECruType() == 0 || rec.getECruType() == 1)	// "Taxerat bes�nd fr�n t.ex. klave" eller "manuallt inl�st"
			{
				// "Typ av best�nd"
				this->GetItem(COLUMN_0)->SetEditable(FALSE); this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
				// "Best�ndsnummer"
				this->GetItem(COLUMN_1)->SetEditable(FALSE); this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
				// "Best�ndsnamn"
				this->GetItem(COLUMN_2)->SetEditable(FALSE); this->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);
				// "Antal tr�d"
				this->GetItem(COLUMN_3)->SetEditable(FALSE); this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
				// "Areal"
				this->GetItem(COLUMN_4)->SetEditable(FALSE); this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
				// "Volym"
				this->GetItem(COLUMN_5)->SetEditable(FALSE); this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
				// "SI"
				this->GetItem(COLUMN_6)->SetEditable(FALSE); this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
				// "Virkesv�rde"
				this->GetItem(COLUMN_7)->SetEditable(FALSE); this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
				// "Kostnader"
				this->GetItem(COLUMN_8)->SetEditable(FALSE); this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
				// "Rotnetto"
				this->GetItem(COLUMN_9)->SetEditable(FALSE); this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
				// "Volym SoT"
				this->GetItem(COLUMN_10)->SetEditable(FALSE); this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
				// "V�rde SoT"
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
				// "Summa volym kanttr�d"
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
				// "Summa v�rde kanttr�d"
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
				// "Sida 1 el. 2, breddning"
				this->GetItem(COLUMN_14)->SetEditable(FALSE);	this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
				// "Annan bredd"
				this->GetItem(COLUMN_15)->SetEditable(TRUE); this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);
				// "Gran-inblandning"
				this->GetItem(COLUMN_16)->SetEditable(FALSE); this->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
				// "TGL manuellt inl�st"
				this->GetItem(COLUMN_17)->SetEditable(FALSE);	this->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
				// "Virkesv�rde/m3fub"
				this->GetItem(COLUMN_18)->SetEditable(FALSE);	this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
				// "Grot (ton)"
				this->GetItem(COLUMN_19)->SetEditable(FALSE);	this->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
				// "Grot v�rde (kr)"
				this->GetItem(COLUMN_20)->SetEditable(FALSE);	this->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
				// "Grot kostnad (kr)"
				this->GetItem(COLUMN_21)->SetEditable(FALSE);	this->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
				// "Bara virkesv�rde"
				if (rec.getECruType() == 0 )
				{
					this->GetItem(COLUMN_22)->SetEditable(TRUE);	
					this->GetItem(COLUMN_22)->SetBackgroundColor(WHITE);
				}
				else if (rec.getECruType() == 1 )
				{
					this->GetItem(COLUMN_22)->SetEditable(FALSE);	
					this->GetItem(COLUMN_22)->SetBackgroundColor(LTCYAN);
				}
			}
/*
			else if (rec.getECruType() == 1)	// "Manuellt inl�st"
			{
				// "Typ av best�nd"
				this->GetItem(COLUMN_0)->SetEditable(FALSE);this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
				// "Best�ndsnummer"
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				// "Best�ndsnamn"
				this->GetItem(COLUMN_2)->SetEditable(TRUE);	this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				// "Antal tr�d"
				this->GetItem(COLUMN_3)->SetEditable(FALSE); this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
				// "Areal"
				this->GetItem(COLUMN_4)->SetEditable(FALSE); this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
				// "SI"
				this->GetItem(COLUMN_5)->SetEditable(TRUE);	this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
				// "Volym"
				this->GetItem(COLUMN_6)->SetEditable(TRUE);	this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
				// "Virkesv�rde"
				this->GetItem(COLUMN_7)->SetEditable(FALSE); this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
				// "Kostnader"
				this->GetItem(COLUMN_8)->SetEditable(FALSE); this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
				// "Rotnetto"
				this->GetItem(COLUMN_9)->SetEditable(FALSE); this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
				// "Volym SoT"
				this->GetItem(COLUMN_10)->SetEditable(FALSE);	this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
				// "V�rde SoT"
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
				// "Summa volym kanttr�d"
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
				// "Summa v�rde kanttr�d"
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
				// "Sida 1 el. 2, breddning"
				this->GetItem(COLUMN_14)->SetEditable(TRUE); this->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
				// "Annan bredd"
				this->GetItem(COLUMN_15)->SetEditable(TRUE); this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);
				// "Gran-inblandning"
				this->GetItem(COLUMN_16)->SetEditable(FALSE); this->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
				// "TGL manuellt inl�st"
				this->GetItem(COLUMN_17)->SetEditable(FALSE);	this->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
				// "Virkesv�rde/m3fub"
				this->GetItem(COLUMN_18)->SetEditable(FALSE);	this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
				// "Grot (ton)"
				this->GetItem(COLUMN_19)->SetEditable(FALSE);	this->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
				// "Grot v�rde (kr)"
				this->GetItem(COLUMN_20)->SetEditable(FALSE);	this->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
				// "Grot kostnad (kr)"
				this->GetItem(COLUMN_21)->SetEditable(FALSE);	this->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
				// "Bara virkesv�rde"
				this->GetItem(COLUMN_22)->SetEditable(FALSE);	this->GetItem(COLUMN_22)->SetBackgroundColor(LTCYAN);
			}
*/
		}
		else
		{
			this->GetItem(COLUMN_0)->SetEditable(FALSE);this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_1)->SetEditable(FALSE);	this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_2)->SetEditable(FALSE);	this->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_3)->SetEditable(FALSE);	this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_4)->SetEditable(FALSE);	this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_5)->SetEditable(FALSE);	this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_6)->SetEditable(FALSE);	this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_7)->SetEditable(FALSE);	this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_8)->SetEditable(FALSE);	this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_9)->SetEditable(FALSE);	this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_10)->SetEditable(FALSE);	this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_11)->SetEditable(FALSE);	this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_12)->SetEditable(FALSE);	this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_13)->SetEditable(FALSE);	this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_14)->SetEditable(FALSE);	this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_15)->SetEditable(FALSE);	this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_16)->SetEditable(FALSE);	this->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_17)->SetEditable(FALSE);	this->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_18)->SetEditable(FALSE);	this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_19)->SetEditable(FALSE);	this->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_20)->SetEditable(FALSE);	this->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_21)->SetEditable(FALSE);	this->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_22)->SetEditable(FALSE);	
			this->GetItem(COLUMN_22)->SetBackgroundColor(LTCYAN);
		}
	}

	CTransaction_elv_cruise& getRecELVCruise(void)	{	return recELVCruise;	}

	double getColumnFloat(int item)	{ return ((CExFloatItem*)GetItem(item))->getFloatItem(); }

	void setColumnFloat(int item,double value) { ((CExFloatItem*)GetItem(item))->setFloatItem(value);	}

	int getColumnInt(int item)	{ return ((CExIntItem*)GetItem(item))->getIntItem(); }

	void setColumnInt(int item,int value)	{ ((CExIntItem*)GetItem(item))->setIntItem(value);	}

	CString getColumnText(int item)	{ return ((CExTextItem*)GetItem(item))->getTextItem();	}

	void setColumnText(int item,LPCTSTR value) { ((CExTextItem*)GetItem(item))->setTextItem(value);	}

	BOOL getColumnCheck(int item)	{	return ((CExCheckItem*)GetItem(item))->getChecked();	}

	void setColumnCheck(int item,BOOL bChecked)	{	((CExCheckItem*)GetItem(item))->setChecked(bChecked);	}

	CString getColumnIconText(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconText();	}
	
	void setColumnIconText(int item,LPCTSTR value) { 	return ((CExIconItem*)GetItem(item))->setExIconText(value);	}

	int getTypeIndex()	{ return nTypeIndex; }

	
    double getColumnIconFloat(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconFloat();	}
};

/////////////////////////////////////////////////////////////////////////////
// CELVObjectEvaluatedReportRec
#define MIN_VALUE_FOR_CORRFACTOR	0.15
#define MAX_VALUE_FOR_CORRFACTOR	1.40
class CELVObjectEvaluatedReportRec : public CXTPReportRecord
{
	//private:
	int nEvalID;
	int nPropID;
	int nObjID;
	CTransaction_eval_evaluation recELVEvaluated;
protected:

	friend CExTextItem;
	
public:
	CELVObjectEvaluatedReportRec(CStringArray &vstand)
	{
		nPropID = -1;
		nObjID = -1;
		recELVEvaluated = CTransaction_eval_evaluation();
		if (vstand.GetCount() > 0)
		{
			AddItem(new CExConstraintTextItem((vstand.GetAt(0))));
		}
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExIconItem(0.0,3,4));
		AddItem(new CExIntItem(0));
		AddItem(new CExTextH100Item(_T(""),-1));
		AddItem(new CExIconItemEx(0.0,2,4,MIN_VALUE_FOR_CORRFACTOR,MAX_VALUE_FOR_CORRFACTOR));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExTextItem(_T("")));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExFloatItem(0.0,(sz0dec)));	//Lagt till volym m3sk och volym m3sk/ha #3379
		AddItem(new CExFloatItem(0.0,(sz0dec)));
		AddItem(new CExIntItem(0));	//L�ngd #4207 150113 J�
		AddItem(new CExIntItem(0));	//Bredd #4207 150113 J�
		AddItem(new CExIntItem(1));	//Skikt #5086 160819 PL
		//AddItem(new CExCheckItem(0)); //�verlappande mark #4829 PL
		AddItem(new CExTextItem(_T("")));	// HMS-41 �ndrat till reducerad mark som kan vara inget/�verlappande/tillf�lligt utnyttjande
		AddItem(new CExIntItem(1)); //Sida

		// Default setting on i.e. New item; 090402 p�d
		this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
		this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
		this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
		this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
		this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_4)->SetEditable(TRUE);	// "SI"
		this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_5)->SetEditable(TRUE);	// "Korrektionsfaktor"
		this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_6)->SetEditable(TRUE);	// "Andel Tall %"
		this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_7)->SetEditable(TRUE);	// "Andel Gran %"
		this->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_8)->SetEditable(TRUE);	// "Andel L�v (Bj�rk) %"
		this->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
		this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
		this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
		this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
		this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
		this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_14)->SetEditable(TRUE);	// "Volym m3sk"
		this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
		this->GetItem(COLUMN_15)->SetEditable(TRUE);	// "Volym m3sk/ha"
		this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);

		this->GetItem(COLUMN_16)->SetEditable(TRUE);			//L�ngd #4207 150113 J�
		this->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_17)->SetEditable(TRUE);			//Bredd #4207 150113 J�
		this->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
		this->GetItem(COLUMN_18)->SetEditable(FALSE);			//Skikt #5086 160819 PL
		this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);

		this->GetItem(COLUMN_19)->SetEditable(FALSE);			//�verlappande mark #4829 PL
		this->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
	}

	//CELVObjectEvaluatedReportRec(int eval_id,int obj_id,int prop_id,short norm_type,CStringArray &vstand,CStringArray &land,short prop_status,CTransaction_eval_evaluation& rec);
	CELVObjectEvaluatedReportRec(int eval_id,int obj_id,int prop_id,short norm_type,CStringArray &vstand,CStringArray &land,short prop_status,CTransaction_eval_evaluation& rec,CStringArray &strArrReducMark);
	
	/*{
		CString S;
		double fCorrFactor = rec.getEValCorrFactor();
		double fPinePerc = rec.getEValPinePart();
		double fSprucePerc = rec.getEValSprucePart();
		double fBirchPerc = rec.getEValBirchPart();
		recELVEvaluated = rec;
		nEvalID = eval_id;
		nPropID = prop_id;
		nObjID = obj_id;
		int nForrestIndex = rec.getEValForrestType();
		int nTypeIndex = rec.getEValType();
		if (vstand.GetCount() > 0)
		{
			if (nTypeIndex >= 0 && nTypeIndex < vstand.GetCount())
				AddItem(new CExConstraintTextItem((vstand.GetAt(nTypeIndex))));
			//else
			//AddItem(new CExConstraintTextItem((vstand.GetAt(0))));
		}
		AddItem(new CExTextItem((rec.getEValName())));
		//Arealen tas fr�n det taxerade best�ndet s� s�tt ingen ikon vid arealen vid v�rdering fr�n taxering
		//20120126 J� Bug #2790
		if(nTypeIndex == EVAL_TYPE_2)
			AddItem(new CExIconItem(rec.getEValAreal(),3,-1));
		else
			AddItem(new CExIconItem(rec.getEValAreal(),3,4));
		AddItem(new CExIntItem(rec.getEValAge()));
		AddItem(new CExTextH100Item(rec.getEValSIH100(),norm_type));
		if (nTypeIndex == EVAL_TYPE_1)
			AddItem(new CExIconItemEx(((fCorrFactor < 0) ? fCorrFactor*(-1.0):fCorrFactor),2,4,MIN_VALUE_FOR_CORRFACTOR,MAX_VALUE_FOR_CORRFACTOR));
		else
			AddItem(new CExIconItemEx(((fCorrFactor < 0) ? fCorrFactor*(-1.0):fCorrFactor),2,8,MIN_VALUE_FOR_CORRFACTOR,MAX_VALUE_FOR_CORRFACTOR));
		AddItem(new CExFloatItem(((fPinePerc < 0) ? fPinePerc*(-1.0):fPinePerc),(sz0dec)));
		AddItem(new CExFloatItem(((fSprucePerc < 0) ? fSprucePerc*(-1.0):fSprucePerc),(sz0dec)));
		AddItem(new CExFloatItem(((fBirchPerc < 0) ? fBirchPerc*(-1.0):fBirchPerc),(sz0dec)));
		if (land.GetCount() > 0)
		{
			if (nForrestIndex > 0 && nForrestIndex < land.GetCount())
				AddItem(new CExTextItem((land.GetAt(nForrestIndex))));
			else
				AddItem(new CExTextItem((land.GetAt(0))));
		}

		AddItem(new CExFloatItem(rec.getEValLandValue(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getEValPreCut(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getEValLandValue_ha(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getEValPreCut_ha(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getEValVolume(),(sz2dec)));

		//Lagt till volym m3sk och m3sk/ha 20121204 J� #3379
		AddItem(new CExFloatItem(rec.getEValVolume(),(sz2dec)));
		if(rec.getEValAreal()>0.0)
			AddItem(new CExFloatItem(rec.getEValVolume()/rec.getEValAreal(),(sz2dec)));
		else
			AddItem(new CExFloatItem(0.0,(sz2dec)));


		// Try to set Editbale or not per column here.
		// Depending on nTypeIndex; 090402 p�d

		//if (prop_status <= STATUS_ADVICED)
		if (canWeCalculateThisProp_cached(prop_status))
		{
			if (nTypeIndex == EVAL_TYPE_1)	// "V�rderingsbest�nd"
			{
				this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
				this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
				this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
				this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_4)->SetEditable(TRUE);	// "SI"
				this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_5)->SetEditable(TRUE);	// "Korrektionsfaktor"
				this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_6)->SetEditable(TRUE);	// "Andel Tall %"
				this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_7)->SetEditable(TRUE);	// "Andel Gran %"
				this->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_8)->SetEditable(TRUE);	// "Andel L�v (Bj�rk) %"
				this->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
				this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
				this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
				this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
				this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
				this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_14)->SetEditable(TRUE);	// "Volym m3sk"
				this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_15)->SetEditable(TRUE);	// "Volym m3sk/ha"
				this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
			}
			else if (nTypeIndex == EVAL_TYPE_2)	// "Taxeringsbest�nd"
			{
				this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
				this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
				this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				//Arealen tas fr�n det taxerade best�ndet s� s�tt arealen som ej editerbar 
				//20120126 J� Bug #2790
				//this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_3)->SetEditable(FALSE);	// "�lder"
				this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
				this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
				this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
				this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
				this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
				this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
				this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
				this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
				this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
				this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
				this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk"
				this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha"
				this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
			}
			else if (nTypeIndex == EVAL_TYPE_3)	// "Annan mark, ex. �kermark"
			{
				this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
				this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
				this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
				this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
				this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
				this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
				this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
				this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
				this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
				this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_10)->SetEditable(TRUE);	// "Markv�rde kr"
				this->GetItem(COLUMN_10)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
				this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_12)->SetEditable(TRUE);	// "Markv�rde kr/ha"
				this->GetItem(COLUMN_12)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
				this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk"
				this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha"
				this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
			}
		}	// if (prop_status <= STATUS_ADVICED)
		else
		{
			this->GetItem(COLUMN_0)->SetEditable(FALSE);	// "Typ av v�rdering"
			this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_1)->SetEditable(FALSE);	// "Namn"
			this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
			this->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_3)->SetEditable(FALSE);	// "�lder"
			this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
			this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
			this->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
			this->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
			this->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
			this->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_9)->SetEditable(FALSE);	// "Typ av skog: Skogsmark,Kalmark etc."
			this->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
			this->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
			this->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
			this->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
			this->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk"
			this->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha"
			this->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
		}

	}*/

	CTransaction_eval_evaluation& getRecELVEvaluated(void)	{		return recELVEvaluated;	}

	double getColumnFloat(int item)	{ return ((CExFloatItem*)GetItem(item))->getFloatItem();	}
	void setColumnFloat(int item,double value) { ((CExFloatItem*)GetItem(item))->setFloatItem(value);	}

	double getIconColumnFloat(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconFloat(); }
	void setIconColumnFloat(int item,double value)	{ ((CExIconItem*)GetItem(item))->setExIconFloat(value); }

	CString getIconColumnText(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconText(); }
	void setIconColumnText(int item,LPCTSTR value)	{ ((CExIconItem*)GetItem(item))->setExIconText(value); }

	double getColumnInt(int item)	{ return ((CExIntItem*)GetItem(item))->getIntItem();	}
	void setColumnInt(int item,int value) { ((CExIntItem*)GetItem(item))->setIntItem(value);	}

	BOOL getColumnCheck(int item)	{ return ((CExCheckItem*)GetItem(item))->getChecked();	}
	void setColumnCheck(int item,BOOL bChecked)	{ ((CExCheckItem*)GetItem(item))->setChecked(bChecked);	}

	CString getColumnText(int item)	{ return ((CExTextItem*)GetItem(item))->getTextItem();	}
	void setColumnText(int item,LPCTSTR value)	{ ((CExTextItem*)GetItem(item))->setTextItem(value);	}

	int getColumnTextConstraintIndex(int item)	{ return ((CExConstraintTextItem*)GetItem(item))->getConstraintIndex();	}

};


/////////////////////////////////////////////////////////////////////////////
// CELVObjectPropStatusRec

class CELVObjectPropStatusRec : public CXTPReportRecord
{
	//private:
	CTransaction_property_status recPropStatus;
protected:
	
public:
	CELVObjectPropStatusRec(short order_num, short id_num = -1)
	{
		recPropStatus = CTransaction_property_status();
		recPropStatus.setPropStatusID_pk(id_num);
		AddItem(new CExCheckItem(FALSE));	// Default, no calculation
		AddItem(new CExIntItem(order_num));
		AddItem(new CExIconItem(_T(""),9));
		AddItem(new CExIconItem(_T(""),11));
	}

	CELVObjectPropStatusRec(CTransaction_property_status& rec,BOOL is_used)
	{
		recPropStatus = rec; 
		AddItem(new CExCheckItem(rec.getPropStatusType() == 1));	// 1 = Can recalculate
		AddItem(new CExIntItem(rec.getPropStatusOrderNum()));
		AddItem(new CExIconItem(recPropStatus.getPropStatusName(),9));
		if (is_used) AddItem(new CExIconItem(_T(""),12));
		else AddItem(new CExIconItem(_T(""),11));

		this->GetItem(COLUMN_2)->SetBackgroundColor(RGB(recPropStatus.getPropStatusRCol(),
																										recPropStatus.getPropStatusGCol(),
																										recPropStatus.getPropStatusBCol()));
	}

	CTransaction_property_status& getRecord(void)	{		return recPropStatus;	}

	BOOL getColumnCheck(int item)	{		return ((CExCheckItem*)GetItem(item))->getChecked();	}

	int getColumnInt(int item)	{ return ((CExIntItem*)GetItem(item))->getIntItem();	}

	CString getIconColumnText(int item)	{ return ((CExIconItem*)GetItem(item))->getExIconText(); }

	CString getColumnText(int item)	{ return ((CExTextItem*)GetItem(item))->getTextItem();	}
	void setColumnText(int item,LPCTSTR value)	{ ((CExIconItem*)GetItem(item))->setTextItem(value);	}

	//////////////////////////////////////////////////////////////////////////////////////////
	void setColumnBgndColor(int column)	
	{ 
		this->GetItem(column)->SetBackgroundColor(RGB(recPropStatus.getPropStatusRCol(),recPropStatus.getPropStatusGCol(),recPropStatus.getPropStatusBCol()));
	}
	void setColumnBgndColor(int column,COLORREF color)	
	{ 
		recPropStatus.setPropStatusRCol(GetRValue(color));
		recPropStatus.setPropStatusGCol(GetGValue(color));
		recPropStatus.setPropStatusBCol(GetBValue(color));
		this->GetItem(column)->SetBackgroundColor(RGB(recPropStatus.getPropStatusRCol(),recPropStatus.getPropStatusGCol(),recPropStatus.getPropStatusBCol()));
	}

};



/////////////////////////////////////////////////////////////////////////////
// CELVObjectSumRec

class CELVObjectSumRec : public CXTPReportRecord
{
	//private:
protected:

	class CExtTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		CFont m_fntSmall;
		CFont m_fntSmallUnderline;
		CFont m_fntSmallBold;
		CFont m_fnt;
		CFont m_fntBold;
		CFont m_fntHeader;
	public:
		CExtTextItem(CString sValue) : 
				CXTPReportRecordItemText(sValue)
		{
			m_fntSmall.CreateFont(14, 0, 0, 0, FW_NORMAL,
													 FALSE, FALSE, FALSE, 0, 
													 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
													 _T("Courier New"));
			m_fntSmallUnderline.CreateFont(14, 0, 0, 0, FW_NORMAL,
													 FALSE, TRUE, FALSE, 0, 
													 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
													 _T("Courier New"));
			m_fntSmallBold.CreateFont(14, 0, 0, 0, FW_BOLD,
																FALSE, FALSE, FALSE, 0, 
																OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
																_T("Courier New"));
			m_fnt.CreateFont(17, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_fntBold.CreateFont(17, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_fntHeader.CreateFont(20, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_sText = sValue;
		}
		virtual ~CExtTextItem(void)
		{
			m_fntSmall.DeleteObject();
			m_fntSmallUnderline.DeleteObject();
			m_fntSmallBold.DeleteObject();
			m_fnt.DeleteObject();
			m_fntBold.DeleteObject();
			m_fntHeader.DeleteObject();
		}

		// Set "normal" font
		void setFontSmall(void)	{ SetFont(&m_fntSmall);	}
		void setFontSmallUnderline(void)	{ SetFont(&m_fntSmallUnderline);	}
		void setFontSmallBold(void)	{ SetFont(&m_fntSmallBold);	}
		void setFont(void)	{ SetFont(&m_fnt);	}
		void setFontBold(void)	{ SetFont(&m_fntBold);	}
		void setFontHeader(void)	{ SetFont(&m_fntHeader);	}
	};
	
public:
	CELVObjectSumRec(void)
	{
		AddItem(new CExtTextItem(_T("")));
	}

	CELVObjectSumRec(LPCTSTR text)
	{
		AddItem(new CExtTextItem((text)));
	}

	void setColumnFont(int fnt_num,int column = 0)	
	{ 
		if (fnt_num == FNT_SMALL)	// Normal font
			((CExtTextItem*)GetItem(column))->setFontSmall();
		else if (fnt_num == FNT_SMALL_UNDERLINE)	// Normal font
			((CExtTextItem*)GetItem(column))->setFontSmallUnderline();
		else if (fnt_num == FNT_SMALL_BOLD)	// Normal font
			((CExtTextItem*)GetItem(column))->setFontSmallBold();
		else if (fnt_num == FNT_NORMAL)	// Normal font
			((CExtTextItem*)GetItem(column))->setFont();
		else if (fnt_num == FNT_NORMAL_BOLD)	// Bold font
			((CExtTextItem*)GetItem(column))->setFontBold();
		else if (fnt_num == FNT_HEADER)	// Header font
			((CExtTextItem*)GetItem(column))->setFontHeader();
	}

};

/////////////////////////////////////////////////////////////////////////////
// CPropOwnerReportRec

class CPropOwnerReportRec : public CXTPReportRecord
{
	int m_nContactID;
protected:

public:

	CPropOwnerReportRec(void)
	{
		m_nContactID = -1;
		AddItem(new CExCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		//Add icon with name to be able to make a shortcut to the contact form J� 20010804 feature 2263
		AddItem(new CExIconItem(_T(""),3));
		//AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CPropOwnerReportRec(int contact_id,BOOL check,LPCTSTR share,LPCTSTR name,LPCTSTR address,LPCTSTR post_address,LPCTSTR phone_work,LPCTSTR phone_home,LPCTSTR cell_phone)
	{
		m_nContactID = contact_id;
		AddItem(new CExCheckItem(check));
		AddItem(new CTextItem(share));
		//Added icon to name column for feature 2263
		AddItem(new CExIconItem((name),3));
		//AddItem(new CTextItem(name));
		AddItem(new CTextItem(address));
		AddItem(new CTextItem(post_address));
		AddItem(new CTextItem(phone_work));
		AddItem(new CTextItem(phone_home));
		AddItem(new CTextItem(cell_phone));
	}

	int getCOntactID(void)	{ return m_nContactID; }

	BOOL getColumnCheck(int item)	{	return ((CExCheckItem*)GetItem(item))->getChecked();	}

	void setColumnCheck(int item,BOOL bChecked)	{	((CExCheckItem*)GetItem(item))->setChecked(bChecked);	}

	CString getColumnText(int item)	{ return ((CTextItem*)GetItem(item))->getTextItem();	}
	
	void setColumnText(int item,LPCTSTR text)	{ ((CTextItem*)GetItem(item))->setTextItem(text);	}

};


/////////////////////////////////////////////////////////////////////////////////////////////////
// "Stondortsindex f�r P30-priser, ny norm"; 090504 p�d
struct _si_from_to
{
	int from;
	int to;
};

/////////////////////////////////////////////////////////////////////////////////////////////////
// Added 2009-06-05 P�D
class Scripts
{
	CString _TableName;
	CString _Script;
	CString _DBName;
public:
	enum actionTypes { TBL_CREATE,TBL_ALTER };

	Scripts(void)
	{
		_TableName = _T("");
		_Script = _T("");
		_DBName = _T("");
	}

	Scripts(LPCTSTR table_name,LPCTSTR script,LPCTSTR db_name)
	{
		_TableName = table_name;
		_Script = script;
		_DBName = db_name;
	}

	CString getTableName()	{ return _TableName; }
	CString getScript()			{ return _Script; }
	CString getDBName()			{ return _DBName; }
};

typedef std::vector<Scripts> vecScriptFiles;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for treetype same as in UCCalculate; 070703 p�d
// OBS! Also in UMEstimate; 080117 p�d
#define SAMPLE_TREE						0		// A sampletree. I.e. it has a heigth etc
#define TREE_OUTSIDE					1		// "Intr�ngsv�rdering; kanttr�d Provtr�d"
#define TREE_OUTSIDE_NO_SAMP	2		// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
#define TREE_SAMPLE_REMAINIG	3		// "Provtr�d (Kvarl�mmnat)"
#define TREE_SAMPLE_EXTRA			4		// "Provtr�d (Extra inl�sta)"

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines type of "St�mplingsl�ngd"; 071121 p�d
// OBS! Also in UMEstimate; 080117 p�d
/*
const int STMP_LEN_WITHDRAW				=	1;		// "St�mplingsl�ngd - Uttag"
const int STMP_LEN_TO_BE_LEFT			=	2;		// "St�mplingsl�ngd - Kvarl�mmnat"
const int STMP_LEN_WITHDRAW_ROAD	= 3;		// "St�mplingsl�ngd - Uttag i Stickv�g"
*/
/////////////////////////////////////////////////////////////////////////////////////////////////
// Identifies that this is a pricelist used for calulation (utbyte)
// using Rune Ollas calculations; 071024 p�d
// OBS! Also in UMEstimate; 080117 p�d
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_1			1		// Pricelist for R. Ollars
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_2			2		// "Simple" avg. prices per assortment

/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated type for determin' kind of assortment; 070531 p�d
#define ASSORTMENT_TIMBER								0
#define ASSORTMENT_NOT_PULP_NOT_TMBER		1
#define ASSORTMENT_PULPWOOD							2

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for "Rotpost" origin
#define ROTPOST_ORIGIN1			1		// "Vanlig" rotpost
#define ROTPOST_ORIGIN2			2		// "Intr�ngsv�rdering" rotpost

/////////////////////////////////////////////////////////////////////////////////////////////////
// Status bar progress index; 080118 p�d
#define PROGRESS_CTRL_INDEX			2	// Position on Statusbar


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

CString getToolBarResourceFN(void);

CView *getFormViewByID(int idd);
CWnd *getFormViewParentByID(int idd);

BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,BOOL do_create_table,CString database_name);
BOOL runSQLScriptFileEx1(LPCTSTR table_name,LPCTSTR column_name,CString script,CString db_name);
// Added 2009-06-05 p�d
BOOL runSQLScriptFileEx2(vecScriptFiles &vec,Scripts::actionTypes action);


void setStatusBarText(int index,LPCTSTR text);
void setStatusBarProgress(int index);

void calculateTransfersByM3Fub(int spc_id,
															 LPCTSTR fromAssort,
															 LPCTSTR toAssort,
															 double m3fub,
															 CTransaction_trakt_data recTraktData,
															 vecTransactionTraktAss &vecTraktAss);


void calculateTransfersByPercent(int spc_id,
																 LPCTSTR fromAssort,
																 LPCTSTR toAssort,
																 double percent,
																 CTransaction_trakt_data recTraktData,
																 vecTransactionTraktAss &vecTraktAss);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp = -1, BOOL bShowinEstimate = FALSE);

// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
CTransaction_elv_object* getActiveObject(void);

CTransaction_elv_properties* getActiveProperty(void);

CString formatData(LPCTSTR,...);

int SplitString(const CString& input,const CString& delimiter, CStringArray& results);

int InsertRow(CListCtrl &ctrl,int nPos,int nNoOfCols, LPCTSTR pText,...);

int GetSelectedItem(CListCtrl &ctrl);
int GetSelectedItemByCB(CListCtrl &ctrl);

// Get the directory for Object , i.e. in format <searchpath>\\ObjectID; 080616 p�d
CString getObjectDirectory(LPCTSTR obj_mapp);
// Get the directory for Object, holding inventory data , i.e. in format <searchpath>\\ObjectID\\inv; 080616 p�d
CString getObjectInventoryDirectory(LPCTSTR obj_mapp);
// Get the directory for Object, holding setup-files etc., i.e. in format <searchpath>\\ObjectID\\setup; 080616 p�d
CString getObjectSetupDirectory(LPCTSTR obj_mapp);
// Get the directory for Object, holding setup-files etc., i.e. in format <searchpath>\\ObjectID\\setup; 080616 p�d
CString getObjectInfoDirectory(LPCTSTR obj_mapp);

BOOL doIsDirectory(LPCTSTR obj_mapp);
BOOL doCreateObjectDataDirectory(LPCTSTR obj_mapp,LPCTSTR sub_dir = _T(""));
BOOL doRenameObjectDataDirectory(LPCTSTR old_name,LPCTSTR new_name);

// Make sure the name of the directory doesn't
// contain any illegal characters; 080924 p�d
CString checkDirectoryName(LPCTSTR dir_name);

//BOOL hitTest_X(int hit_x,int limit_x,int w_x);

BOOL getSTDReports(LPCTSTR fn,LPCTSTR add_to,int index,vecSTDReports &vec);

// Static variable, check if user's opened a window (doesn't matter which).
// Only show "Demo - number of days"-message once.
static BOOL bShowLicenseDialog = TRUE;
BOOL License(void);

void setEditWindow(CMyExtEdit *ed,COLORREF disable_color,BOOL enabled);
void setEditWindow(CMyMaskExtEdit *ed,COLORREF disable_color,BOOL enabled);
void setEditWindowEx(CMyExtEdit *ed,COLORREF disable_color,BOOL enabled);
void setCBoxWindow(CMyComboBox *CB,COLORREF disable_color,BOOL enabled);


// Added 2009-05-06 P�D
void getTemplateMainSettings(TemplateParser *pars,int *hgt_over_sea,int *latitude,int *longitude,LPTSTR growth_area,LPTSTR si_h100,double *corr_fac,int *costs_id,
														 LPTSTR costs_tmpl_name,bool *is_at_coast,bool *is_south_east,bool *is_region5,bool *is_part_of_plot,LPTSTR si_h100_pine);
void getTemplateSettings(TemplateParser *pars,int *exch_id,LPTSTR exch,int *prl_id,int *prl_type_of,LPTSTR prl_name,double *dcls);
void getTemplateSpecies(TemplateParser *pars,vecTransactionSpecies &vec);
void getTemplateFunctionsPerSpcecie(TemplateParser *pars,int spc_id,vecUCFunctionList &vec,double *m3fub_m3to,double *m3sk_m3ub_const);
void getTemplateGrotDataPerSpcecie(TemplateParser *pars,int spc_id,int *func_id,double *precent,double *price,double *costs);
void getTemplateMiscFunctionsPerSpcecie(TemplateParser *pars,int spc_id,int *qdesc_index,LPTSTR qdesc,
																				double *sk_fub,double *fub_to,double *h25,int *transp_dist1,
																				int *transp_dist2,double *gcrown);
void getTemplateTransfersPerSpcecie(TemplateParser *pars,int spc_id,vecTransactionTemplateTransfers &vec);

// Added 2009-09-08 p�d
void parseTGL(LPCTSTR tgl,int* pine_percent,int* spruce_percent,int* birch_percent);
void calculateStormDry_1950(double pine_percent,
														double spruce_percent,
														double birch_percent,
														vecObjectTemplate_p30_table& p30,
														double volume_m3sk,
														LPCTSTR h100,
														LPCTSTR growth_area,
														double percent,
														double* spruce_mix,
														double* st_value,
														double* st_volume,
							double *fStormTorkInfo_SpruceMix,
							double *fStormTorkInfo_SumM3Sk_inside,
							double *fStormTorkInfo_AvgPriceFactor,
							double *fStormTorkInfo_TakeCareOfPerc,
							double *fStormTorkInfo_PineP30Price,
							double *fStormTorkInfo_SpruceP30Price,
							double *fStormTorkInfo_BirchP30Price,
							double *fStormTorkInfo_CompensationLevel,
							double *fStormTorkInfo_Andel_Pine,
							double *fStormTorkInfo_Andel_Spruce,
							double *fStormTorkInfo_Andel_Birch,
							double *fStormTorkInfo_WideningFactor);

void calculateStormDry_2009(double recalc_by,
														double pine_percent,
														double spruce_percent,
														double birch_percent,
														double volume_m3sk,
														vecObjectTemplate_p30_nn_table p30_nn,
														CString& h100,
														short wside,
													  double present_width,
														double new_width,
														double* spruce_mix,
														double* st_value,
														double* st_volume,
														bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor);

void calculateStormDry_2018(double recalc_by,
														double pine_percent,
														double spruce_percent,
														double birch_percent,
														double volume_m3sk,
														vecObjectTemplate_p30_nn_table p30_nn,
														CString& h100,
														short wside,
													  double present_width,
														double new_width,
														double* spruce_mix,
														double* st_value,
														double* st_volume,
														bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor);

void calculateCorrFactor_1950(double volume,
															double pine_percent,
															double spruce_percent,
															double birch_percent,
															vecObjectTemplate_p30_table& p30,
															LPCTSTR h100,
															int age,
															int growth_area,
															double *corr_factor);

void calculateCorrFactor_2009(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor);

void calculateCorrFactor_2018(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor);


void setRegisterLastObjectVisited(int obj_id,LPCTSTR db_name);
void getRegisterLastObjectVisited(int *obj_id,CString& db_name);

void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);

CString getDocumentsDir(int folder );

BOOL logThis(int item);

int TwipsPerPixel();

const int MIN_X_SIZE_SPECIES						= 450;
const int MIN_Y_SIZE_SPECIES						= 250;