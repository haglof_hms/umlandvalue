#pragma once

#include "Resource.h"

#include "CPage.h"

// CLoggMessageDlg dialog

class CLoggMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CLoggMessageDlg)

	CButton m_wndCancelBtn;
	CButton m_wndPrintOutBtn;
	CButton m_wndSaveToFileBtn;

	CMyExtEdit m_wndMessage;
	CString m_sMessage;

	CString m_sCaption;
	CString m_sCancel;
	CString m_sPrintOut;
	CString m_sSaveToFile;
	CString m_sLogFileName;

	int m_nObjID;
	CString m_sObjNameID;
	CString m_sObjName;
public:
	CLoggMessageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoggMessageDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	void setMessage(LPCTSTR msg)
	{
		m_sMessage = msg;
	}

	void setupLanguage(LPCTSTR cap,LPCTSTR cancel,LPCTSTR print_out,LPCTSTR save_to_file,LPCTSTR log_file_name)
	{
		m_sCaption = (cap);
		m_sCancel = (cancel);
		m_sPrintOut = (print_out);
		m_sSaveToFile = (save_to_file);
		m_sLogFileName = (log_file_name);
	}

	inline void setObjID(int obj_id)	{ m_nObjID = obj_id; }
	inline void setObjNameID(LPCTSTR obj_name_id)	{ m_sObjNameID = obj_name_id; }
	inline void setObjName(LPCTSTR obj_name)	{ m_sObjName = obj_name; }

protected:
	void PrintForm(CPage *page);
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo*);
	//}}AFX_MSG

 DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedPrintOut();
	afx_msg void OnBnClickedSaveToFile();
};
