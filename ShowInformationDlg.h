#pragma once

#include "Resource.h"

#include "ResLangFileReader.h"

// CShowInformationDlg dialog

class CShowInformationDlg : public CDialog
{
	DECLARE_DYNAMIC(CShowInformationDlg)

	BOOL m_bInitialized;
	// Setup language filename; 051214 p�d
	CString m_sLangFN;
	RLFReader m_xml;

	CFont m_fnt1;
	CFont m_fnt2;

	CUMLandValueDB *m_pDB;

	void showVoluntaryDeal(CDC *dc);
public:
	CShowInformationDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CShowInformationDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG12 };


	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


