#pragma once

#include "Resource.h"
#include "UMLandValueDB.h"
#include "libxl.h"
#include <list>
#include <map>
#include "afxwin.h"
#include "MapSpeciesDlg.h"
#include "MapTreeTypesDlg.h"

using namespace libxl;


struct TreeData
{
	CString sStand;
	CString sBlock;
	CString sUnit;
	CString sSpecie;
	CString sStandBonitet;
	int		nSpecieId;
	int		nStandAge;
	int		nTreeAge;
	int		nType;
	int		nKateg;
	int		nBark;
	int		nSide;
	int		nStandNumber;
	int		nStandArea;
	int		nStandLength;
	int		nStandWidth;
	int		nBonitetSpecNr;
	double	fToppn;
	double	fFasavst;
	double	fDiameter;
	double	fHeight;
	double	fGkHgt;
	double	fBonitet;
	double	fLatitude;
	double	fLongitude;
	double	fStandLat;
	double	fStandLong;
	bool	bIncluded;
};

typedef std::list<TreeData> TreeDataList;


// CImportExcelDlg dialog

class CImportExcelDlg : public CDialog
{
	DECLARE_DYNAMIC(CImportExcelDlg)

public:
	CImportExcelDlg(CString filename, CUMLandValueDB *pDB, CWnd* pParent = NULL);   // standard constructor
	virtual ~CImportExcelDlg();

	CString GetOutputPath() { return m_sOutputPath; }

	static CString m_sVersion;

// Dialog Data
	enum { IDD = IDD_DIALOG31 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	BOOL GenerateHXL();

	CUMLandValueDB *m_pDB;
	CString m_sFilename;
	Book *m_pBook;
	SpecieMap m_spcmap;
	TreeTypeMap m_treemap;
	CString m_sOutputPath;

	static const int TREETYPE_SAMPLETREE = 0;
	static const int TREETYPE_EDGETREE = 3;
	static const TCHAR* TREEFLAG_INSIDE;
	static const TCHAR* TREEFLAG_OUTSIDE;

	typedef void *(*funcConvertToLatLong)(const int epsg, const double x, const double y, double *lat, double *lon);

	double GpsEncode(double val);
	void LoadSheet();
	void LoadSpecies();
	void LoadTreeTypes();
	void LoadValue(CComboBox &cmb, CString regval);
	void ConvertToLatLong(HINSTANCE hInst, double &lat, double &lon, int type);

	DECLARE_MESSAGE_MAP()
	CComboBox m_wndCombo2;
	CComboBox m_wndCombo3;
	CComboBox m_wndCombo4;
	CComboBox m_wndCombo5;
	CComboBox m_wndCombo6;
	CComboBox m_wndComboHgt;
	CComboBox m_wndCombo8;
	CComboBox m_wndCombo9;
	CComboBox m_wndCombo10;
	CComboBox m_wndCombo11;
	CComboBox m_wndCombo12;
	CComboBox m_wndCombo13;
	CComboBox m_wndCombo14;
	CComboBox m_wndCombo15;
	CComboBox m_wndCombo16;
	CComboBox m_wndComboGkhgt;
	CComboBox m_wndComboGkhgtUnit;
	CComboBox m_wndComboToppn;
	CComboBox m_wndComboToppnUnit;
	CComboBox m_wndComboFasavst;
	CComboBox m_wndComboFasavstUnit;
	CComboBox m_wndComboKateg;
	CComboBox m_wndComboBark;
	CComboBox m_wndComboSida;
	CComboBox m_wndComboAreal;
	CComboBox m_wndComboArealUnit;
	CComboBox m_wndComboBestalder;
	CComboBox m_wndComboBestlat;
	CComboBox m_wndComboBestlong;
	CComboBox m_wndComboBestCoords;
	CComboBox m_wndComboBestbon;
	CComboBox m_wndComboLangd;
	CComboBox m_wndComboBredd;
	CStatic m_wndLabel2;
	CStatic m_wndLabel3;
	CStatic m_wndLabel4;
	CStatic m_wndLabel5;
	CStatic m_wndLabel6;
	CStatic m_wndLabel7;
	CStatic m_wndLabel8;
	CStatic m_wndLabel9;
	CStatic m_wndLabel10;
	CStatic m_wndLabel11;
	CStatic m_wndLabel12;
	CStatic m_wndLabel13;
	CStatic m_wndLabel14;
	CStatic m_wndLabelGkhgt;
	CStatic m_wndLabelToppn;
	CStatic m_wndLabelFasavst;
	CStatic m_wndLabelKateg;
	CStatic m_wndLabelBark;
	CStatic m_wndLabelSida;
	CStatic m_wndLabelAreal;
	CStatic m_wndLabelBestalder;
	CStatic m_wndLabelBestlat;
	CStatic m_wndLabelBestlong;
	CStatic m_wndLabelBestbon;
	CStatic m_wndLabelLangd;
	CStatic m_wndLabelBredd;
	CStatic m_wndGroupBestdata;
	CStatic m_wndGroupTraddata;
	CButton m_wndCheck1;

public:
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnCbnSelchangeCombo16();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnCbnSelchangeCombo7();
	afx_msg void OnCbnSelchangeCombo14();
};
