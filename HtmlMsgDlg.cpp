// HtmlMsgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HtmlMsgDlg.h"
#include "ResLangFileReader.h"


// CHtmlMsgDlg dialog

IMPLEMENT_DYNAMIC(CHtmlMsgDlg, CDialog)


BEGIN_MESSAGE_MAP(CHtmlMsgDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CHtmlMsgDlg::OnBnClickedButton1)
END_MESSAGE_MAP()

CHtmlMsgDlg::CHtmlMsgDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHtmlMsgDlg::IDD, pParent)
{

}

CHtmlMsgDlg::~CHtmlMsgDlg()
{
}

void CHtmlMsgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnPrintOut);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

// CAddKrPerM3Dlg message handlers
BOOL CHtmlMsgDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING229));
			m_wndBtnOK.SetWindowText(xml.str(IDS_STRING4516));
			m_wndBtnCancel.SetWindowText(xml.str(IDS_STRING1161));
			m_wndBtnPrintOut.SetWindowTextW(xml.str(IDS_STRING236));
			xml.clean();
		}
	}

	this->m_cHTML.CreateFromStatic(IDC_HTML_MSG,this);

	return TRUE;
}


void CHtmlMsgDlg::startHTML(void)
{
	m_sHTMLText = L"";
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<html><body bgcolor=\"FFFFE0\" style=\"border-style:none\" \"><table width=\"100%%\">");
	m_sHTMLText = csHTML;
	
}

void CHtmlMsgDlg::addHeader(LPCTSTR text,short font_size,bool bold)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	if (bold)
		csHTML.Format(L"<td><tr style=\"font-size:%dpt\"><b>%s</b></tr></td>",font_size,text);
	else
		csHTML.Format(L"<td><tr style=\"font-size:%dpt\">%s</tr></td>",font_size,text);
	m_sHTMLText += csHTML;
}

void CHtmlMsgDlg::addTable()
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<table width=\"100%%\">");
	m_sHTMLText += csHTML;
}

void CHtmlMsgDlg::endTable()
{
	m_sHTMLText += L"</table>";
}

void CHtmlMsgDlg::addTableBR(short font_size)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<td><tr style=\"font-size:%dpt\"><br/></tr></td>",font_size);
	m_sHTMLText += csHTML;
}

void CHtmlMsgDlg::addTableHR(short font_size)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<td><tr style=\"font-size:%dpt\"><hr/></tr></td>",font_size);
	m_sHTMLText += csHTML;
}

void CHtmlMsgDlg::addTableText(LPCTSTR text,short font_size,bool bold)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	if (bold)
		csHTML.Format(L"<td><tr style=\"font-size:%dpt\"><b>%s</b></tr></td>",font_size,text);
	else
		csHTML.Format(L"<td><tr style=\"font-size:%dpt\">%s</tr></td>",font_size,text);
	m_sHTMLText += csHTML;
}

void CHtmlMsgDlg::endHTML(void)
{
	m_sHTMLText += L"</table></body></html>";
}

void CHtmlMsgDlg::showHTML(void)
{
	this->m_cHTML.SetNewHTMLContent(m_sHTMLText,true);
}

void CHtmlMsgDlg::OnBnClickedButton1()
{
	print();
}
