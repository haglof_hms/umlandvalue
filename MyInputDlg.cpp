// MyInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyInputDlg.h"


#include "ResLangFileReader.h"

// CMyInputDlg dialog

IMPLEMENT_DYNAMIC(CMyInputDlg, CDialog)

BEGIN_MESSAGE_MAP(CMyInputDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CMyInputDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CMyInputDlg::CMyInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyInputDlg::IDD, pParent)
{

}

CMyInputDlg::~CMyInputDlg()
{
}

void CMyInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)

	DDX_Control(pDX, IDC_LBL22_1, m_wndLbl1);

	DDX_Control(pDX, IDC_EDIT22_1, m_wndEdit1);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	//}}AFX_DATA_MAP

}

BOOL CMyInputDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING4080));
			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING2271));

			m_wndLbl1.SetWindowText(xml.str(IDS_STRING4081));
			xml.clean();
		}
	}

	m_wndEdit1.SetRange(0,10000);

	return TRUE;
}

// CMyInputDlg message handlers

void CMyInputDlg::OnBnClickedOk()
{
	m_fVolume = m_wndEdit1.getFloat();
	OnOK();
}
