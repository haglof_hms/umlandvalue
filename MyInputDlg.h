#pragma once

#include "Resource.h"

// CMyInputDlg dialog

class CMyInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CMyInputDlg)

	CMyExtStatic m_wndLbl1;

	CMyExtEdit m_wndEdit1;

	CButton m_wndOKBtn;

	double m_fVolume;
public:
	CMyInputDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMyInputDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG22 };

	double getVolumeEntered(void)	{ return m_fVolume; }

protected:
	//{{AFX_VIRTUAL(CMyInputDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
