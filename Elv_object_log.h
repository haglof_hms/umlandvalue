#pragma once

class CElv_object_log
{
	int m_nObjectId;
	CString m_csObject;
	int m_nType;
	CString m_csUser;
	CString m_csDb;

public:
	CElv_object_log(void);
	CElv_object_log(int /*nObjectId*/, CString /*csObject*/, int /*nType*/, CString /*csUser*/, CString /*csDb*/);
	~CElv_object_log(void);

	int getObjectId() { return m_nObjectId; }
	void setObjectId(int nObjectId) { m_nObjectId = nObjectId; }

	CString getObjectName() { return m_csObject; }
	void setObjectName(CString csObject) { m_csObject = csObject; }

	int getType() { return m_nType; }
	void setType(int nType) { m_nType = nType; }

	CString getUser() { return m_csUser; }
	void setUser(CString csUser) { m_csUser = csUser; }

	CString getDb() { return m_csDb; }
	void setDb(CString csDb) { m_csDb = csDb; }
};
