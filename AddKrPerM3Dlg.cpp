// AddKrPerM3Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "AddKrPerM3Dlg.h"

#include "ResLangFileReader.h"

// CAddKrPerM3Dlg dialog

IMPLEMENT_DYNAMIC(CAddKrPerM3Dlg, CDialog)

BEGIN_MESSAGE_MAP(CAddKrPerM3Dlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAddKrPerM3Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CAddKrPerM3Dlg::OnBnClickedButton1)
END_MESSAGE_MAP()

CAddKrPerM3Dlg::CAddKrPerM3Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddKrPerM3Dlg::IDD, pParent)
{
	m_pDB = NULL;
}

CAddKrPerM3Dlg::~CAddKrPerM3Dlg()
{
}

void CAddKrPerM3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_BUTTON1, m_wndReset);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP
}

// CAddKrPerM3Dlg message handlers
BOOL CAddKrPerM3Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING5700)));
			m_wndReset.SetWindowText((xml.str(IDS_STRING5706)));
			m_wndOKBtn.SetWindowText((xml.str(IDS_STRING5704)));
			m_wndCancelBtn.SetWindowText((xml.str(IDS_STRING5705)));
			xml.clean();
		}
	}

	setupReport();
	getAssortmentData();
	populateData();

	return TRUE;
}

void CAddKrPerM3Dlg::getAssortmentData(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getAssortmentsForTraktAndObject(m_nObjID,vecAssStandsObj);
	}	// if (m_pDB != NULL)
}

void CAddKrPerM3Dlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_KR_PER_M3, FALSE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING5702)), 150));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING5703)), 50,FALSE));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.GetRecords()->SetCaseSensitive(FALSE);

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_KR_PER_M3),2,2,rect.right - 4,rect.bottom - 40);
				xml.clean();
			}
		}	// if (fileExists(sLangFN))
	}
}

void CAddKrPerM3Dlg::populateData(void)
{
	CTransaction_elv_ass_stands_obj rec;
	if (vecAssStandsObj.size() > 0)
	{
		for (UINT i = 0;i < vecAssStandsObj.size();i++)
		{
			rec = vecAssStandsObj[i];

			m_wndReport.AddRecord(new CAssortsStandsObjRec(i,rec));
		}	// for (UINT i = 0;i < vecAssStandsObj.size();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
	}	// if (getAssortmentData.size() > 0)
}

void CAddKrPerM3Dlg::OnBnClickedOk()
{
	CTransaction_elv_ass_stands_obj rec;
	CAssortsStandsObjRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	vecAssStandsObj_return.clear();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CAssortsStandsObjRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				rec =	CTransaction_elv_ass_stands_obj(pRec->getRecord().getAssortID(),
																							pRec->getColumnText(COLUMN_0),
																							pRec->getColumnFloat(COLUMN_1));

				vecAssStandsObj_return.push_back(rec);	
			
			}	// if (pRec != NULL)
		
		}	// for (int i = 0;i < pRows->GetCount();i++)

	}	// if (pRows != NULL)
	OnOK();
}

// This button'll reset values in Report; 080613 p�d
void CAddKrPerM3Dlg::OnBnClickedButton1()
{
	CAssortsStandsObjRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CAssortsStandsObjRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnFloat(COLUMN_1,0.0);			
			}	// if (pRec != NULL)	
		}	// for (int i = 0;i < pRows->GetCount();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
	}	// if (pRows != NULL)
}
