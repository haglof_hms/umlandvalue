// ReplaceTemplateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ReplaceTemplateDlg.h"

#include "ResLangFileReader.h"

// CReplaceTemplateDlg dialog

IMPLEMENT_DYNAMIC(CReplaceTemplateDlg, CDialog)

BEGIN_MESSAGE_MAP(CReplaceTemplateDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO3_1, &CReplaceTemplateDlg::OnCbnSelchangeList31)
	ON_BN_CLICKED(IDOK, &CReplaceTemplateDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON3_1, &CReplaceTemplateDlg::OnBnClicked31)
	ON_BN_CLICKED(IDC_BUTTON3_2, &CReplaceTemplateDlg::OnBnClicked32)
	ON_BN_CLICKED(IDC_BUTTON3_3, &CReplaceTemplateDlg::OnBnClicked33)
	ON_BN_CLICKED(IDC_BUTTON3_4, &CReplaceTemplateDlg::OnBnClicked34)
END_MESSAGE_MAP()

CReplaceTemplateDlg::CReplaceTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReplaceTemplateDlg::IDD, pParent)
{

}

CReplaceTemplateDlg::~CReplaceTemplateDlg()
{
}

void CReplaceTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReplaceTemplateDlg)
	DDX_Control(pDX, IDC_LBL3_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL3_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL3_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL3_5, m_wndLbl5);
	DDX_Control(pDX, IDC_COMBO3_1, m_wndCB1);
	//DDX_Control(pDX, IDC_LIST3_2, m_wndLB2);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	DDX_Control(pDX, IDC_LIST_SOURCE, m_listCtrl);
	DDX_Control(pDX, IDC_LIST_SOURCE2, m_listCtrl2);
	DDX_Control(pDX, IDC_DESCRIPTION, m_Description);
	//}}AFX_DATA_MAP
}

/*
int CReplaceTemplateDlg::InsertRow(CListCtrl &ctrl,int nPos,bool checked,int nNoOfCols, LPCTSTR pText, ...)
{
	va_list argList;
	va_start(argList, pText);
	ASSERT(nNoOfCols >= 1); // use the 'normal' Insert function with 1 argument
	int nIndex = ctrl.InsertItem(LVIF_TEXT|LVIF_STATE, nPos, pText,0,LVIS_SELECTED,0,0);
	ASSERT(nIndex != -1);

	if (nIndex < 0) 
		return(nIndex);
	for (int i = 1; i < nNoOfCols; i++) 
	{
		LPCTSTR p = va_arg(argList,LPCTSTR);
		if (p != NULL) 
		{
			ctrl.SetItemText(nIndex, i, p);   
			ctrl.SetCheck(nPos,checked);
		}
	}
	va_end(argList);
	return(nIndex);
}*/


BOOL CReplaceTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

//	CString csLockedProperties, csInformation;
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING2701));

			m_wndLbl1.SetWindowText(xml.str(IDS_STRING204));
			
			m_wndLbl2.SetLblFont(14,FW_BOLD);
			m_wndLbl2.SetWindowText(m_sObjectName);
			m_wndLbl3.SetWindowText(xml.str(IDS_STRING2708));
			m_wndLbl4.SetLblFont(14,FW_BOLD);
			m_wndLbl4.SetWindowText(xml.str(IDS_STRING2709));
			m_wndLbl5.SetLblFont(14,FW_BOLD);
			m_wndLbl5.SetWindowText(xml.str(IDS_STRING2710));

			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING5805));
			m_wndOKBtn.EnableWindow(FALSE);
			m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING5806));

			CString csBuf;
			csBuf.Format(_T("%s"), xml.str(IDS_STRING2713));
			m_Description.SetWindowText(csBuf);
			m_Description.SetTextColor( RGB(255, 0, 0) );

			m_sPricelistErrorStatus	=xml.str(IDS_STRING701);
			m_sCostErrorStatus		=xml.str(IDS_STRING702);
			m_sErrorStatus			=xml.str(IDS_STRING703);

			xml.clean();
		}
	}


	// Add Templates to ListBox; 090506 p�d
	m_wndCB1.ResetContent();
	if (m_vecTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecTemplates.size();i++)
			m_wndCB1.AddString(m_vecTemplates[i].getTemplateName());
	}

	getPropertiesFromDB();
	int nCounter = 0;
	int nItem = 0;
	
	m_listCtrl.ModifyExtendedStyle(0, LVS_EX_FULLROWSELECT|LVS_EX_FULLROWSELECT);
	m_listCtrl.EnableUserRowColor(TRUE);
    
	m_listCtrl2.ModifyExtendedStyle(0, LVS_EX_FULLROWSELECT|LVS_EX_FULLROWSELECT);
	m_listCtrl2.EnableUserRowColor(TRUE);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			if (m_listCtrl.GetSafeHwnd())
			{
				m_listCtrl.InsertColumn(0,xml.str(IDS_STRING2315), LVCFMT_LEFT, 100);	 
				m_listCtrl.InsertColumn(1,xml.str(IDS_STRING2314), LVCFMT_LEFT, 60);
				m_listCtrl.InsertColumn(2,xml.str(IDS_STRING2311), LVCFMT_LEFT, 100);
				m_listCtrl.InsertColumn(3,xml.str(IDS_STRING2312), LVCFMT_LEFT, 100);
			}
			if (m_listCtrl2.GetSafeHwnd())
			{
				m_listCtrl2.InsertColumn(0,xml.str(IDS_STRING2315), LVCFMT_LEFT, 100);
				m_listCtrl2.InsertColumn(1,xml.str(IDS_STRING2314), LVCFMT_LEFT, 60);
				m_listCtrl2.InsertColumn(2,xml.str(IDS_STRING2311), LVCFMT_LEFT, 100);
				m_listCtrl2.InsertColumn(3,xml.str(IDS_STRING2312), LVCFMT_LEFT, 100);		
			}
			xml.clean();
		}
	}

	// Setup Properties in propertyregister, check Status; 090918 p�d
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			//nItem = m_listCtrl.InsertItem(i, recProps.getPropName());

			nItem=InsertRow(m_listCtrl,i,4,recProps.getPropName(),recProps.getPropNumber(),recProps.getPropCounty(),recProps.getPropMunicipal());

			if (canWeCalculateThisProp_cached(recProps.getPropStatus()))
			{
				m_listCtrl.SetRowColor(nItem, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, recProps.getPropID_pk());
			}
			else	// gray out locked property
			{
				m_listCtrl.SetRowColor(nItem, RGB(255, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, -1);
			}
		}	// for (UINT i = 0;i < m_vecProps.size();i++)

		m_listCtrl.RedrawWindow();


	}	// if (m_vecProps.size() > 0)

	return TRUE;
}


// CReplaceTemplateDlg message handlers

// Add to class data member: m_vecELV_properties; 080415 p�d
void CReplaceTemplateDlg::getPropertiesFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_nObjID,m_vecProps);
	}	// if (m_pDB != NULL)
}

CTransaction_elv_properties& CReplaceTemplateDlg::getProps(int prop_id)
{
	// Setup Properties in propertyregister, check Status; 090918 p�d
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			if (recProps.getPropID_pk() == prop_id)
				return recProps;
		}	// for (UINT i = 0;i < m_vecProps.size();i++)
	}	// if (m_vecProps.size() > 0)

	return CTransaction_elv_properties();
}

void CReplaceTemplateDlg::OnCbnSelchangeList31()
{
	int nSelIdx = m_wndCB1.GetCurSel();
	m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
}

void CReplaceTemplateDlg::OnBnClicked31()
{
	CString sPropName;
	CTransaction_elv_properties rec,rec2;
	int nSelIdx = -1,doAddProp=1; 
	int nSelPropID = -1;
	int nNumSel = m_listCtrl.GetSelectedCount();

	int nItem = -1;
	if (nNumSel > 0)
	{
		for (int i=0; i < nNumSel; i++)
		{
			nItem = m_listCtrl.GetNextItem(-1, LVNI_SELECTED);
			nSelPropID = m_listCtrl.GetItemData(nItem);

			if(nSelPropID != -1)
			{
				rec = getProps(nSelPropID);
				sPropName = rec.getPropName();
				InsertRow(m_listCtrl2,m_listCtrl2.GetItemCount(),4,rec.getPropName(),rec.getPropNumber(),rec.getPropCounty(),rec.getPropMunicipal());
				m_listCtrl2.SetItemData(m_listCtrl2.GetItemCount()-1,rec.getPropID_pk());
				//m_wndLB2.AddString(sPropName);
				//m_wndLB2.SetItemData(m_wndLB2.GetCount()-1,rec.getPropID_pk());
				//Tar bort vald instans fr�n listan feature #2389 20111004 J�
				m_listCtrl.DeleteItem(nItem);
				m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);

			}
		}
		
			//G� igenom m_listCtrl och s�tt r�tt p� de som �r l�sta
			if(m_listCtrl.GetItemCount()>0)
			{
				for(int i = 0; i < m_listCtrl.GetItemCount(); i++)
				{
					nSelPropID = m_listCtrl.GetItemData(i);						
					if(nSelPropID != -1)
					{
						rec = getProps(nSelPropID);
						m_listCtrl.SetRowColor(i, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
						m_listCtrl.SetItemData(i, rec.getPropID_pk());
					}
					else	// gray out locked property
					{
						m_listCtrl.SetRowColor(i, RGB(255, 0, 0), m_listCtrl.GetBkColor(), TRUE);
						m_listCtrl.SetItemData(i, -1);
					}
				}

				m_listCtrl.RedrawWindow();
			}


	}
}

void CReplaceTemplateDlg::OnBnClicked32()
{
	CTransaction_elv_properties rec;
	int i=0;
	//Inte resetta andra listan h�r utan f�r �ver de som �r kvar i f�rsta listan #2389 20111004 J�
	//m_wndLB2.ResetContent();

	if(m_listCtrl.GetItemCount()>0)
	{
		//for(int i = 0; i < m_listCtrl.GetItemCount(); i++)
		do
		{
			int nSelPropID = m_listCtrl.GetItemData(i);
			if(nSelPropID != -1)
			{
				rec = getProps(nSelPropID);
				InsertRow(m_listCtrl2,m_listCtrl2.GetItemCount(),4,rec.getPropName(),rec.getPropNumber(),rec.getPropCounty(),rec.getPropMunicipal());
				m_listCtrl2.SetItemData(m_listCtrl2.GetItemCount()-1,rec.getPropID_pk());
				
				//m_wndLB2.AddString(rec.getPropName());
				//m_wndLB2.SetItemData(m_wndLB2.GetCount()-1,rec.getPropID_pk());
				
				//Tar bort vald instans fr�n listan feature #2389 20111004 J�
				m_listCtrl.DeleteItem(i);
			}
			else
			{
				m_listCtrl.SetRowColor(i, RGB(255, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(i, -1);
				i++;
			}			
		}while(m_listCtrl.GetItemCount()>0 && i<m_listCtrl.GetItemCount());
		m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
		m_listCtrl.RedrawWindow();
	}
}

void CReplaceTemplateDlg::OnBnClicked33()
{
	int nSelIdx = 0,nItem=0,i=0;
	//CString sPropName;
	CTransaction_elv_properties rec;
	int nSelPropID = -1;
	//Lagt till funktion att kunna v�lja flera att f�re tillbaks med enkelpil Bug #2582 20111123 J�
	int nNumSel = m_listCtrl2.GetSelectedCount();

	if (nNumSel > 0)
	{
		for (int i=0; i < nNumSel; i++)
		{
			nItem = m_listCtrl2.GetNextItem(-1, LVNI_SELECTED);
			nSelPropID = m_listCtrl2.GetItemData(nItem);
			rec = getProps(nSelPropID);
			m_listCtrl2.DeleteItem(nItem);
			InsertRow(m_listCtrl,m_listCtrl.GetItemCount(),4,rec.getPropName(),rec.getPropNumber(),rec.getPropCounty(),rec.getPropMunicipal());
			if (canWeCalculateThisProp_cached(rec.getPropStatus()))
			{
				m_listCtrl.SetRowColor(nItem, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, recProps.getPropID_pk());
			}
			else	// gray out locked property
			{
				m_listCtrl.SetRowColor(nItem, RGB(255, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, -1);
			}		
			m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
		}
	}
/*
	if(nNumSel>0)
	{
		LPINT lpnSelItems = new int[nNumSel];
		m_wndLB2.GetSelItems(nNumSel,lpnSelItems);
		do
		{	
			nSelIdx = lpnSelItems[i];
			//L�gger tillbaks vald instans till listan feature #2389 20111004 J�
			nSelPropID = m_wndLB2.GetItemData(nSelIdx);
			m_wndLB2.DeleteString(nSelIdx);
			rec = getProps(nSelPropID);

			nItem = m_listCtrl.InsertItem(m_listCtrl.GetItemCount(), recProps.getPropName());

			if (canWeCalculateThisProp_cached(recProps.getPropStatus()))
			{
				m_listCtrl.SetRowColor(nItem, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, recProps.getPropID_pk());
			}
			else	// gray out locked property
			{
				m_listCtrl.SetRowColor(nItem, RGB(255, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, -1);
			}		
			nNumSel = m_wndLB2.GetSelCount();
			m_wndLB2.GetSelItems(nNumSel,lpnSelItems);		
		}while(i<nNumSel && nNumSel>0);
		delete[] lpnSelItems;
	}	*/
}

void CReplaceTemplateDlg::OnBnClicked34()
{
	int nItem=0;
	m_listCtrl2.DeleteAllItems();

	//L�ser in fastigheter igen, feature #2389 20111004 J�
	m_listCtrl.DeleteAllItems();
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			//nItem = m_listCtrl.InsertItem(i, recProps.getPropName());
			nItem=InsertRow(m_listCtrl,i,4,recProps.getPropName(),recProps.getPropNumber(),recProps.getPropCounty(),recProps.getPropMunicipal());
            if (canWeCalculateThisProp_cached(recProps.getPropStatus()))
			{
				m_listCtrl.SetRowColor(nItem, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, recProps.getPropID_pk());
			}
			else	// gray out locked property
			{
				m_listCtrl.SetRowColor(nItem, RGB(255, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, -1);
			}
		}	// for (UINT i = 0;i < m_vecProps.size();i++)

		m_listCtrl.RedrawWindow();

	}	// if (m_vecProps.size() > 0)

	m_wndOKBtn.EnableWindow(FALSE);
}

void CReplaceTemplateDlg::OnBnClickedOk()
{
	CTransaction_elv_properties rec;
	int nSelIdx = m_wndCB1.GetCurSel();
	int nRet=0,i=0;
	CString sMsg=_T("");

	if (nSelIdx > CB_ERR && nSelIdx < m_vecTemplates.size())
		m_recSelectedTemplate = m_vecTemplates[nSelIdx];

	//L�gg in kontroll om ing�ende prislista eller kostnadsmall �r sparade som under utveckling
	//20111011 J� Bug #2367
	nRet=CheckStandTemplate(m_recSelectedTemplate);


	switch(nRet)
	{
	case 1:
		m_vecProps_change_template.clear();
		/*
		for (int i = 0;i < m_wndLB2.GetCount();i++)
		{
			int nSelPropID = m_wndLB2.GetItemData(i);
			rec = getProps(nSelPropID);
			if (rec.getPropID_pk() > -1)
				m_vecProps_change_template.push_back(rec);
		}*/

		if(m_listCtrl2.GetItemCount()>0)
		{
			//for(int i = 0; i < m_listCtrl.GetItemCount(); i++)
			do
			{
				int nSelPropID = m_listCtrl2.GetItemData(i);
				if(nSelPropID != -1)
				{
					rec = getProps(nSelPropID);
					if (rec.getPropID_pk() > -1)
						m_vecProps_change_template.push_back(rec);
				}
				i++;
			}while(m_listCtrl2.GetItemCount()>0 && i<m_listCtrl2.GetItemCount());
		}

		break;
	case -1://Prislista sparad som under utveckling
		sMsg=m_sPricelistErrorStatus+_T("\n\n")+m_sErrorStatus;
		::MessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
		break;
	case -2://Kostnadsmall sparad som under utveckling
		sMsg=m_sCostErrorStatus+_T("\n\n")+m_sErrorStatus;
		::MessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
		break;
	case -3://Prislista & Kostnadsmall sparad som under utveckling
		sMsg=m_sPricelistErrorStatus+_T("\n")+m_sCostErrorStatus+_T("\n\n")+m_sErrorStatus;
		::MessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
		break;
	}

	if(nRet==1)
		OnOK();
	else
		return;
}

//L�gg in kontroll om ing�ende prislista eller kostnadsmall �r sparade som under utveckling
//20111011 J� Bug #2367	
int CReplaceTemplateDlg::CheckStandTemplate(CTransaction_template recTmpl)
{
	int nRet=1,nPrlId=0,nCostId=0;
	int nPrlID=0,nPrlTypeof=0,nCostTypeof=0;
	TCHAR szFuncName[50];
	TemplateParser pars;
	vecTransactionPricelist vecPrl;
	vecTransaction_costtempl vecCost;
	if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
	{		
		//H�mta id f�r prislista
		pars.getTemplatePricelist(&nPrlId,szFuncName);

		if (m_pDB != NULL)
		{
			if(m_pDB->getPricelists(vecPrl))
			{
				//Leta r�tt p� r�tt prislista och kolla typen, sparad som utveckling:typen= 1
				for(int i=0;i<vecPrl.size();i++)
					if(vecPrl[i].getID()==nPrlId)
					{
						nPrlTypeof=vecPrl[i].getTypeOf();
						break;
					}
			}

			pars.getTemplateCostsTmpl(&nCostId,szFuncName);

			if(m_pDB->getAllCostTmpls(vecCost))
			{
				//Leta r�tt p� r�tt kostnadsmall och kolla typen, sparad som utveckling:typen= 1
				for(int i=0;i<vecCost.size();i++)
					if(vecCost[i].getID()==nCostId)
					{
						nCostTypeof=vecCost[i].getTypeOf();
						break;
					}
			}
		}

	}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))

	if(nPrlTypeof<0 && nCostTypeof<0)
		nRet=-3;
	else
	{
		if(nPrlTypeof<0)
			nRet=-1;
		else
		{
			if(nCostTypeof<0)
			nRet=-2;
		}
	}

	return nRet;
}