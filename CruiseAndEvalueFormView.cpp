// CruiseAndEvalueFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MDI1950ForrestNormFormView.h"
#include "PropertiesFormView.h"
#include "CruiseAndEvalueFormView.h"

#include "ArealCalculatorDlg.h"
#include "AddTGLDlg.h"

#include "ResLangFileReader.h"

#include "1950ForrestNormFrame.h"

#include "ProgressDlg2.h"

#include "AddEditEvalDialog.h"

#include "ShowInformationDlgPrecut.h"
#include "ShowInformationDlgStormTork.h"
#include "ShowInformationDlgMarkvarde.h"

/////////////////////////////////////////////////////////////////////////////
// Holds information on Property Status set in table "elv_property_status_table"; 090916 p�d
extern CUMLandValueDB *global_pDB;
extern StrMap global_langMap;

//////////////////////////////////////////////////////////////////////////////
// CEvaluatedData

IMPLEMENT_DYNCREATE(CEvaluatedData,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CEvaluatedData,  CXTPReportView) //CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportChanged)
	ON_NOTIFY(NM_RCLICK, XTP_ID_REPORT_CONTROL, OnReportItemRClick)
//	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportConstraintChanged)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportHeaderColumnRClick)
END_MESSAGE_MAP()


CEvaluatedData::CEvaluatedData()	
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
	m_bLoadReportOK = false;
}


void CEvaluatedData::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST_EVALUE, &pWnd->m_wndFieldChooserDlg_evalue);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (!m_bInitialized)
	{

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport();

		LoadReportState();

		// To make sure ReportControl column widths are set; 100526 p�d
		if (!m_bLoadReportOK)
		{
			CXTPReportColumns *pCol = GetReportCtrl().GetColumns();
			if (pCol)
			{
				pCol->GetAt(COLUMN_0)->SetWidth(150);
				pCol->GetAt(COLUMN_1)->SetWidth(150);
				pCol->GetAt(COLUMN_2)->SetWidth(100);
				pCol->GetAt(COLUMN_3)->SetWidth(80);
				pCol->GetAt(COLUMN_4)->SetWidth(80);
				pCol->GetAt(COLUMN_5)->SetWidth(80);
				pCol->GetAt(COLUMN_6)->SetWidth(80);
				pCol->GetAt(COLUMN_7)->SetWidth(80);
				pCol->GetAt(COLUMN_8)->SetWidth(80);
				pCol->GetAt(COLUMN_9)->SetWidth(80);
				pCol->GetAt(COLUMN_10)->SetWidth(80);
				pCol->GetAt(COLUMN_11)->SetWidth(80);
				pCol->GetAt(COLUMN_12)->SetWidth(80);
				pCol->GetAt(COLUMN_13)->SetWidth(80);
				pCol->GetAt(COLUMN_14)->SetWidth(80);
				pCol->GetAt(COLUMN_15)->SetWidth(80);
				pCol->GetAt(COLUMN_16)->SetWidth(100);
				pCol->GetAt(COLUMN_17)->SetWidth(100);
				pCol->GetAt(COLUMN_18)->SetWidth(80);
				m_bLoadReportOK = true;
			}
		}

		m_bInitialized = TRUE;
	}
 
}

BOOL CEvaluatedData::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
			global_pDB = m_pDB;
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CEvaluatedData::OnDestroy()
{
	// Try to clear records on exit (for memory deallocation); 080215 p�d
	GetReportCtrl().ResetContent();

	if (m_pDB != NULL)
	{
		delete m_pDB;
		m_pDB = NULL;
	}
	if( global_pDB )
	{
		// global_pDB is pointing to the same instance as m_pDB
		global_pDB = NULL;
	}

	m_ilIcons.DeleteImageList();

	SaveReportState();

	CXTPReportView::OnDestroy();	
}

BOOL CEvaluatedData::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CEvaluatedData::ChangeAreal(CXTPReportRow *pRow,double fAreal)
{
CELVObjectEvaluatedReportRec *pRec = NULL;
	if (pRow != NULL)
	{
		//Check if the property is made from a taxeringsbest�nd then do not show any area calculation 20120201 J� Bug #2790
		pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();

		if (pRec != NULL)
		{
			if (pRec->getRecELVEvaluated().getEValType()!=EVAL_TYPE_FROM_TAXERING)
			{

				pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
				if (pRec != NULL)
				{
					double fLandValue_per_ha = pRec->getColumnFloat(COLUMN_12);
					pRec->setIconColumnFloat(COLUMN_2,fAreal);
					if (fLandValue_per_ha > 0.0 && fAreal > 0.0)
						pRec->setColumnFloat(COLUMN_10,fAreal*fLandValue_per_ha);

					//Uppdatera �ven volym per hektar  4352 20150805 J�
					double fVolume_m3sk = pRec->getColumnFloat(COLUMN_14);
					if(fVolume_m3sk>0.0)
					{
						if(fAreal>0.0)
						pRec->setColumnFloat(COLUMN_15,fVolume_m3sk/fAreal);
						else
						pRec->setColumnFloat(COLUMN_15,0.0);
					}
					GetReportCtrl().Populate();
					GetReportCtrl().UpdateWindow();
				}	// if (pRec != NULL)

			}
		}
	}

}

void CEvaluatedData::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	CString S;
	int nVStandIndex = -1;
	int nEvalType = -1;
	int nEvalId=-1;
	int nCol=-1;
	CString sMsgCap,sMsg;
	CXTPReportColumn *pCol = NULL;
	CXTPReportRow *pRow = NULL;
	CELVObjectEvaluatedReportRec *pRec = NULL;
	CELVObjectCruiseReportRec *pRec2 = NULL;
	
	

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		nCol=pItemNotify->pColumn->GetItemIndex();

		if (!canWeCalculateThisProp_cached(m_nPropertyStatus)) 
		{
			// Visa inforutor �ven om status p� fastighet l�st 20200625 J� HMS-48,HMS-49
			if(!(nCol==COLUMN_10 || nCol==COLUMN_11))
			return;
		}

		switch (nCol)
		{
		case COLUMN_10:	//Markv�rde
			// HMS-48 Info om markv�rde 20200428 J�
			pCol = pItemNotify->pColumn;
			pRow = pItemNotify->pRow;
			if (pCol != NULL && pRow != NULL)
			{
				rect = pCol->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not;
				if (hitTest_X(pt.x,rect.left,13))
				{
					pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord();
					if (pRec != NULL)
					{
							nEvalId=pRec->getRecELVEvaluated().getEValID_pk();							
							CShowInformationDlgMarkvarde *pDlg = new CShowInformationDlgMarkvarde();
							if (pDlg != NULL)				
							{			
								pDlg->setElv(nEvalId);
								pDlg->setDBConnection(m_pDB);						
								pDlg->DoModal();
								delete pDlg;
							}	// if (pDlg != NULL)

					}
				}
			}
			break;
		case COLUMN_11:  //HMS-49 Kolla om klickat p� f�rstoringsglas f�r info om hur f�rtidig �r ber�knad 20191126 J�
				pCol = pItemNotify->pColumn;
				pRow = pItemNotify->pRow;				
				if (pCol != NULL && pRow != NULL)
				{
					rect = pCol->GetRect();		
					pt = pItemNotify->pt;
					// Check if the user clicked on the Icon or not;
					if (hitTest_X(pt.x,rect.left,13))
					{						
						//pRec2 = (CELVObjectCruiseReportRec *)pRow->GetRecord();
						pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord();
						if (pRec != NULL)
						{
							nEvalId=pRec->getRecELVEvaluated().getEValID_pk();
							
							CShowInformationDlgPrecut *pDlg = new CShowInformationDlgPrecut();
							if (pDlg != NULL)				
							{			
								pDlg->setElv(nEvalId);
								pDlg->setDBConnection(m_pDB);						
								pDlg->DoModal();
								delete pDlg;
							}	// if (pDlg != NULL)
						}
					}
				}
			break;
			case COLUMN_2 :
			{
				pCol = pItemNotify->pColumn;
				pRow = pItemNotify->pRow;
				if (pCol != NULL && pRow != NULL)
				{
					rect = pCol->GetRect();		
					pt = pItemNotify->pt;
					// 4352 Kontrollera om arealen �r l�st om v�rderingsbest�nd i s� fall 
					// tala om f�r anv�ndaren att l�ngd och bredd skall s�ttas till 0 f�r att l�sa upp arealsf�lt 20150508 J�
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand 
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						switch(nVStandIndex)
						{
						case EVAL_TYPE_VARDERING:	// "V�rderingsbest�nd"
						case  EVAL_TYPE_ANNAN:	// "Annan mark"
							double fLength=0.0,fWidth=0.0;
							double fAreal=0.0;

							fLength = pRec->getColumnInt(COLUMN_16);
							fWidth = pRec->getColumnInt(COLUMN_17);							

							if(fLength>0.0||fWidth>0.0) //Arealsf�ltet �r l�st
							{
								if (fileExists(m_sLangFN))
								{
									RLFReader xml;
									if (xml.Load(m_sLangFN))
									{
										sMsgCap = xml.str(IDS_STRING229);
										sMsg = xml.str(IDS_STRING4311);
									}	// if (xml->Load(m_sLangFN))
									xml.clean();
								}
								//::MessageBox(this->GetSafeHwnd(),sMsg,sMsgCap,MB_ICONEXCLAMATION | MB_OK);
							}
							break;
						}
					}
					
					//#4207 150114 J� tagit bort arealsdialogicon, lagt till som tv� kolumner ist�llet
					/* 
					// Check if the user clicked on the Icon or not; 080513 p�d
					if (hitTest_X(pt.x,rect.left,13))
					{
						//Check if the property is made from a taxeringsbest�nd then do not show any area calculation 20120201 J� Bug #2790
						pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
						
						if (pRec != NULL)
						{
							nEvalType=pRec->getRecELVEvaluated().getEValType();
							if (nEvalType!=EVAL_TYPE_2)
							{
								// Dialog Calculates Areal (ha) from Length and Width; 080513 p�d
								CArealCalculatorDlg *pDlg = new CArealCalculatorDlg();
								if (pDlg != NULL)
								{
									pDlg->setDlgText(m_mapArealCalculatorDlgText);

									if (pDlg->DoModal() == IDOK)
									{
										pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
										if (pRec != NULL)
										{
											double fAreal = pDlg->getAreal();		
											double fLandValue_per_ha = pRec->getColumnFloat(COLUMN_12);
											pRec->setIconColumnFloat(COLUMN_2,fAreal);
											if (fLandValue_per_ha > 0.0 && fAreal > 0.0)
												pRec->setColumnFloat(COLUMN_10,fAreal*fLandValue_per_ha);

											GetReportCtrl().Populate();
											GetReportCtrl().UpdateWindow();
										}	// if (pRec != NULL)
									}	// if (pDlg->DoModal() == IDOK)

									delete pDlg;
								}	// if (pDlg != NULL)
							}
						}
					}	// if (hitTest_X(pt.x,rect.left,13))
					*/

				}	// if (pCol != NULL && pRow != NULL)
				break;
			}	// case COLUMN_2 :

			case COLUMN_1 :
				{
					pCol = pItemNotify->pColumn;
					pRow = pItemNotify->pRow;
					if (pCol != NULL && pRow != NULL)
					{
						rect = pCol->GetRect();		
						pt = pItemNotify->pt;
						// Check if the user clicked on the Icon or not; 080513 p�d
						if (hitTest_X(pt.x,rect.left,13))
						{

							pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
							if (pRec != NULL)
							{
								if (pRec->getColumnTextConstraintIndex(COLUMN_0) != pRec->getRecELVEvaluated().getEValType())
									nEvalType = pRec->getRecELVEvaluated().getEValType();
								else
									nEvalType = pRec->getColumnTextConstraintIndex(COLUMN_0);
								if (nEvalType == EVAL_TYPE_VARDERING || nEvalType == EVAL_TYPE_ANNAN)
								{
									CTransaction_elv_properties * prop = getActiveProperty();
									CAddEditEvalDialog *pDlg = new CAddEditEvalDialog(prop->getPropObjectID_pk(),prop->getPropID_pk(),m_pDB,pRec->getRecELVEvaluated().getEValID_pk());
									if (pDlg != NULL)
									{
										if (pDlg->DoModal() == IDOK)
										{
											populateReport();
										}
									}
								}
							}
						}	// if (hitTest_X(pt.x,rect.left,13))
					}	// if (pCol != NULL && pRow != NULL)
					break;
				}	// case COLUMN_5 :

		};	// switch (pItemNotify->pColumn->GetItemIndex())
	}
}

void CEvaluatedData::OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CString sMsgCap,sMsg;
	CXTPReportRow *pRow = NULL;
	CELVObjectEvaluatedReportRec *pRec = NULL;
	
	//if (m_nPropertyStatus > STATUS_ADVICED) return;
	if (!canWeCalculateThisProp_cached(m_nPropertyStatus)) return;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		switch (pItemNotify->pColumn->GetItemIndex())
		{
			case COLUMN_0 :
			{
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand and TypOfForrest; 080514 p�d
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						if (nVStandIndex == EVAL_TYPE_VARDERING)	// "V�rderingsbest�nd"
						{
							pRec->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
							pRec->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
							pRec->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
							pRec->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
							pRec->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_4)->SetEditable(TRUE);	// "SI"
							pRec->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
							pRec->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_6)->SetEditable(TRUE);	// "Andel Tall %"
							pRec->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_7)->SetEditable(TRUE);	// "Andel Gran %"
							pRec->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_8)->SetEditable(TRUE);	// "Andel L�v (Bj�rk) %"
							pRec->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
							pRec->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
							pRec->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
							pRec->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
							pRec->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
	//						pRec->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
	//						pRec->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_14)->SetEditable(TRUE);	// "Volym m3sk" 20121204 J� #3379 
							pRec->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_15)->SetEditable(TRUE);	// "Volym m3sk/ha" 20121204 J� #3379 
							pRec->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);

							pRec->GetItem(COLUMN_16)->SetEditable(TRUE);	// "L�ngd [m]" #4207 150114 j�
							pRec->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_17)->SetEditable(TRUE);	// "Bredd [m]" #4207 150114 j�
							pRec->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt" #5086 160819 PL
							pRec->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
							pRec->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_20)->SetEditable(FALSE);	// "Sida" #5171 PL
							pRec->GetItem(COLUMN_20)->SetBackgroundColor(WHITE);

						}	// if (nVStandIndex == EVAL_TYPE_1)
						else if (nVStandIndex == EVAL_TYPE_FROM_TAXERING)	// "Taxeringsbest�nd"
						{
							pRec->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
							pRec->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
							pRec->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
							pRec->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);					
							pRec->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
							pRec->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
							pRec->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
							pRec->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
							pRec->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
							pRec->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
							pRec->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
							pRec->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
							pRec->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
							pRec->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
							pRec->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
	//						pRec->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
	//						pRec->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk" 20121204 J� #3379 
							pRec->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha" 20121204 J� #3379 
							pRec->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_16)->SetEditable(FALSE);	// "L�ngd [m]" #4207 150114 j�
							pRec->GetItem(COLUMN_16)->SetBackgroundColor(WHITE); 
							pRec->GetItem(COLUMN_17)->SetEditable(FALSE);	// "Bredd [m]" #4207 150114 j�
							pRec->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt" #5086 160819 PL
							pRec->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
							pRec->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_19)->SetChecked(FALSE);
							pRec->GetItem(COLUMN_20)->SetEditable(FALSE);	// "Sida" #5171 PL
							pRec->GetItem(COLUMN_20)->SetBackgroundColor(WHITE);
						}	// if (nVStandIndex == EVAL_TYPE_2)
						else if (nVStandIndex == EVAL_TYPE_ANNAN)	// "Annan mark"
						{
							pRec->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
							pRec->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
							pRec->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
							pRec->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
							pRec->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
							pRec->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
							pRec->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
							pRec->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
							pRec->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
							pRec->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
							pRec->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_10)->SetEditable(TRUE);	// "Markv�rde kr"
							pRec->GetItem(COLUMN_10)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
							pRec->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_12)->SetEditable(TRUE);	// "Markv�rde kr/ha"
							pRec->GetItem(COLUMN_12)->SetBackgroundColor(WHITE);
	//						pRec->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
	//						pRec->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk" 20121204 J� #3379 
							pRec->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha" 20121204 J� #3379 
							pRec->GetItem(COLUMN_15)->SetBackgroundColor(LTCYAN);

							pRec->GetItem(COLUMN_16)->SetEditable(TRUE);	// "L�ngd [m]" #4207 150114 j�
							pRec->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_17)->SetEditable(TRUE);	// "Bredd [m]" #4207 150114 j�
							pRec->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt" #5086 160819 PL
							pRec->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
							pRec->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
							pRec->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
							pRec->GetItem(COLUMN_19)->SetChecked(FALSE);
							pRec->GetItem(COLUMN_20)->SetEditable(FALSE);	// "Sida" #5171 PL
							pRec->GetItem(COLUMN_20)->SetBackgroundColor(WHITE);
						}	// if (nVStandIndex == EVAL_TYPE_2)
					}	// if (pRec != NULL)
				} // if (pRow != NULL)
			}
			break;

			// Check changes in column "Markv�rde (kr)"
			case COLUMN_10 :
			{
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						pRec->setColumnFloat(COLUMN_12,0.0);
					}
				}
			}
			break;

			// Check changes in column "Areal" and update "Markv�rde (kr/ha)" and  "Volym (m3sk7ha)" 
			case COLUMN_2 :
			{
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{			
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand and TypOfForrest; 090519 p�d
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						if (nVStandIndex == 2)
						{
							double fAreal = pRec->getIconColumnFloat(COLUMN_2);
							double fValue = pRec->getColumnFloat(COLUMN_12);
							if (fValue > 0.0 && fAreal > 0.0)
								pRec->setColumnFloat(COLUMN_10,fAreal*fValue);
						}
						if (nVStandIndex != 1) //ej taxerad, r�kna ut  m3sk/ha 20121204 J� #3379 
						{ 
							double fAreal = pRec->getIconColumnFloat(COLUMN_2);
							double fValuem3sk = pRec->getColumnFloat(COLUMN_14);
							if (fValuem3sk > 0.0 && fAreal > 0.0)
								pRec->setColumnFloat(COLUMN_15,fValuem3sk/fAreal);
						}
					}
				}
			}
			break;
			// Check changes in column "Markv�rde (kr/ha)" and update "Markv�rde (kr)"
			case COLUMN_12 :
			{
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{			
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand and TypOfForrest; 090519 p�d
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						if (nVStandIndex == 2)
						{
							double fAreal = pRec->getIconColumnFloat(COLUMN_2);
							double fValue = pRec->getColumnFloat(COLUMN_12);
							if (fValue > 0.0 && fAreal > 0.0)
								pRec->setColumnFloat(COLUMN_10,fAreal*fValue);
						}
					}
				}
			}
			break;
			// Check that sum of "tr�dslgsblandning" is less than or equal to 100%; 090622 p�d
			case COLUMN_6 :
			case COLUMN_7 :
			case COLUMN_8 :
			{
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						GetReportCtrl().Populate();
						double fPinePercent = pRec->getColumnFloat(COLUMN_6);
						double fSprucePercent = pRec->getColumnFloat(COLUMN_7);
						double fBirchPercent = pRec->getColumnFloat(COLUMN_8);
						if ((fPinePercent + fSprucePercent + fBirchPercent) > 100.0)
						{
							if (fileExists(m_sLangFN))
							{
								RLFReader xml;
								if (xml.Load(m_sLangFN))
								{
									sMsgCap = xml.str(IDS_STRING229);
									sMsg = xml.str(IDS_STRING22680);
								}	// if (xml->Load(m_sLangFN))
								xml.clean();
							}
							::MessageBox(this->GetSafeHwnd(),sMsg,sMsgCap,MB_ICONEXCLAMATION | MB_OK);
						}	// if ((fPinePercent + fSprucePercent + fBirchPercent) > 100.0)
					}	// if (pRec != NULL)
				}	// if (pRow != NULL)
			}
			break;
			case COLUMN_14://Volym m3sk R�kna ut m3sk/ha 20121204 J� #3379 
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{			
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand 
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						if (nVStandIndex != 1) //Ej taxerad
						{
							double fAreal = pRec->getIconColumnFloat(COLUMN_2);
							double fValue = pRec->getColumnFloat(COLUMN_14);
							if (fValue > 0.0 && fAreal > 0.0)
								pRec->setColumnFloat(COLUMN_15,fValue/fAreal);
						}
					}
				}
				break;
			case COLUMN_15://Volym m3sk/ha R�kna ut m3sk 20121204 J� #3379 
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{			
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand 
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						if (nVStandIndex != 1) //Ej taxerad
						{
							double fAreal = pRec->getIconColumnFloat(COLUMN_2);
							double fValue = pRec->getColumnFloat(COLUMN_15);
							if (fValue > 0.0 && fAreal > 0.0)
								pRec->setColumnFloat(COLUMN_14,fValue*fAreal);
						}
					}
				}
				break;
			case COLUMN_16://L�ngd [m] R�kna ochh uppdatera areal om inte v�rdering fr�n taxering #4207 20150112 J�
			case COLUMN_17://L�ngd [m] R�kna ochh uppdatera areal om inte v�rdering fr�n taxering #4207 20150112 J�
						   // Uppdatera �ven volym per hektar eftersom arealen �ndras #4352 20150508 J�
						   // Om l�ngd och bredd �r 0 s� l�s upp arealsf�ltet
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{			
					pRec = (CELVObjectEvaluatedReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand 
						int nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
						switch(nVStandIndex)
						{
						case EVAL_TYPE_VARDERING:	// "V�rderingsbest�nd"
						case  EVAL_TYPE_ANNAN:	// "Annan mark"
							double fLength=0.0,fWidth=0.0;
							double fAreal=0.0;

							fLength = pRec->getColumnInt(COLUMN_16);
							fWidth = pRec->getColumnInt(COLUMN_17);							

							if(fLength>0.0||fWidth>0.0)
							{
								fAreal=fLength*fWidth/10000.0;
								ChangeAreal(pRow,fAreal);

								//L�s arealsf�lt
								pRec->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
								pRec->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);				
							}
							else//L�s upp arealsf�lt
							{	
								pRec->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
								pRec->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
							}
							break;
						}
					}
				}
				break;

				break;
		};	// switch (pItemNotify->pColumn->GetItemIndex())
	}
}

void CEvaluatedData::OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CMDI1950ForrestNormFrame *pFrame = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		// Show a Popupmenu on rightclick; 070502 p�d
		CPoint curPos;
		CMenu menu;
		CString sMenu1;
		CString sMenu2;
		CString sMenu3;

		CString sMsgCap;
		CString sNoPropMsg;

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				sMenu1 = (xml->str(IDS_STRING22651));
				sMenu2 = (xml->str(IDS_STRING22652));
				sMenu3 = (xml->str(IDS_STRING22653));

				sMsgCap = (xml->str(IDS_STRING229));
				sNoPropMsg.Format(_T("%s\n\n%s\n"),
					 xml->str(IDS_STRING22673),
					 xml->str(IDS_STRING22674));

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}

		// Try to find out how many properties we have.
		// If there's no properties, tell user, that he needs
		// to enter properties, before adding evaluated stands; 090127 p�d
		CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pFormView != NULL)
		{
			CPropertiesFormView *pProp = pFormView->getPropertiesFormView();
			if (pProp != NULL)
			{
				if (pProp->getNumOfProperties() == 0)
				{
					::MessageBox(this->GetSafeHwnd(),sNoPropMsg,sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}
			}
		}

		VERIFY(menu.CreatePopupMenu());
		GetCursorPos(&curPos);
		// create main menu items
		menu.AppendMenu(MF_STRING, ID_TOOLS_ADD_NEW_VSTAND, (sMenu1));
		menu.AppendMenu(MF_STRING, ID_TOOLS_SAVE_CALC_VSTAND, (sMenu2));
		menu.AppendMenu(MF_STRING, ID_TOOLS_REMOVE_SEL_VSTAND, (sMenu3));

		if (!canWeCalculateThisProp_cached(m_nPropertyStatus))
		{
			menu.EnableMenuItem(ID_TOOLS_ADD_NEW_VSTAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_SAVE_CALC_VSTAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_REMOVE_SEL_VSTAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		}
		// track menu
		int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
			curPos.x, curPos.y, this, NULL);
		// other general items
		switch (nMenuResult)
		{
		case ID_TOOLS_ADD_NEW_VSTAND:
			// When this view gets focus, disable the view Add toolbar button; 081007 p�d
			pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pFrame != NULL)	
			{
				pFrame->doAddNewVStandToAvtiveProperty();
				pFrame->doSaveCalculateVStandInAvtiveProperty(false);
				pFrame = NULL;
			}
			break;

		case ID_TOOLS_SAVE_CALC_VSTAND:
			// When this view gets focus, disable the view Add toolbar button; 081007 p�d
			pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pFrame != NULL)	pFrame->doSaveCalculateVStandInAvtiveProperty(false); //#3525 Inte visa meddelande om omr�kning av v�rderingsbest�nd 20120103 J�
			pFrame = NULL;
			break;

		case ID_TOOLS_REMOVE_SEL_VSTAND:
			// When this view gets focus, disable the view Add toolbar button; 081007 p�d
			pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pFrame != NULL)	pFrame->doRemoveCalculateVStandFromAvtiveProperty();
			pFrame = NULL;
			break;
		}
	}
}

void CEvaluatedData::OnReportHeaderColumnRClick(NMHDR * pNotifyStruct, LRESULT * result)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	
	// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
	//menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupBy);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
	/*if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}*/

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER:
		{
			CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pWnd != NULL)
			{
				BOOL bShow = !pWnd->m_wndFieldChooserDlg_evalue.IsVisible();
				pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg_evalue, bShow, FALSE);
			}
		}
		break;

		case ID_SHOW_GROUPBOX:
			//GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;

	}
	menu.DestroyMenu();
}

void CEvaluatedData::getObjectEvaluatedFromDB(int prop_id,int obj_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getObjectEvaluation(prop_id,obj_id,vecELVObjectEvaluated);
	}	// if (m_pDB != NULL)
}

void CEvaluatedData::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
				m_mapArealCalculatorDlgText[IDS_STRING420] = (xml.str(IDS_STRING420));
				m_mapArealCalculatorDlgText[IDS_STRING421] = (xml.str(IDS_STRING421));
				m_mapArealCalculatorDlgText[IDS_STRING422] = (xml.str(IDS_STRING422));
				m_mapArealCalculatorDlgText[IDS_STRING4200] = (xml.str(IDS_STRING4200));
				m_mapArealCalculatorDlgText[IDS_STRING4201] = (xml.str(IDS_STRING4201));
				m_mapArealCalculatorDlgText[IDS_STRING4202] = (xml.str(IDS_STRING4202));
				m_mapArealCalculatorDlgText[IDS_STRING4203] = (xml.str(IDS_STRING4203));

				m_arrTypeOfVStand.Add((xml.str(IDS_STRING4150)));
				m_arrTypeOfVStand.Add((xml.str(IDS_STRING4151)));
				m_arrTypeOfVStand.Add((xml.str(IDS_STRING4152)));
				//m_arrTypeOfVStand.Add((xml.str(IDS_STRING4152)));	Not used for now; 090401 p�d
				//m_arrTypeOfVStand.Add((xml.str(IDS_STRING4153)));	Not used for now;	090401 p�d

				// Add strings; 080416 p�d
				m_sFieldChooser = (xml.str(IDS_STRING105));
				// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
				//m_sGroupBy = (xml.str(IDS_STRING1051));

				m_arrTypeOfLand.Add((xml.str(IDS_STRING4160)));
				m_arrTypeOfLand.Add((xml.str(IDS_STRING4161)));


				m_arrReducMark.Add((xml.str(IDS_STRING42050)));	//Inget
				m_arrReducMark.Add((xml.str(IDS_STRING42051)));	//�verlappande
				m_arrReducMark.Add((xml.str(IDS_STRING42052)));	//Tillf�lligt utnyttjande

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );

					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);
/*
					GetReportCtrl().ShowGroupBy(TRUE);					
*/
					// HMS-136 krash pga grupperingsf�lt s� s�tter det till false 20250204 J�
					GetReportCtrl().ShowGroupBy(FALSE);					

					// Type of evaluation (V�rdering,NOLL-v�rdering)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING4109)), 250));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					//pCol->GetEditOptions()->AddComboButton();
					// Name, description
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING4100)), 250));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetAlignment( DT_LEFT );
					// Areal (ha)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING4101)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					// Age
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING4102)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetAlignment( DT_RIGHT );
					// SI H100
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING4103)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_nMaxLength = 3;	// Max 3 char, i.e. SI in meter, ex. T20 NOT T200; 090504 p�d
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_UPPERCASE; 
					pCol->SetAlignment( DT_LEFT );
					// Correctionfactor
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING4104)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					// Part Pine (Tall)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, (xml.str(IDS_STRING4105)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetAlignment( DT_RIGHT );
					// Part Spruce (Gran)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml.str(IDS_STRING4106)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetAlignment( DT_RIGHT );
					// Part Birch (L�v)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml.str(IDS_STRING4107)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					// Type of forrest (Skogsmark,Kalmark)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml.str(IDS_STRING4108)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					//pCol->GetEditOptions()->AddComboButton();
					// Landvalue (kr)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml.str(IDS_STRING4110)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetAlignment( DT_RIGHT );
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// Precutting value (kr)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml.str(IDS_STRING4111)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );			
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// Landvalue/ha (kr)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml.str(IDS_STRING4112)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetAlignment( DT_RIGHT );
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// Precutting value/ha (kr)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_13, (xml.str(IDS_STRING4113)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					
					/* Tagit bort extra kolumn volym m3sk #3525 20120103 J�
					// Volume m3sk
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, (xml.str(IDS_STRING4114)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;*/

					// Volym m3sk 20121204 J� #3379 
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, (xml.str(IDS_STRING4114)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					//pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetAlignment( DT_RIGHT );
					// Volym m3sk/ha 20121204 J� #3379 
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_15, (xml.str(IDS_STRING4115)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					//pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetAlignment( DT_RIGHT );

					// L�ngd (m)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_16, (xml.str(IDS_STRING4201)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					// Bredd (m)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_17, (xml.str(IDS_STRING4202)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );
					// Skikt
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_18, (xml.str(IDS_STRING4204)), 200));
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetAlignment( DT_RIGHT );

					// Reducerad markers�ttning inget/�verlappande/tillf�lligt utnyttjande
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_19, (xml.str(IDS_STRING4205)), 200));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// Sida #5171
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_20, (xml.str(IDS_STRING4012)), 200));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing(FALSE);
					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().AllowEdit(TRUE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
					GetReportCtrl().FocusSubItems(TRUE);
				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
				xml.clean();
			}	// if (xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))
}

void CEvaluatedData::populateReport(int prop_id,int obj_id,short prop_status,short norm_type)
{
	
	popPropId=prop_id;
	popObjId=obj_id;
	popPropStatus=prop_status;
	popNormType=norm_type;

	CString S;
	CTransaction_eval_evaluation recEval;
	m_nPropertyStatus = prop_status;
	// Get cruise data for Object and Property; 080506 p�d
	getObjectEvaluatedFromDB(prop_id,obj_id);
	GetReportCtrl().BeginUpdate();
	GetReportCtrl().ResetContent();	// Clear
	if (vecELVObjectEvaluated.size() > 0)
	{
		for (UINT i = 0;i < vecELVObjectEvaluated.size();i++)
		{
			recEval = vecELVObjectEvaluated[i];

			//addConstraints();
			GetReportCtrl().AddRecord(new CELVObjectEvaluatedReportRec(recEval.getEValID_pk(),
				recEval.getEValObjID_pk(),
				recEval.getEValPropID_pk(),
				norm_type,
				m_arrTypeOfVStand,
				m_arrTypeOfLand,
				m_nPropertyStatus,
				recEval,
				m_arrReducMark));
		}
	}	// if (vecELVObjectCruise.size() > 0)
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	GetReportCtrl().EndUpdate();

}

int CEvaluatedData::findTypeOfVStandIndex(LPCTSTR vstand)
{
	CString sArr,S;
	if (m_arrTypeOfVStand.GetCount() > 0)
	{
		for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
		{
			sArr = m_arrTypeOfVStand.GetAt(i);

			if (sArr.CompareNoCase(vstand) == 0)
			{
				return i;
			}
		}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
	}	// if (m_arrTypeOfVStand.GetCount() > 0)

	return -1;	// No match
}

int CEvaluatedData::findTypeOfReduceradMark(LPCTSTR reducmarktext)
{
	CString sArr;
	int nSize=m_arrReducMark.GetCount();
	if (nSize > 0)
	{
		for (int i = 0;i < nSize;i++)
		{
			sArr = m_arrReducMark.GetAt(i);

			if (sArr.CompareNoCase(reducmarktext) == 0)
				return i;
		}
	}

	return 0;	// No match
}
int CEvaluatedData::findTypeOfLandIndex(LPCTSTR land)
{
	CString sArr;
	if (m_arrTypeOfLand.GetCount() > 0)
	{
		for (int i = 0;i < m_arrTypeOfLand.GetCount();i++)
		{
			sArr = m_arrTypeOfLand.GetAt(i);

			if (sArr.CompareNoCase(land) == 0)
				return i;
		}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
	}	// if (m_arrTypeOfVStand.GetCount() > 0)

	return -1;	// No match
}

void CEvaluatedData::addConstraints(void)
{
	/*
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pColumns != NULL)
	{
		//--------------------------------------------------------------------------
		// Add type of evaluation stand: "V�rdering","V�rdering startar" etc
		
		CXTPReportColumn *pCol0 = pColumns->Find(COLUMN_0);
		if (pCol0 != NULL)
		{
			// Get constraints for column and remove all items; 071109 p�d
			pCons = pCol0->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_arrTypeOfVStand.GetCount() > 0)
			{
				for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
				{
					pCol0->GetEditOptions()->AddConstraint((m_arrTypeOfVStand.GetAt(i)),i);
				}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
			}	// if (m_arrTypeOfVStand.GetCount() > 0)
		}	// if (pCol0 != NULL)
		//--------------------------------------------------------------------------

		//--------------------------------------------------------------------------
		// Add type of land stand: "Skogsmark","Kalmark" etc
		CXTPReportColumn *pCol9 = pColumns->Find(COLUMN_9);
		if (pCol9 != NULL)
		{
			// Get constraints for column and remove all items; 071109 p�d
			pCons = pCol9->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_arrTypeOfLand.GetCount() > 0)
			{
				for (int i = 0;i < m_arrTypeOfLand.GetCount();i++)
				{
					pCol9->GetEditOptions()->AddConstraint((m_arrTypeOfLand.GetAt(i)),i);
				}	// for (int i = 0;i < m_arrTypeOfLand.GetCount();i++)
			}	// if (m_arrTypeOfVStand.GetCount() > 0)
		}	// if (pCol0 != NULL)
		//--------------------------------------------------------------------------

	}	// if (pColumns != NULL)
	*/
}

void CEvaluatedData::addNewRecordToReport(void)
{
	addConstraints();
	GetReportCtrl().AddRecord(new CELVObjectEvaluatedReportRec(m_arrTypeOfVStand));
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CEvaluatedData::addNewRecordToReportWithDialog(void)
{

	CTransaction_elv_properties * prop = getActiveProperty();
	CAddEditEvalDialog *pDlg = new CAddEditEvalDialog(prop->getPropObjectID_pk(),prop->getPropID_pk(),m_pDB);
	//CAddEditEvalDialog *pDlg = new CAddEditEvalDialog();
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
			populateReport();

			//Select added row.
			CXTPReportRows *pRows = GetReportCtrl().GetRows();
			CELVObjectEvaluatedReportRec* pRec;

			if (pRows != NULL) {
				
				int evalId;
				m_pDB->getObjectEvaluation_last_id(prop->getPropObjectID_pk(),prop->getPropID_pk(),&evalId);

				for (int i = 0;i < pRows->GetCount();i++) {
					pRec = (CELVObjectEvaluatedReportRec*)pRows->GetAt(i)->GetRecord();
					if(pRec->getRecELVEvaluated().getEValID_pk()==evalId) {
						GetReportCtrl().SetFocusedRow(pRows->GetAt(i));
						GetReportCtrl().UpdateWindow();
					}
				}
			}
		}
	}
}




void CEvaluatedData::saveEvalData(double volume)
{
	CString S;
	int nVStandIndex = -1;	// Typeof vstand  "V�rdering","V�rdering start" etc.
	int nForrestIndex = -1;		// TypeofForrest "Skogsmark","Kalmark" etc
	CTransaction_eval_evaluation rec;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CXTPReportRow *pRow = NULL;
	CELVObjectEvaluatedReportRec *pRec = NULL;
	int nEvalID = 1,nMaxEvalID = 0,nDBEvalID = -1,nReduceradMark=0;
	BOOL bDoAdd = TRUE,bOverlap=FALSE,bTillfalligt=FALSE;

	GetReportCtrl().Populate();

	CTransaction_elv_properties* pProperty = getActiveProperty();
	if (pRows != NULL && pProperty != NULL)
	{
		// Check Property status; 090508 p�d
//		if (pProperty->getPropStatus() > STATUS_ADVICED) return;
		if (!canWeCalculateThisProp_cached(pProperty->getPropStatus())) return;
		// Don't do this; 090511 p�d
		//m_pDB->delObjectEvaluation(pProperty->getPropObjectID_pk(),pProperty->getPropID_pk());

		// Is there anything to work with; 080514 p�d
		if (pRows->GetCount() > 0)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRow = pRows->GetAt(i);
				if (pRow != NULL)
				{
					pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord();
					// Find out Index for TypeOfVStand and TypOfForrest; 080514 p�d
					nVStandIndex = findTypeOfVStandIndex(pRec->getColumnText(COLUMN_0));
					nForrestIndex = findTypeOfLandIndex(pRec->getColumnText(COLUMN_9));
					CString sTest=_T("");
					sTest=pRec->getColumnText(COLUMN_19);
					if(sTest.IsEmpty())
						nReduceradMark=EVAL_REDUCMARK_INGET;
					else
					nReduceradMark= findTypeOfReduceradMark(sTest);

					bOverlap=FALSE;
					bTillfalligt=FALSE;
					if(nReduceradMark==EVAL_REDUCMARK_OVERLAPPANDE)
						bOverlap=TRUE;
					else if(nReduceradMark==EVAL_REDUCMARK_TILLFALLIGT)
						bTillfalligt=TRUE;

					if (m_pDB != NULL)
					{
						// Get max evalue id; 090612 p�d
						nDBEvalID = m_pDB->getObjectEvaluated_last_id(pProperty->getPropObjectID_pk(),pProperty->getPropID_pk());
						if (pRec->getRecELVEvaluated().getEValID_pk() < 0)
						{
							nEvalID = nDBEvalID + 1;
							bDoAdd = TRUE;
						}
						else
						{
							nEvalID= pRec->getRecELVEvaluated().getEValID_pk();
							bDoAdd = FALSE;
						}
/*		
						S.Format(_T("PropName %s\nObj %d\nProp %d\nEval %d\nDBEValID %d"),
							pProperty->getPropName(),pProperty->getPropObjectID_pk(),pProperty->getPropID_pk(),nEvalID,nDBEvalID);
						AfxMessageBox(S);
*/

						if (nVStandIndex == EVAL_TYPE_VARDERING || nVStandIndex == EVAL_TYPE_FROM_TAXERING)	// "V�rderings- eller Taxeringsbest�nd"; 090417 p�d
						{
							rec = CTransaction_eval_evaluation(nEvalID,
								pProperty->getPropObjectID_pk(),
								pProperty->getPropID_pk(),
								pRec->getIconColumnText(COLUMN_1),
								pRec->getIconColumnFloat(COLUMN_2),	// Areal
								pRec->getColumnInt(COLUMN_3),	// Age
								pRec->getColumnText(COLUMN_4),
								pRec->getIconColumnFloat(COLUMN_5),
								pRec->getColumnFloat(COLUMN_6),
								pRec->getColumnFloat(COLUMN_7),
								pRec->getColumnFloat(COLUMN_8),
								nForrestIndex,
								nVStandIndex,
								//pRec->getColumnFloat(COLUMN_10),// Bytt ut mot getIconColumnFloat eftersom jag lagt dit en icon HMS-48 20200511
								pRec->getIconColumnFloat(COLUMN_11), //
								//pRec->getColumnFloat(COLUMN_11),  // Bytt ut mot getIconColumnFloat eftersom jag lagt dit en icon HMS-49 20200423
								pRec->getIconColumnFloat(COLUMN_11), //
								pRec->getColumnFloat(COLUMN_12),
								pRec->getColumnFloat(COLUMN_13),
								_T(""),
								pRec->getColumnFloat(COLUMN_14),
								pRec->getColumnInt(COLUMN_16),	// L�ngd #4207 150114 j�
								pRec->getColumnInt(COLUMN_17),	// Bredd #4207 150114 j�
								pRec->getColumnInt(COLUMN_18),	// Skikt #5086 160819 PL
								pRec->getColumnInt(COLUMN_20),  // Sida  #5171 161005 PL
								bOverlap,						// �verlappande mark #4829 160616 PL
								bTillfalligt,   				//#HMS-41 Tillf�lligt utnyttjande 190327 J�
									//HMS-49 In med info om f�rtidig avverkning, tror inte det beh�vs h�r utan det uppdateras vid omr�kning �nd�.
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0,
									0.0
									);
							if (bDoAdd)
								m_pDB->addObjectEvaluation_entered_data(rec);
							else
								m_pDB->updObjectEvaluation_entered_data(rec);
						}																																							
					}	// if (m_pDB != NULL)
				}	// if (pRow != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows->GetCount() > 0)
	}	// if (pRows != NULL)
}

void CEvaluatedData::setDataInReport(CString best_num,CString best_name,CString si,CString tgl,double areal,double volume,int age,double corr_fac,BOOL do_update,double volumeha,int sida,BOOL bTillfUtnyttj)
{

	BOOL bFound = FALSE;
	int nCurPos = 0,nIndex = 0;
	CELVObjectEvaluatedReportRec *pRec = NULL;
	CXTPReportRow *pRow = NULL;
	CString sT,sG,sL,sTGL = tgl;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pRows != NULL)
	{
		if (do_update)
		{
			// We'll check to see if best_name already is used for a Evalue. stand.
			// If so, ask user if he want's to update this stand.
			for (int i = 0;i < pRows->GetCount();i++)
			{
				if ((pRow = pRows->GetAt(i)) != NULL)
				{

					if ((pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord()) != NULL)
					{
						if (pRec->getIconColumnText(COLUMN_1).CompareNoCase(best_name) == 0)
						{
							// First occurence
							bFound = TRUE;
							nIndex = i;				
							break;
						}
					}
				}	// if ((pRow = pRows->GetAt(i)) != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (do_update)


		if (!bFound)
		{
			//HMS-67 20210204 J�
			//kolla om man hittar det nyskapade v�rderingsbest�ndet 
			//iom att sorteringsordningen g�r att det nyskapade nollade v�rderingsbest�ndet kan hamna lite varstans i listan
			//och inte n�dv�ndigtvis hamnar sist vilket antogs f�rut
			for (int i = 0;i < pRows->GetCount();i++)
			{
				if ((pRow = pRows->GetAt(i)) != NULL)
				{		
					if ((pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord()) != NULL)
					{
						//Koilla om namnet �r tomt, arealen 0 markv�rde 0 samt typen �r -1 (betyder att den inte �r sparad) i s� fall �r det det nya tomma
						//v�rderingen som skall fyllas i
						//HMS-67 20220215 J� Kollar p� objektet i bakgrunden(pRec->getRecELVEvaluated()) ist�llet f�r p� kolumnen p� raden
						if (pRec->getRecELVEvaluated().getEValName().GetLength()<=0 && 
							pRec->getRecELVEvaluated().getEValAreal()<=0.0 && 
							pRec->getRecELVEvaluated().getEValLandValue()<=0.0 &&
							pRec->getRecELVEvaluated().getEValType()==-1)
						{
							// First occurence
							bFound = TRUE;
							nIndex = i;
							break;
						}
					}
				}	// if ((pRow = pRows->GetAt(i)) != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)

			if (!bFound)
			nIndex = pRows->GetCount() - 1;
		}
		if (nIndex >= 0 && nIndex < pRows->GetCount())
		{
			if ((pRow = pRows->GetAt(nIndex)) != NULL)
			{
		
				if ((pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord()) != NULL)
				{
					if (sTGL.Right(sTGL.GetLength()) != L";")
						sTGL += L";";		

					sT = sTGL.Tokenize(L";",nCurPos);
					sG = sTGL.Tokenize(L";",nCurPos);
					sL = sTGL.Tokenize(L";",nCurPos);

					pRec->setIconColumnText(COLUMN_1,best_name);
					pRec->setColumnFloat(COLUMN_2, areal);
					pRec->setColumnInt(COLUMN_3, age);
					pRec->setColumnText(COLUMN_4, si.MakeUpper());
					pRec->setColumnFloat(COLUMN_5, corr_fac);
					pRec->setColumnFloat(COLUMN_6, _tstof(sT));
					pRec->setColumnFloat(COLUMN_7, _tstof(sG));
					pRec->setColumnFloat(COLUMN_8, _tstof(sL));                    
					pRec->setColumnFloat(COLUMN_14, volume);
					pRec->setColumnFloat(COLUMN_15, volumeha);//#3525 J� 20130103
					pRec->setColumnInt(COLUMN_20, sida); //Sida
					

					RLFReader *xml = new RLFReader;
					if (xml->Load(m_sLangFN))
					{

						//#HMS-88 S�tt default till behandlad om man g�r ett nyt v�rderingsbest�nd
						if(!do_update)
							pRec->setColumnText(COLUMN_9,xml->str(IDS_STRING4160));

						if(bTillfUtnyttj)
							pRec->setColumnText(COLUMN_19,xml->str(IDS_STRING42052));
						else
							pRec->setColumnText(COLUMN_19,xml->str(IDS_STRING42050));
					}
					delete xml;


					GetReportCtrl().Populate();
					GetReportCtrl().UpdateWindow();
				}
			}	// if ((pRow = pRows->GetAt(nIndex)) != NULL)
		}	// if (nIndex >= 0 && nIndex < pRows->GetCount())
	}
}


void CEvaluatedData::removeSelectedEvaluation(short prop_status,short norm_type)
{
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CELVObjectEvaluatedReportRec *pRec = NULL;
	
	if (pRow != NULL)
	{
		pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			CTransaction_eval_evaluation rec = pRec->getRecELVEvaluated();	
			m_pDB->delObjectEvaluation(rec.getEValID_pk(),rec.getEValObjID_pk(),rec.getEValPropID_pk());

			populateReport(rec.getEValPropID_pk(),rec.getEValObjID_pk(),prop_status,norm_type);
		}	// if (pRec != NULL & m_pDB != NULL)
	}	// if (pRow != NULL)
}

void CEvaluatedData::removeEvaluations(void)
{
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CELVObjectEvaluatedReportRec *pRec = NULL;
	
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CELVObjectEvaluatedReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL && m_pDB != NULL)
			{
				CTransaction_eval_evaluation rec = pRec->getRecELVEvaluated();	
				m_pDB->delObjectEvaluation(rec.getEValID_pk(),rec.getEValObjID_pk(),rec.getEValPropID_pk());

			}	// if (pRec != NULL & m_pDB != NULL)
		}	// if (pRow != NULL)
//		populateReport(rec.getEValPropID_pk(),rec.getEValObjID_pk());
	}
}

void CEvaluatedData::getSelectedEvaluated(CTransaction_eval_evaluation& rec,CString& si,int* age,double* pine_perc,double* spruce_perc,double *birch_perc)
{
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CELVObjectEvaluatedReportRec *pRec = NULL;
	
	GetReportCtrl().Populate();	// Update record; 090911 p�d
	if (pRow != NULL)
	{
		pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			rec = pRec->getRecELVEvaluated();
			si = pRec->getColumnText(COLUMN_4);
			*age = pRec->getColumnInt(COLUMN_3);
			*pine_perc = pRec->getColumnFloat(COLUMN_6);
			*spruce_perc = pRec->getColumnFloat(COLUMN_7);
			*birch_perc = pRec->getColumnFloat(COLUMN_8);
		}	// if (pRec != NULL & m_pDB != NULL)
	}	// if (pRow != NULL)
}


void CEvaluatedData::setManuallyEnteredCorrFactor(CTransaction_eval_evaluation& rec,double corr_factor)
{
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CELVObjectEvaluatedReportRec *pRec = NULL;
	
	if (pRow != NULL)
	{
		pRec = (CELVObjectEvaluatedReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			m_pDB->updObjectEvaluation_calculated_corrfac(rec.getEValID_pk(),rec.getEValObjID_pk(),rec.getEValPropID_pk(),corr_factor);
			pRec->setIconColumnFloat(COLUMN_5,corr_factor);
			GetReportCtrl().Populate();
			GetReportCtrl().UpdateWindow();
		}	// if (pRec != NULL & m_pDB != NULL)
//-----------------------------------------------------------------------------------------------------
/*	Uncomment this, to do a recalculation of VStand; 090914 p�d
		// Do a recalc of Evaluated stands; 090911 p�d
		CMDI1950ForrestNormFrame* pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
		if (pFrame != NULL)	pFrame->doSaveCalculateVStandInAvtiveProperty();
		pFrame = NULL;
*/
//-----------------------------------------------------------------------------------------------------
	}	// if (pRow != NULL)
}

void CEvaluatedData::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_EVALUATED_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	m_bLoadReportOK = true;

}

void CEvaluatedData::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_EVALUATED_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

//////////////////////////////////////////////////////////////////////////////
// CCruisingData

IMPLEMENT_DYNCREATE(CCruisingData,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCruisingData,  CXTPReportView) //CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportChanged)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(NM_RCLICK, XTP_ID_REPORT_CONTROL, OnReportItemRClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportHeaderColumnRClick)
END_MESSAGE_MAP()

CCruisingData::CCruisingData()	
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

void CCruisingData::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST_CRUISE, &pWnd->m_wndFieldChooserDlg_cruise);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (!m_bInitialized)
	{

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport();

		LoadReportState();

		m_bInitialized = TRUE;
	}

}

BOOL CCruisingData::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CCruisingData::OnDestroy()
{
	// Try to clear records on exit (for memory deallocation); 080215 p�d
	GetReportCtrl().ResetContent();

	if (m_pDB != NULL)
		delete m_pDB;

	m_ilIcons.DeleteImageList();

	SaveReportState();

	CXTPReportView::OnDestroy();	
}

BOOL CCruisingData::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CCruisingData::getObjectCruiseFromDB(int prop_id,int obj_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getObjectCruises(prop_id,obj_id,vecELVObjectCruise);
	}	// if (m_pDB != NULL)
}

void CCruisingData::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );

					m_sarrYesNo.Add((xml->str(IDS_STRING110)));
					m_sarrYesNo.Add((xml->str(IDS_STRING111)));

					m_sWidthErrMsg.Format(_T("%s\n\n%s\n%s\n%s\n\n%s\n\n"),
						(xml->str(IDS_STRING1300)),
						(xml->str(IDS_STRING1301)),
						(xml->str(IDS_STRING1302)),
						(xml->str(IDS_STRING1303)),
						(xml->str(IDS_STRING1304)));

					m_sSideNumberMsg.Format(L"%s\n\n%s\n%s\n\n",xml->str(IDS_STRING1305),xml->str(IDS_STRING1306),xml->str(IDS_STRING1307));

					m_sEvalueUpdateMsg.Format(L"%s\n\n%s",xml->str(IDS_STRING4305),xml->str(IDS_STRING4306));
					
					m_arrTypeOfStand.RemoveAll();
					m_arrTypeOfStand.Add((xml->str(IDS_STRING4140)));
					m_arrTypeOfStand.Add((xml->str(IDS_STRING4141)));

					// Add strings; 090610 p�d
					m_sFieldChooser = (xml->str(IDS_STRING105));
					// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
					//m_sGroupBy = (xml->str(IDS_STRING1051));

					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					// "Typ av taxering; Auto , Manuell"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, xml->str(IDS_STRING4014), 150));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					//pCol->GetEditOptions()->AddComboButton();
					// "Best�ndsnummer"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING4000)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->SetAlignment( DT_LEFT );
					// "Best�ndsnamn"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING4001)), 120));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->SetAlignment( DT_LEFT );
					// "Antal tr�d"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING4002)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Areal"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING4003)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Volym"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING4004)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "SI"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, xml->str(IDS_STRING4015), 80));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->SetAlignment( DT_LEFT );
					// "Virkesv�rde
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING4005)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Kostnader"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING4006)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "ROTNETTO"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING4007)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Volym Storm- och Torkskador"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml->str(IDS_STRING4008)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "V�rde Storm- och Torkskador"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml->str(IDS_STRING4009)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Summa volym, kanttr�d"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml->str(IDS_STRING4010)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Summa, v�rde kanttr�d"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_13, (xml->str(IDS_STRING4011)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Sida; breddning"
					// Value for which side the stand's been cruised (breddning, ny norm); 080922 p�d
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, (xml->str(IDS_STRING4012)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );
					// "Annan bredd"
					// Value for width "Breddning", overrides Object value; 090625 p�d
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_15, xml->str(IDS_STRING4013), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "Graninblandning"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_16, xml->str(IDS_STRING4016), 100));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "TGL Manuellt inl�st best�nd"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_17, xml->str(IDS_STRING4017), 100));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "Virkesv�rde/m3fub"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_18, xml->str(IDS_STRING4018), 100));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "Grot (ton)"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_19, xml->str(IDS_STRING4019), 100));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "Grot v�rde (kr)"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_20, xml->str(IDS_STRING4020), 50));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "Grot kostnader (kr)"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_21, xml->str(IDS_STRING4021), 50));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					// "Bara virkesv�rde"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_22, xml->str(IDS_STRING4022), 50));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment( DT_RIGHT );

					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing(FALSE);
					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().AllowEdit(TRUE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
					GetReportCtrl().FocusSubItems(TRUE);
				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
}

void CCruisingData::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	int nVStandIndex = -1;
	int nCol=-1;
	CXTPReportColumn *pCol = NULL;
	CXTPReportRow *pRow = NULL;
	CELVObjectCruiseReportRec *pRec = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties* pProperty = getActiveProperty();
	vecTransaction_eval_evaluation vecEval;
	BOOL bTillfUtnyttj=FALSE;
	
	

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		nCol=pItemNotify->pColumn->GetItemIndex();
		if (!canWeCalculateThisProp_cached(m_nPropertyStatus)) 
		{
			if(!(nCol==COLUMN_11)) //Visa info om storm o tork �ven om status p� fastighet satt till l�st HMS-50 20200625 J�
			return;
		}
		switch (nCol)
		{
			/*
		case COLUMN_10:	//Markv�rde
			// HMS-48 Info om markv�rde 20200428 J�
			pCol = pItemNotify->pColumn;
			pRow = pItemNotify->pRow;
			if (pCol != NULL && pRow != NULL)
			{
				rect = pCol->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not;
				if (hitTest_X(pt.x,rect.left,13))
				{
					pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						if (pRec->getTypeIndex() == CRUISE_TYPE_1 || pRec->getTypeIndex() == CRUISE_TYPE_2)
						{
							CShowInformationDlgMarkvarde *pDlg = new CShowInformationDlgMarkvarde();
							if (pDlg != NULL)				
							{			
								pDlg->setCruise(pRec->getRecELVCruise());
								pDlg->setDBConnection(m_pDB);		
								pDlg->setNorm(pObj->getObjUseNormID());
								pDlg->DoModal();
								delete pDlg;
							}
						}
					}
				}
			}
			break;
			*/
		case COLUMN_11:	//V�rde Storm/Tork (kr)
			//HMS-50 Info om hur storm och tork �r ber�knat 2020-02-12 J�
			pCol = pItemNotify->pColumn;
			pRow = pItemNotify->pRow;
			if (pCol != NULL && pRow != NULL)
			{
				rect = pCol->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not;
				if (hitTest_X(pt.x,rect.left,13))
				{
					pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						if (pRec->getTypeIndex() == CRUISE_TYPE_1 || pRec->getTypeIndex() == CRUISE_TYPE_2)
						{
							CShowInformationDlgStormTork *pDlg = new CShowInformationDlgStormTork();
							if (pDlg != NULL)				
							{			
								pDlg->setCruise(pRec->getRecELVCruise());
								pDlg->setDBConnection(m_pDB);		
								pDlg->setNorm(pObj->getObjUseNormID());
								pDlg->DoModal();
								delete pDlg;
							}
						}
					}
				}
			}
			break;
			case COLUMN_2 :
			{
				BOOL bFound = FALSE,bDoUpdate = FALSE;
				double fCorr = 0.0,fVolPerHA = 0.0;
				int nAge = 0,ret = 0;
				pCol = pItemNotify->pColumn;
				pRow = pItemNotify->pRow;
				if (pCol != NULL && pRow != NULL)
				{
					rect = pCol->GetRect();		
					pt = pItemNotify->pt;
					// Check if the user clicked on the Icon or not; 080513 p�d
					if (hitTest_X(pt.x,rect.left,13))
					{
						pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							if (pRec->getTypeIndex() == CRUISE_TYPE_1)
							{
								openStand();
							}
							else if (pRec->getTypeIndex() == CRUISE_TYPE_2)
							{
								if (m_pDB != NULL)
								{
									m_pDB->getObjectEvaluation(pProperty->getPropID_pk(),pProperty->getPropObjectID_pk(),vecEval);
								}
								bFound = FALSE;
								if (vecEval.size() > 0)
								{
									for (int i = 0;i < vecEval.size();i++)
									{
										if (vecEval[i].getEValName().Compare(pRec->getColumnIconText(COLUMN_2).Trim()) == 0)
										{
											nAge = vecEval[i].getEValAge();
											bFound = TRUE;
											break;
										}
									}
								}	// if (vecEval.size() > 0)

								CAddTGLDlg *pDlg = new CAddTGLDlg();
								if (pDlg != NULL)
								{
									pDlg->setStandNum(pRec->getColumnText(COLUMN_1));
									pDlg->setStandName(pRec->getColumnIconText(COLUMN_2));
									pDlg->setStandSI(pRec->getColumnIconText(COLUMN_6));
									pDlg->setStandAreal(pRec->getColumnFloat(COLUMN_4));
									pDlg->setStandVolume(pRec->getColumnFloat(COLUMN_5));
									pDlg->setTGL(pRec->getColumnText(COLUMN_17));
									pDlg->setStandAge(nAge);
									pDlg->setStandSide(pRec->getColumnInt(COLUMN_14));																	
									pDlg->setTillfUtnyttj(pRec->getRecELVCruise().getECruTillfUtnyttj());

									if (pDlg->DoModal() == IDOK)
									{
										pRec->setColumnText(COLUMN_1,pDlg->getStandNum());
										pRec->setColumnIconText(COLUMN_2,pDlg->getStandName());
										pRec->setColumnText(COLUMN_6,pDlg->getStandSI());
										pRec->setColumnFloat(COLUMN_4,pDlg->getStandAreal());
										pRec->setColumnFloat(COLUMN_5,pDlg->getStandVol());
										pRec->setColumnText(COLUMN_17,pDlg->getTGL());
										pRec->setColumnInt(COLUMN_14,pDlg->getStandSide());
  
										pRec->getRecELVCruise().setECruTillfUtnyttj(pDlg->getTillfUtnyttj());

										saveCruiseData();

										// HMS-16 Flyttar doCalculateManuallyEnteredCruises pga  i den k�rs 
										// setSelectedPropertyId som i s� fall triggar funktionen l�s in v�rderingar och skriv ut dem 
										// i v�rderingsf�nstret innan nuvarande tillagda v�rdering sparats i DB																														
										/*CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
										if (pFrame != NULL)	pFrame->doCalculateManuallyEnteredCruises();
										pFrame = NULL;*/
										
										if (pDlg->getDoCreateVStand())
										{
											if (bFound)
											{
												ret = ::MessageBox(this->GetSafeHwnd(),m_sEvalueUpdateMsg,global_langMap[IDS_STRING229],MB_ICONQUESTION | MB_YESNOCANCEL);
												if (ret == IDYES)
												{
													bDoUpdate = TRUE;
												}
												else if (ret == IDNO)
												{
													bDoUpdate = FALSE;
												}
											}
											// If cancel, nothing'll happen
											if (ret != IDCANCEL)
											{
												CMDI1950ForrestNormFormView *pView = NULL;
												CPropertiesFormView *pPropView = NULL;
												if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
												{
													if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
													{
														// HMS-16 Sparar undan fastighetsid h�r ist�llet f�r i addNewVStandToActiveProperty funktionen pga 
														// setSelectedPropertyId i s� fall triggar funktionen l�s in v�rderingar och skriv ut dem 
														// i v�rderingsf�nstret innan nuvarande tillagda v�rdering sparats i DB
														int propertyId = pPropView->getSelectedPropertyId();

														bTillfUtnyttj=pDlg->getTillfUtnyttj();
														// Just add a new row to Evaluation list; 2012-12-06 P�D #3380
														if (!bDoUpdate)
														{		//HMS-16
															pPropView->addNewVStandToActiveProperty();
														}
														// Add data, save and calculate; 2012-12-06 P�D #3380
														if (pDlg->getStandVol() > 0.0 && pDlg->getStandAreal() > 0.0)
															fVolPerHA = pDlg->getStandVol()/pDlg->getStandAreal();
														else
															fVolPerHA = pDlg->getStandVol();
														pPropView->caclulateCorrFactor(fVolPerHA,
															pDlg->getTGL(),
															pDlg->getStandSI(),
															pDlg->getStandAge(),
															&fCorr);
														pPropView->saveAndCalculateVStandInActiveProperty(pDlg->getStandNum(),
															pDlg->getStandName(),
															pDlg->getStandSI(),
															pDlg->getTGL(),
															pDlg->getStandAreal(),
															pDlg->getStandVol(),
															pDlg->getStandAge(),
															fCorr,
															bDoUpdate,
															pDlg->getStandSide(),
															bTillfUtnyttj);
														
														// HMS-16 Sparar undan fastighetsid h�r ist�llet
														pPropView->setSelectedPropertyId(propertyId);

													}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
												}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
												pView = NULL;
												pPropView = NULL;
											}
										}
									    // HMS-16 Flyttat doCalculateManuallyEnteredCruises hit ist�llet n�r v�rderingen sparats pga  i den k�rs 
										// setSelectedPropertyId som i s� fall triggar funktionen l�s in v�rderingar och skriv ut dem 
										// i v�rderingsf�nstret innan nuvarande tillagda v�rdering sparats i DB																														
										CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
										if (pFrame != NULL)	pFrame->doCalculateManuallyEnteredCruises();
										pFrame = NULL;

									}
									delete pDlg;
								}
							}
						}
					}	// if (hitTest_X(pt.x,rect.left,13))
				}	// if (pCol != NULL && pRow != NULL)
				break;
			}	// case COLUMN_2 :
/*	Commented out 2012-12-05 P�D #3380
			case COLUMN_17 :
			{
				pCol = pItemNotify->pColumn;
				pRow = pItemNotify->pRow;
				if (pCol != NULL && pRow != NULL)
				{
					rect = pCol->GetRect();		
					pt = pItemNotify->pt;
					// Check if the user clicked on the Icon or not; 080513 p�d
					if (hitTest_X(pt.x,rect.left,13))
					{
						pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							int nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
							if (nStandIndex == CRUISE_TYPE_2)	// "Manuellt inl�st"
							{
								CAddTGLDlg *pDlg = new CAddTGLDlg();
								if (pDlg != NULL)
								{
									pDlg->setTGL(pRec->getColumnIconText(COLUMN_17));
									if (pDlg->DoModal() == IDOK)
									{
										pRec->setColumnIconText(COLUMN_17,pDlg->getTGL());
									}
									delete pDlg;
								}
							}
						}
					}	// if (hitTest_X(pt.x,rect.left,13))
				}	// if (pCol != NULL && pRow != NULL)
				break;
			}	// case COLUMN_17 :
*/
			case COLUMN_22 :
			{
/*
				// Create the progress window control; 100203 p�d
				CProgressDlg2 progDlg;
				if (!progDlg.Create(CProgressDlg2::IDD,this))
				{
					TRACE0("Failed to create progress window control.\n");
				}
				if (progDlg.GetSafeHwnd())
				{
					progDlg.ShowWindow(SW_NORMAL);
				}
*/
				pCol = pItemNotify->pColumn;
				pRow = pItemNotify->pRow;
				if (pCol != NULL && pRow != NULL)
				{
					pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						int nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
						if (nStandIndex == CRUISE_TYPE_1)	// "Taxerat best�nd"
						{
							if (m_pDB) 
							{
/*	
								if (progDlg.GetSafeHwnd())
								{
									progDlg.setProgressPos(10);
								}
*/
								GetReportCtrl().Populate();
								GetReportCtrl().UpdateWindow();
								//HMS-18 Test
								int nBaraVirkesVarde=pRec->getColumnCheck(COLUMN_22);
								m_pDB->updObjectCruise_use_for_infr(pRec->getRecELVCruise().getECruID_pk(),
																										pRec->getRecELVCruise().getECruPropID_pk(),
																										pRec->getRecELVCruise().getECruObjectID_pk(),
																										nBaraVirkesVarde);
								m_pDB->updTrakts_use_for_infr(pRec->getRecELVCruise().getECruID_pk(),
																							nBaraVirkesVarde);
								if (pObj != NULL && pProperty != NULL)
								{
									if (pRec->getColumnCheck(COLUMN_22))
									{
/*
										if (progDlg.GetSafeHwnd())
										{
											progDlg.setProgressPos(20);
										}
*/
										/* #3260, reopen, tagit bort nollst�llning av v�rden
										m_pDB->updObjectCruise_randtrees(pRec->getRecELVCruise().getECruID_pk(),
																												pRec->getRecELVCruise().getECruPropID_pk(),
																												pRec->getRecELVCruise().getECruObjectID_pk(),
																												0.0,
																												0.0);*/
/*
										if (progDlg.GetSafeHwnd())
										{
											progDlg.setProgressPos(30);
										}
*/
										/* #3260, reopen, tagit bort nollst�llning av v�rden
										m_pDB->updObjectCruise_storm_and_dry(pRec->getRecELVCruise().getECruID_pk(),
																												pRec->getRecELVCruise().getECruPropID_pk(),
																												pRec->getRecELVCruise().getECruObjectID_pk(),
																												0.0,
																												0.0,
																												0.0);*/
										populateReport(pRec->getRecELVCruise().getECruPropID_pk(),pRec->getRecELVCruise().getECruObjectID_pk(),pProperty->getPropStatus(),pObj->getObjUseNormID());
									}	// if (!pRec->getColumnCheck(COLUMN_22))
								}
/*
								if (progDlg.GetSafeHwnd())
								{
									progDlg.setProgressPos(75);
								}

								CMDI1950ForrestNormFormView *pView = NULL;
								CPropertiesFormView *pPropView = NULL;
								if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
								{
									if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
									{
										pPropView->matchStandsPerPropertyForObject(CProgressDlg());
									}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
								}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
								pView = NULL;
								pPropView = NULL;

								if (progDlg.GetSafeHwnd())
								{
									progDlg.setProgressPos(95);
								}
*/
								// When this view gets focus, disable the view Add toolbar button; 081007 p�d
								CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
								if (pFrame != NULL)	
									pFrame->runCalulation(pRec->getRecELVCruise().getECruPropID_pk());
									//pFrame->doCreateEvaluationFromCruise();
/*
								if (progDlg.GetSafeHwnd())
								{
									progDlg.setProgressPos(100);
									progDlg.ShowWindow(SW_HIDE);
									DeleteObject(progDlg);
								}
*/
							}
						}
					}
				}	// if (pCol != NULL && pRow != NULL)
				break;
			}	// case COLUMN_22 :
		};	// switch (pItemNotify->pColumn->GetItemIndex())
	}
}

void CCruisingData::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	// Don't open Stand if Status > STATUS_ADVICED; 090507 p�d
	// NOT USED, USE ICON-CLICK IN COLUMN, INSTEAD; 090904 p�d
	//if (m_nPropertyStatus > STATUS_ADVICED) return;
	//openStand();
}

void CCruisingData::OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CString sMsgCap,sMsg;
	CXTPReportRow *pRow = NULL;
	CELVObjectCruiseReportRec *pRec = NULL;
	
//	if (m_nPropertyStatus > STATUS_ADVICED) return;
	if (!canWeCalculateThisProp_cached(m_nPropertyStatus)) return;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		switch (pItemNotify->pColumn->GetItemIndex())
		{
			case COLUMN_0 :
			{
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{
					pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						// Find out Index for TypeOfVStand and TypOfForrest; 080514 p�d
						int nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
						if (nStandIndex == CRUISE_TYPE_1)	// "Taxerat best�nd"
						{
							// "Typ av best�nd"
							pRec->GetItem(COLUMN_0)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
							// "Best�ndsnummer"
							pRec->GetItem(COLUMN_1)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
							// "Best�ndsnamn"
							pRec->GetItem(COLUMN_2)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
							// "Antal tr�d"
							pRec->GetItem(COLUMN_3)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
							// "Areal"
							pRec->GetItem(COLUMN_4)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
							// "Volym"
							pRec->GetItem(COLUMN_5)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_5)->SetBackgroundColor(LTCYAN);
							// "SI"
							pRec->GetItem(COLUMN_6)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_6)->SetBackgroundColor(LTCYAN);
							// "Virkesv�rde"
							pRec->GetItem(COLUMN_7)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
							// "Kostnader"
							pRec->GetItem(COLUMN_8)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
							// "Rotnetto"
							pRec->GetItem(COLUMN_9)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
							// "Volym SoT"
							pRec->GetItem(COLUMN_10)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
							// "V�rde SoT"
							pRec->GetItem(COLUMN_11)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
							// "Summa volym kanttr�d"
							pRec->GetItem(COLUMN_12)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
							// "Summa v�rde kanttr�d"
							pRec->GetItem(COLUMN_13)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
							// "Sida 1 el. 2, breddning"
							pRec->GetItem(COLUMN_14)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_14)->SetBackgroundColor(LTCYAN);
							// "Annan bredd"
							pRec->GetItem(COLUMN_15)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);
							// "Gran-inblandning"
							pRec->GetItem(COLUMN_16)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
							// "TGL manuellt inl�st"
							pRec->GetItem(COLUMN_17)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
							// "Virkesv�rde/m3fub"
							pRec->GetItem(COLUMN_18)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
							// "Grot (ton)"
							pRec->GetItem(COLUMN_19)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
							// "Grot v�rde (kr)"
							pRec->GetItem(COLUMN_20)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
							// "Grot kostnad (kr)"
							pRec->GetItem(COLUMN_21)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
							// "Bara virkesv�rde"
							pRec->GetItem(COLUMN_22)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_22)->SetBackgroundColor(WHITE);

						}	// if (nVStandIndex == CRUISE_TYPE_1)
						else if (nStandIndex == CRUISE_TYPE_2)	// "Manuellt inl�st best�nd"
						{
							// "Typ av best�nd"
							pRec->GetItem(COLUMN_0)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
							// "Best�ndsnummer"
							pRec->GetItem(COLUMN_1)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
							// "Best�ndsnamn"
							pRec->GetItem(COLUMN_2)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
							// "Antal tr�d"
							pRec->GetItem(COLUMN_3)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
							// "Areal"
							pRec->GetItem(COLUMN_4)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
							// "SI"
							pRec->GetItem(COLUMN_5)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
							// "Volym"
							pRec->GetItem(COLUMN_6)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
							// "Virkesv�rde"
							pRec->GetItem(COLUMN_7)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_7)->SetBackgroundColor(LTCYAN);
							// "Kostnader"
							pRec->GetItem(COLUMN_8)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_8)->SetBackgroundColor(LTCYAN);
							// "Rotnetto"
							pRec->GetItem(COLUMN_9)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_9)->SetBackgroundColor(LTCYAN);
							// "Volym SoT"
							pRec->GetItem(COLUMN_10)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_10)->SetBackgroundColor(LTCYAN);
							// "V�rde SoT"
							pRec->GetItem(COLUMN_11)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_11)->SetBackgroundColor(LTCYAN);
							// "Summa volym kanttr�d"
							pRec->GetItem(COLUMN_12)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_12)->SetBackgroundColor(LTCYAN);
							// "Summa v�rde kanttr�d"
							pRec->GetItem(COLUMN_13)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_13)->SetBackgroundColor(LTCYAN);
							// "Sida 1 el. 2, breddning"
							pRec->GetItem(COLUMN_14)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
							// "Annan bredd"
							pRec->GetItem(COLUMN_15)->SetEditable(TRUE);	
							pRec->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);
							// "Gran-inblandning"
							pRec->GetItem(COLUMN_16)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_16)->SetBackgroundColor(LTCYAN);
							// "TGL manuellt inl�st"
							pRec->GetItem(COLUMN_17)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_17)->SetBackgroundColor(LTCYAN);
							// "Virkesv�rde/m3fub"
							pRec->GetItem(COLUMN_18)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
							// "Grot (ton)"
							pRec->GetItem(COLUMN_19)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_19)->SetBackgroundColor(LTCYAN);
							// "Grot v�rde (kr)"
							pRec->GetItem(COLUMN_20)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_20)->SetBackgroundColor(LTCYAN);
							// "Grot kostnad (kr)"
							pRec->GetItem(COLUMN_21)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_21)->SetBackgroundColor(LTCYAN);
							// "Bara virkesv�rde"
							pRec->GetItem(COLUMN_22)->SetEditable(FALSE);	
							pRec->GetItem(COLUMN_22)->SetBackgroundColor(LTCYAN);

						}	// if (nVStandIndex == CRUISE_TYPE_2)
					}	// if (pRec != NULL)
				} // if (pRow != NULL)
			}
			break;

			case COLUMN_14 :
			{
				int nIndex = pItemNotify->pRow->GetIndex();
				int nOldValue = -1;				
				int nValue = -1;				
				CString S;
				pRow = pItemNotify->pRow;
				if (pRow != NULL)
				{
					if (nIndex >= 0 && nIndex < vecELVObjectCruise.size())
					{
						nOldValue = vecELVObjectCruise[nIndex].getECruUseSampleTrees();
						if (nOldValue < 1 || nOldValue > 2)
							nOldValue = 1;
					}
					GetReportCtrl().Populate();
					pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
					if (pRec != NULL)
					{
						nValue = pRec->getColumnInt(COLUMN_14);
						if (nValue < 1 || nValue > 2)
						{
							::MessageBox(this->GetSafeHwnd(),m_sSideNumberMsg,global_langMap[IDS_STRING295],MB_ICONASTERISK | MB_OK);
							// Reset back to original value, if within limits
							pRec->setColumnInt(COLUMN_14,nOldValue);
							GetReportCtrl().Populate();
							GetReportCtrl().UpdateWindow();
						}
					}
				}
				break;
			}

		};	// switch (pItemNotify->pColumn->GetItemIndex())
	}
}

void CCruisingData::OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CMDI1950ForrestNormFrame *pFrame = NULL;
	CELVObjectCruiseReportRec *pRec = NULL;
	CXTPReportRow *pRow = NULL;
	int nStandIndex;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		// Show a Popupmenu on rightclick; 070502 p�d
		CPoint curPos;
		CMenu menu;
		CString sMenu;
		CString sMenu0;
		CString sMenu1;
		CString sMenu2;
		CString sMenu3;
		CString sMenu4;
		CString sMenu5;
		CString sMenu6;
		CString sMsgCap;
		CString sNoPropMsg;

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				sMenu	 = (xml->str(IDS_STRING22648));
				sMenu0 = (xml->str(IDS_STRING22649));
				sMenu1 = (xml->str(IDS_STRING22650));
				sMenu2 = (xml->str(IDS_STRING22657));
				sMenu3 = (xml->str(IDS_STRING22666));
				sMenu4 = (xml->str(IDS_STRING22672));
				sMenu5 = (xml->str(IDS_STRING22707));	// #4554
				sMenu6 = (xml->str(IDS_STRING22708));	// #4554

				sMsgCap = (xml->str(IDS_STRING229));
				sNoPropMsg.Format(_T("%s\n\n%s\n"),
					 xml->str(IDS_STRING22673),
					 xml->str(IDS_STRING22675));

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}

		pRow = GetReportCtrl().GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
			if (pRec != NULL)
			{
				// Find out Index for TypeOfStand; 090907 p�d
				// Only "Taxerade best�nd", can be opened; 090907 p�d
				nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
			}
		}
		// Try to find out how many properties we have.
		// If there's no properties, tell user, that he needs
		// to enter properties, before adding evaluated stands; 090127 p�d
		CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		CPropertiesFormView *pProp = NULL;
		if (pFormView != NULL)
		{
			pProp = pFormView->getPropertiesFormView();
			if (pProp != NULL)
			{
				if (pProp->getNumOfProperties() == 0)
				{
					::MessageBox(this->GetSafeHwnd(),sNoPropMsg,sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}
			}
		}

		VERIFY(menu.CreatePopupMenu());
		GetCursorPos(&curPos);
		// create main menu items
		menu.AppendMenu(MF_STRING, ID_TOOLS_ADD_NEW_STAND, (sMenu));
		// Commented out 2012-12-07 P�D #3380
		//menu.AppendMenu(MF_SEPARATOR);
		//menu.AppendMenu(MF_STRING, ID_TOOLS_CALC_NEW_STAND, (sMenu0));
		menu.AppendMenu(MF_SEPARATOR);
		menu.AppendMenu(MF_STRING, ID_TOOLS_MATCH_PROP, (sMenu1));
		menu.AppendMenu(MF_STRING, ID_TOOLS_EVAL_FROM_CRUISE, (sMenu3));			
		menu.AppendMenu(MF_SEPARATOR);
		menu.AppendMenu(MF_STRING, ID_TOOLS_REMOVE_SEL_STAND, (sMenu2));
		menu.AppendMenu(MF_SEPARATOR);
		menu.AppendMenu(MF_STRING, ID_TOOLS_OPEN_SEL_STAND, (sMenu4));
		menu.AppendMenu(MF_SEPARATOR);
		menu.AppendMenu(MF_STRING, ID_TOOLS_OPEN_SEL_STAND_PRICELIST, (sMenu5));	// #4554
		menu.AppendMenu(MF_STRING, ID_TOOLS_OPEN_SEL_STAND_COST_PRICELIST, (sMenu6));	// #4554

		if (!canWeCalculateThisProp_cached(m_nPropertyStatus))
		{
			menu.EnableMenuItem(ID_TOOLS_ADD_NEW_STAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_CALC_NEW_STAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_MATCH_PROP,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_EVAL_FROM_CRUISE,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_REMOVE_SEL_STAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_OPEN_SEL_STAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			menu.EnableMenuItem(ID_TOOLS_OPEN_SEL_STAND_PRICELIST, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);	// #4554
			menu.EnableMenuItem(ID_TOOLS_OPEN_SEL_STAND_COST_PRICELIST, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);	// #4554
		}

		// Commented out 2012-12-05 P�D #3380
		//if (nStandIndex == CRUISE_TYPE_2)
		//	menu.EnableMenuItem(ID_TOOLS_OPEN_SEL_STAND,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		// track menu
		int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
								curPos.x, curPos.y, this, NULL);
		// other general items
		switch (nMenuResult)
		{
			case ID_TOOLS_ADD_NEW_STAND:
				addNewStand();
				saveCruiseData();
			break;

			case ID_TOOLS_CALC_NEW_STAND:
			{
				saveCruiseData();
				// When this view gets focus, disable the view Add toolbar button; 081007 p�d
				pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
				if (pFrame != NULL)	pFrame->doCalculateManuallyEnteredCruises();
				pFrame = NULL;
			}
			break;

			case ID_TOOLS_MATCH_PROP:
				// When this view gets focus, disable the view Add toolbar button; 081007 p�d
				pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
				if (pFrame != NULL)	pFrame->doMatchStandsToAvtiveProperty();
				pFrame = NULL;
			break;

			case ID_TOOLS_EVAL_FROM_CRUISE:
				// When this view gets focus, disable the view Add toolbar button; 081007 p�d
				pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
				if (pFrame != NULL)	pFrame->doCreateEvaluationFromCruise();
				pFrame = NULL;
			break;

			case ID_TOOLS_REMOVE_SEL_STAND:
				// When this view gets focus, disable the view Add toolbar button; 081007 p�d
				pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
				if (pFrame != NULL)	
					pFrame->doOnRemoveSelStandFromProperty();
				pFrame = NULL;
			break;
			// Added 2009-01-23 P�D
			case ID_TOOLS_OPEN_SEL_STAND:
				openStand();
			break;

			case ID_TOOLS_OPEN_SEL_STAND_PRICELIST:	// #4554
				openPricelist();
			break;

			case ID_TOOLS_OPEN_SEL_STAND_COST_PRICELIST:	// #4554
				openCostPricelist();
			break;
		}
	}
}

void CCruisingData::OnReportHeaderColumnRClick(NMHDR * pNotifyStruct, LRESULT * result)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
	//menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupBy);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
	/*if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}*/

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER:
		{
			CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pWnd != NULL)
			{
				BOOL bShow = !pWnd->m_wndFieldChooserDlg_cruise.IsVisible();
				pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg_cruise, bShow, FALSE);
			}
		}
		break;
		case ID_SHOW_GROUPBOX:
			//GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
	}
	menu.DestroyMenu();
}

void CCruisingData::addNewStand(void)
{
	BOOL bFound = FALSE,bDoUpdate = FALSE;
	double fCorr = 0.0,fVolPerHA = 0.0;
	int nAge = 0,ret = 0;
	CTransaction_elv_properties* pProperty = getActiveProperty();
	vecTransaction_eval_evaluation vecEval;
	CELVObjectCruiseReportRec *pRec = NULL;


	CAddTGLDlg *pDlg = new CAddTGLDlg();
	if (pDlg != NULL)
	{
		pDlg->setStandNum(L"");
		pDlg->setStandName(L"");
		pDlg->setStandSI(L"");
		pDlg->setStandAreal(0.0);
		pDlg->setStandVolume(0.0);
		pDlg->setTGL(L"0;0;0");
		pDlg->setStandAge(0);
		pDlg->setStandSide(1);
		pDlg->setTillfUtnyttj(FALSE);
		if (pDlg->DoModal() == IDOK)
		{
		
			addConstraints();

			CELVObjectCruiseReportRec *repRec=new CELVObjectCruiseReportRec(pProperty->getPropStatus(),
				m_arrTypeOfStand,
				pDlg->getStandNum(),
				pDlg->getStandName(),
				pDlg->getStandSI(),
				pDlg->getStandAreal(),
				pDlg->getStandVol(),
				pDlg->getStandSide(),
				pDlg->getTGL());

			repRec->getRecELVCruise().setECruTillfUtnyttj(pDlg->getTillfUtnyttj());
			GetReportCtrl().AddRecord(repRec);			

			saveCruiseData();

			CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pFrame != NULL)	pFrame->doCalculateManuallyEnteredCruises();
			pFrame = NULL;

			GetReportCtrl().Populate();
			GetReportCtrl().UpdateWindow();

			if (pDlg->getDoCreateVStand())
			{

				if (m_pDB != NULL)
				{
					m_pDB->getObjectEvaluation(pProperty->getPropID_pk(),pProperty->getPropObjectID_pk(),vecEval);
				}
				bFound = FALSE;
				if (vecEval.size() > 0)
				{
					for (int i = 0;i < vecEval.size();i++)
					{
						if (vecEval[i].getEValName().CompareNoCase(pDlg->getStandName().Trim()) == 0)
						{
							nAge = vecEval[i].getEValAge();
							bFound = TRUE;
							break;
						}
					}
				}	// if (vecEval.size() > 0)

				if (bFound)
				{
					ret = ::MessageBox(this->GetSafeHwnd(),m_sEvalueUpdateMsg,global_langMap[IDS_STRING229],MB_ICONQUESTION | MB_YESNOCANCEL);
					if (ret == IDYES)
					{
						bDoUpdate = TRUE;
					}
					else if (ret == IDNO)
					{
						bDoUpdate = FALSE;
					}
				}



				CMDI1950ForrestNormFormView *pView = NULL;
				CPropertiesFormView *pPropView = NULL;
				if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
				{
					if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
					{
						// Just add a new row to Evaluation list; 2012-12-06 P�D #3380
						if (!bDoUpdate)
						{
							pPropView->addNewVStandToActiveProperty();
						}
						// Add data, save and calculate; 2012-12-06 P�D #3380
						if (pDlg->getStandVol() > 0.0 && pDlg->getStandAreal() > 0.0)
							fVolPerHA = pDlg->getStandVol()/pDlg->getStandAreal();
						else
							fVolPerHA = pDlg->getStandVol();
						// Add data, save and calculate; 2012-12-06 P�D #3380
						pPropView->caclulateCorrFactor(fVolPerHA,
																					 pDlg->getTGL(),
																					 pDlg->getStandSI(),
																					 pDlg->getStandAge(),
																					 &fCorr);

						pPropView->saveAndCalculateVStandInActiveProperty(pDlg->getStandNum(),
																															pDlg->getStandName(),
																															pDlg->getStandSI(),
																															pDlg->getTGL(),
																															pDlg->getStandAreal(),
																															pDlg->getStandVol(),
																															pDlg->getStandAge(),
																															fCorr,
							FALSE /* Add new eval. stand*/,
							pDlg->getStandSide(),
							pDlg->getTillfUtnyttj()
							);

					}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
				}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
				pView = NULL;
				pPropView = NULL;
			}
		}
		delete pDlg;
	}

}

void CCruisingData::openStand(void)
{
	CTransaction_elv_cruise rec;
	CELVObjectCruiseReportRec *pRec = NULL;
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	
	if (pRow != NULL)
	{
		pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			// Set value in Register to open UMEstimate; 090217 p�d
			// 1 = From NaigationBar
			// 2 = From UMLandValue Object
			setFromNavBarOrOtherInReg(2);

			rec = pRec->getRecELVCruise();
			// Find out Index for TypeOfStand; 090907 p�d
			// Only "Taxerade best�nd", can be opened; 090907 p�d
			int nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
			if (nStandIndex == CRUISE_TYPE_1)	// "Taxerat best�nd"
			{
				CString sLangFN(getLanguageFN(getLanguageDir(),_T("UMEstimate"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
				showFormView(5000,sLangFN);
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
					(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("View5000"),_T(""),3 /* 3 = Identifer for UMLandVlaue module */,rec.getECruID_pk(),0));
			}
			else if (nStandIndex == CRUISE_TYPE_2)	// "Manuellet inl�st" Added 2012-12-05 P�D #3380
			{
				CTransaction_elv_properties* pProperty = getActiveProperty();
				vecTransaction_eval_evaluation vecEval;

				BOOL bFound = FALSE,bDoUpdate = FALSE;
				double fCorr = 0.0,fVolPerHA = 0.0;
				int nAge = 0,ret = 0;
				BOOL bTillfUtnyttj=FALSE;
				CELVObjectCruiseReportRec *pRec = (CELVObjectCruiseReportRec *)pRow->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getTypeIndex() == CRUISE_TYPE_1)
					{
						openStand();
					}
					else if (pRec->getTypeIndex() == CRUISE_TYPE_2)
					{
						if (m_pDB != NULL)
						{
							m_pDB->getObjectEvaluation(pProperty->getPropID_pk(),pProperty->getPropObjectID_pk(),vecEval);
						}
						bFound = FALSE;
						if (vecEval.size() > 0)
						{
							for (int i = 0;i < vecEval.size();i++)
							{
								if (vecEval[i].getEValName().Compare(pRec->getColumnIconText(COLUMN_2).Trim()) == 0)
								{
									nAge = vecEval[i].getEValAge();									
									bFound = TRUE;
									break;
								}
							}
						}	// if (vecEval.size() > 0)

						CAddTGLDlg *pDlg = new CAddTGLDlg();
						if (pDlg != NULL)
						{
							pDlg->setStandNum(pRec->getColumnText(COLUMN_1));
							pDlg->setStandName(pRec->getColumnIconText(COLUMN_2));
							pDlg->setStandSI(pRec->getColumnIconText(COLUMN_6));
							pDlg->setStandAreal(pRec->getColumnFloat(COLUMN_4));
							pDlg->setStandVolume(pRec->getColumnFloat(COLUMN_5));
							pDlg->setTGL(pRec->getColumnText(COLUMN_17));
							pDlg->setStandAge(nAge);
							pDlg->setStandSide(pRec->getColumnInt(COLUMN_14));
							bTillfUtnyttj=pRec->getRecELVCruise().getECruTillfUtnyttj();
							pDlg->setTillfUtnyttj(bTillfUtnyttj);
							if (pDlg->DoModal() == IDOK)
							{
								pRec->setColumnText(COLUMN_1,pDlg->getStandNum());
								pRec->setColumnIconText(COLUMN_2,pDlg->getStandName());
								pRec->setColumnText(COLUMN_6,pDlg->getStandSI());
								pRec->setColumnFloat(COLUMN_4,pDlg->getStandAreal());
								pRec->setColumnFloat(COLUMN_5,pDlg->getStandVol());
								pRec->setColumnText(COLUMN_17,pDlg->getTGL());
								pRec->setColumnInt(COLUMN_14,pDlg->getStandSide());

								bTillfUtnyttj= pDlg->getTillfUtnyttj();
								pRec->getRecELVCruise().setECruTillfUtnyttj(bTillfUtnyttj);
								saveCruiseData();
								CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
								if (pFrame != NULL)	pFrame->doCalculateManuallyEnteredCruises();
								pFrame = NULL;
								
								if (pDlg->getDoCreateVStand())
								{
									if (bFound)
									{
										ret = ::MessageBox(this->GetSafeHwnd(),m_sEvalueUpdateMsg,global_langMap[IDS_STRING229],MB_ICONQUESTION | MB_YESNOCANCEL);
										if (ret == IDYES)
										{
											bDoUpdate = TRUE;
										}
										else if (ret == IDNO)
										{
											bDoUpdate = FALSE;
										}
									}
									// If cancel, nothing'll happen
									if (ret != IDCANCEL)
									{
										CMDI1950ForrestNormFormView *pView = NULL;
										CPropertiesFormView *pPropView = NULL;
										if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
										{
											if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
											{

												// Just add a new row to Evaluation list; 2012-12-06 P�D #3380
												if (!bDoUpdate)
												{
													pPropView->addNewVStandToActiveProperty();
												}
												// Add data, save and calculate; 2012-12-06 P�D #3380
												if (pDlg->getStandVol() > 0.0 && pDlg->getStandAreal() > 0.0)
													fVolPerHA = pDlg->getStandVol()/pDlg->getStandAreal();
												else
													fVolPerHA = pDlg->getStandVol();
												pPropView->caclulateCorrFactor(fVolPerHA,
													pDlg->getTGL(),
													pDlg->getStandSI(),
													pDlg->getStandAge(),
													&fCorr);
												pPropView->saveAndCalculateVStandInActiveProperty(pDlg->getStandNum(),
													pDlg->getStandName(),
													pDlg->getStandSI(),
													pDlg->getTGL(),
													pDlg->getStandAreal(),
													pDlg->getStandVol(),
													pDlg->getStandAge(),
													fCorr,
													bDoUpdate,pDlg->getStandSide(),
													bTillfUtnyttj);

											}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
										}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
										pView = NULL;
										pPropView = NULL;
									}
								}
							}
							delete pDlg;
						}
					}
				}
			}
		}
	}
}

void CCruisingData::populateReport(int prop_id,int obj_id,short prop_status,short norm_type)
{
	CTransaction_elv_cruise recCruise;
	m_nPropertyStatus = prop_status;
	// Get cruise data for Object and Property; 080506 p�d
	getObjectCruiseFromDB(prop_id,obj_id);

	GetReportCtrl().BeginUpdate();
	GetReportCtrl().ResetContent();	// Clear
	if (vecELVObjectCruise.size() > 0)
	{
		for (UINT i = 0;i < vecELVObjectCruise.size();i++)
		{
			recCruise = vecELVObjectCruise[i];
			addConstraints();
			//HMS-18 bytt plats p� norm_type och m_nPropertyStatus stod i fel ordning 20180608 J�
			GetReportCtrl().AddRecord(new CELVObjectCruiseReportRec(recCruise.getECruID_pk(),
				recCruise.getECruPropID_pk(),
				recCruise.getECruObjectID_pk(),																															
				m_nPropertyStatus,
				norm_type,
				m_arrTypeOfStand,
				m_sarrYesNo,
				recCruise,
				m_sWidthErrMsg));
		}
	}	// if (vecELVObjectCruise.size() > 0)
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	GetReportCtrl().EndUpdate();
}

int CCruisingData::findTypeOfStandIndex(LPCTSTR stand)
{
	CString sArr,S;
	if (m_arrTypeOfStand.GetCount() > 0)
	{
		for (int i = 0;i < m_arrTypeOfStand.GetCount();i++)
		{
			sArr = m_arrTypeOfStand.GetAt(i);

			if (sArr.CompareNoCase(stand) == 0)
			{
				return i;
			}
		}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
	}	// if (m_arrTypeOfVStand.GetCount() > 0)

	return -1;	// No match
}

void CCruisingData::addConstraints(void)
{
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pColumns != NULL)
	{
		//--------------------------------------------------------------------------
		// Add type of evaluation stand: "V�rdering","V�rdering startar" etc
		CXTPReportColumn *pCol0 = pColumns->Find(COLUMN_0);
		if (pCol0 != NULL)
		{
			// Get constraints for column and remove all items; 071109 p�d
			pCons = pCol0->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_arrTypeOfStand.GetCount() > 0)
			{
				for (int i = 0;i < m_arrTypeOfStand.GetCount();i++)
				{
					pCol0->GetEditOptions()->AddConstraint((m_arrTypeOfStand.GetAt(i)),i);
				}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
			}	// if (m_arrTypeOfVStand.GetCount() > 0)
		}	// if (pCol0 != NULL)
	}	// if (pColumns != NULL)
}

BOOL CCruisingData::getFocusedCruise(CTransaction_elv_cruise &rec)
{
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CELVObjectCruiseReportRec *pRec = NULL;
	if (pRow != NULL)
	{
		pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			rec = pRec->getRecELVCruise();
	
			pRec = NULL;
		}	// if (pRec != NULL & m_pDB != NULL)
		return TRUE;
	}	// if (pRow != NULL)
	return FALSE;
}

// Added function: Remove "Rotpost" also; 101206 p�d
BOOL CCruisingData::removeSelectedCruise(short prop_status,short norm_type)
{
	BOOL bIsIncluded = FALSE;
	BOOL bReturn = TRUE;
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CELVObjectCruiseReportRec *pRec = NULL;
	vecTransaction_elv_cruise vecCruises;
	CString sMsgRemove;
	if (pRow != NULL)
	{
		pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			CTransaction_elv_cruise rec = pRec->getRecELVCruise();
			if (rec.getECruType() == CRUISE_TYPE_1)
			{
				// Check if cruise is included in Other objects; 101206 p�d
				m_pDB->getObjectCruises(vecCruises);
				if (vecCruises.size() > 0)
				{
					// Check if selected cruise is included in other Object/stands; 101206 p�d
					for (UINT i = 0;i < vecCruises.size();i++)
					{
						if (vecCruises[i].getECruID_pk() == rec.getECruID_pk() && vecCruises[i].getECruObjectID_pk() != rec.getECruObjectID_pk())
						{
							bIsIncluded = TRUE;
							break;
						}
					}			
					if (bIsIncluded)
					{
						sMsgRemove.Format(L"%s %s ; %s\n\n%s\n\n%s\n\n",global_langMap[IDS_STRING295],rec.getECruName(),rec.getECruNumber(),global_langMap[IDS_STRING4121],global_langMap[IDS_STRING412]);
						if (::MessageBox(this->GetSafeHwnd(),sMsgRemove,global_langMap[IDS_STRING229],MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDNO)
							return FALSE;
					}
					else
					{
						sMsgRemove.Format(L"%s %s ; %s\n\n%s\n\n%s\n\n",global_langMap[IDS_STRING295],rec.getECruName(),rec.getECruNumber(),global_langMap[IDS_STRING4120],global_langMap[IDS_STRING412]);
						if (::MessageBox(this->GetSafeHwnd(),sMsgRemove,global_langMap[IDS_STRING229],MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDNO)
							return FALSE;
					}
					
					if (m_pDB->delObjectCruise(rec.getECruID_pk(),rec.getECruPropID_pk(),rec.getECruObjectID_pk(),rec.getECruType()) && !bIsIncluded)
					{
						m_pDB->delTrakt(rec.getECruID_pk());
					}
				}
				populateReport(rec.getECruPropID_pk(),rec.getECruObjectID_pk(),prop_status,norm_type);
			}
			else
			{
				sMsgRemove.Format(L"%s %s ; %s\n\n%s\n\n",global_langMap[IDS_STRING295],rec.getECruName(),rec.getECruNumber(),global_langMap[IDS_STRING4122]);
				if (::MessageBox(this->GetSafeHwnd(),sMsgRemove,global_langMap[IDS_STRING295],MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				{
					m_pDB->delObjectCruise(rec.getECruID_pk(),rec.getECruPropID_pk(),rec.getECruObjectID_pk(),rec.getECruType());
					populateReport(rec.getECruPropID_pk(),rec.getECruObjectID_pk(),prop_status,norm_type);
				}

			}
		}	// if (pRec != NULL & m_pDB != NULL)
	}	// if (pRow != NULL)

	return bReturn;
}

void CCruisingData::saveCruisingWSide(void)
{
	CELVObjectCruiseReportRec *pRec = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CELVObjectCruiseReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (m_pDB != NULL)
				{
					m_pDB->updObjectCruise_wside(pRec->getRecELVCruise().getECruID_pk(),
																		 pRec->getRecELVCruise().getECruPropID_pk(),
																		 pRec->getRecELVCruise().getECruObjectID_pk(),
																		 pRec->getColumnInt(COLUMN_14));
				}	// if (m_pDB != NULL)
				pRec = NULL;
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
		pRows = NULL;
	}	// if (pRows != NULL)
}

void CCruisingData::saveCruisingWidth(void)
{
	CELVObjectCruiseReportRec *pRec = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	double fValue;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CELVObjectCruiseReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (m_pDB != NULL)
				{
					fValue = pRec->getColumnFloat(COLUMN_15);
					m_pDB->updObjectCruise_width(pRec->getRecELVCruise().getECruID_pk(),
																			pRec->getRecELVCruise().getECruPropID_pk(),
																			pRec->getRecELVCruise().getECruObjectID_pk(),
																			fValue);
				}	// if (m_pDB != NULL)
				pRec = NULL;
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
		pRows = NULL;
	}	// if (pRows != NULL)
}

void CCruisingData::saveCruiseData(void)
{
	CString S;
	int nStandIndex = -1;	// Typeof vstand  "V�rdering","V�rdering start" etc.
	CTransaction_elv_cruise rec;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CXTPReportRow *pRow = NULL;
	CELVObjectCruiseReportRec *pRec = NULL;
	long nCruiseID = 1,nCheckCruiseID = 0,nDBCruiseID = -1;
	BOOL bDoAdd = TRUE;

	CTransaction_elv_object *pObj = getActiveObject();

	GetReportCtrl().Populate();

	CTransaction_elv_properties* pProperty = getActiveProperty();
	if (pRows != NULL && pProperty != NULL && pObj != NULL)
	{
		// Check Property status; 090508 p�d
		//if (pProperty->getPropStatus() > STATUS_ADVICED) return;
		if (!canWeCalculateThisProp_cached(pProperty->getPropStatus())) return;
		// Don't do this; 090511 p�d
		//m_pDB->delObjectEvaluation(pProperty->getPropObjectID_pk(),pProperty->getPropID_pk());

		// Is there anything to work with; 080514 p�d
		if (pRows->GetCount() > 0)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRow = pRows->GetAt(i);
				if (pRow != NULL)
				{
					pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
					double dcheck=pRec->getColumnFloat(COLUMN_11);//Test J�

					// Find out Index for TypeOfVStand and TypOfForrest; 080514 p�d
					nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
					if (m_pDB != NULL)
					{
						// Get max cruise id for manually entered stands; 090907 p�d
						nDBCruiseID = m_pDB->getObjectCruise_last_id(pProperty->getPropObjectID_pk(),pProperty->getPropID_pk(),CRUISE_TYPE_2);
						// Check that the nDBCruiseID for manually entered stand, don't exist in a cruised stand; 090907 p�d
						nCheckCruiseID = m_pDB->getObjectCruise_id_exists(nDBCruiseID,pProperty->getPropObjectID_pk(),pProperty->getPropID_pk());
						if (pRec->getRecELVCruise().getECruID_pk() < 0)
						{
							if (nDBCruiseID < 1000000)
								nCruiseID = 1000000;	// First manually entered item; 091021 p�d; Changed from 10000 to 1000000
							else if (nCheckCruiseID > nDBCruiseID+1)
								nCruiseID = nCheckCruiseID;
							else
								nCruiseID = (nDBCruiseID + 1);
							bDoAdd = TRUE;						
						}
						else
						{
							bDoAdd = FALSE;						
							nCruiseID = pRec->getRecELVCruise().getECruID_pk();
						}

						BOOL bTillf=pRec->getRecELVCruise().getECruTillfUtnyttj();
							rec = CTransaction_elv_cruise(nCruiseID,
																						pProperty->getPropObjectID_pk(),
																						pProperty->getPropID_pk(),
																						pRec->getColumnText(COLUMN_1),
																						pRec->getColumnIconText(COLUMN_2),
																						pRec->getColumnInt(COLUMN_3),
																						pRec->getColumnFloat(COLUMN_4),
																						pRec->getColumnFloat(COLUMN_5),
																						pRec->getRecELVCruise().getECruTimberVol(),
																						pRec->getColumnFloat(COLUMN_7),
																						pRec->getColumnFloat(COLUMN_8),
																						pRec->getColumnFloat(COLUMN_9),
																						pRec->getColumnFloat(COLUMN_10),
																						pRec->getColumnIconFloat(COLUMN_11),
																						//pRec->getColumnFloat(COLUMN_11),
																						pRec->getColumnFloat(COLUMN_16),
																						pRec->getRecELVCruise().getECruAvgPriceFactor(),
																						pRec->getColumnFloat(COLUMN_12),
																						pRec->getColumnFloat(COLUMN_13),
																						pRec->getColumnInt(COLUMN_14),
																						0,
																						nStandIndex,
								pRec->getColumnFloat(COLUMN_15),								
																						pRec->getColumnText(COLUMN_6),
								//																						pRec->getColumnIconText(COLUMN_17),	Commanted out 2012-12-05 P�D #3380
																						pRec->getColumnText(COLUMN_17),	// Added 2012-12-05 P�D #3380
																						pRec->getColumnFloat(COLUMN_19),
																						pRec->getColumnFloat(COLUMN_20),
																						pRec->getColumnFloat(COLUMN_21),
								pRec->getColumnCheck(COLUMN_22),
								bTillf,							//HMS-41 Tillf�lligt utnyttjande
								pRec->getRecELVCruise().getStormTorkInfo_SpruceMix(),	//HMS-50 Info om storm o tork 20200415 J�
								pRec->getRecELVCruise().getStormTorkInfo_SumM3Sk_inside(),
								pRec->getRecELVCruise().getStormTorkInfo_AvgPriceFactor(),
								pRec->getRecELVCruise().getStormTorkInfo_TakeCareOfPerc(),
								pRec->getRecELVCruise().getStormTorkInfo_PineP30Price(),
								pRec->getRecELVCruise().getStormTorkInfo_SpruceP30Price(),
								pRec->getRecELVCruise().getStormTorkInfo_BirchP30Price(),
								pRec->getRecELVCruise().getStormTorkInfo_CompensationLevel(),
								pRec->getRecELVCruise().getStormTorkInfo_Andel_Pine(),
								pRec->getRecELVCruise().getStormTorkInfo_Andel_Spruce(),
								pRec->getRecELVCruise().getStormTorkInfo_Andel_Birch(),	
								pRec->getRecELVCruise().getStormTorkInfo_WideningFactor(),
								pRec->getRecELVCruise().getMarkInfo_ValuePine(),
								pRec->getRecELVCruise().getMarkInfo_ValueSpruce(),
								pRec->getRecELVCruise().getMarkInfo_Andel_Pine(),
								pRec->getRecELVCruise().getMarkInfo_Andel_Spruce(),
								pRec->getRecELVCruise().getMarkInfo_P30_Pine(),
								pRec->getRecELVCruise().getMarkInfo_P30_Spruce(),
								pRec->getRecELVCruise().getMarkInfo_ReducedBy()
								);
						if (bDoAdd)
							m_pDB->addObjectCruise(rec);
						else
							m_pDB->updObjectCruise(rec);						
					}	// if (m_pDB != NULL)
				}	// if (pRow != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
			populateReport(rec.getECruPropID_pk(),rec.getECruObjectID_pk(),pProperty->getPropStatus(),pObj->getObjUseNormID());
		}	// if (pRows->GetCount() > 0)
	}	// if (pRows != NULL)

}

void CCruisingData::getIsDoReduceRotpost(vecReduceRotpost& vec)
{
	_struct_reduce_rotpost rec;
	CELVObjectCruiseReportRec *pRec = NULL;
	vec.clear();
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CELVObjectCruiseReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				rec.bIsYes = (pRec->getColumnText(COLUMN_14).CompareNoCase(m_sarrYesNo[0]) == 0);
				rec.bDontUseSampleTrees = FALSE;
				rec.nTraktID = pRec->getRecELVCruise().getECruID_pk();
				vec.push_back(rec);
				pRec = NULL;
			}	
		}	// for (int i = 0;i < pRows->GetCount();i++)
		pRows = NULL;
	}	// if (pRows != NULL)
}

void CCruisingData::setManuallyEnteredCalcData(CTransaction_elv_cruise rec,double spruce_mix,double st_value,double st_volume,						
		double fStormTorkInfo_SpruceMix,
		double fStormTorkInfo_SumM3Sk_inside,
		double fStormTorkInfo_AvgPriceFactor,
		double fStormTorkInfo_TakeCareOfPerc,
		double fStormTorkInfo_PineP30Price,
		double fStormTorkInfo_SpruceP30Price,
		double fStormTorkInfo_BirchP30Price,
		double fStormTorkInfo_CompensationLevel,
		double fStormTorkInfo_Andel_Pine,
		double fStormTorkInfo_Andel_Spruce,
		double fStormTorkInfo_Andel_Birch,
		double fStormTorkInfo_WideningFactor)
{
	if (m_pDB) 
	{
		rec.setECruStormDryVol(st_volume);
		rec.setECruStormDryValue(st_value);
		rec.setECruStormDrySpruceMix(spruce_mix);
		rec.setStormTorkInfo_SpruceMix(fStormTorkInfo_SpruceMix);
		rec.setStormTorkInfo_SumM3Sk_inside(fStormTorkInfo_SumM3Sk_inside);
		rec.setStormTorkInfo_AvgPriceFactor(fStormTorkInfo_AvgPriceFactor);
		rec.setStormTorkInfo_TakeCareOfPerc(fStormTorkInfo_TakeCareOfPerc);
		rec.setStormTorkInfo_PineP30Price(fStormTorkInfo_PineP30Price);
		rec.setStormTorkInfo_SpruceP30Price(fStormTorkInfo_SpruceP30Price);
		rec.setStormTorkInfo_BirchP30Price(fStormTorkInfo_BirchP30Price);
		rec.setStormTorkInfo_CompensationLevel(fStormTorkInfo_CompensationLevel);
		rec.setStormTorkInfo_Andel_Pine(fStormTorkInfo_Andel_Pine);
		rec.setStormTorkInfo_Andel_Spruce(fStormTorkInfo_Andel_Spruce);
		rec.setStormTorkInfo_Andel_Birch(fStormTorkInfo_Andel_Birch);
		rec.setStormTorkInfo_WideningFactor(fStormTorkInfo_WideningFactor);
		m_pDB->updObjectCruise(rec);
	}
}

void CCruisingData::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_CRUISING_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CCruisingData::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_CRUISING_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

void CCruisingData::openPricelist()	// #4554
{
	CTransaction_elv_cruise rec;
	CELVObjectCruiseReportRec *pRec = NULL;
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			rec = pRec->getRecELVCruise();

			CTransaction_trakt_misc_data recTraktMiscData;
			m_pDB->getTraktMiscData(rec.getECruID_pk(), recTraktMiscData);

			// Find out Index for TypeOfStand; 090907 p�d
			int nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
			if (nStandIndex == CRUISE_TYPE_1)	// "Taxerat best�nd"
			{
				CString sLangFN(getLanguageFN(getLanguageDir(),_T("UMEstimate"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
				showFormView(5020, sLangFN, (LPARAM)&recTraktMiscData, TRUE);
			}
		}
	}
}

void CCruisingData::openCostPricelist()	// #4554
{
	CTransaction_elv_cruise rec;
	CELVObjectCruiseReportRec *pRec = NULL;
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CELVObjectCruiseReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			rec = pRec->getRecELVCruise();

			CTransaction_trakt_misc_data recTraktMiscDataTmp;
			m_pDB->getTraktMiscData(rec.getECruID_pk(), recTraktMiscDataTmp);
			CTransaction_trakt_misc_data recTraktMiscData(rec.getECruID_pk(),
				_T(""),
				-1,
				_T(""),
				recTraktMiscDataTmp.getCostsName(),
				recTraktMiscDataTmp.getCostsTypeOf(),
				recTraktMiscDataTmp.getXMLCosts(),
				0.0,
				recTraktMiscDataTmp.getCreated());

			// Find out Index for TypeOfStand; 090907 p�d
			int nStandIndex = findTypeOfStandIndex(pRec->getColumnText(COLUMN_0));
			if (nStandIndex == CRUISE_TYPE_1)	// "Taxerat best�nd"
			{
				CString sLangFN(getLanguageFN(getLanguageDir(),_T("UMEstimate"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
				showFormView(5020, sLangFN, (LPARAM)&recTraktMiscData, TRUE);
			}
		}
	}
}
