// HigherCostsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "1950ForrestNormFrame.h"
#include "HigherCostsFormView.h"

#include "TemplateParser.h"
#include "ResLangFileReader.h"

#include <algorithm>


bool MyDataSort(CTransaction_elv_hcost& d1,CTransaction_elv_hcost& d2)
{
	return (d1.getHCostM3Sk() < d2.getHCostM3Sk());
}

// CHigherCostsFormView

IMPLEMENT_DYNCREATE(CHigherCostsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CHigherCostsFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_REPORT_HCOST_TEMPL, OnReport)
	ON_BN_CLICKED(IDC_RADIO3_1, &CHigherCostsFormView::OnBnClickedRadioUseTable)
	ON_BN_CLICKED(IDC_RADIO3_2, &CHigherCostsFormView::OnBnClickedRadioUseBreakFactor)
	ON_EN_CHANGE(IDC_EDIT2_1, &CHigherCostsFormView::OnEnChangeEdit1)
END_MESSAGE_MAP()

CHigherCostsFormView::CHigherCostsFormView()
	: CXTResizeFormView(CHigherCostsFormView::IDD)
{
		m_bInitialized = FALSE;
		m_bDataEnabled = TRUE;
		m_pDB = NULL;
}

CHigherCostsFormView::~CHigherCostsFormView()
{
}

void CHigherCostsFormView::OnDestroy()
{
	m_vecTransactionTemplate.clear();

	m_wndReport.ResetContent();

	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();	
}

void CHigherCostsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHigherCostsFormView)
	DDX_Control(pDX, IDC_GROUP_HCOST_1, m_wndGroupHCost_1);
	DDX_Control(pDX, IDC_GROUP_HCOST_2, m_wndGroupHCost_2);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL2_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_BREAK3_1, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL_FACTOR3_2, m_wndLbl5);
	
	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT2_3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT_BREAK3_1, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT_FACTOR3_2, m_wndEdit5);

	DDX_Control(pDX, IDC_RADIO3_1, m_wndRBUseTable);
	DDX_Control(pDX, IDC_RADIO3_2, m_wndRBUseBreakAndFactor);

	//}}AFX_DATA_MAP

}

void CHigherCostsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit1.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1.SetReadOnly(TRUE);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit2.SetEnabledColor(BLACK,WHITE );
		m_wndEdit2.SetReadOnly(TRUE);
		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit3.SetEnabledColor(BLACK,WHITE );
		m_wndEdit3.SetReadOnly(TRUE);

		m_wndEdit4.SetAsNumeric();
		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit4.SetEnabledColor(BLACK,WHITE );

		m_wndEdit5.SetAsNumeric();
		m_wndEdit5.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit5.SetEnabledColor(BLACK,WHITE );

		// Setup language filename; 080402 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport();

		m_enumAction = TEMPLATE_NONE;

		getObjectTemplatesFromDB();
		if (m_vecTransactionTemplate.size() > 0)
		{
			m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
			setEnableData(TRUE);
		}
		else
		{
			m_nDBIndex = -1;	// No Objects
			setNavigationButtons(FALSE,FALSE);
			setEnableData(FALSE);
		}

		m_wndRBUseTable.SetCheck(TRUE);
		populateData(m_nDBIndex);

		m_bInitialized = TRUE;

	}
}

void CHigherCostsFormView::OnSetFocus(CWnd *wnd)
{
	if (m_vecTransactionTemplate.size() > 0)
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
	else
		setNavigationButtons(FALSE,FALSE);

	CXTResizeFormView::OnSetFocus(wnd);
}

BOOL CHigherCostsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CHigherCostsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CHigherCostsFormView::doSetNavigationBar()
{
	if (m_vecTransactionTemplate.size() > 0 || m_enumAction == TEMPLATE_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}


// CHigherCostsFormView diagnostics

#ifdef _DEBUG
void CHigherCostsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();                
}

#ifndef _WIN32_WCE
void CHigherCostsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CHigherCostsFormView message handlers

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CHigherCostsFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CHigherCostsFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			saveHCostTemplate(1);
			newHCostTemplate();
			setEnableData(TRUE);
			break;
		}	// case ID_NEW_ITEM :
		case ID_SAVE_ITEM :
		{
			saveHCostTemplate(1);
			break;
		}	// case ID_SAVE_ITEM :	
		case ID_DELETE_ITEM :
		{
			removeHCostTemplate();
			break;
		}	// case ID_DELETE_ITEM :	

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveHCostTemplate(-1);
			m_nDBIndex = 0;
			setNavigationButtons(FALSE,TRUE);
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveHCostTemplate(-1);	
			m_nDBIndex--;
			if (m_nDBIndex < 0)
				m_nDBIndex = 0;
			if (m_nDBIndex == 0)
			{
				setNavigationButtons(FALSE,TRUE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveHCostTemplate(-1);
			m_nDBIndex++;
			if (m_nDBIndex > ((UINT)m_vecTransactionTemplate.size() - 1))
				m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
					
			if (m_nDBIndex == (UINT)m_vecTransactionTemplate.size() - 1)
			{
				setNavigationButtons(TRUE,FALSE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveHCostTemplate(-1);
			m_nDBIndex = (UINT)m_vecTransactionTemplate.size()-1;
			setNavigationButtons(TRUE,FALSE);	
			populateData(m_nDBIndex);
			break;
		}	// case ID_NEW_ITEM :

	}	// switch (wParam)
	return 0L;
}

BOOL CHigherCostsFormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_HCOST_TEMPL, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				m_sMsgCap = (xml->str(IDS_STRING229));
				m_sMsgNoTemplateName =  xml->str(IDS_STRING2503);
				m_sMsgNoTemplateName2.Format(_T("%s\n\n%s\n\n"),
					(xml->str(IDS_STRING2503)),
					(xml->str(IDS_STRING2504)));
				m_sMsgRemoveTemplate = (xml->str(IDS_STRING2506));

				m_sMsgNoTemplatesRegistered.Format(_T("%s\n\n%s\n\n"),
					xml->str(IDS_STRING6002),
					xml->str(IDS_STRING6003));

				m_sMsgCharError.Format(_T("%s < > /"),xml->str(IDS_STRING2526));

				m_wndLbl1.SetWindowText((xml->str(IDS_STRING2500)));
				m_wndLbl2.SetWindowText((xml->str(IDS_STRING2501)));
				m_wndLbl3.SetWindowText((xml->str(IDS_STRING2502)));

				m_wndLbl4.SetWindowText((xml->str(IDS_STRING4300)));
				m_wndLbl5.SetWindowText((xml->str(IDS_STRING4301)));

				m_wndRBUseTable.SetWindowText((xml->str(IDS_STRING4302)));
				m_wndRBUseBreakAndFactor.SetWindowText((xml->str(IDS_STRING4303)));

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING2206)), 60));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING2207)), 60));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING2212)), 60));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;		
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING2208)), 60));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;		
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.SetFocus();
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);

				// Need to set size of Report control; 080402 p�d
				CWnd *pWnd = GetDlgItem(ID_REPORT_HCOST_TEMPL);
				if (pWnd != NULL)
				{
					int tpp = TwipsPerPixel();
					pWnd->SetWindowPos(&CWnd::wndTop,20*15/tpp,135*15/tpp,370*15/tpp,240*15/tpp,SWP_SHOWWINDOW);
				}
			}
			delete xml;
		}	// if (fileExists(sLangFN))
	}
	return TRUE;
}

void CHigherCostsFormView::populateData(int idx)
{
	int nTypeSet = -1;
	double fBreakValue = 0.0;
	double fFactor = 0.0;
	CString sName,sDoneBy;
	vecObjectTemplate_hcost_table vecHCost;
	TemplateParser pars;
	CHCostReportRec *pRec = NULL;
	CTransaction_elv_object *pObject = getActiveObject();
	int nObjID = -1;
	// Make sure we are within limits; 080402 p�d
	if (m_vecTransactionTemplate.size() > 0 &&
			idx >= 0 &&
			idx < m_vecTransactionTemplate.size())
	{
		m_enumAction = TEMPLATE_OPEN;

		m_recActiveTemplate = m_vecTransactionTemplate[idx];

		if (pObject != NULL)
			nObjID = pObject->getObjID_pk();

		// Setup general data; 080402 p�d
		m_wndEdit1.SetWindowText((m_recActiveTemplate.getTemplateName()));
		m_wndEdit2.SetWindowText((m_recActiveTemplate.getCreatedBy()));
		m_wndEdit3.SetWindowText((m_recActiveTemplate.getTemplateNotes()));

		// Get (pars) information on P30 table, from xml-data in DB; 080402 p�d
		m_wndReport.ResetContent();

		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			pars.getObjTmplHCost(&nTypeSet,&fBreakValue,&fFactor,vecHCost,sName,sDoneBy);
			if (nTypeSet != 1 && nTypeSet != 2) nTypeSet = 1;
			// Set type of table in Radiobox; 090602 p�d
			m_wndRBUseTable.SetCheck(nTypeSet == 1);
			m_wndRBUseBreakAndFactor.SetCheck(nTypeSet == 2);
		}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		if (nTypeSet == 1)
		{

			if (vecHCost.size() > 0)
			{
				for (UINT i = 0;i < vecHCost.size();i++)
				{
					CObjectTemplate_hcost_table rec = vecHCost[i];
					m_wndReport.AddRecord((pRec = new CHCostReportRec(CTransaction_elv_hcost(i+1,
																																					 nObjID,
																																					 rec.getM3Sk(),
																																					 rec.getPriceM3Sk(),
																																					 rec.getBaseComp(),
																																					 rec.getSumPrice()))));
					if (pRec != NULL)	pRec->setColumnBold(1,COLUMN_3);
				}	// for (UINT i = 0;i < vecP30.size();i++)
			}	// if (vecHCost.size() > 0)

			m_wndReport.Populate();
			m_wndReport.UpdateWindow();

		
			m_wndLbl4.ShowWindow(SW_HIDE);
			m_wndLbl5.ShowWindow(SW_HIDE);
			m_wndEdit4.ShowWindow(SW_HIDE);
			m_wndEdit5.ShowWindow(SW_HIDE);

			m_wndReport.ShowWindow(SW_NORMAL);
			CMDIHigherCostsFrame *pFrame = (CMDIHigherCostsFrame*)getFormViewByID(IDD_FORMVIEW3)->GetParent();
			if (pFrame != NULL)
			{
				pFrame->setEnableToolbar0(TRUE);
				pFrame->setEnableToolbar1(TRUE);
			}
		}
		if (nTypeSet == 2)
		{
			m_wndLbl4.ShowWindow(SW_NORMAL);
			m_wndLbl5.ShowWindow(SW_NORMAL);

			m_wndEdit4.setFloat(fBreakValue,0);
			m_wndEdit4.ShowWindow(SW_NORMAL);
			
			m_wndEdit5.setFloat(fFactor,0);
			m_wndEdit5.ShowWindow(SW_NORMAL);

			m_wndReport.ShowWindow(SW_HIDE);

			CMDIHigherCostsFrame *pFrame = (CMDIHigherCostsFrame*)getFormViewByID(IDD_FORMVIEW3)->GetParent();
			if (pFrame != NULL)
			{
				pFrame->setEnableToolbar0(FALSE);
				pFrame->setEnableToolbar1(TRUE);
			}
		}

		vecHCost.clear();
	}
	else
	{
		setEnableData(FALSE);
		m_wndLbl4.ShowWindow(SW_HIDE);
		m_wndLbl5.ShowWindow(SW_HIDE);
		m_wndEdit4.ShowWindow(SW_HIDE);
		m_wndEdit5.ShowWindow(SW_HIDE);
		m_enumAction = TEMPLATE_NONE;

		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplatesRegistered),(m_sMsgCap),MB_ICONASTERISK | MB_OK);

	}
	// Need to release ref. pointers; 080520 p�d
	pObject = NULL;

	doSetNavigationBar();
}

void CHigherCostsFormView::setupPopulationOfTemplate(BOOL setup_dbindex)
{
	getObjectTemplatesFromDB();
	if (setup_dbindex)
	{
		if (m_vecTransactionTemplate.size() > 0)
		{
			m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1));
		}
		else
		{
			m_nDBIndex = -1;	// No Objects
			setNavigationButtons(FALSE,FALSE);
//			newHCostTemplate();
		}
	}
	populateData(m_nDBIndex);
}

void CHigherCostsFormView::getObjectTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecTransactionTemplate.clear();
			m_pDB->getObjectTemplates(m_vecTransactionTemplate,TEMPLATE_HCOST);
		}	// if (m_pDB != NULL)
	}	// if (m_bConnected)
}

void CHigherCostsFormView::newHCostTemplate()
{
	// Reset active template record (i.e. set id = -1); 080402 p�d
	m_recActiveTemplate = CTransaction_template();

	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(getUserName().MakeUpper());
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));

	m_wndLbl4.ShowWindow(SW_HIDE);
	m_wndLbl5.ShowWindow(SW_HIDE);
	m_wndEdit4.ShowWindow(SW_HIDE);
	m_wndEdit5.ShowWindow(SW_HIDE);

	m_wndReport.ShowWindow(SW_NORMAL);
	m_wndReport.ResetContent();
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	CMDIHigherCostsFrame *pFrame = (CMDIHigherCostsFrame*)getFormViewByID(IDD_FORMVIEW3)->GetParent();
	if (pFrame != NULL)
	{
		pFrame->setEnableToolbar0(TRUE);
		pFrame->setEnableToolbar1(TRUE);
	}

	m_wndRBUseTable.SetCheck(TRUE);
	m_wndRBUseBreakAndFactor.SetCheck(FALSE);

	m_wndEdit1.SetFocus();

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	m_enumAction = TEMPLATE_NEW;


}

BOOL CHigherCostsFormView::saveHCostTemplate(short action)
{
	CString sXML,sTmp;
	int nID = -1;
	int nObjID = -1;
	CString sTemplateName;
	CString sCreatedBy;
	CString sNotes;
	double fM3Sk;
	double fPrice;
	double fBaseComp;
	double fSumPrice;

	// Check if there's any data to check; 090204 p�d
	if (!m_bDataEnabled) return TRUE;

	// Collect general data; 080402 p�d
	sTemplateName = m_wndEdit1.getText();
	sCreatedBy = m_wndEdit2.getText();
	sNotes = m_wndEdit3.getText();

	if (sTemplateName.IsEmpty())
	{
		if (action == 0)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		else if (action == 1)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
		}
		else if (action == 2)
		{
			if (::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName2),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
				return TRUE;
			else
			{
				m_wndEdit1.SetFocus();						
				return FALSE;
			}
		}	// else if (action == 2)
	}	// if (sTemplateName.IsEmpty())
	else
	{

		std::vector<CTransaction_elv_hcost> vecHCost;
		CHCostReportRec *pRec = NULL;
		CXTPReportRecords *pRecs = m_wndReport.GetRecords();
		if (pRecs != NULL)
		{
			for (int i1 = 0;i1 < pRecs->GetCount();i1++)
			{
				pRec = (CHCostReportRec *)pRecs->GetAt(i1);
				fM3Sk = pRec->getColumnFloat(COLUMN_0);
				fPrice = pRec->getColumnFloat(COLUMN_1);
				fBaseComp = pRec->getColumnFloat(COLUMN_2);
				fSumPrice = pRec->getColumnFloat(COLUMN_3);

				vecHCost.push_back(CTransaction_elv_hcost(i1,1,fM3Sk,fPrice,fBaseComp,fSumPrice));
			}
			std::sort(vecHCost.begin(),vecHCost.end(),MyDataSort);
			
			// Start by setting header and data-tag
			sXML = XML_FILE_HEADER;
			sXML += NODE_HCOST_TMPL_START;
			CString sTemplateNameTmp = sTemplateName;
			sTemplateNameTmp.Replace(_T("&"), _T("&amp;"));
			sTemplateNameTmp.Replace(_T("<"), _T("&lt;"));
			sTemplateNameTmp.Replace(_T(">"), _T("&gt;"));
			if (m_wndRBUseTable.GetCheck())
				sXML += formatData(NODE_HCOST_TMPL_TYPE_SETTING,1,sTemplateNameTmp,sCreatedBy);
			else if (m_wndRBUseBreakAndFactor.GetCheck())
				sXML += formatData(NODE_HCOST_TMPL_TYPE_SETTING,2,sTemplateNameTmp,sCreatedBy);
			// Added 2009-05-29 p�d
			// "Brytv�rde och faktor"; 090529 p�d
			sXML += formatData(NODE_HCOST_TMPL_DATA_BF,m_wndEdit4.getFloat(),m_wndEdit5.getFloat());
			
			sXML += NODE_HCOST_TMPL_START_DATA;
			// Add data by rows in report; 080402 p�d
			for (UINT i = 0;i < vecHCost.size();i++)
			{
				//pRec = (CHCostReportRec *)pRecs->GetAt(i);
				CTransaction_elv_hcost rec = vecHCost[i];
				fM3Sk = rec.getHCostM3Sk();
				fPrice = rec.getHCostPrice();
				fBaseComp = rec.getHCostBaseComp();
				fSumPrice = rec.getHCostSUM();
				sXML += formatData(NODE_HCOST_TMPL_DATA,fM3Sk,fPrice,fBaseComp,fSumPrice);
			}
			sXML += NODE_HCOST_TMPL_END_DATA;
			sXML += NODE_HCOST_TMPL_END;
		}

		if (m_pDB != NULL)
		{
			nID = m_recActiveTemplate.getID();
			if (m_pDB->addObjectTemplate(CTransaction_template(nID,sTemplateName,TEMPLATE_HCOST,sXML,sNotes,sCreatedBy)))
			{
				if (action > -2)
					setupPopulationOfTemplate(TRUE);	// A new item is added; 080402 p�d
			}
			else
			{
				m_pDB->updObjectTemplate(CTransaction_template(nID,sTemplateName,TEMPLATE_HCOST,sXML,sNotes,sCreatedBy));
				if (action > -2)
					setupPopulationOfTemplate(FALSE);	// Just reload data and populate; 080402 p�d
			}
		}
	}	// else

	return TRUE;
}

void CHigherCostsFormView::removeHCostTemplate()
{
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (m_pDB != NULL)
		{
			m_pDB->delObjectTemplate(m_recActiveTemplate);
			setupPopulationOfTemplate(TRUE);	// A new item is added; 080402 p�d
		}
		m_enumAction = TEMPLATE_NONE;
	}
}

//void CHigherCostsFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
void CHigherCostsFormView::OnReport(void)
{
	if (m_wndReport.GetFocusedRow() == NULL)	// No row in focus; 080403 p�d
		return;

	double fVolM3Sk;
	double fPricePerM3Sk;
	double fBaseComp;
	double fSumPrice;

	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CXTPReportColumn *pCol = m_wndReport.GetFocusedColumn();
	CHCostReportRec *pRec = NULL;
	if (pRow != NULL && pCol != NULL)
	{
		m_wndReport.Populate();
		pRec = (CHCostReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
				fVolM3Sk = pRec->getColumnFloat(COLUMN_0);
				fPricePerM3Sk = pRec->getColumnFloat(COLUMN_1);
				fBaseComp = pRec->getColumnFloat(COLUMN_2);	// Added 090529 p�d
				fSumPrice = fVolM3Sk*fPricePerM3Sk+fBaseComp;
				if (fSumPrice < 0.0) fSumPrice = 0.0;
				pRec->setColumnFloat(COLUMN_2,fBaseComp);
				pRec->setColumnFloat(COLUMN_3,fSumPrice);
		}
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}

void CHigherCostsFormView::addRowToReport(void)
{
	CHCostReportRec *pRec = NULL;
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	CXTPReportColumns *pCols = NULL;
	if (m_wndReport.GetSafeHwnd() != NULL)
	{
		m_wndReport.AddRecord((pRec = new CHCostReportRec()));
		if (pRec != NULL) pRec->setColumnBold(1,COLUMN_3);

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();

		pRows = m_wndReport.GetRows();
		if (pRows != NULL)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				m_wndReport.SetFocusedRow(pRow);
				pCols = m_wndReport.GetColumns();
				if (pCols != NULL)
					m_wndReport.SetFocusedColumn(pCols->GetAt(COLUMN_0));
				
				m_wndReport.SetFocus();
			}
		}
	}	// if (m_wndReport.GetSafeHwnd() != NULL)
}

void CHigherCostsFormView::removeLastRowFromReport(void)
{
	if (m_wndReport.GetRows()->GetCount() > 0)
	{
		m_wndReport.GetRecords()->RemoveAt(m_wndReport.GetRows()->GetCount()-1);
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}
// Added 2012-09-21 P�D; redmine #3357
void CHigherCostsFormView::removeFocusedRowFromReport(void)
{
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		m_wndReport.GetRecords()->RemoveAt(pRow->GetIndex());
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}

void CHigherCostsFormView::setEnableData(BOOL enable)
{
	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit3.EnableWindow(enable);
	m_wndEdit3.SetReadOnly(!enable);
	m_wndEdit4.EnableWindow(enable);
	m_wndEdit4.SetReadOnly(!enable);
	m_wndEdit5.EnableWindow(enable);
	m_wndEdit5.SetReadOnly(!enable);
	
	m_wndRBUseTable.EnableWindow(enable);
	m_wndRBUseBreakAndFactor.EnableWindow(enable);

	CMDIHigherCostsFrame *pFrame = (CMDIHigherCostsFrame*)getFormViewByID(IDD_FORMVIEW3)->GetParent();
	if (pFrame != NULL)
	{
		pFrame->setEnableToolbar0(enable);
		pFrame->setEnableToolbar1(enable);
	}

	if (enable)	m_wndEdit1.SetFocus();

	m_bDataEnabled = enable;
}

void CHigherCostsFormView::importHighCostTable(void)
{
	int nTypeSet = -1;
	double fBreakValue = 0.0;
	double fFactor = 0.0;
	CString sName1,sDoneBy;
	vecObjectTemplate_hcost_table vecHCost;
	int nCountName = 0;
	CString sTmplName,sName,sCompareName,sCheckName;
	CString sXML;
	CString sXMLCompleted;
	CString sFilter;
	CTransaction_template recAdded;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22704),HCOST_TABLE_EXTENSION,HCOST_TABLE_EXTENSION);
		}
		delete xml;
	}

	// Handles clik on open button
	CFileDialog dlg( TRUE, HCOST_TABLE_EXTENSION, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		// Setup transport table; 081201 p�d
		TemplateParser pars;
		if (pars.LoadFromFile(dlg.GetPathName()))
		{
			sTmplName = dlg.GetFileTitle();

			pars.getObjTmplHCost(&nTypeSet,&fBreakValue,&fFactor,vecHCost,sName1,sDoneBy);

			// Check that the name isn't already used; 091027 p�d
			if (m_vecTransactionTemplate.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
				{
					sCompareName = m_vecTransactionTemplate[i].getTemplateName().Left(sTmplName.GetLength());
					if (sCompareName.CompareNoCase(sTmplName) == 0)
						nCountName++;
				}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			}	// if (m_vecTransactionTemplate.size() > 0)
			
			if (nCountName > 0) sName.Format(_T("%s(%d)"),sTmplName,nCountName);
			else sName = sTmplName;

			pars.getXML(sXML);
			sXMLCompleted.Format(_T("%s%s"),XML_FILE_HEADER,sXML);

			if (sDoneBy.IsEmpty())
				sDoneBy = getUserName().MakeUpper();

			m_wndEdit2.SetWindowTextW(sDoneBy);
			// Only ADD A NEW "Under utveckling"; 081201 p�d
			if (m_pDB != NULL)
			{
				m_pDB->addObjectTemplate(CTransaction_template(-1,sName,TEMPLATE_HCOST,sXMLCompleted,_T(""),sDoneBy));
			}	// if (m_pDB != NULL)
			// We need to reload templates from database
			// and set m_nDBIncdex to point to the last (just created)
			// template; 081201 p�d
			getObjectTemplatesFromDB();
			if (m_vecTransactionTemplate.size() > 0)
				m_nDBIndex = m_vecTransactionTemplate.size() - 1;	// Point to last item
		}	// if (pars.LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
		populateData(m_nDBIndex);
		setEnableData(TRUE);
	}	// if(dlg.DoModal() == IDOK)
}

void CHigherCostsFormView::exportHighCostTable(void)
{
	CString sName;
	CString sFilter;
	//-------------------------------------------------------------------
	// Save data before tryin' to export; 091012 p�d
	if (!saveHCostTemplate(0)) return;
	//-------------------------------------------------------------------

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22704),HCOST_TABLE_EXTENSION,HCOST_TABLE_EXTENSION);
		}
		delete xml;
	}

	sName = m_recActiveTemplate.getTemplateName();
	scanFileName(sName);
	if (sName.Right(6) != HCOST_TABLE_EXTENSION)
		sName += HCOST_TABLE_EXTENSION;
	// Handles clik on open button
	CFileDialog dlg( FALSE, HCOST_TABLE_EXTENSION, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		TemplateParser pars;
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			sName = dlg.GetPathName();
			if (sName.Right(6) != HCOST_TABLE_EXTENSION)
				sName += HCOST_TABLE_EXTENSION;
			pars.SaveToFile(sName);
		}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
	}
}


void CHigherCostsFormView::OnBnClickedRadioUseTable()
{
	m_wndLbl4.ShowWindow(SW_HIDE);
	m_wndLbl5.ShowWindow(SW_HIDE);
	m_wndEdit4.ShowWindow(SW_HIDE);
	m_wndEdit5.ShowWindow(SW_HIDE);

	m_wndReport.ShowWindow(SW_NORMAL);

	CMDIHigherCostsFrame *pFrame = (CMDIHigherCostsFrame*)getFormViewByID(IDD_FORMVIEW3)->GetParent();
	if (pFrame != NULL)
	{
		pFrame->setEnableToolbar0(TRUE);
		pFrame->setEnableToolbar1(TRUE);
	}

}

void CHigherCostsFormView::OnBnClickedRadioUseBreakFactor()
{
	m_wndReport.ShowWindow(SW_HIDE);

	m_wndLbl4.ShowWindow(SW_NORMAL);
	m_wndLbl5.ShowWindow(SW_NORMAL);
	m_wndEdit4.ShowWindow(SW_NORMAL);
	m_wndEdit5.ShowWindow(SW_NORMAL);

	CMDIHigherCostsFrame *pFrame = (CMDIHigherCostsFrame*)getFormViewByID(IDD_FORMVIEW3)->GetParent();
	if (pFrame != NULL)
	{
		pFrame->setEnableToolbar0(FALSE);
		pFrame->setEnableToolbar1(TRUE);
	}

}

void CHigherCostsFormView::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());
		m_wndEdit1.SetFocus();

	}
}