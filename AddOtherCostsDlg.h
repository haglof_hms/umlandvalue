#pragma once

#include "Resource.h"

// CAddOtherCostsDlg dialog

class CAddOtherCostsDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddOtherCostsDlg)

	CString m_sLangFN;

	CComboBox	m_wndCB1;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;

	//CListBox m_wndLB1;
	//CListBox m_wndLB2;

	CXTListCtrl m_listCtrl;
	CXTListCtrl m_listCtrl2;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CString m_sObjectName;
	int m_nObjID;

	CTransaction_elv_object m_recObject;

	vecTransaction_elv_properties m_vecProps;
	vecTransaction_elv_properties m_vecProps_selected;
	CTransaction_elv_properties recProps;
	void getPropertiesFromDB(void);
	CTransaction_elv_properties& getProps(int prop_id);

	int m_nSelectedCostsType;
	double m_fSumOtherComp;
	double m_fSumOtherComp_calc;
	CString m_sNotes;

	CUMLandValueDB *m_pDB;
protected:
	enum { NONE,COMP,COMP_VAT,COMP_INFR,COMP_MERCH } m_enumCompType;


public:
	CAddOtherCostsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddOtherCostsDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG25 };

	void setObject(CTransaction_elv_object& obj)	
	{ 
		m_recObject = obj; 
		m_sObjectName = m_recObject.getObjectName();
		m_nObjID = m_recObject.getObjID_pk();
	}

	vecTransaction_elv_properties& getSelectedProperties(void)	{ return m_vecProps_selected; }
	int getSelectedCostType(void)	{ return m_nSelectedCostsType; }
	double getOtherCompValue(void)	{ return m_fSumOtherComp; }
	double getOtherCompCalc(void)	{ return m_fSumOtherComp_calc; }
	CString getOtherCompNotes(void)	{ return m_sNotes; }

	void setDBConnection(CUMLandValueDB *db)	{	m_pDB = db;	}

protected:
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();

	afx_msg void OnBnClicked251();
	afx_msg void OnBnClicked252();
	afx_msg void OnBnClicked253();
	afx_msg void OnBnClicked254();
	afx_msg void OnCbnSelchangeList251();
	afx_msg void OnBnClickedOk();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit251();
};
