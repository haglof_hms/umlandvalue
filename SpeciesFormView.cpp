// SpeciesFormView.cpp : implementation file
//

#include "stdafx.h"
#include "SpeciesFormView.h"

#include "ResLangFileReader.h"


/////////////////////////////////////////////////////////////////////////////////////
//	Reportclasses for Species; 060317 p�d

//#HMS-94 inst�llningar f�r P30 tr�dslag p� objekt kontra alla tr�dslag 20220921 J�

/////////////////////////////////////////////////////////////////////////////////////
// CSpeciesFormView

IMPLEMENT_DYNCREATE(CSpeciesFormView, CXTResizeFormView)


BEGIN_MESSAGE_MAP(CSpeciesFormView, CXTResizeFormView)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	//ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_SPECIES_REPORT, OnReportChanged)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CSpeciesFormView::CSpeciesFormView()
	: CXTResizeFormView(CSpeciesFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bTest=FALSE;
	m_pDB = NULL;
}

CSpeciesFormView::~CSpeciesFormView()
{
	m_bTest=TRUE;
}

void CSpeciesFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_vecSpeciesData.clear();
	//m_sMsgCap.ReleaseBuffer();
	//m_sDoneSavingMsg.ReleaseBuffer();
	m_sLangAbrev.ReleaseBuffer();
	m_sLangFN.ReleaseBuffer();
	m_sarrCol.RemoveAll();
	m_arrP3Species.RemoveAll();
	if (m_wndReport1.GetSafeHwnd())
	{
		m_wndReport1.ResetContent();
	}

	CXTResizeFormView::OnDestroy();	
}

void CSpeciesFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

BOOL CSpeciesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
			m_bTest=FALSE;
		}

	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CSpeciesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

}

BOOL CSpeciesFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CSpeciesFormView::OnEraseBkgnd(CDC *pDC)
{

	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));
	return FALSE;
}

void CSpeciesFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		// TODO: Add extra initialization here
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				//m_sMsgCap	= xml.str(IDS_STRING213);
				//m_sDoneSavingMsg =	(xml.str(IDS_STRING2061));
				/*m_sRemoveSpecieMsg1 =	(xml.str(IDS_STRING3100));
				m_sRemoveSpecieMsg2.Format(_T("%s\n\n%s\n"),
					(xml.str(IDS_STRING3101)),
					(xml.str(IDS_STRING3102)));*/
				m_sarrCol.Add((xml.str(IDS_STRING900)));
				m_sarrCol.Add((xml.str(IDS_STRING901)));
				m_sarrCol.Add((xml.str(IDS_STRING902)));
				m_sarrCol.Add((xml.str(IDS_STRING903)));
			}
			xml.clean();
		}	// if (fileExists(sLangFN))

		m_arrP3Species.Add(_T("Tall"));
		m_arrP3Species.Add(_T("Gran"));
		m_arrP3Species.Add(_T("L�v"));

		setupReport1();

		m_bInitialized = TRUE;
	}

}
// CSpeciesFormView diagnostics

#ifdef _DEBUG
void CSpeciesFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CSpeciesFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PRIVATE METHODS

BOOL CSpeciesFormView::getSpecies(void)
{
	CTransaction_elv_object *pObj = getActiveObject();

	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getP30SpeciesInfo(m_vecSpeciesData,pObj->getObjID_pk()))
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void CSpeciesFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}

}

BOOL CSpeciesFormView::setupReport1(void)
{

	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_SPECIES_REPORT,FALSE,FALSE))
//		if (!m_wndReport1.Create(WS_CHILD|WS_TABSTOP|WS_VISIBLE|WM_VSCROLL, CRect(0, 0, 0, 0), this, IDC_SPECIES_REPORT))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0,m_sarrCol.GetAt(0), 40));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1,m_sarrCol.GetAt(1), 50));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2,m_sarrCol.GetAt(2), 120));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(3,m_sarrCol.GetAt(3), 120));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->GetEditOptions()->AddComboButton();



		m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
		m_wndReport1.SetMultipleSelection( FALSE );
		m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );

		addP30SpeciesConstraints();
		// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
		getSpecies();
		populateReport();

		// Need to set size of Report control; 051219 p�d
		RECT rect;
		GetClientRect(&rect);
		setResize(GetDlgItem(IDC_SPECIES_REPORT),1,1,rect.right - 1,rect.bottom - 1);

		m_wndReport1.AllowEdit(TRUE);
		m_wndReport1.FocusSubItems(TRUE);
		m_wndReport1.SetFocus();

	}

	return TRUE;

}

void CSpeciesFormView::OnSize(UINT nType,int cx,int cy)
{
	// Need to set size of Report control; 051219 p�d
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_SPECIES_REPORT),1,1,rect.right - 1,rect.bottom - 1);

	CXTResizeFormView::OnSize(nType, cx, cy);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CSpeciesFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{

	switch (wParam)
	{
		case ID_NEW_ITEM :
		{/*
			addSpecies();
			if (saveSpecies())
			{
				getSpecies();
				populateReport();
				setReportFocus();
			}*/
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (saveSpecies())
			{
				getSpecies();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			/*
			if (saveSpecies())
			{
				//getSpecies();
				if (removeSpecies())
				{
					getSpecies();
					populateReport();
				}
			}*/
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CSpeciesFormView::populateReport(void)
{
	// populate report; 060317 p�d
	m_wndReport1.ResetContent();
	int nP30specid=0;
	if (m_vecSpeciesData.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpeciesData.size();i++)
		{
			CTransaction_species rec = m_vecSpeciesData[i];
			nP30specid=rec.getP30SpcID();
			if(nP30specid<1 || nP30specid>3)
				nP30specid=1;//#HMS-94 Default Tall...g�r n�got innan en kontroll och s�tt r�tt default id p� alla tr�dslag id mellan 1-3
			m_wndReport1.AddRecord(new CSpeciesReportRec(rec.getID(),rec,(m_arrP3Species.GetAt(nP30specid-1))));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	return TRUE;
}


/*
void CSpeciesFormView::OnReportChanged(NMHDR * pNotifyStruct, LRESULT * )
{
	CString cTest=_T("");
	int nData=0;
	//enum_sortOrderDirection sortOrderDir = SORT_NONE;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
	case 3 :	// P30 tr�dslag
		{
			int nP30SpecId = 0;
			CXTPReportColumn *pCol0 = pItemNotify->pColumn->GetColumns()->Find(3);
			if (pCol0 != NULL)
			{
				if (pItemNotify->pColumn->GetIndex() == pCol0->GetIndex())
				{
					CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
					if (pRecItem != NULL)
					{
						CSpeciesReportRec *pRec = (CSpeciesReportRec*)pRecItem->GetRecord();
						CXTPReportRecordItemEditOptions *pEdOptions = pItemNotify->pItem->GetEditOptions(pCol0);
						if (pEdOptions != NULL)
						{
							cTest=pRecItem->GetCaption(pCol0);
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(cTest);
							if (pConst != NULL)
							{
								nData=pConst->m_dwData;
								// index skall vara mellan 1 och 3, dvs tall gran eller l�v
								if ( nData >= 1 && nData <=	3)
								{
									nP30SpecId = nData;
									pRec->GetItem(3)->SetCaption(m_arrP3Species.GetAt(nP30SpecId-1));
								}
							}	
						}
					}
				}
			}
			break;
		}
	};
}*/


void CSpeciesFormView::addP30SpeciesConstraints(void)
{
	int nPropID = -1,nStatusNum = 0;
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport1.GetColumns();
	if (pColumns != NULL)
	{
		// Add Status-types; 080410 p�d
		CXTPReportColumn *pStatusCol = pColumns->Find(3);
		if (pStatusCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 080410 p�d
			pCons = pStatusCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			if (m_arrP3Species.GetCount() > 0)
			{
				for (int i = 0;i < m_arrP3Species.GetCount();i++)
				{
					pStatusCol->GetEditOptions()->AddConstraint((m_arrP3Species.GetAt(i)),i+1);
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)

}

void CSpeciesFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	CSpeciesReportRec *pRec = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRow = pRows->GetAt(i);
				if (pRow != NULL)
				{
					pRec = (CSpeciesReportRec*)pRow->GetRecord();
					if (pRec != NULL)
					{
						if (pRec->getColumnText(1).IsEmpty())
						{
							pRow->SetSelected(TRUE);
							m_wndReport1.SetFocusedRow(pRow);
							break;
						}	// if (pRec->getColumnText(1).IsEmpty())
					}	// if (pRec != NULL)
				}	// if (pRow != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows)
	}
}




BOOL CSpeciesFormView::saveSpecies(void)
{
CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_species rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{

			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{

				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CSpeciesReportRec *pRec = (CSpeciesReportRec *)pRecs->GetAt(i);
										
					int nP30SpecId = 0;
					int nSpecId=0;

					//CXTPReportColumn *pCol0 = m_wndReport1.GetColumns()->GetAt(i);
					CXTPReportColumn *pCol0 = m_wndReport1.GetColumns()->Find(3);

					if (pCol0 != NULL)
					{
						CXTPReportRecordItemEditOptions *pEdOptions = pCol0->GetEditOptions();
						

						if (pEdOptions != NULL)
						{
							CString cTest=pRec->GetItem(3)->GetCaption(pCol0);
							
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(cTest);
							if (pConst != NULL)
							{
								// index mellan 1-3 dvs tall gran eller l�v.
								if (pConst->m_dwData >= 1 && pConst->m_dwData <=3)
								{
									nP30SpecId = pConst->m_dwData;
									nSpecId=pRec->getColumnInt(0);
									m_pDB->updateP30SpeciesInfo(nSpecId,nP30SpecId,pObj->getObjID_pk());
								}

							}	
						}

					}
					
				}	// for (int i = 0;i < pRecs->GetCount();i++)

	// Commented out (PL); 070402 p�d
	//			::MessageBox(0,m_sDoneSavingMsg,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
			}	// if (pRecs->GetCount() > 0)
			m_wndReport1.setIsDirty(FALSE);
			bReturn = TRUE;

		}	// if (pDB != NULL)
	}
	return bReturn;
}

/*
// Get Next id for Specie, based on last id entered; 071126 p�d
int CSpeciesFormView::getSpecieNextID(void)
{
	BOOL bFound = TRUE;
	int nNextID = 1;
	CString S;
	CSpeciesReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport1.GetRows();
	CXTPReportRows *pRow = NULL;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CSpeciesReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getColumnInt(0) != i+1)
				{
					return i+1;
				}
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
	return pRows->GetCount()+1;
}*/

/*
BOOL CSpeciesFormView::addSpecies(void)
{
	int nNextSpcID = 1;
	CXTPReportRows *pRows = m_wndReport1.GetRows();
	if (pRows != NULL) nNextSpcID = getSpecieNextID();

	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new CSpeciesReportRec(nNextSpcID,(m_arrP3Species.GetAt(0))));
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();
	}

	return TRUE;
}*/

/*
BOOL CSpeciesFormView::removeSpecies(void)
{
	CTransaction_species data;
	CString sMsg;
	CSpeciesReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CSpeciesReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			data = pRec->getRecord();
			// Ask user if he realy want to remove specie; 081029 p�d
			sMsg.Format(_T("%s -> %s\n\n%s"),m_sRemoveSpecieMsg1,pRec->getColumnText(1),m_sRemoveSpecieMsg2);
			if (::MessageBox(this->GetSafeHwnd(),(sMsg),(m_sMsgCap),MB_ICONQUESTION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
			{
				m_pDB->delSpecie(data);
			}
		}	// if (pRec != NULL && m_pDB != NULL)
		return TRUE;
	}	// if (pRow != NULL)

	return FALSE;
}*/