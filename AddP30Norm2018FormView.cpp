// AddP30NewNormFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "AddP30Norm2018FormView.h"

#include "ResLangFileReader.h"

// Values for PINE
_si_from_to pine_2018_area1_2_3_4A[4]	= { {10,16},{17,20},{21,24},{25,40} };
_si_from_to pine_2018_area4B[4]			= { {10,18},{19,23},{24,27},{28,40} };
_si_from_to pine_2018_area5[4]			= { {10,18},{19,23},{24,27},{28,40} };

_si_from_to spruce_2018_area1_2[4]		= { {10,15},{16,20},{21,27},{28,40} };
_si_from_to spruce_2018_area3[4]		= { {10,14},{15,18},{19,24},{25,40} };
_si_from_to spruce_2018_area4A[4]		= { {10,14},{15,18},{19,24},{25,40} };
_si_from_to spruce_2108_area4B[4]		= { {10,16},{17,21},{22,25},{26,40} };
_si_from_to spruce_2108_area5[4]		= { {10,16},{17,21},{22,25},{26,40} };

_si_from_to birch_2108_area1_2_3_4A[3]	= { {14,18},{19,22},{23,40} };
_si_from_to birch_2018_area4B_5[3]		= { {14,18},{19,23},{24,40} };

 

IMPLEMENT_DYNCREATE(CAddP30Norm2018FormView, CXTPReportView)

BEGIN_MESSAGE_MAP(CAddP30Norm2018FormView, CXTPReportView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportValueChanged)
END_MESSAGE_MAP()

CAddP30Norm2018FormView::CAddP30Norm2018FormView()
	: CXTPReportView()
{
	m_bInitialized = FALSE;
}

CAddP30Norm2018FormView::~CAddP30Norm2018FormView()
{
}

void CAddP30Norm2018FormView::OnDestroy()
{

	CXTPReportView::OnDestroy();	
}

BOOL CAddP30Norm2018FormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTPReportView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CAddP30Norm2018FormView::DoDataExchange(CDataExchange* pDX)
{
	CXTPReportView::DoDataExchange(pDX);
}

void CAddP30Norm2018FormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	if (! m_bInitialized )
	{

		// Setup language filename; 080402 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		// Setup report
		setupReport();

		m_bInitialized = TRUE;
	}
}

void CAddP30Norm2018FormView::OnSize(UINT nType,int cx,int cy)
{

	if (m_wndReport.GetSafeHwnd())
		setResize(&m_wndReport,2,2,cx-4,cy-4);

	CXTPReportView::OnSize(nType,cx,cy);
}

// CAddP30NewNormFormView diagnostics

void CAddP30Norm2018FormView::OnReportValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CP30NewNormReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_3)
		{
			CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
			if (pRecItem != NULL)
			{
				pRec = (CP30NewNormReportRec*)pRecItem->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColumnFloatMM(COLUMN_3) == 0.0 || pRec->getColumnFloatOK(COLUMN_3) == false)
					{
						::MessageBox(this->GetSafeHwnd(),m_sMsgValueError,m_sMsgCap,MB_ICONSTOP | MB_OK);
						// Make sure toolbuttons are enabled; 101006 p�d
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
						AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
					}
				}
			}			
		}
	}
}

#ifdef _DEBUG
void CAddP30Norm2018FormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

#ifndef _WIN32_WCE
void CAddP30Norm2018FormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
}
#endif
#endif //_DEBUG


BOOL CAddP30Norm2018FormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			// Get text from languagefile; 061207 p�d
			if (GetReportCtrl().GetSafeHwnd() != NULL)
			{

				GetReportCtrl().ShowWindow( SW_NORMAL );

				m_sMsgCap = xml.str(IDS_STRING229);
				m_sMsgValueError = xml.str(IDS_STRING2524);

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING2515), 100));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING2516), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING2517), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING2518), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
				GetReportCtrl().SetMultipleSelection( FALSE );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				GetReportCtrl().AllowEdit(TRUE);
				GetReportCtrl().FocusSubItems(TRUE);
			}
		}
		xml.clean();
	}	// if (fileExists(sLangFN))
	return TRUE;
}

// CAddP30NewNormFormView message handlers

void CAddP30Norm2018FormView::addRow(void)
{
	if (GetReportCtrl().GetSafeHwnd())
	{
		GetReportCtrl().AddRecord(new CP30NewNormReportRec());

		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();

	}	// if (GetReportCtrl().GetSafeHwnd())
}

void CAddP30Norm2018FormView::delRow(void)
{
	CXTPReportRow *pRow = NULL;
	CXTPReportRecord *pRec = NULL;
	if (GetReportCtrl().GetSafeHwnd())
	{
		pRow = GetReportCtrl().GetFocusedRow();
		if (pRow != NULL)
		{
			pRec = pRow->GetRecord();
			if (pRec != NULL) 
			{
				pRec->Delete();

				GetReportCtrl().Populate();
				GetReportCtrl().UpdateWindow();
			}	// if (pRec != NULL) 
		}	// if (pRow != NULL)
	}	// if (GetReportCtrl().GetSafeHwnd())
}

// Create data and add to m_sarrData, return; 090415 p�d
void CAddP30Norm2018FormView::checkData(LPCTSTR spc_name,CStringArray &arr_err)
{
	CString sData,sMsg;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CP30NewNormReportRec *pRec = NULL;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (!pRec->getColumnFloatOK(COLUMN_3))
				{
					sMsg.Format(L"  %s  %d - %d  -> %.1f",spc_name,
								pRec->getColumnInt(COLUMN_0),
								pRec->getColumnInt(COLUMN_1),
								pRec->getColumnFloatMM(COLUMN_3));
					arr_err.Add(sMsg);
					pRec = NULL;
				}
			}
		}

		pRows = NULL;
	}
}

void CAddP30Norm2018FormView::getData(CStringArray &arr)
{
	CString sData;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CP30NewNormReportRec *pRec = NULL;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				sData.Format(NODE_P30NN_DATA,
					pRec->getColumnInt(COLUMN_0),
					pRec->getColumnInt(COLUMN_1),
					pRec->getColumnInt(COLUMN_2),
					pRec->getColumnFloatMM(COLUMN_3));

					arr.Add(sData);
					pRec = NULL;
			}
		}

		pRows = NULL;
	}
}


void CAddP30Norm2018FormView::setReport(vecObjectTemplate_p30_nn_table &vec,int spc_id)
{
	if (vec.size() > 0)
	{
		if (GetReportCtrl().GetSafeHwnd())
		{
			resetReport();
			for (UINT i = 0;i < vec.size();i++)
			{
				if (vec[i].getSpcID() == spc_id)
				{
					GetReportCtrl().AddRecord(new CP30NewNormReportRec(vec[i].getFrom(),vec[i].getTo(),vec[i].getPrice(),vec[i].getPriceRel()));
				}	// if (vec[i].getSpcID() == spc_id)
			}	// for (UINT i = 0;i < vec.size();i++)
			GetReportCtrl().Populate();
			GetReportCtrl().UpdateWindow();
		}	// if (GetReportCtrl().GetSafeHwnd())
	}	// if (vec.size() > 0)
}

void CAddP30Norm2018FormView::setupSIForP30(LPCTSTR area,int spc)
{
	CString sArea(area);
	if (GetReportCtrl().GetSafeHwnd())
	{

		CXTPReportRows *pRows = GetReportCtrl().GetRows();
		CP30NewNormReportRec *pRec = NULL;
		//GetReportCtrl().ResetContent(); Ej nolla V�rden, s�tt bara om gr�nserna 20180320 J�
		//------------------------------------------------------------------------------------------------------------
		// Handle Pine; 090504 p�d
		if (spc == 1)
		{
			if (sArea.Compare(_T("1")) == 0 || sArea.Compare(_T("2")) == 0 || sArea.Compare(_T("3")) == 0 || sArea.Compare(_T("4A")) == 0)
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,pine_2018_area1_2_3_4A[i].from);
								pRec->setColumnInt(COLUMN_1,pine_2018_area1_2_3_4A[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(pine_2018_area1_2_3_4A[i].from,pine_2018_area1_2_3_4A[i].to,0,0.0));
					}
				}
			}
			else if (sArea == _T("4B"))
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,pine_2018_area4B[i].from);
								pRec->setColumnInt(COLUMN_1,pine_2018_area4B[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{

					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(pine_2018_area4B[i].from,pine_2018_area4B[i].to,0,0.0));
					}
				}
			}
			else if (sArea == _T("5"))
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,pine_2018_area5[i].from);
								pRec->setColumnInt(COLUMN_1,pine_2018_area5[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(pine_2018_area5[i].from,pine_2018_area5[i].to,0,0.0));
					}
				}
			}
		}	//	if (spc == 1)
		//------------------------------------------------------------------------------------------------------------
		// Handle Spruce; 090504 p�d
		if (spc == 2)
		{
			if (sArea.Compare(_T("1")) == 0 || sArea.Compare(_T("2")) == 0)
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,spruce_2018_area1_2[i].from);
								pRec->setColumnInt(COLUMN_1,spruce_2018_area1_2[i].to);
							}
							pRec = NULL;
						}
					}	
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(spruce_2018_area1_2[i].from,spruce_2018_area1_2[i].to,0,0.0));
					}
				}
			}
			else if (sArea == _T("3"))
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,spruce_2018_area3[i].from);
								pRec->setColumnInt(COLUMN_1,spruce_2018_area3[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(spruce_2018_area3[i].from,spruce_2018_area3[i].to,0,0.0));
					}
				}
			}
			else if (sArea == _T("4A"))
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,spruce_2018_area4A[i].from);
								pRec->setColumnInt(COLUMN_1,spruce_2018_area4A[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(spruce_2018_area4A[i].from,spruce_2018_area4A[i].to,0,0.0));
					}
				}
			}
			else if (sArea == _T("4B"))
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,spruce_2108_area4B[i].from);
								pRec->setColumnInt(COLUMN_1,spruce_2108_area4B[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(spruce_2108_area4B[i].from,spruce_2108_area4B[i].to,0,0.0));
					}
				}
			}
			else if (sArea == _T("5"))
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 4;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,spruce_2108_area5[i].from);
								pRec->setColumnInt(COLUMN_1,spruce_2108_area5[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 4;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(spruce_2108_area5[i].from,spruce_2108_area5[i].to,0,0.0));
					}
				}
			}
		}	// if (spc == 2)
		//------------------------------------------------------------------------------------------------------------
		// Handle Birch; 090504 p�d
		if (spc == 3)
		{
			if (sArea.Compare(_T("1")) == 0 || sArea.Compare(_T("2")) == 0 || sArea.Compare(_T("3")) == 0 || sArea.Compare(_T("4A")) == 0)
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 3;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,birch_2108_area1_2_3_4A[i].from);
								pRec->setColumnInt(COLUMN_1,birch_2108_area1_2_3_4A[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 3;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(birch_2108_area1_2_3_4A[i].from,birch_2108_area1_2_3_4A[i].to,0,0.0));
					}
				}
			}
			else if (sArea.Compare(_T("4B")) == 0 || sArea.Compare(_T("5")) == 0)
			{
				if(pRows->GetCount()>0)	//Kolla om det finns n�gra rader annars skapa rader
				{
					if (pRows != NULL)
					{
						for (int i = 0;i < 3;i++)
						{
							pRec = (CP30NewNormReportRec*)pRows->GetAt(i)->GetRecord();
							if (pRec != NULL)
							{
								pRec->setColumnInt(COLUMN_0,birch_2018_area4B_5[i].from);
								pRec->setColumnInt(COLUMN_1,birch_2018_area4B_5[i].to);
							}
							pRec = NULL;
						}
					}
				}
				else
				{
					for (short i = 0;i < 3;i++)
					{
						GetReportCtrl().AddRecord(new CP30NewNormReportRec(birch_2018_area4B_5[i].from,birch_2018_area4B_5[i].to,0,0.0));
					}
				}
			}
		}	// if (spc == 3)

		pRows = NULL;

		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();

	}

}
