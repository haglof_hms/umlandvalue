#include "stdafx.h"
#include "ShowInformationDlgPrecut.h"


// CShowInformationDlgPrecut dialog
// HMS-49 2020-02-11
IMPLEMENT_DYNAMIC(CShowInformationDlgPrecut, CDialog)



BEGIN_MESSAGE_MAP(CShowInformationDlgPrecut, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CShowInformationDlgPrecut::CShowInformationDlgPrecut(CWnd* pParent)
	: CDialog(CShowInformationDlgPrecut::IDD, pParent)
{
	m_pDB = NULL;

	m_bInitialized = FALSE;
	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;	
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);


	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;	
	lfFont1.lfStrikeOut = TRUE;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1_strikeout.CreatePointFontIndirect(&lfFont1);




	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	lfFont1.lfStrikeOut = TRUE;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2_strikeout.CreatePointFontIndirect(&lfFont1);

	

	m_nEvalId=-1;
	m_nObjId=-1;
	m_nPropId=-1;

}

CShowInformationDlgPrecut::~CShowInformationDlgPrecut()
{
}

void CShowInformationDlgPrecut::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();
	m_fnt1_strikeout.DeleteObject();
	m_fnt2_strikeout.DeleteObject();

	m_xml.clean();

	CDialog::OnDestroy();
}

void CShowInformationDlgPrecut::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// CShowInformationDlg2 message handlers

BOOL CShowInformationDlgPrecut::OnInitDialog()
{
	CDialog::OnInitDialog();
	CWnd *pBtn = NULL;

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			m_xml.Load(m_sLangFN);

			SetWindowText((m_xml.str(IDS_STRING6400)));
			if ((pBtn = GetDlgItem(IDCANCEL)) != NULL)
				pBtn->SetWindowText((m_xml.str(IDS_STRING4245)));
		}
		m_bInitialized = TRUE;
	}		// if (!m_bInitialized)



	return TRUE;
}


// CShowInformationDlg message handlers

void CShowInformationDlgPrecut::OnPaint()
{
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-30,10,10);

	showPreCut(pDC);

	CDialog::OnPaint();

}


// PRIVATE

void CShowInformationDlgPrecut::showPreCut(CDC *dc)
{

	CString sName,sDoneBy;
	TEXTMETRIC tm;
	BOOL bFound = FALSE;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties *pProp = getActiveProperty();

	double fPreCutInfo_P30_Pine=0.0;
	double fPreCutInfo_P30_Spruce=0.0;
	double fPreCutInfo_P30_Birch=0.0;
	double fPreCutInfo_Andel_Pine=0.0;
	double fPreCutInfo_Andel_Spruce=0.0;
	double fPreCutInfo_Andel_Birch=0.0;
	double fPreCutInfo_CorrFact=0.0;
	double fPreCutInfo_Ers_Pine=0.0;
	double fPreCutInfo_Ers_Spruce=0.0;
	double fPreCutInfo_Ers_Birch=0.0;
	double fPreCutInfo_ReducedBy=0.0;
	double fErs_Pine=0.0;
	double fErs_Spruce=0.0;
	double fErs_Birch=0.0;
	double fAreal=0.0;
	double fErs_Tot=0.0;
	CString sStr=_T("");
	int nLen=0,nLen1=0,nLen2=0,nLen3=0;

	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_hcost_table vecHCost;
	CObjectTemplate_hcost_table recHCost;
	if (pObj != NULL && pProp != NULL && m_pDB != NULL && m_nEvalId!=-1)
	{
		m_nObjId = pObj->getObjID_pk();
		m_nPropId= pProp->getPropID_pk();

		CTransaction_eval_evaluation *pEval = m_pDB->getObjectEvaluation(m_nPropId,m_nObjId,m_nEvalId);

		if(pEval!=NULL)
		{

			if(pEval->getEValType()==EVAL_TYPE_ANNAN)  // #HMS-95 Ingen f�rtidig om annan mark �r satt som v�rderingstyp
			{
				dc->SelectObject(&m_fnt1);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

				dc->TextOut(10, tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53019))));
			}
			else
			{


				fPreCutInfo_P30_Pine=pEval->getPreCutInfo_P30_Pine();
				fPreCutInfo_P30_Spruce=pEval->getPreCutInfo_P30_Spruce();
				fPreCutInfo_P30_Birch=pEval->getPreCutInfo_P30_Birch();
				fPreCutInfo_Andel_Pine=pEval->getPreCutInfo_Andel_Pine();
				fPreCutInfo_Andel_Spruce=pEval->getPreCutInfo_Andel_Spruce();
				fPreCutInfo_Andel_Birch=pEval->getPreCutInfo_Andel_Birch();
				fPreCutInfo_CorrFact=pEval->getPreCutInfo_CorrFact();
				fPreCutInfo_Ers_Pine=pEval->getPreCutInfo_Ers_Pine();
				fPreCutInfo_Ers_Spruce=pEval->getPreCutInfo_Ers_Spruce();
				fPreCutInfo_Ers_Birch=pEval->getPreCutInfo_Ers_Birch();
				fPreCutInfo_ReducedBy=pEval->getPreCutInfo_ReducedBy();
				fAreal=pEval->getEValAreal();
				fErs_Pine=fPreCutInfo_P30_Pine/10.0*fPreCutInfo_Andel_Pine*fPreCutInfo_Ers_Pine;
				fErs_Spruce=fPreCutInfo_P30_Spruce/10.0*fPreCutInfo_Andel_Spruce*fPreCutInfo_Ers_Spruce;
				fErs_Birch=fPreCutInfo_P30_Birch/10.0*fPreCutInfo_Andel_Birch*fPreCutInfo_Ers_Birch;
				fErs_Tot=(fErs_Pine+fErs_Spruce+fErs_Birch)*fPreCutInfo_CorrFact;


				nLen=0;
				dc->SelectObject(&m_fnt1);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

				dc->TextOut(10, tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53016))));

				dc->TextOut(10, 3*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53010))));
				dc->TextOut(10, 4*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53011))));
				dc->TextOut(10, 5*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53012))));

				dc->TextOut(10, 6*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53013))));
				dc->TextOut(10, 7*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53017))));

				dc->TextOut(10, 9*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53014))));

				if(fPreCutInfo_ReducedBy>0.0)
				{
					dc->SelectObject(&m_fnt1_strikeout);
					dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
				}

				dc->TextOut(10, 11*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53015))));
				dc->TextOut(10, 12*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53018))));

				nLen1 = m_xml.str(IDS_STRING53010).GetLength();
				nLen2 = m_xml.str(IDS_STRING53011).GetLength();
				nLen3 = m_xml.str(IDS_STRING53012).GetLength();
				nLen = max(nLen1,nLen2);
				nLen = max(nLen,nLen3);

				if(fPreCutInfo_ReducedBy>0.0)
				{
					dc->GetTextMetrics(&tm);
					dc->SelectObject(&m_fnt2_strikeout);			
				}
				else
				{
					dc->GetTextMetrics(&tm);
					dc->SelectObject(&m_fnt2);
				}
				dc->DrawText(formatData(_T("(%.1f x %.2f x %.0f) = %.1f"),fPreCutInfo_P30_Pine/10.0,fPreCutInfo_Andel_Pine,fPreCutInfo_Ers_Pine,fErs_Pine)			,CRect(50+nLen*tm.tmAveCharWidth,3*tm.tmHeight,210+nLen*tm.tmAveCharWidth,4*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("(%.1f x %.2f x %.0f) = %.1f"),fPreCutInfo_P30_Spruce/10.0,fPreCutInfo_Andel_Spruce,fPreCutInfo_Ers_Spruce,fErs_Spruce)	,CRect(50+nLen*tm.tmAveCharWidth,4*tm.tmHeight,210+nLen*tm.tmAveCharWidth,5*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("(%.1f x %.2f x %.0f) = %.1f"),fPreCutInfo_P30_Birch/10.0,fPreCutInfo_Andel_Birch,fPreCutInfo_Ers_Birch,fErs_Birch)		,CRect(50+nLen*tm.tmAveCharWidth,5*tm.tmHeight,210+nLen*tm.tmAveCharWidth,6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fPreCutInfo_CorrFact)																							,CRect(50+nLen*tm.tmAveCharWidth,6*tm.tmHeight,210+nLen*tm.tmAveCharWidth,7*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.3f"),fAreal)																											,CRect(50+nLen*tm.tmAveCharWidth,7*tm.tmHeight,210+nLen*tm.tmAveCharWidth,8*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);

				nLen = m_xml.str(IDS_STRING53015).GetLength();

				dc->DrawText(formatData(_T("(%.1f + %.1f + %.1f)x %.2f = %.0fkr/ha"),fErs_Pine,fErs_Spruce,fErs_Birch,fPreCutInfo_CorrFact,fErs_Tot)				,CRect(10+nLen*tm.tmAveCharWidth,11*tm.tmHeight,270+nLen*tm.tmAveCharWidth,12*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("(%.1f x %.3f) = %.0fkr"),fErs_Tot,fAreal,fErs_Tot*fAreal)																,CRect(10+nLen*tm.tmAveCharWidth,12*tm.tmHeight,255+nLen*tm.tmAveCharWidth,13*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);

				//Markers�ttning satt till �verlappande 25%
				if(fPreCutInfo_ReducedBy>0.0)
				{
					dc->SelectObject(&m_fnt1);			
					dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
					dc->TextOut(10, 14*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING42051))));
					dc->TextOut(10, 15*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53015))));
					dc->TextOut(10, 16*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53018))));

					dc->GetTextMetrics(&tm);
					dc->SelectObject(&m_fnt2);
					dc->DrawText(formatData(_T("%.0f x %.2f = %.0fkr/ha"),fErs_Tot,fPreCutInfo_ReducedBy,fErs_Tot*fPreCutInfo_ReducedBy)								,CRect(10+nLen*tm.tmAveCharWidth,15*tm.tmHeight,270+nLen*tm.tmAveCharWidth,16*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
					dc->DrawText(formatData(_T("(%.1f x %.3f) = %.0fkr"),fErs_Tot*fPreCutInfo_ReducedBy,fAreal,fErs_Tot*fPreCutInfo_ReducedBy*fAreal)					,CRect(10+nLen*tm.tmAveCharWidth,16*tm.tmHeight,255+nLen*tm.tmAveCharWidth,17*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				}
			}
		}
	}
	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	pProp = NULL;
}