#pragma once

#include "Resource.h"
#include <list>
#include <map>

typedef std::map<CString, int> SpecieMap;
typedef std::list<CString*> SpecieList;


// CMapSpeciesDlg dialog

class CMapSpeciesDlg : public CDialog
{
	DECLARE_DYNAMIC(CMapSpeciesDlg)

public:
	CMapSpeciesDlg(SpecieMap *pSpcmap,
				   vecTransactionSpecies *pSpecies,
				   CWnd* pParent = NULL);
	virtual ~CMapSpeciesDlg();

	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG32 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

	CXTPPropertyGrid m_grid;
	SpecieList m_vals;
	SpecieMap *m_pSpcmap;
	vecTransactionSpecies *m_pSpecies;

	DECLARE_MESSAGE_MAP()
};
