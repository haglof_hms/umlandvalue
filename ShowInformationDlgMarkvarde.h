#pragma once
#include "Resource.h"
#include "ResLangFileReader.h"

// CShowInformationDlgMarkvarde dialog
// HMS-49 2020-02-11
class CShowInformationDlgMarkvarde : public CDialog
{
	DECLARE_DYNAMIC(CShowInformationDlgMarkvarde)

	BOOL m_bInitialized;
	CString m_sLangFN;
	RLFReader m_xml;

	int m_nEvalId;
	int m_nObjId;
	int m_nPropId;
	int m_nNormId;
	CFont m_fnt1;
	CFont m_fnt1_strikeout;
	CFont m_fnt2;
	CFont m_fnt2_strikeout;

	CUMLandValueDB *m_pDB;

	void showMarkvarde(CDC *dc);
	void setElv(int nEvalId)
	{	
	m_nEvalId=nEvalId;
	}


public:
	CShowInformationDlgMarkvarde(CWnd* pParent = NULL);   // standard constructor
	virtual ~CShowInformationDlgMarkvarde();	

// Dialog Data
	enum { IDD = IDD_DIALOG_MARKVARDE };

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
