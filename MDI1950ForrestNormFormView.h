#pragma once

#include "Resource.h"

#include "1950ForrestNormFrame.h"
#include "ObjectFormView.h"
#include "ObjectResultFormView.h"
#include "PropertiesFormView.h"
#include "CruiseAndEvalueFormView.h"

// CMDI1950ForrestNormFormView form view
///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void PreCloseFrame(CFrameWnd *);
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc2

class CMDIFrameDoc2 : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc2();
	DECLARE_DYNCREATE(CMDIFrameDoc2)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc2)
	public:
	virtual BOOL OnNewDocument();
	virtual void PreCloseFrame(CFrameWnd *);
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc2();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CMDI1950ForrestNormFormView : public CView
{
	DECLARE_DYNCREATE(CMDI1950ForrestNormFormView)

	//private:
	BOOL m_bInitialized;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CString m_sObjTabCaption;
	CString m_sPropTabCaption;

	enumACTION m_enumACTION;
protected:
	CMDI1950ForrestNormFormView();           // protected constructor used by dynamic creation
	virtual ~CMDI1950ForrestNormFormView();

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL	saveObject(void);
	BOOL	createInventoryDirectory(void);
public:
//	enum { IDD = IDD_FORMVIEW };

	CMyTabControl &getTabCtrl(void)
	{
		return m_wndTabControl;
	}

	CObjectFormView *getObjectFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(TAB_GENERAL_DATA);
		if (m_tabManager)
		{
			CObjectFormView* pView = DYNAMIC_DOWNCAST(CObjectFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CObjectFormView, pView);
			return pView;
		}
		return NULL;
	}
	
	CPropertiesFormView *getPropertiesFormView(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(TAB_PROPERTIES);
		if (m_tabManager)
		{
			CPropertiesFormView* pView = DYNAMIC_DOWNCAST(CPropertiesFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CPropertiesFormView, pView);
			return pView;
		}
		return NULL;
	}
	enumACTION getAction(void)				{ return m_enumACTION; }
	void setAction(enumACTION action)	{ m_enumACTION = action; }

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	CMDIFrameDoc* GetDocument();

protected:
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDI1950ForrestNormFormView)
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	void OnSetFocus(CWnd* pWnd);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);

	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIFrameDoc* CMDI1950ForrestNormFormView::GetDocument()
	{ return (CMDIFrameDoc*)m_pDocument; }
#endif

