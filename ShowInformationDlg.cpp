// ShowInformationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ShowInformationDlg.h"


// CShowInformationDlg dialog

IMPLEMENT_DYNAMIC(CShowInformationDlg, CDialog)

BEGIN_MESSAGE_MAP(CShowInformationDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CShowInformationDlg::CShowInformationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShowInformationDlg::IDD, pParent)
{
	m_pDB = NULL;

	m_bInitialized = FALSE;
	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);
}

CShowInformationDlg::~CShowInformationDlg()
{
}

void CShowInformationDlg::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();

	m_xml.clean();

	CDialog::OnDestroy();
}


void CShowInformationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CShowInformationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CWnd *pBtn = NULL;

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			m_xml.Load(m_sLangFN);

			if ((pBtn = GetDlgItem(IDCANCEL)) != NULL)	pBtn->SetWindowText((m_xml.str(IDS_STRING4245)));
		}
		m_bInitialized = TRUE;
	}		// if (!m_bInitialized)



	return TRUE;
}


// CShowInformationDlg message handlers

void CShowInformationDlg::OnPaint()
{
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

//	clip.DeflateRect(1,1);
	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-30,10,10);

	showVoluntaryDeal(pDC);

	CDialog::OnPaint();

}


// PRIVATE

void CShowInformationDlg::showVoluntaryDeal(CDC *dc)
{
	int nTypeOfNet = -1;	// Lokalt = 0,Regionalt = 1
	int nObjID = -1;
	int nPropID = -1;
	int nLen = 0;
	int nLen1_0 = 0;
	int nLen1_1 = 0;
	int nLen1_2 = 0;
	int nLen1_3 = 0;
	int nLen1_4 = 0;
	int nLen1_5 = 0;
	int nLen2_0 = 0;
	int nLen2_1 = 0;
	int nLen2_2 = 0;
	int nLen2_3 = 0;
	int nLen2_4 = 0;
	int nLenPlus=55;
	int nNumOfEvals = 0;
	int nIndex=0;
	int nObjTyp=0;
	double fLandValue = 0.0;
	double fPreCutValue = 0.0;
	double fStormDryValue = 0.0;
	double fRandTreesValue = 0.0;
	double fPriceBase = 0.0;
	double fMaxPercent = 0.0;
	double fPercent = 0.0;
	double fSumPercentOfPriceBase = 0.0;
	double fSUMCompensation = 0.0;
	double fTimberVolume = 0.0;
	double fTimberValue = 0.0;
	double fTimberCost = 0.0;
	double fTimberRotnetto = 0.0;
	double fGrotVolume = 0.0;
	double fGrotValue = 0.0;
	double fGrotCost = 0.0;

	double fSUMExpropriation = 0.0;
	double fExpropriationPerc = 0.0;

	double fMaxCompensation = 0.0;
	double fActualCompensation = 0.0;
	double fPercentOfPriceBase = 0.0;
	double fDbVolDealValue = 0.0;
	CString sVolDeal, sSumVolDeal;
	CString sPolicy;
	TEXTMETRIC tm;
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

	CTransaction_elv_properties *pProp = getActiveProperty();
	// Get information on "Basbelopp","Procenttal"; 080515 p�d
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL && m_pDB != NULL)
	{
		// Setup data; 080515 p�d
		nObjID = pProp->getPropObjectID_pk();
		nPropID = pProp->getPropID_pk();
		fDbVolDealValue = pProp->getPropVoluntaryDealValue();
		fSumPercentOfPriceBase = 0.0;
		m_pDB->getObjVolDealValues(nObjID, &nTypeOfNet, &fPriceBase, &fMaxPercent, &fPercent, &fPercentOfPriceBase);
		m_pDB->getNumOfEvaluations(nObjID, nPropID, &nNumOfEvals);
		if (!(pProp->getPropNumOfStands() || nNumOfEvals))
		{
			fPriceBase = 0;
			fMaxPercent = 0;
			fPercent = 0;
		}
		
		dc->SelectObject(&m_fnt1);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

		// Set dialog caption
		if (nTypeOfNet == TYPEOFNET_LOCAL)
		{
			// Old local
			sVolDeal = m_xml.str(IDS_STRING3014);
		}
		else if(nTypeOfNet == TYPEOFNET_SVENSKAKRAFTNAT)
		{
			// Regional/SVK
			sVolDeal = m_xml.str(IDS_STRING3036);
		}
		else if(nTypeOfNet == TYPEOFNET_SWEDISHENERGY) 
		{
			// Svensk Energi 2014
			sVolDeal = m_xml.str(IDS_STRING3037);
		}
		else 
		{
			// Svensk Energi 2019 
			sVolDeal = m_xml.str(IDS_STRING3039);
		}
		SetWindowText(m_xml.str(IDS_STRING5210) + _T(" ") + sVolDeal);

		// Get aggregate information from database, on Infring; 080515 p�d
		m_pDB->getObjProp_infr(nObjID,nPropID,&fLandValue,&fPreCutValue,&fStormDryValue,&fRandTreesValue);
		m_pDB->getObjPropTraktSUM_rotpost(nObjID,nPropID,&fTimberVolume,&fTimberValue,&fTimberCost,&fTimberRotnetto);
		m_pDB->getObjPropTraktSUM_grot(nObjID,nPropID,&fGrotVolume,&fGrotValue,&fGrotCost);
		fSUMCompensation = fLandValue + fPreCutValue + fStormDryValue + fRandTreesValue;

		// #4835 L�gg p� uppr�knings% (expropriation) om inte underh�ll, om underh�ll s� inget till�gg
		nObjTyp=_tstoi(pObj->getObjTypeOfInfring());
		if(nObjTyp != 2)
		{
		fExpropriationPerc=pObj->getObjRecalcBy();
			if(fExpropriationPerc>0.0)
			{
				fSUMExpropriation=fSUMCompensation * (fExpropriationPerc/100.0);
				
				if(nTypeOfNet != TYPEOFNET_SVENSKAKRAFTNAT)//Ej ha med p�slag p� virke om svenska kraftn�t �r vald som s�rskild ers�ttning #5505 20180212 J�
				{
					//fSUMCompensation =fSUMCompensation + fSUMExpropriation + ((fTimberRotnetto + fGrotValue) * (fExpropriationPerc/100.0));
					//HMS-113 Dra av grotkostnad fr�n grotv�rde innnan ber�kning av kompensation 20240213
					fSUMCompensation =fSUMCompensation + fSUMExpropriation + ((fTimberRotnetto + (fGrotValue-fGrotCost)) * (fExpropriationPerc/100.0));
				}
				else
					fSUMCompensation =fSUMCompensation + fSUMExpropriation;
			}
		}
		else
		{
		fExpropriationPerc=0.0;
		fSUMExpropriation=0.0;
		}

		fSumPercentOfPriceBase = fPriceBase *  fPercentOfPriceBase/100.0;	// #4420 20150804 J� H�mta procent av prisbasbelopp som nu ligger i inst�llningar f�r frivillig uppg�relse p� objektet.
		// Max compansation
		fMaxCompensation = fPriceBase * (fMaxPercent/100.0);
		//---------------------------------------------------------------------------------------
		// Always display this information; 080519 p�d

		// Show sum of "Intr�ng"; 080519 p�d
		nIndex=1;
		dc->TextOut(10, nIndex*tm.tmHeight+30,(m_xml.str(IDS_STRING5214)));nIndex++;
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52140))));nIndex++;
		nLen1_0 = m_xml.str(IDS_STRING52140).GetLength();
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52141))));nIndex++;
		nLen1_1 = m_xml.str(IDS_STRING52141).GetLength();
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52142))));nIndex++;
		nLen1_2 = m_xml.str(IDS_STRING52142).GetLength();
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52143))));nIndex++;
		nLen1_3 = m_xml.str(IDS_STRING52143).GetLength();
		if(fExpropriationPerc>0.0 ) 
		{
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52145))));nIndex++;
			if(nTypeOfNet != TYPEOFNET_SVENSKAKRAFTNAT)//Ej ha med p�slag p� virke om svenska kraftn�t �r vald som s�rskild ers�ttning #5505 20180212 J�
			{
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%.0f %s"),fExpropriationPerc,(m_xml.str(IDS_STRING52146))));nIndex++;
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%.0f %s"),fExpropriationPerc,(m_xml.str(IDS_STRING52147))));nIndex++;
			}
			nLen1_4 = m_xml.str(IDS_STRING52145).GetLength();
		}
		else
			nLen1_4=0;

		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52144))));nIndex++;
		nLen1_5 = m_xml.str(IDS_STRING52144).GetLength();

		nLen = max(nLen1_0,nLen1_1);
		nLen = max(nLen,nLen1_3);
		nLen = max(nLen,nLen1_4);
		nLen = max(nLen,nLen1_5);

		dc->SelectObject(&m_fnt2);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
		nIndex=2;
		dc->DrawText(formatData(_T("%.0f"),fLandValue),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fPreCutValue),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fStormDryValue),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fRandTreesValue),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth, (nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;

		if(fExpropriationPerc>0.0)
		{
			dc->DrawText(formatData(_T("%.0f"),fSUMExpropriation),CRect(10+nLen*tm.tmAveCharWidth, nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth, (nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
			if(nTypeOfNet != TYPEOFNET_SVENSKAKRAFTNAT)//Ej ha med p�slag p� virke om svenska kraftn�t �r vald som s�rskild ers�ttning #5505 20180212 J�
			{
			dc->DrawText(formatData(_T("%.0f"),fTimberRotnetto * (fExpropriationPerc/100.0)),CRect(10+nLen*tm.tmAveCharWidth, nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth, (nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
			//HMS-113 Dra av grotkostnad fr�n grotv�rde innnan ber�kning av kompensation 20240213
			dc->DrawText(formatData(_T("%.0f"),(fGrotValue-fGrotCost) * (fExpropriationPerc/100.0)),CRect(10+nLen*tm.tmAveCharWidth, nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth, (nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
			}
		}
		
		dc->SetTextColor(BLUE);
		dc->DrawText(formatData(_T("%.0f"),fSUMCompensation),CRect(10+nLen*tm.tmAveCharWidth, nIndex*tm.tmHeight+30,nLenPlus+nLen*tm.tmAveCharWidth, (nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->SetTextColor(BLACK);
		dc->MoveTo(10,nIndex*tm.tmHeight+35);
		dc->LineTo(clip.right-10,nIndex*tm.tmHeight+35);nIndex++;
		dc->SelectObject(&m_fnt1);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

		// "Show Prisbasbelopp"; 080519 p�d
		dc->TextOut(10, nIndex*tm.tmHeight+30,(m_xml.str(IDS_STRING5215)));nIndex++;
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52150))));nIndex++;
		nLen2_0 = m_xml.str(IDS_STRING52150).GetLength();
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52151))));nIndex++;
		nLen2_1 = m_xml.str(IDS_STRING52151).GetLength();
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52152))));nIndex++;
		nLen2_2 = m_xml.str(IDS_STRING52152).GetLength();

		if(nTypeOfNet == TYPEOFNET_SWEDISHENERGY_2019)//#HMS-75 20220208 svensk energi 2019 alltid grunders�ttning 5%
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s%.f%s"),(m_xml.str(IDS_STRING52189)),fPercentOfPriceBase,(m_xml.str(IDS_STRING52190))));
		else
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%.f%s"),fPercentOfPriceBase,(m_xml.str(IDS_STRING52153))));
		nIndex++;
		nLen2_3 = m_xml.str(IDS_STRING52153).GetLength();
		if(nTypeOfNet == TYPEOFNET_SVENSKAKRAFTNAT)// 
		{
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52155))));
			nIndex++;
		}
		else
		{
		dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(_T("%s"),(m_xml.str(IDS_STRING52154))));
		nIndex++;
		}
		nLen2_4 = m_xml.str(IDS_STRING52154).GetLength();

		nLen = max(nLen2_0,nLen2_1);
		nLen = max(nLen,nLen2_2);
		nLen = max(nLen,nLen2_3);
		nLen = max(nLen,nLen2_4);

		dc->SelectObject(&m_fnt2);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
		nIndex=nIndex-5;
		dc->DrawText(formatData(_T("%.0f"),fPriceBase),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+10+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fMaxPercent),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+10+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fPercent),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+10+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fSumPercentOfPriceBase),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+10+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->DrawText(formatData(_T("%.0f"),fMaxCompensation),CRect(10+nLen*tm.tmAveCharWidth,nIndex*tm.tmHeight+30,nLenPlus+10+nLen*tm.tmAveCharWidth,(nIndex+1)*tm.tmHeight+30),DT_RIGHT|DT_SINGLELINE);nIndex++;
		dc->MoveTo(10,nIndex*tm.tmHeight+35);
		dc->LineTo(clip.right-10,nIndex*tm.tmHeight+35);nIndex++;
		dc->SelectObject(&m_fnt1);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
		dc->TextOut(10, nIndex*tm.tmHeight+30,(m_xml.str(IDS_STRING5216)));nIndex++;

		m_pDB->getNumOfEvaluations(nObjID, nPropID, &nNumOfEvals);

		if (!pProp->getPropNumOfStands() && !nNumOfEvals)
		{
			fSumPercentOfPriceBase = 0;
			fActualCompensation = 0;
			fMaxCompensation = 0;
			fSUMCompensation = 0;
		}

		if (nTypeOfNet == TYPEOFNET_LOCAL)			{ sPolicy = m_xml.str(IDS_STRING2120); }
		else if (nTypeOfNet == TYPEOFNET_SVENSKAKRAFTNAT)	{ sPolicy = m_xml.str(IDS_STRING2121); }
		else if (nTypeOfNet == TYPEOFNET_SWEDISHENERGY) { sPolicy = m_xml.str(IDS_STRING2122); }
		else										{ sPolicy = m_xml.str(IDS_STRING2123); }	// Svensk energi 2019 HMS-75 20220208 J�
		dc->SelectObject(&m_fnt1);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
		dc->TextOut(10,10,(m_xml.str(IDS_STRING5211)));
		dc->SelectObject(&m_fnt2);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
		dc->TextOut(10+tm.tmAveCharWidth*m_xml.str(IDS_STRING5211).GetLength(),10,sPolicy);
		dc->SelectObject(&m_fnt1);
		dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

		//---------------------------------------------------------------------------------------
		// Check which type of net we're dealing with; 080515 p�d
		if (nTypeOfNet == TYPEOFNET_LOCAL)	// "Lokalt n�t"
		{
			sSumVolDeal = m_xml.str(IDS_STRING52161) + sVolDeal + _T(" ") + m_xml.str(IDS_STRING3038) + _T(":");

			// Compansation'll only be 3 percent of Pricebase; 080519 p�d
			if (fSUMCompensation < fSumPercentOfPriceBase)
			{
				dc->TextOut(10, nIndex*tm.tmHeight+30,(m_xml.str(IDS_STRING52130)));nIndex++;
				dc->TextOut(10, nIndex*tm.tmHeight+30,formatData((m_xml.str(IDS_STRING52131)),fPercentOfPriceBase,fPercentOfPriceBase));nIndex+=2;

				dc->SelectObject(&m_fnt2);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
				dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f"),sSumVolDeal,fDbVolDealValue/*f3Percent*/));			
				dc->SelectObject(&m_fnt1);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
			}
			else if (fSUMCompensation >= fSumPercentOfPriceBase)
			{
				dc->TextOut(10, nIndex*tm.tmHeight+30,formatData((m_xml.str(IDS_STRING52132)),fPercentOfPriceBase,fPercent));nIndex++;
				dc->TextOut(10, nIndex*tm.tmHeight+30,formatData((m_xml.str(IDS_STRING52133)),fPercentOfPriceBase,fPercentOfPriceBase));

				// calculate the actual cxompensation; 080515 p�d
				fActualCompensation = fSUMCompensation * fPercent/100.0;
				// Compensation can't be > Max compensation; 080515 p�d
				if (fActualCompensation > fMaxCompensation)
				{
					nIndex+=2;
					dc->TextOut(10,nIndex*tm.tmHeight+30,(m_xml.str(IDS_STRING52160)));nIndex++;
					dc->SelectObject(&m_fnt2);
					dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
					dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f"),sSumVolDeal,fDbVolDealValue/*fMaxCompensation*/));			
					dc->SelectObject(&m_fnt1);
					dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
				}
				else
				{
					nIndex+=2;
					dc->SelectObject(&m_fnt2);
					dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
					// Need to check that compensation > f3Percent; 080519 p�d	
					if (fActualCompensation > fSumPercentOfPriceBase)
					{
						// Show calcultaion of compensation; 080519 p�d
						if (int(fActualCompensation) == int(fDbVolDealValue))
						{
							dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f*%.0f%s = %.0f"),
																												 sSumVolDeal,
																												 fSUMCompensation,fPercent,"%",fActualCompensation));			
						}
						else
						{
							dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f"),sSumVolDeal,fDbVolDealValue));			
						}
					}
					else
					{
						// Show calcultaion of compensation; 080519 p�d
						dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f"),sSumVolDeal,fDbVolDealValue/*f3Percent*/));			
					}
					dc->SelectObject(&m_fnt1);
					dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
				}
			}
		}
		else if (nTypeOfNet == TYPEOFNET_SVENSKAKRAFTNAT)	// "Regionalt n�t"   Svenska kraftn�t
		{
	/*	Vid frivilligt godk�nnande av ers�ttningserbjudande utg�r
		� dels ett grundbelopp motsvarande 10% av prisbasbeloppet, vid v�rdetidpunkten,
		� dels 20% av intr�ngsers�ttningen (best�ende skada + 25% p�slaget enligt expr.lagen p� best�ende skada), dock max 20% av prisbasbeloppet. OBS! Virket (25% enl. expr.lagen) ska inte vara med vid ber�kning av den frivilliga ers�ttningen.

		Exempel 1
		F�ruts�ttningar :
		Intr�ngsers�ttning (best�ende skada) 10.000:-
		25% enligt expr.lagen p� best�ende skada 2.500:-

		Frivillig ers�ttning n�r 20% av intr�ngsers�ttningen �r max 20% av prisbasbeloppet:
		10% av prisbasbeloppet (45.500:- * 10%) = 4.550:-
		20% av intr�ngsers�ttningen (10.000:- + 2.500) * 20% = 2.500:-
		Summa frivillig ers�ttning = 7.050:-

		Exempel 2
		F�ruts�ttningar :
		Intr�ngsers�ttning (best�ende skada) 100.000:-
		25% enligt expr.lagen p� best�ende skada 25.000:-

		Frivillig ers�ttning n�r 20% av intr�ngsers�ttningen �verstiger 20% av prisbasbeloppet:
		10% av prisbasbeloppet (45.500:- * 10%) = 4.550:-
		20% av intr�ngsers�ttningen (100.000:- + 25.000) * 20% max 20% av prisbasbeloppet = 9.100:-
		Summa Max frivillig ers�ttning 2018. = 13.650:-   */
			double fSvenskaKraftnatIntrangsErsattning=fSUMCompensation * fPercent/100.0;
			double fSvenskaKraftnatGrundErsattning=fSumPercentOfPriceBase;
			double fSvenskaKraftnatTotalErsattning=0.0;
			sSumVolDeal = sVolDeal + _T(" ") + m_xml.str(IDS_STRING3038) + _T(":");

			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData((m_xml.str(IDS_STRING52134)),fPercentOfPriceBase,fPercent));nIndex++;
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData((m_xml.str(IDS_STRING52135)),fPercent,fMaxPercent));nIndex++;
			dc->TextOut(10, nIndex*tm.tmHeight+30,formatData((m_xml.str(IDS_STRING52136)),fPercentOfPriceBase,fMaxPercent));
			// calculate the actual cxompensation; 080515 p�d
			//fActualCompensation = fSumPercentOfPriceBase + fSUMCompensation * fPercent/100.0;

			// Compensation can't be > Max compensation; 080515 p�d
			//if (fActualCompensation > fMaxCompensation)
			
			if(fSvenskaKraftnatIntrangsErsattning<=fMaxCompensation) // Om Intr�ngsers�ttningen �r mindre �n max ers�ttning utg�r Intr�ngsers�ttningen samt Grunders�ttning(procent av prisbasbelopp)
			{
				fSvenskaKraftnatTotalErsattning=fSvenskaKraftnatGrundErsattning+fSvenskaKraftnatIntrangsErsattning;
				nIndex+=2;
				dc->SelectObject(&m_fnt2);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
					dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f + %.0f*%.0f%s = %.0f")
						,sSumVolDeal
						,fSumPercentOfPriceBase
						,fSUMCompensation
						,fPercent
						,"%"
						,fSvenskaKraftnatTotalErsattning));			
				dc->SelectObject(&m_fnt1);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			}
			else	// Om Intr�ngsers�ttningen �r st�rre �n max ers�ttning utg�r Grunders�ttning(procent av prisbasbelopp) samt Max ers�ttning
			{
				fSvenskaKraftnatTotalErsattning=fSvenskaKraftnatGrundErsattning+fMaxCompensation;
				nIndex+=2;
				// Show calcultaion of compensation; 080519 p�d
				dc->SelectObject(&m_fnt2);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
					dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f + %.0f = %.0f")
						,sSumVolDeal
						,fSumPercentOfPriceBase
						,fMaxCompensation
						,fSvenskaKraftnatTotalErsattning));			
	
				dc->SelectObject(&m_fnt1);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
			}
		}
		else if (nTypeOfNet == TYPEOFNET_SWEDISHENERGY)
		{
			sSumVolDeal = sVolDeal + _T(" ") + m_xml.str(IDS_STRING3038) + _T(":");

			if (fSUMCompensation * (fPercent / 100.0 + 1.0) < fSumPercentOfPriceBase)
			{				
				fDbVolDealValue = fSumPercentOfPriceBase - fSUMCompensation;				
				dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52180),fPercentOfPriceBase));nIndex++;
				dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52181),fPercentOfPriceBase));
			}
			else
			{
				if (fSUMCompensation * fPercent > fPriceBase * fMaxPercent)
				{
					fDbVolDealValue = fPriceBase * fMaxPercent / 100.0;
					dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52182),fMaxPercent));nIndex++;
					dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52183),fMaxPercent));
				}
				else
				{
					fDbVolDealValue = fSUMCompensation * fPercent / 100.0;
					dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52184),fPercent));nIndex++;
					dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52185),fMaxPercent));
				}
			}
			nIndex += 2;

			dc->SelectObject(&m_fnt2);
			dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f"),sSumVolDeal,fDbVolDealValue));
			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);
		}
		else if (nTypeOfNet == TYPEOFNET_SWEDISHENERGY_2019)	//HMS-75 20220208 S�rskild ers svensk energi 2019
		{
			sSumVolDeal = sVolDeal + _T(" ") + m_xml.str(IDS_STRING3038) + _T(":");
			fDbVolDealValue=0.0;
			//Grunders�ttning 
			fDbVolDealValue=fSumPercentOfPriceBase;
			
			if (fSUMCompensation * fPercent > fPriceBase * fMaxPercent)
			{
				fDbVolDealValue += fPriceBase * fMaxPercent / 100.0;
				dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52186),fMaxPercent));nIndex++;
				dc->TextOut(10, nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52187),fMaxPercent));
			}
			else
			{
				fDbVolDealValue += fSUMCompensation * fPercent / 100.0;
				dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(m_xml.str(IDS_STRING52188),fPercent));nIndex++;
			}
			
			nIndex += 2;

			dc->SelectObject(&m_fnt2);
			dc->TextOut(10,nIndex*tm.tmHeight+30,formatData(_T("%s %.0f"),sSumVolDeal,fDbVolDealValue));
			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);
		}

	}
	pObj = NULL;
	pProp = NULL;
}
