// UMLandValue.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "Resource.h"

#include "1950ForrestNormFrame.h"
#include "ObjectSelListFormView.h"
#include "P30TemplateFormView.h"
#include "HigherCostsFormView.h"
#include "DocumentTmplFormView.h"
#include "DocumentTmplListFormView.h"
#include "MDITemplateListFrameView.h"
#include "MDITemplate2ListFrameView.h"
#include "MDITemplate3ListFrameView.h"
#include "MDITemplate2018ListFrameView.h"
#include "PrlViewAndPrint.h"
#include "PropertyStatusFormView.h"
#include "ImportExcelDlg.h"
#include "GlobalSearch.h"
#include "SpeciesFormView.h"

//#include "PropertyRemoveSelListFormView.h"
#include "MDI1950ForrestNormFormView.h"

#include "CreateTables.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

static AFX_EXTENSION_MODULE UMLandValueDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

CUMLandValueDB *global_pDB = NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Languagefile Strings
StrMap global_langMap;

// Initialize the DLL, register the classes etc
//extern "C" AFX_EXT_API void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

// Added: EXPORTED function doDatabaseTables; 080131 p�d
extern "C" AFX_EXT_API void DoDatabaseTables(LPCTSTR);

extern "C" AFX_EXT_API void DoAlterTables(LPCTSTR);

extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	AfxInitRichEdit();

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMLandValue.DLL Initializing!\n");

		CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
		if (fileExists(sLangFN))
		{
			RLFReader xml;
			xml.LoadEx(sLangFN,global_langMap);
		}
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMLandValueDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMLandValue.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMLandValueDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMLandValueDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 0801 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	//#HMS-94 inst�llningar f�r P30 tr�dslag p� objekt kontra alla tr�dslag 20220921 J�
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW_P30SPEC_SET,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDISpeciesFrame),
			RUNTIME_CLASS(CSpeciesFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW_P30SPEC_SET,suite,sLangFN,TRUE));



	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDI1950ForrestNormFrame),
			RUNTIME_CLASS(CMDI1950ForrestNormFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	// Added 2008-04-02 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDIP30TemplateFrame),
			RUNTIME_CLASS(CP30TemplateFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW2,suite,sLangFN,TRUE));

	// Added 2009-04-15 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW11,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDIP30NewNormTemplateFrame),
			RUNTIME_CLASS(CP30NewNormTemplateFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW11,suite,sLangFN,TRUE));

	// Added 2008-04-02 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDIHigherCostsFrame),
			RUNTIME_CLASS(CHigherCostsFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,sLangFN,TRUE));

	// Added 2008-04-02 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDITemplateListFrame),
			RUNTIME_CLASS(CTemplateListFrameView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW4,suite,sLangFN,TRUE));

	// Added 2008-04-02 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDITemplate2ListFrame),
			RUNTIME_CLASS(CTemplate2ListFrameView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW5,suite,sLangFN,TRUE));

	// Added 2009-04-17 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW12,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDITemplate3ListFrame),
			RUNTIME_CLASS(CTemplate3ListFrameView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW12,suite,sLangFN,TRUE));

	// Added 2008-04-01 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW_SELLIST,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDI1950ForrestNormListFrame),
			RUNTIME_CLASS(CObjectSelListFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW_SELLIST,suite,sLangFN,TRUE));

	// Added 2008-06-10 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CDocumentTmplFrame),
			RUNTIME_CLASS(CDocumentTmplFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW8,suite,sLangFN,TRUE));

	// Added 2008-06-11 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW9,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CDocumentTmplListFrame),
			RUNTIME_CLASS(CDocumentTmplListFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW9,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW10,
			RUNTIME_CLASS(CMDIPrlViewAndPrintDoc),
			RUNTIME_CLASS(CMDIPrlViewAndPrintFrame),
			RUNTIME_CLASS(CPrlViewAndPrint)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW10,suite,sLangFN,TRUE));

	// Added 2009-09-14 p�d
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW13,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CPropertyStatusFrame),
			RUNTIME_CLASS(CPropertyStatusFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW13,suite,sLangFN,TRUE));

	// Added 2016-12-21 PH
	app->AddDocTemplate(new CMultiDocTemplate(IDD_GLOBALSEARCH,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIChildWnd),
			RUNTIME_CLASS(GlobalSearch)));
	idx.push_back(INDEX_TABLE(IDD_GLOBALSEARCH,suite,sLangFN,TRUE));


	// Added 2018-03-20 j�
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW14,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDIP30Norm2018TemplateFrame),
			RUNTIME_CLASS(CP30Norm2018TemplateFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW14,suite,sLangFN,TRUE));


	// Added 2018-03-20 j�
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW15,
			RUNTIME_CLASS(CMDIFrameDoc2),
			RUNTIME_CLASS(CMDITemplate2018ListFrame),
			RUNTIME_CLASS(CTemplate2018ListFrameView)));	
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW15,suite,sLangFN,TRUE));

	/*//Trycker upp en listview h�r f�r att kunna v�lja flera fastigheter att ta bort. Feature #2456 20111021 J�
	app->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW1,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CPropertyRemoveSelListFrame),
			RUNTIME_CLASS(CPropertyRemoveSelListFormView)));*/


	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	CImportExcelDlg::m_sVersion = sVersion;

	sVersion.ReleaseBuffer();
	sCopyright.ReleaseBuffer();
	sCompany.ReleaseBuffer();

	DoDatabaseTables(_T(""));	// Empty arg = use default database; 081001 p�d
	DoAlterTables(_T(""));	// Empty arg = use default database; 081001 p�d
}


void DoDatabaseTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	CString sLang(getLangSet());
	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{
		//OBS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Only for SQL Server database; 080630 p�d

		vec.push_back(Scripts(TBL_ELV_OBJECT,table_ElvObject,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP,table_ElvProperties,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP_LOG,table_ElvPropertiesLog,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP_OTHER_COMP,table_ElvPropertiesOtherComp,db_name));
		vec.push_back(Scripts(TBL_ELV_EVALUATION,table_ElvEvaluation,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,table_ElvCruise,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE_RANDTREES,table_ElvCruiseRandtrees,db_name));
		vec.push_back(Scripts(TBL_ELV_TEMPLATE,table_ElvTemplate,db_name));
		vec.push_back(Scripts(TBL_ELV_DOC_TEMPLATE,table_ElvDocumentTemplate,db_name));
		// Adding a table for 'manual' cruise, not used 20100504 p�d
		//vec.push_back(Scripts(TBL_ELV_MCRUISE,table_ElvMCruise,db_name));
		// Set Status depending on language; 091104 p�d
		if (sLang.CompareNoCase(_T("SVE")) == 0)
		{
			vec.push_back(Scripts(TBL_ELV_PROP_STATUS,table_ElvPropertyStatusSVE,db_name));
		}
		else
		{
			vec.push_back(Scripts(TBL_ELV_PROP_STATUS,table_ElvPropertyStatusENU,db_name));
		}


		runSQLScriptFileEx2(vec,Scripts::TBL_CREATE);
		vec.clear();

		// H�mta ut om vi ska logga objekts�ndring eller ej (#3877)
		bool bLogObject = regGetInt(REG_ROOT, REG_LANDVALUE_KEY, REG_LANDVALUE_LOG_OBJECT, false);
		if(bLogObject == true)
		{
			// skapa log-tabell i admin databasen (#3877)
			vec.push_back(Scripts(TBL_ELV_OBJECT_LOG, table_ElvObjectLog, ADMIN_DB_NAME));
			runSQLScriptFileEx2(vec, Scripts::TBL_CREATE);
			vec.clear();
		}
	}
}

void DoAlterTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	// Check if there's a connection set; 090525 p�d
	if (getIsDBConSet() == 1)
	{
		// Alter table scripts

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable_ParallelWidth,db_name));

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable_PerOfPriceBase_1,db_name)); // Procent av prisbasbelopp #4420 20150803 J�
		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable_PerOfPriceBase_2,db_name));

		vec.push_back(Scripts(TBL_ELV_PROP_LOG,alter_ElvPropertyLog,db_name));
		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable090625_1,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable090625_2,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable090904_1,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable090908_1,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP,alter_ElvPropertyTable090914_1,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP,alter_ElvPropertyTable091009_1,db_name));
		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable100225_1,db_name));
		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable100323_1,db_name));	// Added 100323 p�d
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable100323_1,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable100323_2,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP,alter_ElvPropertyTable100323_1,db_name));
		vec.push_back(Scripts(TBL_ELV_PROP,alter_ElvPropertyTable100323_2,db_name));
		
		vec.push_back(Scripts(TBL_ELV_PROP,alter_ElvPropertyTable100517_1,db_name));	// Added 100517 p�d
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable100517_1,db_name));	// Added 100517 p�d

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable100610_1,db_name));	// Added 100610 p�d

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable100615_1,db_name));	// Added 100615 p�d

		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable100702_1,db_name));	// Added 100702 p�d

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable120601_1,db_name));	// Added 120601 J� #3116

		vec.push_back(Scripts(TBL_ELV_OBJECT, alter_ElvObjectTable121011_1,db_name));	//#3385
		vec.push_back(Scripts(TBL_ELV_OBJECT, alter_ElvObjectTable121011_2,db_name));	//#3385

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable151210,db_name));		//#4689 Change image to varbinary(MAX)

		vec.push_back(Scripts(TBL_ELV_OBJECT,alter_ElvObjectTable20241210,db_name));	//HMS-119 ut�ka tecken p� object littra till 50

		vec.push_back(Scripts(TBL_ELV_PROP, alter_ElvPropertyTable120905_1, db_name));	// #3734 koordinat

		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20150112_1,db_name));	//#4207 L�ngd [m] 150112 J�
		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20150112_2,db_name));	//#4207 Bredd [m] 150112 J�		

		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20160817_1,db_name));	//#5086 Skikt 160817 PL		
		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20160817_2,db_name));	//#5086 Skikt 160817 PL		

		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20160616,db_name));	//#4829 �verlappand mark 160616 PL

		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20161005_1,db_name));	//#5171 Sida p� v�rdering 161005 PL
		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20161005_2,db_name));

		vec.push_back(Scripts(TBL_ELV_EVAL,alter_ElvEvalTable20190326,db_name));	//#HMS-41 Tillf�lligt utnyttjande 190326 J�


		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable190327,db_name));	//#HMS-41 Tillf�lligt utnyttjande 190327 J�


		//#HMS-49 Info om f�rtidig avverkning 191126 J�
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_01,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_02,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_03,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_04,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_05,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_06,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_07,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_08,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_09,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_10,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable191126_11,db_name));	

		
		//HMS-48 Info om markv�rde 20200427 J�
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_01,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_02,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_03,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_04,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_05,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_06,db_name));	
		vec.push_back(Scripts(TBL_ELV_EVALUATION,alter_ElvEvalutationTable200427_07,db_name));	

		

		
		//#HMS-50 Info om storm o tork 20200414 J�
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_1,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_2,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_3,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_4,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_5,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_6,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_7,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_8,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_9,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_10,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_11,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200414_12,db_name)); 

		//HMS-48 Info om markv�rde 20200427 J�
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_01,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_02,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_03,db_name)); 
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_04,db_name)); 		
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_05,db_name));
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_06,db_name)); 	
		vec.push_back(Scripts(TBL_ELV_CRUISE,alter_ElvCruiseTable20200427_07,db_name)); 	
		

		runSQLScriptFileEx2(vec,Scripts::TBL_ALTER);
		vec.clear();
	}
}
