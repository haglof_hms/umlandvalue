#pragma once
#include "AddP30NewNormFormView.h"
#include "AddP30Norm2018FormView.h"

#include "Resource.h"

// CP30TemplateFormView form view

class CP30TemplateFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CP30TemplateFormView)

	BOOL m_bInitialized;

	BOOL m_bDataEnabled;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgNoTemplateName;
	CString m_sMsgNoTemplateName2;
	CString m_sMsgRemoveTemplate;
	CString m_sMsgNoTemplatesRegistered;
	CString m_sMsgCharError;

	CXTResizeGroupBox m_wndGroupP30_1;
	CXTResizeGroupBox m_wndGroupP30_2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void addSpecieConstaints(void);
	vecTransactionSpecies m_vecSpecies;
	BOOL getSpeciesFromDB(void);

	vecTransactionTemplate m_vecTransactionTemplate;
	CTransaction_template m_recActiveTemplate;
	void getObjectTemplatesFromDB(void);

	int getSpcID(LPCTSTR);

	void newP30Template(void);
	void removeP30Template(void);

	int m_nDBIndex;
	void populateData(int);

	void setupPopulationOfTemplate(BOOL setup_dbindex);	// E.g. after save; 080402 p�d

	void setEnableData(BOOL enable);

	enum enumAction { TEMPLATE_NONE,
										TEMPLATE_NEW,
										TEMPLATE_OPEN } m_enumAction;
protected:
	CP30TemplateFormView();           // protected constructor used by dynamic creation
	virtual ~CP30TemplateFormView();

	CMyReportCtrl m_wndReport;
	BOOL setupReport(void);
public:
	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	// Public, so we can run it from MDIChildWnd (CMDIP30TemplateFrame::OnClose(void)); 080404 p�d
	BOOL saveP30Template(short action);

	void doPopulateData(int idx)
	{
		m_nDBIndex = idx;
		populateData(m_nDBIndex);
	}

	int getDBIndex(void)
	{
		return m_nDBIndex;
	}

	int getNumOfTemplateItems(void)
	{
		return (int)m_vecTransactionTemplate.size();
	}

	void addSpecieToReport(void);
	void removeSelectedSpecie(void);
	void importP30Table(void);
	void exportP30Table(void);

	void doSetNavigationBar();

protected:
	//{{AFX_VIRTUAL(CP30TemplateFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDI1950ForrestNormFrame)
	afx_msg void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnEnChangeEdit1();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


//*****************************************************************************************************
// CP30NewNormTemplateFormView form view
//*****************************************************************************************************

class CP30NewNormTemplateFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CP30NewNormTemplateFormView)

	BOOL m_bInitialized;

	BOOL m_bDataEnabled;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgNoTemplateName;
	CString m_sMsgNoTemplateName2;
	CString m_sMsgRemoveTemplate;
	CString m_sMsgNameUsedTemplate;
	CString m_sMsgChangeGArea;
	CString m_sLbl5Msg;
	CString m_sMsgCharError;
	CString m_sMsgPRelError1;
	CString m_sMsgPRelError2;

	CXTResizeGroupBox m_wndGroup2_1;
	CXTResizeGroupBox m_wndGroup2_2;
	
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	
	CMyComboBox m_wndCBox1;

	CMyTabControl m_wndTabControl;

	CButton m_wndAdd;
	CButton m_wndRemove;

	CStringArray m_sarrAreas;

	vecTransactionSpecies m_vecSpecies;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CXTPTabManagerItem *m_tabManager;

	vecTransactionTemplate m_vecTransactionTemplate;
	CTransaction_template m_recActiveTemplate;
	void getObjectTemplatesFromDB(void);

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void setEnableData(BOOL enable);

	int m_nAreaIndex;

	enum enumAction { TEMPLATE_NONE,
										TEMPLATE_NEW,
										TEMPLATE_OPEN } m_enumAction;

protected:
	CP30NewNormTemplateFormView();           // protected constructor used by dynamic creation
	virtual ~CP30NewNormTemplateFormView();

	CString getSpcNameFromID(int id);

	int m_nDBIndex;
	void populateData(int);

	void newP30NewNormTemplate(void);
	void removeP30NewNormTemplate(void);

	BOOL createXMLFile(short *ret_value);

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id);

public:
	enum { IDD = IDD_FORMVIEW11 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	BOOL saveP30NewNormTemplate(short action);

	void doPopulateData(int idx)
	{
		m_nDBIndex = idx;
		populateData(m_nDBIndex);
	}

	int getDBIndex(void)	{	return m_nDBIndex;	}

	int getNumOfTemplateItems(void)	{	return (int)m_vecTransactionTemplate.size();	}

	CAddP30NewNormFormView *getSelectedTabFormView();
	CAddP30NewNormFormView *getTabFormView(int tab_index,int *data);

	void importP30Table(void);
	void exportP30Table(void);

	void doSetNavigationBar();

protected:
	//{{AFX_VIRTUAL(CP30NewNormTemplateFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CP30NewNormTemplateFormView)
	afx_msg void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLCtrlClick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton21();
	afx_msg void OnBnClickedButton22();
	afx_msg void OnCbnSelchangeCombo21();
	afx_msg void OnEnChangeEdit1();
};



//*****************************************************************************************************
// CP30Norm2018TemplateFormView form view
//*****************************************************************************************************

class CP30Norm2018TemplateFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CP30Norm2018TemplateFormView)

	BOOL m_bInitialized;

	BOOL m_bDataEnabled;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgNoTemplateName;
	CString m_sMsgNoTemplateName2;
	CString m_sMsgRemoveTemplate;
	CString m_sMsgNameUsedTemplate;
	CString m_sMsgChangeGArea;
	CString m_sLbl5Msg;
	CString m_sMsgCharError;
	CString m_sMsgPRelError1;
	CString m_sMsgPRelError2;

	CXTResizeGroupBox m_wndGroup14_1;
	CXTResizeGroupBox m_wndGroup14_2;
	
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	
	CMyComboBox m_wndCBox1;

	CMyTabControl m_wndTabControl;

	CButton m_wndAdd;
	CButton m_wndRemove;

	CStringArray m_sarrAreas;

	vecTransactionSpecies m_vecSpecies;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CXTPTabManagerItem *m_tabManager;

	vecTransactionTemplate m_vecTransactionTemplate;
	CTransaction_template m_recActiveTemplate;
	void getObjectTemplatesFromDB(void);

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void setEnableData(BOOL enable);

	int m_nAreaIndex;

	enum enumAction { TEMPLATE_NONE,
										TEMPLATE_NEW,
										TEMPLATE_OPEN } m_enumAction;

protected:
	CP30Norm2018TemplateFormView();           // protected constructor used by dynamic creation
	virtual ~CP30Norm2018TemplateFormView();

	CString getSpcNameFromID(int id);

	int m_nDBIndex;
	void populateData(int);

	void newP30Norm2018Template(void);
	void removeP30Norm2018Template(void);

	BOOL createXMLFile(short *ret_value);

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id);

public:
	enum { IDD = IDD_FORMVIEW14 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	BOOL saveP30Norm2018Template(short action);

	void doPopulateData(int idx)
	{
		m_nDBIndex = idx;
		populateData(m_nDBIndex);
	}

	int getDBIndex(void)	{	return m_nDBIndex;	}

	int getNumOfTemplateItems(void)	{	return (int)m_vecTransactionTemplate.size();	}

	CAddP30Norm2018FormView *getSelectedTabFormView();
	CAddP30Norm2018FormView *getTabFormView(int tab_index,int *data);

	void importP30Table(void);
	void exportP30Table(void);

	void doSetNavigationBar();

protected:
	//{{AFX_VIRTUAL(CP30NewNormTemplateFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CP30NewNormTemplateFormView)
	afx_msg void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLCtrlClick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton141();
	afx_msg void OnBnClickedButton142();
	afx_msg void OnCbnSelchangeCombo141();
	afx_msg void OnEnChangeEdit141();
};
