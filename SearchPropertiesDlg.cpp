// SearchPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SearchPropertiesDlg.h"

#include "ResLangFileReader.h"

// CSearchPropertiesDlg dialog

IMPLEMENT_DYNAMIC(CSearchPropertiesDlg, CDialog)

BEGIN_MESSAGE_MAP(CSearchPropertiesDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_SEARCH, &CSearchPropertiesDlg::OnBnClickedBtnSearch)
	ON_BN_CLICKED(IDC_BTN_CLEAR, &CSearchPropertiesDlg::OnBnClickedBtnClear)
	ON_BN_CLICKED(IDOK, &CSearchPropertiesDlg::OnBnClickedBtnOK)
	ON_BN_CLICKED(IDC_BUTTON1, &CSearchPropertiesDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSearchPropertiesDlg::OnBnClickedButton2)
	ON_NOTIFY(NM_CLICK, ID_REPORT_SEARCH_PROPS, OnReportItemClick)
END_MESSAGE_MAP()

CSearchPropertiesDlg::CSearchPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchPropertiesDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CSearchPropertiesDlg::~CSearchPropertiesDlg()
{
	m_vecSelectedProperties.clear();
	m_vecTransactionProperty.clear();
	m_ilIcons.DeleteImageList();
}

void CSearchPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl7);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT20, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);

	DDX_Control(pDX, IDC_BTN_SEARCH, m_wndSearchBtn);
	DDX_Control(pDX, IDC_BTN_CLEAR, m_wndClearBtn);
	DDX_Control(pDX, IDC_BUTTON1, m_wndCheckAll);
	DDX_Control(pDX, IDC_BUTTON2, m_wndUncheckAll);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP
}

BOOL CSearchPropertiesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING2310)));

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING2311)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING2312)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING2313)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING2314)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING2315)));
			m_wndLbl6.SetWindowText((xml->str(IDS_STRING2316)));
			m_wndLbl7.SetWindowText((xml->str(IDS_STRING2317)));

			m_wndSearchBtn.SetWindowText((xml->str(IDS_STRING2318)));
			m_wndClearBtn.SetWindowText((xml->str(IDS_STRING2319)));
			m_wndCheckAll.SetWindowText((xml->str(IDS_STRING2323)));
			m_wndUncheckAll.SetWindowText((xml->str(IDS_STRING2324)));
			CString sOKBtnText;
			sOKBtnText.Format(_T("%s/%s"),
									(xml->str(IDS_STRING2320)),
									(xml->str(IDS_STRING2322)));
			m_wndOKBtn.SetWindowText(sOKBtnText);
			m_wndCancelBtn.SetWindowText((xml->str(IDS_STRING2321)));
		}
		delete xml;
	}

	setupReport();

	populateData();

	m_wndOKBtn.EnableWindow(FALSE);

	return TRUE;
}

void CSearchPropertiesDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CXTPReportRows *pRows = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == 0)
		{
			if (isTableSelected()) return;
		}	// if (pItemNotify->pColumn->GetItemIndex() == 0)
	}
}


BOOL CSearchPropertiesDlg::isTableSelected(void)
{
	CXTPReportRows *pRows = NULL;
	CSelectedPropertiesDataRec *pRec = NULL;
	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CSelectedPropertiesDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getColumnCheck(0))
				{
					// Condition for TRUE is that there's selctions in From and To Database; 081008 p�d
					m_wndOKBtn.EnableWindow(TRUE);
					return TRUE;
				}	// if (pRec->getColumnCheck())
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
	m_wndOKBtn.EnableWindow(FALSE);

	return FALSE;
}

// PRIVATE
void CSearchPropertiesDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_SEARCH_PROPS, TRUE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
		m_ilIcons.Add(&bmp, RGB(255, 0, 255));

		m_wndReport.SetImageList(&m_ilIcons);

		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 20,FALSE,12));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING2313)), 80));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING2311)), 150));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING2312)), 120));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING3001)), 120));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING3000)), 200));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING2316)), 50));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING2317)), 50));
				pCol->SetEditable( FALSE );

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_REPORT_SEARCH_PROPS),5,70,rect.right - 10,rect.bottom - 100);
			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}
}

void CSearchPropertiesDlg::populateData(void)
{
	CString sSQL;

	// Clear report for next question
	m_wndReport.ResetContent();
	// Do collecting data from Database; 080122 p�d
	if (m_pDB != NULL)
	{
		sSQL.Format(_T("SELECT * FROM %s AS p\
						WHERE NOT EXISTS(SELECT null\
						FROM %s AS o\
						WHERE p.id=o.prop_id)"),TBL_PROPERTY,TBL_ELV_PROP);	// Select all properties not connected to object; 160609 ph
		m_pDB->getProperties(sSQL,m_vecTransactionProperty);

		if (m_vecTransactionProperty.size() > 0)
		{
			// Setup data in report; 080122 p�d
			for (UINT ii = 0;ii < m_vecTransactionProperty.size();ii++)
			{
				m_wndReport.AddRecord(new CSelectedPropertiesDataRec(ii,m_vecTransactionProperty[ii]));
			}
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CSearchPropertiesDlg::setupSQLFromSelections(void)
{
	CString sSQL;
	CString sSearchCrit = L"";

	//#5007 PH 20160615
	sSQL.Format(_T("SELECT * FROM %s AS p\
					WHERE NOT EXISTS(SELECT null\
					FROM %s AS o\
					WHERE p.id=o.prop_id) "),TBL_PROPERTY,TBL_ELV_PROP);	// Select all properties not connected to object; 160609 ph

	// Add search criteria
	if (!m_wndEdit3.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.obj_id LIKE \'%s%s%s\' "),_T("%"),m_wndEdit3.getText(),_T("%"));
		sSQL += sSearchCrit;
	}
	if (!m_wndEdit1.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.county_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit1.getText(),_T("%"));
		sSQL += sSearchCrit;
	}
	if (!m_wndEdit2.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.municipal_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit2.getText(),_T("%"));
		sSQL += sSearchCrit;
	}
	if (!m_wndEdit4.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.prop_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit4.getText(),_T("%"));
		sSQL += sSearchCrit;
	}
	if (!m_wndEdit5.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.prop_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit5.getText(),_T("%"));
		sSQL += sSearchCrit;
	}
	if (!m_wndEdit6.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.block_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit6.getText(),_T("%"));
		sSQL += sSearchCrit;
	}
	if (!m_wndEdit7.getText().IsEmpty())
	{
		sSearchCrit.Format(_T("AND p.unit_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit7.getText(),_T("%"));
		sSQL += sSearchCrit;
	}

	// Clear report for next question
	m_wndReport.ResetContent();
	// Do collecting data from Database; 080122 p�d
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(sSQL,m_vecTransactionProperty);

		if (m_vecTransactionProperty.size() > 0)
		{
			// Setup data in report; 080122 p�d
			for (UINT ii = 0;ii < m_vecTransactionProperty.size();ii++)
			{
				m_wndReport.AddRecord(new CSelectedPropertiesDataRec(ii,m_vecTransactionProperty[ii]));
			}
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CSearchPropertiesDlg::OnBnClickedBtnSearch()
{
	setupSQLFromSelections();
}

void CSearchPropertiesDlg::OnBnClickedBtnClear()
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));

	populateData();

	m_wndEdit1.SetFocus();
}

void CSearchPropertiesDlg::OnBnClickedBtnOK()
{
	BOOL bChecked;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	m_vecSelectedProperties.clear();
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CSelectedPropertiesDataRec *pRec = (CSelectedPropertiesDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				bChecked = pRec->getColumnCheck(COLUMN_0);	// First column
				if (bChecked)
				{
					m_vecSelectedProperties.push_back(pRec->getRecord());
				}
			}
		}
		OnOK();
	}
}

void CSearchPropertiesDlg::OnBnClickedButton1()
{
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CSelectedPropertiesDataRec *pRec = (CSelectedPropertiesDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				pRec->setColumnCheck(COLUMN_0,TRUE);
			}
		}	// for (int i = 0;i < pRecs->GetCount();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
		m_wndOKBtn.EnableWindow(TRUE);
	}	// if (pRecs != NULL)
}

void CSearchPropertiesDlg::OnBnClickedButton2()
{
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CSelectedPropertiesDataRec *pRec = (CSelectedPropertiesDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				pRec->setColumnCheck(COLUMN_0,FALSE);
			}
		}	// for (int i = 0;i < pRecs->GetCount();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
		m_wndOKBtn.EnableWindow(FALSE);
	}	// if (pRecs != NULL)
}
