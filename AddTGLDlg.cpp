// AddTGLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AddTGLDlg.h"

#include "ResLangFileReader.h"

// CAddTGLDlg dialog

IMPLEMENT_DYNAMIC(CAddTGLDlg, CDialog)

BEGIN_MESSAGE_MAP(CAddTGLDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON21_0, &CAddTGLDlg::OnBnClickedButton210)
	ON_BN_CLICKED(IDC_BUTTON21_1, &CAddTGLDlg::OnBnClickedSparaVardering)
END_MESSAGE_MAP()

CAddTGLDlg::CAddTGLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddTGLDlg::IDD, pParent)
{
	m_bCreateVStand = FALSE;
}

CAddTGLDlg::~CAddTGLDlg()
{
}

void CAddTGLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)

	DDX_Control(pDX, IDC_LBL21_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL21_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL21_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL21_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL21_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL21_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL21_7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL21_8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL21_9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL21_10, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL21_11, m_wndLbl11);

	DDX_Control(pDX, IDC_LBL21_12, m_wndLbl12);	
	DDX_Control(pDX, IDC_CHECK21_1, m_wndCheckBox);

	DDX_Control(pDX, IDC_STATIC_TSLBL, m_wndLblTslbl);

	DDX_Control(pDX, IDC_EDIT21_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT21_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT21_3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT21_4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT21_5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT21_6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT21_7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT21_8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT21_9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT21_10, m_wndEdit10);

	DDX_Control(pDX, IDC_BUTTON21_0, m_wndOKBtn);
	DDX_Control(pDX, IDC_BUTTON21_2, m_wndOKBtn2);
	DDX_Control(pDX, IDCANCEL, m_wndCancel);
	//}}AFX_DATA_MAP
}

BOOL CAddTGLDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING229);
			m_sMsgSumError = xml.str(IDS_STRING4074);
			m_sMsgSIError  = xml.str(IDS_STRING4075);

			SetWindowText(xml.str(IDS_STRING4070));
			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING40707));
			m_wndOKBtn2.SetWindowText(xml.str(IDS_STRING40708));
			m_wndCancel.SetWindowText(xml.str(IDS_STRING22696));

			m_wndLblTslbl.SetWindowText(xml.str(IDS_STRING40709));
			m_wndLbl1.SetWindowText(xml.str(IDS_STRING4071));
			m_wndLbl2.SetWindowText(xml.str(IDS_STRING4072));
			m_wndLbl3.SetWindowText(xml.str(IDS_STRING4073));
			m_wndLbl4.SetWindowText(xml.str(IDS_STRING40700));
			m_wndLbl5.SetWindowText(xml.str(IDS_STRING40701));
			m_wndLbl6.SetWindowText(xml.str(IDS_STRING40702));
			m_wndLbl7.SetWindowText(xml.str(IDS_STRING40703));
			m_wndLbl8.SetWindowText(xml.str(IDS_STRING40704));
			m_wndLbl9.SetWindowText(xml.str(IDS_STRING40705));
			m_wndLbl10.SetWindowText(xml.str(IDS_STRING40706));
			m_wndLbl11.SetWindowText(xml.str(IDS_STRING40710));

			m_wndLbl12.SetWindowText(xml.str(IDS_STRING42053));
			m_wndCheckBox.SetWindowTextW(xml.str(IDS_STRING42054));

			xml.clean();
		}
	}

	m_wndEdit1.SetRange(0,100);
	m_wndEdit2.SetRange(0,100);
	m_wndEdit3.SetRange(0,100);

	m_wndEdit1.setInt(m_nPine);
	m_wndEdit2.setInt(m_nSpruce);
	m_wndEdit3.setInt(m_nBirch);
	m_wndEdit6.SetEditMask(L">99",L"___");
	m_wndEdit7.SetAsNumeric();
	m_wndEdit8.SetAsNumeric();
	m_wndEdit4.SetWindowTextW(m_sStandNum);
	m_wndEdit5.SetWindowTextW(m_sStandName);
	m_wndEdit6.SetWindowTextW(m_sStandSI);
	m_wndEdit7.setFloat(m_fAreal,3);
	m_wndEdit8.setFloat(m_fVolym,2);
	m_wndEdit9.setInt(m_nAge);
	m_wndEdit10.setInt(m_nSide);
	m_wndEdit10.SetAsNumeric();
	m_wndEdit10.SetRange(1,2);

	m_wndCheckBox.SetCheck(m_bTillfUtnyttj);

	return TRUE;
}


// CAddTGLDlg message handlers

void CAddTGLDlg::setTGL(LPCTSTR tgl)
{
/*
	CString sTGL(tgl),sToken;
	CStringArray sarrTokens;
	int nCurPos;

	m_nPine = m_nSpruce = m_nBirch = nCurPos = 0;
	if (!sTGL.IsEmpty())
	{
		if (sTGL.Left(sTGL.GetLength()) != _T(";")) sTGL += _T(";");
		sToken = sTGL.Tokenize(_T(";"),nCurPos);
		while (!sToken.IsEmpty())
		{
			sarrTokens.Add(sToken);
			sToken = sTGL.Tokenize(_T(";"),nCurPos);
		}
		if (sarrTokens.GetCount() == 3)
		{
			m_nPine = atoi(sarrTokens.GetAt(0));
			m_nSpruce = atoi(sarrTokens.GetAt(1));
			m_nBirch = atoi(sarrTokens.GetAt(2));
		}
	}
*/
	// Get TGL
	parseTGL(tgl,&m_nPine,&m_nSpruce,&m_nBirch);
}


void CAddTGLDlg::setTillfUtnyttj(BOOL tillf)
{  
	m_bTillfUtnyttj=tillf;
	if(m_bTillfUtnyttj)
		m_wndCheckBox.SetCheck(true);
}


void CAddTGLDlg::OnBnClickedButton210()
{
	int m_nOldNorm=0;
	CTransaction_elv_object *pObj = getActiveObject();
	// Check that Sum of spcie mix = 100%
	m_nPine = m_wndEdit1.getInt();
	m_nSpruce = m_wndEdit2.getInt();
	m_nBirch = m_wndEdit3.getInt();

	m_sStandNum = m_wndEdit4.getText();
	m_sStandName = m_wndEdit5.getText();
	m_wndEdit6.GetWindowTextW(m_sStandSI);
	m_fAreal = m_wndEdit7.getFloat();
	m_fVolym = m_wndEdit8.getFloat();
	m_nAge = m_wndEdit9.getInt();
	m_nSide = m_wndEdit10.getText().IsEmpty() ? 1 : m_wndEdit10.getInt();

	if(m_wndCheckBox.GetCheck())
		m_bTillfUtnyttj=TRUE;
	else
		m_bTillfUtnyttj=FALSE;

	if(pObj!=NULL)
	{
		if (pObj->getObjP30TypeOf() == TEMPLATE_P30)
			m_nOldNorm=1;
		else
			m_nOldNorm=0;
	}

	if ((m_nPine+m_nSpruce+m_nBirch) == 100)
	{
		//Check SI #3592 J� 20130318		
		if (m_sStandSI.FindOneOf(_T("TGBFCE")) > -1 || m_sStandSI.IsEmpty() || (m_sStandSI.CompareNoCase(L"IMP") == 0 && m_nOldNorm))
		{
			m_bCreateVStand = FALSE;
			m_sReturnTGL.Format(_T("%d;%d;%d"),
				m_nPine,m_nSpruce,m_nBirch);
			OnOK();
		}
		else
			::MessageBox(this->GetSafeHwnd(),m_sMsgSIError,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}
	else
		::MessageBox(this->GetSafeHwnd(),m_sMsgSumError,m_sMsgCap,MB_ICONSTOP | MB_OK);
}

// We'll also create a evaluation stand
void CAddTGLDlg::OnBnClickedSparaVardering()
{
	int m_nOldNorm=0;
	CTransaction_elv_object *pObj = getActiveObject();
	// Check that Sum of spcie mix = 100%
	m_nPine = m_wndEdit1.getInt();
	m_nSpruce = m_wndEdit2.getInt();
	m_nBirch = m_wndEdit3.getInt();

	m_sStandNum = m_wndEdit4.getText();
	m_sStandName = m_wndEdit5.getText();
	m_wndEdit6.GetWindowTextW(m_sStandSI);
	m_fAreal = m_wndEdit7.getFloat();
	m_fVolym = m_wndEdit8.getFloat();
	m_nAge = m_wndEdit9.getInt();
	m_nSide = m_wndEdit10.getText().IsEmpty() ? 1 : m_wndEdit10.getInt();

	if(m_wndCheckBox.GetCheck())
		m_bTillfUtnyttj=TRUE;
	else
		m_bTillfUtnyttj=FALSE;


	if(pObj!=NULL)
	{
		if (pObj->getObjP30TypeOf() == TEMPLATE_P30)
			m_nOldNorm=1;
		else
			m_nOldNorm=0;
	}

	if ((m_nPine+m_nSpruce+m_nBirch) == 100)
	{
		//Check SI #3592 J� 20130318		
		if (m_sStandSI.FindOneOf(_T("TGBFCE")) > -1 || m_sStandSI.IsEmpty() || (m_sStandSI.CompareNoCase(L"IMP") == 0 && m_nOldNorm))
		{
			m_bCreateVStand = TRUE;
			m_sReturnTGL.Format(_T("%d;%d;%d"),
			m_nPine,m_nSpruce,m_nBirch);
			OnOK();
		}
		else
			::MessageBox(this->GetSafeHwnd(),m_sMsgSIError,m_sMsgCap,MB_ICONSTOP | MB_OK);

	}
	else
		::MessageBox(this->GetSafeHwnd(),m_sMsgSumError,m_sMsgCap,MB_ICONSTOP | MB_OK);
}
