// CLoggMessageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LoggMessageDlg.h"

// CLoggMessageDlg dialog

IMPLEMENT_DYNAMIC(CLoggMessageDlg, CDialog)

BEGIN_MESSAGE_MAP(CLoggMessageDlg, CDialog)
	ON_BN_CLICKED(IDC_PRINT_OUT, &CLoggMessageDlg::OnBnClickedPrintOut)
	ON_BN_CLICKED(IDC_SAVE_TO_FILE, &CLoggMessageDlg::OnBnClickedSaveToFile)
END_MESSAGE_MAP()

CLoggMessageDlg::CLoggMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoggMessageDlg::IDD, pParent)
{
}

CLoggMessageDlg::~CLoggMessageDlg()
{
}

void CLoggMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_EDIT1, m_wndMessage);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	DDX_Control(pDX, IDC_PRINT_OUT, m_wndPrintOutBtn);
	DDX_Control(pDX, IDC_SAVE_TO_FILE, m_wndSaveToFileBtn);
	//}}AFX_DATA_MAP
}

BOOL CLoggMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_sCaption);

	m_wndMessage.SetEnabledColor(BLACK,INFOBK);
	m_wndMessage.SetDisabledColor(BLACK,INFOBK);
	m_wndMessage.SetReadOnly();

	m_wndMessage.SetWindowText((m_sMessage));
	m_wndCancelBtn.SetWindowText(m_sCancel);
	m_wndPrintOutBtn.SetWindowText(m_sPrintOut);
	m_wndSaveToFileBtn.SetWindowText(m_sSaveToFile);

	return TRUE;
}

void CLoggMessageDlg::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void CLoggMessageDlg::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
}

void CLoggMessageDlg::OnPrint(CDC* pDC, CPrintInfo *pInfo)
{
	CPage *ps = new CPage(pInfo->m_rectDraw,pDC,MM_TEXT);
	PrintForm(ps);
	delete ps;
}

// CLoggMessageDlg message handlers

// Setup form for printout; 080121 p�d
void CLoggMessageDlg::PrintForm(CPage *page)
{
	//	Print logg message
	page->Print(0.0,0.0,TEXT_NORMAL|TEXT_LEFT,8,m_sMessage);
}

// Print out Loggbook; 080121 p�d
void CLoggMessageDlg::OnBnClickedPrintOut()
{
		CDC dc;
    CPrintDialog printDlg(FALSE);

    if (printDlg.DoModal() == IDCANCEL)     // Get printer settings from user
        return;

    dc.Attach(printDlg.GetPrinterDC());     // Get and attach a printer DC
    dc.m_bPrinting = TRUE;

    CString strTitle;                       // Get the application title
    strTitle.LoadString(AFX_IDS_APP_TITLE);

    DOCINFO di;                             // Initialise print document details
    ::ZeroMemory (&di, sizeof (DOCINFO));
    di.cbSize = sizeof (DOCINFO);
    di.lpszDocName = strTitle;

    BOOL bPrintingOK = dc.StartDoc(&di);    // Begin a new print job

    // Get the printing extents and store in the m_rectDraw field of a 
    // CPrintInfo object
    CPrintInfo Info;
    Info.m_rectDraw.SetRect(0,0, 
                            dc.GetDeviceCaps(HORZRES), 
                            dc.GetDeviceCaps(VERTRES));


    OnBeginPrinting(&dc, &Info);            // Call your "Init printing" function
    OnPrint(&dc, &Info);										// Call your "Print page" function
    OnEndPrinting(&dc, &Info);              // Call your "Clean up" function

    if (bPrintingOK)
        dc.EndDoc();                        // end a print job
    else
        dc.AbortDoc();                      // abort job.

    dc.DeleteDC();                          // delete the printer DC
	

}

void CLoggMessageDlg::OnBnClickedSaveToFile()
{
	CString sObjMapp;
	//	something to contain the number of bytes read
	DWORD dwNumWritten;

	// szFilters is a text string for file name filter
	TCHAR szFilters[]= _T("Filer (*.txt)|*.txt||");

	sObjMapp.Format(_T("%s_%s"),m_sObjName,m_sObjNameID);
	sObjMapp = checkDirectoryName(sObjMapp);
	// Check if info directory for Object exists or not; 080618 p�d
	doCreateObjectDataDirectory(sObjMapp,SETUP_INFO_DIR);
	// Create an Open dialog; the default file name extension is ".txt".
	CString sDefaultFileName;
	sDefaultFileName.Format(_T("%s\\%s%s.txt"),getObjectInfoDirectory(sObjMapp),m_sLogFileName,getDateTimeEx());
	CFileDialog fileDlg (FALSE, _T("txt"), sDefaultFileName,OFN_OVERWRITEPROMPT, szFilters, this);
	 
	if (fileDlg.DoModal() == IDOK)
	{
		CString sSearchPath = fileDlg.GetPathName();
		HANDLE hFile = CreateFile(sSearchPath, GENERIC_WRITE, FILE_SHARE_WRITE,
														NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile)
		{
			WriteFile(hFile, m_sMessage.GetBuffer(), m_sMessage.GetAllocLength()*sizeof(TCHAR), &dwNumWritten,NULL);
			
			CloseHandle(hFile);
		}	// if (hFile)
	}	// if (fileDlg.DoModal() == IDOK)
}
