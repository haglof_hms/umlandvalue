#pragma once
#include "Resource.h"
#include "ResLangFileReader.h"

// CShowInformationDlgPrecut dialog
// HMS-49 2020-02-11
class CShowInformationDlgPrecut : public CDialog
{
	DECLARE_DYNAMIC(CShowInformationDlgPrecut)

	BOOL m_bInitialized;
	CString m_sLangFN;
	RLFReader m_xml;

	int m_nEvalId;
	int m_nObjId;
	int m_nPropId;

	CFont m_fnt1;
	CFont m_fnt1_strikeout;
	CFont m_fnt2;
	CFont m_fnt2_strikeout;

	CUMLandValueDB *m_pDB;

	void showPreCut(CDC *dc);
	void setElv(int nEvalId)
	{	
	m_nEvalId=nEvalId;
	}

public:
	CShowInformationDlgPrecut(CWnd* pParent = NULL);   // standard constructor
	virtual ~CShowInformationDlgPrecut();	

// Dialog Data
	enum { IDD = IDD_DIALOG_PRECUT };

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
