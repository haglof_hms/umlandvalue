#pragma once

#include "1950ForrestNormFrame.h"
#include "Resource.h"
#include "UMLandValueDB.h"

/////////////////////////////////////////////////////////////////////////////////////
// CSpeciesFormView form view

class CSpeciesFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSpeciesFormView)

// private:
	BOOL m_bInitialized;
	BOOL m_bTest;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sDoneSavingMsg;
	//CString m_sRemoveSpecieMsg1;
	//CString m_sRemoveSpecieMsg2;
	//CString m_sMsgCap;
	CStringArray m_sarrCol;
	CStringArray m_arrP3Species;
	vecTransactionSpecies m_vecSpeciesData;
	BOOL getSpecies(void);
	//int getSpecieNextID(void);

protected:
	CSpeciesFormView();           // protected constructor used by dynamic creation
	virtual ~CSpeciesFormView();

	// My data members
	CMyReportCtrl m_wndReport1;

	// My methods
	void setResize(CWnd *,int x,int y,int w,int h,BOOL use_winpos = FALSE);
	BOOL setupReport1(void);
	
	BOOL populateReport(void);
	BOOL clearReport(void);

	//BOOL addSpecies(void);
	//BOOL removeSpecies(void);

	CUMLandValueDB *m_pDB;
	CUMLandValueDB m_DB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	void setReportFocus(void);
	void addP30SpeciesConstraints(void);

public:
	BOOL saveSpecies(void);

	BOOL getIsDirty(void)
	{
		if (m_wndReport1.GetSafeHwnd() != NULL)
			return m_wndReport1.isDirty();

		return FALSE;
	}

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW_P30SPEC_SET };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnClose();
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// CSpeciesReportRec

class CSpeciesReportRec : public CXTPReportRecord
{
	int m_nID;
	CTransaction_species m_recSpecie;
protected:

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CSpeciesReportRec(void)
	{
		m_arrItems.SetSize(0,0);
		m_nID	= -1;	
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));


		AddItem(new CExIconItem(_T(""),3));

	}

	CSpeciesReportRec(int spc_id,LPCTSTR P30SpecTExt)
	{
		m_arrItems.SetSize(0,1);
		m_nID	= -1;	
		AddItem(new CIntItem(spc_id));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CExTextItem((P30SpecTExt)));
	}

	CSpeciesReportRec(int id,CTransaction_species rec,LPCTSTR P30SpecTExt)
	{
		m_recSpecie = rec;
		m_arrItems.SetSize(0,1);
		m_nID	= id;
		AddItem(new CIntItem(rec.getSpcID()));
		AddItem(new CTextItem(rec.getSpcName()));
		AddItem(new CTextItem(rec.getNotes()));
		AddItem(new CExTextItem((P30SpecTExt)));
	}

	virtual ~CSpeciesReportRec(void)
	{
		m_arrItems.RemoveAll();
		m_arrItems.SetSize(0,0);
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		if (item == 0)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

	CTransaction_species& getRecord(void)	{ return m_recSpecie; }
};



