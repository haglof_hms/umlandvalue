
#include "stdafx.h"
#include "PropertyRemoveSelListFormView.h"
#include "1950ForrestNormFrame.h"
#include "ResLangFileReader.h"

#include "XTPPreviewView.h"

//Lagt till ny klass f�r att v�lja fler fastigheter att radera p� en g�ng Feature #2456 20111020 J�
// CPropertyRemoveSelListFormView

IMPLEMENT_DYNCREATE(CPropertyRemoveSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertyRemoveSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()

CPropertyRemoveSelListFormView::CPropertyRemoveSelListFormView()
	: CXTPReportView()
{
	m_nObjId=-1;
	m_pDB = NULL;
}

CPropertyRemoveSelListFormView::~CPropertyRemoveSelListFormView()
{
	m_vecPropertyData.clear();
}

void CPropertyRemoveSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	if(m_nObjId!=-1)
		getProperties(m_nObjId);

	setupReport();

	//CPropertyRemoveSelListFrame* pWnd = (CPropertyRemoveSelListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);

	/*if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST1, &pWnd->m_wndFieldChooser);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}*/

	LoadReportState();
}

BOOL CPropertyRemoveSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CPropertyRemoveSelListFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

LRESULT CPropertyRemoveSelListFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{	

	switch (wParam)
	{
		case ID_SHOWVIEW_MSG :

			m_nObjId=(int)lParam;
			getProperties(m_nObjId);
			break;
	}
		return 0L;
}

BOOL CPropertyRemoveSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertyRemoveSelListFormView diagnostics

#ifdef _DEBUG
void CPropertyRemoveSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CPropertyRemoveSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CPropertyRemoveSelListFormView message handlers

// CPropertyRemoveSelListFormView message handlers
void CPropertyRemoveSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CPropertyRemoveSelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CPropertyRemoveSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );
					// Add these 3 lines to add scrollbars for View; 070319 p�d
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0, (xml.str(IDS_STRING2313)), 40));
					pCol->AllowRemove(false);
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(1, (xml.str(IDS_STRING253)), 50));
					pCol->AllowRemove(false);
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(2, (xml.str(IDS_STRING254)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(3, (xml.str(IDS_STRING255)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(4, (xml.str(IDS_STRING261)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(5, (xml.str(IDS_STRING256)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(6, (xml.str(IDS_STRING257)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(7, (xml.str(IDS_STRING258)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(8, (xml.str(IDS_STRING259)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(9, (xml.str(IDS_STRING260)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->AllowRemove(false);

					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);
					GetReportCtrl().FocusSubItems(TRUE);

					//populateReport();		

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CPropertyRemoveSelListFormView::populateReport(void)
{
	CString S;
	CXTPReportRecord *pRec = NULL;

	if(m_nObjId!=-1)
	{
		GetReportCtrl().GetRecords()->RemoveAll();
		for (UINT i = 0;i < m_vecPropertyData.size();i++)
		{
			CTransaction_property data = m_vecPropertyData[i];
			if (data.getID() == m_nCurrentPropId)
			{
				pRec = GetReportCtrl().AddRecord(new CPropertyRemoveReportDataRec(i,data));
			}
			else
			{
				GetReportCtrl().AddRecord(new CPropertyRemoveReportDataRec(i,data));
			}
		}
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
		if (pRec)
		{
			CXTPReportRow *pRow = GetReportCtrl().GetRows()->Find(pRec);
			if (pRow)
			{
				GetReportCtrl().SetFocusedRow(pRow);
			}
		}
	}
}




void CPropertyRemoveSelListFormView::OnRefresh()
{
	if(m_nObjId!=-1)
	{
	getProperties(m_nObjId);
	populateReport();
	}
}

// CPropertyRemoveSelListFormView message handlers
/*
void CPropertyRemoveSelListFormView::getProperties(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_vecPropertyData);
	}	// if (m_pDB != NULL)
}*/

void CPropertyRemoveSelListFormView::getProperties(int nObjId)
{
	CString sSql=_T("");
	m_nObjId=nObjId;
	sSql.Format(_T("select \
fst_property_table.id,\
fst_property_table.county_code,\
fst_property_table.municipal_code,\
fst_property_table.parish_code,\
fst_property_table.county_name,\
fst_property_table.municipal_name,\
fst_property_table.parish_name,\
fst_property_table.prop_number,\
fst_property_table.prop_name,\
fst_property_table.block_number,\
fst_property_table.unit_number,\
fst_property_table.areal_ha,\
fst_property_table.areal_measured_ha,\
fst_property_table.created_by,\
fst_property_table.created,\
fst_property_table.obj_id,\
fst_property_table.prop_full_name from %s Join %s on %s.id=%s.prop_id where prop_object_id=%d"),TBL_PROPERTY,TBL_ELV_PROPERTIES,TBL_PROPERTY,TBL_ELV_PROPERTIES,nObjId);
	//AfxMessageBox(sSql);
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(sSql,m_vecPropertyData);
		populateReport();
	}	// if (m_pDB != NULL)
}


void CPropertyRemoveSelListFormView::LoadReportState()
{
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PROPERTY_REMOVE_SELLIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	
	GetReportCtrl().Populate();
		
}

void CPropertyRemoveSelListFormView::SaveReportState()
{
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PROPERTY_REMOVE_SELLIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);


}

