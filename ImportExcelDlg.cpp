// ImportExcelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImportExcelDlg.h"
#include "MapSpeciesDlg.h"
#include "MapTreeTypesDlg.h"
#include "ResLangFileReader.h"


// CImportExcelDlg dialog

CString CImportExcelDlg::m_sVersion;

IMPLEMENT_DYNAMIC(CImportExcelDlg, CDialog)

CImportExcelDlg::CImportExcelDlg(CString filename, CUMLandValueDB *pDB, CWnd* pParent /*=NULL*/)
	: CDialog(CImportExcelDlg::IDD, pParent)
{
	m_sFilename = filename;
	m_pDB = pDB;
	m_pBook = NULL;
}

CImportExcelDlg::~CImportExcelDlg()
{
	if(m_pBook)
	{
		m_pBook->release();
		m_pBook = NULL;
	}
}

void CImportExcelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO5, m_wndCombo2);
	DDX_Control(pDX, IDC_COMBO2, m_wndCombo3);
	DDX_Control(pDX, IDC_COMBO6, m_wndCombo4);
	DDX_Control(pDX, IDC_COMBO7, m_wndCombo5);
	DDX_Control(pDX, IDC_COMBO8, m_wndCombo6);
	DDX_Control(pDX, IDC_COMBO9, m_wndComboHgt);
	DDX_Control(pDX, IDC_COMBO10, m_wndCombo8);
	DDX_Control(pDX, IDC_COMBO11, m_wndCombo9);
	DDX_Control(pDX, IDC_COMBO12, m_wndCombo10);
	DDX_Control(pDX, IDC_COMBO13, m_wndCombo11);
	DDX_Control(pDX, IDC_COMBO14, m_wndCombo12);
	DDX_Control(pDX, IDC_COMBO3, m_wndCombo13);
	DDX_Control(pDX, IDC_COMBO4, m_wndCombo14);
	DDX_Control(pDX, IDC_COMBO15, m_wndCombo15);
	DDX_Control(pDX, IDC_COMBO16, m_wndCombo16);
	DDX_Control(pDX, IDC_COMBO17, m_wndComboSida);
	DDX_Control(pDX, IDC_COMBO18, m_wndComboAreal);
	DDX_Control(pDX, IDC_COMBO25, m_wndComboArealUnit);
	DDX_Control(pDX, IDC_COMBO19, m_wndComboBestalder);
	DDX_Control(pDX, IDC_COMBO20, m_wndComboBestlat);
	DDX_Control(pDX, IDC_COMBO21, m_wndComboBestlong);
	DDX_Control(pDX, IDC_COMBO26, m_wndComboBestCoords);
	DDX_Control(pDX, IDC_COMBO22, m_wndComboBestbon);
	DDX_Control(pDX, IDC_COMBO23, m_wndComboLangd);
	DDX_Control(pDX, IDC_COMBO24, m_wndComboBredd);
	DDX_Control(pDX, IDC_COMBO_TOPPN, m_wndComboToppn);
	DDX_Control(pDX, IDC_COMBO_FASAVST, m_wndComboFasavst);
	DDX_Control(pDX, IDC_COMBO_KATEG, m_wndComboKateg);
	DDX_Control(pDX, IDC_COMBO_TOPPN_UNIT, m_wndComboToppnUnit);
	DDX_Control(pDX, IDC_COMBO_FASAVST_UNIT, m_wndComboFasavstUnit);
	DDX_Control(pDX, IDC_COMBO_GKHGT, m_wndComboGkhgt);
	DDX_Control(pDX, IDC_COMBO_GKHGT_UNIT, m_wndComboGkhgtUnit);
	DDX_Control(pDX, IDC_COMBO_BARK, m_wndComboBark);
	DDX_Control(pDX, IDC_LBL2, m_wndLabel2);
	DDX_Control(pDX, IDC_LBL3, m_wndLabel3);
	DDX_Control(pDX, IDC_LBL4, m_wndLabel4);
	DDX_Control(pDX, IDC_LBL5, m_wndLabel5);
	DDX_Control(pDX, IDC_LBL6, m_wndLabel6);
	DDX_Control(pDX, IDC_LBL7, m_wndLabel7);
	DDX_Control(pDX, IDC_LBL8, m_wndLabel8);
	DDX_Control(pDX, IDC_LBL9, m_wndLabel9);
	DDX_Control(pDX, IDC_LBL10, m_wndLabel10);
	DDX_Control(pDX, IDC_LBL11, m_wndLabel11);
	DDX_Control(pDX, IDC_LBL12, m_wndLabel12);
	DDX_Control(pDX, IDC_LBL13, m_wndLabel13);
	DDX_Control(pDX, IDC_LBL14, m_wndLabel14);
	DDX_Control(pDX, IDC_LBL15, m_wndLabelSida);
	DDX_Control(pDX, IDC_LBL21, m_wndLabelAreal);
	DDX_Control(pDX, IDC_LBL16, m_wndLabelBestalder);
	DDX_Control(pDX, IDC_LBL17, m_wndLabelBestlat);
	DDX_Control(pDX, IDC_LBL22, m_wndLabelBestlong);
	DDX_Control(pDX, IDC_LBL18, m_wndLabelBestbon);
	DDX_Control(pDX, IDC_LBL19, m_wndLabelLangd);
	DDX_Control(pDX, IDC_LBL20, m_wndLabelBredd);
	DDX_Control(pDX, IDC_LBL_TOPPN, m_wndLabelToppn);
	DDX_Control(pDX, IDC_LBL_FASAVST, m_wndLabelFasavst);
	DDX_Control(pDX, IDC_LBL_KATEG, m_wndLabelKateg);
	DDX_Control(pDX, IDC_LBL_GKHGT, m_wndLabelGkhgt);
	DDX_Control(pDX, IDC_LBL_BARK, m_wndLabelBark);
	DDX_Control(pDX, IDC_GROUP1, m_wndGroupBestdata);
	DDX_Control(pDX, IDC_GROUP2, m_wndGroupTraddata);
	DDX_Control(pDX, IDC_CHECK1, m_wndCheck1);
}

BOOL CImportExcelDlg::OnInitDialog()
{
	CString buf;

	if( !CDialog::OnInitDialog() )
		return FALSE;

	// Set up language
	CString	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING3269));
			m_wndGroupBestdata.SetWindowText(xml->str(IDS_STRING3308));
			m_wndGroupTraddata.SetWindowText(xml->str(IDS_STRING3309));
			m_wndLabel2.SetWindowText(xml->str(IDS_STRING3261));
			m_wndLabel3.SetWindowText(xml->str(IDS_STRING3262));
			m_wndLabel4.SetWindowText(xml->str(IDS_STRING3263));
			m_wndLabel5.SetWindowText(xml->str(IDS_STRING3264));
			m_wndLabel6.SetWindowText(xml->str(IDS_STRING3265));
			m_wndLabel7.SetWindowText(xml->str(IDS_STRING3266));
			m_wndLabel8.SetWindowText(xml->str(IDS_STRING3271));
			m_wndLabel9.SetWindowText(xml->str(IDS_STRING3272));
			m_wndLabel10.SetWindowText(xml->str(IDS_STRING3267));
			m_wndLabel11.SetWindowText(xml->str(IDS_STRING3268));
			m_wndLabel12.SetWindowText(xml->str(IDS_STRING3273)); buf = xml->str(IDS_STRING3276); buf.Replace(_T("\\n"), _T("\n"));
			m_wndLabel13.SetWindowText(buf);
			m_wndLabelSida.SetWindowText(xml->str(IDS_STRING3300));
			m_wndLabelAreal.SetWindowText(xml->str(IDS_STRING3301));
			m_wndLabelBestalder.SetWindowText(xml->str(IDS_STRING3302));
			m_wndLabelBestlat.SetWindowText(xml->str(IDS_STRING3303));
			m_wndLabelBestlong.SetWindowText(xml->str(IDS_STRING3304));
			m_wndLabelBestbon.SetWindowText(xml->str(IDS_STRING3305));
			m_wndLabelLangd.SetWindowText(xml->str(IDS_STRING3306));
			m_wndLabelBredd.SetWindowText(xml->str(IDS_STRING3307));
			m_wndLabelToppn.SetWindowText(xml->str(IDS_STRING3280));
			m_wndLabelFasavst.SetWindowText(xml->str(IDS_STRING3281));
			m_wndLabelKateg.SetWindowText(xml->str(IDS_STRING3282));
			m_wndLabelGkhgt.SetWindowText(xml->str(IDS_STRING3283));
			m_wndLabelBark.SetWindowText(xml->str(IDS_STRING3291));
			m_wndCheck1.SetWindowText(xml->str(IDS_STRING3284));
			m_wndLabel14.SetWindowText(xml->str(IDS_STRING3285));

			m_wndComboArealUnit.AddString(_T("ha"));
			m_wndComboArealUnit.AddString(_T("m2"));

			m_wndCombo13.AddString(_T("m"));
			m_wndCombo13.AddString(_T("dm"));

			m_wndComboBestCoords.AddString(_T("SWEREF99 TM"));
			m_wndComboBestCoords.AddString(_T("RT90"));
			m_wndComboBestCoords.AddString(_T("ddmm.mmmm"));
			m_wndComboBestCoords.AddString(_T("dd.dddddd"));

			m_wndCombo14.AddString(_T("SWEREF99 TM"));
			m_wndCombo14.AddString(_T("RT90"));
			m_wndCombo14.AddString(_T("ddmm.mmmm"));
			m_wndCombo14.AddString(_T("dd.dddddd"));

			m_wndCombo15.AddString(_T("cm"));
			m_wndCombo15.AddString(_T("mm"));

			m_wndComboToppnUnit.AddString(_T("m"));
			m_wndComboToppnUnit.AddString(_T("dm"));

			m_wndComboFasavstUnit.AddString(_T("m"));
			m_wndComboFasavstUnit.AddString(_T("dm"));

			m_wndComboGkhgtUnit.AddString(_T("m"));
			m_wndComboGkhgtUnit.AddString(_T("dm"));
		}
	}

	// Load Excel
	if( m_sFilename.Right(4).CompareNoCase(_T(".xls")) == 0 )
		m_pBook = xlCreateBook();
	else if( m_sFilename.Right(5).CompareNoCase(_T(".xlsx")) == 0 )
		m_pBook = xlCreateXMLBook();

	if( m_pBook )
	{
		if( m_pBook->load(m_sFilename) )
		{
			//m_pBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
		   //m_pBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
		   m_pBook->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");

			// List sheets in workbook
			for( int i = 0; i < m_pBook->sheetCount(); i++ )
			{
				m_wndCombo16.AddString(m_pBook->getSheet(i)->name());
			}

			// Select first sheet
			if( m_wndCombo16.GetCount() > 0 )
			{
				m_wndCombo16.SetCurSel(0);
				LoadSheet();
			}
		}
	}

	return TRUE;
}

void CImportExcelDlg::OnOK()
{
	if( GenerateHXL() )
	{
		CDialog::OnOK();
	}
}

double CImportExcelDlg::GpsEncode(double val)
{
	// Convert dd.dddddd to ddmm.mmmm
	double d = (int)val;
	val -= d;
	return d * 100 + (val * 60.0);
}

void CImportExcelDlg::LoadSpecies()
{
	CString spc;
	int col;
	vecTransactionSpecies vecSpecies;

	m_spcmap.clear();
	m_pDB->getSpecies(vecSpecies);

	// Make a map of specie names
	col = m_wndCombo5.GetCurSel() > 0 ? m_wndCombo5.GetItemData(m_wndCombo5.GetCurSel()) : -1;
	if( col >= 0 )
	{
		Sheet* pSheet = m_pBook->getSheet(m_wndCombo16.GetCurSel());
		if( pSheet )
		{
			for( int row = pSheet->firstRow() + 1; row < pSheet->lastRow(); row++ )
			{
				spc = pSheet->readStr(row, col);
				if( !spc.IsEmpty() ) 
				{
					// Load value from registry
					m_spcmap[spc] = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_SPECIES_KEY, spc, -1);

					// Try to match specie by name if no mapping were found
					if( m_spcmap[spc] == -1 )
					{
						for( int i = 0; i < vecSpecies.size(); i++ )
						{
							if( vecSpecies[i].getSpcName() == spc )
							{
								m_spcmap[spc] = vecSpecies[i].getSpcID();
								break;
							}
						}
					}
				}
			}
		}
	}
}

void CImportExcelDlg::LoadTreeTypes()
{
	CString type;
	int col;

	m_treemap.clear();

	// Make a map of tree types
	col = m_wndCombo12.GetCurSel() > 2 ? m_wndCombo12.GetItemData(m_wndCombo12.GetCurSel()) : -1;
	if( col >= 0 )
	{
		Sheet* pSheet = m_pBook->getSheet(m_wndCombo16.GetCurSel());
		if( pSheet )
		{
			for( int row = pSheet->firstRow() + 1; row < pSheet->lastRow(); row++ )
			{
				type = pSheet->readStr(row, col);
				if( !type.IsEmpty() ) m_treemap[type] = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_TREETYPES_KEY, type, -1); // Load value from registry
			}
		}
	}
}

void CImportExcelDlg::LoadSheet()
{
	CString buf;
	int idx;

	m_wndCombo2.ResetContent(); m_wndCombo2.AddString(_T(""));
	m_wndCombo3.ResetContent(); m_wndCombo3.AddString(_T(""));
	m_wndCombo4.ResetContent(); m_wndCombo4.AddString(_T(""));
	m_wndComboSida.ResetContent(); m_wndComboSida.AddString(_T(""));
	m_wndComboAreal.ResetContent(); m_wndComboAreal.AddString(_T(""));
	m_wndComboBestalder.ResetContent(); m_wndComboBestalder.AddString(_T(""));
	m_wndComboBestlat.ResetContent(); m_wndComboBestlat.AddString(_T(""));
	m_wndComboBestlong.ResetContent(); m_wndComboBestlong.AddString(_T(""));
	m_wndComboBestbon.ResetContent(); m_wndComboBestbon.AddString(_T(""));
	m_wndComboLangd.ResetContent(); m_wndComboLangd.AddString(_T(""));
	m_wndComboBredd.ResetContent(); m_wndComboBredd.AddString(_T(""));
	m_wndCombo5.ResetContent(); m_wndCombo5.AddString(_T(""));
	m_wndCombo6.ResetContent(); m_wndCombo6.AddString(_T(""));
	m_wndComboHgt.ResetContent(); m_wndComboHgt.AddString(_T(""));
	m_wndCombo8.ResetContent(); m_wndCombo8.AddString(_T(""));
	m_wndCombo9.ResetContent(); m_wndCombo9.AddString(_T(""));
	m_wndCombo10.ResetContent(); m_wndCombo10.AddString(_T(""));
	m_wndCombo11.ResetContent(); m_wndCombo11.AddString(_T(""));
	m_wndCombo12.ResetContent(); m_wndCombo12.AddString(_T(""));
	m_wndComboToppn.ResetContent(); m_wndComboToppn.AddString(_T(""));
	m_wndComboFasavst.ResetContent(); m_wndComboFasavst.AddString(_T(""));
	m_wndComboKateg.ResetContent(); m_wndComboKateg.AddString(_T(""));
	m_wndComboGkhgt.ResetContent(); m_wndComboGkhgt.AddString(_T(""));
	m_wndComboBark.ResetContent(); m_wndComboBark.AddString(_T(""));

	// Language dependent tree types
	CString	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			m_wndCombo12.AddString(xml->str(IDS_STRING3274));
			m_wndCombo12.AddString(xml->str(IDS_STRING3275));
		}
	}

	// Load data
	Sheet* pSheet = m_pBook->getSheet(m_wndCombo16.GetCurSel());
	if(pSheet)
	{
		// List sheet columns
		int row = pSheet->firstRow();
		for(int col = pSheet->firstCol(); col < pSheet->lastCol(); col++)
		{
			CellType ct = pSheet->cellType(row, col);
			if(ct == CELLTYPE_STRING)
			{
				buf = pSheet->readStr(row, col);
				idx = m_wndCombo2.AddString(buf); m_wndCombo2.SetItemData(idx, col);
				idx = m_wndCombo3.AddString(buf); m_wndCombo3.SetItemData(idx, col);
				idx = m_wndCombo4.AddString(buf); m_wndCombo4.SetItemData(idx, col);
				idx = m_wndComboSida.AddString(buf); m_wndComboSida.SetItemData(idx, col);
				idx = m_wndComboAreal.AddString(buf); m_wndComboAreal.SetItemData(idx, col);
				idx = m_wndComboBestalder.AddString(buf); m_wndComboBestalder.SetItemData(idx, col);
				idx = m_wndComboBestlat.AddString(buf); m_wndComboBestlat.SetItemData(idx, col);
				idx = m_wndComboBestlong.AddString(buf); m_wndComboBestlong.SetItemData(idx, col);
				idx = m_wndComboBestbon.AddString(buf); m_wndComboBestbon.SetItemData(idx, col);
				idx = m_wndComboLangd.AddString(buf); m_wndComboLangd.SetItemData(idx, col);
				idx = m_wndComboBredd.AddString(buf); m_wndComboBredd.SetItemData(idx, col);
				idx = m_wndCombo5.AddString(buf); m_wndCombo5.SetItemData(idx, col);
				idx = m_wndCombo6.AddString(buf); m_wndCombo6.SetItemData(idx, col);
				idx = m_wndComboHgt.AddString(buf); m_wndComboHgt.SetItemData(idx, col);
				idx = m_wndCombo8.AddString(buf); m_wndCombo8.SetItemData(idx, col);
				idx = m_wndCombo9.AddString(buf); m_wndCombo9.SetItemData(idx, col);
				idx = m_wndCombo10.AddString(buf); m_wndCombo10.SetItemData(idx, col);
				idx = m_wndCombo11.AddString(buf); m_wndCombo11.SetItemData(idx, col);
				idx = m_wndCombo12.AddString(buf); m_wndCombo12.SetItemData(idx, col);
				idx = m_wndComboToppn.AddString(buf); m_wndComboToppn.SetItemData(idx, col);
				idx = m_wndComboFasavst.AddString(buf); m_wndComboFasavst.SetItemData(idx, col);
				idx = m_wndComboKateg.AddString(buf); m_wndComboKateg.SetItemData(idx, col);
				idx = m_wndComboGkhgt.AddString(buf); m_wndComboGkhgt.SetItemData(idx, col);
				idx = m_wndComboBark.AddString(buf); m_wndComboBark.SetItemData(idx, col);
			}
		}

		// Load previous mappings
		LoadValue(m_wndCombo2, REG_IMPORT_EXCEL_VAL2);
		LoadValue(m_wndCombo3, REG_IMPORT_EXCEL_VAL3);
		LoadValue(m_wndCombo4, REG_IMPORT_EXCEL_VAL4);
		LoadValue(m_wndComboSida, REG_IMPORT_EXCEL_SIDA);
		LoadValue(m_wndComboAreal, REG_IMPORT_EXCEL_AREAL);
		LoadValue(m_wndComboBestalder, REG_IMPORT_EXCEL_BESTALDER);
		LoadValue(m_wndComboBestlat, REG_IMPORT_EXCEL_BESTLAT);
		LoadValue(m_wndComboBestlong, REG_IMPORT_EXCEL_BESTLONG);
		LoadValue(m_wndComboBestbon, REG_IMPORT_EXCEL_BESTBON);
		LoadValue(m_wndComboLangd, REG_IMPORT_EXCEL_LANGD);
		LoadValue(m_wndComboBredd, REG_IMPORT_EXCEL_BREDD);
		LoadValue(m_wndCombo5, REG_IMPORT_EXCEL_VAL5);
		LoadValue(m_wndCombo6, REG_IMPORT_EXCEL_VAL6);
		LoadValue(m_wndComboHgt, REG_IMPORT_EXCEL_VAL7);
		LoadValue(m_wndCombo8, REG_IMPORT_EXCEL_VAL8);
		LoadValue(m_wndCombo9, REG_IMPORT_EXCEL_VAL9);
		LoadValue(m_wndCombo10, REG_IMPORT_EXCEL_VAL10);
		LoadValue(m_wndCombo11, REG_IMPORT_EXCEL_VAL11);
		LoadValue(m_wndCombo12, REG_IMPORT_EXCEL_VAL12);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL13, 1);			if( idx >= 0 && (m_wndCombo13.GetCount()-1) >= idx ) m_wndCombo13.SetCurSel(idx);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL14, 0);			if( idx >= 0 && (m_wndCombo14.GetCount()-1) >= idx ) m_wndCombo14.SetCurSel(idx);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL15, 1);			if( idx >= 0 && (m_wndCombo15.GetCount()-1) >= idx ) m_wndCombo15.SetCurSel(idx);
		m_wndCheck1.SetCheck(AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL16, 0)); OnBnClickedCheck1();
		LoadValue(m_wndComboToppn, REG_IMPORT_EXCEL_TOPPN);
		LoadValue(m_wndComboFasavst, REG_IMPORT_EXCEL_FASAVST);
		LoadValue(m_wndComboKateg, REG_IMPORT_EXCEL_KATEG);
		LoadValue(m_wndComboGkhgt, REG_IMPORT_EXCEL_GKHGT);
		LoadValue(m_wndComboBark, REG_IMPORT_EXCEL_BARK);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_TOPPN_UNIT, 1);		if( idx >= 0 && (m_wndComboToppnUnit.GetCount()-1) >= idx ) m_wndComboToppnUnit.SetCurSel(idx);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_FASAVST_UNIT, 0);	if( idx >= 0 && (m_wndComboFasavstUnit.GetCount()-1) >= idx ) m_wndComboFasavstUnit.SetCurSel(idx);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_GKHGT_UNIT, 1);		if( idx >= 0 && (m_wndComboGkhgtUnit.GetCount()-1) >= idx ) m_wndComboGkhgtUnit.SetCurSel(idx);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_AREAL_UNIT, 0);		if( idx >= 0 && (m_wndComboArealUnit.GetCount()-1) >= idx ) m_wndComboArealUnit.SetCurSel(idx);
		idx = AfxGetApp()->GetProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BESTCOORD_UNIT, 0);	if( idx >= 0 && (m_wndComboBestCoords.GetCount()-1) >= idx ) m_wndComboBestCoords.SetCurSel(idx);		

		// Reload specie and tree type maps
		LoadSpecies();
		LoadTreeTypes();
	}
}

void CImportExcelDlg::LoadValue(CComboBox &cmb, CString regval)
{
	// Load stored value from registry, set current selection if found
	CString val, sval = AfxGetApp()->GetProfileString(REG_IMPORT_EXCEL_KEY, regval);
	for( int i = 0; i < cmb.GetCount(); i++ )
	{
		cmb.GetLBText(i, val);
		if( val == sval )
		{
			cmb.SetCurSel(i);
			break;
		}
	}
}

void CImportExcelDlg::ConvertToLatLong(HINSTANCE hInst, double &lat, double &lon, int type)
{
	funcConvertToLatLong convert = (funcConvertToLatLong)GetProcAddress(hInst, "ConvertToLatLong");

	// Tree coords
	if( convert && (type == 0 || type == 1) ) // SWEREF99, RT90
	{
		// Use Tatuk GIS to convert to lat/long
		int epsg = type == 0 ? 3006 : 3021;
		convert(epsg, lon, lat, &lat, &lon);
		lat = GpsEncode(lat);
		lon = GpsEncode(lon);
	}
	else if( type == 2 ) // ddmm.mmmm (HXL standard)
	{
		// Do nothing
	}
	else if( type == 3 ) // dd.dddddd
	{
		// Convert to HXL format
		lat = GpsEncode(lat);
		lon = GpsEncode(lon);
	}
	else
	{
		// Something went wrong
		lat = 0;
		lon = 0;
	}
}

BOOL CImportExcelDlg::GenerateHXL()
{
	BOOL ret = TRUE;
	CFile hxl;
	CString path, stand, ignoredRows, ignoredStands, warningRows, bonitet, val;
	CStringA buf, lorig, spcbuf, treebuf;
	bool bIgnoreCurrent;
	int numspc,antH=0;
	int nIgnoredStands = 0;
	int nIgnoredTrees = 0;
	int nWarningTrees = 0;
	const int nMaxRows = 30;
	double minb, maxb;
	double fTempAge=0.0;
	std::map<int, bool> includespc;
	std::list<CString> filelist;
	TreeData td;
	TreeDataList trees;
	vecTransactionSpecies vecSpecies;

	// Use mappings
	const int nColStand			= m_wndCombo2.GetCurSel() > 0 ? m_wndCombo2.GetItemData(m_wndCombo2.GetCurSel()) : -1;
	const int nColBlockUnit		= m_wndCombo3.GetCurSel() > 0 ? m_wndCombo3.GetItemData(m_wndCombo3.GetCurSel()) : -1;
	const int nColStandNumber	= m_wndCombo4.GetCurSel() > 0 ? m_wndCombo4.GetItemData(m_wndCombo4.GetCurSel()) : -1;
	const int nColSida			= m_wndComboSida.GetCurSel() > 0 ? m_wndComboSida.GetItemData(m_wndComboSida.GetCurSel()) : -1;
	const int nColAreal			= m_wndComboAreal.GetCurSel() > 0 ? m_wndComboAreal.GetItemData(m_wndComboAreal.GetCurSel()) : -1;
	const int nColBestalder		= m_wndComboBestalder.GetCurSel() > 0 ? m_wndComboBestalder.GetItemData(m_wndComboBestalder.GetCurSel()) : -1;
	const int nColBestlat		= m_wndComboBestlat.GetCurSel() > 0 ? m_wndComboBestlat.GetItemData(m_wndComboBestlat.GetCurSel()) : -1;
	const int nColBestlong		= m_wndComboBestlong.GetCurSel() > 0 ? m_wndComboBestlong.GetItemData(m_wndComboBestlong.GetCurSel()) : -1;
	const int nColBestbon		= m_wndComboBestbon.GetCurSel() > 0 ? m_wndComboBestbon.GetItemData(m_wndComboBestbon.GetCurSel()) : -1;
	const int nColLangd			= m_wndComboLangd.GetCurSel() > 0 ? m_wndComboLangd.GetItemData(m_wndComboLangd.GetCurSel()) : -1;
	const int nColBredd			= m_wndComboBredd.GetCurSel() > 0 ? m_wndComboBredd.GetItemData(m_wndComboBredd.GetCurSel()) : -1;
	const int nColSpecie		= m_wndCombo5.GetCurSel() > 0 ? m_wndCombo5.GetItemData(m_wndCombo5.GetCurSel()) : -1;
	const int nColDiameter		= m_wndCombo6.GetCurSel() > 0 ? m_wndCombo6.GetItemData(m_wndCombo6.GetCurSel()) : -1;
	const int nColHeight		= m_wndComboHgt.GetCurSel() > 0 ? m_wndComboHgt.GetItemData(m_wndComboHgt.GetCurSel()) : -1;
	const int nColAge			= m_wndCombo8.GetCurSel() > 0 ? m_wndCombo8.GetItemData(m_wndCombo8.GetCurSel()) : -1;
	const int nColBonitet		= m_wndCombo9.GetCurSel() > 0 ? m_wndCombo9.GetItemData(m_wndCombo9.GetCurSel()) : -1;
	const int nColLatitude		= m_wndCombo10.GetCurSel() > 0 ? m_wndCombo10.GetItemData(m_wndCombo10.GetCurSel()) : -1;
	const int nColLongitude		= m_wndCombo11.GetCurSel() > 0 ? m_wndCombo11.GetItemData(m_wndCombo11.GetCurSel()) : -1;
	const int nColTypeOfTree	= m_wndCombo12.GetCurSel() > 2 ? m_wndCombo12.GetItemData(m_wndCombo12.GetCurSel()) : -1;
	const int nColToppn			= m_wndComboToppn.GetCurSel() > 0 ? m_wndComboToppn.GetItemData(m_wndComboToppn.GetCurSel()) : -1;
	const int nColFasavst		= m_wndComboFasavst.GetCurSel() > 0 ? m_wndComboFasavst.GetItemData(m_wndComboFasavst.GetCurSel()) : -1;
	const int nColKateg			= m_wndComboKateg.GetCurSel() > 0 ? m_wndComboKateg.GetItemData(m_wndComboKateg.GetCurSel()) : -1;
	const int nColGkHgt 		= m_wndComboGkhgt.GetCurSel() > 0 ? m_wndComboGkhgt.GetItemData(m_wndComboGkhgt.GetCurSel()) : -1;
	const int nColBark			= m_wndComboBark.GetCurSel() > 0 ? m_wndComboBark.GetItemData(m_wndComboBark.GetCurSel()) : -1;

	// Save mappings
	m_wndCombo2.GetCurSel() >= 0 ? m_wndCombo2.GetLBText(m_wndCombo2.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL2, val);
	m_wndCombo3.GetCurSel() >= 0 ? m_wndCombo3.GetLBText(m_wndCombo3.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL3, val);
	m_wndCombo4.GetCurSel() >= 0 ? m_wndCombo4.GetLBText(m_wndCombo4.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL4, val);
	m_wndCombo5.GetCurSel() >= 0 ? m_wndCombo5.GetLBText(m_wndCombo5.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL5, val);
	m_wndCombo6.GetCurSel() >= 0 ? m_wndCombo6.GetLBText(m_wndCombo6.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL6, val);
	m_wndComboHgt.GetCurSel() >= 0 ? m_wndComboHgt.GetLBText(m_wndComboHgt.GetCurSel(), val) : val.Empty();						AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL7, val);
	m_wndCombo8.GetCurSel() >= 0 ? m_wndCombo8.GetLBText(m_wndCombo8.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL8, val);
	m_wndCombo9.GetCurSel() >= 0 ? m_wndCombo9.GetLBText(m_wndCombo9.GetCurSel(), val) : val.Empty();							AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL9, val);
	m_wndCombo10.GetCurSel() >= 0 ? m_wndCombo10.GetLBText(m_wndCombo10.GetCurSel(), val) : val.Empty();						AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL10, val);
	m_wndCombo11.GetCurSel() >= 0 ? m_wndCombo11.GetLBText(m_wndCombo11.GetCurSel(), val) : val.Empty();						AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL11, val);
	m_wndCombo12.GetCurSel() >= 0 ? m_wndCombo12.GetLBText(m_wndCombo12.GetCurSel(), val) : val.Empty();						AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL12, val);
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL13, m_wndCombo13.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL14, m_wndCombo14.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL15, m_wndCombo15.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_VAL16, m_wndCheck1.GetCheck());
	m_wndComboSida.GetCurSel() >= 0 ? m_wndComboSida.GetLBText(m_wndComboSida.GetCurSel(), val) : val.Empty();					AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_SIDA, val);
	m_wndComboAreal.GetCurSel() >= 0 ? m_wndComboAreal.GetLBText(m_wndComboAreal.GetCurSel(), val) : val.Empty();				AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_AREAL, val);
	m_wndComboBestalder.GetCurSel() >= 0 ? m_wndComboBestalder.GetLBText(m_wndComboBestalder.GetCurSel(), val) : val.Empty();	AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BESTALDER, val);
	m_wndComboBestlat.GetCurSel() >= 0 ? m_wndComboBestlat.GetLBText(m_wndComboBestlat.GetCurSel(), val) : val.Empty();			AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BESTLAT, val);
	m_wndComboBestlong.GetCurSel() >= 0 ? m_wndComboBestlong.GetLBText(m_wndComboBestlong.GetCurSel(), val) : val.Empty();		AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BESTLONG, val);
	m_wndComboBestbon.GetCurSel() >= 0 ? m_wndComboBestbon.GetLBText(m_wndComboBestbon.GetCurSel(), val) : val.Empty();			AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BESTBON, val);
	m_wndComboLangd.GetCurSel() >= 0 ? m_wndComboLangd.GetLBText(m_wndComboLangd.GetCurSel(), val) : val.Empty();				AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_LANGD, val);
	m_wndComboBredd.GetCurSel() >= 0 ? m_wndComboBredd.GetLBText(m_wndComboBredd.GetCurSel(), val) : val.Empty();				AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BREDD, val);
	m_wndComboToppn.GetCurSel() >= 0 ? m_wndComboToppn.GetLBText(m_wndComboToppn.GetCurSel(), val) : val.Empty();				AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_TOPPN, val);
	m_wndComboFasavst.GetCurSel() >= 0 ? m_wndComboFasavst.GetLBText(m_wndComboFasavst.GetCurSel(), val) : val.Empty();			AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_FASAVST, val);
	m_wndComboKateg.GetCurSel() >= 0 ? m_wndComboKateg.GetLBText(m_wndComboKateg.GetCurSel(), val) : val.Empty();				AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_KATEG, val);
	m_wndComboGkhgt.GetCurSel() >= 0 ? m_wndComboGkhgt.GetLBText(m_wndComboGkhgt.GetCurSel(), val) : val.Empty();				AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_GKHGT, val);
	m_wndComboBark.GetCurSel() >= 0 ? m_wndComboBark.GetLBText(m_wndComboBark.GetCurSel(), val) : val.Empty();					AfxGetApp()->WriteProfileString(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BARK, val);
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_TOPPN_UNIT, m_wndComboToppnUnit.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_FASAVST_UNIT, m_wndComboFasavstUnit.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_GKHGT_UNIT, m_wndComboGkhgtUnit.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_AREAL_UNIT, m_wndComboArealUnit.GetCurSel());
	AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_KEY, REG_IMPORT_EXCEL_BESTCOORD_UNIT, m_wndComboBestCoords.GetCurSel());


	CString	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			// Make sure required fields are filled in
			if( nColStand < 0 || (nColBlockUnit < 0 && !m_wndCheck1.GetCheck() ) || nColSpecie < 0 || nColDiameter < 0 || (nColTypeOfTree < 0 && m_wndCombo12.GetCurSel() <= 0) )
			{
				AfxMessageBox(xml->str(IDS_STRING3270));
				return FALSE;
			}
			else if( (nColLatitude > 0 || nColLongitude > 0) && m_wndCombo14.GetCurSel() < 0 )
			{
				AfxMessageBox(xml->str(IDS_STRING3278));
				return FALSE;
			}
			else if( m_wndComboArealUnit.GetCurSel() < 0 || m_wndComboBestCoords.GetCurSel() < 0 || 
					 m_wndCombo13.GetCurSel() < 0 || m_wndCombo14.GetCurSel() < 0 || m_wndCombo15.GetCurSel() < 0 ||
					 m_wndComboToppnUnit.GetCurSel() < 0 || m_wndComboFasavstUnit.GetCurSel() < 0 || m_wndComboGkhgtUnit.GetCurSel() < 0 )
			{
				AfxMessageBox(xml->str(IDS_STRING3289));
				return FALSE;
			}

			// Make sure all species are mapped
			for( SpecieMap::iterator iter = m_spcmap.begin(); iter != m_spcmap.end(); iter++ )
			{
				if( iter->second < 1 )
				{
					AfxMessageBox(xml->str(IDS_STRING3288));
					return FALSE;
				}
			}

			// Make sure all tree types are mapped
			for( TreeTypeMap::iterator iter = m_treemap.begin(); iter != m_treemap.end(); iter++ )
			{
				if( iter->second < 0 )
				{
					AfxMessageBox(xml->str(IDS_STRING3294));
					return FALSE;
				}
			}

			// Create directory for output
			m_sOutputPath = m_sFilename;
			m_sOutputPath.Truncate(m_sOutputPath.ReverseFind('\\'));
			m_wndCombo16.GetLBText(m_wndCombo16.GetCurSel(), val);
			val.Replace('\\', '_');
			val.Replace('/', '_');
			val.Replace(':', '_');
			val.Replace('*', '_');
			val.Replace('?', '_');
			val.Replace('\"', '_');
			val.Replace('<', '_');
			val.Replace('>', '_');
			val.Replace('|', '_');
			m_sOutputPath += _T("\\") + val;
			CreateDirectory(m_sOutputPath, NULL);

			// Use english locale (this will change comma separator to .)
			lorig = setlocale(LC_ALL, NULL);
			setlocale(LC_ALL, "English");

			// Get specie list from db
			m_pDB->getSpecies(vecSpecies);

			if( m_pBook )
			{
				HINSTANCE hInst = AfxLoadLibrary(_T("Suites\\UMGIS.dll"));

				// Map columns to corresponding field
				Sheet* pSheet = m_pBook->getSheet(m_wndCombo16.GetCurSel());
				if( pSheet )
				{
					for( int row = pSheet->firstRow() + 1; row < pSheet->lastRow(); row++ )
					{

						

						if( nColStand >= 0 )		td.sStand			= pSheet->readStr(row, nColStand);
						if( nColBlockUnit >= 0 )	td.sBlock			= pSheet->readStr(row, nColBlockUnit);
						if( nColBlockUnit >= 0 )	td.sUnit			= pSheet->readStr(row, nColBlockUnit);
						if( nColStandNumber >= 0 )	td.nStandNumber		= pSheet->readNum(row, nColStandNumber);else td.nStandNumber= 0;
						if( nColBestalder >= 0 )	td.nStandAge		= pSheet->readNum(row, nColBestalder);	else td.nStandAge	= 0;
						if( nColAreal >= 0 )		td.nStandArea		= pSheet->readNum(row, nColAreal);		else td.nStandArea	= 0;
						if( nColLangd >= 0 )		td.nStandLength		= pSheet->readNum(row, nColLangd);		else td.nStandLength= 0;
						if( nColBredd >= 0 )		td.nStandWidth		= pSheet->readNum(row, nColBredd);		else td.nStandWidth	= 0;
						if( nColBestbon >= 0 )		td.sStandBonitet	= pSheet->readStr(row, nColBestbon);
						if( nColSida >= 0 )			td.nSide			= pSheet->readNum(row, nColSida);		else td.nSide		= 1;
						if( nColBestlat >= 0 )		td.fStandLat		= pSheet->readNum(row, nColBestlat);	else td.fStandLat	= 0;
						if( nColBestlong >= 0 )		td.fStandLong		= pSheet->readNum(row, nColBestlong);	else td.fStandLong	= 0;
						if( nColSpecie >= 0 )		td.sSpecie			= pSheet->readStr(row, nColSpecie);
						if( nColDiameter >= 0 )		td.fDiameter		= pSheet->readNum(row, nColDiameter);	else td.fDiameter	= 0;
						if( nColHeight >= 0 )		td.fHeight			= pSheet->readNum(row, nColHeight);		else td.fHeight		= 0;

						
						if( nColAge >= 0 )			
						{
							fTempAge=pSheet->readNum(row, nColAge);
						    td.nTreeAge=floor(fTempAge+0.5);	//Avrunda �lder, kan vara decimaltal #5479 20180212 J�
							//td.nTreeAge	= pSheet->readNum(row, nColAge);		
						}
						else 
							td.nTreeAge	= 0;
						
						if( nColBonitet >= 0 )		bonitet				= pSheet->readStr(row, nColBonitet);	if( bonitet.IsEmpty() ) bonitet.Format(_T("%.0f"), pSheet->readNum(row, nColBonitet));
						if( nColLatitude >= 0 )		td.fLatitude		= pSheet->readNum(row, nColLatitude);	else td.fLatitude	= 0;
						if( nColLongitude >= 0 )	td.fLongitude		= pSheet->readNum(row, nColLongitude);	else td.fLongitude	= 0;
						if( nColToppn >= 0 )		td.fToppn			= pSheet->readNum(row, nColToppn);		else td.fToppn		= 0;
						if( nColFasavst >= 0 )		td.fFasavst			= pSheet->readNum(row, nColFasavst);	else td.fFasavst	= 0;
						if( nColKateg >= 0 )		td.nKateg			= pSheet->readNum(row, nColKateg);		else td.nKateg		= 0;
						if( nColGkHgt >= 0 )		td.fGkHgt			= pSheet->readNum(row, nColGkHgt);		else td.fGkHgt		= 0;
						if( nColBark >= 0 )			td.nBark			= pSheet->readNum(row, nColBark);		else td.nBark		= 0;


							//Plocka bort ev mellanslag i slutet p� fastighetsnamn. #5479 20180112  HMS-17 20180514
							td.sStand.TrimRight();

							/*int nCheck=td.sStand.ReverseFind(' ');
							int nSize=td.sStand.GetLength();
							CString cTemp=_T("");
							if(nCheck==nSize-1 && nCheck>=0 && nSize>0)
							{
								do
								{
									cTemp.Format(_T("%*.*s"),nSize-1,nSize-1,td.sStand);
									td.sStand=cTemp;
									nCheck=td.sStand.ReverseFind(' ');
									nSize=td.sStand.GetLength();
								}while(nCheck==nSize-1);
							}*/

							// Map specie name
							td.nSpecieId = td.sSpecie.IsEmpty() ? -1 : m_spcmap[td.sSpecie];

							// Extract bonitet (last two digits)
							if( !bonitet.IsEmpty() ) td.fBonitet = _wtof(bonitet.Right(2));

							// Extract bonitet species 20180109 J� #5479
							if(!bonitet.IsEmpty())
							{				
								CString  cTest=bonitet.Left(1);
								if(cTest.Compare(_T("T"))==0 || cTest.Compare(_T("t"))==0)	//Tall
									td.nBonitetSpecNr=1;
								else
								{
									if(cTest.Compare(_T("G"))==0 || cTest.Compare(_T("g"))==0)	//Gran
										td.nBonitetSpecNr=2;
									else
									{
										if(cTest.Compare(_T("B"))==0 || cTest.Compare(_T("b"))==0)	//Bj�rk
											td.nBonitetSpecNr=2;
										else
										{
											if(cTest.Compare(_T("C"))==0 || cTest.Compare(_T("c"))==0)	// Contorta
												td.nBonitetSpecNr=6;
											else
											{
												if(cTest.Compare(_T("E"))==0 || cTest.Compare(_T("e"))==0)	// Ek
													td.nBonitetSpecNr=9;						
												else
												{
													if(cTest.Compare(_T("F"))==0 || cTest.Compare(_T("f"))==0)	// Bok
														td.nBonitetSpecNr=13;
													else
														td.nBonitetSpecNr=td.nSpecieId;	//S�tt till tr�dets tr�dslag om den inte hittar n�got #5479 20180212 J�
												}
											}
										}
									}
								}
							}
							else
								td.nBonitetSpecNr=td.nSpecieId;

							// Extract block unit from stand name
							if( m_wndCheck1.GetCheck() )
							{
								td.sBlock.Empty();
								td.sUnit.Empty();
								int nTest = td.sStand.ReverseFind(':');
								CString cTest=_T("");
								if(nTest>2)
								{
									//HMS-78 20220209 J� gjort om tester f�r at hitta block och enhet 
									cTest=td.sStand.Left(nTest);
									int start = cTest.ReverseFind(' ');
									//int start = td.sStand.ReverseFind(' ');							

									if( start >= 0 )
									{
										int sep = td.sStand.ReverseFind(':');
										if( sep > start )
										{
											int end = td.sStand.ReverseFind('-');
											//if( end < 0 ) end = td.sStand.GetLength();
											//HMS-68 20220208 J�
											if( end < 0 || end<sep ) end = td.sStand.GetLength();

											td.sBlock = td.sStand.Mid(start + 1, sep - start - 1);
											td.sUnit = td.sStand.Mid(sep + 1, end - sep - 1);
											CString newname = td.sStand.Mid(0, start);
											td.sStand = newname;
										}
									}
								}
							}

							// Convert m to dm
							if( m_wndComboGkhgtUnit.GetCurSel() == 0 ) // m
							{
								td.fGkHgt *= 10.0;
							}
							if(m_wndComboToppnUnit.GetCurSel() == 0 ) // m
							{
								td.fToppn *= 10.0;
							}
							if(m_wndComboFasavstUnit.GetCurSel() == 0 ) // m
							{
								td.fFasavst *= 10.0;
							}
							if( m_wndCombo13.GetCurSel() == 0 ) // m
							{
								td.fHeight *= 10.0;
							}
							if( m_wndCombo15.GetCurSel() == 0 ) // cm
							{
								td.fDiameter *= 10.0;
							}

							if( nColTypeOfTree > 0 )
							{
								// Use column data
								td.nType = m_treemap[pSheet->readStr(row, nColTypeOfTree)];
								if( td.nType == 5 && td.fHeight == 0 ) td.nType = 6; // "Kanttr�d l�mnnas (ber�knad h�jd)"
							}
							else if( m_wndCombo12.GetCurSel() == 2 )
							{
								td.nType = 3; // "Utanf�r gata (kanttr�d)"
							}
							else
							{
								td.nType = 0; // "I gata"
							}

							td.bIncluded = false;

							// Make sure required fields are filled in (age and bonitet required only for edge trees), otherwise ignore this row
							if( td.sStand.IsEmpty() || td.sSpecie.IsEmpty() || td.nType < 0 || (td.nType == TREETYPE_EDGETREE && (td.nTreeAge == 0 || td.fBonitet == 0)) )
							{
								// Add to ignore list only if row is not empty
								if( !td.sStand.IsEmpty() || !td.sBlock.IsEmpty() || !td.sUnit.IsEmpty() || !td.sSpecie.IsEmpty() ||
									td.nStandNumber || td.fDiameter || td.nTreeAge || td.fBonitet || td.fLatitude || td.fLongitude )
								{
									nIgnoredTrees++;
									if( nIgnoredTrees <= nMaxRows )
									{
										if( !ignoredRows.IsEmpty() ) ignoredRows += _T(", ");
										CString tmp; tmp.Format(_T("%d"), row+1);
										ignoredRows += tmp;
									}
									else if( nIgnoredTrees == nMaxRows + 1 )
									{
										ignoredRows += _T(", ...");
									}
								}
								continue;
							}

							// Make sure values are reasonable
							if( td.nSpecieId == 1 || td.nSpecieId == 2 )	{ minb = 10.0; maxb = 32.0; } // Tall, Gran
							else if( td.nSpecieId == 3 )					{ minb = 10.0; maxb = 40.0; } // Bj�rk
							else											{ minb = 14.0; maxb = 30.0; } // �vriga tr�dslag
							if( td.fHeight < 0.4 * td.fDiameter || td.fHeight > 1.5 * td.fDiameter || td.fGkHgt > td.fHeight || (td.fBonitet > 0 && (td.fBonitet < minb || td.fBonitet > maxb)) )
							{
								nWarningTrees++;
								if( nWarningTrees <= nMaxRows )
								{
									if( !warningRows.IsEmpty() ) warningRows += _T(", ");
									CString tmp; tmp.Format(_T("%d"), row+1);
									warningRows += tmp;
								}
								else if( nWarningTrees == nMaxRows + 1 )
								{
									warningRows += _T(", ...");
								}
							}

							// Convert lat/long to GPS encoded coordinates
							if( hInst )
							{
								// Tree coords, stand coords
								if( td.fLatitude != 0 && td.fLongitude != 0 ) ConvertToLatLong(hInst, td.fLatitude, td.fLongitude, m_wndCombo14.GetCurSel());
								if( td.fStandLat != 0 && td.fStandLong != 0 ) ConvertToLatLong(hInst, td.fStandLat, td.fStandLong, m_wndComboBestCoords.GetCurSel());
							}

							// Separate block and unit if not already done
							if( !m_wndCheck1.GetCheck() )
							{
								td.sBlock = td.sBlock.Left(td.sBlock.Find(':'));
								td.sUnit  = td.sUnit.Right(td.sUnit.GetLength() - td.sUnit.Find(':') - 1);
							}
							trees.push_back(td);
					}
				}

				AfxFreeLibrary(hInst);

				// Go through rows, stand by stand
				for( TreeDataList::iterator iter = trees.begin(); iter != trees.end(); iter++ )
				{
					if( !iter->bIncluded )
					{
						// Start new HXL file for current stand
						if( !iter->sBlock.IsEmpty() && !iter->sUnit.IsEmpty() )
						{
							if( iter->nStandNumber )
							{
								stand.Format(_T("%s %s;%s-%d"), iter->sStand, iter->sBlock, iter->sUnit, iter->nStandNumber);
							}
							else
							{
								stand.Format(_T("%s %s;%s"), iter->sStand, iter->sBlock, iter->sUnit);
							}
						}
						else
						{
							stand.Format(_T("%s"), iter->sStand);
						}						
						if(stand.Compare(_T("\\"))==0 
							|| stand.Compare(_T("/"))==0 
							|| stand.Compare(_T(":"))==0 
							|| stand.Compare(_T("*"))==0 
							|| stand.Compare(_T("?"))==0 
							//|| stand.Compare(_T("""))==0 
							|| stand.Compare(_T("<"))==0 
							|| stand.Compare(_T(">"))==0 
							|| stand.Compare(_T("|"))==0)
						{
							stand.Replace('\\', '_');
							stand.Replace('/', '_');
							stand.Replace(':', '_');
							stand.Replace('*', '_');
							stand.Replace('?', '_');
							stand.Replace('"', '_');
							stand.Replace('<', '_');
							stand.Replace('>', '_');
							stand.Replace('|', '_');
						}
						path.Format(_T("%s\\%s.HXL"), m_sOutputPath, stand);
						filelist.push_back(path);
						hxl.Open(path, CFile::modeCreate | CFile::modeWrite);
						stand.Replace(';', ':'); // Use ':' within file

						struct tm T;
						time_t t;
						time(&t);
						localtime_s(&T, &t);

						numspc = 0;
						includespc.clear();
						spcbuf.Empty();
						treebuf.Empty();

						// Find all occurences of current stand
						bIgnoreCurrent = false;
						for( TreeDataList::iterator iter2 = trees.begin(); iter2 != trees.end(); iter2++ )
						{
							if( iter2->sStand == iter->sStand &&
								iter2->sBlock == iter->sBlock &&
								iter2->sUnit == iter->sUnit &&
								iter2->nStandNumber == iter->nStandNumber &&
								iter2->nSide == iter->nSide &&
								iter2->bIncluded == false )
							{
								iter2->bIncluded = true;
								if( !bIgnoreCurrent )
								{
									// Make sure stand specific values match previous values for current stand
									if( iter2->nStandArea != iter->nStandArea ||
										iter2->nStandAge != iter->nStandAge ||
										iter2->fStandLat != iter->fStandLat ||
										iter2->fStandLong != iter->fStandLong ||
										iter2->sStandBonitet != iter->sStandBonitet ||
										iter2->nStandLength != iter->nStandLength ||
										iter2->nStandWidth != iter->nStandWidth )
									{
										bIgnoreCurrent = true;
										nIgnoredStands++;
										if( nIgnoredStands <= nMaxRows )
										{
											ignoredStands += stand + _T("\n");
										}
										else if( nIgnoredStands == nMaxRows )
										{
											ignoredStands += _T("...");
										}
										continue;
									}

									// Add specie to HXL
									if( !includespc[iter2->nSpecieId] )
									{
										buf.Format("%S,%d\n", iter2->sSpecie, iter2->nSpecieId);
										spcbuf += buf;
										includespc[iter2->nSpecieId] = true;
										numspc++;
									}
									antH=0;
									if(iter2->fHeight>0)
										antH+=1;
									if(iter2->fGkHgt>0)
										antH+=1;

									// Add tree to HXL
									buf.Format("%d,%.f,%d,%.f,%.f,%d,%d,%.f,%d,%d,%f,%f,%d,%f,%f,%d\n",
											   iter2->nSpecieId,					// SpecNr
											   floor(iter2->fDiameter + 0.5),		// DBH
											   antH,								// HgtNum
											   floor(iter2->fHeight*10.0 + 0.5),	// Hgt
											   floor(iter2->fGkHgt*10.0 + 0.5),		// Greencrown Hgt
											   iter2->nTreeAge,						// Age
											   iter2->nType,						// Type
											   floor(iter2->fBonitet*10.0 + 0.5),	// Bonitet
											   0,									// Yta
											   iter2->nBark,						// Bark
											   iter2->fLatitude,					// Lat
											   iter2->fLongitude,					// Lon
											   iter2->nKateg,						// Kategori
											   floor(iter2->fFasavst+0.5),			// Fasavst�nd
											   floor(iter2->fToppn+0.5),			// Toppningsv�rde
											   iter2->nBonitetSpecNr);			    // Bonitet tr�dslagsnummer #5479 20180109 J�
									treebuf += buf;
								}
							}
						}

						// Write header
						buf.Format("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
								   "<HXL>\n"
								   "<Document>\n"
								   "<FileDate>%04d%02d%02d</FileDate>\n"
								   "<Type>HMS Excel HXL</Type>\n"
								   "<Ver>111</Ver>\n"
								   "</Document>\n"
								   "<Object>\n"
								   "<objectData>\n"
								   "<ProgramVersion Name=\"PROGRAMVERSION\" Var=\"1\" Type=\"2\">UMLandValue %S</ProgramVersion>\n"
								   "<Tecken Name=\"CHARSET\" Var=\"1\" Type=\"3\">ISO 8859-1</Tecken>\n"
								   "<Best Name=\"STANDID\" Var=\"2\" Type=\"1\">%S</Best>\n"
								   "<Date Name=\"DATE\" Var=\"12\" Type=\"4\">%04d%02d%02d%02d%02d%02d</Date>\n"
								   "<Numsp Name=\"NUMSPEC\" Var=\"111\" Type=\"1\">%d</Numsp>\n"
								   "<SpecieSet>\n"
								   "<SpecieVar Name=\"SPECIENAME\" Var=\"120\" Type=\"1\"/>\n"
								   "<SpecieVar Name=\"SPECIENUMBER\" Var=\"120\" Type=\"3\"/>\n"
								   "<Species>%s"
								   "</Species>\n"
								   "</SpecieSet>\n"
								   "<Numplots Name=\"NUMPLOT\" Var=\"651\" Type=\"1\">1</Numplots>\n"
								   "<BestAge Name=\"BESTAGE\" Var=\"660\" Type=\"1\">%d</BestAge>\n"
								   "<Areal Name=\"AREAL\" Var=\"670\" Type=\"1\">%d</Areal>\n"
								   "<Length Name=\"LENGTH\" Var=\"670\" Type=\"2\">%d</Length>\n"
								   "<Width Name=\"WIDTH\" Var=\"670\" Type=\"3\">%d</Width>\n"
								   "<Metod Name=\"TAXMETHOD\" Var=\"2030\" Type=\"1\">5</Metod>\n"
								   "<Bonitet2 Name=\"BONITET2\" Var=\"2095\" Type=\"2\">%S</Bonitet2>\n"
								   "<BestSida Name=\"BESTSIDA\" Var=\"2104\" Type=\"1\">%d</BestSida>\n"
								   "</objectData>\n"
								   "<Point><coordinates> %.4f, %.4f</coordinates></Point>\n"
								   "<PlotSet>\n"
								   "<Plot>\n"
								   "<Id Name=\"ID\" Var=\"2052\" Type=\"1\">0</Id>\n"
								   "</Plot>\n"
								   "</PlotSet>\n"
								   "<TreeSet>\n"
								   "<TreeVar Name=\"SPECNR\" Var=\"652\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"DBH\" Var=\"653\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"HGTNUM\" Var=\"654\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"HGT\" Var=\"656\" Type=\"1\" Def=\"50\"/>\n"
								   "<TreeVar Name=\"HGT2\" Var=\"656\" Type=\"1\" Def=\"51\"/>\n"
								   "<TreeVar Name=\"AGE\" Var=\"2001\" Type=\"2\" Def=\"\"/>\n"
								   "<TreeVar Name=\"TYPE\" Var=\"2004\" Type=\"2\" Def=\"\"/>\n"
								   "<TreeVar Name=\"BONITET\" Var=\"2006\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"PLOT\" Var=\"2052\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"BARK\" Var=\"2003\" Type=\"2\" Def=\"\"/>\n"
								   "<TreeVar Name=\"LATITUDE\" Var=\"2200\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"LONGITUDE\" Var=\"2201\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"CATEGORY\" Var=\"2206\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"FASAVST\" Var=\"2207\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"TOPCUT\" Var=\"2208\" Type=\"1\" Def=\"\"/>\n"
								   "<TreeVar Name=\"BONITETSPEC\" Var=\"2006\" Type=\"2\" Def=\"\"/>"
								   "<Tree>\n",
								   T.tm_year + 1900, T.tm_mon + 1, T.tm_mday,	// FileDate
								   m_sVersion,									// ProgramVersion
								   stand,										// Best
								   T.tm_year + 1900, T.tm_mon + 1, T.tm_mday, T.tm_hour, T.tm_min, T.tm_sec, // Date
								   numspc,										// NumSp
								   spcbuf,										// Species
								   iter->nStandAge,								// BestAge
								   iter->nStandArea,							// Areal
								   iter->nStandLength,							// Length
								   iter->nStandWidth,							// Width
								   iter->sStandBonitet,							// Bonitet2
								   iter->nSide,									// BestSida
								   iter->fStandLat, iter->fStandLong);			// Point (coordinates)
						hxl.Write(buf, buf.GetLength());

						// Write trees
						hxl.Write(treebuf, treebuf.GetLength());
						
						// Write end
						buf = "</Tree>\n</TreeSet>\n</Object>\n</HXL>\n";
						hxl.Write(buf, buf.GetLength());
						hxl.Close();
					}
				}
			}

			// Delete result files in case of an error
			if( nIgnoredStands > 0 || nIgnoredTrees > 0 )
			{
				for( std::list<CString>::iterator iter = filelist.begin(); iter != filelist.end(); iter++ )
				{
					DeleteFile(*iter);
				}
			}

			// Notify user if any trees were ignored
			if( nIgnoredStands > 0 )
			{
				CString msg = xml->str(IDS_STRING3310); msg.Replace(_T("\\n"), _T("\n"));
				CString msg2; msg2.Format(msg, nIgnoredStands, ignoredStands);
				AfxMessageBox(msg2);
				ret = FALSE;
			}
			else if( nIgnoredTrees > 0 )
			{
				CString msg = xml->str(IDS_STRING3277); msg.Replace(_T("\\n"), _T("\n"));
				CString msg2; msg2.Format(msg, nIgnoredTrees, ignoredRows);
				AfxMessageBox(msg2);
				ret = FALSE;
			}
			else if( nWarningTrees > 0 )
			{
				// Unreasonable values found, ask user whether to continue or not
				CString msg = xml->str(IDS_STRING3299); msg.Replace(_T("\\n"), _T("\n"));
				CString msg2; msg2.Format(msg, nWarningTrees, warningRows);
				if( AfxMessageBox(msg2, MB_ICONEXCLAMATION|MB_YESNO) == IDNO )
				{
					// Delete result files
					for( std::list<CString>::iterator iter = filelist.begin(); iter != filelist.end(); iter++ )
					{
						DeleteFile(*iter);
					}
					ret = FALSE;
				}
			}

			// Restore locale
			setlocale(LC_ALL, lorig);
		}
	}

	return ret;
}


BEGIN_MESSAGE_MAP(CImportExcelDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK1, &CImportExcelDlg::OnBnClickedCheck1)
	ON_CBN_SELCHANGE(IDC_COMBO16, OnCbnSelchangeCombo16)
	ON_BN_CLICKED(IDC_BUTTON1, &CImportExcelDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CImportExcelDlg::OnBnClickedButton2)
	ON_CBN_SELCHANGE(IDC_COMBO7, &CImportExcelDlg::OnCbnSelchangeCombo7)
	ON_CBN_SELCHANGE(IDC_COMBO14, &CImportExcelDlg::OnCbnSelchangeCombo14)
END_MESSAGE_MAP()


// CImportExcelDlg message handlers

void CImportExcelDlg::OnBnClickedCheck1()
{
	m_wndCombo3.EnableWindow(!m_wndCheck1.GetCheck());
}

void CImportExcelDlg::OnCbnSelchangeCombo16()
{
	LoadSheet();
}

void CImportExcelDlg::OnCbnSelchangeCombo7()
{
	LoadSpecies();
}

void CImportExcelDlg::OnCbnSelchangeCombo14()
{
	LoadTreeTypes();
}

void CImportExcelDlg::OnBnClickedButton1()
{
	vecTransactionSpecies vecSpecies;
	m_pDB->getSpecies(vecSpecies);

	CMapSpeciesDlg dlg(&m_spcmap, &vecSpecies);
	dlg.DoModal();
}

void CImportExcelDlg::OnBnClickedButton2()
{
	CMapTreeTypesDlg dlg(&m_treemap);
	dlg.DoModal();
}
