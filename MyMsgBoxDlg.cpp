// MyMsgBoxDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyMsgBoxDlg.h"

#include "ResLangFileReader.h"

// CMyMsgBoxDlg dialog

IMPLEMENT_DYNAMIC(CMyMsgBoxDlg, CDialog)

BEGIN_MESSAGE_MAP(CMyMsgBoxDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON2, &CMyMsgBoxDlg::OnBnClickedButton2)
END_MESSAGE_MAP()

CMyMsgBoxDlg::CMyMsgBoxDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyMsgBoxDlg::IDD, pParent)
{

}

CMyMsgBoxDlg::~CMyMsgBoxDlg()
{
	if (m_wndPicCtrl.m_hWnd)
		m_wndPicCtrl.DestroyWindow();
}

void CMyMsgBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMyMsgBoxDlg)
	DDX_Control(pDX, IDC_LBL27_1, m_wndLbl1);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtnPrintOut);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

BOOL CMyMsgBoxDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			m_wndBtnOK.SetWindowText(xml.str(IDS_STRING4516));
			m_wndBtnCancel.SetWindowText(xml.str(IDS_STRING1161));
			m_wndBtnPrintOut.SetWindowTextW(xml.str(IDS_STRING236));

			xml.clean();
		}
	}

	int nYIxon = GetSystemMetrics(SM_CYICON);
	int nXIxon = GetSystemMetrics(SM_CXICON);
	HICON hIcon = ::LoadIcon(NULL,IDI_ASTERISK);
	if (m_wndPicCtrl.m_hWnd == NULL && hIcon)
	{
		if (m_wndPicCtrl.Create(L"",WS_CHILD|WS_VISIBLE|SS_ICON|SS_CENTERIMAGE,CRect(15,15,nYIxon+15,nXIxon+15),this))
			m_wndPicCtrl.SetIcon(hIcon);
	}

	this->m_cHTML.CreateFromStatic(IDC_HTML_TEXT,this);

	SetWindowText(m_sCaption);
	m_wndLbl1.SetWindowText(m_sLbl1Text);

	return TRUE;
}

// CMyMsgBoxDlg message handlers

void CMyMsgBoxDlg::startHTML(void)
{
	m_sHTMLText = L"";
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<html><body style=\"border-style:none\" \"><table width=\"100%%\">");
//	csHTML.Format(L"<html><body \"><table width=\"100%%\" border=\"0\">");
	m_sHTMLText = csHTML;
	
}



void CMyMsgBoxDlg::addHeader(LPCTSTR text,short font_size,bool bold)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	if (bold)
		csHTML.Format(L"<tr><td style=\"font-size:%dpt\"><b>%s</b></td></tr>",font_size,text);
	else
		csHTML.Format(L"<tr><td style=\"font-size:%dpt\">%s</td></tr>",font_size,text);
	m_sHTMLText += csHTML;
}

void CMyMsgBoxDlg::addTable()
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<table border=\"0\" width=\"100%%\">");
	m_sHTMLText += csHTML;
}

void CMyMsgBoxDlg::endTable()
{
	m_sHTMLText += L"</table>";
}

void CMyMsgBoxDlg::addTableBR(short font_size)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	csHTML.Format(L"<tr><td style=\"font-size:%dpt\"><br/></td></tr>",font_size);
	m_sHTMLText += csHTML;
}

void CMyMsgBoxDlg::addTableHR(short font_size,HR_STYLE hrStyle)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	if (hrStyle == HR_SOLID)
		csHTML.Format(L"<tr><td><hr style=\"border-style:solid;color:#666666;height:%d;\"/></td></tr>",font_size);
	else if (hrStyle == HR_DASHED)
		csHTML.Format(L"<tr><td><hr style=\"border-style:dashed;color:#666666;height:%d;\"/></td></tr>",font_size);
	else if (hrStyle == HR_DOTTED)
		csHTML.Format(L"<tr><td><hr style=\"border-style:dotted;color:#666666;height:%d;\"/></td></tr>",font_size);
	m_sHTMLText += csHTML;
}

void CMyMsgBoxDlg::addTableText(LPCTSTR text,short font_size,bool bold)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	if (bold)
		csHTML.Format(L"<tr><td style=\"font-size:%dpt\"><b>%s</b></td></tr>",font_size,text);
	else
		csHTML.Format(L"<tr><td style=\"font-size:%dpt\">%s</td></tr>",font_size,text);
	m_sHTMLText += csHTML;
}

void CMyMsgBoxDlg::addTableColText(LPCTSTR text,short font_size,bool bold,int w)
{
	CString csHTML = L"";
	// Preparing the HTML page to be shown...
	if (bold)
		csHTML.Format(L"<td width=\"%d\" style=\"font-size:%dpt\"><b>%s</b></td>",w,font_size,text);
	else
		csHTML.Format(L"<td width=\"%d\" style=\"font-size:%dpt\">%s</td>",w,font_size,text);
	m_sHTMLText += csHTML;
}

void CMyMsgBoxDlg::addTableCol()
{
	m_sHTMLText += L"<tr>";
}

void CMyMsgBoxDlg::endTableCol()
{
	m_sHTMLText += L"</tr>";
}

void CMyMsgBoxDlg::endHTML(void)
{
	m_sHTMLText += L"</table></body></html>";
}

void CMyMsgBoxDlg::showHTML(void)
{
	this->m_cHTML.SetNewHTMLContent(m_sHTMLText,true);
}



void CMyMsgBoxDlg::OnBnClickedButton2()
{
	print();
}
