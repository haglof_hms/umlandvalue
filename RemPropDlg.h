#pragma once

#include "Resource.h"

//Lagt till en dialog f�r fr�ga om fastighet(erna) skall tas bort feature #2453 20111114 J�
//Ja och nej samt ja nej till alla

class CRemPropDlg : public CDialog
{
	DECLARE_DYNAMIC(CRemPropDlg)


	CMyExtStatic m_wndText;
	bool bChecked;
	CButton m_btnYes;
	CButton m_btnNo;
	CButton m_wndCBox1;
	CString csText;
		void setLanguage(void);
public:
	//CRemPropDlg(CWnd* pParent = NULL);   // standard constructor
	CRemPropDlg(CWnd* pParent = NULL,bool bDoAll=false,bool bToAll=false,CString strText=_T(""));
	virtual ~CRemPropDlg();
	bool OnAll();
// Dialog Data
	enum { IDD = IDD_DIALOG28 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddChangeObjIDDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();	
	afx_msg void OnBnChecked();
};