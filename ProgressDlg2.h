#pragma once

#include "Resource.h"

// CProgressDlg2 dialog

class CProgressDlg2 : public CDialog
{
	DECLARE_DYNAMIC(CProgressDlg2)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CProgressCtrl m_wndProgress29_1;

	CBrush m_brush;

public:
	CProgressDlg2(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDlg2();

// Dialog Data
	enum { IDD = IDD_DIALOG29 };

	inline void setProgressPos(int step)
	{
		if (m_wndProgress29_1.GetSafeHwnd() != NULL)
		{
			m_wndProgress29_1.SetPos(step);
			//m_wndProgress29_1.Invalidate();
		}
	}


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);


	DECLARE_MESSAGE_MAP()
};
