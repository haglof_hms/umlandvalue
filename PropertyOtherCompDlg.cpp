// PropertyOtherCompDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PropertyOtherCompDlg.h"

#include "ResLangFileReader.h"

// CPropertyOtherCompDlg dialog

IMPLEMENT_DYNAMIC(CPropertyOtherCompDlg, CDialog)

BEGIN_MESSAGE_MAP(CPropertyOtherCompDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPropertyOtherCompDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CPropertyOtherCompDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CPropertyOtherCompDlg::OnBnClickedButton2)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_OTHER_COMP, OnReportValueChanged)
END_MESSAGE_MAP()


CPropertyOtherCompDlg::CPropertyOtherCompDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropertyOtherCompDlg::IDD, pParent)
{
	m_pDB = NULL;
	m_fSumOtherComp = 0.0;
	m_bInitialized = FALSE;
}

CPropertyOtherCompDlg::~CPropertyOtherCompDlg()
{
	m_vecOtherComp.clear();
	m_sarrTypes.RemoveAll();
}

// CPropertyOtherCompDlg message handlers

void CPropertyOtherCompDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL_OCOMP1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_OCOMP2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_OCOMP3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_OCOMP4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL_OCOMP5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL_OCOMP6, m_wndLbl6);

	DDX_Control(pDX, IDC_BUTTON1, m_wndAddRowBtn);
	DDX_Control(pDX, IDC_BUTTON2, m_wndDelRowBtn);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP
}

BOOL CPropertyOtherCompDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();
	
		getPropertyOtherComp();

		populateData();

		// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
		CTransaction_elv_object *pObj = getActiveObject();
		// Get lastest data on Object, from database; 080520 p�d
		m_pDB->getObject(pObj->getObjID_pk(),m_recObject);

		m_bInitialized = TRUE;
	}

	return TRUE;
}

void CPropertyOtherCompDlg::OnReportValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	double fValue_entered = 0.0;
	double fValue_vat = 0.0;
	double fValue_calc = 0.0;
	int nTypeOfIndex = -1;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportColumn *pCol0 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_1);
		if (pCol0 != NULL)
		{
			if (pItemNotify->pColumn->GetIndex() == pCol0->GetIndex())
			{
				CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
				if (pRecItem != NULL)
				{
					CPropertiesOtherCompDataRec *pRec = (CPropertiesOtherCompDataRec*)pRecItem->GetRecord();
					nTypeOfIndex = findTypeOf(pRec->getColumnText(COLUMN_0));
					if (nTypeOfIndex > -1 && nTypeOfIndex < m_sarrTypes.GetCount())
					{
						// Check typeof: if typeof = 1 ("Intr�ngsers�ttning"), we need to
						// check percent on "Frivillig uppg�relse"; 080520 p�d
						if (nTypeOfIndex == 0)
							fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
						else if (nTypeOfIndex == 1)	// "Fast ers�ttning + moms"
						{
							fValue_entered = pRec->getColumnFloat(COLUMN_1);
							fValue_vat = fValue_entered * m_recObject.getObjVAT()/100.0;
							fValue_calc = fValue_entered + fValue_vat;
						}
						else if (nTypeOfIndex == 2)	// "Intr�ngsers�ttning"
						{
							fValue_entered = pRec->getColumnFloat(COLUMN_1);
							fValue_calc = fValue_entered * m_recObject.getObjPercent()/100.0;
						}
						else if (nTypeOfIndex == 3)	// "kr per m3 gagnvirke"  #4083 20140625 J�
						{
							fValue_entered = pRec->getColumnFloat(COLUMN_1);
							fValue_calc = fValue_entered * m_recProperty.getPropWoodVolume();
						}
						else if (nTypeOfIndex == 4) // ers�ttning f�r hinder i �kermark HMS-56 20200608 J�
						{
							fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
						}
						else if (nTypeOfIndex == 5) // ers�ttning f�r ledning i skogsmark HMS-56 20200608 J�
						{
							fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
						}
						else if (nTypeOfIndex == 6) // ers�ttning f�r �vrigt intr�ng HMS-56 20200608 J�
						{
							fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
						}


						pRec->setColumnFloat(COLUMN_2,fValue_calc);
						m_wndReport.Populate();

					}	// if (nTypeOfIndex > -1 && nTypeOfIndex < m_sarrTypes.GetCount())
				}	// if (pRecItem != NULL)
			}	// if (pItemNotify->pColumn->GetIndex() == pCol0->GetIndex())
		}	// if (pCol0 != NULL)
	}	// if (pItemNotify != NULL)
}

// CPropertyOtherCompDlg message handlers

// PRIVATE
void CPropertyOtherCompDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_OTHER_COMP, FALSE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText((xml.str(IDS_STRING424)));

				m_wndLbl1.SetWindowText((xml.str(IDS_STRING204)));
				m_wndLbl2.SetWindowText((xml.str(IDS_STRING3203)));
				m_wndLbl3.SetWindowText((xml.str(IDS_STRING3204)));
				m_wndLbl4.SetLblFontEx(-1,FW_BOLD);
				m_wndLbl5.SetLblFontEx(-1,FW_BOLD);
				m_wndLbl6.SetLblFontEx(-1,FW_BOLD);

				m_wndAddRowBtn.SetWindowText((xml.str(IDS_STRING4242)));
				m_wndDelRowBtn.SetWindowText((xml.str(IDS_STRING4243)));

				m_wndCancelBtn.SetWindowText((xml.str(IDS_STRING4249)));
				m_wndOKBtn.SetWindowText((xml.str(IDS_STRING4244)));

				m_sMsgCap = (xml.str(IDS_STRING229));
				m_sMsgRemoveOtherCompEntry = (xml.str(IDS_STRING4246));

				m_sLogAddOtherCompMsg = (xml.str(IDS_STRING3501));
				m_sLogDelOtherCompMsg = (xml.str(IDS_STRING3502));
				
				m_sarrTypes.Add((xml.str(IDS_STRING42330)));
				m_sarrTypes.Add((xml.str(IDS_STRING42331)));
				m_sarrTypes.Add((xml.str(IDS_STRING42332)));
				m_sarrTypes.Add((xml.str(IDS_STRING42333))); // #4083 20140625 J� lagt till ers�ttningstyp

				m_sarrTypes.Add((xml.str(IDS_STRING42334))); // HMS-56 nya typer av annan ers�ttning 20200608 J� Hinder i �kermark
				m_sarrTypes.Add((xml.str(IDS_STRING42335))); // HMS-56 nya typer av annan ers�ttning 20200608 J� ledning i skogsmark
				m_sarrTypes.Add((xml.str(IDS_STRING42336))); // HMS-56 nya typer av annan ers�ttning 20200608 J� �vrigt intr�ng
				
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING4248)), 100));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING4247)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING4240)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING4241)), 200));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 255;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_LEFT);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_REPORT_OTHER_COMP),10,80,rect.right - 20,rect.bottom - 120);
			}
		}	// if (fileExists(sLangFN))

	}
}

void CPropertyOtherCompDlg::getPropertyOtherComp(void)
{
	if (m_pDB != NULL)
	{
		// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
		CTransaction_elv_object *pObj = getActiveObject();

		m_pDB->getPropertyOtherComp(pObj->getObjID_pk(),
															  m_recProperty.getPropID_pk(),														
															  m_vecOtherComp);
		pObj = NULL;
	}
}

void CPropertyOtherCompDlg::savePropertyOtherComp(void)
{
	vecTransaction_elv_properties_logbook vecLogBook;
	CTransaction_elv_properties_logbook recLogBook;
	CTransaction_elv_properties_other_comp recOtherComp;
	CPropertiesOtherCompDataRec *pRec = NULL;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	int nTypeOfIndex = -1;
	int nIndex = 1;
	double fValue_entered = 0.0;
	double fValue_vat = 0.0;
	double fValue_calc = 0.0;
	BOOL bAddToLog = FALSE;
	if (pRecs != NULL && m_pDB != NULL)
	{
		// Get entries in logbook for Objekt and property; 100819 p�d
		m_pDB->getPropertyLogBook(m_recProperty.getPropID_pk(),m_recObject.getObjID_pk(),vecLogBook);

		// Remove all entries for "Annan ers�ttning", for this Object and Property; 100617 p�d
		m_pDB->delPropertyOtherComp(m_recObject.getObjID_pk(),m_recProperty.getPropID_pk());
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			pRec = (CPropertiesOtherCompDataRec *)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				nTypeOfIndex = findTypeOf(pRec->getColumnText(COLUMN_0));
				// Check typeof: if typeof = 1 ("Intr�ngsers�ttning"), we need to
				// check percent on "Frivillig uppg�relse"; 080520 p�d
				if (nTypeOfIndex == 0)
					fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
				else if (nTypeOfIndex == 1)	// "Fast ers�ttning + moms"
				{
					fValue_entered = pRec->getColumnFloat(COLUMN_1);
					fValue_vat = fValue_entered * m_recObject.getObjVAT()/100.0;
					fValue_calc = fValue_entered + fValue_vat;
				}
				else if (nTypeOfIndex == 2)	// "Intr�ngsers�ttning"
				{
					fValue_entered = pRec->getColumnFloat(COLUMN_1);
					fValue_calc = fValue_entered * m_recObject.getObjPercent()/100.0;
				}
				else if (nTypeOfIndex == 3)	// "kr per gagnvirkesvolym"
				{
					fValue_entered = pRec->getColumnFloat(COLUMN_1);
					fValue_calc = fValue_entered * m_recProperty.getPropWoodVolume();
				}
				else if (nTypeOfIndex == 4) // ers�ttning f�r hinder i �kermark HMS-56 20200608 J�
				{
					fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
				}
				else if (nTypeOfIndex == 5) // ers�ttning f�r ledning i skogsmark HMS-56 20200608 J�
				{
					fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
				}
				else if (nTypeOfIndex == 6) // ers�ttning f�r �vrigt intr�ng HMS-56 20200608 J�
				{
					fValue_entered = fValue_calc = pRec->getColumnFloat(COLUMN_1);
				}
				else
				{
					fValue_entered = 0.0;
					fValue_calc = 0.0;
				}
				// Check index, if index = -1, get index from method: getPropertyOtherCompNextIndex(...); 090306 p�d
				if (pRec->getIndex() == -1 && m_pDB != NULL)
					nIndex = m_pDB->getPropertyOtherCompNextIndex(m_recObject.getObjID_pk(),m_recProperty.getPropID_pk());
				else
					nIndex = pRec->getIndex();
				recOtherComp = CTransaction_elv_properties_other_comp(nIndex,
																															m_recObject.getObjID_pk(),
																															m_recProperty.getPropID_pk(),
																															fValue_entered,
																															fValue_calc,
																															pRec->getColumnText(COLUMN_3),
																															nTypeOfIndex);

				if (!m_pDB->addPropertyOtherComp(recOtherComp))
					m_pDB->updPropertyOtherComp(recOtherComp);

				bAddToLog = TRUE;
				//------------------------------------------------------------------------
				// Check if item already entered, i.e. same text. If so, do not add to Log; 100819 p�d
				for (UINT ii = 0;ii < vecLogBook.size();ii++)
				{
					recLogBook = vecLogBook[ii];
					if (recLogBook.getPropLogBookNote().CompareNoCase(m_sLogAddOtherCompMsg + L": " + pRec->getColumnText(COLUMN_3)) == 0)
					{
						bAddToLog = FALSE;
						break;
					}
				}	// for (UINT ii = 0;ii < vecLogBook.size();ii++)

				if (bAddToLog && logThis(1))
				{
					//------------------------------------------------------------------------
					// Add information to logbook that other compensation been added; 100812 p�d
					recLogBook = CTransaction_elv_properties_logbook(m_recProperty.getPropID_pk(),
																													 m_recProperty.getPropObjectID_pk(),
																													 getDateTimeEx2(),
																													 getUserName().MakeUpper(),
																													 m_sLogAddOtherCompMsg + L": " + pRec->getColumnText(COLUMN_3),
																													 getDateTime(),
																													 -2);

					m_pDB->addPropertyLogBook(recLogBook);
				}	// if (bAddToLog)

				//------------------------------------------------------------------------


			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)
}

void CPropertyOtherCompDlg::delEntryInPropertyOtherComp(CTransaction_elv_properties_other_comp &rec)
{
	if (m_pDB != NULL)
	{
		m_pDB->delPropertyOtherComp(rec.getPropOtherCompID_pk(),
																rec.getPropOtherCompObjectID_pk(),
																rec.getPropOtherCompPropID_pk());
	}	// if (m_pDB != NULL)
}

void CPropertyOtherCompDlg::populateData()
{
	m_wndLbl4.SetWindowText((m_sObjectName));
	m_wndLbl5.SetWindowText((m_sPropName));
	m_wndLbl6.SetWindowText((m_sPropNum));

	CTransaction_elv_properties_other_comp rec;
	m_wndReport.ResetContent();
	if (m_vecOtherComp.size() > 0)
	{
		for (UINT i = 0;i < m_vecOtherComp.size();i++)
		{
			addConstraints();
			rec = m_vecOtherComp[i];
			m_wndReport.AddRecord(new CPropertiesOtherCompDataRec(rec.getPropOtherCompID_pk(),m_sarrTypes,rec));
			CPropertiesOtherCompDataRec rec;
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CPropertyOtherCompDlg::addConstraints(void)
{
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport.GetColumns();
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pColumns != NULL)
	{
		// Add type of evaluation stand: "V�rdering","V�rdering startar" etc
		CXTPReportColumn *pCol2 = pColumns->Find(COLUMN_0);
		if (pCol2 != NULL)
		{
			// Get constraints for column and remove all items; 071109 p�d
			pCons = pCol2->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 070509 p�d
			if (m_sarrTypes.GetCount() > 0)
			{
				for (int i = 0;i < m_sarrTypes.GetCount();i++)
				{
					pCol2->GetEditOptions()->AddConstraint((m_sarrTypes.GetAt(i)),i);
				}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
			}	// if (m_arrTypeOfVStand.GetCount() > 0)
		}	// if (pCol0 != NULL)
	}	// if (pColumns != NULL)
}

int CPropertyOtherCompDlg::findTypeOf(LPCTSTR type_of)
{
	CString sArr;
	if (m_sarrTypes.GetCount() > 0)
	{
		for (int i = 0;i < m_sarrTypes.GetCount();i++)
		{
			sArr = m_sarrTypes.GetAt(i);

			if (sArr.CompareNoCase(type_of) == 0)
				return i;
		}	// for (int i = 0;i < m_arrTypeOfVStand.GetCount();i++)
	}	// if (m_arrTypeOfVStand.GetCount() > 0)

	return -1;	// No match
}


void CPropertyOtherCompDlg::OnBnClickedButton1()
{
	CTransaction_elv_properties_other_comp recOtherComp;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (m_wndReport.GetSafeHwnd())
	{
		addConstraints();
		m_wndReport.AddRecord(new CPropertiesOtherCompDataRec());
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}

void CPropertyOtherCompDlg::OnBnClickedButton2()
{
	CTransaction_elv_properties_logbook recLogBook;
	// First ask user if he realy want to remove entry; 080423 p�d
	if(::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveOtherCompEntry),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		return;

	CPropertiesOtherCompDataRec *pRec = (CPropertiesOtherCompDataRec*)m_wndReport.GetFocusedRow()->GetRecord();
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL && pRecs != NULL && pRec != NULL && logThis(1))
	{
		//------------------------------------------------------------------------
		// Add information to logbook that other compensation been deleted; 100812 p�d
		recLogBook = CTransaction_elv_properties_logbook(pRec->getRecord().getPropOtherCompPropID_pk(),
																										 pRec->getRecord().getPropOtherCompObjectID_pk(),
																										 getDateTimeEx2(),
																										 getUserName().MakeUpper(),
																										 m_sLogDelOtherCompMsg + L": " + pRec->getRecord().getPropOtherCompNote(),
																										 getDateTime(),
																										 -2);
		//K�r en koll h�r s� att n�r du tar bort en annan ers�ttning som itne �r sparad s� g�rs ingen anteckning, annars blir det ett felmedellande.
		//20111121 Bug #2563 J�
		if(recLogBook.getPropLogBookObjectID_pk()==-1 && recLogBook.getPropLogBookID_pk()==-1)
		{}
		else
		{
			if (m_pDB)
				m_pDB->addPropertyLogBook(recLogBook);
		}

		//------------------------------------------------------------------------

		m_wndReport.RemoveRowEx(pRow);
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
}

void CPropertyOtherCompDlg::OnBnClickedOk()
{
	m_fSumOtherComp = 0.0;
	// First we save data, just to be sure; 090306 p�d
	savePropertyOtherComp();
	// Next we repopulate the saved data; 090306 p�d
	getPropertyOtherComp();
	// We check if there's any data for "annan ers�ttning"; 090306 p�d
	if (m_vecOtherComp.size() > 0)
	{
		// Sum. compensation for "annan ers�ttning"; 090306 p�d
		for (UINT i = 0;i < m_vecOtherComp.size();i++)
		{
			// If item was added to DB, do aggregare infomation; 080516 p�d
			m_fSumOtherComp += m_vecOtherComp[i].getPropOtherCompValue_calc();
		}	// for (UINT i = 0;i < m_vecOtherComp.size();i++)
	}	// if (m_vecOtherComp.size() > 0)

	OnOK();
}
