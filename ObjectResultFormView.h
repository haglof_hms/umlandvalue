#pragma once

#include "Resource.h"

#include "ResLangFileReader.h"

class CObjectReportView : public CXTPReportView
{
	DECLARE_DYNCREATE(CObjectReportView)
public:
	CObjectReportView();
	virtual ~CObjectReportView();

	void doPrintReport(void);

protected:
	//{{AFX_MSG(CObjectResultView)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

///////////////////////////////////////////////////////////////////////////////
// CObjectResultFormView form view

class CObjectResultFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CObjectResultFormView)

	struct _data
	{
		double _dcls;
		vecString _vec_str;
	};

	// This vector'll holds specie id per column; 080528 p�d
	vecInt _vec_spc;

	BOOL m_bInitialized;

	RLFReader m_xml;

	CString m_sLangFN;
	CObjectReportView *m_pReportView;

	CString m_sSelectedLandOwnerNumber;

	CString m_sSQLTraktIDs;
	CString m_sSQLTraktIDs_assorts;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	int m_nActiveReport;	// In Summarized data tab; 080527 p�d

	_data recDclsColumns;
	std::vector<_data> vecDclsColumns;

protected:

public:
	CObjectResultFormView();           // protected constructor used by dynamic creation
	virtual ~CObjectResultFormView();
	enum { IDD = IDD_FORMVIEW7 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	void setupSLenReport(void);
	void setupEvaluationReport(void);
	void setupRotpostReport(void);
	
	void setupSQLTraktIDs(LPCTSTR lnum);

	virtual void printReport(void);
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjectResultFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDI1950ForrestNormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
//	afx_msg void OnClose();
//	afx_msg void OnSetFocus(CWnd *);
//	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


