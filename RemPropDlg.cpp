#include "stdafx.h"
#include "RemPropDlg.h"
#include "ResLangFileReader.h"

//Lagt till en dialog f�r fr�ga om fastighet(erna) skall tas bort feature #2453 20111114 J�
//Ja och nej samt ja nej till alla

IMPLEMENT_DYNAMIC(CRemPropDlg, CDialog)


BEGIN_MESSAGE_MAP(CRemPropDlg, CDialog)
	ON_BN_CLICKED(IDYES, &CRemPropDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDNO, &CRemPropDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK28_1, &CRemPropDlg::OnBnChecked)
END_MESSAGE_MAP()

//CRemPropDlg::CRemPropDlg(CWnd* pParent /*=NULL*/)
//  : CDialog(CRemPropDlg::IDD, pParent)
//{
//}

CRemPropDlg::CRemPropDlg(CWnd* pParent /*=NULL*/,bool bDoAll,bool bToAll,CString strText)
: CDialog(CRemPropDlg::IDD, pParent)
{
	bChecked=false;
	if(bDoAll)
	{
		m_wndCBox1.EnableWindow(1);	
		if(bToAll)
			m_wndCBox1.SetCheck(1);
		else
			m_wndCBox1.SetCheck(0);
	}
	else
		m_wndCBox1.EnableWindow(0);	
	csText=strText;
	
};

CRemPropDlg::~CRemPropDlg()
{
}

void CRemPropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRemPropDlg)
	DDX_Control(pDX, IDC_STATIC28_1, m_wndText);
	DDX_Control(pDX, IDC_CHECK28_1, m_wndCBox1);
	DDX_Control(pDX, IDOK, m_btnYes);
	DDX_Control(pDX, IDCANCEL, m_btnNo);
	//}}AFX_DATA_MAP

}

BOOL CRemPropDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	setLanguage();

	return TRUE;
}

// CRemPropDlg message handlers

void CRemPropDlg::setLanguage(void)
{
	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	m_wndText.SetWindowText(csText);
	
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING340)));			
			m_wndCBox1.SetWindowText((xml.str(IDS_STRING341)));
			m_btnYes.SetWindowText((xml.str(IDS_STRING110)));
			m_btnNo.SetWindowText((xml.str(IDS_STRING111)));
		}
		xml.clean();
	}
}

void CRemPropDlg::OnBnClickedOk()
{
	OnOK();
}

void CRemPropDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CRemPropDlg::OnBnChecked()
{
	int nRet=m_wndCBox1.GetCheck();
	if(nRet)
		bChecked=true;
	else
		bChecked=false;
}

bool CRemPropDlg::OnAll()
{
	return bChecked;
}
