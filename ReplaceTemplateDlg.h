#pragma once

#include "Resource.h"

// CReplaceTemplateDlg dialog

class CReplaceTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CReplaceTemplateDlg)

	CString m_sPricelistErrorStatus;
	CString m_sCostErrorStatus;
	CString m_sErrorStatus;

	CString m_sLangFN;

	CComboBox	m_wndCB1;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CXTListCtrl m_listCtrl;
	CXTListCtrl m_listCtrl2;
	//CListBox m_wndLB2;
	CXHTMLStatic m_Description;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CString m_sObjectName;
	vecTransactionTemplate m_vecTemplates;
	CTransaction_template m_recSelectedTemplate;

	CUMLandValueDB *m_pDB;
	
	int m_nObjID;

	vecTransaction_elv_properties m_vecProps;
	vecTransaction_elv_properties m_vecProps_change_template;
	CTransaction_elv_properties recProps;
	void getPropertiesFromDB(void);
	CTransaction_elv_properties& getProps(int prop_id);
	


public:
	CReplaceTemplateDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CReplaceTemplateDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG19 };

	void setObjectName(LPCTSTR s,int obj_id)	{ m_sObjectName = s; m_nObjID = obj_id; }
	void setTemplates(vecTransactionTemplate &v)	{ m_vecTemplates = v; }
	CTransaction_template& getSelectedTemplate(void)	{ return m_recSelectedTemplate; }
	vecTransaction_elv_properties& getPropertiesToChangeTemplate(void)	{ return m_vecProps_change_template; }

	void setDBConnection(CUMLandValueDB *db)	{	m_pDB = db;	}
	int CheckStandTemplate(CTransaction_template recTmpl);
protected:
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClicked31();
	afx_msg void OnBnClicked32();
	afx_msg void OnBnClicked33();
	afx_msg void OnBnClicked34();
	afx_msg void OnCbnSelchangeList31();
	afx_msg void OnBnClickedOk();
};
