#pragma once

// GlobalSearch form view

class GlobalSearch : public CXTResizeFormView
{
	DECLARE_DYNCREATE(GlobalSearch)

	//Labels
	CMyExtStatic m_wndLblSearch;

	//EditText
	CMyExtEdit m_wndEditSearch;

	CMyReportCtrl m_wndReport;
	
	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CString m_sLangFN;

	std::vector<searchResult> results;

protected:
	GlobalSearch();           // protected constructor used by dynamic creation
	virtual ~GlobalSearch();
	void populateReport();
	void setupReport();

public:
	enum { IDD = IDD_GLOBALSEARCH };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL OnInitDialog();
	afx_msg void OnEnChangeEdit1();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg	void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
	
};

class CXTPSearchReportRecord : public CXTPReportRecord
{
public:
	CXTPSearchReportRecord(searchResult r);
	searchResult getSearchResult() { return result; };
private:
	searchResult result;

};


