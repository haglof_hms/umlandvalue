// SelectTemplateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectTemplateDlg.h"

#include "ResLangFileReader.h"

// CSelectTemplateDlg dialog

IMPLEMENT_DYNAMIC(CSelectTemplateDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CSelectTemplateDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, &CSelectTemplateDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CSelectTemplateDlg::CSelectTemplateDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CSelectTemplateDlg::IDD, pParent)
{

}

CSelectTemplateDlg::~CSelectTemplateDlg()
{
}

void CSelectTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LIST3, m_wndLCtrl);

	DDX_Control(pDX, IDC_GROUP_TEMPL, m_wndGroup_templ);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

BOOL CSelectTemplateDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		// Enable Office XP themes.
		//XTThemeManager()->SetTheme(xtThemeOfficeXP);

		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING2600)));

			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING2601)));
			m_wndCancelBtn.SetWindowText((xml->str(IDS_STRING2602)));
			m_sMsgCap = (xml->str(IDS_STRING229));
			m_sMsgNoSelection = (xml->str(IDS_STRING2603));

			m_wndLCtrl.InsertColumn(0,(xml->str(IDS_STRING2500)),LVCFMT_LEFT,100);
			m_wndLCtrl.InsertColumn(1,(xml->str(IDS_STRING2501)),LVCFMT_LEFT,100);
			m_wndLCtrl.InsertColumn(2,(xml->str(IDS_STRING2507)),LVCFMT_LEFT,110);
		}
		delete xml;
	}

	m_wndLCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndLCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_Header.SubclassWindow(hWndHeader);
	m_Header.SetTheme(new CXTHeaderCtrlThemeOfficeXP());
	CXTPPaintManager::SetTheme(xtpThemeOfficeXP);

	setupList();

	return TRUE;
}

// CSelectTemplateDlg message handlers


void CSelectTemplateDlg::setupList(void)
{
	int nIndex = -1;

	CTransaction_template rec;
	m_wndLCtrl.DeleteAllItems();
	// Make sure there's any data to work with; 080404 p�d
	if (m_vecTemplates.size() > 0)
	{	
		for (UINT i = 0;i < m_vecTemplates.size();i++)
		{
			rec = m_vecTemplates[i];
			InsertRow(m_wndLCtrl,i,3,(rec.getTemplateName()),(rec.getCreatedBy()),(rec.getCreated()));
			// Try to match the name in m_sActiveTmplName to a 
			// name in the list; 090303 p�d
			if (rec.getTemplateName().CompareNoCase(m_sActiveTmplName) == 0) 
				nIndex = i;
		}	// for (UINT i = 0;i < m_vecTemplates.size();i++)

		if (nIndex != -1)
		{
			m_wndLCtrl.SetSelectionMark(nIndex);
			m_wndLCtrl.SetItemState(nIndex,LVIS_SELECTED,LVIS_SELECTED|LVIS_FOCUSED);
			m_wndLCtrl.EnsureVisible(nIndex,FALSE);
		}		

	}	// if (m_vecTemplates.size() > 0)

}


void CSelectTemplateDlg::OnBnClickedOk()
{
	int nSelectedItem = GetSelectedItem(m_wndLCtrl);
	if (nSelectedItem > -1)
	{
		m_recSelectedTemplate = m_vecTemplates[nSelectedItem];
		OnOK();
	}	// if (nSelectedItem > -1)
	else
		::MessageBox(this->GetSafeHwnd(),(m_sMsgNoSelection),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);

}
