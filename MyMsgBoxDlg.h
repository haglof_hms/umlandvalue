#pragma once

#include "htmlctrl.h"

#include "Resource.h"

// CMyMsgBoxDlg dialog

class CMyMsgBoxDlg : public CDialog
{
	DECLARE_DYNAMIC(CMyMsgBoxDlg)

	CHTMLCtrl m_cHTML;

	CString m_sHTMLText;

	CMyExtStatic m_wndLbl1;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
	CButton m_wndBtnPrintOut;

	CString m_sCaption;
	CString m_sLbl1Text;
public:

	enum HR_STYLE { HR_SOLID,HR_DASHED,HR_DOTTED};

	void startHTML(void);
	void addHeader(LPCTSTR text,short font_size=10,bool bold=false);
	void addTable();
	void endTable();
	void addTableBR(short font_size=10);
	void addTableHR(short font_size=10,HR_STYLE hrStyle=HR_SOLID);
	void addTableText(LPCTSTR text,short font_size=10,bool bold=false);
	void addTableColText(LPCTSTR text,short font_size=10,bool bold=false,int w=0);
	void addTableCol();
	void endTableCol();

	int ShowDialog()	{ endHTML(); showHTML(); return DoModal(); }

	inline void print()	{ m_cHTML.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER, NULL, NULL); }


	inline void setCaption(LPCTSTR text)	{ m_sCaption = text; }
	inline void setLbl1Text(LPCTSTR text)	{ m_sLbl1Text = text; }
protected:
	void endHTML(void);
	void showHTML(void);

	CStatic m_wndPicCtrl;
public:
	CMyMsgBoxDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMyMsgBoxDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG27 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
};
