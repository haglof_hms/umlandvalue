// InfoDlgBar.cpp : implementation file
//

#include "stdafx.h"
#include "InfoDlgBar.h"


// CInfoDlgBar dialog

IMPLEMENT_DYNAMIC(CInfoDlgBar, CDialogBar)

BEGIN_MESSAGE_MAP(CInfoDlgBar, CDialogBar)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CInfoDlgBar::CInfoDlgBar(CWnd* pParent /*=NULL*/)
	: CDialogBar()
{

	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);

	m_bClearWindow = FALSE;

}

CInfoDlgBar::~CInfoDlgBar()
{
}

void CInfoDlgBar::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();

	CDialogBar::OnDestroy();
}

void CInfoDlgBar::DoDataExchange(CDataExchange* pDX)
{
	CDialogBar::DoDataExchange(pDX);
}

// CInfoDlgBar message handlers

BOOL CInfoDlgBar::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}

void CInfoDlgBar::OnPaint()
{
	CString sObjectID;
	CString sObjectDate;
	CString sPropAreal;
	int nTextInX_object_id_cap = 0;
	int nTextInX_object_id = 0;
	int nTextInX_object_date_cap = 0;
	int nTextInX_object_date = 0;
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	int nTextInX_prop_areal = 0;
	int nTextInX_prop_areal_value = 0;
	int nPos = 0;

	CString sCap1 = L"";
	CString sCap2 = L"";
	TEXTMETRIC tm;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

//	clip.DeflateRect(1,1);
	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-2,10,10);

	//----------------------------------------------------------
	// Object information; 080428 p�d
	nTextInX_object_id_cap = 10;
	pDC->SelectObject(&m_fnt1);
	pDC->GetTextMetrics(&tm);
	pDC->TextOut(nTextInX_object_id_cap,10,(m_sObjID));

	nTextInX_object_id = nTextInX_object_id_cap + m_sObjID.GetLength()*tm.tmAveCharWidth+tm.tmAveCharWidth;
	pDC->SelectObject(&m_fnt2);
	if (!m_bClearWindow && recObject.getObjID_pk() > -1)
		sObjectID.Format(_T("%d"),recObject.getObjID_pk());
	else
		sObjectID.Empty();
	pDC->TextOut(nTextInX_object_id,10,(sObjectID));

	//----------------------------------------------------------
	// Object information TimeStamp; 080428 p�d
	nTextInX_object_date_cap = 80;
	pDC->SelectObject(&m_fnt1);
	pDC->GetTextMetrics(&tm);
	pDC->TextOut(nTextInX_object_date_cap,10,(m_sTimeStamp));

	pDC->SelectObject(&m_fnt2);
	pDC->GetTextMetrics(&tm);
	nTextInX_object_date = nTextInX_object_date_cap + m_sTimeStamp.GetLength()*tm.tmAveCharWidth+tm.tmAveCharWidth ;
	if (!m_bClearWindow && recObject.getObjDate().GetLength() > 0)
		sObjectDate.Format(_T("%s"),recObject.getObjDate().Tokenize(L".",nPos));
	else
		sObjectDate.Empty();
	pDC->TextOut(nTextInX_object_date,10,(sObjectDate));

	//----------------------------------------------------------
	nTextInX_object = 260;
	pDC->SelectObject(&m_fnt1);
	pDC->GetTextMetrics(&tm);
	pDC->TextOut(nTextInX_object,10,(m_sObjCap));

	nTextInX_object_name = nTextInX_object + m_sObjCap.GetLength()*tm.tmAveCharWidth+tm.tmAveCharWidth;
	pDC->SelectObject(&m_fnt2);
	if (!m_bClearWindow)
		pDC->TextOut(nTextInX_object_name,10,(recObject.getObjectName()));
	else
		pDC->TextOut(nTextInX_object_name,10,_T(""));


	if (m_bIsPropData)
	{
		// Property information; 080428 p�d
		nTextInX_prop = nTextInX_object_name + (recObject.getObjectName().GetLength()*tm.tmAveCharWidth)+tm.tmAveCharWidth*10;
		pDC->SelectObject(&m_fnt1);
		pDC->GetTextMetrics(&tm);
		pDC->TextOut(nTextInX_prop,10,(m_sPropCap));

		pDC->SelectObject(&m_fnt2);
		nTextInX_prop_name = nTextInX_prop + (m_sPropCap.GetLength()*tm.tmAveCharWidth)+tm.tmAveCharWidth;
		pDC->TextOut(nTextInX_prop_name,10,(recProp.getPropName()));

		nTextInX_prop_areal = nTextInX_prop_name + (recProp.getPropName().GetLength()*tm.tmAveCharWidth)+tm.tmAveCharWidth*10;
		pDC->SelectObject(&m_fnt1);
		pDC->GetTextMetrics(&tm);
		pDC->TextOut(nTextInX_prop_areal,10,(m_sPropAreal));

		pDC->SelectObject(&m_fnt2);
		nTextInX_prop_areal_value = nTextInX_prop_areal + (m_sPropAreal.GetLength()*tm.tmAveCharWidth)+tm.tmAveCharWidth;
		sPropAreal.Format(_T("%.2f"),recProp.getPropAreal());
		pDC->TextOut(nTextInX_prop_areal_value,10,(sPropAreal));

	}

	// Reset to don't clear window.
	m_bClearWindow = FALSE;

	CDialogBar::OnPaint();
}
