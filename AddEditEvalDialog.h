#pragma once


#include "resource.h"
#include <cmath>

using namespace std;
// CAddEditEvalDialog dialog

class CAddEditEvalDialog : public CDialog
{
	DECLARE_DYNAMIC(CAddEditEvalDialog)

	CUMLandValueDB *m_pDB;

	//Labels
	CMyExtStatic m_wndLblType;
	CMyExtStatic m_wndLblName;
	CMyExtStatic m_wndLblArea;
	CMyExtStatic m_wndLblAge;
	CMyExtStatic m_wndLblH100;
	CMyExtStatic m_wndLblCorrFactor;
	CMyExtStatic m_wndLblPartPine;
	CMyExtStatic m_wndLblPartSpruce;
	CMyExtStatic m_wndLblPartBirch;
	CMyExtStatic m_wndLblVolume;
	CMyExtStatic m_wndLblVolumePerHa;
	CMyExtStatic m_wndLblLength;
	CMyExtStatic m_wndLblWidth;
	CMyExtStatic m_wndLblLandType;
	CMyExtStatic m_wndLblLandValue;
	CMyExtStatic m_wndLblLandValuePerHa;
	CMyExtStatic m_wndLblLayer;
	CMyExtStatic m_wndLblReducMark;		//#HMS-41 Tillf�lligt utnyttjande 190327 J�

	//Buttons
	CButton m_wndOKBtn;
	CButton m_wndCancel;
	CButton m_wndCalculateCorrFactor;
	CButton m_wndOverlappingLand;

	//EditText
	CMyExtEdit m_wndEditName;
	CMyMaskExtEdit m_wndEditArea;
	CMyExtEdit m_wndEditAge;
	CMyMaskExtEdit m_wndEditH100;
	CMyMaskExtEdit m_wndEditCorrFactor;
	CMyExtEdit m_wndEditPartPine;
	CMyExtEdit m_wndEditPartSpruce;
	CMyExtEdit m_wndEditPartBirch;
	CMyMaskExtEdit m_wndEditVolume;
	CMyMaskExtEdit m_wndEditVolumePerHa;
	CMyExtEdit m_wndEditLength;
	CMyExtEdit m_wndEditWidth;
	CMyMaskExtEdit m_wndEditLandValue;
	CMyMaskExtEdit m_wndEditLandValuePerHa;
	CMyMaskExtEdit m_wndEditLayer;

	//Dropdown
	CComboBox m_wndComboType;
	CComboBox m_wndComboLandType;
	CComboBox m_wndComboReducMark;	//#HMS-41 Tillf�lligt utnyttjande 190327 J�

	CTransaction_eval_evaluation * record;
	int objectId;
	int propId;
	int evalId;

	bool updateLandValue;
	bool updateLandValuePerHa;
	bool updateVolumePerHa;
	bool updateVolume;

	void enableDisableFields();
	void enableDisableEdit(CMyExtEdit * edit,bool enabled);
	void enableDisableEdit(CWnd * edit,bool enabled);
	void calculateArea();
	void calculateVolume();
	void calculateLandValue();
	void calculateCorrFactor();
	bool checkTreePercent();
	void showMessage(int id);
	void showMessage(CString msg);



	CTransaction_eval_evaluation getRecord() { return *record; }
	

public:
	CAddEditEvalDialog(CWnd* pParent = NULL);   // standard constructor
	CAddEditEvalDialog(int objId,int propertyId,CUMLandValueDB * db,int evaluationId=-1);
	virtual ~CAddEditEvalDialog();

// Dialog Data
	enum { IDD = IDD_ADD_EVAL_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddEvalDialogOkBtn();
	afx_msg void OnCbnSelchangeAddEvalDialogTypeComboo();
	afx_msg void OnEnChangeAddEvalDialogLengthEdit();
	afx_msg void OnEnChangeAddEvalDialogWidthEdit();
	afx_msg void OnEnChangeAddEvalDialogAreaEdit();
	afx_msg void OnEnChangeAddEvalDialogVolumePerHaEdit();
	afx_msg void OnEnChangeAddEvalDialogVolumeEdit();
	afx_msg void OnBnClickedAddEvalDialogCancelButton();
	afx_msg void OnBnClickedAddEvalDialogCalculateButton();
	afx_msg void OnEnChangeAddEvalDialogLandvalueEdit();
	afx_msg void OnEnChangeAddEvalDialogLandvaluePerHaEdit();
	afx_msg void OnEnChangeAddEvalDialogCorrFactorEdit();
};
