// SelectContractorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectContractorDlg.h"

#include "ResLangFileReader.h"

// CSelectContractorDlg dialog

IMPLEMENT_DYNAMIC(CSelectContractorDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectContractorDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectContractorDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_SEARCH, &CSelectContractorDlg::OnBnClickedBtnSearch)
	ON_BN_CLICKED(IDC_BTN_CLEAR, &CSelectContractorDlg::OnBnClickedBtnClear)
END_MESSAGE_MAP()

CSelectContractorDlg::CSelectContractorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectContractorDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CSelectContractorDlg::~CSelectContractorDlg()
{
}

void CSelectContractorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
//	DDX_Control(pDX, IDC_LBL3_1, m_wndLbl3_1);
	DDX_Control(pDX, IDC_LBL3_2, m_wndLbl3_2);
	DDX_Control(pDX, IDC_LBL3_3, m_wndLbl3_3);
	DDX_Control(pDX, IDC_LBL3_4, m_wndLbl3_4);
	DDX_Control(pDX, IDC_LBL3_5, m_wndLbl3_5);
	DDX_Control(pDX, IDC_LBL3_6, m_wndLbl3_6);
	DDX_Control(pDX, IDC_LBL3_7, m_wndLbl3_7);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox);

	DDX_Control(pDX, IDC_BTN_SEARCH, m_wndSearch);
	DDX_Control(pDX, IDC_BTN_CLEAR, m_wndClear);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP
}

BOOL CSelectContractorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangSet = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
//			SetWindowText((xml->str(IDS_STRING2310)));
			SetWindowText((m_sTitle));

//			m_wndLbl3_1.SetWindowText((xml->str(IDS_STRING2411)));
			m_wndLbl3_2.SetWindowText((xml->str(IDS_STRING2414)));
			m_wndLbl3_3.SetWindowText((xml->str(IDS_STRING2401)));
			m_wndLbl3_4.SetWindowText((xml->str(IDS_STRING2402)));
			m_wndLbl3_5.SetWindowText((xml->str(IDS_STRING2403)));
			m_wndLbl3_6.SetWindowText((xml->str(IDS_STRING2404)));
			m_wndLbl3_7.SetWindowText((xml->str(IDS_STRING2405)));

			m_wndSearch.SetWindowText((xml->str(IDS_STRING2318)));
			m_wndClear.SetWindowText((xml->str(IDS_STRING2319)));
			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING2320)));
			m_wndCancelBtn.SetWindowText((xml->str(IDS_STRING2321)));

			m_sNoCategory = (xml->str(IDS_STRING2416));
		}
		delete xml;
	}

	setupReport();
	getCategories();
	addCategoriesToCBox();
	getContacts();
	populateData();
	return TRUE;
}
// CSelectContractorDlg message handlers

void CSelectContractorDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_SEARCH_PROPS, TRUE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING2400)), 80));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING2401)), 120));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING2402)), 120));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING2403)), 100));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING2404)), 100));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING2405)), 100));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING2406)), 100));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING2407)), 100));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING2408)), 100));
				pCol->SetEditable( FALSE );

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.AllowEdit(FALSE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_REPORT_SEARCH_PROPS),2,75,rect.right - 4,rect.bottom - 115);
			}
			delete xml;
		}	// if (fileExists(sLangFN))
	
	}
}

void CSelectContractorDlg::populateData(void)
{
	m_wndReport.ResetContent();
	if (m_vecContacts.size() > 0)
	{
		for (UINT i = 0;i < m_vecContacts.size();i++)
		{
			CTransaction_contacts rec = m_vecContacts[i];
			m_wndReport.AddRecord(new CContactsReportDataRec(rec.getID(),rec));
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CSelectContractorDlg::OnBnClickedOk()
{
	m_bIsContractorSelected = FALSE;

	// Get information from selected contractor; 080327 p�d
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		CContactsReportDataRec *pRec = (CContactsReportDataRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			m_nID_pk = pRec->getIndex();
			m_sPerOrgNum = pRec->getColumnText(COLUMN_0);
			m_sContactPers = pRec->getColumnText(COLUMN_1);
			m_sCompany = pRec->getColumnText(COLUMN_2);
			m_sAddress = pRec->getColumnText(COLUMN_3);
			m_sPostNum = pRec->getColumnText(COLUMN_4);
			m_sPostAddress = pRec->getColumnText(COLUMN_5);
			m_sPhoneWork = pRec->getColumnText(COLUMN_6);
			m_sPhoneHome = pRec->getColumnText(COLUMN_7);
			m_sMobile = pRec->getColumnText(COLUMN_8);
			m_bIsContractorSelected = TRUE;
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)


	OnOK();
}


void CSelectContractorDlg::getCategories(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getCategories(m_vecCategories);
	}	// if (m_pDB != NULL)
}

void CSelectContractorDlg::getContacts(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getContacts(m_vecContacts);
	}	// if (m_pDB != NULL)
}

void CSelectContractorDlg::addCategoriesToCBox(void)
{
	// Set categories in ComboBox; 080630 p�d
	m_wndCBox.ResetContent();
	if (m_vecCategories.size() > 0)
	{
		// Start by adding 'No catrgory'; 080630 p�d
		m_wndCBox.AddString((m_sNoCategory));
		for (UINT i = 0;i < m_vecCategories.size();i++)
		{
			CTransaction_category rec = m_vecCategories[i];
			m_wndCBox.AddString((rec.getCategory()));
		}
		m_wndCBox.SetCurSel(0);	// Select first item in ComboBox; 080630 p�d
	}	// if (m_vecCategories.size() > 0)
}

void CSelectContractorDlg::OnBnClickedBtnSearch()
{

	CString sSQL,sSQLWhere;
	BOOL bFirstAdded = FALSE;
	BOOL bIsSomethingAdded = FALSE;
	CString sCategory;
	int nIndex = m_wndCBox.GetCurSel();

	if (nIndex > 0)
	{
		m_wndCBox.GetWindowText(sCategory);
	}	// if (nIndex != CB_ERR)

	if (!m_wndEdit1.getText().IsEmpty())
	{
		if (!bFirstAdded)
		{
			sSQLWhere.Format(_T("name_of like '%c%s%c'"),'%',m_wndEdit1.getText(),'%');
			bFirstAdded = TRUE;
		}
		else
			sSQLWhere.Format(_T(" and name_of like '%c%s%c'"),'%',m_wndEdit1.getText(),'%');
		sSQL += sSQLWhere;
		bIsSomethingAdded = TRUE;
	}

	if (!m_wndEdit2.getText().IsEmpty())
	{
		if (!bFirstAdded)
		{
			sSQLWhere.Format(_T("company like '%c%s%c'"),'%',m_wndEdit2.getText(),'%');
			bFirstAdded = TRUE;
		}
		else
			sSQLWhere.Format(_T(" and company like '%c%s%c'"),'%',m_wndEdit2.getText(),'%');
		sSQL += sSQLWhere;
		bIsSomethingAdded = TRUE;
	}

	if (!m_wndEdit3.getText().IsEmpty())
	{
		if (!bFirstAdded)
		{
			sSQLWhere.Format(_T("address like '%c%s%c'"),'%',m_wndEdit3.getText(),'%');
			bFirstAdded = TRUE;
		}
		else
			sSQLWhere.Format(_T(" and address like '%c%s%c'"),'%',m_wndEdit3.getText(),'%');
		sSQL += sSQLWhere;
		bIsSomethingAdded = TRUE;
	}

	if (!m_wndEdit4.getText().IsEmpty())
	{
		if (!bFirstAdded)
		{
			sSQLWhere.Format(_T("post_num like '%c%s%c'"),'%',m_wndEdit4.getText(),'%');
			bFirstAdded = TRUE;
		}
		else
			sSQLWhere.Format(_T(" and post_num like '%c%s%c'"),'%',m_wndEdit4.getText(),'%');
		sSQL += sSQLWhere;
		bIsSomethingAdded = TRUE;
	}

	if (!m_wndEdit5.getText().IsEmpty())
	{
		if (!bFirstAdded)
		{
			sSQLWhere.Format(_T("post_address like '%c%s%c'"),'%',m_wndEdit5.getText(),'%');
			bFirstAdded = TRUE;
		}
		else
			sSQLWhere.Format(_T(" and post_address like '%c%s%c'"),'%',m_wndEdit5.getText(),'%');
		sSQL += sSQLWhere;
		bIsSomethingAdded = TRUE;
	}

	if (m_pDB != NULL)
	{
		if (nIndex >= 1)
			m_pDB->getContacts(sCategory,sSQL,m_vecContacts);
		else if (nIndex < 1 && bIsSomethingAdded)
			m_pDB->getContacts(sSQL,m_vecContacts);
		else
			m_pDB->getContacts(m_vecContacts);

	}

	populateData();
}

void CSelectContractorDlg::OnBnClickedBtnClear()
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndCBox.SetCurSel(0);
	getContacts();
	populateData();
}
