#pragma once

#include "Resource.h"

// CAddP30NewNormFormView form view

class CAddP30NewNormFormView : public CXTPReportView
{
	DECLARE_DYNCREATE(CAddP30NewNormFormView)

	BOOL m_bInitialized;

	CString m_sLangFN;


protected:
	CAddP30NewNormFormView();           // protected constructor used by dynamic creation
	virtual ~CAddP30NewNormFormView();

	BOOL setupReport(void);

	CString m_sMsgCap;
	CString m_sMsgValueError;

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	void addRow(void);
	void delRow(void);

	void checkData(LPCTSTR spc_name,CStringArray &arr_err);

	void getData(CStringArray &arr);

	void setReport(vecObjectTemplate_p30_nn_table &,int spc_id);

	void resetReport(void)	{ GetReportCtrl().ResetContent(); }

	void setupSIForP30(LPCTSTR area,int spc);

protected:
	//{{AFX_VIRTUAL(CP30NewNormTemplateFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CP30NewNormTemplateFormView)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnReportValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

