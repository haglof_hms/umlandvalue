// MyRichEditCtrl.cpp : implementation file
//
// Filename:      MyRichEditCtrl.cpp
// Project:       MyRtf
// Module:        MyRtf
// Programmer:    Frunza Adriana
// Version:       1.00
// Revision Date: 19-09-2001
//----------------------------------------------------------------------------
// Description:   Definition of class CMyRichEditCtrl
//                See CMyRichEditCtrl.h
//----------------------------------------------------------------------------
// Revision history:
// 19-09-2001 - First implementation
//----------------------------------------------------------------------------
// Bugs: ........1. D:\MyProjects\MyRtf\MyRtfDlg.cpp(200) : error C2440:
//                  '=' : cannot convert from 'unsigned long (__stdcall 
//				    CMyRtfDlg::*)(unsigned long,unsigned char *,
//					long,long *)' to 'unsigned long (__stdcall *)
//					(unsigned long,unsigned char *,long,long *)'
//				 2. D:\MyProjects\MyRtf\MyRichEditCtrl.cpp(88) : error
//                  C2724: 'MyStreamInCallback' : 'static' should not 
//                  be used on member functions defined at file scope
//				 3. Line: SetBackgroundColor(FALSE, RGB(255,0, 0));	has
//                  no effect 
//----------------------------------------------------------------------------
// @doc
//----------------------------------------------------------------------------
// Bugs fix:.....
// 1. function below taken from MyRtfDlg.cpp and put in CMyRichEditCtrl.cpp 
//    and make it static
// 2. delete keyword static from MyStreamInCallback from 
//    MyRichEditCtrl.cpp  	
// 3. Line is moved in class CMyRtfDlg::OnInitDialog() and has effect
//----------------------------------------------------------------------------


#include "stdafx.h"
//#include "MyRtf.h"
#include "resource.h"

#include "MyRichEditCtrl.h"

#include "ResLangFileReader.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyRichEditCtrl



//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::CMyRichEditCtrl
// @mfunc   constructor
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 19-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

CMyRichEditCtrl::CMyRichEditCtrl()
{
	bold = TRUE;
	italic = TRUE;
	underlined = TRUE;
	m_bClick=false;
	m_bTab=false;
}


//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::~CMyRichEditCtrl
// @mfunc   destructor
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 19-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

CMyRichEditCtrl::~CMyRichEditCtrl()
{
}


BEGIN_MESSAGE_MAP(CMyRichEditCtrl, CRichEditCtrl)
	//{{AFX_MSG_MAP(CMyRichEditCtrl)
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyRichEditCtrl message handlers


// My callback procedure that writes a file content
// to the rich edit control.
// function added on 20 september 2001 from VC help(MSDN)
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::MyStreamInCallback
// @func    writes the file content to the rich edit control
// @parm    DWORD  | dwCookie | handle to an open file
// @parm    LPBYTE | pbBuff   | Pointer to a buffer to read from to.
//                              For a stream-in (read) operation, 
//								the callback function fills this 
//								buffer with data to transfer into the
//								rich edit control
// @parm    LONG   | cb       | Number of bytes to read 
// @parm    LONG   | *pcb     | Pointer to a variable that the 
//								callback function sets to the number 
//								of bytes actually read  
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 20-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

DWORD CALLBACK CMyRichEditCtrl ::MyStreamInCallback(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
   CFile* pFile = (CFile*) dwCookie;
   ASSERT_KINDOF(CFile,pFile);
   *pcb = pFile->Read(pbBuff, cb);
   return 0;
}

/*	Callback function to stream an RTF string into the rich edit control.*/
DWORD CALLBACK CMyRichEditCtrl::CBStreamIn(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
	// We insert the rich text here.
/*	
	This function taken from CodeGuru.com
	http://www.codeguru.com/richedit/rtf_string_streamin.shtml
	Zafir Anjum
*/
	CString *psBuffer = (CString* )dwCookie; 
	CString stream = *psBuffer; 
	int i=0;
	if (cb > psBuffer->GetLength()) 
	{ 
		cb = psBuffer->GetLength(); 
		for (i=0;i<cb;i++) 
		{ 
			*(pbBuff+i) = psBuffer->GetAt(i); 
		} 

		*pcb = cb; 
		*psBuffer = stream.Mid(0,cb); 
	}
	else 
		//Lagt till en else sats f�r att ta hand om texter som �r l�ngre �n cb dvs 4096, 
		//d�rav *psBuffer = psBuffer->Right(psBuffer->GetLength() - cb); Bug #2242 20010804 J�
		//Annars k�rs denna callback flera g�nger om texten �verstiger 4096 och d� kopieras det hela tiden fr�n b�rjan 
		//av texten in i rich edit kontrollen, m�ste flytta pekaren dit den skall b�rja l�sa n�sta g�ng s� att s�ga.
	{
		for (i=0;i<cb;i++) 
		{ 
			*(pbBuff+i) = psBuffer->GetAt(i); 
		} 

		*pcb = cb; 
    	*psBuffer = psBuffer->Right(psBuffer->GetLength() - cb);
	}



/*
	CString *psBuffer = (CString* )dwCookie; 
	CString stream = *psBuffer; 

	if (cb > psBuffer->GetLength()) 
	{ 
		cb = psBuffer->GetLength(); 
	} 

	for (int i=0;i<cb;i++) 
	{ 
		*(pbBuff+i) = psBuffer->GetAt(i); 
	} 


	*pcb = cb; 
	//*psBuffer = psBuffer->Mid(0,cb); 
	*psBuffer = stream.Mid(0,cb); */

/*
	CString *pstr = (CString *) dwCookie;

	if (pstr->GetLength() < cb)
	{
		*pcb = pstr->GetLength();
		memcpy(pbBuff, (LPCTSTR) *pstr, *pcb);
		pstr->Empty();
	}
	else
	{
		*pcb = cb;
		memcpy(pbBuff, (LPCTSTR)*pstr, *pcb);
		*pstr = pstr->Right(pstr->GetLength() - cb);
	}
	///
*/
	return 0;
}

/*	Callback function to stream the RTF string out of the rich edit control.*/
DWORD CALLBACK CMyRichEditCtrl::CBStreamOut(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
	// Address of our string var is in psEntry
	CString *psEntry = (CString*) dwCookie;

	CString tmpEntry = _T("");
	tmpEntry = (CString) pbBuff;

	// And write it!!!
	*psEntry += tmpEntry.Left(cb);

	return 0;
}

// function added on 20 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetRtf
// @func    Calling this will insert the file content.
// @parm    CFile*  | pInputFile | the file to be opened
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 20-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetRtf(CFile* pInputFile )
{
	// Read the text in
	EDITSTREAM es;
	es.dwError = 0;
	es.pfnCallback = (EDITSTREAMCALLBACK)MyStreamInCallback;
	es.dwCookie = (DWORD)pInputFile;
	StreamIn(SF_RTF, es);	// Do it.
}

void CMyRichEditCtrl::SetRtf(CString sRTF)
{
	// Put the RTF string sRTF into the rich edit control.
	// Read the text in
	EDITSTREAM es;
	es.dwError = 0;
	es.pfnCallback = (EDITSTREAMCALLBACK)CBStreamIn;
	es.dwCookie = (DWORD) &sRTF;
	StreamIn(SF_RTF, es);	// Do it.
	
}

CString CMyRichEditCtrl::GetRtf()
{
	// Return the RTF string of the text in the control.
	
	// Stream out here.
	EDITSTREAM es;
	es.dwError = 0;
	es.pfnCallback = (EDITSTREAMCALLBACK)CBStreamOut;		// Set the callback
	CString sRTF = _T("");
	es.dwCookie = (DWORD) &sRTF;	// so sRTF receives the string
	StreamOut(SF_RTF, es);			// Call CRichEditCtrl::StreamOut to get the string.
	///

	return sRTF;

}

CString CMyRichEditCtrl::IsTextInRTF(CString rtf)
{
	CString sRTF(_T("{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1053{\\fonttbl{\\f0\\fnil\\fcharset0 Arial;}}\\viewkind4\\uc1\\pard\\fs17 %s \\par }"));

	CString sRTFString(rtf);
	if (rtf.Left(6) != _T("{\\rtf1"))
	{
		sRTFString.Format(sRTF,rtf);
	}

	return sRTFString;
}


// function added on Friday 21 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetSelectionBold
// @func    sets the current selection to be bold (Note: this will 
//			actually act like pressing the Bold toolbar button in 
//			Wordpad - if the text is not bold, it will become bold; 
//			if the text is already bold, it will remove the bold 
//			formatting; if part of the selection is bold, the entire 
//			selection will become bold; if there is not anything 
//			selected, the format is changed to bold so when the user 
//			starts typing, it will be in bold). 
// @parm    no parameters
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 21-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetSelectionBold()
{
	cf.cbSize = sizeof(CHARFORMAT);
	GetSelectionCharFormat(cf);
	
	if (cf.dwMask & CFM_BOLD)	// selection is all the same
	{
		cf.dwEffects ^= CFE_BOLD; 
	}
	else
	{
		cf.dwEffects |= CFE_BOLD;
	}
	
	cf.dwMask = CFM_BOLD;

	SetSelectionCharFormat(cf);
}
// function added on Friday 21 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetSelectionItalic
// @func    sets the current selection to be italic (Note: this will 
//			actually act like pressing the Italic toolbar button in 
//			Wordpad - if the text is not Italic, it will become Italic; 
//			if the text is already Italic, it will remove the Italic 
//			formatting; if part of the selection is Italic, the entire 
//			selection will become Italic; if there is not anything 
//			selected, the format is changed to Italic so when the user 
//			starts typing, it will be in Italic). 
// @parm    no parameters
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 21-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetSelectionItalic()
{
	cf.dwMask = CFM_ITALIC;
	if ( italic == TRUE )	
		 cf.dwEffects |= CFE_ITALIC;
	else 
		 cf.dwEffects &= ~CFE_ITALIC;
	SetSelectionCharFormat(cf);
	italic = !italic;
}


// function added on Friday 21 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetSelectionUnderline
// @func    sets the current selection to be Underlined (Note: this 
//			will actually act like pressing the Underlined toolbar 
//			button in Wordpad - if the text is not Underlined, it will 
//			become Underlined; if the text is already Underlined, it 
//			will remove the Underlined formatting; if part of the 
//			selection is Underlined, the entire selection will become 
//			Underlined; if there is not anything selected, the format
//			is changed to Underlined so when the user starts typing,
//			it will be in Underlined). 
// @parm    no parameters
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 21-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetSelectionUnderline()
{
	cf.dwMask = CFM_UNDERLINE;
	if ( underlined == TRUE )	
		 cf.dwEffects |= CFE_UNDERLINE;
	else 
		 cf.dwEffects &= ~CFE_UNDERLINE;
	SetSelectionCharFormat(cf);
	underlined = !underlined;
}
// function added on Saturday 22 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetColour
// @func    displays a color selection dialog box and sets the 
//			selection to the chosen color (if OK is pressed). 
// @parm    no parameters
// @xref    <c CMyRichEditCtrl>
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 22-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetColour()
{
	CColorDialog dlg;
	
	if (cf.dwEffects & CFE_AUTOCOLOR) 
		cf.dwEffects -= CFE_AUTOCOLOR;
	
	// Get a color from the common color dialog box
	if ( dlg.DoModal() == IDOK )
	{
		cf.crTextColor = dlg.GetColor();
	}
	cf.dwMask = CFM_COLOR;	

	SetSelectionCharFormat(cf);
}

// function added on Saturday 22 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetParagraphLeft
// @func    sets the current paragraph to be left 
// @parm    no parameters
// @xref    <c CMyRichEditCtrl>
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 22-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------
void CMyRichEditCtrl::SetParagraphLeft()
{

	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_ALIGNMENT;    
	pf.wAlignment = PFA_LEFT;
	
	SetParaFormat(pf);
}


// function added on Monday 24 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetParagraphRight
// @func    sets the current paragraph to be right 
// @parm    no parameters
// @xref    <c CMyRichEditCtrl>
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 24-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------
void CMyRichEditCtrl::SetParagraphRight()
{

	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_ALIGNMENT;    
	pf.wAlignment = PFA_RIGHT;
	
	SetParaFormat(pf);
}


// function added on Monday 24 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetParagraphCenter
// @func    Modify the paragraph format so that the text is centered. 
// @parm    no parameters
// @xref    <c CMyRichEditCtrl>
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
// 24-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------
void CMyRichEditCtrl::SetParagraphCenter()
{

	pf.cbSize = sizeof(PARAFORMAT);
	pf.dwMask = PFM_ALIGNMENT;    
	pf.wAlignment = PFA_CENTER;
	
	SetParaFormat(pf);
}


// function added on Wednesday 26 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetParagraphBulleted
// @func    Modify the paragraph format so that the text is bulleted. 
// @parm    no parameters
// @xref    <c CMyRichEditCtrl>
//----------------------------------------------------------------------------
// taken from www.codeproject.com 
// Jeremy Iverson
// @revs 
// 26-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------
void CMyRichEditCtrl::SetParagraphBulleted()
{
	GetParaFormat(pf);

	if ( (pf.dwMask & PFM_NUMBERING) && (pf.wNumbering == PFN_BULLET) )
	{
		pf.wNumbering = 0;
		pf.dxOffset = 0;
		pf.dxStartIndent = 0;
		pf.dwMask = PFM_NUMBERING | PFM_STARTINDENT | PFM_OFFSET;
	}
	else
	{
		pf.wNumbering = PFN_BULLET;
		pf.dwMask = PFM_NUMBERING;
		if (pf.dxOffset == 0)
		{
			pf.dxOffset = 4;
			pf.dwMask = PFM_NUMBERING | PFM_STARTINDENT | PFM_OFFSET;
		}
	}

	SetParaFormat(pf);
}

// function added on Friday 28 september 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetFontName
// @func    Set the font 
// @parm    CString  | sFontName | the font name
//----------------------------------------------------------------------------
// taken from www.codeproject.com 
// Jeremy Iverson
// @revs 
// 28-09-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetFontName(CString sFontName)
{
    GetSelectionCharFormat(cf);

	// Set the font name.
	_tcscpy(cf.szFaceName, sFontName);
	cf.dwMask |=CFM_FACE;
	SetSelectionCharFormat(cf);
}

// function added on Tuesday 2 Octomber 2001 
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetFontSize
// @func    Set the font 
// @parm    int  | nFontSize | the font size
//----------------------------------------------------------------------------
// taken from www.codeproject.com 
// Jemery Iverson
// @revs 
// 2-10-2001 - First implementation
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetFontSize(int nFontSize)
{
	GetSelectionCharFormat(cf);
	nFontSize *= 2;	// convert from to twips
	cf.yHeight = nFontSize ;
	cf.dwMask = CFM_SIZE;

	SetSelectionCharFormat ( cf );
}

// function added on Friday 12 Octomber 2001 
// this code is taken from Vadim Z.'s article RTF based Tooltips
// www.codeguru.com
//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::Print
// @func    print the rich edit control content
// @parm    bool  | bShowPrintDialog | show the dialog box CPrintDialog
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------
void CMyRichEditCtrl::Print(bool bShowPrintDialog)
{
	CPrintDialog printDialog(false);
	
	
	if (bShowPrintDialog)
	{
		int r = printDialog.DoModal();
		if (r == IDCANCEL)
			return; // User pressed cancel, don't print.
	}
	else
	{
		printDialog.GetDefaults();
	}

	HDC hPrinterDC = printDialog.GetPrinterDC();

	// This code basically taken from MS KB article Q129860 
	
	FORMATRANGE fr;
	  int	  nHorizRes = GetDeviceCaps(hPrinterDC, HORZRES);
	  int	  nVertRes = GetDeviceCaps(hPrinterDC, VERTRES);
      int     nLogPixelsX = GetDeviceCaps(hPrinterDC, LOGPIXELSX);
	  int     nLogPixelsY = GetDeviceCaps(hPrinterDC, LOGPIXELSY);
	  LONG	      lTextLength;   // Length of document.
	  LONG	      lTextPrinted;  // Amount of document printed.

	 // Ensure the printer DC is in MM_TEXT mode.
	  SetMapMode ( hPrinterDC, MM_TEXT );

	 // Rendering to the same DC we are measuring.
	  ZeroMemory(&fr, sizeof(fr));
	  fr.hdc = fr.hdcTarget = hPrinterDC;

	 // Set up the page.
	  fr.rcPage.left     = fr.rcPage.top = 0;
	  fr.rcPage.right    = (nHorizRes/nLogPixelsX) * 1440;
	  fr.rcPage.bottom   = (nVertRes/nLogPixelsY) * 1440;

	 // Set up 0" margins all around.
	  fr.rc.left   = fr.rcPage.left ;//+ 1440;  // 1440 TWIPS = 1 inch.
	  fr.rc.top    = fr.rcPage.top ;//+ 1440;
	  fr.rc.right  = fr.rcPage.right ;//- 1440;
	  fr.rc.bottom = fr.rcPage.bottom ;//- 1440;

	 // Default the range of text to print as the entire document.
	  fr.chrg.cpMin = 0;
	  fr.chrg.cpMax = -1;
      FormatRange(&fr,true);      
	 // Set up the print job (standard printing stuff here).
		DOCINFO di;
	  ZeroMemory(&di, sizeof(di));
	  di.cbSize = sizeof(DOCINFO);

	  di.lpszDocName = _T("ELVRtfDocument");

	    // Do not print to file.
	     di.lpszOutput = NULL;
	     

	 // Start the document.
	  StartDoc(hPrinterDC, &di);

	 // Find out real size of document in characters.
	  lTextLength = GetTextLength();//SendMessage ( /*hRTFWnd,*/ WM_GETTEXTLENGTH, 0, 0 );
	
	 do
	     {
	     // Start the page.
	     StartPage(hPrinterDC);

	    // Print as much text as can fit on a page. The return value is
	     // the index of the first character on the next page. Using TRUE
	     // for the wParam parameter causes the text to be printed.

	    lTextPrinted =FormatRange(&fr,true);
		
		DisplayBand(&fr.rc );
	    
	    // Print last page.
	     EndPage(hPrinterDC);

	    // If there is more text to print, adjust the range of characters
	     // to start printing at the first character of the next page.
		if (lTextPrinted < lTextLength)
		{
		fr.chrg.cpMin = lTextPrinted;
		fr.chrg.cpMax = -1;
		}
	     }
	  while (lTextPrinted < lTextLength);

	 // Tell the control to release cached information.
	  FormatRange(NULL,false);
		  //SendMessage( EM_FORMATRANGE, 0, (LPARAM)NULL);

	 EndDoc (hPrinterDC);

	  ///////
	  
	DeleteDC(hPrinterDC);

}

void CMyRichEditCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// Show a Popupmenu on rightclick; 070502 p�d
	CString sLangFN;
	CPoint curPos;
	CMenu menu;

	VERIFY(menu.CreatePopupMenu());
	GetCursorPos(&curPos);

	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			// create main menu items
			menu.AppendMenu(MF_STRING, ID_TBTN_CUT, xml.str(IDS_STRING6211));
			menu.AppendMenu(MF_STRING, ID_TBTN_COPY, xml.str(IDS_STRING6212));
			menu.AppendMenu(MF_STRING, ID_TBTN_PASTE, xml.str(IDS_STRING6213));
			menu.AppendMenu(MF_STRING, ID_TBTN_UNDO, xml.str(IDS_STRING6214));
		}	// if (xml->Load(m_sLangFN))
	}
	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
							curPos.x, curPos.y, this, NULL);
	// other general items
	switch (nMenuResult)
	{
		case ID_TBTN_CUT		: Cut(); break;
		case ID_TBTN_COPY		: Copy(); break;
		case ID_TBTN_PASTE	: Paste(); break;
		case ID_TBTN_UNDO		: Undo(); break;
	}
	CRichEditCtrl::OnRButtonDown(nFlags, point);
}

BOOL CMyRichEditCtrl::PreTranslateMessage(MSG *pMSG)
{
	UINT  nKeyCode = pMSG->wParam; // virtual key code of the key pressed
  // not a WM_KEYDOWN message?
  if (pMSG->message == WM_KEYDOWN)
	{
	  // CTRL+C, CTRL+X, CTRL+V and CTRL+BACKSPACE
		if ( nKeyCode == _T('C') && (::GetKeyState(VK_CONTROL) & 0x8000) )
		{
			CopyText();
		}
		if ( nKeyCode == _T('X') && (::GetKeyState(VK_CONTROL) & 0x8000) )
		{
			CutText();
		}
		if ( nKeyCode == _T('V') && (::GetKeyState(VK_CONTROL) & 0x8000) )
		{
			PasteText();
		}
		if ( nKeyCode == _T('Z') && (::GetKeyState(VK_CONTROL) & 0x8000) )
		{
			UndoLast();
		}
	}

	return CRichEditCtrl::PreTranslateMessage(pMSG);
}

//----------------------------------------------------------------------------
// Function CMyRichEditCtrl::SetColor
// @func    sets the selection to the color
// @parm    COLORREF | clr | 
// @xref    <c CMyRichEditCtrl>
//----------------------------------------------------------------------------
// @prog 
// Frunza Adriana
// @revs 
//----------------------------------------------------------------------------
// @todo 
//----------------------------------------------------------------------------

void CMyRichEditCtrl::SetColor(COLORREF clr)
{
	if (cf.dwEffects & CFE_AUTOCOLOR) 
		cf.dwEffects -= CFE_AUTOCOLOR;
	
		cf.crTextColor = clr;

		cf.dwMask = CFM_COLOR;	

	SetSelectionCharFormat(cf);
}

bool CMyRichEditCtrl::SelectionIsBold()
{
	 GetSelectionCharFormat(cf);	
	
	if (cf.dwEffects & CFM_BOLD)
		return true;
	else
		return false;
}

bool CMyRichEditCtrl::SelectionIsItalic()
{
	 GetSelectionCharFormat(cf);	
	
	if (cf.dwEffects & CFM_ITALIC)
		return true;
	else
		return false;
}

bool CMyRichEditCtrl::SelectionIsUnderlined()
{
	GetSelectionCharFormat(cf);	
	
	if (cf.dwEffects & CFM_UNDERLINE)
		return true;
	else
		return false;
}
bool CMyRichEditCtrl::ParagraphIsCentered()
{
	 GetParaFormat(pf);

	if (pf.wAlignment == PFA_CENTER)
		return true;
	else
		return false;
}

bool CMyRichEditCtrl::ParagraphIsLeft()
{
	GetParaFormat(pf);

	if (pf.wAlignment == PFA_LEFT)
		return true;
	else
		return false;
}

bool CMyRichEditCtrl::ParagraphIsRight()
{
	GetParaFormat(pf);

	if (pf.wAlignment == PFA_RIGHT)
		return true;
	else
		return false;
}
bool CMyRichEditCtrl::ParagraphIsBulleted()
{
	GetParaFormat(pf);

	if (pf.wNumbering == PFN_BULLET)
		return true;
	else
		return false;
}


