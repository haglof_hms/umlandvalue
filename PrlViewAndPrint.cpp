// PrlViewAndPrint.cpp : implementation file
//

// PrlViewAndPrint.cpp : implementation file
//

#include "stdafx.h"
//#include "UMPricelists.h"
#include "PrlViewAndPrint.h"

//#include "PrlViewAndPrintSettingsDlg.h"

#include "ResLangFileReader.h"


/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc

IMPLEMENT_DYNCREATE(CMDIPrlViewAndPrintDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIPrlViewAndPrintDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIPrlViewAndPrintDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc construction/destruction

CMDIPrlViewAndPrintDoc::CMDIPrlViewAndPrintDoc()
{
}

CMDIPrlViewAndPrintDoc::~CMDIPrlViewAndPrintDoc()
{
}

BOOL CMDIPrlViewAndPrintDoc::OnNewDocument()
{

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc serialization

void CMDIPrlViewAndPrintDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc diagnostics

#ifdef _DEBUG
void CMDIPrlViewAndPrintDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIPrlViewAndPrintDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIPrlViewAndPrintDoc commands

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintFrame


IMPLEMENT_DYNCREATE(CMDIPrlViewAndPrintFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPrlViewAndPrintFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPrlViewAndPrintFrame)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_PRINT_OUT_PRL, OnPrintOut)
//	ON_COMMAND(ID_TBTN_SETTINGS, OnSettings)
//	ON_UPDATE_COMMAND_UI(ID_TBTN_SETTINGS, OnSettingsTBtn)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPrlViewAndPrintFrame::CMDIPrlViewAndPrintFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	m_bIsSettingsTBtn = TRUE;
}

CMDIPrlViewAndPrintFrame::~CMDIPrlViewAndPrintFrame()
{
}

void CMDIPrlViewAndPrintFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_PRINT_PRICELIST_KEY);
	SavePlacement(this, csBuf);
}

int CMDIPrlViewAndPrintFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			sToolTip1 = xml.str(IDS_STRING4251);
		}
		xml.clean();
	}

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR3);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();


	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR3)
				{		
					setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_PRINT,sToolTip1);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIPrlViewAndPrintFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CMDIPrlViewAndPrintFrame diagnostics

#ifdef _DEBUG
void CMDIPrlViewAndPrintFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPrlViewAndPrintFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// PRIVATE
void CMDIPrlViewAndPrintFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIPrlViewAndPrintFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_PRINT_PRICELIST_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPrlViewAndPrintFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIPrlViewAndPrintFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELISTS_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELISTS_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPrlViewAndPrintFrame::OnPrintOut(void)
{

	CPrlViewAndPrint *pPrlView = (CPrlViewAndPrint*)getFormViewByID(IDD_FORMVIEW10);
	if (pPrlView != NULL)
	{
		pPrlView->printOut();
		pPrlView = NULL;
	}

}

void CMDIPrlViewAndPrintFrame::OnSettings(void)
{
/*
	CPrlViewAndPrint *pPrlView = (CPrlViewAndPrint*)getFormViewByID(IDD_FORMVIEW10);
	if (pPrlView != NULL)
	{
		pPrlView->settingsDlg();
		pPrlView = NULL;
	}
*/
}

void CMDIPrlViewAndPrintFrame::OnSettingsTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsSettingsTBtn );
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPrlViewAndPrintFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDIPrlViewAndPrintFrame::setLanguage()
{
}

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint

IMPLEMENT_DYNCREATE(CPrlViewAndPrint, CMyHtmlView)

BEGIN_MESSAGE_MAP(CPrlViewAndPrint, CMyHtmlView)
	//{{AFX_MSG_MAP(CPrlViewAndPrint)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPrlViewAndPrint::CPrlViewAndPrint()
{
	//{{AFX_DATA_INIT(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPrlViewAndPrint::~CPrlViewAndPrint()
{
}

void CPrlViewAndPrint::DoDataExchange(CDataExchange* pDX)
{
	CMyHtmlView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint diagnostics

#ifdef _DEBUG
void CPrlViewAndPrint::AssertValid() const
{
	CMyHtmlView::AssertValid();
}

void CPrlViewAndPrint::Dump(CDumpContext& dc) const
{
	CMyHtmlView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint message handlers

void CPrlViewAndPrint::OnInitialUpdate()
{
	setHTMFileName(PRL_VIEW_FN);
	setupHTM();
}

void CPrlViewAndPrint::setupHTM()
{
	m_bUseSettings = FALSE;
	m_bShowPriceByQual = TRUE;

	BOOL bIsAvgPrl = FALSE;
	CStringArray sarrQDesc;
	CString sQualityName;
	CTransaction_pricelist rec;
	CELVPrlViewAndPrintRec *pRec = NULL;
	TCHAR szName[128];
	int nPriceIn;
	double fPricePerQual;
	std::map<int,double> mapPricePerQual;

	vecTransactionSpecies vecSpc,vecSpcALL;
	CTransaction_species recSpc;
	vecTransactionAssort vecAss;
	CTransaction_assort recAss;
	vecTransactionDiameterclass vecDCLS;
	CTransaction_diameterclass recDCLS;
	vecTransactionPrlData vecPrlData;
	CTransaction_prl_data recPrlData;
	vecTransactionPrlData vecPrlData_perc;
	CTransaction_prl_data recPrlData_perc;

	CStringArray sarrPulpWoodTypes;

	CString sObjectName;
	CString sObjectID;

	CString sPulpwoodType;
	CTransaction_elv_object *pObject = getActiveObject();
	if (pObject != NULL)
	{
		sObjectName = pObject->getObjectName();
		sObjectID = pObject->getObjIDNumber();
	}


	CString sBuff;
	CString sData;
	CString sDCLSData;

	xmllitePricelistParser *parser = new xmllitePricelistParser();
	if (parser == NULL) return;

	RLFReader xml;
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		if (parser->loadStream(pObject->getObjPricelistXML()) && xml.Load(m_sLangFN))
		{
			// Add Pulpwood types to this StringArray; 070608 p�d
			sarrPulpWoodTypes.RemoveAll();
			sarrPulpWoodTypes.Add((xml.str(IDS_STRING1552)));
			sarrPulpWoodTypes.Add((xml.str(IDS_STRING1550)));
			sarrPulpWoodTypes.Add((xml.str(IDS_STRING1551)));
			// Start HTM-file; 090901 p�d
			startHTM();

			parser->getHeaderName(szName);
			parser->getHeaderPriceIn(&nPriceIn);
			bIsAvgPrl = (pObject->getObjTypeOfPricelist() == 2);

			if (!m_bUseSettings /* = FALSE */)
			{
				parser->getSpeciesInPricelistFile(vecSpc);
				vecSpcALL = vecSpc;
			}
			parser->getAssortmentPerSpecie(vecAss);
			
			setHTM_text(getDBDateTime(),HTM_FREE_FMT,1);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING131) + _T(" : "),HTM_FREE_FMT,4);
			setHTM_text(szName,HTM_FREE_FMT,4,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			// Add stand-information; 090130 p�d
			setHTM_text(xml.str(IDS_STRING204) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(sObjectName,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING1000) + _T(" : "),HTM_FREE_FMT,2);
			setHTM_text(sObjectID,HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			if (!bIsAvgPrl)
			{
				// Check if pricelist is set to be used; 090108 p�d
				if (nPriceIn == 1)
				{
					setHTM_text(xml.str(IDS_STRING1559) + _T(" : "),HTM_FREE_FMT,2);
					setHTM_text(xml.str(IDS_STRING1560),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				}
				else if (nPriceIn == 2)
				{
					setHTM_text(xml.str(IDS_STRING1559) + _T(" : "),HTM_FREE_FMT,2);
					setHTM_text(xml.str(IDS_STRING1561),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
				}
				setHTML_linefeed(1);
			}

			setHTML_line(HTM_HR_NOSHADE,2);

			// OBS! Print by specie; 090107 p�d
			if (vecSpc.size() > 0)
			{
				for (UINT spc = 0;spc < vecSpc.size();spc++)
				{
					recSpc = vecSpc[spc];			
					// Set name of specie; 090902 p�d
					setHTM_text(recSpc.getSpcName(),HTM_FREE_FMT,4,_T(""),HTM_COLOR_BLACK,true);
					setHTML_linefeed(1);

					//////////////////////////////////////////////////////////////////////////////////////////
					// START --- ADDING ASSORTMENTS/SPECIE; 090902 P�D		
					//////////////////////////////////////////////////////////////////////////////////////////
					if (vecAss.size() > 0)
					{
						// Start by setting up the column widths; 090902 p�d
						setHTM_table();
						setHTM_start_table_column();
						setHTM_table_column(xml.str(IDS_STRING1553),150,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1554),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1555),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1556),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_table_column(xml.str(IDS_STRING1557),100,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
						setHTM_end_table_column();

						for (UINT ass = 0;ass < vecAss.size();ass++)
						{
							recAss = vecAss[ass];
							if (recAss.getSpcID() == recSpc.getSpcID() && !recAss.getAssortName().IsEmpty())
							{
								if (recAss.getPulpType() > -1 && recAss.getPulpType() < sarrPulpWoodTypes.GetCount() && sarrPulpWoodTypes.GetCount() > 0)
									sPulpwoodType = sarrPulpWoodTypes.GetAt(recAss.getPulpType()).Left(20);
								else
									sPulpwoodType = _T("");

								setHTM_start_table_column();
								setHTM_table_column(recAss.getAssortName().Left(20),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
								setHTM_table_column(sPulpwoodType,100,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_table_column(recAss.getMinDiam(),100,1,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_table_column(recAss.getPriceM3fub(),100,1,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_table_column(recAss.getPriceM3to(),100,1,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
								setHTM_end_table_column();
							}
						}	// for (UINT ass = 0;ass < vecAss.size();ass++)
						endHTM_table();
					}	// if (vecAss.size() > 0)
					//////////////////////////////////////////////////////////////////////////////////////////
					// END --- ADDING ASSORTMENTS/SPECIE; 090902 P�D		
					//////////////////////////////////////////////////////////////////////////////////////////

					setHTML_linefeed(1);

					//----------------------------------------------------------------------------------------
					if (!bIsAvgPrl)
					{
						parser->getDCLSForSpecie(recSpc.getSpcID(),vecDCLS);
						parser->getPriceForSpecie(recSpc.getSpcID(),vecPrlData);

						//////////////////////////////////////////////////////////////////////////////////////////
						// START --- ADDING DIAMTRERCLASSES AND PRICE/DIAMTERCLASS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
						if (vecDCLS.size() > 0 && vecPrlData.size() > 0)
						{
							setHTM_table();
							setHTM_start_table_column();
							setHTM_table_column(xml.str(IDS_STRING1558),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true,false,true);
							sBuff.Empty();
							for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
							{
								recDCLS = vecDCLS[dcls];
								sBuff.Format(_T("%d-"),recDCLS.getStartDiam());
								setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
								// Initciate map for prices per quality; 090109 p�d
								mapPricePerQual[dcls] = 0.0;
							}	// for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
							setHTM_end_table_column();

							for (UINT prl_data = 0;prl_data < vecPrlData.size();prl_data++)
							{
								recPrlData = vecPrlData[prl_data];
								if (_tcslen(recPrlData.getQualName()) > 0)
								{
									setHTM_start_table_column();
	
									setHTM_table_column(recPrlData.getQualName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
									sBuff.Empty();
									// Check if there's any prices; 090108 p�d
									if (recPrlData.getInts().size() > 0)
									{
										for (UINT price = 0;price < recPrlData.getInts().size();price++)
										{
											setHTM_table_column(recPrlData.getInts()[price],50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
										}	// for (UINT price = 0;price < recPrlData.getInts().size();price++)
									}	// if (recPrlData.getInts().size() > 0)
									setHTM_end_table_column();
								} // if (_tcslen(recPrlData.getQualName()) > 0)
							}	// for (UINT prl_data = 0;prl_data < vecPrlData.size();prl_data++)
							endHTM_table();
						}	// if (vecDCLS.size() > 0 && vecPrlData.size() > 0)
						//////////////////////////////////////////////////////////////////////////////////////////
						// END --- ADDING DIAMTRERCLASSES AND PRICE/DIAMTERCLASS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
	

						//////////////////////////////////////////////////////////////////////////////////////////
						// START --- ADDING QUALITYDESCRIPTIONS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
						parser->getQualDescNameForSpecie(recSpc.getSpcID(),sarrQDesc);
						if (sarrQDesc.GetCount() > 0)
						{
							setHTM_table();
							for (int i = 0;i < sarrQDesc.GetCount();i++)
							{
								sQualityName = sarrQDesc.GetAt(i);

								if (vecDCLS.size() > 0)
								{
									setHTM_start_table_column();
									setHTM_table_column(sQualityName,150,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_CENTER,true,true);
									setHTM_end_table_column();

									setHTM_start_table_column();
									setHTM_table_column(xml.str(IDS_STRING1558),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true,false,true);
									sBuff.Empty();
									for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
									{
										recDCLS = vecDCLS[dcls];
										sBuff.Format(_T("%d-"),recDCLS.getStartDiam());
										setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_RIGHT,false,false,true);
										// Initciate map for prices per quality; 090109 p�d
										mapPricePerQual[dcls] = 0.0;
									}	// for (UINT dcls = 0;dcls < vecDCLS.size();dcls++)
									setHTM_end_table_column();
								}

								parser->getQualDescPercentForQualityAndSpecie(recSpc.getSpcID(),sQualityName,vecPrlData_perc);
								if (spc < vecPrlData_perc.size())
								{
									if (vecPrlData_perc[spc].getInts().size() > 0)
									{
										//----------------------------------------------------------------------------------------
										for (UINT prl_data1 = 0;prl_data1 < vecPrlData_perc.size();prl_data1++)
										{
											setHTM_start_table_column();
											recPrlData_perc = vecPrlData_perc[prl_data1];
											setHTM_table_column(recPrlData_perc.getQualName(),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true);
											sBuff.Empty();
											for (UINT qual = 0;qual < recPrlData_perc.getInts().size();qual++)
											{
												sBuff.Format(_T("%d"),recPrlData_perc.getInts()[qual]);
												setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true);
											}	// for (UINT qual = 0;qual < recPrlData_perc.getInts().size(),qual++)
											setHTM_end_table_column();
										}	// for (UINT prl_data1 = 0;prl_data1 < vecPrlData_perc.size();prl_data1++)
									}	// if (vecPrlData_perc[spc].getInts().size() > 0)
								}	// if (spc < vecPrlData_perc.size())

								if (m_bShowPriceByQual)
								{

									// Calculate Price per diameterclass and qualitydescription; 090109 p�d
									if (vecPrlData.size() == vecPrlData_perc.size())
									{
										for (UINT qsum = 0;qsum < vecPrlData_perc.size();qsum++)
										{
											recPrlData = vecPrlData[qsum];
											recPrlData_perc = vecPrlData_perc[qsum];
											if (recPrlData.getInts().size() == recPrlData_perc.getInts().size())
											{
												for (UINT qsum_1 = 0;qsum_1 < recPrlData.getInts().size();qsum_1++)
												{
													// OBS! qsum_1 = column (dcls); 090109 p�d
													mapPricePerQual[qsum_1] += recPrlData.getInts()[qsum_1]*recPrlData_perc.getInts()[qsum_1]/100.0;
												}	// for (UINT qsum_1 = 0;qsum_1 < recPrlData.getInts().size();qsum_1++)
											}	// if (recPrlData.getInts().size() == recPrlData_perc.getInts().size())
										}							
										
										if (mapPricePerQual.size() > 0)
										{
											setHTM_start_table_column();
											setHTM_table_column(xml.str(IDS_STRING1902),150,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,true,false,false);
											for (UINT map = 0;map < mapPricePerQual.size();map++)
											{
												fPricePerQual = mapPricePerQual[map];
												sBuff.Format(_T("%.0f"),fPricePerQual);

												setHTM_table_column(sBuff,50,1,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_RIGHT,true,true);
											}	// for (UINT map = 0;map < mapPricePerQual.size();map++)
											setHTM_end_table_column();
										}	// if (mapPricePerQual.size() > 0)

									}
									mapPricePerQual.clear();

								}	// if (m_bShowPriceByQual)

							}	// for (int i = 0;i < sarrQDesc.GetCount();i++)
							endHTM_table();
						}	// if (sarrQDesc.GetCount() > 0)
						//////////////////////////////////////////////////////////////////////////////////////////
						// END --- ADDING QUALITYDESCRIPTIONS; 090902 P�D		
						//////////////////////////////////////////////////////////////////////////////////////////
						
					}	// if (!bIsAvgPrl)	
					// Don't add a line after the last specie; 090902 p�d
					if (spc < vecSpc.size()-1) setHTML_line(HTM_HR_NOSHADE,1);

				}	// for (UINT spc = 0;spc < vecSpc.size();spc++)
			}	// if (vecSpc.size() > 0)
			
			xml.clean();
		}
	}

	delete parser;

	sarrQDesc.RemoveAll();
	pRec = NULL;
	vecAss.clear();
	vecDCLS.clear();
	vecPrlData.clear();
	vecPrlData_perc.clear();
	mapPricePerQual.clear();

	// End of HTML-file
	endHTM();
	// Save to disk and load into Viewer; 090901 p�d
	showHTM();
}

void CPrlViewAndPrint::printOut()
{
	printOutHTM();
}


