#pragma once

#include "Resource.h"
#include "UMLandValueDB.h"
//Lagt till ny klass f�r att v�lja fler fastigheter att radera p� en g�ng Feature #2456 20111020 J�
class CPropertyRemoveSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CPropertyRemoveSelListFormView)
	//CString m_sGroupByThisField;
	//CString m_sGroupByBox;
	//CString m_sFieldChooser;

protected:
	CPropertyRemoveSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertyRemoveSelListFormView();

	
	BOOL setupReport(void);
	
	//void getProperties(void);

	//CXTPReportSubListControl m_wndSubList;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);
	void getProperties(int nObjId);
	void populateReport(void);
	
	int m_nObjId;
    CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	int m_nCurrentPropId;
	vecTransactionProperty m_vecPropertyData;

public:
	//enum { IDD = IDD_REPORTVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnRefresh();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};