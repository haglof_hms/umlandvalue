// P30TemplateFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "1950ForrestNormFrame.h"
#include "P30TemplateFormView.h"
#include "TemplateParser.h"
#include "SelectSpcDlg.h"

#include "ResLangFileReader.h"

// CP30TemplateFormView

IMPLEMENT_DYNCREATE(CP30TemplateFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CP30TemplateFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_EN_CHANGE(IDC_EDIT1, &CP30TemplateFormView::OnEnChangeEdit1)
END_MESSAGE_MAP()

CP30TemplateFormView::CP30TemplateFormView()
	: CXTResizeFormView(CP30TemplateFormView::IDD)
{

	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CP30TemplateFormView::~CP30TemplateFormView()
{
}

void CP30TemplateFormView::OnDestroy()
{
	m_vecSpecies.clear();
	m_vecTransactionTemplate.clear();
	
	m_wndReport.ResetContent();

	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();	
}

void CP30TemplateFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CP30TemplateFormView)
	DDX_Control(pDX, IDC_GROUP_P30_1, m_wndGroupP30_1);
	DDX_Control(pDX, IDC_GROUP_P30_2, m_wndGroupP30_2);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	//}}AFX_DATA_MAP
}

void CP30TemplateFormView::OnSetFocus(CWnd *wnd)
{

	if (m_vecTransactionTemplate.size() > 0)
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
	else
		setNavigationButtons(FALSE,FALSE);

	CXTResizeFormView::OnSetFocus(wnd);
}

void CP30TemplateFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit1.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1.SetReadOnly(TRUE);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit2.SetEnabledColor(BLACK,WHITE );
		m_wndEdit2.SetReadOnly(TRUE);
		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE );
		m_wndEdit3.SetEnabledColor(BLACK,WHITE );
		m_wndEdit3.SetReadOnly(TRUE);

		// Setup language filename; 080402 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport();

//		addSpecieConstaints();

		m_enumAction = TEMPLATE_NONE;

		getSpeciesFromDB();

		getObjectTemplatesFromDB();

		if (m_vecTransactionTemplate.size() > 0)
		{
			m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
			setEnableData(TRUE);
		}
		else
		{
			m_nDBIndex = -1;	// No Objects
			setNavigationButtons(FALSE,FALSE);
			setEnableData(FALSE);
		}

		populateData(m_nDBIndex);
		
		m_bInitialized = TRUE;
	}
}

BOOL CP30TemplateFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CP30TemplateFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CP30TemplateFormView::doSetNavigationBar()
{
	if (m_vecTransactionTemplate.size() > 0 || m_enumAction == TEMPLATE_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CP30TemplateFormView diagnostics

#ifdef _DEBUG
void CP30TemplateFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CP30TemplateFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CP30TemplateFormView message handlers

BOOL CP30TemplateFormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_P30_TEMPL, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				m_sMsgCap = (xml->str(IDS_STRING229));
				m_sMsgNoTemplateName = xml->str(IDS_STRING2503);
				m_sMsgNoTemplateName2.Format(_T("%s\n\n%s\n\n"),
					(xml->str(IDS_STRING2503)),
					(xml->str(IDS_STRING2504)));
				m_sMsgRemoveTemplate = (xml->str(IDS_STRING2505));

				m_sMsgNoTemplatesRegistered.Format(_T("%s\n\n%s\n\n"),
					xml->str(IDS_STRING6000),
					xml->str(IDS_STRING6001));

				m_sMsgCharError.Format(_T("%s < > /"),xml->str(IDS_STRING2526));

				m_wndLbl1.SetWindowText((xml->str(IDS_STRING2500)));
				m_wndLbl2.SetWindowText((xml->str(IDS_STRING2501)));
				m_wndLbl3.SetWindowText((xml->str(IDS_STRING2502)));

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING2202)), 100));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, _T(""), 1));
				pCol->SetVisible(FALSE);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING2203)), 60));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING2204)), 60));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 080402 p�d
				CWnd *pWnd = GetDlgItem(ID_REPORT_P30_TEMPL);
				if (pWnd != NULL)
				{
					int tpp = TwipsPerPixel();
					pWnd->SetWindowPos(&CWnd::wndTop,20*15/tpp,100*15/tpp,300*15/tpp,240*15/tpp,SWP_SHOWWINDOW);
				}

			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}
	return TRUE;
}

void CP30TemplateFormView::populateData(int idx)
{
	CString sName,sDoneBy,sNotes;
	vecObjectTemplate_p30_table vecP30;
	TemplateParser pars;
	CTransaction_elv_object *pObject = getActiveObject();
	int nObjID = -1;
	// Make sure we are within limits; 080402 p�d
	if (m_vecTransactionTemplate.size() > 0 &&
			idx >= 0 &&
			idx < (int)m_vecTransactionTemplate.size())
	{
		m_enumAction = TEMPLATE_OPEN;

		m_recActiveTemplate = m_vecTransactionTemplate[idx];

		if (pObject != NULL)
			nObjID = pObject->getObjID_pk();

		// Setup general data; 080402 p�d
		m_wndEdit1.SetWindowText((m_recActiveTemplate.getTemplateName()));
		m_wndEdit2.SetWindowText((m_recActiveTemplate.getCreatedBy()));
		m_wndEdit3.SetWindowText((m_recActiveTemplate.getTemplateNotes()));

		// Get (pars) information on P30 table, from xml-data in DB; 080402 p�d
		m_wndReport.ResetContent();
	
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			pars.getObjTmplP30(vecP30,sName,sDoneBy,sNotes);
			if (vecP30.size() > 0)
			{
				for (UINT i = 0;i < vecP30.size();i++)
				{
					CObjectTemplate_p30_table rec = vecP30[i];
					m_wndReport.AddRecord(new CP30ReportRec(CTransaction_elv_p30(rec.getSpcID(),
						nObjID,
						rec.getSpcID(),
						rec.getSpcName(),
						rec.getPrice(),
						rec.getPriceRel())));
				}	// for (UINT i = 0;i < vecP30.size();i++)
			}	// if (vecP30.size() > 0)
		}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))

		m_wndReport.Populate();
		m_wndReport.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);

		vecP30.clear();
	}
	else
	{
		m_wndEdit1.SetWindowText(_T(""));
		m_wndEdit2.SetWindowText(_T(""));
		m_wndEdit3.SetWindowText(_T(""));
		setEnableData(FALSE);
		m_enumAction = TEMPLATE_NONE;
		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplatesRegistered),(m_sMsgCap),MB_ICONASTERISK | MB_OK);

	}
	// Need to release ref. pointers; 080520 p�d
	pObject = NULL;

	doSetNavigationBar();

}

void CP30TemplateFormView::setupPopulationOfTemplate(BOOL setup_dbindex)
{
	getObjectTemplatesFromDB();

	if (setup_dbindex)
	{
		if (m_vecTransactionTemplate.size() > 0)
		{
			m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
		}
		else
		{
			m_nDBIndex = -1;	// No Objects
			setNavigationButtons(FALSE,FALSE);
			newP30Template();
		}
	}

	populateData(m_nDBIndex);
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CP30TemplateFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}


void CP30TemplateFormView::addSpecieConstaints(void)
{
/*
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport.GetColumns();
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 0; 071109 p�d
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_0);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 071109 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// Get species in pricelist for this trakt.
			// Get info. from esti_trakt_misc_data_table; 070509 p�d
			getSpeciesFromDB();
			// If there's species, add to Specie column in report; 070509 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)
*/
}

BOOL CP30TemplateFormView::getSpeciesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecSpecies.clear();
			m_pDB->getSpecies(m_vecSpecies);
			return TRUE;
		}
	}
	return FALSE;
}

void CP30TemplateFormView::getObjectTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecTransactionTemplate.clear();
			m_pDB->getObjectTemplates(m_vecTransactionTemplate,TEMPLATE_P30);
		}
	}
}

int CP30TemplateFormView::getSpcID(LPCTSTR spc_name)
{
	// Do we have any data; 080402 p�d
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			CTransaction_species rec = m_vecSpecies[i];
			if (rec.getSpcName().CompareNoCase(spc_name) == 0)
			{
				return  rec.getSpcID();
			}	// if (rec.getSpcName().Compare(spc_name) == 0)
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// if (m_vecSpecies.size() > 0)

	return -1;	// Not found; 080402 p�d
}

void CP30TemplateFormView::newP30Template(void)
{
	// Reset active template record (i.e. set id = -1); 080402 p�d
	m_recActiveTemplate = CTransaction_template();

	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(getUserName().MakeUpper());
	m_wndEdit3.SetWindowText(_T(""));

	m_wndReport.ResetContent();
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	m_wndEdit1.SetFocus();

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

}

void CP30TemplateFormView::addSpecieToReport(void)
{
	vecIntegers spc_used;
	CP30ReportRec *pRec = NULL;
	CTransaction_elv_p30 recP30;
	CTransaction_species recSpc;
	vecTransactionSpecies vecSpc;
	// Try to get species already used in Report; 081202 p�d
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CP30ReportRec *)pRows->GetAt(i)->GetRecord();
			recP30 = pRec->getRecP30();
			spc_used.push_back(recP30.getP30SpcID());
		}	// for (int i 0 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	CSelectSpcDlg *pDlg = new CSelectSpcDlg();
	if (pDlg != NULL)
	{
		pDlg->setSpeciesUsed(spc_used);
		if (pDlg->DoModal() == IDOK)
		{
			vecSpc = pDlg->getSpeciesSelected();
			if (vecSpc.size() > 0)
			{
				for (UINT i = 0;i < vecSpc.size();i++)
				{
					recSpc = vecSpc[i];
					recP30 = CTransaction_elv_p30(recSpc.getSpcID(),
																				m_recActiveTemplate.getID(),
																				recSpc.getSpcID(),
																				recSpc.getSpcName(),
																				0.0,
																				0.0);
					m_wndReport.AddRecord(new CP30ReportRec(recP30));
				}	// for (UINT i = 0;i < vecSpc.size();i++)
				
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}	// if (vecSpc.size() > 0)
		}	// if (pDlg->DoModal() == IDOK)
		delete pDlg;
	}	// if (pDlg != NULL)
}

void CP30TemplateFormView::removeSelectedSpecie(void)
{
	if (m_wndReport.GetRows()->GetCount() > 0)
	{
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow(); 
		if (pRow != NULL)
		{
			CXTPReportRecord *pRec = pRow->GetRecord();		
			if (pRec != NULL)
			{
				pRec->Delete();			
				m_wndReport.Populate();
				//m_wndReport.UpdateWindow();
				// Added 081006 p�d
				m_wndReport.RedrawControl();
			}
		}
	}
}

void CP30TemplateFormView::importP30Table(void)
{
	vecObjectTemplate_p30_table vecP30;
	CString sName,sDoneBy,sNotes,sCompareName,sTmplName;
	CString sXML;
	CString sXMLCompleted;
	CString sFilter;
	int nCountName = 0;
	CTransaction_template recAdded;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22703),P30_TABLE_EXTENSION,P30_TABLE_EXTENSION);
		}
		delete xml;
	}

	// Handles clik on open button
	CFileDialog dlg( TRUE, P30_TABLE_EXTENSION, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		setEnableData(TRUE);
		// Setup transport table; 081201 p�d
		TemplateParser pars;
		if (pars.LoadFromFile(dlg.GetPathName()))
		{
			pars.getObjTmplP30(vecP30,sName,sDoneBy,sNotes);
			pars.getXML(sXML);
			sXMLCompleted.Format(_T("%s%s"),XML_FILE_HEADER,sXML);

			// Check that the name isn't already used; 091027 p�d
			if (m_vecTransactionTemplate.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
				{
					sCompareName = m_vecTransactionTemplate[i].getTemplateName().Left(sName.GetLength());
					if (sCompareName.CompareNoCase(sName) == 0)
						nCountName++;
				}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			}	// if (m_vecTransactionTemplate.size() > 0)
			
			if (nCountName > 0) sTmplName.Format(_T("%s(%d)"),sName,nCountName,sNotes);
			else sTmplName = sName;
			m_wndEdit1.SetWindowTextW(sTmplName);
			m_wndEdit2.SetWindowTextW(sDoneBy);
			if (sDoneBy.IsEmpty())
				sDoneBy = getUserName().MakeUpper();
			m_wndEdit3.SetWindowTextW(sNotes);

			// Only ADD A NEW "Under utveckling"; 081201 p�d
			if (m_pDB != NULL)
			{
				m_pDB->addObjectTemplate(CTransaction_template(-1,sTmplName,TEMPLATE_P30,sXMLCompleted,sNotes,sDoneBy));
			}	// if (m_pDB != NULL)
			// We need to reload templates from database
			// and set m_nDBIncdex to point to the last (just created)
			// template; 081201 p�d
			getObjectTemplatesFromDB();
			if (m_vecTransactionTemplate.size() > 0)
				m_nDBIndex = m_vecTransactionTemplate.size() - 1;	// Point to last item
		}	// if (pars.LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
		populateData(m_nDBIndex);

		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1)); 
	}	// if(dlg.DoModal() == IDOK)
}

void CP30TemplateFormView::exportP30Table(void)
{
	CString sName;
	CString sFilter;
	//-------------------------------------------------------------------
	// Save data before tryin' to export; 091012 p�d
	if (!saveP30Template(0)) return;
	getObjectTemplatesFromDB();
	populateData(m_nDBIndex);
	//-------------------------------------------------------------------

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22703),P30_TABLE_EXTENSION,P30_TABLE_EXTENSION);
		}
		delete xml;
	}

	sName = m_recActiveTemplate.getTemplateName();
	scanFileName(sName);
	if (sName.Right(6) != P30_TABLE_EXTENSION)
		sName += P30_TABLE_EXTENSION;
	// Handles clik on open button
	CFileDialog dlg( FALSE, P30_TABLE_EXTENSION, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER,sFilter, this);

	if (dlg.DoModal() == IDOK)
	{
		TemplateParser pars;
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			sName = dlg.GetPathName();
			if (sName.Right(6) != P30_TABLE_EXTENSION)
				sName += P30_TABLE_EXTENSION;
			pars.SaveToFile(sName);
		}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
	}
}

BOOL CP30TemplateFormView::saveP30Template(short action)
{
	CString sXML;
	int nID = -1;
	CString sTemplateName;
	CString sCreatedBy;
	CString sNotes;
	int nSpcID;
	CString sSpcName;
	double fPrice;
	double fPriceRel;

	// Check if there's any data to check; 090204 p�d
	if (!m_bDataEnabled) return TRUE;
	// Collect general data; 080402 p�d
	sTemplateName = m_wndEdit1.getText();
	sCreatedBy = m_wndEdit2.getText();
	sNotes = m_wndEdit3.getText();

	if (sTemplateName.IsEmpty())
	{
		if (action == 0)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		else if (action == 1)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE; // HMS-9 20180409 J� ej spara prislista utan namn eller valt omr�de
		}
		else if (action == 2)
		{
			if (::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName2),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
				return TRUE;
			else
			{
				m_wndEdit1.SetFocus();						
				return FALSE;
			}
		}	// else if (action == 2)
	}	// if (sTemplateName.IsEmpty())
	else
	{

		CP30ReportRec *pRec = NULL;
		CXTPReportRecords *pRecs = m_wndReport.GetRecords();
		if (pRecs != NULL)
		{
			// Start by setting header and data-tag
			sXML = XML_FILE_HEADER;
			sXML += NODE_P30_TMPL_START;

			CString sTmpName = sTemplateName;
			sTmpName.Replace(_T("&"), _T("&amp;"));
			sTmpName.Replace(_T("<"), _T("&lt;"));
			sTmpName.Replace(_T(">"), _T("&gt;"));

			CString sTmpNotes = sNotes;
			sTmpNotes.Replace(_T("&"), _T("&amp;"));
			sTmpNotes.Replace(_T("<"), _T("&lt;"));
			sTmpNotes.Replace(_T(">"), _T("&gt;"));

			sXML += formatData(NODE_P30_TMPL_START_DATA, sTmpName, sCreatedBy, sTmpNotes);
			// Add data by rows in report; 080402 p�d
			for (int i = 0;i < pRecs->GetCount();i++)
			{
				pRec = (CP30ReportRec *)pRecs->GetAt(i);
				sSpcName = pRec->getColumnText(COLUMN_0);
				nSpcID = getSpcID(sSpcName);
				fPrice = pRec->getColumnFloat(COLUMN_2);
				fPriceRel = pRec->getColumnFloat(COLUMN_3);
				sXML += formatData(NODE_P30_TMPL_DATA,nSpcID,sSpcName,fPrice,fPriceRel);
			}
			sXML += NODE_P30_TMPL_END_DATA;
			sXML += NODE_P30_TMPL_END;
		}

		if (m_pDB != NULL)
		{
			nID = m_recActiveTemplate.getID();
			if (m_pDB->addObjectTemplate(CTransaction_template(nID,sTemplateName,TEMPLATE_P30,sXML,sNotes,sCreatedBy)))
			{
				setupPopulationOfTemplate(TRUE);	// A new item is added; 080402 p�d
			}
			else
			{
				m_pDB->updObjectTemplate(CTransaction_template(nID,sTemplateName,TEMPLATE_P30,sXML,sNotes,sCreatedBy));
				setupPopulationOfTemplate(FALSE);	// Just reload data and populate; 080402 p�d
			}
		}
	} // else

	return TRUE;
}

void CP30TemplateFormView::removeP30Template(void)
{
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (m_pDB != NULL)
		{
			m_pDB->delObjectTemplate(m_recActiveTemplate);
			setupPopulationOfTemplate(TRUE);	// A new item is added; 080402 p�d
		}
		m_enumAction = TEMPLATE_NONE;
	}
}

void CP30TemplateFormView::setEnableData(BOOL enable)
{
	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit3.EnableWindow(enable);
	m_wndEdit3.SetReadOnly(!enable);

	CMDIP30TemplateFrame *pFrame = (CMDIP30TemplateFrame*)getFormViewByID(IDD_FORMVIEW2)->GetParent();
	if (pFrame != NULL)
		pFrame->setEnableToolbar(enable);

	if (enable)	m_wndEdit1.SetFocus();

	m_bDataEnabled = enable;
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CP30TemplateFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			saveP30Template(1);
			newP30Template();
			setEnableData(TRUE);
			m_enumAction = TEMPLATE_NEW;
			break;
		}	// case ID_NEW_ITEM :
		case ID_SAVE_ITEM :
		{
			saveP30Template(1);
			break;
		}	// case ID_SAVE_ITEM :	
		case ID_DELETE_ITEM :
		{
			removeP30Template();
			break;
		}	// case ID_DELETE_ITEM :	

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveP30Template(-1);
			m_nDBIndex = 0;
			setNavigationButtons(FALSE,TRUE);
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveP30Template(-1);	
			m_nDBIndex--;
			if (m_nDBIndex < 0)
				m_nDBIndex = 0;
			if (m_nDBIndex == 0)
			{
				setNavigationButtons(FALSE,TRUE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveP30Template(-1);
			m_nDBIndex++;
			if (m_nDBIndex > ((int)m_vecTransactionTemplate.size() - 1))
				m_nDBIndex = (int)m_vecTransactionTemplate.size() - 1;
					
			if (m_nDBIndex == (UINT)m_vecTransactionTemplate.size() - 1)
			{
				setNavigationButtons(TRUE,FALSE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			saveP30Template(-1);
			m_nDBIndex = (UINT)m_vecTransactionTemplate.size()-1;
			setNavigationButtons(TRUE,FALSE);	
			populateData(m_nDBIndex);
			break;
		}	// case ID_NEW_ITEM :

	}	// switch (wParam)
	return 0L;
}

void CP30TemplateFormView::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());
		m_wndEdit1.SetFocus();
	}
}

//*****************************************************************************************************
// CP30NewNormTemplateFormView form view 2009-04-15 P�D
//*****************************************************************************************************

IMPLEMENT_DYNCREATE(CP30NewNormTemplateFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CP30NewNormTemplateFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON2_1, &CP30NewNormTemplateFormView::OnBnClickedButton21)
	ON_BN_CLICKED(IDC_BUTTON2_2, &CP30NewNormTemplateFormView::OnBnClickedButton22)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL_4, OnSelectedChanged)
	ON_CBN_SELCHANGE(IDC_COMBO2_1, &CP30NewNormTemplateFormView::OnCbnSelchangeCombo21)
	ON_EN_CHANGE(IDC_EDIT2_1, &CP30NewNormTemplateFormView::OnEnChangeEdit1)
END_MESSAGE_MAP()

CP30NewNormTemplateFormView::CP30NewNormTemplateFormView()
	: CXTResizeFormView(CP30NewNormTemplateFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CP30NewNormTemplateFormView::~CP30NewNormTemplateFormView()
{
}

void CP30NewNormTemplateFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();	
}

void CP30NewNormTemplateFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CP30NewNormTemplateFormView)
	DDX_Control(pDX, IDC_GROUP2_1, m_wndGroup2_1);
	DDX_Control(pDX, IDC_GROUP2_2, m_wndGroup2_2);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL2_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL2_4, m_wndLbl4);
	
	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT2_3, m_wndEdit3);

	DDX_Control(pDX, IDC_COMBO2_1, m_wndCBox1);

	DDX_Control(pDX, IDC_BUTTON2_1, m_wndAdd);
	DDX_Control(pDX, IDC_BUTTON2_2, m_wndRemove);

	//}}AFX_DATA_MAP
}

void CP30NewNormTemplateFormView::OnSetFocus(CWnd *wnd)
{

	if (m_vecTransactionTemplate.size() > 0)
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
	else
		setNavigationButtons(FALSE,FALSE);


	CXTResizeFormView::OnSetFocus(wnd);
}

void CP30NewNormTemplateFormView::OnInitialUpdate()
{
	CString sPine,sSpruce,sBirch;

	CXTResizeFormView::OnInitialUpdate();

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL_4);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	if (m_wndTabControl.GetSafeHwnd())
	{
		CRect rect1;
		CRect rect2;
		m_wndGroup2_1.GetClientRect(rect1);
		m_wndGroup2_2.GetClientRect(rect2);
		setResize(&m_wndTabControl,20,120,rect1.Width()-40,rect2.Height()-4);
//		setResize(&m_wndTabControl,rect2.Width()+4,120,rect1.Width()-rect2.Width()-2,rect2.Height()-4);
	}

	m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
	m_wndEdit1.SetEnabledColor(BLACK,WHITE );
	m_wndEdit1.SetReadOnly(TRUE);
	m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE );
	m_wndEdit2.SetEnabledColor(BLACK,WHITE );
	m_wndEdit2.SetReadOnly(TRUE);
	m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE );
	m_wndEdit3.SetEnabledColor(BLACK,WHITE );
	m_wndEdit3.SetReadOnly(TRUE);
	m_wndEdit3.SetLimitText(1000);

	if (! m_bInitialized )
	{

		// Setup language filename; 080402 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (m_pDB != NULL)
			m_pDB->getSpecies(m_vecSpecies);
		// Seek name of species that have specieid: 1 (Tall), 2 (Gran) and 3 (L�v)
		if (m_vecSpecies.size() > 0)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				if (m_vecSpecies[i].getSpcID() == 1) sPine = m_vecSpecies[i].getSpcName();
				if (m_vecSpecies[i].getSpcID() == 2) sSpruce = m_vecSpecies[i].getSpcName();
				if (m_vecSpecies[i].getSpcID() == 3) sBirch = m_vecSpecies[i].getSpcName();
			}
		}

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_wndGroup2_2.SetWindowTextW(xml.str(IDS_STRING2519));
				m_wndGroup2_2.ShowWindow(SW_HIDE);
				m_wndAdd.SetWindowTextW(xml.str(IDS_STRING2520));
				m_wndAdd.ShowWindow(SW_HIDE);
				m_wndRemove.SetWindowTextW(xml.str(IDS_STRING2521));
				m_wndRemove.ShowWindow(SW_HIDE);

				m_sMsgCap = (xml.str(IDS_STRING229));
				m_sMsgNoTemplateName = xml.str(IDS_STRING2522);
				m_sMsgNoTemplateName2.Format(_T("%s\n\n%s\n\n"),
					(xml.str(IDS_STRING2522)),
					(xml.str(IDS_STRING2504)));
				m_sMsgRemoveTemplate = xml.str(IDS_STRING2505);
				m_sMsgNameUsedTemplate = xml.str(IDS_STRING2523);
				m_sMsgChangeGArea = xml.str(IDS_STRING2525);
				m_sMsgCharError.Format(_T("%s < > /"),xml.str(IDS_STRING2526));
				
				m_sMsgPRelError1 = xml.str(IDS_STRING2527);
				m_sMsgPRelError2 = xml.str(IDS_STRING2528);

				m_wndLbl1.SetWindowTextW(xml.str(IDS_STRING2500));
				m_wndLbl2.SetWindowTextW(xml.str(IDS_STRING2501));
				m_wndLbl3.SetWindowTextW(xml.str(IDS_STRING2508));
				m_wndLbl4.SetWindowTextW(xml.str(IDS_STRING2502));

				// "Tillv�xtomr�den"
				m_sarrAreas.Add(xml.str(IDS_STRING2509));
				m_sarrAreas.Add(xml.str(IDS_STRING2510));
				m_sarrAreas.Add(xml.str(IDS_STRING2511));
				m_sarrAreas.Add(xml.str(IDS_STRING2512));
				m_sarrAreas.Add(xml.str(IDS_STRING2513));
				m_sarrAreas.Add(xml.str(IDS_STRING2514));
				// Add to m_wndCBox1
				m_wndCBox1.ResetContent();
				m_wndCBox1.AddString(m_sarrAreas.GetAt(0));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(1));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(2));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(3));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(4));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(5));
				m_wndCBox1.SetCurSel(-1);	// No selection yet; 090415 p�d

				if (m_wndTabControl.GetSafeHwnd())
				{
					AddView(RUNTIME_CLASS(CAddP30NewNormFormView), sPine, 3,1);		// Pine
					AddView(RUNTIME_CLASS(CAddP30NewNormFormView), sSpruce, 3,2);	// Spruce
					//AddView(RUNTIME_CLASS(CAddP30NewNormFormView), sBirch, 3,3);	// Birch
					AddView(RUNTIME_CLASS(CAddP30NewNormFormView), _T("L�v"), 3,3);	// Birch
				}	// if (m_wndTabControl.GetSafeHwnd())

				xml.clean();
			}	// if (xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))


		m_enumAction = TEMPLATE_NONE;

		getObjectTemplatesFromDB();

		if (m_vecTransactionTemplate.size() > 0)
		{
			m_nDBIndex = m_vecTransactionTemplate.size()-1;
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
			setEnableData(TRUE);
		}
		else
		{
			m_nDBIndex = -1;
			setNavigationButtons(FALSE,FALSE);
			setEnableData(FALSE);
		}
		populateData(m_nDBIndex);

		m_bInitialized = TRUE;
	}
}

BOOL CP30NewNormTemplateFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CP30NewNormTemplateFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CP30NewNormTemplateFormView diagnostics

#ifdef _DEBUG
void CP30NewNormTemplateFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CP30NewNormTemplateFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CP30NewNormTemplateFormView::doSetNavigationBar()
{
	if (m_vecTransactionTemplate.size() > 0 || m_enumAction == TEMPLATE_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CP30NewNormTemplateFormView message handlers

void CP30NewNormTemplateFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CP30NewNormTemplateFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

LRESULT CP30NewNormTemplateFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{

		case ID_NEW_ITEM :
		{
			saveP30NewNormTemplate(1);
			newP30NewNormTemplate();
			setEnableData(TRUE);
			m_enumAction = TEMPLATE_NEW;
			break;
		}	// case ID_NEW_ITEM :
		case ID_SAVE_ITEM :
		{
			saveP30NewNormTemplate(1);
			break;
		}	// case ID_SAVE_ITEM :	
		case ID_DELETE_ITEM :
		{
			removeP30NewNormTemplate();
			break;
		}	// case ID_DELETE_ITEM :	

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30NewNormTemplate(-1))
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30NewNormTemplate(-1))
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
					setNavigationButtons(FALSE,TRUE);
				else
					setNavigationButtons(TRUE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30NewNormTemplate(-1))
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((int)m_vecTransactionTemplate.size() - 1))
					m_nDBIndex = (int)m_vecTransactionTemplate.size() - 1;
					
				if (m_nDBIndex == (UINT)m_vecTransactionTemplate.size() - 1)
					setNavigationButtons(TRUE,FALSE);
				else
					setNavigationButtons(TRUE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30NewNormTemplate(-1))
			{
				m_nDBIndex = (UINT)m_vecTransactionTemplate.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	}	// switch (wParam)
	return 0L;
}

void CP30NewNormTemplateFormView::populateData(int idx)
{
	TemplateParser pars;
	CString sAreaName;
	CString sNotes;
	vecObjectTemplate_p30_nn_table vecP30_nn;

	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	int nSpcID = 0;
	// Make sure we are within limits; 080402 p�d
	if (m_vecTransactionTemplate.size() > 0 &&
			idx >= 0 &&
			idx < (int)m_vecTransactionTemplate.size())
	{
		m_recActiveTemplate = m_vecTransactionTemplate[idx];

		m_enumAction = TEMPLATE_OPEN;

		// Setup general data; 080402 p�d
		m_wndEdit1.SetWindowText((m_recActiveTemplate.getTemplateName()));
		m_wndEdit2.SetWindowText((m_recActiveTemplate.getCreatedBy()));
		m_wndEdit3.SetWindowText((m_recActiveTemplate.getTemplateNotes()));
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			pars.getObjTmplP30_nn(vecP30_nn,sAreaName,&m_nAreaIndex);
			m_wndCBox1.SetCurSel(m_nAreaIndex);
			if (nNumOfTabs > 0 && vecP30_nn.size() > 0)
			{
				for (int i = 0;i < nNumOfTabs;i++)
				{
					CAddP30NewNormFormView *pView = getTabFormView(i,&nSpcID /* Spc id */);
					if (pView != NULL)
					{
						pView->setReport(vecP30_nn,nSpcID);
					}	// if (pView != NULL)
				}	// for (int i = 0;i < nNumOfTabs;i++)
			}	// if (nNumOfTabs > 0)
		}

	}
	else
	{
		m_wndEdit1.SetWindowText(_T(""));	// 090428 p�d
		m_wndEdit2.SetWindowText(_T(""));	// 090428 p�d
		m_wndEdit3.SetWindowText(_T(""));	// 090428 p�d
		m_wndCBox1.SetCurSel(-1);	// No selection; 090428 p�d
		setEnableData(FALSE);
		newP30NewNormTemplate();
		m_enumAction = TEMPLATE_NONE;
		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplatesRegistered),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
	}

	doSetNavigationBar();
}

CString CP30NewNormTemplateFormView::getSpcNameFromID(int id)
{
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == id) 
				return m_vecSpecies[i].getSpcName();
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// if (m_vecSpecies.size() > 0)

	return _T("");
}

void CP30NewNormTemplateFormView::newP30NewNormTemplate(void)
{
	int nData = -1;
	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	// Clear data; 090417 p�d
	m_wndEdit1.SetWindowTextW(_T(""));
	m_wndEdit2.SetWindowTextW(getUserName().MakeUpper());
	m_wndEdit3.SetWindowTextW(_T(""));
	m_wndCBox1.SetCurSel(-1);

	if (nNumOfTabs > 0)
	{
		for (int i = 0;i < nNumOfTabs;i++)
		{
			CAddP30NewNormFormView *pView = getTabFormView(i,&nData /* Spc id */);
			if (pView != NULL)
			{
				pView->resetReport();
			}	// if (pView != NULL)
		}	// for (int i = 0;i < nNumOfTabs;i++)
		m_wndTabControl.SetSelectedItem(m_wndTabControl.getTabPage(0));
	}	// if (nNumOfTabs > 0)
	m_recActiveTemplate = CTransaction_template();

	m_wndEdit1.SetFocus();


	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

}

void CP30NewNormTemplateFormView::removeP30NewNormTemplate(void)
{
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (m_pDB != NULL)
		{
			if (m_pDB->delObjectTemplate(m_recActiveTemplate))
			{
				getObjectTemplatesFromDB();
				if (m_vecTransactionTemplate.size() > 0)
				{
					m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
					setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
				}	// if (m_vecTransactionTemplate.size() > 0)
				else
				{
					m_nDBIndex = -1;	// No Objects
					setNavigationButtons(FALSE,FALSE);
					//newP30NewNormTemplate();
				}
			}	// if (m_pDB->delObjectTemplate(m_recActiveTemplate))
		}	// if (m_pDB != NULL)
		populateData(m_nDBIndex);
	}	// if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
}

// Save to database; 090416 p�d
BOOL CP30NewNormTemplateFormView::saveP30NewNormTemplate(short action)
{
	short nRetValue;
	CTransaction_template rec;
	BOOL bNameAlreadyUsed = FALSE;
	// If not enabled, nothing to save; 090428 p�d
	if (!m_bDataEnabled)
	{
		if (action == 1) return FALSE;
		else if (action == 2) return TRUE;
	}	// if (!m_bDataEnabled)
	// Do a check that user entered a name and selcted an area; 090416 p�d
	if (m_wndEdit1.getText().IsEmpty() || m_wndCBox1.GetCurSel() == -1)
	{
		if (action == 0)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		else if (action == 1)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE;	// HMS-9 20180409 J� ej spara prislista utan namn eller valt omr�de
		}
		else if (action == 2)
		{
			if (::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName2),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
				return TRUE;
			else
			{
				m_wndEdit1.SetFocus();						
				return FALSE;
			}
		}	// else if (action == 2)
	}
	else
	{
		// We'll also check if name entered, have already been used; 090428 p�d
		if (m_vecTransactionTemplate.size() > 0)
		{
			for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			{
				rec = m_vecTransactionTemplate[i];
				if (rec.getTemplateName().CompareNoCase(m_wndEdit1.getText()) == 0 && i != m_nDBIndex)
				{
					bNameAlreadyUsed = TRUE;
					break;
				}
			}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		}	// if (m_vecTransactionTemplate.size() > 0)
	}

	if (!bNameAlreadyUsed)
	{

		BOOL bCreate = createXMLFile(&nRetValue);
		if (bCreate)
		{
			getObjectTemplatesFromDB();
			if (action > -1)
			{
				if (nRetValue == 0)
					m_nDBIndex = m_vecTransactionTemplate.size()-1;
				setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1)); 
				populateData(m_nDBIndex);
			}
		}
		return bCreate;
	}
	else
	{
		::MessageBox(this->GetSafeHwnd(),(m_sMsgNameUsedTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}
	return FALSE;
}

// Create the XML-file here; 090416 p�d
BOOL CP30NewNormTemplateFormView::createXMLFile(short *ret_value)
{
	CString sXML,sData,sArea,sSpcName,sName,sDoneBy,sNotes,sMsg;
	CStringArray sarrSpcData,sarrErr;
	int nData = 0;
	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	int nCBoxIndex = m_wndCBox1.GetCurSel();
	CTransaction_species rec;
	short nCnt = 1;

	// Setup XML-header data here
	sXML = XML_FILE_HEADER;
	sXML += NODE_P30NN_START;

	// Name of template; 090416 p�d
	sName = m_wndEdit1.getText();
	CString sNameTmp = sName;
	sNameTmp.Replace(_T("&"), _T("&amp;"));
	sNameTmp.Replace(_T("<"), _T("&lt;"));
	sNameTmp.Replace(_T(">"), _T("&gt;"));
	sData.Format(NODE_P30NN_NAME,sNameTmp);
	sXML += sData;
	// Done by template; 090416 p�d
	sDoneBy = m_wndEdit2.getText();
	sData.Format(NODE_P30NN_DONE_BY,sDoneBy);
	sXML += sData;
	// Area for template; 090416 p�d
	m_wndCBox1.GetWindowTextW(sArea);
	sData.Format(NODE_P30NN_AREA,sArea,nCBoxIndex);
	sXML += sData;
	// Notes template; 090416 p�d
	sNotes = m_wndEdit3.getText();
	CString sNotesTmp = sNotes;
	sNotesTmp.Replace(_T("&"), _T("&amp;"));
	sNotesTmp.Replace(_T("<"), _T("&lt;"));
	sNotesTmp.Replace(_T(">"), _T("&gt;"));
	sData.Format(NODE_P30NN_NOTES,sNotesTmp);
	sXML += sData;

	if (nNumOfTabs > 0)
	{
		sarrErr.RemoveAll();
		sarrErr.Add(m_sMsgPRelError1);
		sarrErr.Add(m_sMsgPRelError2);
		sarrErr.Add(L"");
		for (int i = 0;i < nNumOfTabs;i++)
		{
			CAddP30NewNormFormView *pView = getTabFormView(i,&nData /* Spc id */);
			if (pView != NULL)
			{
				// Specie data; 090416 p�d
				sSpcName = getSpcNameFromID(nData);
				// Check added 101006 p�d
				pView->checkData(sSpcName,sarrErr);
			}
		}
		if (sarrErr.GetCount() > 3)
		{
			for (short ii = 0;ii < sarrErr.GetCount();ii++)
				sMsg += sarrErr.GetAt(ii) + L"\n";
			::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		for (int i = 0;i < nNumOfTabs;i++)
		{
			sarrSpcData.RemoveAll();
			CAddP30NewNormFormView *pView = getTabFormView(i,&nData /* Spc id */);
			if (pView != NULL)
			{
				// Specie data; 090416 p�d
				sSpcName = getSpcNameFromID(nData);
				// Check added 101006 p�d
				pView->getData(sarrSpcData);

				if (nData == 1) sData.Format(NODE_P30NN_SPC_START,sSpcName,nData,_T("T"));				// Pine (Tall)
				else if (nData == 2) sData.Format(NODE_P30NN_SPC_START,sSpcName,nData,_T("G"));	// Spruce (Gran)
				else if (nData == 3) sData.Format(NODE_P30NN_SPC_START,sSpcName,nData,_T("B"));	// Birch (Bj�rk)
				sXML += sData;
				
				if (sarrSpcData.GetCount() > 0)
				{
					for (int i1 = 0;i1 < sarrSpcData.GetCount();i1++)
					{
						sXML += sarrSpcData.GetAt(i1);
					}	// for (int i1 = 0;i1 < sarrSpcData.GetCount();i1++)
				}
				sXML += NODE_P30NN_SPC_END;

				// Add relations to Pine and Spruce. We don't need to do relations for
				// birch, because not relted to pine or spruce is related to birch; 090608 p�d

				pView = NULL;
			}	// if (pView != NULL)
		}	// for (int i = 0;i < nNumOfTabs;i++)
	}	// if (nNumOfTabs > 0)

	// XML-file end here; 090416 p�d
	sXML += NODE_P30NN_END;

	// Save to database; 090416 p�d
	if (m_pDB != NULL)
	{
		if (!sName.IsEmpty())
		{
			*ret_value = 0;
			if (!m_pDB->addObjectTemplate(CTransaction_template(m_recActiveTemplate.getID(),sName,TEMPLATE_P30_NEW_NORM,sXML,sNotes,sDoneBy)))
			{
				*ret_value = 1;
				m_pDB->updObjectTemplate(CTransaction_template(m_recActiveTemplate.getID(),sName,TEMPLATE_P30_NEW_NORM,sXML,sNotes,sDoneBy));
			}
		}
	}

	return TRUE;

}

void CP30NewNormTemplateFormView::importP30Table(void)
{
	CString sXML;
	CString sName,sCompareName,sTmplName;
	CString sDoneBy;
	CString sNotes;
	CString sXMLCompleted;
	CString sFilter;
	int nCountName = 0;
	CTransaction_template recAdded;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22703),P30_NN_TABLE_EXTENSION,P30_NN_TABLE_EXTENSION);
		}
		delete xml;
	}

	// Handles clik on open button
	CFileDialog dlg( TRUE, P30_NN_TABLE_EXTENSION, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		setEnableData(TRUE);
			// Setup transport table; 081201 p�d
		TemplateParser pars;
		if (pars.LoadFromFile(dlg.GetPathName()))
		{
			pars.getXML(sXML);
			pars.getObjTmplP30_nn_name(sName);
			pars.getObjTmplP30_nn_done_by(sDoneBy);
			if (sDoneBy.IsEmpty())
				sDoneBy = getUserName().MakeUpper();
			pars.getObjTmplP30_nn_notes(sNotes);
			// Check that the name isn't already used; 091027 p�d
			if (m_vecTransactionTemplate.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
				{
					sCompareName = m_vecTransactionTemplate[i].getTemplateName().Left(sName.GetLength());
					if (sCompareName.CompareNoCase(sName) == 0)
						nCountName++;
				}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			}	// if (m_vecTransactionTemplate.size() > 0)
			
			if (nCountName > 0) sTmplName.Format(_T("%s(%d)"),sName,nCountName);
			else sTmplName = sName;

			sXMLCompleted.Format(_T("%s%s"),XML_FILE_HEADER,sXML);
			// Only ADD A NEW "Under utveckling"; 081201 p�d
			if (m_pDB != NULL)
			{
				m_pDB->addObjectTemplate(CTransaction_template(-1,sTmplName,TEMPLATE_P30_NEW_NORM,sXMLCompleted,sNotes,sDoneBy));
			}	// if (m_pDB != NULL)
			// We need to reload templates from database
			// and set m_nDBIncdex to point to the last (just created)
			// template; 081201 p�d
			getObjectTemplatesFromDB();
			if (m_vecTransactionTemplate.size() > 0)
				m_nDBIndex = m_vecTransactionTemplate.size() - 1;	// Point to last item
		}	// if (pars.LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
		populateData(m_nDBIndex);

		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1)); 
	}	// if(dlg.DoModal() == IDOK)

}

void CP30NewNormTemplateFormView::exportP30Table(void)
{
	CString sName;
	CString sFilter;
	//-------------------------------------------------------------------
	// Save data before tryin' to export; 091012 p�d
	if (!saveP30NewNormTemplate(0)) return;
	getObjectTemplatesFromDB();
	populateData(m_nDBIndex);
	//-------------------------------------------------------------------

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22703),P30_NN_TABLE_EXTENSION,P30_NN_TABLE_EXTENSION);
		}
		delete xml;
	}

	sName = m_recActiveTemplate.getTemplateName();
	scanFileName(sName);
	if (sName.Right(7) != P30_NN_TABLE_EXTENSION)
		sName += P30_NN_TABLE_EXTENSION;
	// Handles clik on open button
	CFileDialog dlg( FALSE, P30_NN_TABLE_EXTENSION, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER,sFilter, this);

	if (dlg.DoModal() == IDOK)
	{
		TemplateParser pars;
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			sName = dlg.GetPathName();
			if (sName.Right(7) != P30_NN_TABLE_EXTENSION)
				sName += P30_NN_TABLE_EXTENSION;
			pars.SaveToFile(sName);
		}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
	}
}

void CP30NewNormTemplateFormView::getObjectTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecTransactionTemplate.clear();
			m_pDB->getObjectTemplates(m_vecTransactionTemplate,TEMPLATE_P30_NEW_NORM);
		}
	}
}

void CP30NewNormTemplateFormView::setEnableData(BOOL enable)
{
	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit3.EnableWindow(enable);
	m_wndEdit3.SetReadOnly(!enable);
	m_wndCBox1.EnableWindow(enable);

	m_wndAdd.EnableWindow(enable);
	m_wndRemove.EnableWindow(enable);

	CMDIP30NewNormTemplateFrame *pFrame = (CMDIP30NewNormTemplateFrame*)getFormViewByID(IDD_FORMVIEW11)->GetParent();
	if (pFrame != NULL)
		pFrame->setEnableToolbar(enable);

	if (m_wndTabControl.GetSafeHwnd())
		m_wndTabControl.EnableWindow(enable);
	
	if (enable)	m_wndEdit1.SetFocus();

	m_bDataEnabled = enable;

}

CAddP30NewNormFormView *CP30NewNormTemplateFormView::getSelectedTabFormView()
{
	m_tabManager = m_wndTabControl.getSelectedTabPage();
	if (m_tabManager)
	{
		CAddP30NewNormFormView* pView = DYNAMIC_DOWNCAST(CAddP30NewNormFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CAddP30NewNormFormView, pView);
		return pView;
	}
	return NULL;
}

CAddP30NewNormFormView *CP30NewNormTemplateFormView::getTabFormView(int tab_index,int *data)
{
	m_tabManager = m_wndTabControl.getTabPage(tab_index);
	if (m_tabManager)
	{
		CAddP30NewNormFormView* pView = DYNAMIC_DOWNCAST(CAddP30NewNormFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CAddP30NewNormFormView, pView);
		*data = m_tabManager->GetData();
		return pView;
	}
	return NULL;
}


// Added 090415 p�d; Add a row for "Bonitetsgr�ns"
void CP30NewNormTemplateFormView::OnBnClickedButton21()
{
	CAddP30NewNormFormView *pView = getSelectedTabFormView();
	if (pView != NULL)
	{
		pView->addRow();
		pView = NULL;
	}
}

// Added 090415 p�d; Remove last added row for "Bonitetsgr�ns"
void CP30NewNormTemplateFormView::OnBnClickedButton22()
{
	CAddP30NewNormFormView *pView = getSelectedTabFormView();
	if (pView != NULL)
	{
		pView->delRow();
		pView = NULL;
	}
}

BOOL CP30NewNormTemplateFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = NULL; //GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = NULL; //GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	CXTPTabManagerItem *pItem =	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	if (pItem != NULL) pItem->SetData(id);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CP30NewNormTemplateFormView::OnCbnSelchangeCombo21()
{
	CAddP30NewNormFormView *pPine = NULL;
	CAddP30NewNormFormView *pSpruce = NULL;
	CAddP30NewNormFormView *pBirch = NULL;
	int nData;
	int nSelIndex = m_wndCBox1.GetCurSel();
	if (nSelIndex == CB_ERR) return;

	if (::MessageBox(this->GetSafeHwnd(),m_sMsgChangeGArea,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
	{
		m_wndCBox1.SetCurSel(m_nAreaIndex);
		return;
	}

	pPine = getTabFormView(0,&nData);
	pSpruce = getTabFormView(1,&nData);
	pBirch = getTabFormView(2,&nData);
	switch (nSelIndex)
	{
		case 0 :	// "Omr�de 1"
			if (pPine != NULL) pPine->setupSIForP30(_T("1"),1);  
			if (pSpruce != NULL) pSpruce->setupSIForP30(_T("1"),2);
			if (pBirch != NULL) pBirch->setupSIForP30(_T("1"),3);	
		break;

		case 1 :	// "Omr�de 2"
			if (pPine != NULL) pPine->setupSIForP30(_T("2"),1);  
			if (pSpruce != NULL) pSpruce->setupSIForP30(_T("2"),2);
			if (pBirch != NULL) pBirch->setupSIForP30(_T("2"),3);	
		break;

		case 2 :	// "Omr�de 3"
			if (pPine != NULL) pPine->setupSIForP30(_T("3"),1);  
			if (pSpruce != NULL) pSpruce->setupSIForP30(_T("3"),2);
			if (pBirch != NULL) pBirch->setupSIForP30(_T("3"),3);	
		break;

		case 3 :	// "Omr�de 4A"
			if (pPine != NULL) pPine->setupSIForP30(_T("4A"),1);  
			if (pSpruce != NULL) pSpruce->setupSIForP30(_T("4A"),2);
			if (pBirch != NULL) pBirch->setupSIForP30(_T("4A"),3);	
		break;

		case 4 :	// "Omr�de 4B"
			if (pPine != NULL) pPine->setupSIForP30(_T("4B"),1);  
			if (pSpruce != NULL) pSpruce->setupSIForP30(_T("4B"),2);
			if (pBirch != NULL) pBirch->setupSIForP30(_T("4B"),3);	
		break;

		case 5 :	// "Omr�de 5"
			if (pPine != NULL) pPine->setupSIForP30(_T("5"),1);  
			if (pSpruce != NULL) pSpruce->setupSIForP30(_T("5"),2);
			if (pBirch != NULL) pBirch->setupSIForP30(_T("5"),3);	
		break;
	};
}

void CP30NewNormTemplateFormView::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());
		m_wndEdit1.SetFocus();
	}
}


// --------------------------------------------------------------------------------------------------------------------------------------
//*****************************************************************************************************
// CP30Norm2018TemplateFormView form view 2018-03-20 J�
//*****************************************************************************************************

IMPLEMENT_DYNCREATE(CP30Norm2018TemplateFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CP30Norm2018TemplateFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON14_1, &CP30Norm2018TemplateFormView::OnBnClickedButton141)
	ON_BN_CLICKED(IDC_BUTTON14_2, &CP30Norm2018TemplateFormView::OnBnClickedButton142)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL_P30_2018, OnSelectedChanged)
	ON_CBN_SELCHANGE(IDC_COMBO14_1, &CP30Norm2018TemplateFormView::OnCbnSelchangeCombo141)
	ON_EN_CHANGE(IDC_EDIT14_1, &CP30Norm2018TemplateFormView::OnEnChangeEdit141)
END_MESSAGE_MAP()

CP30Norm2018TemplateFormView::CP30Norm2018TemplateFormView()
	: CXTResizeFormView(CP30Norm2018TemplateFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CP30Norm2018TemplateFormView::~CP30Norm2018TemplateFormView()
{
}

void CP30Norm2018TemplateFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();	
}

void CP30Norm2018TemplateFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CP30NewNormTemplateFormView)
	DDX_Control(pDX, IDC_GROUP14_1, m_wndGroup14_1);
	DDX_Control(pDX, IDC_GROUP14_2, m_wndGroup14_2);

	DDX_Control(pDX, IDC_LBL14_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL14_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL14_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL14_4, m_wndLbl4);
	
	DDX_Control(pDX, IDC_EDIT14_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT14_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT14_3, m_wndEdit3);

	DDX_Control(pDX, IDC_COMBO14_1, m_wndCBox1);

	DDX_Control(pDX, IDC_BUTTON14_1, m_wndAdd);
	DDX_Control(pDX, IDC_BUTTON14_2, m_wndRemove);

	//}}AFX_DATA_MAP
}

void CP30Norm2018TemplateFormView::OnSetFocus(CWnd *wnd)
{

	if (m_vecTransactionTemplate.size() > 0)
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
	else
		setNavigationButtons(FALSE,FALSE);


	CXTResizeFormView::OnSetFocus(wnd);
}

void CP30Norm2018TemplateFormView::OnInitialUpdate()
{
	CString sPine,sSpruce,sBirch;

	CXTResizeFormView::OnInitialUpdate();

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL_P30_2018);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	if (m_wndTabControl.GetSafeHwnd())
	{
		CRect rect1;
		CRect rect2;
		m_wndGroup14_1.GetClientRect(rect1);
		m_wndGroup14_2.GetClientRect(rect2);
		setResize(&m_wndTabControl,20,120,rect1.Width()-40,rect2.Height()-4);
//		setResize(&m_wndTabControl,rect2.Width()+4,120,rect1.Width()-rect2.Width()-2,rect2.Height()-4);
	}

	m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE );
	m_wndEdit1.SetEnabledColor(BLACK,WHITE );
	m_wndEdit1.SetReadOnly(TRUE);
	m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE );
	m_wndEdit2.SetEnabledColor(BLACK,WHITE );
	m_wndEdit2.SetReadOnly(TRUE);
	m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE );
	m_wndEdit3.SetEnabledColor(BLACK,WHITE );
	m_wndEdit3.SetReadOnly(TRUE);
	m_wndEdit3.SetLimitText(1000);

	if (! m_bInitialized )
	{

		// Setup language filename; 080402 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (m_pDB != NULL)
			m_pDB->getSpecies(m_vecSpecies);
		// Seek name of species that have specieid: 1 (Tall), 2 (Gran) and 3 (L�v)
		if (m_vecSpecies.size() > 0)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				if (m_vecSpecies[i].getSpcID() == 1) sPine = m_vecSpecies[i].getSpcName();
				if (m_vecSpecies[i].getSpcID() == 2) sSpruce = m_vecSpecies[i].getSpcName();
				if (m_vecSpecies[i].getSpcID() == 3) sBirch = m_vecSpecies[i].getSpcName();
			}
		}

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_wndGroup14_2.SetWindowTextW(xml.str(IDS_STRING2519));
				m_wndGroup14_2.ShowWindow(SW_HIDE);
				m_wndAdd.SetWindowTextW(xml.str(IDS_STRING2520));
				m_wndAdd.ShowWindow(SW_HIDE);
				m_wndRemove.SetWindowTextW(xml.str(IDS_STRING2521));
				m_wndRemove.ShowWindow(SW_HIDE);

				m_sMsgCap = (xml.str(IDS_STRING229));
				m_sMsgNoTemplateName = xml.str(IDS_STRING2522);
				m_sMsgNoTemplateName2.Format(_T("%s\n\n%s\n\n"),
					(xml.str(IDS_STRING2522)),
					(xml.str(IDS_STRING2504)));
				m_sMsgRemoveTemplate = xml.str(IDS_STRING2505);
				m_sMsgNameUsedTemplate = xml.str(IDS_STRING2523);
				m_sMsgChangeGArea = xml.str(IDS_STRING2525);
				m_sMsgCharError.Format(_T("%s < > /"),xml.str(IDS_STRING2526));
				
				m_sMsgPRelError1 = xml.str(IDS_STRING2527);
				m_sMsgPRelError2 = xml.str(IDS_STRING2528);

				m_wndLbl1.SetWindowTextW(xml.str(IDS_STRING2500));
				m_wndLbl2.SetWindowTextW(xml.str(IDS_STRING2501));
				m_wndLbl3.SetWindowTextW(xml.str(IDS_STRING2508));
				m_wndLbl4.SetWindowTextW(xml.str(IDS_STRING2502));

				// "Tillv�xtomr�den"
				m_sarrAreas.Add(xml.str(IDS_STRING2509));
				m_sarrAreas.Add(xml.str(IDS_STRING2510));
				m_sarrAreas.Add(xml.str(IDS_STRING2511));
				m_sarrAreas.Add(xml.str(IDS_STRING2512));
				m_sarrAreas.Add(xml.str(IDS_STRING2513));
				m_sarrAreas.Add(xml.str(IDS_STRING2514));
				// Add to m_wndCBox1
				m_wndCBox1.ResetContent();
				m_wndCBox1.AddString(m_sarrAreas.GetAt(0));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(1));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(2));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(3));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(4));
				m_wndCBox1.AddString(m_sarrAreas.GetAt(5));
				m_wndCBox1.SetCurSel(-1);	// No selection yet; 090415 p�d

				if (m_wndTabControl.GetSafeHwnd())
				{
					AddView(RUNTIME_CLASS(CAddP30Norm2018FormView), sPine, 3,1);		// Pine
					AddView(RUNTIME_CLASS(CAddP30Norm2018FormView), sSpruce, 3,2);	// Spruce
					//AddView(RUNTIME_CLASS(CAddP30Norm2018FormView), sBirch, 3,3);	// Birch 
					AddView(RUNTIME_CLASS(CAddP30Norm2018FormView), _T("L�v"), 3,3);	// Birch 
				}	// if (m_wndTabControl.GetSafeHwnd())

				xml.clean();
			}	// if (xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))


		m_enumAction = TEMPLATE_NONE;

		getObjectTemplatesFromDB();

		if (m_vecTransactionTemplate.size() > 0)
		{
			m_nDBIndex = m_vecTransactionTemplate.size()-1;
			setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
			setEnableData(TRUE);
		}
		else
		{
			m_nDBIndex = -1;
			setNavigationButtons(FALSE,FALSE);
			setEnableData(FALSE);
		}
		populateData(m_nDBIndex);

		m_bInitialized = TRUE;
	}
}

BOOL CP30Norm2018TemplateFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CP30Norm2018TemplateFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CP30NewNormTemplateFormView diagnostics

#ifdef _DEBUG
void CP30Norm2018TemplateFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CP30Norm2018TemplateFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CP30Norm2018TemplateFormView::doSetNavigationBar()
{
	if (m_vecTransactionTemplate.size() > 0 || m_enumAction == TEMPLATE_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CP30NewNormTemplateFormView message handlers

void CP30Norm2018TemplateFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CP30Norm2018TemplateFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

LRESULT CP30Norm2018TemplateFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{

		case ID_NEW_ITEM :
		{
			saveP30Norm2018Template(1);
			newP30Norm2018Template();
			setEnableData(TRUE);
			m_enumAction = TEMPLATE_NEW;
			break;
		}	// case ID_NEW_ITEM :
		case ID_SAVE_ITEM :
		{
			saveP30Norm2018Template(1);
			break;
		}	// case ID_SAVE_ITEM :	
		case ID_DELETE_ITEM :
		{
			removeP30Norm2018Template();
			break;
		}	// case ID_DELETE_ITEM :	

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30Norm2018Template(-1))
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30Norm2018Template(-1))
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
					setNavigationButtons(FALSE,TRUE);
				else
					setNavigationButtons(TRUE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30Norm2018Template(-1))
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((int)m_vecTransactionTemplate.size() - 1))
					m_nDBIndex = (int)m_vecTransactionTemplate.size() - 1;
					
				if (m_nDBIndex == (UINT)m_vecTransactionTemplate.size() - 1)
					setNavigationButtons(TRUE,FALSE);
				else
					setNavigationButtons(TRUE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveP30Norm2018Template(-1))
			{
				m_nDBIndex = (UINT)m_vecTransactionTemplate.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	}	// switch (wParam)
	return 0L;
}

void CP30Norm2018TemplateFormView::populateData(int idx)
{
	TemplateParser pars;
	CString sAreaName;
	CString sNotes;
	vecObjectTemplate_p30_nn_table vecP30_nn;

	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	int nSpcID = 0;
	// Make sure we are within limits; 080402 p�d
	if (m_vecTransactionTemplate.size() > 0 &&
			idx >= 0 &&
			idx < (int)m_vecTransactionTemplate.size())
	{
		m_recActiveTemplate = m_vecTransactionTemplate[idx];

		m_enumAction = TEMPLATE_OPEN;

		// Setup general data; 080402 p�d
		m_wndEdit1.SetWindowText((m_recActiveTemplate.getTemplateName()));
		m_wndEdit2.SetWindowText((m_recActiveTemplate.getCreatedBy()));
		m_wndEdit3.SetWindowText((m_recActiveTemplate.getTemplateNotes()));
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			pars.getObjTmplP30_nn(vecP30_nn,sAreaName,&m_nAreaIndex);
			m_wndCBox1.SetCurSel(m_nAreaIndex);
			if (nNumOfTabs > 0 && vecP30_nn.size() > 0)
			{
				for (int i = 0;i < nNumOfTabs;i++)
				{
					CAddP30Norm2018FormView *pView = getTabFormView(i,&nSpcID /* Spc id */);
					if (pView != NULL)
					{
						pView->setReport(vecP30_nn,nSpcID);
					}	// if (pView != NULL)
				}	// for (int i = 0;i < nNumOfTabs;i++)
			}	// if (nNumOfTabs > 0)
		}

	}
	else
	{
		m_wndEdit1.SetWindowText(_T(""));	// 090428 p�d
		m_wndEdit2.SetWindowText(_T(""));	// 090428 p�d
		m_wndEdit3.SetWindowText(_T(""));	// 090428 p�d
		m_wndCBox1.SetCurSel(-1);	// No selection; 090428 p�d
		setEnableData(FALSE);
		newP30Norm2018Template();
		m_enumAction = TEMPLATE_NONE;
		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplatesRegistered),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
	}

	doSetNavigationBar();
}

CString CP30Norm2018TemplateFormView::getSpcNameFromID(int id)
{
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == id) 
				return m_vecSpecies[i].getSpcName();
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// if (m_vecSpecies.size() > 0)

	return _T("");
}

void CP30Norm2018TemplateFormView::newP30Norm2018Template(void)
{
	int nData = -1;
	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	// Clear data; 090417 p�d
	m_wndEdit1.SetWindowTextW(_T(""));
	m_wndEdit2.SetWindowTextW(getUserName().MakeUpper());
	m_wndEdit3.SetWindowTextW(_T(""));
	m_wndCBox1.SetCurSel(-1);

	if (nNumOfTabs > 0)
	{
		for (int i = 0;i < nNumOfTabs;i++)
		{
			CAddP30Norm2018FormView *pView = getTabFormView(i,&nData /* Spc id */);
			if (pView != NULL)
			{
				pView->resetReport();
			}	// if (pView != NULL)
		}	// for (int i = 0;i < nNumOfTabs;i++)
		m_wndTabControl.SetSelectedItem(m_wndTabControl.getTabPage(0));
	}	// if (nNumOfTabs > 0)
	m_recActiveTemplate = CTransaction_template();

	m_wndEdit1.SetFocus();


	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

}

void CP30Norm2018TemplateFormView::removeP30Norm2018Template(void)
{
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (m_pDB != NULL)
		{
			if (m_pDB->delObjectTemplate(m_recActiveTemplate))
			{
				getObjectTemplatesFromDB();
				if (m_vecTransactionTemplate.size() > 0)
				{
					m_nDBIndex = (UINT)m_vecTransactionTemplate.size() - 1;
					setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecTransactionTemplate.size()-1));
				}	// if (m_vecTransactionTemplate.size() > 0)
				else
				{
					m_nDBIndex = -1;	// No Objects
					setNavigationButtons(FALSE,FALSE);
					//newP30NewNormTemplate();
				}
			}	// if (m_pDB->delObjectTemplate(m_recActiveTemplate))
		}	// if (m_pDB != NULL)
		populateData(m_nDBIndex);
	}	// if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
}

// Save to database; 090416 p�d
BOOL CP30Norm2018TemplateFormView::saveP30Norm2018Template(short action)
{
	short nRetValue;
	CTransaction_template rec;
	BOOL bNameAlreadyUsed = FALSE;
	// If not enabled, nothing to save; 090428 p�d
	if (!m_bDataEnabled)
	{
		if (action == 1) return FALSE;
		else if (action == 2) return TRUE;
	}	// if (!m_bDataEnabled)
	// Do a check that user entered a name and selcted an area; 090416 p�d
	if (m_wndEdit1.getText().IsEmpty() || m_wndCBox1.GetCurSel() == -1)
	{
		if (action == 0)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		else if (action == 1)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return FALSE; // HMS-9 20180409 J� ej spara prislista utan namn eller valt omr�de
		}
		else if (action == 2)
		{
			if (::MessageBox(this->GetSafeHwnd(),(m_sMsgNoTemplateName2),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
				return TRUE;
			else
			{
				m_wndEdit1.SetFocus();						
				return FALSE;
			}
		}	// else if (action == 2)
	}
	else
	{
		// We'll also check if name entered, have already been used; 090428 p�d
		if (m_vecTransactionTemplate.size() > 0)
		{
			for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			{
				rec = m_vecTransactionTemplate[i];
				if (rec.getTemplateName().CompareNoCase(m_wndEdit1.getText()) == 0 && i != m_nDBIndex)
				{
					bNameAlreadyUsed = TRUE;
					break;
				}
			}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		}	// if (m_vecTransactionTemplate.size() > 0)
	}

	if (!bNameAlreadyUsed)
	{

		BOOL bCreate = createXMLFile(&nRetValue);
		if (bCreate)
		{
			getObjectTemplatesFromDB();
			if (action > -1)
			{
				if (nRetValue == 0)
					m_nDBIndex = m_vecTransactionTemplate.size()-1;
				setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1)); 
				populateData(m_nDBIndex);
			}
		}
		return bCreate;
	}
	else
	{
		::MessageBox(this->GetSafeHwnd(),(m_sMsgNameUsedTemplate),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}
	return FALSE;
}

// Create the XML-file here; 090416 p�d
BOOL CP30Norm2018TemplateFormView::createXMLFile(short *ret_value)
{
	CString sXML,sData,sArea,sSpcName,sName,sDoneBy,sNotes,sMsg;
	CStringArray sarrSpcData,sarrErr;
	int nData = 0;
	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	int nCBoxIndex = m_wndCBox1.GetCurSel();
	CTransaction_species rec;
	short nCnt = 1;

	// Setup XML-header data here
	sXML = XML_FILE_HEADER;
	sXML += NODE_P30NN_START;

	// Name of template; 090416 p�d
	sName = m_wndEdit1.getText();
	CString sNameTmp = sName;
	sNameTmp.Replace(_T("&"), _T("&amp;"));
	sNameTmp.Replace(_T("<"), _T("&lt;"));
	sNameTmp.Replace(_T(">"), _T("&gt;"));
	sData.Format(NODE_P30NN_NAME,sNameTmp);
	sXML += sData;
	// Done by template; 090416 p�d
	sDoneBy = m_wndEdit2.getText();
	sData.Format(NODE_P30NN_DONE_BY,sDoneBy);
	sXML += sData;
	// Area for template; 090416 p�d
	m_wndCBox1.GetWindowTextW(sArea);
	sData.Format(NODE_P30NN_AREA,sArea,nCBoxIndex);
	sXML += sData;
	// Notes template; 090416 p�d
	sNotes = m_wndEdit3.getText();
	CString sNotesTmp = sNotes;
	sNotesTmp.Replace(_T("&"), _T("&amp;"));
	sNotesTmp.Replace(_T("<"), _T("&lt;"));
	sNotesTmp.Replace(_T(">"), _T("&gt;"));
	sData.Format(NODE_P30NN_NOTES,sNotesTmp);
	sXML += sData;

	if (nNumOfTabs > 0)
	{
		sarrErr.RemoveAll();
		sarrErr.Add(m_sMsgPRelError1);
		sarrErr.Add(m_sMsgPRelError2);
		sarrErr.Add(L"");
		for (int i = 0;i < nNumOfTabs;i++)
		{
			CAddP30Norm2018FormView *pView = getTabFormView(i,&nData /* Spc id */);
			if (pView != NULL)
			{
				// Specie data; 090416 p�d
				sSpcName = getSpcNameFromID(nData);
				// Check added 101006 p�d
				pView->checkData(sSpcName,sarrErr);
			}
		}
		if (sarrErr.GetCount() > 3)
		{
			for (short ii = 0;ii < sarrErr.GetCount();ii++)
				sMsg += sarrErr.GetAt(ii) + L"\n";
			::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		for (int i = 0;i < nNumOfTabs;i++)
		{
			sarrSpcData.RemoveAll();
			CAddP30Norm2018FormView *pView = getTabFormView(i,&nData /* Spc id */);
			if (pView != NULL)
			{
				// Specie data; 090416 p�d
				sSpcName = getSpcNameFromID(nData);
				// Check added 101006 p�d
				pView->getData(sarrSpcData);

				if (nData == 1) sData.Format(NODE_P30NN_SPC_START,sSpcName,nData,_T("T"));				// Pine (Tall)
				else if (nData == 2) sData.Format(NODE_P30NN_SPC_START,sSpcName,nData,_T("G"));	// Spruce (Gran)
				else if (nData == 3) sData.Format(NODE_P30NN_SPC_START,sSpcName,nData,_T("B"));	// Birch (Bj�rk)
				sXML += sData;
				
				if (sarrSpcData.GetCount() > 0)
				{
					for (int i1 = 0;i1 < sarrSpcData.GetCount();i1++)
					{
						sXML += sarrSpcData.GetAt(i1);
					}	// for (int i1 = 0;i1 < sarrSpcData.GetCount();i1++)
				}
				sXML += NODE_P30NN_SPC_END;

				// Add relations to Pine and Spruce. We don't need to do relations for
				// birch, because not relted to pine or spruce is related to birch; 090608 p�d

				pView = NULL;
			}	// if (pView != NULL)
		}	// for (int i = 0;i < nNumOfTabs;i++)
	}	// if (nNumOfTabs > 0)

	// XML-file end here; 090416 p�d
	sXML += NODE_P30NN_END;

	// Save to database; 090416 p�d
	if (m_pDB != NULL)
	{
		if (!sName.IsEmpty())
		{
			*ret_value = 0;
			if (!m_pDB->addObjectTemplate(CTransaction_template(m_recActiveTemplate.getID(),sName,TEMPLATE_P30_2018_NORM,sXML,sNotes,sDoneBy)))
			{
				*ret_value = 1;
				m_pDB->updObjectTemplate(CTransaction_template(m_recActiveTemplate.getID(),sName,TEMPLATE_P30_2018_NORM,sXML,sNotes,sDoneBy));
			}
		}
	}

	return TRUE;

}

void CP30Norm2018TemplateFormView::importP30Table(void)
{
	CString sXML;
	CString sName,sCompareName,sTmplName;
	CString sDoneBy;
	CString sNotes;
	CString sXMLCompleted;
	CString sFilter;
	int nCountName = 0;
	CTransaction_template recAdded;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22703),P30_2018_TABLE_EXTENSION,P30_2018_TABLE_EXTENSION);
		}
		delete xml;
	}

	// Handles clik on open button
	CFileDialog dlg( TRUE, P30_2018_TABLE_EXTENSION, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		setEnableData(TRUE);
			// Setup transport table; 081201 p�d
		TemplateParser pars;
		if (pars.LoadFromFile(dlg.GetPathName()))
		{
			pars.getXML(sXML);
			pars.getObjTmplP30_nn_name(sName);
			pars.getObjTmplP30_nn_done_by(sDoneBy);
			if (sDoneBy.IsEmpty())
				sDoneBy = getUserName().MakeUpper();
			pars.getObjTmplP30_nn_notes(sNotes);
			// Check that the name isn't already used; 091027 p�d
			if (m_vecTransactionTemplate.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
				{
					sCompareName = m_vecTransactionTemplate[i].getTemplateName().Left(sName.GetLength());
					if (sCompareName.CompareNoCase(sName) == 0)
						nCountName++;
				}	// for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
			}	// if (m_vecTransactionTemplate.size() > 0)
			
			if (nCountName > 0) sTmplName.Format(_T("%s(%d)"),sName,nCountName);
			else sTmplName = sName;

			sXMLCompleted.Format(_T("%s%s"),XML_FILE_HEADER,sXML);
			// Only ADD A NEW "Under utveckling"; 081201 p�d
			if (m_pDB != NULL)
			{
				m_pDB->addObjectTemplate(CTransaction_template(-1,sTmplName,TEMPLATE_P30_2018_NORM,sXMLCompleted,sNotes,sDoneBy));
			}	// if (m_pDB != NULL)
			// We need to reload templates from database
			// and set m_nDBIncdex to point to the last (just created)
			// template; 081201 p�d
			getObjectTemplatesFromDB();
			if (m_vecTransactionTemplate.size() > 0)
				m_nDBIndex = m_vecTransactionTemplate.size() - 1;	// Point to last item
		}	// if (pars.LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
		populateData(m_nDBIndex);

		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransactionTemplate.size()-1)); 
	}	// if(dlg.DoModal() == IDOK)

}

void CP30Norm2018TemplateFormView::exportP30Table(void)
{
	CString sName;
	CString sFilter;
	//-------------------------------------------------------------------
	// Save data before tryin' to export; 091012 p�d
	if (!saveP30Norm2018Template(0)) return;
	getObjectTemplatesFromDB();
	populateData(m_nDBIndex);
	//-------------------------------------------------------------------

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING22703),P30_2018_TABLE_EXTENSION,P30_2018_TABLE_EXTENSION);
		}
		delete xml;
	}

	sName = m_recActiveTemplate.getTemplateName();
	scanFileName(sName);
	if (sName.Right(9) != P30_2018_TABLE_EXTENSION)
		sName += P30_2018_TABLE_EXTENSION;
	// Handles clik on open button
	CFileDialog dlg( FALSE, P30_2018_TABLE_EXTENSION, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER,sFilter, this);

	if (dlg.DoModal() == IDOK)
	{
		TemplateParser pars;
		if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
		{
			sName = dlg.GetPathName();
			if (sName.Right(9) != P30_2018_TABLE_EXTENSION)
				sName += P30_2018_TABLE_EXTENSION;
			pars.SaveToFile(sName);
		}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
	}
}

void CP30Norm2018TemplateFormView::getObjectTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_vecTransactionTemplate.clear();
			m_pDB->getObjectTemplates(m_vecTransactionTemplate,TEMPLATE_P30_2018_NORM);
		}
	}
}

void CP30Norm2018TemplateFormView::setEnableData(BOOL enable)
{
	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit3.EnableWindow(enable);
	m_wndEdit3.SetReadOnly(!enable);
	m_wndCBox1.EnableWindow(enable);

	m_wndAdd.EnableWindow(enable);
	m_wndRemove.EnableWindow(enable);

	CMDIP30Norm2018TemplateFrame *pFrame = (CMDIP30Norm2018TemplateFrame*)getFormViewByID(IDD_FORMVIEW14)->GetParent();
	if (pFrame != NULL)
		pFrame->setEnableToolbar(enable);

	if (m_wndTabControl.GetSafeHwnd())
		m_wndTabControl.EnableWindow(enable);
	
	if (enable)	m_wndEdit1.SetFocus();

	m_bDataEnabled = enable;

}

CAddP30Norm2018FormView *CP30Norm2018TemplateFormView::getSelectedTabFormView()
{
	m_tabManager = m_wndTabControl.getSelectedTabPage();
	if (m_tabManager)
	{
		CAddP30Norm2018FormView* pView = DYNAMIC_DOWNCAST(CAddP30Norm2018FormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CAddP30Norm2018FormView, pView);
		return pView;
	}
	return NULL;
}

CAddP30Norm2018FormView *CP30Norm2018TemplateFormView::getTabFormView(int tab_index,int *data)
{
	m_tabManager = m_wndTabControl.getTabPage(tab_index);
	if (m_tabManager)
	{
		CAddP30Norm2018FormView* pView = DYNAMIC_DOWNCAST(CAddP30Norm2018FormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CAddP30Norm2018FormView, pView);
		*data = m_tabManager->GetData();
		return pView;
	}
	return NULL;
}


// Added 090415 p�d; Add a row for "Bonitetsgr�ns"
void CP30Norm2018TemplateFormView::OnBnClickedButton141()
{
	CAddP30Norm2018FormView *pView = getSelectedTabFormView();
	if (pView != NULL)
	{
		pView->addRow();
		pView = NULL;
	}
}

// Added 090415 p�d; Remove last added row for "Bonitetsgr�ns"
void CP30Norm2018TemplateFormView::OnBnClickedButton142()
{
	CAddP30Norm2018FormView *pView = getSelectedTabFormView();
	if (pView != NULL)
	{
		pView->delRow();
		pView = NULL;
	}
}

BOOL CP30Norm2018TemplateFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = NULL; //GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = NULL; //GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	CXTPTabManagerItem *pItem =	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	if (pItem != NULL) pItem->SetData(id);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CP30Norm2018TemplateFormView::OnCbnSelchangeCombo141()
{
	CAddP30Norm2018FormView *pPine = NULL;
	CAddP30Norm2018FormView *pSpruce = NULL;
	CAddP30Norm2018FormView *pBirch = NULL;
	int nData;
	int nSelIndex = m_wndCBox1.GetCurSel();
	if (nSelIndex == CB_ERR) return;

	if (::MessageBox(this->GetSafeHwnd(),m_sMsgChangeGArea,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
	{
		m_wndCBox1.SetCurSel(m_nAreaIndex);
		return;
	}

	pPine = getTabFormView(0,&nData);
	pSpruce = getTabFormView(1,&nData);
	pBirch = getTabFormView(2,&nData);
	switch (nSelIndex)
	{
		case 0 :	// "Omr�de 1"
			if (pPine != NULL) 
				pPine->setupSIForP30(_T("1"),1);  
			if (pSpruce != NULL) 
				pSpruce->setupSIForP30(_T("1"),2);
			if (pBirch != NULL) 
				pBirch->setupSIForP30(_T("1"),3);	
		break;

		case 1 :	// "Omr�de 2"
			if (pPine != NULL) 
				pPine->setupSIForP30(_T("2"),1);  
			if (pSpruce != NULL) 
				pSpruce->setupSIForP30(_T("2"),2);
			if (pBirch != NULL) 
				pBirch->setupSIForP30(_T("2"),3);	
		break;

		case 2 :	// "Omr�de 3"
			if (pPine != NULL) 
				pPine->setupSIForP30(_T("3"),1);  
			if (pSpruce != NULL) 
				pSpruce->setupSIForP30(_T("3"),2);
			if (pBirch != NULL) 
				pBirch->setupSIForP30(_T("3"),3);	
		break;

		case 3 :	// "Omr�de 4A"
			if (pPine != NULL) 
				pPine->setupSIForP30(_T("4A"),1);  
			if (pSpruce != NULL) 
				pSpruce->setupSIForP30(_T("4A"),2);
			if (pBirch != NULL) 
				pBirch->setupSIForP30(_T("4A"),3);	
		break;

		case 4 :	// "Omr�de 4B"
			if (pPine != NULL) 
				pPine->setupSIForP30(_T("4B"),1);  
			if (pSpruce != NULL) 
				pSpruce->setupSIForP30(_T("4B"),2);
			if (pBirch != NULL) 
				pBirch->setupSIForP30(_T("4B"),3);	
		break;

		case 5 :	// "Omr�de 5"
			if (pPine != NULL) 
				pPine->setupSIForP30(_T("5"),1);  
			if (pSpruce != NULL) 
				pSpruce->setupSIForP30(_T("5"),2);
			if (pBirch != NULL) 
				pBirch->setupSIForP30(_T("5"),3);	
		break;
	};
}

void CP30Norm2018TemplateFormView::OnEnChangeEdit141()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());
		m_wndEdit1.SetFocus();
	}
}
