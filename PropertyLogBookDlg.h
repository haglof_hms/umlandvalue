#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////////
// CPropertiesLogBookDataRec

class CPropertiesLogBookDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_elv_properties_logbook propLogBookRec;
	CString sPropName;
	CString sLogDate;
	CString sShowDate;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:
	CPropertiesLogBookDataRec(void)
	{
		m_nIndex = -1;
		sLogDate = getDateTimeEx2();
		sShowDate = sLogDate.Left(sLogDate.GetLength() - (sLogDate.GetLength() - sLogDate.ReverseFind(':')));
		AddItem(new CTextItem((sShowDate)));
		AddItem(new CTextItem((getUserName().MakeUpper())));
		AddItem(new CTextItem(_T("")));
	}

	CPropertiesLogBookDataRec(UINT index,CTransaction_elv_properties_logbook data)	 
	{
		m_nIndex = index;
		propLogBookRec = data;
		sLogDate = data.getPropLogBookDate();
		sShowDate = sLogDate.Left(sLogDate.GetLength() - (sLogDate.GetLength() - sLogDate.ReverseFind(':')));
		AddItem(new CTextItem((sShowDate)));
		AddItem(new CTextItem((data.getPropLogBookAddedBy())));
		AddItem(new CTextItem((data.getPropLogBookNote())));	
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	CString getLogDate(void)	{ return sLogDate; }

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
	CTransaction_elv_properties_logbook &getRecord(void)
	{
		return propLogBookRec;
	}

};

// CPropertyLogBookDlg dialog

class CPropertyLogBookDlg : public CDialog
{
	DECLARE_DYNAMIC(CPropertyLogBookDlg)
	
	// Setup language filename; 051214 p�d
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgRemoveLogEntry;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;

	CButton m_wndAddRowBtn;
	CButton m_wndDelRowBtn;
	CButton m_wndOKBtn;

	CMyReportCtrl m_wndReport;
	void setupReport(void);

	CString m_sObjectName;
	CString m_sPropName;
	CString m_sPropNum;

	int m_nNumOfEntriesInLogBook;

	CTransaction_elv_properties m_recProperty;
	vecTransaction_elv_properties_logbook m_vecLogBook;
	CUMLandValueDB *m_pDB;

	void populateData();
public:
	CPropertyLogBookDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPropertyLogBookDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG7 };

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

	void setObjectAndPropertyInfo(LPCTSTR obj_name,CTransaction_elv_properties &rec)
	{
		m_sObjectName = obj_name;
		m_sPropName = rec.getPropName();
		m_sPropNum = rec.getPropNumber();
		m_recProperty = rec;
	}

	int getNumOfEntriesInLogBook(void)
	{
		return m_nNumOfEntriesInLogBook;
	}

	void getPropertyLogBook(void);
	void savePropertyLogBook(void);
	void delEntryInPropertyLogBook(CTransaction_elv_properties_logbook &);
protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton2();
};
