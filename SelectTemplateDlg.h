#pragma once

#include "Resource.h"

// CSelectTemplateDlg dialog

class CSelectTemplateDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CSelectTemplateDlg)

	CString m_sLangFN;
	CString m_sActiveTmplName;
	vecTransactionTemplate m_vecTemplates;
	CTransaction_template m_recSelectedTemplate;
	CXTResizeGroupBox m_wndGroup_templ;

	CXTListCtrl m_wndLCtrl;
	CXTHeaderCtrl   m_Header;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CString m_sMsgCap;
	CString m_sMsgNoSelection;
	void setupList(void);
public:
	CSelectTemplateDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectTemplateDlg();

	void setActiveTmplName(LPCTSTR name)							{ m_sActiveTmplName = name; }
	void setTemplates(vecTransactionTemplate &vec)	{	m_vecTemplates = vec;	}
// Dialog Data
	enum { IDD = IDD_DIALOG4 };

	CTransaction_template &getSelectedTemplate(void)	{ return m_recSelectedTemplate; }
protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
