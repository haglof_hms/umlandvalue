// ProgressDlg2.cpp : implementation file
//

#include "stdafx.h"
#include "ProgressDlg2.h"

#include "ResLangFileReader.h"

// CProgressDlg2 dialog

IMPLEMENT_DYNAMIC(CProgressDlg2, CDialog)

BEGIN_MESSAGE_MAP(CProgressDlg2, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

CProgressDlg2::CProgressDlg2(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDlg2::IDD, pParent),
	m_bInitialized(FALSE)

{

}

CProgressDlg2::~CProgressDlg2()
{
}

void CProgressDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressDlg2)
	DDX_Control(pDX, IDC_PROGRESS29_1, m_wndProgress29_1);
	//}}AFX_DATA_MAP
}

BOOL CProgressDlg2::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (!m_bInitialized)
	{
		m_brush.CreateSolidBrush(RGB(255, 255, 255)); // color white brush

		// initialize progress controls
		m_wndProgress29_1.SetRange(0, 100);
		m_wndProgress29_1.SetPos(0);
		m_wndProgress29_1.SetStep(10);

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText(xml.str(IDS_STRING7000));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}
		m_bInitialized = TRUE;
	}

	return FALSE;
}

// CProgressDlg2 message handlers
HBRUSH CProgressDlg2::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	return m_brush;
}
