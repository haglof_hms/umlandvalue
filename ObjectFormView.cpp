// ObjectFormView.cpp : implementation file
//

//TODO: #3385
//klart - l�gga till statusflagga i db, create + alter
//klart - g� igenom och s�tta statusv�rde i db p� gamla objekt
//klart - s�tta r�tt status p� objektet n�r det visas, populatedata
//klart - spara status�ndring till db 
//klart - spara status n�r skapa nytt objekt
//klart - filerfunktioner i verktyg
//klart - anv�nder befintliga funktioner - filterfunktioner i objektlistan
//ska f�lt l�sas n� man s�tter objektet till slutf�rd?
//klart - �ndra i transaktionsklassen i funclibet

//klart - ta bort knappen i objektsf�nstret och visa bara text ist�llet, anv fet stil och f�rg
//klart - l�gg till funktioner i vertygsmenyn, meny Objekt, undermeny slutf�r objekt, �terta objekt, visa alla, visa p�g�ende, visa slutf�rda
//klart - default alltid visa p�g�ende, beh�ver inte spara visa inst�llningen n�gonstans
//klart - ett objekt som satts som slutf�rt ska inte visas n�r man bl�ddrar om man inte v�ljer att g�ra det
//klart - filterinst�llningen �ver vad som visas ska speglas till v�lj ur lista funktionen (PIL UPP). Ska inte kunna �ndra filterinst�llningen i listan
//klart - kolla om det g�r att gr�a ut fuktioner i verktygsf�ltet. F�r ett p�g�ende objekt ska alt. att s�tta som p�g�ende inte vara valbart etc.
//klart - l�gg till fr�ga n�r objektet s�tts som slutf�rt
//klart - l�gg till text i spr�kfilerna
//klart - kan bli fel om man har listv�ljaren �ppen och �ndrar status som ska visas i objektet, borde k�ra en refresh p� listv�ljaren d�

//klart - Sp�rra visa slutf�rda om det inte finns n�gra slutf�rda objekt
//klart - S�tt en checkruta som markerar vilket visa alternativ som �r aktivt
//klart - Vid uppstart av objektsf�nstret s� k�r �ven en refresh p� listv�ljaren ifall den �r uppe, ska bara visa p�g�ende objekt som default
//klart - sp�rra visa p�g�ende om det inte finns n�gra p�g�ende projekt, hur g�ra vid uppstart eftersom p�g�ende visas default?

#include "stdafx.h"
#include "resource.h"
#include "MDI1950ForrestNormFormView.h"
#include "TemplateParser.h"
#include "ObjectFormView.h"
#include "SelectTemplateDlg.h"
#include "SelectObjectDlg.h"
#include "NewObjectFromTemplates.h"
#include "SelectContractorDlg.h"
#include "ShowInformationDlg3.h"
#include "ObjectSelListFormView.h"

#include "ResLangFileReader.h"

// CObjectFormView

IMPLEMENT_DYNCREATE(CObjectFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CObjectFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
//	ON_BN_CLICKED(IDC_BUTTON1, &CObjectFormView::OnBnClickedButton1)
//	ON_BN_CLICKED(IDC_BUTTON2, &CObjectFormView::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BTN_ADD_CONTRACTOR, &CObjectFormView::OnBnClickedBtnAddContractor)
	ON_BN_CLICKED(IDC_BTN_SHOW_CONTRACTOR, &CObjectFormView::OnBnClickedBtnShowContractor)
	ON_BN_CLICKED(IDC_BUTTON3, &CObjectFormView::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BTN_ADD_LOGO, &CObjectFormView::OnBnClickedBtnAddLogo)
	ON_BN_CLICKED(IDC_BTN_DEL_LOGO, &CObjectFormView::OnBnClickedBtnDelLogo)
	ON_BN_CLICKED(IDC_BTN_DEL_CONTRACTOR, &CObjectFormView::OnBnClickedBtnDelContractor)
	ON_BN_CLICKED(IDC_BUTTON_P30_SETTINGS,&CObjectFormView::OnBnClickedBtnP30Settings)
	ON_BN_CLICKED(IDC_BTN_DEL_RETADR, &CObjectFormView::OnBnClickedBtnDelRetadr)
END_MESSAGE_MAP()

CObjectFormView::CObjectFormView()
	: CXTResizeFormView(CObjectFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bSetFocus = FALSE;
	m_pDB = NULL;
	m_pDBadmin = NULL;
	m_bIsLastVisitedObjectID_Valid = TRUE;
}

CObjectFormView::~CObjectFormView()
{
}

void CObjectFormView::OnDestroy()
{
	m_vecTraktTemplate.clear();
	m_vecObjectP30Template.clear();
	m_vecObjectP30TemplateNN.clear();
	m_vecObjectP30Template2018.clear();
	m_vecObjectHCostTemplate.clear();

	m_wndReport.ResetContent();
	m_wndReport1.ResetContent();

	writeLastObjectIDToRegistry();

	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();	
}

void CObjectFormView::OnClose()
{
	CXTResizeFormView::OnClose();	
}

void CObjectFormView::OnSetFocus(CWnd* pWnd)
{
//	OutputDebugString(_T("CObjectFormView::OnSetFocus\n"));
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,getNumOfItems() > 0 ); //nNumOf > 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,getNumOfItems() > 0); //nNumOf > 0);	

	CXTResizeFormView::OnSetFocus(pWnd);	
}


void CObjectFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP1_1, m_wndGroup1_1);
	DDX_Control(pDX, IDC_GROUP1_2, m_wndGroup1_2);
	DDX_Control(pDX, IDC_GROUP1_6, m_wndGroup1_6);
	DDX_Control(pDX, IDC_GROUP1_7, m_wndGroup1_7);
	DDX_Control(pDX, IDC_GROUP1_8, m_wndGroup1_8);
	DDX_Control(pDX, IDC_GROUP1_9, m_wndGroup1_9);

	DDX_Control(pDX, IDC_LBL1_1, m_wndLbl1_1);
	DDX_Control(pDX, IDC_LBL1_2, m_wndLbl1_2);
	DDX_Control(pDX, IDC_LBL1_3, m_wndLbl1_3);
	DDX_Control(pDX, IDC_LBL1_4, m_wndLbl1_4);
//	DDX_Control(pDX, IDC_LBL1_5, m_wndLbl1_5);
	DDX_Control(pDX, IDC_LBL1_6, m_wndLbl1_6);
	DDX_Control(pDX, IDC_LBL1_7, m_wndLbl1_7);
//	DDX_Control(pDX, IDC_LBL1_8, m_wndLbl1_8);
//	DDX_Control(pDX, IDC_LBL1_9, m_wndLbl1_9);
//	DDX_Control(pDX, IDC_LBL1_10, m_wndLbl1_10);
	//DDX_Control(pDX, IDC_LBL1_11, m_wndLbl1_11);
	DDX_Control(pDX, IDC_LBL1_12, m_wndLbl1_12);
	DDX_Control(pDX, IDC_LBL1_13, m_wndLbl1_13);
	DDX_Control(pDX, IDC_LBL1_14, m_wndLbl1_14);
	//DDX_Control(pDX, IDC_LBL1_15, m_wndLbl1_15);
	//DDX_Control(pDX, IDC_LBL1_16, m_wndLbl1_16);
	DDX_Control(pDX, IDC_LBL1_17, m_wndLbl1_17);
	//DDX_Control(pDX, IDC_LBL1_18, m_wndLbl1_18);
	DDX_Control(pDX, IDC_LBL1_19, m_wndLbl1_19);
	DDX_Control(pDX, IDC_LBL1_20, m_wndLbl1_20);
	DDX_Control(pDX, IDC_LBL1_21, m_wndLbl1_21);
	DDX_Control(pDX, IDC_LBL1_22, m_wndLbl1_22);
	DDX_Control(pDX, IDC_LBL1_23, m_wndLbl1_23);
	DDX_Control(pDX, IDC_LBL1_24, m_wndLbl1_24);
	DDX_Control(pDX, IDC_LBL1_25, m_wndLbl1_25);
	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl2_1);	//new #3385

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1_1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit1_2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit1_3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit1_4);
//	DDX_Control(pDX, IDC_EDIT5, m_wndEdit1_5);

	DDX_Control(pDX, IDC_EDIT6, m_wndEdit1_6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit1_7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit1_8);
	//DDX_Control(pDX, IDC_EDIT9, m_wndEdit1_9);
	//DDX_Control(pDX, IDC_EDIT15, m_wndEdit1_15);
	//DDX_Control(pDX, IDC_EDIT24, m_wndEdit1_24);
	DDX_Control(pDX, IDC_EDIT25, m_wndEdit1_25);
	DDX_Control(pDX, IDC_EDIT26, m_wndEdit1_26);
	DDX_Control(pDX, IDC_EDIT27, m_wndEdit1_27);

	DDX_Control(pDX, IDC_EDIT10, m_wndEdit1_10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit1_11);
//	DDX_Control(pDX, IDC_EDIT12, m_wndEdit1_12);
//	DDX_Control(pDX, IDC_EDIT13, m_wndEdit1_13);
//	DDX_Control(pDX, IDC_EDIT14, m_wndEdit1_14);
	DDX_Control(pDX, IDC_EDIT21, m_wndEdit1_21);

	DDX_Control(pDX, IDC_EDIT16, m_wndEdit1_16);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit1_17);
	DDX_Control(pDX, IDC_EDIT18, m_wndEdit1_18);
	DDX_Control(pDX, IDC_EDIT19, m_wndEdit1_19);

	DDX_Control(pDX, IDC_EDIT1_29, m_wndEdit1_29);
	DDX_Control(pDX, IDC_EDIT1_30, m_wndEdit1_30);

	DDX_Control(pDX, IDC_EDIT5, m_wndEdit2_1);

	DDX_Control(pDX, IDC_BTN_ADD_CONTRACTOR, m_wndAddContractor);
	DDX_Control(pDX, IDC_BTN_DEL_CONTRACTOR, m_wndDelContractor);
	DDX_Control(pDX, IDC_BTN_SHOW_CONTRACTOR, m_wndShowContractor);
	
	DDX_Control(pDX, IDC_BUTTON3, m_wndAddRetAdr);
	DDX_Control(pDX, IDC_BTN_DEL_RETADR, m_wndDelRetAdr);

	DDX_Control(pDX, IDC_BTN_ADD_LOGO, m_wndAddLogo);
	DDX_Control(pDX, IDC_BTN_DEL_LOGO, m_wndDelLogo);
	DDX_Control(pDX, IDC_PICTURE, m_wndPicCtrl);

	//DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	//}}AFX_DATA_MAP

}

void CObjectFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
//		m_wndEdit1_5.SetAsNumeric();
//		m_wndEdit1_5.SetRange(1,6);

		m_wndEdit1_2.SetLimitText(50);
		m_wndEdit1_3.SetLimitText(10);

		m_wndEdit1_1.SetLimitText(50);	// #3197
		// HMS-119 ut�kat ledningslittera till 50 tecken 20250205 J�
		m_wndEdit1_4.SetLimitText(50);

		m_wndEdit1_10.SetLimitText(20);

		m_wndEdit1_11.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_11.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_11.SetReadOnly();
		m_wndEdit1_11.SetEditMask(L"0000-00-00",L"____-__-__");

		m_wndEdit1_6.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_6.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_6.SetReadOnly();
		
		m_wndEdit1_7.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_7.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_7.SetReadOnly();

		m_wndEdit1_8.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_8.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_8.SetReadOnly();

//		m_wndEdit1_9.SetDisabledColor(BLACK,INFOBK );
//		m_wndEdit1_9.SetEnabledColor(BLACK,WHITE );
//		m_wndEdit1_9.SetReadOnly();

//		m_wndEdit1_15.SetDisabledColor(BLACK,INFOBK );
//		m_wndEdit1_15.SetEnabledColor(BLACK,WHITE );
//		m_wndEdit1_15.SetReadOnly();

		m_wndEdit1_16.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_16.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_16.SetReadOnly();

		m_wndEdit1_17.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_17.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_17.SetReadOnly();

		m_wndEdit1_18.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_18.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_18.SetReadOnly();

		m_wndEdit1_19.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_19.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_19.SetReadOnly();

//		m_wndEdit1_24.SetDisabledColor(BLACK,INFOBK );
//		m_wndEdit1_24.SetEnabledColor(BLACK,WHITE );
//		m_wndEdit1_24.SetReadOnly();

		m_wndEdit1_25.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_25.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_25.SetReadOnly();

		m_wndEdit1_26.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_26.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_26.SetReadOnly();

		m_wndEdit1_27.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_27.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_27.SetReadOnly();

		m_wndEdit1_29.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_29.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_29.SetReadOnly();

		m_wndEdit1_30.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1_30.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1_30.SetReadOnly();

		//#3385
//		m_wndEdit2_1.SetDisabledColor(BLACK,INFOBK );
//		m_wndEdit2_1.SetEnabledColor(BLACK,WHITE );
		m_wndEdit2_1.SetBkColor(WHITE);
//		m_wndEdit2_1.SetReadOnly();


//		m_wndCBox1.SetDisabledColor(BLACK,INFOBK );
//		m_wndCBox1.SetEnabledColor(BLACK,WHITE);

		

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		// H�mta ut om vi ska logga objekts�ndring eller ej (#3877)
		m_bLogObject = regGetInt(REG_ROOT, REG_LANDVALUE_KEY, REG_LANDVALUE_LOG_OBJECT, false);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupLanguage();

		setupReport();

// TypeOfInfr in propertyGrid; 090403 P�D
//		addLandValueTypeOfInfring();

		//#3385
		m_nShowObjects = 0;	//default visa endast p�g�ende objekt

		m_bIsLastVisitedObjectID_Valid = readLastObjectIDFromRegistry();

		setPopulationOfObject();
		
		//#3385 g�r en refresh p� listv�ljaren ifall den �r uppe
		CObjectSelListFormView* pListView = NULL;
		pListView = (CObjectSelListFormView*)getFormViewByID(IDD_FORMVIEW_SELLIST);
		if(pListView)
		{
			pListView->Refresh();
			pListView = NULL;
		}

		m_bInitialized = TRUE;
	}
}

BOOL CObjectFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CObjectFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

		// #3877: l�gg till koppling till admin databasen
		if (m_bLogObject == true)
		{
			if ( m_dbConnectionData.admin_conn->isConnected() )
			{
				m_pDBadmin = new CUMLandValueDB(m_dbConnectionData, 2);
			}
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

BOOL CObjectFormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	StrMap vecStrings;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		xml->LoadEx(m_sLangFN,vecStrings);
		delete xml;
	}

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_P30, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,ID_REPORT_HCOST, FALSE, FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport.GetSafeHwnd() != NULL)
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (vecStrings[IDS_STRING2202]), 80));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
		pCol->SetAlignment(DT_LEFT);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (vecStrings[IDS_STRING2209]), 60));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
		pCol->SetAlignment(DT_CENTER);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (vecStrings[IDS_STRING2203]), 60));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
		pCol->SetAlignment(DT_RIGHT);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (vecStrings[IDS_STRING2204]), 60));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
		pCol->SetAlignment(DT_RIGHT);

		m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
		m_wndReport.SetMultipleSelection( FALSE );
		m_wndReport.AllowEdit(FALSE);
		m_wndReport.GetPaintManager()->m_bHideSelection = TRUE;
		m_wndReport.GetPaintManager()->m_clrHighlightText = RGB(0,0,0);
		m_wndReport.GetPaintManager()->m_clrHighlight = INFOBK;
		m_wndReport.GetPaintManager()->m_clrControlBack = INFOBK;
		m_wndReport.GetPaintManager()->m_clrHeaderControl = INFOBK;
		m_wndReport.GetPaintManager()->m_clrControlLightLight = INFOBK;
		m_wndReport.GetPaintManager()->m_clrControlDark = INFOBK;
		m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
		// Setup Font for Headers; 080407 p�d
		LOGFONT lf;
		memset(&lf,0,sizeof(LOGFONT));
		lf.lfUnderline = TRUE;
		lf.lfHeight = 13;
		lf.lfWeight = FW_BOLD;
		m_wndReport.GetPaintManager()->SetCaptionFont(lf);
		m_wndReport.GetPaintManager()->m_clrCaptionText = RGB(0,0,255);

		// Need to set size of Report control; 071109 p�d
		CWnd *pWnd = GetDlgItem(ID_REPORT_P30);
		if (pWnd != NULL)
		{
			int tpp = TwipsPerPixel();
			pWnd->SetWindowPos(&CWnd::wndTop,15*15/tpp,260*15/tpp,280*15/tpp,170*15/tpp,SWP_SHOWWINDOW);	//changed #3385
		}

	}

	if (m_wndReport1.GetSafeHwnd() != NULL)
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_0, (vecStrings[IDS_STRING2206]), 60));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
		pCol->SetAlignment(DT_LEFT);

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_1, (vecStrings[IDS_STRING2207]), 60));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
		pCol->SetAlignment(DT_RIGHT);

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_2, (vecStrings[IDS_STRING2212]), 60));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
		pCol->SetAlignment(DT_RIGHT);

		pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_3, (vecStrings[IDS_STRING2208]), 60));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
		pCol->SetAlignment(DT_RIGHT);

		m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
		m_wndReport1.SetMultipleSelection( FALSE );
		m_wndReport1.AllowEdit(FALSE);
		m_wndReport1.GetPaintManager()->m_bHideSelection = TRUE;
		m_wndReport1.GetPaintManager()->m_clrHighlightText = RGB(0,0,0);
		m_wndReport1.GetPaintManager()->m_clrHighlight = INFOBK;
		m_wndReport1.GetPaintManager()->m_clrControlBack = INFOBK;
		m_wndReport1.GetPaintManager()->m_clrHeaderControl = INFOBK;
		m_wndReport1.GetPaintManager()->m_clrControlLightLight = INFOBK;
		m_wndReport1.GetPaintManager()->m_clrControlDark = INFOBK;
		m_wndReport1.GetPaintManager()->SetFixedRowHeight(FALSE);

		// Setup Font for Headers; 080407 p�d
		LOGFONT lf;
		memset(&lf,0,sizeof(LOGFONT));
		lf.lfUnderline = TRUE;
		lf.lfHeight = 13;
		lf.lfWeight = FW_BOLD;
		m_wndReport1.GetPaintManager()->SetCaptionFont(lf);
		m_wndReport1.GetPaintManager()->m_clrCaptionText = RGB(0,0,255);

		// Need to set size of Report control; 071109 p�d
		CWnd *pWnd = GetDlgItem(ID_REPORT_HCOST);
		if (pWnd != NULL)
		{
			int tpp = TwipsPerPixel();
			pWnd->SetWindowPos(&CWnd::wndTop,320*15/tpp,260*15/tpp,250*15/tpp,170*15/tpp,SWP_SHOWWINDOW);	//changed #3385
		}

	}


	vecStrings.clear();
	return TRUE;
}

//#HMS-94 20220920 kontroll av p30tr�dslagsinst�llningar, om ej finns s� skapa fr�n defaultinst�llningar
void CObjectFormView::checkP30SpeciesSettings(int nObjId)
{
	vecTransactionSpecies vecP30SpeciesObject;
	vecTransactionSpecies vecP30SpeciesGlobal;
	if (m_pDB)
	{
		m_pDB->getP30SpeciesInfo(vecP30SpeciesObject,nObjId);
		if(vecP30SpeciesObject.size()<=0)
		{
			m_pDB->getSpecies(vecP30SpeciesGlobal);
			if(vecP30SpeciesGlobal.size()>0)
				m_pDB->createP30SpeciesInfo(vecP30SpeciesGlobal,nObjId);
			else
			{
				AfxMessageBox(_T("Kunde inte skapa p30 tr�dslagsinst�llningar f�r objektet!! Kanttr�dsv�rdesber�kning kommer ej fungera"));
			}
		}
	}
	else
		AfxMessageBox(_T("Kunde inte skapa p30 tr�dslagsinst�llningar f�r objektet!! Kanttr�dsv�rdesber�kning kommer ej fungera"));
	vecP30SpeciesObject.clear();
	vecP30SpeciesGlobal.clear();
}

void CObjectFormView::populateData(void)
{
	CString S;
	// Collect data from PropertyGrid - Settings on Frame; 080319 p�d
	vecObjectTemplate_hcost_table vecHCost;
	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_p30_table vecP30;
	CHCostReportRec *pRec = NULL;
	int nObjectID = -1;
	int nContactID = -1;
	int nReturnContactID = -1;
	int nLogoID = -1;
	CString sPicPath;
	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	// Check that we have data m_vecObjectIndex
	// and that idx is within size of m_vecObjectIndex; 080328 p�d
	if (m_vecObjectIndex.size() > 0 &&
		  m_nDBIndex >= 0 &&
			m_nDBIndex < m_vecObjectIndex.size())
	{

		setEnableWindows(TRUE,FALSE);
		if (pFormView) pFormView->getTabCtrl().EnableWindow(TRUE);
		nObjectID = m_vecObjectIndex[m_nDBIndex];
		// Load ObjectData for ObjectID in m_vecObjectIndex; 080328 p�d
		getObjectFromDB(nObjectID,m_recActiveObject);
		checkP30SpeciesSettings(nObjectID);
		nContactID = m_recActiveObject.getObjContractorID();
		nReturnContactID = m_recActiveObject.getObjReturnAddressID();
		nLogoID  = m_recActiveObject.getObjLogoID();
		if (nLogoID > 0)
		{
			// Try to load image from DB and save to disk; 090825 p�d
			TCHAR lpTempPathBuffer[1025];
			DWORD dwRetVal = GetTempPath(1024, lpTempPathBuffer);
			CString csBuf = lpTempPathBuffer;

			sPicPath.Format(_T("%s%s"), csBuf, _T("tmp.jpg"));
			if (m_pDB)
			{
				if (m_pDB->loadImage(sPicPath,nLogoID))
				{
					if (fileExists(sPicPath)) 
					{
						m_wndPicCtrl.Load(sPicPath);
						m_wndPicCtrl.setPictureID(nLogoID);
					}
				}
			}
			Invalidate();
			UpdateWindow();
			m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		
			removeFile(sPicPath);
		}
		else
		{
			m_wndPicCtrl.FreeData();
			m_wndPicCtrl.setPictureID(-1);
			Invalidate();
			UpdateWindow();
			m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		
		}

		m_wndEdit1_21.SetWindowText(m_recActiveObject.getObjNotes());
		if (nContactID > 0)
		{
			getContractorFromDB(nContactID,m_recActiveContractor);
					
			// Start populating form; 080328 p�d
			// Contractor data
			setContractorID_pk(nContactID);
			setPersOrgNum(m_recActiveContractor.getPNR_ORGNR());
			setContactPers(m_recActiveContractor.getName());
			setCompany(m_recActiveContractor.getCompany());
//			setAddress(m_recActiveContractor.getAddress());
//			setPostnum(m_recActiveContractor.getPostNum());
//			setPostAddress(m_recActiveContractor.getPostAddress());
			setPhoneWork(cleanCRLF(m_recActiveContractor.getPhoneWork(),L", "));
			setPhoneHome(cleanCRLF(m_recActiveContractor.getPhoneHome(),L", "));
			setMobile(m_recActiveContractor.getMobile());

		}
		else
		{
			setContractorID_pk(nContactID);	// >> -1
			clearContractor();	// Just clear data for Contractor; 080401 p�d
		}
		
		if (nReturnContactID > 0)
		{
			// Get Return address information; 080603 p�d
			getContractorFromDB(nReturnContactID,m_recActiveContractor);
			
			setReturnContactID(nReturnContactID);
			setReturnName(m_recActiveContractor.getName());
			setReturnAddress(cleanCRLF(m_recActiveContractor.getAddress(),L", "));
			setReturnPostNumber(m_recActiveContractor.getPostNum());
			setReturnPostAddress(m_recActiveContractor.getPostAddress());

		}
		else
		{
			m_wndEdit1_16.setItemIntData(-1);	// Nothing
			m_wndEdit1_16.SetWindowText(_T(""));
			m_wndEdit1_17.SetWindowText(_T(""));
			m_wndEdit1_18.SetWindowText(_T(""));
			m_wndEdit1_19.SetWindowText(_T(""));
		}

		// General data
		setObjectName(m_recActiveObject.getObjectName());
		setObjectID(m_recActiveObject.getObjIDNumber());
		setErrandNum(m_recActiveObject.getObjErrandNum());
		setLineLittra(m_recActiveObject.getObjLittra());
//		setGrowthArea(m_recActiveObject.getObjGrowthArea());
//		setTranspDist(m_recActiveObject.getObjTransportDist());
//		setLatitude(m_recActiveObject.getObjLatitude());
//		setAltitude(m_recActiveObject.getObjAltitude());
// TypeOfInfr in propertyGrid; 090403 P�D
//		setTypeOfInfring(m_recActiveObject.getObjTypeOfInfring());
		setDoneBy(m_recActiveObject.getObjCreatedBy());
		setStartDate(m_recActiveObject.getObjStartDate());

		// Populate table for "P30 priser"; 080407 p�d
		populateP30Table();

		// Populate table for "F�rdyrad avverkning"; 080407 p�d
		populateHCostTable();

		// Collect data from PropertyGrid - Settings on Frame; 080319 p�d
		if (pFrame != NULL)
		{
			pFrame->setActiveObject(m_recActiveObject,FALSE /* Don't clear */);
			pFrame->setPopulateSettings();
			pFrame->setSubMenuItemsExcluded(m_recActiveObject.getObjStatus());	//new #3385
		}

		if (pFormView != NULL)
		{
			CPropertiesFormView *pProp = pFormView->getPropertiesFormView();
			if (pProp != NULL)
			{
				if (pProp->populateProperties())
					pProp->setActivePropertyInReport();
				else
				{
					pProp->resetEvaluatedReport();
					pProp->resetCruisingReport();
					pProp->setActivePropertyInReport();
				}
				// Need to release ref. pointers; 080520 p�d
				pProp = NULL;
			}
		}
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecObjectIndex.size()-1));

		if (!m_bIsPRelOKInP30Table)
		{
			S.Format(m_sPRelErrMsg,MIN_PREL_VALUE,MAX_PREL_VALUE);		
			::MessageBox(this->GetSafeHwnd(),S,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}

		//#3385
		setObjectStatusLbl(m_recActiveObject.getObjStatus());

	}	// if (m_vecObjectIndex.size > 0 &&
	// Clear all, NO OBJECTS; 080409 p�d
	else
	{
		// Clear all information i there's no more Objects; 080903 p�d
		clearContractor();
		clearObject();
		if (pFrame != NULL)
		{
			pFrame->clearSettingsPropertyGrid();
			pFrame->enablePropertyGrid(FALSE);
			pFrame->m_wndInfoDlgBar.setPropertyRecord(CTransaction_elv_properties(),FALSE);	
			pFrame->setActiveObject(m_recActiveObject,TRUE /* Clear */);
			pFrame->setSubMenuItemsExcluded(m_recActiveObject.getObjStatus());	//new #3385
		}	// if (pFrame != NULL)
		if (pFormView != NULL)
		{
			CPropertiesFormView *pProp1 = pFormView->getPropertiesFormView();
			if (pProp1 != NULL)
			{
				pProp1->resetEvaluatedReport();
				pProp1->resetCruisingReport();
				pProp1->resetPropertyReport();
			}	// if (pProp1 != NULL)

			setEnableWindows(FALSE,TRUE);
			pFormView->getTabCtrl().EnableWindow(FALSE);
		}
		// Don't navigate; 090311 p�d
		setNavigationButtons(FALSE,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
	pFormView = NULL;
	pFrame = NULL;
}

void CObjectFormView::populateHCostTable(void)
{
	int nTypeSet = -1;
	double fBreakValue = 0.0;
	double fFactor = 0.0;
	CString sName,sDoneBy;
	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_hcost_table vecHCost;
	CTransaction_template recTmpl;
	CHCostReportRec *pRec = NULL;
	int nObjID = m_recActiveObject.getObjID_pk();

	m_wndReport1.ResetContent();
	if (pars.LoadFromBuffer(m_recActiveObject.getObjHCostXML() ))
	{
		vecHCost.clear();
		pars.getObjTmplHCost(&nTypeSet,&fBreakValue,&fFactor,vecHCost,sName,sDoneBy);
		if (nTypeSet != 1 && nTypeSet != 2) nTypeSet = 1;
		if (nTypeSet == 1)
		{
			m_wndEdit1_29.ShowWindow(SW_HIDE);
			m_wndEdit1_30.ShowWindow(SW_HIDE);
			m_wndLbl1_24.ShowWindow(SW_HIDE);
			m_wndLbl1_25.ShowWindow(SW_HIDE);
			m_wndReport1.ShowWindow(SW_NORMAL);
			if (vecHCost.size() > 0)
			{
				for (UINT i = 0;i < vecHCost.size();i++)
				{
					recTmpl = CTransaction_template(nObjID,m_recActiveObject.getObjHCostName(),
																								 m_recActiveObject.getObjHCostTypeOf(),
																								 m_recActiveObject.getObjHCostXML(),
																								 _T(""),
																								 _T(""));
					CObjectTemplate_hcost_table rec = vecHCost[i];
					m_wndReport1.AddRecord((pRec = new CHCostReportRec(recTmpl,
																															CTransaction_elv_hcost(i+1,
																																										 nObjID,
																																										 rec.getM3Sk(),
																																										 rec.getPriceM3Sk(),
																																										 rec.getBaseComp(),
																																										 rec.getSumPrice()))));
					if (pRec != NULL)	pRec->setColumnBold(1,COLUMN_2);
				}	// for (UINT i = 0;i < vecP30.size();i++)
				m_wndReport1.Populate();
				m_wndReport1.UpdateWindow();
				// Added 081006 p�d
				m_wndReport1.RedrawControl();
			}	// if (vecHCost.size() > 0)
		}
		else if (nTypeSet == 2)
		{
			m_wndReport1.ShowWindow(SW_HIDE);
			m_wndEdit1_29.ShowWindow(SW_NORMAL);
			m_wndEdit1_30.ShowWindow(SW_NORMAL);
			m_wndLbl1_24.ShowWindow(SW_NORMAL);
			m_wndLbl1_25.ShowWindow(SW_NORMAL);
			m_wndEdit1_29.setFloat(fBreakValue,0);
			m_wndEdit1_30.setFloat(fFactor,0);
		}
	}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))

}

void CObjectFormView::populateP30Table(void)
{
	CString sName,sDoneBy,sNotes;
	CString sArea,sSI_FromTo;

	int dAreaIndex;
	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_p30_table vecP30;
	CTransaction_template recTmpl;
	vecObjectTemplate_p30_nn_table vecP30_nn;
	CTransaction_template recTmpl_nn;
	m_bIsPRelOKInP30Table = TRUE;

	int nObjID = m_recActiveObject.getObjID_pk();

	m_wndReport.ResetContent();
	if (pars.LoadFromBuffer(m_recActiveObject.getObjP30XML() ))
	{
		// Check to see which ForestNorm we're running. TEMPLATE_P30 = "1950:�rs skogsnorm"; 090420 p�d
		if (m_recActiveObject.getObjP30TypeOf() == TEMPLATE_P30)
		{
			vecP30.clear();
			pars.getObjTmplP30(vecP30,sName,sDoneBy,sNotes);
			if (vecP30.size() > 0)
			{
				for (UINT i = 0;i < vecP30.size();i++)
				{
					recTmpl = CTransaction_template(nObjID,m_recActiveObject.getObjHCostName(),
						m_recActiveObject.getObjHCostTypeOf(),
						m_recActiveObject.getObjHCostXML(),
						_T(""),
						_T(""));

					CObjectTemplate_p30_table rec = vecP30[i];
					m_wndReport.AddRecord(new CP30ReportRec(recTmpl,
						CTransaction_elv_p30(rec.getSpcID(),
						nObjID,
						rec.getSpcID(),
						rec.getSpcName(),
						rec.getPrice(),
						rec.getPriceRel()),
						rec.getSpcName(),
						_T("---"),
						rec.getPrice(),
						rec.getPriceRel()));

				}	// for (UINT i = 0;i < vecP30.size();i++)
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
				// Added 081006 p�d
				m_wndReport.RedrawControl();
				//m_wndReport.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
			}	// if (vecP30.size() > 0)
		}	// if (m_recActiveObject.getObjP30TypeOf() == TEMPLATE_P30)
		// Check to see which ForestNorm we're running. TEMPLATE_P30 _NEW_NORM= "ny skogsnorm"; 090420 p�d
		else if (m_recActiveObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
		{
			vecP30_nn.clear();
			pars.getObjTmplP30_nn(vecP30_nn,sArea,&dAreaIndex);

			if (vecP30_nn.size() > 0)
			{
				// Just check P30-tabel, to see if there's any 'prisrelation' GT 0.8.
				// If so, make user aware of the fact; 101006 p�d
				for (UINT i = 0;i < vecP30_nn.size();i++)
				{
					CObjectTemplate_p30_nn_table rec_nn = vecP30_nn[i];
					if (rec_nn.getPriceRel() > MAX_PREL_VALUE || 
							rec_nn.getPriceRel() < MIN_PREL_VALUE)
					{
						m_bIsPRelOKInP30Table = FALSE;
					}
				}	// for (UINT i = 0;i < vecP30_nn.size();i++)


				for (UINT i = 0;i < vecP30_nn.size();i++)
				{
					recTmpl = CTransaction_template(nObjID,m_recActiveObject.getObjHCostName(),
						m_recActiveObject.getObjHCostTypeOf(),
						m_recActiveObject.getObjHCostXML(),
						_T(""),
						_T(""));

					CObjectTemplate_p30_nn_table rec_nn = vecP30_nn[i];
					sSI_FromTo.Format(_T("%s%d-%s%d"),rec_nn.getSI(),rec_nn.getFrom(),rec_nn.getSI(),rec_nn.getTo());
					m_wndReport.AddRecord(new CP30ReportRec(recTmpl,
						CTransaction_elv_p30(rec_nn.getSpcID(),
						nObjID,
						rec_nn.getSpcID(),
						rec_nn.getSpcName(),
						rec_nn.getPrice(),
						rec_nn.getPriceRel()),
						rec_nn.getSpcName(),
						sSI_FromTo,
						rec_nn.getPrice(),
						rec_nn.getPriceRel()));

				}	// for (UINT i = 0;i < vecP30.size();i++)
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
				// Added 081006 p�d
				//m_wndReport.RedrawControl();
				//m_wndReport.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
			}	// if (vecP30_nn.size() > 0)
		}
		else if (m_recActiveObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
		{
			vecP30_nn.clear();
			pars.getObjTmplP30_nn(vecP30_nn,sArea,&dAreaIndex);

			if (vecP30_nn.size() > 0)
			{
				// Just check P30-tabel, to see if there's any 'prisrelation' GT 0.8.
				// If so, make user aware of the fact; 101006 p�d
				for (UINT i = 0;i < vecP30_nn.size();i++)
				{
					CObjectTemplate_p30_nn_table rec_nn = vecP30_nn[i];
					if (rec_nn.getPriceRel() > MAX_PREL_VALUE || 
							rec_nn.getPriceRel() < MIN_PREL_VALUE)
					{
						m_bIsPRelOKInP30Table = FALSE;
					}
				}	// for (UINT i = 0;i < vecP30_nn.size();i++)


				for (UINT i = 0;i < vecP30_nn.size();i++)
				{
					recTmpl = CTransaction_template(nObjID,m_recActiveObject.getObjHCostName(),
						m_recActiveObject.getObjHCostTypeOf(),
						m_recActiveObject.getObjHCostXML(),
						_T(""),
						_T(""));

					CObjectTemplate_p30_nn_table rec_nn = vecP30_nn[i];
					sSI_FromTo.Format(_T("%s%d-%s%d"),rec_nn.getSI(),rec_nn.getFrom(),rec_nn.getSI(),rec_nn.getTo());
					m_wndReport.AddRecord(new CP30ReportRec(recTmpl,
						CTransaction_elv_p30(rec_nn.getSpcID(),
						nObjID,
						rec_nn.getSpcID(),
						rec_nn.getSpcName(),
						rec_nn.getPrice(),
						rec_nn.getPriceRel()),
						rec_nn.getSpcName(),
						sSI_FromTo,
						rec_nn.getPrice(),
						rec_nn.getPriceRel()));

				}	// for (UINT i = 0;i < vecP30.size();i++)
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
				// Added 081006 p�d
				//m_wndReport.RedrawControl();
				//m_wndReport.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
			}	// if (vecP30_nn.size() > 0)
		}

	}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
}


void CObjectFormView::setupLanguage(void)
{
	CString sCaption;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndGroup1_1.SetWindowText((xml->str(IDS_STRING101)));
			m_wndGroup1_2.SetWindowText((xml->str(IDS_STRING100)));
			sCaption.Format(_T("%s / %s"),(xml->str(IDS_STRING102)),(xml->str(IDS_STRING103)));
			m_wndGroup1_7.SetWindowText(sCaption);
			m_wndGroup1_6.SetWindowText((xml->str(IDS_STRING1040)));
			m_wndGroup1_8.SetWindowText((xml->str(IDS_STRING1041)));
			m_wndGroup1_9.SetWindowText((xml->str(IDS_STRING6310)));

			m_sMsgCap = (xml->str(IDS_STRING229));
			m_sCreateNewObjectFromTmpl = (xml->str(IDS_STRING2620));
			m_sCreateNewObjectFromOtherObject.Format(_T("%s\n\n%s"),
					(xml->str(IDS_STRING2621)),
					(xml->str(IDS_STRING2622)));

			m_sMandatoryDataMissing.Format(_T("%s\n\n%s\n\n%s\n%s\n\n"),
					(xml->str(IDS_STRING2900)),
					(xml->str(IDS_STRING2901)),
					(xml->str(IDS_STRING2903)),
					(xml->str(IDS_STRING2904)));
			
			m_sQuitAnyway = (xml->str(IDS_STRING2905));
			
			m_sRemoveObject.Format(_T("%s\n\n%s\n%s\n\n"),
					(xml->str(IDS_STRING2290)),
					(xml->str(IDS_STRING2291)),
					(xml->str(IDS_STRING2292)));
		
			m_sSearchDlgTitle1 = (xml->str(IDS_STRING2410));
			m_sSearchDlgTitle2 = (xml->str(IDS_STRING2411));
			m_sSearchDlgTitle3 = (xml->str(IDS_STRING2418));
			
			m_sNoObjectMsg.Format(_T("%s\n%s\n\n"),
				xml->str(IDS_STRING6004),
				xml->str(IDS_STRING6005));

			m_sRemoveLogoMsg = xml->str(IDS_STRING6311);
			m_sRemoveContractorMsg = xml->str(IDS_STRING6312);
			m_sRemoveRetAddressMsg = xml->str(IDS_STRING6313);

			m_sPRelErrMsg.Format(L"%s\n\n%s\n%s\n",
				xml->str(IDS_STRING6314),
				xml->str(IDS_STRING6315),
				xml->str(IDS_STRING6316));

			m_sGoToLastObjMsg = xml->str(IDS_STRING2262);
			
			m_sGrowthAreaLbl = xml->str(IDS_STRING207);
			m_wndLbl1_1.SetWindowText((xml->str(IDS_STRING203)));
			m_wndLbl1_2.SetWindowText((xml->str(IDS_STRING204)));
			m_wndLbl1_3.SetWindowText((xml->str(IDS_STRING205)));
			m_wndLbl1_4.SetWindowText((xml->str(IDS_STRING206)));
//			m_wndLbl1_5.SetWindowText(m_sGrowthAreaLbl);
			m_wndLbl1_6.SetWindowText((xml->str(IDS_STRING210)));
			m_wndLbl1_7.SetWindowText((xml->str(IDS_STRING2110)));
//			m_wndLbl1_8.SetWindowText((xml->str(IDS_STRING213)));
//			m_wndLbl1_9.SetWindowText((xml->str(IDS_STRING216)));
//			m_wndLbl1_10.SetWindowText((xml->str(IDS_STRING217)));
// TypeOfInfr in propertyGrid; 090403 P�D
//			m_wndLbl1_11.SetWindowText((xml->str(IDS_STRING214)));
			m_wndLbl1_12.SetWindowText((xml->str(IDS_STRING2400)));
			m_wndLbl1_13.SetWindowText((xml->str(IDS_STRING2401)));
			m_wndLbl1_14.SetWindowText((xml->str(IDS_STRING2402)));
			//m_wndLbl1_15.SetWindowText((xml->str(IDS_STRING2404)));
			//m_wndLbl1_16.SetWindowText((xml->str(IDS_STRING2405)));
			m_wndLbl1_17.SetWindowText((xml->str(IDS_STRING2406)));
			//m_wndLbl1_18.SetWindowText((xml->str(IDS_STRING2403)));
			m_wndLbl1_19.SetWindowText((xml->str(IDS_STRING2407)));
			m_wndLbl1_20.SetWindowText((xml->str(IDS_STRING2408)));
		
			m_wndLbl1_21.SetWindowText((xml->str(IDS_STRING5500)));
			m_wndLbl1_22.SetWindowText((xml->str(IDS_STRING5501)));
			m_wndLbl1_23.SetWindowText((xml->str(IDS_STRING5502)));
			m_wndLbl1_24.SetWindowText((xml->str(IDS_STRING4300)));
			m_wndLbl1_25.SetWindowText((xml->str(IDS_STRING4301)));

			m_wndAddContractor.SetWindowText((xml->str(IDS_STRING2413)));
			m_wndDelContractor.SetWindowText((xml->str(IDS_STRING222)));
			m_wndShowContractor.SetWindowText((xml->str(IDS_STRING2417)));
			m_wndAddRetAdr.SetWindowText((xml->str(IDS_STRING5503)));
			m_wndDelRetAdr.SetWindowText((xml->str(IDS_STRING222)));
			m_wndAddLogo.SetWindowText((xml->str(IDS_STRING221)));
			m_wndDelLogo.SetWindowText((xml->str(IDS_STRING222)));
			m_sPropTabCaption = (xml->str(IDS_STRING201));
			//#3385
			m_wndLbl2_1.SetLblFont(16,FW_BOLD);
			m_wndLbl2_1.SetWindowTextW(xml->str(IDS_STRING1042));
			m_sStatusLblOngoing = xml->str(IDS_STRING1043);
			m_sStatusLblFinished = xml->str(IDS_STRING1044);

		}
		delete xml;
	}
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CObjectFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CObjectFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			// Goto last Object; 091009 p�d
			m_bIsLastVisitedObjectID_Valid = FALSE;
			newObject();
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (saveObject(0))
				populateData();
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			// Goto last Object; 091009 p�d
			m_bIsLastVisitedObjectID_Valid = FALSE;
			removeObject();
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveObject(0))
			{
				m_nDBIndex = 0;
			}
			doPopulateData(m_nDBIndex);

			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveObject(0))
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
			}
			doPopulateData(m_nDBIndex);

			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveObject(0))
			{
				m_nDBIndex++;

				if (m_nDBIndex > ((UINT)m_vecObjectIndex.size() - 1))
					m_nDBIndex = (UINT)m_vecObjectIndex.size() - 1;
			}
			doPopulateData(m_nDBIndex);

			break;
		}
		case ID_DBNAVIG_END :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d
			if (saveObject(0))
			{
				m_nDBIndex = (UINT)m_vecObjectIndex.size()-1;
			}
			doPopulateData(m_nDBIndex);

			break;
		}	// case ID_NEW_ITEM :
	}	// switch (wParam)
	return 0L;
}


// CObjectFormView message handlers

void CObjectFormView::getContractorFromDB(int contact_id,CTransaction_contacts &rec)
{
	if (m_pDB != NULL)
	{
		m_pDB->getContact(contact_id,rec);
	}	// if (m_pDB != NULL)
}

// This method activates every time we want to
// read an Object from the DataBase; 080328 p�d
void CObjectFormView::getObjectFromDB(int obj_id,CTransaction_elv_object &rec)
{
	if (m_pDB != NULL)
	{
		m_pDB->getObject(obj_id,rec);
	}	// if (m_pDB != NULL)
}

void CObjectFormView::getObjectIndexFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getObjectIndex(m_vecObjectIndex, m_nShowObjects);	//changed #3385 filtrerar ut p� objektsstatus
	}	// if (m_pDB != NULL)
}

/*
BOOL CObjectFormView::getSpeciesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getSpecies(m_vecSpecies);
			return TRUE;
		}
	}
	return FALSE;
}
*/

BOOL CObjectFormView::getTraktTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getTemplates(m_vecTraktTemplate,ID_TEMPLATE_TRAKT);	// Only valid Templates; 080407 p�d
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CObjectFormView::getObjectP30TemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getObjectTemplates(m_vecObjectP30Template,TEMPLATE_P30);	// Only valid Templates; 080407 p�d
			m_pDB->getObjectTemplates(m_vecObjectP30TemplateNN,TEMPLATE_P30_NEW_NORM);	// Only valid Templates; 080407 p�d
			m_pDB->getObjectTemplates(m_vecObjectP30Template2018,TEMPLATE_P30_2018_NORM);	// Only valid Templates; 20180321 J�
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CObjectFormView::getObjectHCostTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getObjectTemplates(m_vecObjectHCostTemplate,TEMPLATE_HCOST);	// Only valid Templates; 080407 p�d
			return TRUE;
		}
	}
	return FALSE;
}

void CObjectFormView::clearContractor(void)
{
	m_wndEdit1_6.setItemIntData(-1);	// Nothing
	m_wndEdit1_6.SetWindowText(_T(""));
	m_wndEdit1_7.SetWindowText(_T(""));
	m_wndEdit1_8.SetWindowText(_T(""));
//	m_wndEdit1_9.SetWindowText(_T(""));
//	m_wndEdit1_15.SetWindowText(_T(""));
//	m_wndEdit1_24.SetWindowText(_T(""));
	m_wndEdit1_25.SetWindowText(_T(""));
	m_wndEdit1_26.SetWindowText(_T(""));
	m_wndEdit1_27.SetWindowText(_T(""));
}

void CObjectFormView::clearObject(void)
{
	m_wndEdit1_1.SetWindowText(_T(""));
	m_wndEdit1_2.SetWindowText(_T(""));
	m_wndEdit1_3.SetWindowText(_T(""));
	m_wndEdit1_4.SetWindowText(_T(""));
//	m_wndEdit1_5.SetWindowText(_T(""));
	m_wndEdit1_10.SetWindowText((getUserName().MakeUpper()));
	m_wndEdit1_11.SetWindowText(_T(""));
//	m_wndEdit1_12.SetWindowText(_T(""));
//	m_wndEdit1_13.SetWindowText(_T(""));
//	m_wndEdit1_14.SetWindowText(_T(""));
	m_wndEdit1_21.SetWindowText(_T(""));
	m_wndEdit1_16.setItemIntData(-1);	// Nothing
	m_wndEdit1_16.SetWindowText(_T(""));
	m_wndEdit1_17.SetWindowText(_T(""));
	m_wndEdit1_18.SetWindowText(_T(""));
	m_wndEdit1_19.SetWindowText(_T(""));
//	m_wndCBox1.SetCurSel(-1);
	
	
   //Caption reset p� sammanst�llnign fastigheter 20121203 #3500
	CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFormView != NULL)
	{
		CXTPTabManagerItem *m_tabManager;
		m_tabManager = pFormView->getTabCtrl().getTabPage(TAB_PROPERTIES);
		if (m_tabManager != NULL)
			m_tabManager->SetCaption(m_sPropTabCaption);
	}

	//Logo reset 20121203 #3500
	m_wndPicCtrl.FreeData();
	m_wndPicCtrl.setPictureID(-1);
	m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		

	m_wndReport.ResetContent();
	m_wndReport1.ResetContent();

	//#3385
	setObjectStatusLbl(-1);
}

void CObjectFormView::setEnableWindows(BOOL enable,BOOL is_empty)
{
	if (is_empty)
	{
		setEditWindow(&m_wndEdit1_1,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_2,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_3,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_4,COL3DFACE,enable);
//		setEditWindow(&m_wndEdit1_5,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_6,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_7,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_8,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_9,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_10,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_11,COL3DFACE,enable);
//		setEditWindow(&m_wndEdit1_12,COL3DFACE,enable);
//		setEditWindow(&m_wndEdit1_13,COL3DFACE,enable);
//		setEditWindow(&m_wndEdit1_14,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_15,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_16,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_17,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_18,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_19,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_21,COL3DFACE,enable);
		//setEditWindow(&m_wndEdit1_24,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_25,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_26,COL3DFACE,enable);
		setEditWindow(&m_wndEdit1_27,COL3DFACE,enable);
		//setCBoxWindow(&m_wndCBox1,COL3DFACE,enable);
	}
	else
	{
		setEditWindow(&m_wndEdit1_1,INFOBK,enable);
		setEditWindow(&m_wndEdit1_2,INFOBK,enable);
		setEditWindow(&m_wndEdit1_3,INFOBK,enable);
		setEditWindow(&m_wndEdit1_4,INFOBK,enable);
//		setEditWindow(&m_wndEdit1_5,INFOBK,enable);
		// Contractor
		setEditWindowEx(&m_wndEdit1_6,INFOBK,enable);
		setEditWindowEx(&m_wndEdit1_7,INFOBK,enable);
		setEditWindowEx(&m_wndEdit1_8,INFOBK,enable);
		//setEditWindowEx(&m_wndEdit1_9,INFOBK,enable);
		//setEditWindowEx(&m_wndEdit1_15,INFOBK,enable);
		//setEditWindowEx(&m_wndEdit1_24,INFOBK,enable);
		setEditWindowEx(&m_wndEdit1_25,INFOBK,enable);
		setEditWindowEx(&m_wndEdit1_26,INFOBK,enable);
		setEditWindowEx(&m_wndEdit1_27,INFOBK,enable);

		setEditWindow(&m_wndEdit1_10,INFOBK,enable);
		setEditWindow(&m_wndEdit1_11,INFOBK,enable);
//		setEditWindow(&m_wndEdit1_12,INFOBK,enable);
//		setEditWindow(&m_wndEdit1_13,INFOBK,enable);
//		setEditWindow(&m_wndEdit1_14,INFOBK,enable);
		//setEditWindow(&m_wndEdit1_16,INFOBK,enable);
		//setEditWindow(&m_wndEdit1_17,INFOBK,enable);
		//setEditWindow(&m_wndEdit1_18,INFOBK,enable);
		//setEditWindow(&m_wndEdit1_19,INFOBK,enable);
		setEditWindow(&m_wndEdit1_21,INFOBK,enable);
		//setCBoxWindow(&m_wndCBox1,INFOBK,enable);
	}

	m_wndAddContractor.EnableWindow(enable);
	m_wndDelContractor.EnableWindow(enable);
	m_wndShowContractor.EnableWindow(enable);
	m_wndAddLogo.EnableWindow(enable);
	m_wndDelLogo.EnableWindow(enable);
	m_wndAddRetAdr.EnableWindow(enable);
	m_wndDelRetAdr.EnableWindow(enable);
}

void CObjectFormView::doSetNavigationButtons()
{
	if (m_vecObjectIndex.size() == 0)
	{
		m_nDBIndex = -1;
		setNavigationButtons(FALSE,FALSE);
	}
	else
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecObjectIndex.size()-1));
}

BOOL CObjectFormView::newObject(void)
{
	CString S;
	CNewObjectFromTemplates *pDlgFromTemplates = NULL;
	CSelectObjectDlg *pDlgFromObject = NULL;
	int nReturn = -1;
	vecTransaction_elv_object vecObjects;
	// Get Objects alreadey created, in database; 080409 p�d
	if (m_pDB != NULL)
			m_pDB->getObject(vecObjects);

	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);

	// Save any changes
	saveObject(0);

	// Check if there's any Objects already added to database.
	// If not, ask user to create the Object 1-Usin' templates, 2-Manually; 080407 p�d
	if (m_vecObjectIndex.size() == 0)	// No objects; 080407 p�d
	{
		getTraktTemplatesFromDB();
		getObjectP30TemplatesFromDB();
		getObjectHCostTemplatesFromDB();
		pDlgFromTemplates = new CNewObjectFromTemplates();
		if (pDlgFromTemplates != NULL)
		{
			pDlgFromTemplates->setTraktTemplates(m_vecTraktTemplate);
			pDlgFromTemplates->setObjectP30Templates(m_vecObjectP30Template);
			pDlgFromTemplates->setObjectP30TemplatesNN(m_vecObjectP30TemplateNN);
			pDlgFromTemplates->setObjectP30Templates2018(m_vecObjectP30Template2018);
			pDlgFromTemplates->setObjectHCostTemplates(m_vecObjectHCostTemplate);
			pDlgFromTemplates->setObjects(vecObjects);
			nReturn = pDlgFromTemplates->DoModal();
			if (nReturn == IDOK)
			{
				if (pFormView != NULL)
				{
					CPropertiesFormView *pProp1 = pFormView->getPropertiesFormView();
					if (pProp1 != NULL)
					{
						pProp1->resetEvaluatedReport();
						pProp1->resetCruisingReport();
						pProp1->resetPropertyReport();
						pProp1->setActivePropertyInReport();
					}	// if (pProp1 != NULL)
				}

				createNewObjectFromTemplates(CTransaction_template(),//pDlgFromTemplates->getSelectedTemplate(),
																		 pDlgFromTemplates->getSelectedP30Template(),
																		 pDlgFromTemplates->getSelectedHCostTemplate(),
																		 pDlgFromTemplates->getObjectName(),
																		 pDlgFromTemplates->getObjectNumber(),
																		 pDlgFromTemplates->getObjectForestNormIndex(),
																		 pDlgFromTemplates->getObjectGrowthArea());
			}
			delete pDlgFromTemplates;
		}	// if (pDlgFromTemplates != NULL)
	}
	// If there's objects already created, give user a choice.
	// If he say NO to created from Object, he'll be asked to create from
	// templates. If NO just clear; 090219 p�d
	else	
	{
		pDlgFromObject = new CSelectObjectDlg();
		if (pDlgFromObject != NULL)
		{
			pDlgFromObject->setObjects(vecObjects);
			nReturn = pDlgFromObject->DoModal();
			if (nReturn == IDOK)
			{
				if (pFormView != NULL)
				{
					CPropertiesFormView *pProp1 = pFormView->getPropertiesFormView();
					if (pProp1 != NULL)
					{
						pProp1->resetEvaluatedReport();
						pProp1->resetCruisingReport();
						pProp1->resetPropertyReport();
						pProp1->setActivePropertyInReport();
					}	// if (pProp1 != NULL)
				}

				createNewObjectFromOtherObject(pDlgFromObject->getSelectedObject(),
																			 pDlgFromObject->getObjName(),
																			 pDlgFromObject->getObjID());
			}	// if (nReturn == IDOK)
			// Create from Templates; 090219 p�d
//			else if (nReturn == IDCANCEL)
			else if (nReturn == ID_BTN_FROM_TMPL_RETURN)
			{
				getTraktTemplatesFromDB();
				getObjectP30TemplatesFromDB();
				getObjectHCostTemplatesFromDB();
				pDlgFromTemplates = new CNewObjectFromTemplates();
				if (pDlgFromTemplates != NULL)
				{
					pDlgFromTemplates->setTraktTemplates(m_vecTraktTemplate);
					pDlgFromTemplates->setObjectP30Templates(m_vecObjectP30Template);
					pDlgFromTemplates->setObjectP30TemplatesNN(m_vecObjectP30TemplateNN);
					pDlgFromTemplates->setObjectP30Templates2018(m_vecObjectP30Template2018);
					pDlgFromTemplates->setObjectHCostTemplates(m_vecObjectHCostTemplate);
					pDlgFromTemplates->setObjects(vecObjects);
					nReturn = pDlgFromTemplates->DoModal();
					if (nReturn == IDOK)
					{
						if (pFormView != NULL)
						{
							CPropertiesFormView *pProp1 = pFormView->getPropertiesFormView();
							if (pProp1 != NULL)
							{
								pProp1->resetEvaluatedReport();
								pProp1->resetCruisingReport();
								pProp1->resetPropertyReport();
								pProp1->setActivePropertyInReport();
							}	// if (pProp1 != NULL)
						}
						
						createNewObjectFromTemplates(CTransaction_template(),//pDlgFromTemplates->getSelectedTemplate(),
																				 pDlgFromTemplates->getSelectedP30Template(),
																				 pDlgFromTemplates->getSelectedHCostTemplate(),
																				 pDlgFromTemplates->getObjectName(),
																				 pDlgFromTemplates->getObjectNumber(),
																				 pDlgFromTemplates->getObjectForestNormIndex(),
																				 pDlgFromTemplates->getObjectGrowthArea());


					}
					delete pDlgFromTemplates;
				}	// if (pDlgFromTemplates != NULL)
			}	// else if (nReturn == IDNO)
			delete pDlgFromObject;
		}	// if (pDlgFromObject != NULL)
	}	// else

	return (nReturn == IDOK);
}

// Create Object from templates, Trakt-template,P30 and HigherCosts; 080407 p�d
void CObjectFormView::createNewObjectFromTemplates(CTransaction_template& tmpl,
																									 CTransaction_template& tmplP30,
																									 CTransaction_template& tmplHCost,
																									 CString obj_name,
																									 CString obj_number,
																									 int forest_norm_index,
																									 int growth_area)
{
	// Function(s) in UCLandValueNorm.dll; 081217 p�d
	vecUCFunctions vecForrestNormFunc;
	int nForestNormIndex = forest_norm_index;		// Selected
	CString sForestNormName;		// Selected

	CString sGrowthArea;

	CString S;
	CString sObjectName(obj_name);
	CString sObjectNumber(obj_number);
	int nLastObjID = 0;

	// Get Function(s) in UCLandValueNorm.dll; 081217 p�d
	getForrestNormFunctions(vecForrestNormFunc);
	if (nForestNormIndex >= 0 && nForestNormIndex < vecForrestNormFunc.size() )
		sForestNormName = vecForrestNormFunc[nForestNormIndex].getName();

	//-------------------------------------------------------------------
	// Start adding data to database; 080409 p�d
	sGrowthArea.Format(_T("%d"),growth_area);

	float maxPercent = 0.0;
	float percent = 0.0;
	float percentOfPriceBaseAmount = 3.0;

	CTransaction_elv_object newObject = CTransaction_elv_object(-1,	// New item
																														_T(""), //rec.getObjErrandNum(),
																														sObjectName, //rec.getObjectName(),
																														sObjectNumber, //rec.getObjIDNumber(),
																														_T(""), //rec.getObjLittra(),
																														sGrowthArea, //rec.getObjGrowthArea(),
																														0, //rec.getObjTransportDist(),
																														0,
																														0,
																														_T(""), //rec.getObjTypeOfInfring(),
																														0.0, //rec.getObjPresentWidth1(),
																														0.0, //rec.getObjPresentWidth2(),
																														0.0, //rec.getObjAddedWidth1(),
																														0.0, //rec.getObjAddedWidth2(),
																														0.0, //rec.getObjectParallelWidth(),
																														2, //rec.getObjTypeOfNet(),
																														0.0, //rec.getObjPriceDiv1(),
																														0.0, //rec.getObjPriceDiv2(),
																														0.0, //rec.getObjPriceBase(),
																														20.0, //rec.getObjMaxPercent(),
																														20.0,//rec.getObjPercent(),
																														0.0, //rec.getObjVAT(),
																														percentOfPriceBaseAmount,
																														nForestNormIndex, //rec.getObjUseNormID(),
																														sForestNormName, //rec.getObjUseNorm(),
																														0.0, //rec.getObjTakeCareOfPerc(),
																														0.0, //rec.getObjCorrFactor(),
																														_T(""), //rec.getObjNameOfPricelist(),
																														-1, //rec.getObjTypeOfPricelist(),
																														_T(""), //rec.getObjPricelistXML(),
																														_T(""),//rec.getObjNameOfCosts(),
																														-1, //rec.getObjTypeOfCosts(),
																														_T(""), // rec.getObjCostsXML(),
																														-1, //rec.getObjContractorID(),
																														0.0, //rec.getObjDCLS(),
																														_T(""), //rec.getObjExtraInfo(),
																														_T(""),
																														-1,
																														_T(""),
																														_T(""),
																														-1,
																														_T(""),
																														1,	// "Includera/Excludera MOMS"
																														1,	// "Includera/Excludera Frivillig uppg�relse"
																														1,	// "Includera/Excludera F�rdyrad avverkning"
																														1,	// "Includera/Excludera Annan ers�ttning"
																														getUserName().MakeUpper(),	// Default to user logon
																														_T(""),
																														-1, // "ID f�r returadress, h�mta fr�n kontakter"
																														-1,	// "ID logo, h�mta fr�n kontakter"
																														-1,	// Use Grot
																														_T(""), //m_wndEdit1_11.getText(),
																														INCREASE_INFR_PERCENT);	
																														
	if (m_pDB != NULL)
	{
		if (m_pDB->addObject(newObject))
		{
			// It's a new object'll so we need to get the
			// last ObjectID created; 080408 p�d
			int	nObjID = m_pDB->getLastObjectID();			
			if (nObjID > -1)
			{

				//HMS-94 Om nytt objekt fr�n templates s� spara p30 tr�dslagsinfo fr�n de globala tr�dslagsinst�llningarna
				vecTransactionSpecies vecP30SpeciesGlobal;
				m_pDB->getSpecies(vecP30SpeciesGlobal);
				m_pDB->createP30SpeciesInfo(vecP30SpeciesGlobal,nObjID);
				vecP30SpeciesGlobal.clear();

				// Save data for "P30-priser"; 080409 p�d
				// OBS! Only do update of the active object; 080408 p�d
				m_pDB->updObject_p30(nObjID,tmplP30.getTemplateName(),tmplP30.getTypeOf(),tmplP30.getTemplateFile());
				
				// Save data for "F�rdyrad avverkning"; 080409 p�d
				// OBS! Only do update of the active object; 080408 p�d
				m_pDB->updObject_hcost(nObjID,tmplHCost.getTemplateName(),tmplHCost.getTypeOf(),tmplHCost.getTemplateFile());
				
				// Added 2009-06-29 p�d
				// Save information on items to be included/excluded on reports.
				// OBS! Included is set as default; 090629 p�d
				m_pDB->updObject_include_exclude(nObjID,TRUE,TRUE,TRUE,TRUE,TRUE);

				setPopulationOfObject();
			}	// if (nObjID > -1)
		}	// if (m_pDB->addObject(newObject))
	}	// if (m_pDB != NULL)	

	vecForrestNormFunc.clear();

}

void CObjectFormView::createNewObjectFromOtherObject(CTransaction_elv_object& rec,CString obj_name,CString obj_id)
{
	CTransaction_elv_object newObject = CTransaction_elv_object(-1,	// New item
																															_T(""), //rec.getObjErrandNum(),
																															obj_name, //rec.getObjectName(),
																															obj_id, //rec.getObjIDNumber(),
																															_T(""), //rec.getObjLittra(),
																															rec.getObjGrowthArea(),
																															rec.getObjTransportDist(),
																															rec.getObjLatitude(),
																															rec.getObjAltitude(),
																															rec.getObjTypeOfInfring(),
																															0.0, //rec.getObjPresentWidth1(),
																															0.0, //rec.getObjPresentWidth2(),
																															0.0, //rec.getObjAddedWidth1(),
																															0.0, //rec.getObjAddedWidth2(),
																															0.0, //rec.getObjectParallelWidth(),
																															rec.getObjTypeOfNet(),
																															rec.getObjPriceDiv1(),
																															rec.getObjPriceDiv2(),
																															rec.getObjPriceBase(),
																															rec.getObjMaxPercent(),
																															rec.getObjPercent(),
																															rec.getObjVAT(),
																															rec.getObjPercentOfPriceBase(),
																															rec.getObjUseNormID(),
																															rec.getObjUseNorm(),
																															rec.getObjTakeCareOfPerc(),
																															rec.getObjCorrFactor(),
																															rec.getObjNameOfPricelist(),
																															rec.getObjTypeOfPricelist(),
																															rec.getObjPricelistXML(),
																															rec.getObjNameOfCosts(),
																															rec.getObjTypeOfCosts(),
																															rec.getObjCostsXML(),
																															-1, //rec.getObjContractorID(),
																															rec.getObjDCLS(),
																															rec.getObjExtraInfo(),
																															rec.getObjP30Name(),
																															rec.getObjP30TypeOf(),
																															rec.getObjP30XML(),
																															rec.getObjHCostName(),
																															rec.getObjHCostTypeOf(),
																															rec.getObjHCostXML(),
																															rec.getObjIsVATIncluded(),
																															rec.getObjIsVoluntaryDealIncluded(),
																															rec.getObjIsHigherCostsIncluded(),
																															rec.getObjIsOtherCompensIncluded(),
																															rec.getObjCreatedBy(),
																															rec.getObjNotes(),
																															rec.getObjReturnAddressID(),
																															rec.getObjLogoID(),
																															rec.getObjIsGrotIncluded(),
																															_T(""),//rec.getObjStartDate(),
																															rec.getObjRecalcBy());
																															
	if (m_pDB != NULL)
	{
		if (m_pDB->addObject(newObject))
		{
			// If it's a new object'll need to get the
			// last ObjectID created; 080408 p�d
			int	nObjID = m_pDB->getLastObjectID();			
			if (nObjID > -1)
			{
				// HMS-94 Om nytt objekt fr�n annat objekt s� spara p30 tr�dslagsinfo fr�n det objektet
				// �r den infon tom s� spara fr�n globala inst�llningarna, h�nder om man aldrig �ppnat det gamla objektet man kopierar ifr�n med version>=2.19.0 d� finns det ej n�gon P30 tr�dslagsinfo sparad
				// 
				vecTransactionSpecies vecP30SpeciesObject;
				vecTransactionSpecies vecP30SpeciesGlobal;		
				m_pDB->getP30SpeciesInfo(vecP30SpeciesObject,rec.getObjID_pk());
				m_pDB->getSpecies(vecP30SpeciesGlobal);				
				if(vecP30SpeciesObject.size()>0)
					m_pDB->createP30SpeciesInfo(vecP30SpeciesObject,nObjID);
				else
					m_pDB->createP30SpeciesInfo(vecP30SpeciesGlobal,nObjID);
				vecP30SpeciesGlobal.clear();
				vecP30SpeciesObject.clear();

				// Also save data for Pricelist; 080408 p�d
				// OBS! Only do update of the active object; 080408 p�d
				m_pDB->updObject_prl(nObjID,
														 (rec.getObjNameOfPricelist()),
														 rec.getObjTypeOfPricelist(),
														 (rec.getObjPricelistXML()));
				// Also save data for "Kostnader"; 080408 p�d
				// OBS! Only do update of the active object; 080408 p�d
				m_pDB->updObject_cost(nObjID,
														 (rec.getObjNameOfCosts()),
														 rec.getObjTypeOfCosts(),
														 (rec.getObjCostsXML()));
				
				// Save data for "P30-priser"; 080409 p�d
				m_pDB->updObject_p30(nObjID,
														(rec.getObjP30Name()),
														rec.getObjP30TypeOf(),
														(rec.getObjP30XML()));
				// Save data for "F�rdyrad avverkning"; 080409 p�d
				m_pDB->updObject_hcost(nObjID,
															(rec.getObjHCostName()),
															rec.getObjHCostTypeOf(),
															(rec.getObjHCostXML()));
	
				// Added 2011-11-23, refs #2554, MK
				// Save information on items to be included/excluded on reports.
				m_pDB->updObject_include_exclude(nObjID,rec.getObjIsVATIncluded(), rec.getObjIsVoluntaryDealIncluded(), rec.getObjIsHigherCostsIncluded(), rec.getObjIsOtherCompensIncluded(), rec.getObjIsGrotIncluded());


				setPopulationOfObject();
			}	// if (nObjID > -1)
		}	// if (m_pDB->addObject(newObject))
	}	// if (m_pDB != NULL)
}

BOOL CObjectFormView::saveObject(int show_msg_level)
{
	CXTPReportRecords *pP30Recs = m_wndReport.GetRecords();
	CXTPReportRecords *pHCostRecs = m_wndReport1.GetRecords();
	CTransaction_template recP30;
	CTransaction_template recHCost;
	CTransaction_elv_object rec;
	CString sOldObjMapp;
	CString sNewObjMapp;
	BOOL bExistsOldObjDir = FALSE;
	BOOL bExistsNewObjDir = FALSE;
	int nObjID = -1;
	CString sMsg,S;
	BOOL bSaveObject = TRUE;

	// Try to get the ObjectView, and collect data; 080204 p�d
	// Check if it's a new (NEW_ITEM) or an update (UPD_ITEM); 080204 p�d
	nObjID = getActiveObjectID_pk();
	// Check id of Object. If ID = -1 there's no object in DB; 090128 p�d
	if (nObjID == -1) return TRUE;

	// Check to see if the mandatory data's been added; 080409 p�d
	if (getObjectName().IsEmpty() || 
			getObjectID().IsEmpty())
	{

		if (show_msg_level == 0)
		{
			sMsg = m_sMandatoryDataMissing;
			::MessageBox(this->GetSafeHwnd(),(sMsg),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
		}
		else if (show_msg_level == 1)
		{
			sMsg.Format(_T("%s\n%s\n\n"),m_sMandatoryDataMissing,m_sQuitAnyway);
			if (::MessageBox(this->GetSafeHwnd(),(sMsg),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				// On this, we'll also delete the Object i.e. the invalid Object; 081006 p�d
// COMMENTED OUT 2008-10-16 P�D
// Sorry, but we can't do this, because the Object may already been created!
//				if (m_pDB != NULL)
//					m_pDB->delObject(nObjID);
				// Quit without saving; 081006 p�d		
				return TRUE;
			}
			else
				// Don't quit, give user an oppurtunity to change data; 081006 p�d
				return FALSE;  
		}

		return FALSE; // Default returnvalue on missing data; 081006 p�d
	}

	
	// Collect data from PropertyGrid - Settings on Frame; 080319 p�d
	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		rec = CTransaction_elv_object(nObjID,
																	// General object information ...
																	getErrandNum(),			// General
																	getObjectName(),		// General	
																	getObjectID(),			// General
																	getLineLittra(),		// General	
																	_T(""), //getGrowthArea(),		// General
																	0,	//getTranspDist(),		// General
																	0,			// General
																	0,			// General
//																	getTypeOfInfring(),	// General; commented out 090403 P�D, use setting in Propertygrid
																	// Information in settings ...
																	_T(""), //pFrame->getTypeOfInfrSelected(),	// Settings (PropertyGrid)
																	pFrame->getCurrentWidth1(),		// Settings (PropertyGrid)
																	pFrame->getCurrentWidth2(),		// Settings (PropertyGrid)
																	pFrame->getAddedWidth1(),			// Settings (PropertyGrid)
																	pFrame->getAddedWidth2(),			// Settings (PropertyGrid)
																	pFrame->getParallelWidth(),			// Settings (PropertyGrid)
																	-1, // Not used, use settings set in PropGrid; 091110 p�d pFrame->getTypeOfNet(),				// Settings (PropertyGrid)
																	pFrame->getPriceDiv1(),				// Settings (PropertyGrid)
																	pFrame->getPriceDiv2(),				// Settings (PropertyGrid)
																	pFrame->getPriceBase(),				// Settings (PropertyGrid)
																	pFrame->getMaxPercent(),			// Settings (PropertyGrid)
																	pFrame->getCurrPercent(),			// Settings (PropertyGrid)
																	pFrame->getVAT(),							// Settings (PropertyGrid)
																	pFrame->getPercentOfPriceBase(),	// #4420 20150804 J� 
																	pFrame->getForrestNormIndex(),// Settings (PropertyGrid)
																	pFrame->getForrestNormName(),	// Settings (PropertyGrid)
																	pFrame->getTakeCareOfPerc(),	// Settings (PropertyGrid)
																	pFrame->getCorrFactor(),			// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	0,														// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	0,														// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	// General object information ...
																	getContractorID_pk(),					// General
																	pFrame->getDiameterClass(),		// Settings (PropertyGrid)
																	(pFrame->getExtraInfo()),		// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	0,														// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	0,														// Settings (PropertyGrid)
																	_T(""),												// Settings (PropertyGrid)
																	1,														// Settings (PropertyGrid)
																	1,														// Settings (PropertyGrid)
																	1,														// Settings (PropertyGrid)
																	1,														// Settings (PropertyGrid)
																	getDoneBy(),									// General
																	getNotes(),										// General
																	getReturnContactID(),					// General
																	m_wndPicCtrl.getPictureID(),
																	pFrame->getIsGROT(),
																	getStartDate(),
																	pFrame->getObjReclacBy(),
																	pFrame->getObjLength(),
																	_T(""));			
/*
		// Setup a check of items in record "rec" compared to
		// record "m_recActiveObject"; 100111 p�d
		if (rec.getObjAddedWidth1() == m_recActiveObject.getObjAddedWidth1() &&
				rec.getObjAddedWidth2() == m_recActiveObject.getObjAddedWidth2() &&
				//rec.getObjAltitude	== m_recActiveObject.getObjAltitude &&
				rec.getObjContractorID() == m_recActiveObject.getObjContractorID() &&
				rec.getObjCorrFactor() == m_recActiveObject.getObjCorrFactor() &&
				rec.getObjDCLS() == m_recActiveObject.getObjDCLS() &&
				rec.getObjectName() == m_recActiveObject.getObjectName() &&
				rec.getObjIDNumber() == m_recActiveObject.getObjIDNumber() &&
				rec.getObjErrandNum() == m_recActiveObject.getObjErrandNum() &&
				rec.getObjExtraInfo() == m_recActiveObject.getObjExtraInfo() &&
				//rec.getObjGrowthArea == m_recActiveObject.getObjGrowthArea &&
				//rec.getObjHCostName == m_recActiveObject.getObjHCostName &&
				//rec.getObjHCostTypeOf == m_recActiveObject.getObjHCostTypeOf &&
				rec.getObjID_pk() == m_recActiveObject.getObjID_pk() &&
				//rec.getObjIsHigherCostsIncluded == m_recActiveObject.getObjIsHigherCostsIncluded &&
				//rec.getObjIsOtherCompensIncluded == m_recActiveObject.getObjIsOtherCompensIncluded &&
				//rec.getObjIsVATIncluded == m_recActiveObject.getObjIsVATIncluded &&
				//rec.getObjIsVoluntaryDealIncluded == m_recActiveObject.getObjIsVoluntaryDealIncluded &&
				//rec.getObjLatitude == m_recActiveObject.getObjLatitude &&
				rec.getObjLength() == m_recActiveObject.getObjLength() &&
				rec.getObjLittra() == m_recActiveObject.getObjLittra() &&
				rec.getObjMaxPercent() == m_recActiveObject.getObjMaxPercent() &&
				//rec.getObjNameOfCosts() == m_recActiveObject.getObjNameOfCosts() &&
				//rec.getObjNameOfPricelist() == m_recActiveObject.getObjNameOfPricelist() &&
				rec.getObjNotes() == m_recActiveObject.getObjNotes() &&
				//rec.getObjP30Name == m_recActiveObject.getObjP30Name &&
				//rec.getObjP30TypeOf == m_recActiveObject.getObjP30TypeOf &&
				rec.getObjPercent() == m_recActiveObject.getObjPercent() &&
				rec.getObjPresentWidth1() == m_recActiveObject.getObjPresentWidth1() &&
				rec.getObjPresentWidth2() == m_recActiveObject.getObjPresentWidth2() &&
				rec.getObjPriceBase() == m_recActiveObject.getObjPriceBase() &&
				rec.getObjPriceDiv1() == m_recActiveObject.getObjPriceDiv1() &&
				rec.getObjPriceDiv2() == m_recActiveObject.getObjPriceDiv2() &&
				rec.getObjReturnAddressID() == m_recActiveObject.getObjReturnAddressID() &&
				rec.getObjTakeCareOfPerc() == m_recActiveObject.getObjTakeCareOfPerc() &&
				//rec.getObjTransportDist == m_recActiveObject.getObjTransportDist &&
				//rec.getObjTypeOfCosts == m_recActiveObject.getObjTypeOfCosts &&
				//rec.getObjTypeOfInfring == m_recActiveObject.getObjTypeOfInfring &&
				//rec.getObjTypeOfNet == m_recActiveObject.getObjTypeOfNet &&
				//rec.getObjTypeOfPricelist == m_recActiveObject.getObjTypeOfPricelist &&
				rec.getObjCreatedBy() == m_recActiveObject.getObjCreatedBy() &&
				rec.getObjUseNormID() == m_recActiveObject.getObjUseNormID() &&
				rec.getObjUseNorm() == m_recActiveObject.getObjUseNorm() &&
				rec.getObjStartDate() == m_recActiveObject.getObjStartDate()) 
		{
			bSaveObject = FALSE;
		}
*/
		bSaveObject = TRUE;
		if (bSaveObject)
		{
			if (!m_pDB->addObject(rec))
			{
				m_sOldObjName = m_recActiveObject.getObjectName();
				m_sOldObjID = m_recActiveObject.getObjIDNumber();
				sOldObjMapp.Format(_T("%s_%s"),m_sOldObjName,m_sOldObjID);
				sOldObjMapp = checkDirectoryName(sOldObjMapp);

				// Check if there's a Object directory set for
				// Objectname and ObjectID in m_recActiveObject.
				// If so compare it to the objectdirectory that'll
				// be of Objectname and ObjectID to be saved; 080923 p�d
				bExistsOldObjDir = doIsDirectory(sOldObjMapp);
				if (bExistsOldObjDir)
				{
					// Something's changed. Could be either Name or ID of Object.
					// We need to rename then old directory to the name one
					if (m_sOldObjName.CompareNoCase(getObjectName()) != 0 ||
							m_sOldObjID.CompareNoCase(getObjectID()) != 0)
					{
						// We'll check that the New directory doesn't already
						// exist. I.e. user's set ObjectName or ObjectID equal
						// to one existing; 080923 p�d
						sNewObjMapp.Format(_T("%s_%s"),getObjectName(),getObjectID());
						sNewObjMapp = checkDirectoryName(sNewObjMapp);

						if(doRenameObjectDataDirectory(sOldObjMapp,sNewObjMapp))
						{//Rename succesful, update to the new values.
							m_pDB->updObject(rec);
						}
					}	// if (m_sOldObjName.CompareNoCase(getObjectName()) != 0 ||
					else
					{
						m_pDB->updObject(rec);
					}

				}	// if (bExistsOldObjDir)
				else
				{
					// If the Old Objectmapp didn't exists, we need to create
					// an ObjectMapp, based on data entered by user; 080923 p�d
					sNewObjMapp.Format(_T("%s_%s"),getObjectName(),getObjectID());
					sNewObjMapp = checkDirectoryName(sNewObjMapp);
					bExistsNewObjDir = doIsDirectory(sNewObjMapp);
					if (!bExistsNewObjDir)
					{
						if(doCreateObjectDataDirectory(sNewObjMapp))
						{ //create successful, update to the new values
							m_pDB->updObject(rec);
						}
					}	// if (!bExistsNewObjDir)
					else
					{
						m_pDB->updObject(rec);
					}
				}
			}
			else
			{
				// If it's a new object'll need to get the
				// last ObjectID created; 080408 p�d
				nObjID = m_pDB->getLastObjectID();			
				if (nObjID > -1)
				{
					// Also save data for Pricelist; 080408 p�d
					// OBS! Only do update of the active object; 080408 p�d
					m_pDB->updObject_prl(nObjID,
															 (pFrame->getPricelistSelected()),
															 pFrame->getPricelistTypeOf(),
															 (pFrame->getPricelistXML()));
					// Also save data for "Kostnader"; 080408 p�d
					// OBS! Only do update of the active object; 080408 p�d
					m_pDB->updObject_cost(nObjID,
															 (pFrame->getCostsSelected()),
															 pFrame->getCostsTypeOf(),
															 (pFrame->getCostsXML()));
					
					// Save data for "P30-priser"; 080409 p�d
					pP30Recs = m_wndReport.GetRecords();
					if (pP30Recs != NULL)
					{
						if (pP30Recs->GetCount() > 0)
						{
							CP30ReportRec *pRec = (CP30ReportRec *)pP30Recs->GetAt(0);
							if (pRec != NULL)
							{
								recP30 = pRec->getRecTemplate();
								m_pDB->updObject_p30(nObjID,
																		(recP30.getTemplateName()),
																		recP30.getTypeOf(),
																		(recP30.getTemplateFile()));
							}	// if (pRec != NULL)
						}	// if (pP30Recs->GetCount() > 0)
					}	// if (pP30Recs != NULL)
					// Save data for "F�rdyrad avverkning"; 080409 p�d
					pHCostRecs = m_wndReport1.GetRecords();
					if (pHCostRecs != NULL)
					{
						if (pHCostRecs->GetCount() > 0)
						{
							CHCostReportRec *pRec = (CHCostReportRec *)pHCostRecs->GetAt(0);
							if (pRec != NULL)
							{
								recHCost = pRec->getRecTemplate();
								m_pDB->updObject_hcost(nObjID,
																		(recHCost.getTemplateName()),
																		recHCost.getTypeOf(),
																		(recHCost.getTemplateFile()));
							}	// if (pRec != NULL)
						}	// if (pHCostRecs->GetCount() > 0)
					}	// if (pHCostRecs != NULL)
				}
			}
		}	// if (bSaveObject)

		//setPopulationOfObject();	// Added a new item; 080401 p�d
		// I think we also should save "V�rdering" and "Taxering"; 080611 p�d
		CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pFormView != NULL)
		{
			CPropertiesFormView *pProp = pFormView->getPropertiesFormView();
			if (pProp != NULL)
			{
				// pProp->getEvaluatedView()->saveEvalData(); Tagit bort, g�r �nd� ingenting i funktionen 20200428 J�
				pProp->getCruisingView()->saveCruiseData();
				pProp->getCruisingView()->saveCruisingWSide();
				pProp->getCruisingView()->saveCruisingWidth();
				pProp->savePropertyGroupings(false);	// Added 2009-03-20 p�d
				pProp->savePropertyTypeOfAction();	// Added 2009-09-14 p�d
				//Removed this function 20100305 J�, not needed to set the sort order when you save or recalculate the object, only when the
				//user changes the sort order himself.
				//pProp->setPropertySortOrder();			// Added 2009-10-12 p�d
				pProp = NULL;
			}
			// Need to release ref. pointers; 080520 p�d
			pFormView = NULL;
		}

		return TRUE;
	}	// if (pFrame != NULL)													

	return FALSE;
}


void CObjectFormView::setUpdateInfoDlgBarInNormFrame(void)
{
	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		pFrame->setPopulateSettings();
	}
}

void CObjectFormView::removeObject(void)
{
	if (::MessageBox(this->GetSafeHwnd(),(m_sRemoveObject),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (m_pDB != NULL)
		{
			m_pDB->delObject(getActiveObjectID_pk());
		}	// if (m_pDB != NULL)
		setPopulationOfObject();
	}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sRemoveObject),_T(m_sMsgCap),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
}

// This metod'll load ObjectIndex, set m_nDBIndex to last
// entry and populateData(m_nDBIndex); 080401 p�d
void CObjectFormView::setPopulationOfObject(void)
{
		// Endable/Disable toolbar buttons; 080122 p�d
		CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
		// If object's been deleted, reload ObjectIndex and
		// repopulate; 080401 p�d
		getObjectIndexFromDB();
		if (m_vecObjectIndex.size() > 0)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

			//changed #3385
			//if (!m_bIsLastVisitedObjectID_Valid)
			m_nDBIndex = (UINT)m_vecObjectIndex.size() - 1;
			//else
			if (m_bIsLastVisitedObjectID_Valid)
			{
				// Try to find the m_bLastVisitedObjectID in m_vecObjectIndex and
				// set m_nDBIndex to point to this Object; 090922 p�d
				for (UINT i = 0;i < m_vecObjectIndex.size();i++)
				{
					if (m_vecObjectIndex[i] == m_nLastVisitedObjectID)
					{
						m_nDBIndex = i;
					}
				}
		
			}
			if (pFrame != NULL)
			{
				pFrame->setContractorTBtn(TRUE);
				pFrame->setCalculateTBtn(TRUE);
				pFrame->setUpdateTBtn(TRUE);
				pFrame->setPrintoutsCBox(TRUE);
				pFrame->setGISButton(TRUE);
				pFrame->setImportPropertiesTBtn(FALSE);
				pFrame->setRemoveTBtn(FALSE);
				pFrame->setToolTBtn(TRUE);
				pFrame->setExportTBtn(TRUE);
				pFrame->setExtDocTBtn(TRUE);
			}
		}
		else
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

			m_nDBIndex = -1;	// No Objects
			m_recActiveObject = CTransaction_elv_object();	// Set active object, NOT ACTIVE; 090205 p�d
			setNavigationButtons(FALSE,FALSE);
			if (pFrame != NULL)
			{
				pFrame->setContractorTBtn(FALSE);
				pFrame->setCalculateTBtn(FALSE);
				pFrame->setUpdateTBtn(FALSE);
				pFrame->setPrintoutsCBox(FALSE);
				pFrame->setGISButton(FALSE);
				pFrame->setImportPropertiesTBtn(FALSE);
				pFrame->setRemoveTBtn(FALSE);
				pFrame->setToolTBtn(TRUE);
				pFrame->setExportTBtn(FALSE);
				pFrame->setExtDocTBtn(FALSE);

				m_wndEdit1_29.ShowWindow(SW_HIDE);
				m_wndEdit1_30.ShowWindow(SW_HIDE);
				m_wndLbl1_24.ShowWindow(SW_HIDE);
				m_wndLbl1_25.ShowWindow(SW_HIDE);

			}
		}
		//populateData();
		doPopulateData(m_nDBIndex);
		pFrame->setPopulateSettings();
}


// Added 2009-03-31 P�D
// Create a new Object based on information sent from GIS-module; 090331 p�d
void CObjectFormView::createObjectFromGIS(int *properties)
{
	CString S;
	vecTransactionProperty vecProps;
	vecTransactionProperty vecSelectedProperties;
	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;

	CTransaction_property recProp;
	int *arrayProp = properties;

	// Get properties from "fst_property_table"; 090924 p�d
	if (m_pDB != NULL)
		m_pDB->getProperties(vecProps);
	
	// Check that there's more than one item in list.
	// I.e. arrayProp[0] > 1; 090331 p�d
	// Or there's not Properties in database table; 090331 p�d
	if (vecProps.size() == 0) return;

	// Start by creting a new object; 090924 p�d
	if (newObject())
	{

		for (int i = 1;i < arrayProp[0]+1;i++)
		{
			for (UINT i1 = 0;i1 < vecProps.size();i1++)
			{
				recProp = vecProps[i1];
				if (recProp.getID() == arrayProp[i])
				{
					vecSelectedProperties.push_back(recProp);
				}	// if (recProp.getID() == arrayProp[i])
			}	// for (UINT i1 = 0;i1 < vecProps.size();i1++)
		}	// for (int i = 0;i < arrayProp[0];i++)

		// Set to point at last created Object; 090924 p�d
		m_bIsLastVisitedObjectID_Valid = FALSE;
		setPopulationOfObject();

		// Add properties, from GIS to Object; 090924 p�d
		if (vecSelectedProperties.size() > 0)
		{
			if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
			{
				if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
				{
					pPropView->setSelectedProperties(vecSelectedProperties);
				}	// if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
			}	// if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
		}	// if (vecSelectedProperties.size() > 0)
		// Need to release ref. pointers; 080520 p�d
		pView = NULL;
		pPropView = NULL;

		arrayProp = NULL;
		vecProps.clear();
		vecSelectedProperties.clear();
		// We'll finish off by goin' back to GIS and also return ObjectID; 090924 p�d
		// We'll finish off just by goin' returnig ObjectID; 090925 p�d
		CTransaction_elv_object *pObj = getActiveObject();
		if (pObj != NULL)
		{
			// Bring up GIS; 090924 p�d
			//CString sLangFile(getLanguageFN(getLanguageDir(), _T("UMGIS"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV));
			//showFormView(888, sLangFile);
			// Return ionformation to GIS-module; 090924 p�d
			CView *pViewGIS = getFormViewByID(888);	// 888 = Identifer for GIS-module; 090924 p�d
			if( pViewGIS )
			{
				pViewGIS->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x03, (LPARAM)pObj->getObjID_pk());
			}
		}
	}


}

// CObjectFormView diagnostics

#ifdef _DEBUG
void CObjectFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CObjectFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

/*	Commented out 2009-05-11 P�D, NOT USED, set in PropertyGrid; 090511 p�d
// Add a P30 template to Object, from elv_template_table; 080404 p�d
void CObjectFormView::OnBnClickedButton1()
{
	CString S;
	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_p30_table vecP30;
	CTransaction_elv_object recObject;

	CTransaction_template recTemplate;
	vecTransactionTemplate vecTemplate; // Templates in 'elv_template_table'; 080404 p�d
	int nObjID = getActiveObject()->getObjID_pk();

	if (m_pDB != NULL)
	{
		m_pDB->getObjectTemplates(vecTemplate,TEMPLATE_P30);
		m_pDB->getObject(nObjID,recObject);

		CSelectTemplateDlg *pDlg = new CSelectTemplateDlg();
		if (pDlg != NULL)
		{
			pDlg->setActiveTmplName(recObject.getObjP30Name());
			pDlg->setTemplates(vecTemplate);
			if (pDlg->DoModal() == IDOK)
			{
				recTemplate = pDlg->getSelectedTemplate();
				m_wndReport.ResetContent();
				if (pars.LoadFromBuffer(recTemplate.getTemplateFile()))
				{
					vecP30.clear();
					pars.getObjTmplP30(vecP30);
					if (vecP30.size() > 0)
					{
						for (UINT i = 0;i < vecP30.size();i++)
						{
							CObjectTemplate_p30_table rec = vecP30[i];
							m_wndReport.AddRecord(new CP30ReportRec(recTemplate,
																											CTransaction_elv_p30(rec.getSpcID(),
																																					 nObjID,
																																					 rec.getSpcID(),
																																					 rec.getSpcName(),
																																					 rec.getPrice(),
																																					 rec.getPriceRel())));
						}	// for (UINT i = 0;i < vecP30.size();i++)
						m_wndReport.Populate();
//						m_wndReport.UpdateWindow();
						// Added 081006 p�d
						m_wndReport.RedrawControl();
						// Also update "elv_object_table"; 080409 p�d
						if (nObjID > -1)
							m_pDB->updObject_p30(nObjID,recTemplate.getTemplateName(),recTemplate.getTypeOf(),recTemplate.getTemplateFile());

					}	// if (vecP30.size() > 0)
				}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
			}
			delete pDlg;
		}
		populateData();
	}
}

// Add a Higher Costs template to Object, from elv_template_table; 080404 p�d
void CObjectFormView::OnBnClickedButton2()
{
	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_hcost_table vecHCost;
	CTransaction_elv_object recObject;
	CHCostReportRec *pRec = NULL;
	int nRet = -1;

	CTransaction_template recTemplate;
	vecTransactionTemplate vecTemplate; // Templates in 'elv_template_table'; 080404 p�d
	int nObjID = getActiveObject()->getObjID_pk();
	if (m_pDB != NULL)
	{
		m_pDB->getObjectTemplates(vecTemplate,TEMPLATE_HCOST);
		m_pDB->getObject(nObjID,recObject);

		CSelectTemplateDlg *pDlg = new CSelectTemplateDlg();
		if (pDlg != NULL)
		{
			pDlg->setActiveTmplName(recObject.getObjHCostName());
			pDlg->setTemplates(vecTemplate);
			if ((nRet = pDlg->DoModal()) == IDOK)
			{
				recTemplate = pDlg->getSelectedTemplate();

				if (pars.LoadFromBuffer(recTemplate.getTemplateFile() ))
				{
					vecHCost.clear();
					pars.getObjTmplHCost(vecHCost);
					if (vecHCost.size() > 0)
					{
						m_wndReport1.ResetContent();
						for (UINT i = 0;i < vecHCost.size();i++)
						{
							CObjectTemplate_hcost_table rec = vecHCost[i];
							m_wndReport1.AddRecord((pRec = new CHCostReportRec(recTemplate,
																																 CTransaction_elv_hcost(i+1,
																																							 nObjID,
																																							 rec.getM3Sk(),
																																							 rec.getPriceM3Sk(),
																																							 rec.getSumPrice()))));
							if (pRec != NULL)	pRec->setColumnBold(1,COLUMN_2);
						}	// for (UINT i = 0;i < vecP30.size();i++)
						m_wndReport1.Populate();
//						m_wndReport1.UpdateWindow();
						// Added 081006 p�d
						m_wndReport1.RedrawControl();
						// Also update "elv_object_table"; 080409 p�d
						if (nObjID > -1)
							m_pDB->updObject_hcost(nObjID,recTemplate.getTemplateName(),recTemplate.getTypeOf(),recTemplate.getTemplateFile());
					}	// if (vecHCost.size() > 0)
				}	// if (pars.LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))
			}
			delete pDlg;
		}
		if (nRet == IDOK)
		{
			// We'll do a recalulation of "F�rdyrad avverkning", for all properties in Objekt; 080520 p�d
			CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
			if (pFormView != NULL)
			{
				CPropertiesFormView *pProp = pFormView->getPropertiesFormView();
				if (pProp != NULL)
				{
					pProp->recalulateAllHigherCosts();
					// Need to release ref. pointers; 080520 p�d
					pProp = NULL;
				}
				// Need to release ref. pointers; 080520 p�d
				pFormView = NULL;
			}
		}	// if (nRet == IDOK)
		populateData();
	}
	// Need to remove ref. pointer; 080520 p�d
	pRec = NULL;
}
*/
// Add Contractor data; 100225 p�d
void CObjectFormView::OnBnClickedBtnAddContractor()
{
	CSelectContractorDlg *selectDlg = new CSelectContractorDlg();
	if (selectDlg != NULL)
	{
		selectDlg->setDBConnection(m_pDB);
		selectDlg->setDialogTitle(m_sSearchDlgTitle1);
		if (selectDlg->DoModal() == IDOK)
		{
			// Check if there's a contractor selected; 080327 p�d
			if (selectDlg->getIsSelected())
			{
				setContractorID_pk(selectDlg->getID_pk());
				setPersOrgNum(selectDlg->getPersOrgNum());
				setContactPers(selectDlg->getContactPers());
				setCompany(selectDlg->getCompany());
				setPhoneWork(selectDlg->getPhoneWork());
				setPhoneHome(selectDlg->getPhoneHome());
				setMobile(selectDlg->getMobile());
				if (m_pDB) m_pDB->updObject_contractor_id(m_recActiveObject.getObjID_pk(),selectDlg->getID_pk());
				populateData();
			}	// if (selectDlg->getIsContractorSelected())
		}	// if (selectDlg->DoModal() == IDOK)
		delete selectDlg;
	} // if (selectDlg != NULL)
}

// Show Contractor data; 100225 p�d
void CObjectFormView::OnBnClickedBtnShowContractor()
{
	CShowInformationDlg3 *pDlg = new CShowInformationDlg3();
	if (pDlg)
	{
		pDlg->setDBConnection(m_pDB);
		pDlg->DoModal();

		delete pDlg;
	}
}

// Add return address, from Contact-register; 080603 p�d
void CObjectFormView::OnBnClickedButton3()
{
	CSelectContractorDlg *selectDlg = new CSelectContractorDlg();
	if (selectDlg != NULL)
	{
		selectDlg->setDBConnection(m_pDB);
		selectDlg->setDialogTitle(m_sSearchDlgTitle2);
		if (selectDlg->DoModal() == IDOK)
		{
			// Check if there's a contact selected; 080327 p�d
			if (selectDlg->getIsSelected())
			{
				setReturnName(selectDlg->getContactPers());
				setReturnContactID(selectDlg->getID_pk());
				setReturnAddress(selectDlg->getAddress());
				setReturnPostNumber(selectDlg->getPostNum());
				setReturnPostAddress(selectDlg->getPostAddress());
				if (m_pDB) m_pDB->updObject_retadr_id(m_recActiveObject.getObjID_pk(),selectDlg->getID_pk());
				
			}	// if (selectDlg->getIsContractorSelected())
		}	// if (selectDlg->DoModal() == IDOK)

		delete selectDlg;
	}
}

void CObjectFormView::writeLastObjectIDToRegistry(void)
{
	TCHAR szDBName[127];
	// We'll add last object id to Registry, to make it possible
	// to go to last object on start of Module; 090122 p�d
	regSetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_LAST_OBJECT,m_recActiveObject.getObjID_pk());
	GetUserDBInRegistry(szDBName);
	regSetStr(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_OBJ_IN_DB,szDBName);
}

BOOL CObjectFormView::readLastObjectIDFromRegistry(void)
{
	CString sDBNameLastUsed,S;
	TCHAR szDBName[127];

	// Start by askin' if the user likes to open the last visited object; 090922 p�d
	// Commented out 2009-09-23 p�d. This will always make this method return TRUE if everything's ok.
	// I.e. we'll goto last opened Object; 090923 p�d
	//if (::MessageBox(this->GetSafeHwnd(),m_sGoToLastObjMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDNO) return FALSE;


	// Try to read LastObjectID from registry and point to this on
	// start of Module; 090122 p�d
	m_nLastVisitedObjectID = regGetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_LAST_OBJECT,-1);
	// Get workin' userdatabase; 090922 p�d
	GetUserDBInRegistry(szDBName);
	// Get userdatabase last used; 090922 p�d
	sDBNameLastUsed = regGetStr(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_OBJ_IN_DB,_T(""));

	// First we'll check if we're still in the same userdatabase, if not return FALSE; 090922 p�d
	if (sDBNameLastUsed.CompareNoCase(szDBName) != 0) return FALSE;

	// If we're in the same userdatabase, we'll ceck that the ObjectID is still walid
	// within the userdatabase; 090922 p�d
	if (m_pDB) 
	{
		if (m_pDB->isObjectInDB_status(m_nLastVisitedObjectID,m_nShowObjects)) //changed #3385 tar �ven h�nsyn till objektets status
			return TRUE;
	}

	return FALSE;
}

void CObjectFormView::OnBnClickedBtnAddLogo()
{
	CString sPicPath;
	CSelectContractorDlg *selectDlg = new CSelectContractorDlg();
	if (selectDlg != NULL)
	{
		selectDlg->setDBConnection(m_pDB);
		selectDlg->setDialogTitle(m_sSearchDlgTitle3);
		if (selectDlg->DoModal() == IDOK)
		{
			// Check if there's a contractor selected; 080327 p�d
			if (selectDlg->getIsSelected())
			{		
				// Try to load image from DB and save to disk; 090825 p�d
				TCHAR lpTempPathBuffer[1025];
				DWORD dwRetVal = GetTempPath(1024, lpTempPathBuffer);
				CString csBuf = lpTempPathBuffer;

				sPicPath.Format(_T("%s%s"), csBuf, _T("tmp.jpg"));
				if (m_pDB)
				{
					if (m_pDB->loadImage(sPicPath, selectDlg->getID_pk()))
					{
						if (fileExists(sPicPath)) 
						{
							m_wndPicCtrl.Load(sPicPath);
							m_wndPicCtrl.setPictureID(selectDlg->getID_pk());
							m_pDB->updObject_logo_id(m_recActiveObject.getObjID_pk(),selectDlg->getID_pk());
						}
					}
					else
					{
						m_wndPicCtrl.FreeData();
						m_wndPicCtrl.setPictureID(-1);
						m_pDB->updObject_logo_id(m_recActiveObject.getObjID_pk(),-1);
					}
				}
				Invalidate();
				UpdateWindow();
				m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		
				removeFile(sPicPath);
			}	// if (selectDlg->getIsContractorSelected())
		}	// if (selectDlg->DoModal() == IDOK)
		delete selectDlg;
	} // if (selectDlg != NULL)
}

void CObjectFormView::OnBnClickedBtnDelLogo()
{
	if (::MessageBox(this->GetSafeHwnd(),m_sRemoveLogoMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO) return;

	m_wndPicCtrl.FreeData();

	if(m_pDB != NULL)
	{
		m_wndPicCtrl.setPictureID(-1);
		m_pDB->updObject_logo_id(m_recActiveObject.getObjID_pk(),-1);
	}
	Invalidate();
	UpdateWindow();
	m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);
}

//#HMS-94 �ppna objektets inst�llningar f�r P30 tr�dslag kontra alla tr�dslag 20220921 J�
void CObjectFormView::OnBnClickedBtnP30Settings()
{
	showFormView(IDD_FORMVIEW_P30SPEC_SET,m_sLangFN);
}

void CObjectFormView::OnBnClickedBtnDelContractor()
{
	if (::MessageBox(this->GetSafeHwnd(),m_sRemoveContractorMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO) return;
	if(m_pDB != NULL)
	{
		m_pDB->updObject_contractor_id(m_recActiveObject.getObjID_pk(),-1);
		m_wndEdit1_6.SetWindowTextW(L"");
		m_wndEdit1_7.SetWindowTextW(L"");
		m_wndEdit1_8.SetWindowTextW(L"");
		m_wndEdit1_25.SetWindowTextW(L"");
		m_wndEdit1_26.SetWindowTextW(L"");
		m_wndEdit1_27.SetWindowTextW(L"");
		populateData();
	}
}

void CObjectFormView::OnBnClickedBtnDelRetadr()
{
	if (::MessageBox(this->GetSafeHwnd(),m_sRemoveRetAddressMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO) return;
	if(m_pDB != NULL)
	{
		m_pDB->updObject_retadr_id(m_recActiveObject.getObjID_pk(),-1);
		m_wndEdit1_16.SetWindowTextW(L"");
		m_wndEdit1_17.SetWindowTextW(L"");
		m_wndEdit1_18.SetWindowTextW(L"");
		m_wndEdit1_19.SetWindowTextW(L"");
		setReturnContactID(-1);
	}
}

//Lagt till funktion f�r att �pnna ett objekt och visa en fastighet id et objektet Feature #2590,#2529 20111125 J�
void CObjectFormView::doPopulateObjectAndProperty(int obj_id,int prop_id)
{
	CXTPReportRow *pRow = NULL;
	BOOL bFound = FALSE;
	// Find ObjectID match; 090122 p�d
	if (m_vecObjectIndex.size() > 0)
	{
		for (UINT i = 0;i < m_vecObjectIndex.size();i++)
		{
			if (m_vecObjectIndex[i] == obj_id)
			{
				m_nDBIndex = i;
				bFound = TRUE;
				break;
			}
		}
	}
	if (bFound)
	{	
		populateData();
		//Visa fastighetstab och s�tt fokus p� r�tt fastighet
		CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pFormView) 
		{
			pFormView->getTabCtrl().SetCurSel(TAB_PROPERTIES);
			CMyReportCtrl *wndReportProperties = pFormView->getPropertiesFormView()->getReportProperties();
			CXTPReportRows *pRows = wndReportProperties->GetRows();
			if (pRows != NULL)
			{
				for (int i = 0;i < pRows->GetCount();i++)
				{
					pRow = pRows->GetAt(i);
					if (pRow != NULL)
					{
						CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
						if (pRec != NULL)
						{
							if(prop_id==pRec->getPropId())
							{
								pRow->SetSelected(TRUE);
								wndReportProperties->SetFocusedRow(pRow);
								pFormView->getPropertiesFormView()->setActivePropertyInReport();
								pFormView->getPropertiesFormView()->m_wndTabControl.SetCurSel(TAB_CRUISING);
							}
						}
					}
				}
			}

		}

	}
}


//#3385 setObjectStatusLbl(int status)
//S�tt text och f�rg f�r objektets status
void CObjectFormView::setObjectStatusLbl(int status)
{
	if(status == 1)
	{
		m_wndEdit2_1.SetBkColor(GREEN);
		m_wndEdit2_1.SetWindowTextW(m_sStatusLblFinished);
	}
	else if(status == 0)
	{
		m_wndEdit2_1.SetBkColor(WHITE);
		m_wndEdit2_1.SetWindowTextW(m_sStatusLblOngoing);
	}
	else
	{
		m_wndEdit2_1.SetBkColor(INFOBK);
		m_wndEdit2_1.SetWindowText(_T(""));
	}
}

//#3385 setObjectStatus(int status)
//uppdaterara status p� objektet i db, �ndrar text och populerar om Index-vektorn f�r vilka objekt som ska visas
void CObjectFormView::setObjectStatus(int status)
{
	if(m_pDB != NULL)
	{
		if(status <0 || status > 1)
			status = 0;
		m_pDB->updObject_status(m_recActiveObject.getObjID_pk(),status);

		//uppdatera text
		setObjectStatusLbl(status);
		m_nLastVisitedObjectID = m_recActiveObject.getObjID_pk();
		m_bIsLastVisitedObjectID_Valid = TRUE;
		setPopulationOfObject();
	}
}

//#3385 void setShowObjectStatus(int status)
void CObjectFormView::setShowObjectStatus(int status) 
{
	m_nShowObjects = status;
	m_nLastVisitedObjectID = m_recActiveObject.getObjID_pk();
	m_bIsLastVisitedObjectID_Valid = TRUE;
	setPopulationOfObject();
}	