// ChangePropStatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ChangePropStatusDlg.h"

#include "ResLangFileReader.h"

// CChangePropStatusDlg dialog

IMPLEMENT_DYNAMIC(CChangePropStatusDlg, CDialog)

BEGIN_MESSAGE_MAP(CChangePropStatusDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON16_1, &CChangePropStatusDlg::OnBnClickedAddToListCtrl)
	ON_BN_CLICKED(IDC_BUTTON16_2, &CChangePropStatusDlg::OnBnClickedAddAllToListCtrl)
	ON_BN_CLICKED(IDC_BUTTON16_3, &CChangePropStatusDlg::OnBnClickedRemoveFromListCtrl)
	ON_BN_CLICKED(IDC_BUTTON16_4, &CChangePropStatusDlg::OnBnClickedRemoveAllFromListCtrl)
	ON_BN_CLICKED(IDOK, &CChangePropStatusDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CChangePropStatusDlg::CChangePropStatusDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChangePropStatusDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CChangePropStatusDlg::~CChangePropStatusDlg()
{
	m_sarrStatus.RemoveAll();
	m_vecTransaction_elv_properties.clear();
	m_vecTransaction_elv_properties_return.clear();
}

void CChangePropStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_LIST16_1, m_wndLCtrl);
	DDX_Control(pDX, IDC_EDIT16_1, m_wndEdit1);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CChangePropStatusDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	getPropActionStatusFromDB();

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING5800)));

			m_wndLbl1.SetWindowText(xml.str(IDS_STRING5801));
			m_wndLbl2.SetWindowText(xml.str(IDS_STRING5807));
			m_wndLbl3.SetWindowText(xml.str(IDS_STRING5808));
			m_wndOKBtn.SetWindowText((xml.str(IDS_STRING5805)));
			m_wndCancelBtn.SetWindowText((xml.str(IDS_STRING5806)));

			if (m_vecPropStatus.size() > 0)
			{
				for (UINT i = 0;i < m_vecPropStatus.size();i++)
					m_sarrStatus.Add(m_vecPropStatus[i].getPropStatusName());
			}
			xml.clean();
		}
	}

	setupReport();
	getPropertiesInObject();
	populateData();

	return TRUE;
}

void CChangePropStatusDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_CHANGE_STATUS, FALSE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);
		m_wndReport.ShowGroupBy( TRUE );

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING5802)), 150));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING5803)), 100));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->SetVisible(FALSE);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING5804)), 150));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING2314)), 80));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING2311)), 100));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING2312)), 100));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.GetRecords()->SetCaseSensitive(FALSE);

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_CHANGE_STATUS),2,80,430,rect.bottom - 120);

				if (m_wndLCtrl.GetSafeHwnd())
				{
					// Need to set size of Report control; 071109 p�d
					CWnd *pWnd = GetDlgItem(IDC_LIST16_1);
					if (pWnd != NULL)
					{
						int tpp = TwipsPerPixel();
						pWnd->SetWindowPos(&CWnd::wndTop,480*15/tpp,80*15/tpp,480*15/tpp,(rect.bottom-120)*15/tpp,SWP_SHOWWINDOW);
					}
					m_wndLCtrl.InsertColumn(0,(xml.str(IDS_STRING2315)),LVCFMT_LEFT,150);		// "Fastighet inkuderad i Setup-fil"; 090424 p�d
					m_wndLCtrl.InsertColumn(1,(xml.str(IDS_STRING2314)),LVCFMT_LEFT,80);
					m_wndLCtrl.InsertColumn(2,(xml.str(IDS_STRING2311)),LVCFMT_LEFT,100);
					m_wndLCtrl.InsertColumn(3,(xml.str(IDS_STRING2312)),LVCFMT_LEFT,100);
				}
				xml.clean();
			}
		}	// if (fileExists(sLangFN))
	}
}

void CChangePropStatusDlg::getPropertiesInObject(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_nObjID,m_vecTransaction_elv_properties);
	}	// if (m_pDB != NULL)
}

void CChangePropStatusDlg::getPropActionStatusFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getPropertyActionStatus(m_vecPropStatus);
	}	// if (m_pDB != NULL)
}

void CChangePropStatusDlg::populateData(void)
{
	int nStatusIndex = -1;
	CString sStatus;
	
	// Setup Status-items in ComboBox; 080613 p�d
	m_wndCBox1.ResetContent();
	for (int i = 0;i < m_sarrStatus.GetCount();i++)
			m_wndCBox1.AddString(m_sarrStatus.GetAt(i));
	m_wndCBox1.SetCurSel(0);	// Set first item selected, as default; 080613 p�d

	// set properties for Object; 080613 p�d
	if (m_vecTransaction_elv_properties.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		{
			m_recElvProps = m_vecTransaction_elv_properties[i];

			nStatusIndex = m_recElvProps.getPropStatus();
			sStatus.Empty();
			if( nStatusIndex >= 0 )
			{
				for( UINT j = 0; j < m_vecPropStatus.size(); j++ )
				{
					if( m_vecPropStatus[j].getPropStatusID_pk() == nStatusIndex )
					{
						sStatus = m_sarrStatus.GetAt(j);
					}
				}
			}

			m_wndReport.AddRecord(new CChangeStatusRec(i,m_recElvProps.getPropName(),m_recElvProps.getPropGroupID(),sStatus,m_recElvProps,m_recElvProps.getPropNumber(),m_recElvProps.getPropCounty(),m_recElvProps.getPropMunicipal()));
			
		}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
	}	// m_vecTransaction_elv_properties
}

void CChangePropStatusDlg::OnBnClickedAddToListCtrl()
{
	LVFINDINFO info;
	int nNumOfItems = -1,nNumRows=0,i=0;
	CXTPReportSelectedRows *pRows = m_wndReport.GetSelectedRows();
	CChangeStatusRec *pRec = NULL;
	if (pRows != NULL)
	{
		nNumRows=pRows->GetCount();
		i=0;
		if(nNumRows>0)
		{
			do
			{
				pRec = (CChangeStatusRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					nNumOfItems =	m_wndLCtrl.GetItemCount();
					// Check to see if property already inserted; 090424 p�d
					info.flags = LVFI_PARTIAL|LVFI_STRING;
					info.psz = pRec->getColumnText(COLUMN_0);;
					if (m_wndLCtrl.FindItem(&info) == -1)
					{
						InsertRow(m_wndLCtrl,nNumOfItems,4,pRec->getRecord().getPropName(),pRec->getRecord().getPropNumber(),pRec->getRecord().getPropCounty(),pRec->getRecord().getPropMunicipal());
						//m_wndLCtrl.SetItemData(nItem, recProps.getPropID_pk())

							nNumOfItems =	m_wndLCtrl.GetItemCount();						
						//Ta bort fastighet fr�n lista feature #2389 20111004 J�
						m_wndReport.RemoveRecordEx(pRec);
						//�ndrat s� att raderna populeras om n�r man tagit bort en Bug #2582 20111123 J�
						pRows = m_wndReport.GetSelectedRows();
						nNumRows=pRows->GetCount();
						i=-1;
					}
				}	// if (pRec != NULL)
				i++;
			}while(i < nNumRows);
		}

	}
}

void CChangePropStatusDlg::OnBnClickedAddAllToListCtrl()
{
	int nNumOfItems = -1;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CChangeStatusRec *pRec = NULL;
	if (pRows != NULL)
	{
		//Lagt till en koll om det finns n�gra att �verf�ra
		//Bug #2564 20111117 J�
		if(pRows->GetCount()>0)
		{
			// Clear List before adding ALL items in list; 090424 p�d	
			//m_wndLCtrl.DeleteAllItems();

			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CChangeStatusRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					nNumOfItems =	m_wndLCtrl.GetItemCount();
					//InsertRow(m_wndLCtrl,nNumOfItems,1,pRec->getColumnText(COLUMN_0),pRec->getRecord().getPropID_pk());	
					InsertRow(m_wndLCtrl,nNumOfItems,4,pRec->getRecord().getPropName(),pRec->getRecord().getPropNumber(),pRec->getRecord().getPropCounty(),pRec->getRecord().getPropMunicipal());
					nNumOfItems =	m_wndLCtrl.GetItemCount();

				}
			}
			//Ta bort fastigheter fr�n lista feature #2389 20111004 J�
			m_wndReport.ClearReport();
			m_wndReport.Populate();
			m_wndReport.RedrawControl();
		}
	}
}

void CChangePropStatusDlg::OnBnClickedRemoveFromListCtrl()
{
	int nNumOfItems =	-1;
	int nSelectedItem = GetSelectedItem(m_wndLCtrl);
	int nStatusIndex = -1;
	CString sStatus;
	CString sText;

	//L�gg till fastighet till lista feature #2389 20111004 J�
	do	
	{
		sText = m_wndLCtrl.GetItemText(nSelectedItem,0);
		for (UINT i1 = 0;i1 < m_vecTransaction_elv_properties.size();i1++)
		{
			if (m_vecTransaction_elv_properties[i1].getPropName().CompareNoCase(sText) == 0)
			{
				m_recElvProps = m_vecTransaction_elv_properties[i1];
				nStatusIndex = m_recElvProps.getPropStatus();
				sStatus.Empty();
				if (nStatusIndex > -1 && nStatusIndex < m_sarrStatus.GetCount())
					sStatus = m_sarrStatus.GetAt(nStatusIndex);
				m_wndReport.AddRecord(new CChangeStatusRec(m_wndReport.GetSelectedRows()->GetCount()+1,m_vecTransaction_elv_properties[i1].getPropName(),m_vecTransaction_elv_properties[i1].getPropGroupID(),sStatus,m_recElvProps,m_vecTransaction_elv_properties[i1].getPropNumber(),m_vecTransaction_elv_properties[i1].getPropCounty(),m_vecTransaction_elv_properties[i1].getPropMunicipal()));
			}
		}
		m_wndLCtrl.DeleteItem(nSelectedItem);	
		nNumOfItems =	m_wndLCtrl.GetItemCount();
		m_wndReport.Populate();
		m_wndReport.RedrawControl();
		nSelectedItem = GetSelectedItem(m_wndLCtrl);
	}while(nSelectedItem > CB_ERR);
}

void CChangePropStatusDlg::OnBnClickedRemoveAllFromListCtrl()
{
		int nStatusIndex = -1;
	CString sStatus;

	m_wndLCtrl.DeleteAllItems();

	m_wndReport.ClearReport();
	//L�gg till alla fastigheter till lista feature #2389 20111004 J�
	if (m_vecTransaction_elv_properties.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		{
			m_recElvProps = m_vecTransaction_elv_properties[i];

			nStatusIndex = m_recElvProps.getPropStatus();
			sStatus.Empty();
			if (nStatusIndex > -1 && nStatusIndex < m_sarrStatus.GetCount())
				sStatus = m_sarrStatus.GetAt(nStatusIndex);

			m_wndReport.AddRecord(new CChangeStatusRec(i,m_recElvProps.getPropName(),m_recElvProps.getPropGroupID(),sStatus,m_recElvProps,m_recElvProps.getPropNumber(),m_recElvProps.getPropCounty(),m_recElvProps.getPropMunicipal()));
			
		}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
	}	// m_vecTransaction_elv_properties

}

void CChangePropStatusDlg::OnBnClickedOk()
{
	CString sText;
	int nSelStatusIdx = m_wndCBox1.GetCurSel();
	if (m_wndLCtrl.GetItemCount() > 0 && m_vecTransaction_elv_properties.size() > 0 && nSelStatusIdx >= 0 && nSelStatusIdx < m_wndCBox1.GetCount())
	{
		m_sStatusSet = m_sarrStatus[nSelStatusIdx];
		m_nStatusIndex = m_vecPropStatus[nSelStatusIdx].getPropStatusID_pk();
		m_sBatchStr = m_wndEdit1.getText();
		for (int i = 0;i < m_wndLCtrl.GetItemCount();i++)
		{
			sText = m_wndLCtrl.GetItemText(i,0);
			// Try to find  properties selected in ListCtrl and add
			// "m_vecTransaction_elv_properties_return"; 091105 p�d
			for (UINT i1 = 0;i1 < m_vecTransaction_elv_properties.size();i1++)
			{
				if (m_vecTransaction_elv_properties[i1].getPropName().CompareNoCase(sText) == 0)
				{
					m_vecTransaction_elv_properties_return.push_back(m_vecTransaction_elv_properties[i1]);
				}	// if (m_vecTransaction_elv_properties[i1].getPropName().CompareNoCase(sText) == 0)
			}	// for (UINT i1 = 0;i1 < m_vecTransaction_elv_properties.size();i1++)
		}	// for (int i = 0;i < m_wndLCtrl.GetItemCount();i++)
		OnOK();
	}	// if (m_wndLCtrl.GetItemCount() > 0 && m_vecTransaction_elv_properties.size() > 0 && nSelStatusIdx >= 0 && nSelStatusIdx < m_wndCBox1.GetCount())
	else OnCancel();
}
