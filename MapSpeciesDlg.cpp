// MapSpeciesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MapSpeciesDlg.h"
#include "ResLangFileReader.h"


// CMapSpeciesDlg dialog

IMPLEMENT_DYNAMIC(CMapSpeciesDlg, CDialog)

CMapSpeciesDlg::CMapSpeciesDlg(SpecieMap *pSpcmap,
							   vecTransactionSpecies *pSpecies,
							   CWnd* pParent /*=NULL*/)
	: CDialog(CMapSpeciesDlg::IDD, pParent),
	m_pSpcmap(pSpcmap),
	m_pSpecies(pSpecies)
{
}

CMapSpeciesDlg::~CMapSpeciesDlg()
{
	// Cleanup
	for( SpecieList::iterator iter = m_vals.begin(); iter != m_vals.end(); iter++ )
	{
		CString *pVal = *iter;
		if( pVal )
		{
			delete pVal;
			pVal = NULL;
		}
	}
}

void CMapSpeciesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CMapSpeciesDlg::OnInitDialog()
{
	CString curval;

	if( !CDialog::OnInitDialog() )
		return FALSE;

	// Load language
	CString	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING3286));

			// Create grid
			RECT rect;
			GetWindowRect(&rect);
			m_grid.Create(CRect(0, 0, rect.right - rect.left - 6, rect.bottom - rect.top - 70), this, ID_MAPSPECIESGRID);
			m_grid.ShowHelp(FALSE);
			m_grid.SetTheme(xtpGridThemeOffice2003);
			if( m_grid.m_hWnd )
			{
				CXTPPropertyGridItem *pItem;
				CXTPPropertyGridItem *pSpecies = m_grid.AddCategory(xml->str(IDS_STRING3287));
				pSpecies->Expand();

				// List species from Excel and all avaliable species as constraints
				for( SpecieMap::iterator iterm = m_pSpcmap->begin(); iterm != m_pSpcmap->end(); iterm++ )
				{
					pItem = new CXTPPropertyGridItem(iterm->first);
					curval.Empty();
					for( vecTransactionSpecies::iterator iters = m_pSpecies->begin(); iters != m_pSpecies->end(); iters++ )
					{
						pItem->GetConstraints()->AddConstraint(iters->getSpcName());
						if( iterm->second == iters->getSpcID() ) curval = iters->getSpcName(); // Find current value
					}
					pItem->SetFlags(xtpGridItemHasComboButton);

					// Bind value to string
					CString *pBind = new CString;
					pItem->BindToString(pBind);
					m_vals.push_back(pBind);
					*pBind = curval;

					pSpecies->AddChildItem(pItem);
				}
			}
		}
	}

	return TRUE;
}

void CMapSpeciesDlg::OnOK()
{
	// Store values to specie map
	SpecieList::iterator iterv = m_vals.begin();
	for( SpecieMap::iterator iterm = m_pSpcmap->begin(); iterm != m_pSpcmap->end(); iterm++ )
	{
		if( iterv != m_vals.end() && !(*iterv)->IsEmpty() )
		{
			// Go through list of species to find corresponding id
			for( vecTransactionSpecies::iterator iters = m_pSpecies->begin(); iters != m_pSpecies->end(); iters++ )
			{
				if( iters->getSpcName() == **iterv )
				{
					iterm->second = iters->getSpcID();
					AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_SPECIES_KEY, iterm->first, iters->getSpcID()); // Save to registry
					break;
				}
			}
		}

		iterv++;
	}

	CDialog::OnOK();
}


BEGIN_MESSAGE_MAP(CMapSpeciesDlg, CDialog)
END_MESSAGE_MAP()


// CMapSpeciesDlg message handlers
