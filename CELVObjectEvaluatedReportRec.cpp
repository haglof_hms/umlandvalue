#include "stdafx.h"

CELVObjectEvaluatedReportRec::CELVObjectEvaluatedReportRec(int eval_id,int obj_id,int prop_id,short norm_type,CStringArray &vstand,CStringArray &land,short prop_status,CTransaction_eval_evaluation& rec,CStringArray &strArrReducMark)
{
		CString S;
		double fCorrFactor = rec.getEValCorrFactor();
		double fPinePerc = rec.getEValPinePart();
		double fSprucePerc = rec.getEValSprucePart();
		double fBirchPerc = rec.getEValBirchPart();
		recELVEvaluated = rec;
		nEvalID = eval_id;
		nPropID = prop_id;
		nObjID = obj_id;
		int nForrestIndex = rec.getEValForrestType();
		int nTypeIndex = rec.getEValType();
		int nLen=0,nWidth=0;
		int nLayer=0;
		int nSide=0;
		if (vstand.GetCount() > 0)
		{
			if (nTypeIndex >= 0 && nTypeIndex < vstand.GetCount())
				AddItem(new CExTextItem((vstand.GetAt(nTypeIndex))));
			//else
			//AddItem(new CExConstraintTextItem((vstand.GetAt(0))));
		}
		//#5029 PH 20160706, Added icon to open Eval form.
		if (nTypeIndex == EVAL_TYPE_VARDERING || nTypeIndex == EVAL_TYPE_ANNAN)
			AddItem(new CExIconItemEx((rec.getEValName()),2));
		else
			AddItem(new CExIconItemEx((rec.getEValName()),8));

		//#4207 ta bort arealsber�kningsicon, lagt till som tv� kolumner ist�llet (L�ngd&Bredd)
		AddItem(new CExIconItem(rec.getEValAreal(),3,-1));
		/*
		//Arealen tas fr�n det taxerade best�ndet s� s�tt ingen ikon vid arealen vid v�rdering fr�n taxering
		//20120126 J� Bug #2790
		if(nTypeIndex == EVAL_TYPE_2)
			AddItem(new CExIconItem(rec.getEValAreal(),3,-1));
		else
			AddItem(new CExIconItem(rec.getEValAreal(),3,4));*/

		AddItem(new CExIntItem(rec.getEValAge()));
		AddItem(new CExTextH100Item(rec.getEValSIH100(),norm_type));
		
		//#5029 PH 20160706, Removed the Calculate correction factor button.
		/*
		if (nTypeIndex == EVAL_TYPE_1)
			AddItem(new CExIconItemEx(((fCorrFactor < 0) ? fCorrFactor*(-1.0):fCorrFactor),2,4,MIN_VALUE_FOR_CORRFACTOR,MAX_VALUE_FOR_CORRFACTOR));
		else
			AddItem(new CExIconItemEx(((fCorrFactor < 0) ? fCorrFactor*(-1.0):fCorrFactor),2,8,MIN_VALUE_FOR_CORRFACTOR,MAX_VALUE_FOR_CORRFACTOR));
		*/
		AddItem(new CExFloatItem(((fCorrFactor < 0) ? fCorrFactor*(-1.0):fCorrFactor),sz2dec));

		AddItem(new CExFloatItem(((fPinePerc < 0) ? fPinePerc*(-1.0):fPinePerc),(sz0dec)));
		AddItem(new CExFloatItem(((fSprucePerc < 0) ? fSprucePerc*(-1.0):fSprucePerc),(sz0dec)));
		AddItem(new CExFloatItem(((fBirchPerc < 0) ? fBirchPerc*(-1.0):fBirchPerc),(sz0dec)));
		if (land.GetCount() > 0)
		{
			if (nForrestIndex > 0 && nForrestIndex < land.GetCount())
				AddItem(new CExTextItem((land.GetAt(nForrestIndex))));
			else
				AddItem(new CExTextItem((land.GetAt(0))));
		}

		//HMS-48 L�gg till f�rstoringsglas f�r info om hur markv�rde
		//AddItem(new CExFloatItem(rec.getEValLandValue(),(sz0dec)));
		AddItem(new CExIconItem(rec.getEValLandValue(),0,5));

		//HMS-49 L�gg till f�rstoringsglas f�r info om hur f�rtidig �r ber�knad
		//AddItem(new CExFloatItem(rec.getEValPreCut(),(sz0dec)));
		AddItem(new CExIconItem(rec.getEValPreCut(),0,5));

		
		
		AddItem(new CExFloatItem(rec.getEValLandValue_ha(),(sz0dec)));
		AddItem(new CExFloatItem(rec.getEValPreCut_ha(),(sz0dec)));
		
		//Lagt till volym m3sk och m3sk/ha 20121204 J� #3379
		double fVolHa=0.0;
		AddItem(new CExFloatItem(rec.getEValVolume(),(sz2dec)));
		if(rec.getEValAreal()>0.0)
		{
			fVolHa=rec.getEValVolume()/rec.getEValAreal();
			AddItem(new CExFloatItem(fVolHa,(sz2dec)));
		}
		else
			AddItem(new CExFloatItem(fVolHa,(sz2dec)));

		nLen=rec.getEValLength();
		nWidth=rec.getEValWidth();
		nLayer=rec.getEValLayer();
		nSide=rec.getEValSide();

		AddItem(new CExIntItem(nLen)); // #4207 150114 j�
		AddItem(new CExIntItem(nWidth));  // #4207 150114 j�
		
		AddItem(new CExIntItem(nLayer));  // #5086 160819 PL

		
		//AddItem(new CExCheckItem(rec.getEValOverlappingLand()));  // #4829 PL
		//#HMS-41 Tillf�lligt utnyttjande 190327 J�
		if(rec.getEValOverlappingLand())
			AddItem(new CExTextItem((strArrReducMark.GetAt(EVAL_REDUCMARK_OVERLAPPANDE))));	//�verlappande
		else if(rec.getEValTillfUtnyttjande())
			AddItem(new CExTextItem((strArrReducMark.GetAt(EVAL_REDUCMARK_TILLFALLIGT))));		//tillf�lligt utnyttjande
		else
			AddItem(new CExTextItem((strArrReducMark.GetAt(EVAL_REDUCMARK_INGET))));	//inget

		AddItem(new CExIntItem(nSide));  // #5171 161005 PL

		// Try to set Editbale or not per column here.
		// Depending on nTypeIndex; 090402 p�d

		//if (prop_status <= STATUS_ADVICED)
		if (canWeCalculateThisProp_cached(prop_status))
		{
			if (nTypeIndex == EVAL_TYPE_VARDERING)	// "V�rderingsbest�nd"
			{
				this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
				this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
				this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				//4352 S�tt areal som l�st om l�ngd eller bredd �r 0 20150508 J�
				if(nWidth>0 && nLen>0)
				{
				this->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);				
				}
				else
				{	
				this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				}
				this->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
				this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_4)->SetEditable(TRUE);	// "SI"
				this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_5)->SetEditable(TRUE);	// "Korrektionsfaktor"
				this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_6)->SetEditable(TRUE);	// "Andel Tall %"
				this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_7)->SetEditable(TRUE);	// "Andel Gran %"
				this->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_8)->SetEditable(TRUE);	// "Andel L�v (Bj�rk) %"
				this->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
				this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
				this->GetItem(COLUMN_10)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
				this->GetItem(COLUMN_11)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
				this->GetItem(COLUMN_12)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
				this->GetItem(COLUMN_13)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_14)->SetEditable(TRUE);	// "Volym m3sk"
				this->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_15)->SetEditable(TRUE);	// "Volym m3sk/ha"
				this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);

				this->GetItem(COLUMN_16)->SetEditable(TRUE);	// "L�ngd"  #4207 150114 j�
				this->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_17)->SetEditable(TRUE);	// "Bredd"  #4207 150114 j�
				this->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt"  #5086 160819 PL
				this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
				this->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
			}
			else if (nTypeIndex == EVAL_TYPE_FROM_TAXERING)	// "Taxeringsbest�nd"
			{
				this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
				this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
				this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				//Arealen tas fr�n det taxerade best�ndet s� s�tt arealen som ej editerbar 
				//20120126 J� Bug #2790
				//this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_3)->SetEditable(FALSE);	// "�lder"
				this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
				this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
				this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
				this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
				this->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
				this->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
				this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
				this->GetItem(COLUMN_10)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
				this->GetItem(COLUMN_11)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
				this->GetItem(COLUMN_12)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
				this->GetItem(COLUMN_13)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk"
				this->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha"
				this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);

				this->GetItem(COLUMN_16)->SetEditable(FALSE);	// "L�ngd" #4207 150114 j�
				this->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_17)->SetEditable(FALSE);	// "Bredd"	#4207 150114 j�
				this->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt"  #5086 160819 PL
				this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
				this->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
			}
			else if (nTypeIndex == EVAL_TYPE_ANNAN)	// "Annan mark, ex. �kermark"
			{
				this->GetItem(COLUMN_0)->SetEditable(TRUE);	// "Typ av v�rdering"
				this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_1)->SetEditable(TRUE);	// "Namn"
				this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
				//4352 S�tt areal som l�st om l�ngd eller bredd �r 0 20150508 J�
				if(nWidth>0 && nLen>0)
				{
				this->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);				
				}
				else
				{	
				this->GetItem(COLUMN_2)->SetEditable(TRUE);	// "Areal (ha)"
				this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
				}
     			this->GetItem(COLUMN_3)->SetEditable(TRUE);	// "�lder"
				this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
				this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
				this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
				this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
				this->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
				this->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_9)->SetEditable(TRUE);	// "Typ av skog: Skogsmark,Kalmark etc."
				this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_10)->SetEditable(TRUE);	// "Markv�rde kr"
				this->GetItem(COLUMN_10)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
				this->GetItem(COLUMN_11)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_12)->SetEditable(TRUE);	// "Markv�rde kr/ha"
				this->GetItem(COLUMN_12)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
				this->GetItem(COLUMN_13)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk"
				this->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha"
				this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);

				this->GetItem(COLUMN_16)->SetEditable(TRUE);	// "L�ngd" #4207 150114 j�
				this->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_17)->SetEditable(TRUE);	// "Bredd" #4207 150114 j�
				this->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
				this->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt"  #5086 160819 PL
				this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
				this->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
				this->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
			}
		}	// if (prop_status <= STATUS_ADVICED)
		else
		{
			this->GetItem(COLUMN_0)->SetEditable(FALSE);	// "Typ av v�rdering"
			this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_1)->SetEditable(FALSE);	// "Namn"
			this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_2)->SetEditable(FALSE);	// "Areal (ha)"
			this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_3)->SetEditable(FALSE);	// "�lder"
			this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_4)->SetEditable(FALSE);	// "SI"
			this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_5)->SetEditable(FALSE);	// "Korrektionsfaktor"
			this->GetItem(COLUMN_5)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_6)->SetEditable(FALSE);	// "Andel Tall %"
			this->GetItem(COLUMN_6)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_7)->SetEditable(FALSE);	// "Andel Gran %"
			this->GetItem(COLUMN_7)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_8)->SetEditable(FALSE);	// "Andel L�v (Bj�rk) %"
			this->GetItem(COLUMN_8)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_9)->SetEditable(FALSE);	// "Typ av skog: Skogsmark,Kalmark etc."
			this->GetItem(COLUMN_9)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_10)->SetEditable(FALSE);	// "Markv�rde kr"
			this->GetItem(COLUMN_10)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_11)->SetEditable(FALSE);	// "Merv�rde kr"
			this->GetItem(COLUMN_11)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_12)->SetEditable(FALSE);	// "Markv�rde kr/ha"
			this->GetItem(COLUMN_12)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_13)->SetEditable(FALSE);	// "Merv�rde kr/ha"
			this->GetItem(COLUMN_13)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_14)->SetEditable(FALSE);	// "Volym m3sk"
			this->GetItem(COLUMN_14)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_15)->SetEditable(FALSE);	// "Volym m3sk/ha"
			this->GetItem(COLUMN_15)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_16)->SetEditable(FALSE);	// "L�ngd" #4207 150114 j�
			this->GetItem(COLUMN_16)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_17)->SetEditable(FALSE);	// "Bredd" #4207 150114 j�
			this->GetItem(COLUMN_17)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_18)->SetEditable(FALSE);	// "Skikt"  #5086 160819 PL
			this->GetItem(COLUMN_18)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_19)->SetEditable(FALSE);	// "�verlappande mark" #4829 PL
			this->GetItem(COLUMN_19)->SetBackgroundColor(WHITE);
		}

	}