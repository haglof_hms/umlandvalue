// ObjectResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ObjectResultDlg.h"

#include "ObjectResultFormView.h"

// CObjectResultDlg dialog

IMPLEMENT_DYNAMIC(CObjectResultDlg, CDialog)

BEGIN_MESSAGE_MAP(CObjectResultDlg, CDialog)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL_3, OnSelectedChanged)
	ON_BN_CLICKED(IDC_BUTTON1, &CObjectResultDlg::OnBnClickedButton1)
END_MESSAGE_MAP()

CObjectResultDlg::CObjectResultDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CObjectResultDlg::IDD, pParent)
{

}

CObjectResultDlg::~CObjectResultDlg()
{
}

void CObjectResultDlg::OnDestroy()
{
	CDialog::OnDestroy();	
}

int CObjectResultDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL_3);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		if (m_xml.Load(m_sLangFN))
		{
			if (m_wndTabControl.GetSafeHwnd())
			{
				AddView(RUNTIME_CLASS(CObjectResultFormView), (m_xml.str(IDS_STRING5410)), 3);
				AddView(RUNTIME_CLASS(CObjectResultFormView), (m_xml.str(IDS_STRING5411)), 3);
				AddView(RUNTIME_CLASS(CObjectResultFormView), (m_xml.str(IDS_STRING5420)), 3);
			}	// if (m_wndTabControl.GetSafeHwnd())
		}	// if (m_xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))


	return 0;
}

void CObjectResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnPrintOut);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP

}

BOOL CObjectResultDlg::OnInitDialog()
{
	CRect rect;
	CDialog::OnInitDialog();
	if (m_wndTabControl.GetSafeHwnd())
	{
		GetClientRect(&rect);
		setResize(&m_wndTabControl,1,1,rect.right-2,rect.bottom-30);

		// Setup landownernumber for each tab; 080603 p�d
		getSLenTab()->setupSQLTraktIDs(m_sLandOwnerNumber);
		getEvalueTab()->setupSQLTraktIDs(m_sLandOwnerNumber);
		getRotpostTab()->setupSQLTraktIDs(m_sLandOwnerNumber);
	}

	if (fileExists(m_sLangFN))
	{
		if (m_xml.Load(m_sLangFN))
		{
			if (m_wndTabControl.GetSafeHwnd())
			{
				SetWindowText((m_xml.str(IDS_STRING202)));
				m_wndBtnPrintOut.SetWindowText((m_xml.str(IDS_STRING2302)));
				m_wndBtnCancel.SetWindowText((m_xml.str(IDS_STRING54216)));
			}	// if (m_wndTabControl.GetSafeHwnd())
		}	// if (m_xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))


	return TRUE;
}

void CObjectResultDlg::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		switch (pItem->GetIndex())
		{
			case TAB_RESULT_SLEN_REPORT :
				getSLenTab()->setupSLenReport();
			break;
			case TAB_RESULT_EVAL_REPORT :
				getEvalueTab()->setupEvaluationReport();
			break;
			case TAB_RESULT_NETTO_REPORT :
				getRotpostTab()->setupRotpostReport();
			break;
		};
	}	// if (pItem != NULL)
}

// CObjectResultDlg message handlers

BOOL CObjectResultDlg::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = NULL; //GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = NULL; //GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}


void CObjectResultDlg::OnBnClickedButton1()
{
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		switch (pItem->GetIndex())
		{
			case TAB_RESULT_SLEN_REPORT :
				getSLenTab()->printReport();
			break;
			case TAB_RESULT_EVAL_REPORT :
				getEvalueTab()->printReport();
			break;
			case TAB_RESULT_NETTO_REPORT :
				getRotpostTab()->printReport();;
			break;
		};
	}	// if (pItem != NULL)
}
