// CMDI1950ForrestNormFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "1950ForrestNormFrame.h"
#include "MDI1950ForrestNormFormView.h"
//#include "SearchPropertiesDlg.h"
//#include "SelectContractorDlg.h"

#include "ObjectFormView.h"
#include "ObjectResultFormView.h"
#include "PropertiesFormView.h"
#include "CruiseAndEvalueFormView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
	// TODO: add one-time construction code here

}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2008-08-18 P�D
	
	if (!License())
					return FALSE;

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)


	return TRUE;
}

 void CMDIFrameDoc::PreCloseFrame(CFrameWnd *frame)
 {
	 CDocument::PreCloseFrame(frame);
 }

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc2

IMPLEMENT_DYNCREATE(CMDIFrameDoc2, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc2, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc2::CMDIFrameDoc2()
{
	// TODO: add one-time construction code here

}

CMDIFrameDoc2::~CMDIFrameDoc2()
{
}

BOOL CMDIFrameDoc2::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

 void CMDIFrameDoc2::PreCloseFrame(CFrameWnd *frame)
 {
	 CDocument::PreCloseFrame(frame);
 }

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc2::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc2::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc2::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMDIFrameDoc commands
// CMDIFrameDoc commands
// CMDI1950ForrestNormFormView

IMPLEMENT_DYNCREATE(CMDI1950ForrestNormFormView, CView)

BEGIN_MESSAGE_MAP(CMDI1950ForrestNormFormView, CView)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL_1, OnSelectedChanged)
//	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
END_MESSAGE_MAP()

CMDI1950ForrestNormFormView::CMDI1950ForrestNormFormView()
//	: CView(CMDI1950ForrestNormFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CMDI1950ForrestNormFormView::~CMDI1950ForrestNormFormView()
{
}

void CMDI1950ForrestNormFormView::OnDestroy(void)
{
	if (m_pDB != NULL)
		delete m_pDB;

	CView::OnDestroy();
}

void CMDI1950ForrestNormFormView::OnClose(void)
{
	// OnClose, save data for
	// active object; 080401 p�d
	saveObject();

	CView::OnClose();
}

void CMDI1950ForrestNormFormView::OnSetFocus(CWnd* pWnd)
{
	if (m_bInitialized)
	{
		int nNumOf = 0;
		CObjectFormView *pObjectFormView = getObjectFormView();
		if (pObjectFormView != NULL)
			nNumOf = pObjectFormView->getNumOfItems();

		CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
		if (pItem != NULL)
		{
			int nIndex = pItem->GetIndex();
			if (nIndex > -1)
			{
				if (nIndex == 0)
				{
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM, nNumOf > 0);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM, nNumOf > 0);	
				}
				else if (nIndex == 1)
				{
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM, nNumOf > 0);
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);	
				}
			}
		}
		else
		{
			// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
	}

	CView::OnSetFocus(pWnd);
}


BOOL CMDI1950ForrestNormFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


int CMDI1950ForrestNormFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	if (!	m_bInitialized )
	{
		m_sAbrevLangSet = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		// Send message to HMSShell, please send back the DB connection; 070430 p�d
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				m_sObjTabCaption = (xml->str(IDS_STRING200));
				m_sPropTabCaption = (xml->str(IDS_STRING201));

				AddView(RUNTIME_CLASS(CObjectFormView), m_sObjTabCaption, 3);
				AddView(RUNTIME_CLASS(CPropertiesFormView), m_sPropTabCaption, 3);
			}
			delete xml;
		}

		// Set action to update as default, maybe we'll check if there's any object.
		// If not we'll set it the NEW_ITEM; 080204 p�d
		m_enumACTION = UPD_ITEM;

		m_bInitialized = TRUE;

		CString sObjectDirectory;
		// Get inventory directory set in register.
		// If not set, use the deafult Object Inventory directory; 080205 p�d
		sObjectDirectory = getRegisterObjectInventoryDir();
		// Check to see if the main directory for
		// LandValue Object Inventroy data exists.
		// If not, we'll create it; 080204 p�d
		if (!sObjectDirectory.IsEmpty())
		{
			if (!isDirectory(sObjectDirectory))
			{
				createDirectory(sObjectDirectory);
			}
		}
		else
		{
			sObjectDirectory = getDefaultObjectInventoryDir();
			// Check if directory exists, on disk. If not create it; 080205 p�d
			if (!isDirectory(sObjectDirectory))
			{
				createDirectory(sObjectDirectory);
			}
			// Set directory in register; 080205 p�d
			setRegisterObjectInventoryDir(sObjectDirectory);
		}
	}	// if (!	m_bInitialized )

	return 0;
}

BOOL CMDI1950ForrestNormFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CView::OnCopyData(pWnd, pData);
}

void CMDI1950ForrestNormFormView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 0, cx, cy);
	}
}

void CMDI1950ForrestNormFormView::OnDraw(CDC*)
{
	
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDI1950ForrestNormFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			// This is a new item; 080401 p�d
			m_enumACTION = NEW_ITEM;
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			saveObject();
			// This is an old item; 080401 p�d
			m_enumACTION = UPD_ITEM;
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			// This is an old item; 080401 p�d
			m_enumACTION = UPD_ITEM;
			break;
		}	// case ID_DELETE_ITEM :

		case ID_PREVIEW_ITEM :
		{

			// This is an old item; 080401 p�d
			m_enumACTION = UPD_ITEM;
			break;
		}	// case ID_DELETE_ITEM :

		case ID_DBNAVIG_START :
		case ID_DBNAVIG_PREV :
		case ID_DBNAVIG_NEXT :
		case ID_DBNAVIG_END :
		{
			saveObject();
			break;
		}	// case ID_NEW_ITEM :

	}	// switch (wParam)
	return 0L;
}

void CMDI1950ForrestNormFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	CString S;
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	int nNumOf = 0;
	CObjectFormView *pObjectFormView = getObjectFormView();
	if (pObjectFormView != NULL)
		nNumOf = pObjectFormView->getNumOfItems();
	if (pItem != NULL)
	{
		int nIndex = pItem->GetIndex();
		if (nIndex > -1)
		{
			CTransaction_elv_object *pObj = getActiveObject();
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,nIndex == 0);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,nNumOf > 0);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,nNumOf > 0 && nIndex == 0);
			// Endable/Disable toolbar buttons; 080122 p�d
			CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pFrame != NULL)
			{
				if (nIndex == TAB_GENERAL_DATA)
				{
					pFrame->setContractorTBtn(pObj->getObjID_pk() > -1);
					pFrame->setCalculateTBtn(pObj->getObjID_pk() > -1);
					pFrame->setUpdateTBtn(pObj->getObjID_pk() > -1);
					pFrame->setImportPropertiesTBtn(FALSE);
					pFrame->setPrintoutsCBox(pObj->getObjID_pk() > -1);
					pFrame->setGISButton(pObj->getObjID_pk() > -1);
					pFrame->setToolTBtn(pObj->getObjID_pk() > -1);
					pFrame->setRemoveTBtn(FALSE);
					pFrame->setExportTBtn(pObj->getObjID_pk() > -1);
					pFrame->setExtDocTBtn(pObj->getObjID_pk() > -1);
				}
				else if (nIndex == TAB_PROPERTIES)
				{
					pFrame->setContractorTBtn(FALSE);
					pFrame->setCalculateTBtn(pObj->getObjID_pk() > -1);
					pFrame->setUpdateTBtn(pObj->getObjID_pk() > -1);
					pFrame->setImportPropertiesTBtn(pObj->getObjID_pk() > -1);
					pFrame->setRemoveTBtn(pObj->getObjID_pk() > -1);
					pFrame->setToolTBtn(pObj->getObjID_pk() > -1);
					pFrame->setExportTBtn(pObj->getObjID_pk() > -1);
					pFrame->setPrintoutsCBox(pObj->getObjID_pk() > -1);
					pFrame->setGISButton(pObj->getObjID_pk() > -1);
					pFrame->setExtDocTBtn(FALSE);
				}
				pFrame->setExcludedToolItems(nIndex);
			}	// if (pFrame != NULL)
		}	// if (nIndex > -1)
	}	// if (pItem != NULL)
}

BOOL CMDI1950ForrestNormFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

// CMDI1950ForrestNormFormView diagnostics

/*
// Select a contractor. Can be based on data entered in form; 080327 p�d
void CMDI1950ForrestNormFormView::OnContractorTBtn(void)
{
	CSelectContractorDlg *selectDlg = new CSelectContractorDlg();
	if (selectDlg != NULL)
	{
//		selectDlg->setDBConnection(m_pDB);
		selectDlg->DoModal();

		delete selectDlg;
	}
}
*/

// CMDI1950ForrestNormFormView message handlers
BOOL CMDI1950ForrestNormFormView::saveObject(void)
{
	//-----------------------------------------------------------------------
	// This MUST be before we set 
	// m_enumACTION = UPD_ITEM, because
	// this might be a NEW_ITEM; 080204 p�d
	//createInventoryDirectory();

	// No longer a NEW_ITEM; 080204 p�d
	m_enumACTION = UPD_ITEM;
	//-----------------------------------------------------------------------

	return TRUE;
}

BOOL CMDI1950ForrestNormFormView::createInventoryDirectory(void)
{
	CTransaction_elv_object recObj;
	CString sObjMapp;
	if (m_enumACTION != NEW_ITEM) return FALSE;
	if (m_pDB != NULL)
	{
		m_pDB->getObject(m_pDB->getLastObjectID(),recObj);
		sObjMapp.Format(_T("%s_%s"),recObj.getObjectName(),recObj.getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		doCreateObjectDataDirectory(sObjMapp);
	}	// if (m_pDB != NULL)

	return TRUE;
}

