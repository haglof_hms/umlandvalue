// AddOtherCostsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AddOtherCostsDlg.h"

#include "ResLangFileReader.h"

// CAddOtherCostsDlg dialog

IMPLEMENT_DYNAMIC(CAddOtherCostsDlg, CDialog)


BEGIN_MESSAGE_MAP(CAddOtherCostsDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO25_1, &CAddOtherCostsDlg::OnCbnSelchangeList251)
	ON_BN_CLICKED(IDOK, &CAddOtherCostsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON25_1, &CAddOtherCostsDlg::OnBnClicked251)
	ON_BN_CLICKED(IDC_BUTTON25_2, &CAddOtherCostsDlg::OnBnClicked252)
	ON_BN_CLICKED(IDC_BUTTON25_3, &CAddOtherCostsDlg::OnBnClicked253)
	ON_BN_CLICKED(IDC_BUTTON25_4, &CAddOtherCostsDlg::OnBnClicked254)
	ON_EN_CHANGE(IDC_EDIT25_1, &CAddOtherCostsDlg::OnEnChangeEdit251)
END_MESSAGE_MAP()


CAddOtherCostsDlg::CAddOtherCostsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddOtherCostsDlg::IDD, pParent)
{
	m_enumCompType = NONE;
}

CAddOtherCostsDlg::~CAddOtherCostsDlg()
{
}

void CAddOtherCostsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddOtherCostsDlg)
	DDX_Control(pDX, IDC_LBL25_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL25_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL25_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL25_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL25_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL25_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL25_7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL25_8, m_wndLbl8);
	
	DDX_Control(pDX, IDC_EDIT25_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT25_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT25_3, m_wndEdit3);

	DDX_Control(pDX, IDC_COMBO25_1, m_wndCB1);
	
	//DDX_Control(pDX, IDC_LIST25_1, m_wndLB1);
	//DDX_Control(pDX, IDC_LIST25_2, m_wndLB2);
	DDX_Control(pDX, IDC_LIST_SOURCE4, m_listCtrl);
	DDX_Control(pDX, IDC_LIST_SOURCE3, m_listCtrl2);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP
}

BOOL CAddOtherCostsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING2712));

			m_wndLbl1.SetWindowText(xml.str(IDS_STRING204));
			
			m_wndLbl2.SetLblFont(14,FW_BOLD);
			m_wndLbl2.SetWindowText(m_sObjectName);

			m_wndLbl3.SetWindowText(xml.str(IDS_STRING4248));
			m_wndLbl4.SetWindowText(xml.str(IDS_STRING4247));
			m_wndLbl5.SetWindowText(xml.str(IDS_STRING4240));
			m_wndLbl6.SetWindowText(xml.str(IDS_STRING4241));

			m_wndLbl7.SetLblFont(14,FW_BOLD);
			m_wndLbl7.SetWindowText(xml.str(IDS_STRING2709));
			m_wndLbl8.SetLblFont(14,FW_BOLD);
			m_wndLbl8.SetWindowText(xml.str(IDS_STRING2711));

			m_wndEdit2.SetDisabledColor(BLACK,INFOBK );
			m_wndEdit2.SetEnabledColor(BLACK,WHITE );
			m_wndEdit2.SetReadOnly();

			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING5805));
			m_wndOKBtn.EnableWindow(FALSE);
			m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING5806));

			// Add Templates to ListBox; 090506 p�d
			m_wndCB1.ResetContent();
			m_wndCB1.AddString(xml.str(IDS_STRING42330));
			m_wndCB1.AddString(xml.str(IDS_STRING42331));
			m_wndCB1.AddString(xml.str(IDS_STRING42332));
			m_wndCB1.AddString(xml.str(IDS_STRING42333));

			xml.clean();
		}
	}

	getPropertiesFromDB();
	//m_wndLB1.ResetContent();
	int nCounter = 0;
	int nItem = 0;

	m_listCtrl.ModifyExtendedStyle(0, LVS_EX_FULLROWSELECT|LVS_EX_FULLROWSELECT);
	m_listCtrl.EnableUserRowColor(TRUE);
	m_listCtrl2.ModifyExtendedStyle(0, LVS_EX_FULLROWSELECT|LVS_EX_FULLROWSELECT);
	m_listCtrl2.EnableUserRowColor(TRUE);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_listCtrl.InsertColumn(0,xml.str(IDS_STRING2315), LVCFMT_LEFT, 100);	 
			m_listCtrl.InsertColumn(1,xml.str(IDS_STRING2314), LVCFMT_LEFT, 60);
			m_listCtrl.InsertColumn(2,xml.str(IDS_STRING2311), LVCFMT_LEFT, 100);
			m_listCtrl.InsertColumn(3,xml.str(IDS_STRING2312), LVCFMT_LEFT, 100);
			m_listCtrl2.InsertColumn(0,xml.str(IDS_STRING2315), LVCFMT_LEFT, 100);
			m_listCtrl2.InsertColumn(1,xml.str(IDS_STRING2314), LVCFMT_LEFT, 60);
			m_listCtrl2.InsertColumn(2,xml.str(IDS_STRING2311), LVCFMT_LEFT, 100);
			m_listCtrl2.InsertColumn(3,xml.str(IDS_STRING2312), LVCFMT_LEFT, 100);		
		}
	}


	// Setup Properties in propertyregister, check Status; 090918 p�d
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			nItem=InsertRow(m_listCtrl,i,4,recProps.getPropName(),recProps.getPropNumber(),recProps.getPropCounty(),recProps.getPropMunicipal());
			if (canWeCalculateThisProp_cached(recProps.getPropStatus()))
			{
				m_listCtrl.SetRowColor(nItem, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, recProps.getPropID_pk());
				//m_wndLB1.AddString(recProps.getPropName());
				//m_wndLB1.SetItemData(nCounter,recProps.getPropID_pk());
				//nCounter++;
			}
		}	// for (UINT i = 0;i < m_vecProps.size();i++)
	}	// if (m_vecProps.size() > 0)

		m_listCtrl.RedrawWindow();		

	return TRUE;
}

// CAddOtherCostsDlg message handlers
// Add to class data member: m_vecELV_properties; 080415 p�d
void CAddOtherCostsDlg::getPropertiesFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_nObjID,m_vecProps);
	}	// if (m_pDB != NULL)
}

CTransaction_elv_properties& CAddOtherCostsDlg::getProps(int prop_id)
{
	// Setup Properties in propertyregister, check Status; 090918 p�d
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			if (recProps.getPropID_pk() == prop_id)
				return recProps;
		}	// for (UINT i = 0;i < m_vecProps.size();i++)
	}	// if (m_vecProps.size() > 0)

	return CTransaction_elv_properties();
}

void CAddOtherCostsDlg::OnCbnSelchangeList251()
{
	int nSelIdx = m_wndCB1.GetCurSel();
	// Set comp. type; 100611 p�d
	if (nSelIdx == 0) m_enumCompType = COMP;
	else if (nSelIdx == 1) m_enumCompType = COMP_VAT;
	else if (nSelIdx == 2) m_enumCompType = COMP_INFR;
	else if (nSelIdx == 3) m_enumCompType = COMP_MERCH;
	else m_enumCompType = NONE;

	m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && nSelIdx > CB_ERR);
	OnEnChangeEdit251();
}

void CAddOtherCostsDlg::OnBnClicked251()
{
	CString sPropName;
	CTransaction_elv_properties rec;
	int nSelIdx = -1,i=0; 
	int nSelPropID = -1;
	int nItem = -1;
	//int nNumSel = m_wndLB1.GetSelCount();
	int nNumSel = m_listCtrl.GetSelectedCount();
	if (nNumSel > 0)
	{
		for (int i=0; i < nNumSel; i++)
		{
			nItem = m_listCtrl.GetNextItem(-1, LVNI_SELECTED);
			nSelPropID = m_listCtrl.GetItemData(nItem);

			if(nSelPropID != -1)
			{
				rec = getProps(nSelPropID);
				sPropName = rec.getPropName();
				InsertRow(m_listCtrl2,m_listCtrl2.GetItemCount(),4,rec.getPropName(),rec.getPropNumber(),rec.getPropCounty(),rec.getPropMunicipal());
				m_listCtrl2.SetItemData(m_listCtrl2.GetItemCount()-1,rec.getPropID_pk());
				//m_wndLB2.AddString(sPropName);
				//m_wndLB2.SetItemData(m_wndLB2.GetCount()-1,rec.getPropID_pk());
				//Tar bort vald instans fr�n listan feature #2389 20111004 J�
				m_listCtrl.DeleteItem(nItem);
				m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
			}
		}
	}

	/*
	if(nNumSel>0)
	{
		LPINT lpnSelItems = new int[nNumSel];
		m_wndLB1.GetSelItems(nNumSel,lpnSelItems);
		//Removed Resetcontent, do not reset current "Annan ers�ttning" choises when adding another "annan ers" to the list, bug #2308 20110826 J�
		//m_wndLB2.ResetContent();
		do	
		{
			nSelIdx = lpnSelItems[i];
			nSelPropID = m_wndLB1.GetItemData(nSelIdx);
			rec = getProps(nSelPropID);
			sPropName = rec.getPropName();
			// Check that Property isn-t already added; 091009 p�d
			if (m_wndLB2.FindString(0,sPropName) == LB_ERR)
			{
				m_wndLB2.AddString(sPropName);
				m_wndLB2.SetItemData(m_wndLB2.GetCount()-1,rec.getPropID_pk());
				//Tar bort vald instans fr�n listan feature #2389 20111004 J�
				m_wndLB1.DeleteString(nSelIdx);
				nNumSel = m_wndLB1.GetSelCount();
				m_wndLB1.GetSelItems(nNumSel,lpnSelItems);
				i=-1;
			}
			i++;
			m_wndOKBtn.EnableWindow(m_wndLB2.GetCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
		}while(i < nNumSel);

		delete[] lpnSelItems;
	}*/

}

void CAddOtherCostsDlg::OnBnClicked252()
{
	int i=0;
	CTransaction_elv_properties rec;
	if(m_listCtrl.GetItemCount()>0)
	{
		//for(int i = 0; i < m_listCtrl.GetItemCount(); i++)
		do
		{
			int nSelPropID = m_listCtrl.GetItemData(i);
			if(nSelPropID != -1)
			{
				rec = getProps(nSelPropID);
				InsertRow(m_listCtrl2,m_listCtrl2.GetItemCount(),4,rec.getPropName(),rec.getPropNumber(),rec.getPropCounty(),rec.getPropMunicipal());
				m_listCtrl2.SetItemData(m_listCtrl2.GetItemCount()-1,rec.getPropID_pk());

				//m_wndLB2.AddString(rec.getPropName());
				//m_wndLB2.SetItemData(m_wndLB2.GetCount()-1,rec.getPropID_pk());

				//Tar bort vald instans fr�n listan feature #2389 20111004 J�
				m_listCtrl.DeleteItem(i);
			}
		}while(m_listCtrl.GetItemCount()>0);
		m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
		m_listCtrl.RedrawWindow();
	}


	//Inte resetta andra listan h�r utan f�r �ver de som �r kvar i f�rsta listan #2389 20111004 J�
	//m_wndLB2.ResetContent();
	/*
	for (int i = 0;i < m_wndLB1.GetCount();i++)
	{
		int nSelPropID = m_wndLB1.GetItemData(i);
		rec = getProps(nSelPropID);
		m_wndLB2.AddString(rec.getPropName());
		m_wndLB2.SetItemData(m_wndLB2.GetCount()-1,rec.getPropID_pk());
	}
	//Nollar f�rsta listan efter att alla �r �verf�rda feature #2389 20111004 J�
	m_wndLB1.ResetContent();
	m_wndOKBtn.EnableWindow(m_wndLB2.GetCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);*/
}

void CAddOtherCostsDlg::OnBnClicked253()
{
	int nSelIdx = 0;
	int i=0;
	CString sPropName;
	CTransaction_elv_properties rec;
	int nSelPropID = -1;
	int nNumSel = m_listCtrl2.GetSelectedCount();
	int nItem = -1;
	if (nNumSel > 0)
	{
		for (int i=0; i < nNumSel; i++)
		{
			nItem = m_listCtrl2.GetNextItem(-1, LVNI_SELECTED);
			nSelPropID = m_listCtrl2.GetItemData(nItem);
			rec = getProps(nSelPropID);
			m_listCtrl2.DeleteItem(nItem);
			InsertRow(m_listCtrl,m_listCtrl.GetItemCount(),4,rec.getPropName(),rec.getPropNumber(),rec.getPropCounty(),rec.getPropMunicipal());
			m_wndOKBtn.EnableWindow(m_listCtrl2.GetItemCount() > 0 && m_wndCB1.GetCurSel() > CB_ERR);
		}
	}
	/*
	//Lagt till funktion att kunna v�lja flera att f�re tillbaks med enkelpil Bug #2582 20111123 J�
	int nNumSel = m_wndLB2.GetSelCount();
	if(nNumSel>0)
	{
		LPINT lpnSelItems = new int[nNumSel];
		m_wndLB2.GetSelItems(nNumSel,lpnSelItems);
		do	
		{
			nSelIdx = lpnSelItems[i];
			//L�gger tillbaks vald instans till listan feature #2389 20111004 J�
			nSelPropID = m_wndLB2.GetItemData(nSelIdx);
			m_wndLB2.DeleteString(nSelIdx);
			rec = getProps(nSelPropID);
			sPropName = rec.getPropName();
			m_wndLB1.AddString(sPropName);
			m_wndLB1.SetItemData(m_wndLB1.GetCount()-1,rec.getPropID_pk());
			nNumSel = m_wndLB2.GetSelCount();
			m_wndLB2.GetSelItems(nNumSel,lpnSelItems);
		}while(i<nNumSel && nNumSel>0);
		delete[] lpnSelItems;
	}*/
}

void CAddOtherCostsDlg::OnBnClicked254()
{
	int nItem=0;
	m_listCtrl2.DeleteAllItems();

	//L�ser in fastigheter igen, feature #2389 20111004 J�
	m_listCtrl.DeleteAllItems();
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			nItem = m_listCtrl.InsertItem(i, recProps.getPropName());

			if (canWeCalculateThisProp_cached(recProps.getPropStatus()))
			{
				m_listCtrl.SetRowColor(nItem, RGB(0, 0, 0), m_listCtrl.GetBkColor(), TRUE);
				m_listCtrl.SetItemData(nItem, recProps.getPropID_pk());
			}
		}	// for (UINT i = 0;i < m_vecProps.size();i++)
		m_listCtrl.RedrawWindow();
	}	// if (m_vecProps.size() > 0)
	m_wndOKBtn.EnableWindow(FALSE);

	/*
	m_wndLB2.ResetContent();   
	//L�ser in fastigheterna igen #2389 20111004 J�
	m_wndLB1.ResetContent();
	int nCounter = 0;
	// Setup Properties in propertyregister, check Status; 090918 p�d
	if (m_vecProps.size() > 0)
	{
		for (UINT i = 0;i < m_vecProps.size();i++)
		{
			recProps = m_vecProps[i];
			if (canWeCalculateThisProp_cached(recProps.getPropStatus()))
			{
				m_wndLB1.AddString(recProps.getPropName());
				m_wndLB1.SetItemData(nCounter,recProps.getPropID_pk());
				nCounter++;
			}
		}	// for (UINT i = 0;i < m_vecProps.size();i++)
	}	// if (m_vecProps.size() > 0)
	m_wndOKBtn.EnableWindow(FALSE);*/
}

void CAddOtherCostsDlg::OnEnChangeEdit251()
{
	// Get values added in EditBox;
	double fValue = m_wndEdit1.getFloat();
	
	if (m_enumCompType == COMP)
	{
		m_wndEdit2.setFloat(fValue,0);
	}
	else if (m_enumCompType == COMP_VAT)
	{
		m_wndEdit2.setFloat(fValue*(1.0 + (m_recObject.getObjVAT()/100.0)),0);
	}
	else if (m_enumCompType == COMP_INFR)
	{
		m_wndEdit2.setFloat(fValue*(m_recObject.getObjPercent()/100.0),0);
	}
	else if ( m_enumCompType == COMP_MERCH) // Kan inte skriva ut detta d� det ber�knas per m3 gagnvirke per fastighet och inte f�r alla valda fastigheter
	{
		m_wndEdit2.SetWindowText(L"");
	}
}

void CAddOtherCostsDlg::OnBnClickedOk()
{
	CTransaction_elv_properties rec;
	int i=0;
	m_nSelectedCostsType = m_wndCB1.GetCurSel();
	m_fSumOtherComp = m_wndEdit1.getFloat();
	m_fSumOtherComp_calc = m_wndEdit2.getFloat();
	m_sNotes = m_wndEdit3.getText();
	m_vecProps_selected.clear();

	if(m_listCtrl2.GetItemCount()>0)
	{
		//for(int i = 0; i < m_listCtrl.GetItemCount(); i++)
		do
		{
			int nSelPropID = m_listCtrl2.GetItemData(i);
			if(nSelPropID != -1)
			{
				rec = getProps(nSelPropID);
				if (rec.getPropID_pk() > -1)
					m_vecProps_selected.push_back(rec);
			}
			i++;
		}while(m_listCtrl2.GetItemCount()>0 && i<m_listCtrl2.GetItemCount());
	}
	/*
	for (int i = 0;i < m_wndLB2.GetCount();i++)
	{
		int nSelPropID = m_wndLB2.GetItemData(i);
		rec = getProps(nSelPropID);
		if (rec.getPropID_pk() > -1)
			m_vecProps_selected.push_back(rec);
	}*/

	OnOK();
}
