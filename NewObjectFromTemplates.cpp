// NewObjectFromTemplates.cpp : implementation file
//

#include "stdafx.h"
#include "NewObjectFromTemplates.h"

#include "ResLangFileReader.h"

// CNewObjectFromTemplates dialog

IMPLEMENT_DYNAMIC(CNewObjectFromTemplates, CDialog)


BEGIN_MESSAGE_MAP(CNewObjectFromTemplates, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnCbnSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_COMBO3, OnCbnSelchangeCombo3)
	ON_EN_CHANGE(IDC_EDIT1, OnBnObjName)
	ON_EN_CHANGE(IDC_EDIT20, OnBnObjNumber)
	ON_BN_CLICKED(IDOK, &CNewObjectFromTemplates::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO4, &CNewObjectFromTemplates::OnCbnSelchangeCombo4)
END_MESSAGE_MAP()

CNewObjectFromTemplates::CNewObjectFromTemplates(CWnd* pParent /*=NULL*/)
	: CDialog(CNewObjectFromTemplates::IDD, pParent)
{
	m_bIsSelectionOK = FALSE;
	m_nSelectedForstNorm = -1;
}

CNewObjectFromTemplates::~CNewObjectFromTemplates()
{
}

void CNewObjectFromTemplates::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL_TRAKT_TMPL2, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_TRAKT_TMPL3, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_P30_TMPL, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL_HCOST_TMPL, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL_P30_TMPL3, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL_P30_TMPL2, m_wndLbl7);

	DDX_Control(pDX, IDC_EDIT1, m_wndObjName);
	DDX_Control(pDX, IDC_EDIT20, m_wndObjNumber);
	DDX_Control(pDX, IDC_EDIT2, m_wndObjGArea);

	DDX_Control(pDX, IDC_COMBO2, m_wndCBoxP30Templates);
	DDX_Control(pDX, IDC_COMBO3, m_wndCBoxHCostTemplates);
	DDX_Control(pDX, IDC_COMBO4, m_wndCBoxForestNorm);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

// CNewObjectFromTemplates message handlers

BOOL CNewObjectFromTemplates::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING2700)));

			m_sGrowthAreaLbl = xml->str(IDS_STRING207);

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING204)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING205)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING2702)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING2703)));
			m_wndLbl7.SetWindowText((xml->str(IDS_STRING2250)));

			setRangeForGrowthArea(1,6);

			m_sMsgCap = (xml->str(IDS_STRING229));
			m_sMsgNoSelection = (xml->str(IDS_STRING2707));

			m_sMsgNameAndID.Format(_T("%s\n\n%s\n"),
				xml->str(IDS_STRING6006),
				xml->str(IDS_STRING6007));

			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING2704)));
			m_wndOKBtn.EnableWindow(FALSE);
			m_wndCancelBtn.SetWindowText((xml->str(IDS_STRING2706)));
		}
		delete xml;
	}

	m_wndCBoxP30Templates.SetEnabledColor(BLACK,WHITE);
	m_wndCBoxP30Templates.SetDisabledColor(BLACK,COL3DFACE);

	m_wndObjName.SetLimitText(50);
	m_wndObjNumber.SetLimitText(10);

	m_wndObjGArea.SetAsNumeric();
	m_wndObjGArea.SetRange(1,6);
	m_wndObjGArea.setInt(1);
	m_wndObjGArea.SetEnabledColor(BLACK,WHITE);
	m_wndObjGArea.SetDisabledColor(BLACK,COL3DFACE);
	m_wndObjGArea.SetReadOnly();

	m_nIndexCBox1 = CB_ERR;
	m_nIndexCBox2 = CB_ERR;
	m_nIndexCBox3 = CB_ERR;
	m_nIndexCBox4 = CB_ERR;

	// Get Function(s) in UCLandValueNorm.dll; 081217 p�d
	getForrestNormFunctions(m_vecForrestNormFunc);

	setupComboboxes();

	m_wndCBoxP30Templates.EnableWindow(FALSE);
	m_wndObjGArea.EnableWindow(FALSE);
	return TRUE;
}

void CNewObjectFromTemplates::setupP30CBox(int type_of)
{
	m_nSelectedForstNorm = type_of;
	// P30 templates; 080407 p�d
	m_wndCBoxP30Templates.ResetContent();
	if (type_of == TEMPLATE_P30)
	{
		if (m_vecObjectP30Template.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecObjectP30Template.size();i1++)
			{
				m_wndCBoxP30Templates.AddString(m_vecObjectP30Template[i1].getTemplateName());
			}
			m_wndCBoxP30Templates.SetCurSel(-1);	// No selection; 090429 p�d
		}
	}
	else if (type_of == TEMPLATE_P30_NEW_NORM)
	{
		if (m_vecObjectP30TemplateNN.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecObjectP30TemplateNN.size();i1++)
			{
				m_wndCBoxP30Templates.AddString(m_vecObjectP30TemplateNN[i1].getTemplateName());
			}
			m_wndCBoxP30Templates.SetCurSel(-1);	// No selection; 090429 p�d
		}
	}
	else if (type_of == TEMPLATE_P30_2018_NORM)
	{
		if (m_vecObjectP30Template2018.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecObjectP30Template2018.size();i1++)
			{
				m_wndCBoxP30Templates.AddString(m_vecObjectP30Template2018[i1].getTemplateName());
			}
			m_wndCBoxP30Templates.SetCurSel(-1);	// No selection; 090429 p�d
		}
	}
}

void CNewObjectFromTemplates::setupComboboxes(void)
{
	// Add Forest norms; 090429 p�d
	m_wndCBoxForestNorm.ResetContent();
	if (m_vecForrestNormFunc.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecForrestNormFunc.size();i1++)
		{
			m_wndCBoxForestNorm.AddString(m_vecForrestNormFunc[i1].getName());
		}
	}

	// 'F�rdyrad avverkning' templates; 080407 p�d
	m_wndCBoxHCostTemplates.ResetContent();
	if (m_vecObjectHCostTemplate.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecObjectHCostTemplate.size();i1++)
		{
			m_wndCBoxHCostTemplates.AddString(m_vecObjectHCostTemplate[i1].getTemplateName());
		}
	}

}

void CNewObjectFromTemplates::OnBnObjName()
{
	m_sObjName = m_wndObjName.getText();
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);

}

void CNewObjectFromTemplates::OnBnObjNumber()
{
	m_sObjNumber = m_wndObjNumber.getText();
//	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox1 > CB_ERR && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);
}

void CNewObjectFromTemplates::OnCbnSelchangeCombo2()
{
	TemplateParser pars;
	CTransaction_template recP30NN;
	vecObjectTemplate_p30_nn_table vecP30_nn;
	CString sArea;
	int nAreaIndex = -1;
	m_nIndexCBox2 = m_wndCBoxP30Templates.GetCurSel();
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);

	// Setup "tillv�xtomr�de", based on "P30-prislista"; 100920 p�d
	// Only for "nya skogsnormen"  180131 PH) 
	if (m_nIndexCBox4 == 1 )
	{
		if (m_nIndexCBox2 >= 0 && m_nIndexCBox2 < m_vecObjectP30TemplateNN.size())
		{
			recP30NN = m_vecObjectP30TemplateNN[m_nIndexCBox2];
			if (pars.LoadFromBuffer(recP30NN.getTemplateFile()))
			{
				pars.getObjTmplP30_nn(vecP30_nn,sArea,&nAreaIndex);
				switch (nAreaIndex)
				{
					case 0 : m_wndObjGArea.setInt(1); break;
					case 1 : m_wndObjGArea.setInt(2); break;
					case 2 : m_wndObjGArea.setInt(3); break;
					case 3 :
					case 4 : m_wndObjGArea.setInt(4); break;
					case 5 : m_wndObjGArea.setInt(5); break;
				};
			}	// if (pars.LoadFromBuffer(recP30NN.getTemplateFile()))
		}	// if (m_nIndexCBox2 >= 0 && m_vecObjectP30TemplateNN.size() < m_nIndexCBox2)
	}	// if (m_nIndexCBox4 == 1)
// Only for "2108 skogsnormen"  180131 PH) 
	if (m_nIndexCBox4 == 2)
	{
		if (m_nIndexCBox2 >= 0 && m_nIndexCBox2 < m_vecObjectP30Template2018.size())
		{
			recP30NN = m_vecObjectP30Template2018[m_nIndexCBox2];
			if (pars.LoadFromBuffer(recP30NN.getTemplateFile()))
			{
				pars.getObjTmplP30_nn(vecP30_nn,sArea,&nAreaIndex);
				switch (nAreaIndex)
				{
					case 0 : m_wndObjGArea.setInt(1); break;
					case 1 : m_wndObjGArea.setInt(2); break;
					case 2 : m_wndObjGArea.setInt(3); break;
					case 3 :
					case 4 : m_wndObjGArea.setInt(4); break;
					case 5 : m_wndObjGArea.setInt(5); break;
				};
			}	// if (pars.LoadFromBuffer(recP30NN.getTemplateFile()))
		}	// if (m_nIndexCBox2 >= 0 && m_vecObjectP30TemplateNN.size() < m_nIndexCBox2)
	}	// if (m_nIndexCBox4 == 1)

	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);
}

void CNewObjectFromTemplates::OnCbnSelchangeCombo3()
{
	m_nIndexCBox3 = m_wndCBoxHCostTemplates.GetCurSel();
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);
}

void CNewObjectFromTemplates::OnBnClickedOk()
{
	BOOL bObjectNameAndIDUsed = FALSE;
	CTransaction_elv_object rec;
	
	if (m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR)
	{
		
		if (m_nSelectedForstNorm == TEMPLATE_P30)
		{
			m_recSelectedP30Template = m_vecObjectP30Template[m_nIndexCBox2];
			m_nForestNormIndex=0;
		}
		else if (m_nSelectedForstNorm == TEMPLATE_P30_NEW_NORM)
		{
			m_nForestNormIndex=m_nIndexCBox4;
			m_recSelectedP30Template = m_vecObjectP30TemplateNN[m_nIndexCBox2];
		}
		else if (m_nSelectedForstNorm == TEMPLATE_P30_2018_NORM)
		{
			m_nForestNormIndex=m_nIndexCBox4;
			m_recSelectedP30Template = m_vecObjectP30Template2018[m_nIndexCBox2];
		}

		m_recSelectedHCostTemplate = m_vecObjectHCostTemplate[m_nIndexCBox3];
		m_nGrowthArea = m_wndObjGArea.getInt();
		// Check if Objectname and ObjectID is unique; 090309 p�d
		if (m_vecObjects.size() > 0)
		{
			for (UINT i = 0;i < m_vecObjects.size();i++)
			{
				rec = m_vecObjects[i];
				if (rec.getObjectName().CompareNoCase(m_sObjName) == 0 &&	rec.getObjIDNumber().CompareNoCase(m_sObjNumber) == 0)
				{
					bObjectNameAndIDUsed = TRUE;
				}	// if (rec.getObjectName().CompareNoCase(m_sObjName) == 0 ||
			}	// for (UINT i 0 0;i < m_vecObjects.size();i++)
		}	// if (m_vecObjects.size() > 0)

		// Objectname and ObjectID unique, quit ok; 090309 p�d
		if (!bObjectNameAndIDUsed)	OnOK();
		else
		{
			::MessageBox(this->GetSafeHwnd(),m_sMsgNameAndID,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			m_sObjName.Empty();
			m_sObjNumber.Empty();
			m_wndObjName.SetWindowTextW(_T(""));
			m_wndObjNumber.SetWindowTextW(_T(""));
			m_wndObjName.SetFocus();
		}
	}
	else
		::MessageBox(this->GetSafeHwnd(),m_sMsgNoSelection,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

}

void CNewObjectFromTemplates::OnCbnSelchangeCombo4()
{
	m_nIndexCBox4 = m_wndCBoxForestNorm.GetCurSel();
	m_nIndexCBox2 = CB_ERR;
	if (m_nIndexCBox4 > CB_ERR)
	{
		m_wndCBoxP30Templates.EnableWindow(TRUE);
		m_wndObjGArea.EnableWindow(TRUE);
		m_wndObjGArea.SetReadOnly(FALSE);
		// "1950:�rs norm"; 090429 p�d
		if (m_nIndexCBox4 == 0)
		{
			// Set "P30-priser f�r 1950:�rs norm"; 090429 p�d
			setupP30CBox(TEMPLATE_P30);
			setRangeForGrowthArea(1,6);
			//#4529 20151012 J�
			m_wndObjGArea.SetReadOnly(FALSE);
		}
		// "Skogsnormen ver1.1"; 090429 p�d		
		else if (m_nIndexCBox4 == 1 )
		{
			// Set "P30-priser f�r ny norm"; 090429 p�d
			setupP30CBox(TEMPLATE_P30_NEW_NORM);
			setRangeForGrowthArea(1,5);
			//#4529 20151012 J�
			m_wndObjGArea.SetReadOnly(TRUE);
		}
		else if (m_nIndexCBox4 == 2)
		{
			// Set "P30-priser f�r 2018 norm"; 20180321 J�
			setupP30CBox(TEMPLATE_P30_2018_NORM);
			setRangeForGrowthArea(1,5);
			m_wndObjGArea.SetReadOnly(TRUE);
		}
	}
	else
	{
		m_wndCBoxP30Templates.SetCurSel(-1);
		m_wndObjGArea.SetWindowTextW(_T(""));
		m_wndCBoxP30Templates.EnableWindow(FALSE);
		m_wndObjGArea.EnableWindow(FALSE);
		m_wndObjGArea.SetReadOnly(TRUE);
	}
//	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox1 > CB_ERR && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjNumber.IsEmpty() && m_nIndexCBox2 > CB_ERR && m_nIndexCBox3 > CB_ERR && m_nIndexCBox4 > CB_ERR);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);
}
