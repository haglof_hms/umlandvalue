// stdafx.cpp : source file that includes just the standard includes
// UMLandValue.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "Resource.h"
#include "ResLangFileReader.h"

#include "1950ForrestNormFrame.h"
#include "MDI1950ForrestNormFormView.h"

#include "ObjectFormView.h"


//////////////////////////////////////////////////////////////////////////
// Set in UMLandValue.cpp; 090916 p�d
extern CUMLandValueDB *global_pDB;

extern HINSTANCE hInst;

extern StrMap global_langMap;

//////////////////////////////////////////////////////////////////////////
// CExTextItem

CExTextItem::CExTextItem(CString sValue) 
: CXTPReportRecordItemText(sValue)
{
	m_sText = sValue;
}
void CExTextItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	m_sText = szText;
	SetValue(m_sText);
}

CString CExTextItem::getTextItem(void)	{ return m_sText; }
void CExTextItem::setTextItem(LPCTSTR text)	
{ 
	m_sText = text; 
	SetValue(m_sText);
}

void CExTextItem::setIsBold(void)		{ SetBold();	}

void CExTextItem::setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
void CExTextItem::setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}

////////////////////////////////////////////////////////////////////////////////////////////////
// 
int CExConstraintTextItem::m_nConstraintIndex = 0;
CExConstraintTextItem::CExConstraintTextItem(CString sValue) 
: CXTPReportRecordItemText(sValue)
{
	m_sText = sValue;
}
void CExConstraintTextItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	m_sText = szText;
	SetValue(m_sText);
}

void CExConstraintTextItem::OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs,CXTPReportRecordItemConstraint* pConstraint)
{
	if (pConstraint != NULL)
	{
		m_nConstraintIndex = pConstraint->GetIndex();
	}

	CXTPReportRecordItemText::OnConstraintChanged(pItemArgs,pConstraint);
}

CString CExConstraintTextItem::getTextItem(void)	{ return m_sText; }
void CExConstraintTextItem::setTextItem(LPCTSTR text)	
{ 
	m_sText = text; 
	SetValue(m_sText);
}

int CExConstraintTextItem::getConstraintIndex(void)		{ return m_nConstraintIndex; }

void CExConstraintTextItem::setIsBold(void)		{ SetBold();	}

void CExConstraintTextItem::setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
void CExConstraintTextItem::setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}


//////////////////////////////////////////////////////////////////////////
// CExIconItem

CExIconItem::CExIconItem(CString text,int icon_id)
{
	m_sText = text;
	SetIconIndex(icon_id);
	SetCaption((text));
	m_dec = -1;
}
	
CExIconItem::CExIconItem(int num_of,int icon_id)
{
	if (num_of > 0)
	{
		m_sText.Format(_T("(%d)"),num_of);
		SetIconIndex(icon_id);
		SetCaption(m_sText);
	}
	m_dec = -1;
}

CExIconItem::CExIconItem(double value,int dec,int icon_id)
{
	TCHAR tmp[50];
	m_fValue = value;
	m_dec = dec; 
	SetIconIndex(icon_id);
	_stprintf(tmp,_T("%.*f"),m_dec,value);
	SetCaption((tmp));
}

void CExIconItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	TCHAR tmp[50];
	m_sText = szText;
	if (m_dec > -1)
	{
		m_fValue = _tstof(m_sText);
		_stprintf(tmp,_T("%.*f"),m_dec,_tstof(m_sText));
		SetCaption(tmp);
	}
	else
		SetCaption(m_sText);

}



//////////////////////////////////////////////////////////////////////////
// CExIconItemEx

CExIconItemEx::CExIconItemEx(CString text,int icon_id)
{
	m_sText = text;
	SetIconIndex(icon_id);
	SetCaption((text));
	m_dec = -1;
}
	
CExIconItemEx::CExIconItemEx(int num_of,int icon_id,int min,int max)
{
	if (num_of > 0)
	{
		m_sText.Format(_T("(%d)"),num_of);
		SetIconIndex(icon_id);
		SetCaption(m_sText);
	}
	m_dec = -1;
	m_nMin = min;
	m_nMax = max;
}

CExIconItemEx::CExIconItemEx(double value,int dec,int icon_id,double min,double max)
{
	TCHAR tmp[50];
	m_fValue = value;
	m_dec = dec; 
	SetIconIndex(icon_id);
	_stprintf(tmp,_T("%.*f"),m_dec,value);
	SetCaption((tmp));
	m_fMin = min;
	m_fMax = max;
}

void CExIconItemEx::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	TCHAR tmp[50];
	m_sText = szText;
	if (m_dec > -1)
	{
		m_fValue = _tstof(m_sText);
		//  Check value; value within limits
		if (m_fValue >= m_fMin && m_fValue <= m_fMax)
		{
			_stprintf(tmp,_T("%.*f"),m_dec,_tstof(m_sText));
			SetCaption(tmp);
		}
		//  Check value; less than minvalue but GT 0.0
		else if (m_fValue > 0.0 && m_fValue < m_fMin)
		{	
			CString sMsg;
			sMsg.Format(global_langMap[IDS_STRING6320],m_fMin,m_fMax);
			::MessageBox(0,sMsg,global_langMap[IDS_STRING229],MB_ICONASTERISK | MB_OK);
			_stprintf(tmp,_T("%.*f"),m_dec,_tstof(m_sText));
			SetCaption(tmp);
		}
		//  Check value; value greater than max value
		else if (m_fValue > m_fMax)
		{	
			CString sMsg;
			sMsg.Format(global_langMap[IDS_STRING6320],m_fMin,m_fMax);
			::MessageBox(0,sMsg,global_langMap[IDS_STRING229],MB_ICONASTERISK | MB_OK);
			_stprintf(tmp,_T("%.*f"),m_dec,_tstof(m_sText));
			SetCaption(tmp);
		}
		//  Check value; less than less than 0.0
		else if (m_fValue < 0.0)
		{	
			CString sMsg1(global_langMap[IDS_STRING6317]),sMsg2,sMsg;
			sMsg2.Format(global_langMap[IDS_STRING6320],m_fMin,m_fMax);
			sMsg = sMsg1 + L"\n" + sMsg2 + L"\n" + global_langMap[IDS_STRING6319];
			::MessageBox(0,sMsg,global_langMap[IDS_STRING229],MB_ICONASTERISK | MB_OK);
		}
	}
	else
		SetCaption(m_sText);

}


//////////////////////////////////////////////////////////////////////////
// CExIconToogleItem
CExIconToogleItem::CExIconToogleItem(vecIconToogle &vec,int default_item,enumToogleType toogle_type)
{
	m_enumTOOGLE_TYPE = toogle_type;
	m_enumDirection = NEXT;
	m_vecIconToogle = vec;
	m_nSize = m_vecIconToogle.size();

	if (m_vecIconToogle.size() > 0)
	{
		if (m_nSize > default_item)
			m_nToogleValue = default_item;
		else
			m_nToogleValue = 0;

		m_sText = m_vecIconToogle[m_nToogleValue].szText;
		SetIconIndex(m_vecIconToogle[m_nToogleValue]._icon_id);
		SetCaption(m_sText);
	}	// if (m_vecIconToogle.size() > 0)
}

void CExIconToogleItem::setExIconToogleText(LPCTSTR v)	
{ 
	m_sText = v; 
	SetCaption((m_sText));
}

void CExIconToogleItem::setToogle(void)
{
	if (m_vecIconToogle.size() > 0)
	{
		if (m_enumTOOGLE_TYPE == TOOGLE_FW_BW)
		{
			if (m_nToogleValue <= 0) m_enumDirection = NEXT;	// Move forward
			if (m_nToogleValue >= m_nSize-1) m_enumDirection = PREV;	// Move backward
			if (m_enumDirection == NEXT)	m_nToogleValue++;
			else if (m_enumDirection == PREV)	m_nToogleValue--; 
		}
		else if (m_enumTOOGLE_TYPE == TOOGLE_ONLY_FW)
		{
			m_nToogleValue++;
			if (m_nToogleValue > m_nSize-1) m_nToogleValue = 0;
		}

		m_sText = m_vecIconToogle[m_nToogleValue].szText;
		SetIconIndex(m_vecIconToogle[m_nToogleValue]._icon_id);
		SetCaption(m_sText);
	}	// if (m_nIconToogle != NULL)
}

/////////////////////////////////////////////////////////////////////////////////
void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString S;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (strcmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,BOOL do_create_table,CString database_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!database_name.IsEmpty())
					_tcscpy_s(sDBName,127,database_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (do_create_table)
								{
									if (!pDB->existsTableInDB(sDBName,table_name))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name);
										pDB->commandExecute(sScript);
										bReturn = TRUE;
									}
								}
								else if (!do_create_table)
								{
									if (pDB->existsTableInDB(sDBName,table_name))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name,table_name);
										pDB->commandExecute(sScript);
										bReturn = TRUE;
									}
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (strcmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx1(LPCTSTR table_name,LPCTSTR column_name,CString script,CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!db_name.IsEmpty())
					_tcscpy_s(sDBName,127,db_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsColumnInTableInDB(sDBName,table_name,column_name))
								{															
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									bReturn = TRUE;								
								}	// if (!pDB->existsColumnInTableInDB(sDBName,table_name,column_name))
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (strcmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

BOOL runSQLScriptFileEx2(vecScriptFiles &vec,Scripts::actionTypes action)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	Scripts scripts;

	CString sScript;

	try
	{

		if (vec.size() > 0)
		{

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							for (UINT i = 0;i < vec.size();i++)
							{
								scripts = vec[i];

								if (!scripts.getDBName().IsEmpty())
											_tcscpy_s(sDBName,127,scripts.getDBName());

								// Set database as set in sDBName; 061109 p�d
								// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d

								if (pDB->existsDatabase(sDBName))
								{
									if (action == Scripts::TBL_CREATE)
									{
										if (!pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (!pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// if (scripts._CreateTable)
									else if (action == Scripts::TBL_ALTER)
									{
										if (pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// else if (!scripts._CreateTable)
								}	// if (pDB->existsDatabase(sDBName))
							} // for (UINT i = 0;i < vec.size();i++)
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (strcmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (!script.IsEmpty())
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


void setStatusBarText(int index,LPCTSTR text)
{
	// When this view gets focus, disable the view Add toolbar button; 070308 p�d
	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		pFrame->setStatusBarText(index,text);
	}
}

void setStatusBarProgress()
{
/*
	// When this view gets focus, disable the view Add toolbar button; 070308 p�d
	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		pFrame->setStatusBarProgress();
	}
*/
}

/*
void getDiamClass(double dbh,double dcls,double *dcls_from,double *dcls_to)
{
	double fDBH = dbh/10.0;	// mm -> cm
	double fDCLS = 0.0;
	double fMaxDiam = 1000.0;	// 1000 cm (10.0 m)
	//---------------------------------------------------------
	// Try to find out the Diamterclass for each tree, and
	// Check if we have a diamterclass to work with.
	// If not set diamgerclass = 0.0; 070615 p�d
	if (dcls > 0.0 && fDBH > 0.0)
	{
		for (double fDCLS = 0.0;fDCLS < (fMaxDiam + dcls);fDCLS += dcls)
		{
			if (fDBH >= fDCLS && fDBH < (fDCLS + dcls))
			{
				*dcls_from = fDCLS;
				*dcls_to = fDCLS + dcls;
				return;
			}	// if (fDBH >= fDCLS && fDBH < fDCLS + m_recMiscData.getDiamClass())
		}	// for (double fDCLS = m_recMiscData.getDiamClass(); fDCLS < fMaxDia;fDCLS += m_recMiscData.getDiamClass())
	}
}
*/

// This method was set in CSpecieDataFormView, but moved here, so we can use it also in
// adding Transfers form a Template; 070907 p�d
void calculateTransfersByM3Fub(int spc_id,
 															 LPCTSTR fromAssort,
															 LPCTSTR toAssort,
															 double m3fub,
															 CTransaction_trakt_data recTraktData,
															 vecTransactionTraktAss &vecTraktAss)

{
	double fFromM3Fub = 0.0;			// M3Fub Volume in "From" Assortment, before transfer; 070613 p�d
	double fFromM3To = 0.0;				// M3To Volume in "From" Assortment, before transfer; 070613 p�d
	double fFromM3FubPrice = 0.0;	// Price in m3fub for "From" assortment; 070613 p�d
	double fFromM3ToPrice = 0.0;	// Price in m3to for "From" assortment; 070613 p�d
	double fFromM3FubLeft = 0.0;	// Volume left in "From" Assortment, after transfer; 070613 p�d
	
	double fToM3Fub = 0.0;				// Volume in "To" Assortment, before transfer; 070613 p�d
	double fToM3To = 0.0;					// Volume in "To" Assortment, before transfer; 070613 p�d
	double fToM3FubPrice = 0.0;		// Price in m3fub for "To" assortment; 070613 p�d
	double fToM3ToPrice = 0.0;		// Price in m3to for "To" assortment; 070613 p�d
	double fToM3FubAdded = 0.0;		// Volume left in "To" Assortment, after transfer; 070613 p�d

	double fCalcFactor = 0.0;			// "Omf�ringstal" between m3fub and m3to; 070613 p�d

	double fTransferM3Fub = 0.0;
	CString sFromAssort;
	CString sToAssort;
	CString S;

	// Get data from active record in m_wndReport1 (Transfer data); 070613 p�d
	sFromAssort = fromAssort;
	sToAssort = toAssort;
	fTransferM3Fub = m3fub;

	// Find assortment in m_vecTraktAss and matches m_recTraktData; 070613 p�d
	for (UINT j = 0;j < vecTraktAss.size();j++)
	{
		CTransaction_trakt_ass traktData = vecTraktAss[j];
		
		if (recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
				recTraktData.getTDataID() == spc_id &&		
				recTraktData.getTDataTraktID() == traktData.getTAssTraktID())
		{
			// Try to find data for "From" assortment; 070613 p�d
			if (traktData.getTAssName().Compare(sFromAssort) == 0)
			{
				// Get Volume in "From" assortment; 070613 p�d
				fFromM3Fub = traktData.getM3FUB();
				fFromM3To = traktData.getM3TO();
				fFromM3FubPrice = traktData.getPriceM3FUB();
				fFromM3ToPrice = traktData.getPriceM3TO();
				// Calculate the "Omf�ringstal" between M3Fub and M3To; 070613 p�d
				if (fFromM3Fub > 0.0 && fFromM3To > 0.0)
				{
					fCalcFactor = fFromM3To / fFromM3Fub;
				}
				// Calculate volume left in "From" assortment; 070613 p�d
				fFromM3FubLeft = fFromM3Fub - fTransferM3Fub;
				// Update M3Fub value after transfer; 070613 p�d
				vecTraktAss[j].setM3FUB(fFromM3FubLeft);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fFromM3To > 0.0)
					vecTraktAss[j].setM3TO(fFromM3FubLeft*fCalcFactor);

				// Update Price for FUB and TO; 070613 p�d
				vecTraktAss[j].setValueM3FUB(fFromM3FubLeft*fFromM3FubPrice);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fFromM3To > 0.0)
					vecTraktAss[j].setValueM3TO(fFromM3FubLeft*fCalcFactor*fFromM3ToPrice);		
/*
				S.Format("FROM %s (%s) %s\n\nfFromM3Fub %f\nfFromM3To %f\nfFromM3FubPrice %f\nfFromM3ToPrice %f\nfTransferM3Fub %f\nfFromM3FubLeft %f",
					sFromAssort,sToAssort,traktData.getTAssName(),fFromM3Fub,fFromM3To,fFromM3FubPrice,fFromM3ToPrice,fTransferM3Fub,fFromM3FubLeft);
					AfxMessageBox(S);
*/
				break;
			}	// if (transData.getFromName().Compare(traktData.getTAssName()))
		}	// if (m_recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
	}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)

	// Find assortment in m_vecTraktAss and matches m_recTraktData; 070613 p�d
	for (UINT j = 0;j < vecTraktAss.size();j++)
	{
		CTransaction_trakt_ass traktData = vecTraktAss[j];
		
		if (recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
				recTraktData.getTDataID() == spc_id &&		
				recTraktData.getTDataTraktID() == traktData.getTAssTraktID())
		{
			// Try to find data for "To" assortment; 070613 p�d
			if (traktData.getTAssName().Compare(sToAssort) == 0)
			{
				// Get Volume in "To" assortment; 070613 p�d
				fToM3Fub = traktData.getM3FUB();
				fToM3To = traktData.getM3TO();
				// Calculate volume that'll be added to "To" assortment; 070613 p�d
				fToM3FubAdded = fToM3Fub + (fFromM3Fub - fFromM3FubLeft);
				// Update M3Fub value after transfer; 070613 p�d
				vecTraktAss[j].setM3FUB(fToM3FubAdded);
				// Check if there's a price set on m3to, if not don't set volume for m3to; 070613 p�d
				if (fToM3ToPrice > 0.0)
				{
					vecTraktAss[j].setM3TO(fToM3FubAdded*fCalcFactor);
				}
				else
				{
					vecTraktAss[j].setM3TO(0.0);	// No price on m3to and not volume for m3to; 070613 p�d
				}
				// Update Price for FUB and TO; 070613 p�d
				vecTraktAss[j].setValueM3FUB(fToM3FubAdded*fFromM3ToPrice);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fToM3To > 0.0)
					vecTraktAss[j].setValueM3TO(fToM3FubAdded*fCalcFactor*fToM3ToPrice);
				break;
			}	// if (transData.getToName().Compare(traktData.getTAssName()))
		}	// if (m_recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
	}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)
}


// This method was set in CSpecieDataFormView, but moved here, so we can use it also in
// adding Transfers form a Template; 070907 p�d
void calculateTransfersByPercent(int spc_id,
																 LPCTSTR fromAssort,
																 LPCTSTR toAssort,
																 double percent,
																 CTransaction_trakt_data recTraktData,
																 vecTransactionTraktAss &vecTraktAss)
{
	double fFromM3Fub = 0.0;			// M3Fub Volume in "From" Assortment, before transfer; 070613 p�d
	double fFromM3To = 0.0;				// M3To Volume in "From" Assortment, before transfer; 070613 p�d
	double fFromM3FubPrice = 0.0;	// Price in m3fub for "From" assortment; 070613 p�d
	double fFromM3ToPrice = 0.0;	// Price in m3to for "From" assortment; 070613 p�d
	double fFromM3FubLeft = 0.0;	// Volume left in "From" Assortment, after transfer; 070613 p�d
	
	double fToM3Fub = 0.0;				// Volume in "To" Assortment, before transfer; 070613 p�d
	double fToM3To = 0.0;					// Volume in "To" Assortment, before transfer; 070613 p�d
	double fToM3FubPrice = 0.0;		// Price in m3fub for "To" assortment; 070613 p�d
	double fToM3ToPrice = 0.0;		// Price in m3to for "To" assortment; 070613 p�d
	double fToM3FubAdded = 0.0;		// Volume left in "To" Assortment, after transfer; 070613 p�d

	double fCalcFactor = 0.0;			// "Omf�ringstal" between m3fub and m3to; 070613 p�d

	double fTranferPercent = 0.0;
	CString sFromAssort;
	CString sToAssort;
	CString S;

	// Get data from active record in m_wndReport1 (Transfer data); 070613 p�d
	sFromAssort = fromAssort;
	sToAssort = toAssort;
	fTranferPercent = percent;

	// Find assortment in m_vecTraktAss and matches m_recTraktData; 070613 p�d
	for (UINT j = 0;j < vecTraktAss.size();j++)
	{
		CTransaction_trakt_ass traktAssData = vecTraktAss[j];

		if (recTraktData.getTDataID() == traktAssData.getTAssTraktDataID() && 
				recTraktData.getTDataID() == spc_id &&		
				recTraktData.getTDataTraktID() == traktAssData.getTAssTraktID())
		{
	
			// Try to find data for "From" assortment; 070613 p�d
			if (traktAssData.getTAssName().Compare(sFromAssort) == 0)
			{
				// Get Volume in "From" assortment; 070613 p�d
				fFromM3Fub = traktAssData.getM3FUB();
				fFromM3To = traktAssData.getM3TO();
				fFromM3FubPrice = traktAssData.getPriceM3FUB();
				fFromM3ToPrice = traktAssData.getPriceM3TO();

				// Calculate the "Omf�ringstal" between M3Fub and M3To; 070613 p�d
				if (fFromM3Fub > 0.0 && fFromM3To > 0.0)
				{
					fCalcFactor = fFromM3To / fFromM3Fub;
				}
				// Calculate volume left in "From" assortment; 070613 p�d
				fFromM3FubLeft = fFromM3Fub - fFromM3Fub * (fTranferPercent/100.0);
				// Update M3Fub value after transfer; 070613 p�d
				vecTraktAss[j].setM3FUB(fFromM3FubLeft);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fFromM3To > 0.0)
					vecTraktAss[j].setM3TO(fFromM3FubLeft*fCalcFactor);
		
				// Update Price for FUB and TO; 070613 p�d
				vecTraktAss[j].setValueM3FUB(fFromM3FubLeft*fFromM3FubPrice);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fFromM3To > 0.0)
					vecTraktAss[j].setValueM3TO(fFromM3FubLeft*fCalcFactor*fFromM3ToPrice);		
/*
				S.Format("FROM %s (%s) %s\n\nfFromM3Fub %f\nfFromM3To %f\nfFromM3FubPrice %f\nfFromM3ToPrice %f\ntransData.getPercent()/100.0 %f\nfFromM3FubLeft %f\nfCalcFactor %f\nvecTraktAss[j].getValueM3TO %f\nfToM3TO %f",
					sFromAssort,sToAssort,traktAssData.getTAssName(),fFromM3Fub,fFromM3To,fFromM3FubPrice,fFromM3ToPrice,fTranferPercent/100.0,fFromM3FubLeft,fCalcFactor,vecTraktAss[j].getValueM3TO(),fToM3To);
					AfxMessageBox(S);
*/
				break;
			}	// if (transData.getFromName().Compare(traktData.getTAssName()))
		}	// if (m_recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
	}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)

	// Find assortment in m_vecTraktAss and matches m_recTraktData; 070613 p�d
	for (UINT j = 0;j < vecTraktAss.size();j++)
	{
		CTransaction_trakt_ass traktData = vecTraktAss[j];
		
		if (recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
				recTraktData.getTDataID() == spc_id &&		
				recTraktData.getTDataTraktID() == traktData.getTAssTraktID())
		{
			// Try to find data for "To" assortment; 070613 p�d
			if (traktData.getTAssName().Compare(sToAssort) == 0)
			{
				// Get Volume in "To" assortment; 070613 p�d
				fToM3Fub = traktData.getM3FUB();
				fToM3To = traktData.getM3TO();
				// Calculate volume that'll be added to "To" assortment; 070613 p�d
				fToM3FubAdded = fToM3Fub + (fFromM3Fub - fFromM3FubLeft);
				// Update M3Fub value after transfer; 070613 p�d
				vecTraktAss[j].setM3FUB(fToM3FubAdded);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fToM3To > 0.0)
					vecTraktAss[j].setM3TO(fToM3FubAdded*fCalcFactor);

				// Update Price for FUB and TO; 070613 p�d
				vecTraktAss[j].setValueM3FUB(fToM3FubAdded*fFromM3ToPrice);
				// Only calculate for m3to if we have m3to volume; 070907 p�d
				if (fToM3To > 0.0)
					vecTraktAss[j].setValueM3TO(fToM3FubAdded*fCalcFactor*fToM3ToPrice);

				break;
			}	// if (transData.getToName().Compare(traktData.getTAssName()))
		}	// if (m_recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
	}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)
}

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp, BOOL bSendToEstimate)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{		
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					// #4554: Hack f�r att ID_SHOWVIEW_MSG �r olika i varje svit/modul...
					pView->SendMessage(MSG_IN_SUITE, bSendToEstimate ? ID_SHOWVIEW_MSG_UMESTIMATE : ID_SHOWVIEW_MSG, lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						// #4554: Hack f�r att ID_SHOWVIEW_MSG �r olika i varje svit/modul...
						pView->SendMessage(MSG_IN_SUITE, bSendToEstimate ? ID_SHOWVIEW_MSG_UMESTIMATE : ID_SHOWVIEW_MSG, lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}


CTransaction_elv_object* getActiveObject(void)
{
	CObjectFormView *pObjView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pObjView = pFrame->getObjectFormView();
	}
	if (pObjView != NULL)
	{
		return &pObjView->getActiveObjectRecord();
	}
	return NULL;
}

CTransaction_elv_properties* getActiveProperty(void)
{
	CPropertiesFormView *pPropView = NULL;
	CMDI1950ForrestNormFormView *pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFrame != NULL)
	{
		// Try to get the ObjectView, and collect data; 080204 p�d
		pPropView = pFrame->getPropertiesFormView();
	}
	if (pPropView != NULL)
	{
		return pPropView->getActivePropertyRecord();
	}
	return NULL;
}

CString formatData(LPCTSTR fmt,...)
{
	CString sFmt;
	va_list ap;
	va_start(ap,fmt);
	sFmt.FormatV(fmt,ap);
	va_end(ap);
	return (sFmt);
}

int SplitString(const CString& input,const CString& delimiter, CStringArray& results)
{
	CString tmp;
  int iPos = 0;
  int newPos = -1;
  int sizeS2 = delimiter.GetLength();
  int isize = input.GetLength();

  CArray<INT, int> positions;

  newPos = input.Find(delimiter, 0);

  if( newPos < 0 ) { return 0; }

  int numFound = 0;

  while( newPos >= iPos )
  {
    numFound++;
    positions.Add(newPos);
    iPos = newPos;
    newPos = input.Find (delimiter, iPos+sizeS2); //+1);
  }

  for( int i = 0; i < positions.GetSize(); i++ )
  {
    CString s;
    if( i == 0 )
		{
      s = input.Mid( i, positions[i] );
		}
    else
    {
      int offset = positions[i-1] + sizeS2;
      if( offset < isize )
      {
        if ( i == positions.GetSize() )
				{
          s = input.Mid(offset);
				}
        else if ( i > 0 )
				{
          s = input.Mid( positions[i-1] + sizeS2, 
                 positions[i] - positions[i-1] - sizeS2 );
				}
      }
    }
    if( s.GetLength() >= 0 )
      results.Add(s);
  }
  return numFound;
}


int InsertRow(CListCtrl &ctrl,int nPos,int nNoOfCols, LPCTSTR pText,...)
{
	va_list argList;
	va_start(argList, pText);

	ASSERT(nNoOfCols >= 1); // use the 'normal' Insert function with 1 argument
	int nIndex = ctrl.InsertItem(LVIF_TEXT|LVIF_STATE, nPos, pText,0,LVIS_SELECTED,0,0);
	ASSERT(nIndex != -1);
	if (nIndex < 0) return(nIndex);
	for (int i = 1; i < nNoOfCols; i++) 
	{
		LPCTSTR p = va_arg(argList,LPCTSTR);
		if (p != NULL) 
		{
			ctrl.SetItemText(nIndex, i, p);    
		}
	}
	va_end(argList);
	return(nIndex);
}

int GetSelectedItem(CListCtrl &ctrl)
{
	int nItem = -1;
  POSITION nPos = ctrl.GetFirstSelectedItemPosition();
  if (nPos)
  {
		nItem = ctrl.GetNextSelectedItem(nPos);
  }
  return nItem;
}

int GetSelectedItemByCB(CListCtrl &ctrl)
{
	CPoint pt;
	GetCursorPos(&pt);
	ScreenToClient(ctrl.GetSafeHwnd(),&pt);
	UINT uFlags = 0;
	int iItem = ctrl.HitTest(pt, &uFlags);
	UINT uFlag1 = LVHT_ONITEMSTATEICON & uFlags;
	if((LVHT_ONITEMSTATEICON & uFlags) == LVHT_ONITEMSTATEICON)
	{
	  return iItem;
	}

	return -1;	// No found
}


BOOL hitTest_X(int hit_x,int limit_x,int w_x)
{
	BOOL bHitX_OK = FALSE;
	// Check if hit, in x, is within lim�ts; 080513 p�d
	// if w_x = 0, don't check this value; 080513 p�d
	if (w_x == 0) bHitX_OK = TRUE;
	else if (w_x > 0)
	{
		bHitX_OK = (hit_x >= limit_x && hit_x <= limit_x+w_x);
	}

	return bHitX_OK;

}

// Get the directory for Object , i.e. in format <searchpath>\\ObjectID
CString getObjectDirectory(LPCTSTR obj_mapp)
{
	CString sObjectDirectory;
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (obj_mapp == _T("") || _tcslen(szDB) == 0) return FALSE;

	sObjectDirectory.Format(_T("%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),obj_mapp);
	return sObjectDirectory;

}

// Get the directory for Object, holding inventory data , i.e. in format <searchpath>\\ObjectID\\inv
CString getObjectInventoryDirectory(LPCTSTR obj_mapp)
{
	CString sObjectDirectory;
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (obj_mapp == _T("") || _tcslen(szDB) == 0) return FALSE;
	sObjectDirectory.Format(_T("%s\\%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),checkDirectoryName(obj_mapp),INVENTORY_DIR);
	return sObjectDirectory;
}

// Get the directory for Object, holding setup-files etc., i.e. in format <searchpath>\\ObjectID\\setup
CString getObjectSetupDirectory(LPCTSTR obj_mapp)
{
	CString sObjectDirectory;
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (obj_mapp == _T("") || _tcslen(szDB) == 0) return FALSE;
	sObjectDirectory.Format(_T("%s\\%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),checkDirectoryName(obj_mapp),SETUP_DATA_DIR);
	return sObjectDirectory;
}

// Get the directory for Object, holding setup-files etc., i.e. in format <searchpath>\\ObjectID\\setup
CString getObjectInfoDirectory(LPCTSTR obj_mapp)
{
	CString sObjectDirectory;
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (obj_mapp == _T("") || _tcslen(szDB) == 0) return FALSE;
	sObjectDirectory.Format(_T("%s\\%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),checkDirectoryName(obj_mapp),SETUP_INFO_DIR);
	return sObjectDirectory;
}

BOOL doIsDirectory(LPCTSTR obj_mapp)
{
	CString sObjectDirectory;
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (obj_mapp == _T("") || _tcslen(szDB) == 0) return FALSE;
	// We must to create the Inventory directory; 080204 p�d
	sObjectDirectory.Format(_T("%s\\%s\\%s"),getRegisterObjectInventoryDir(),szDB,obj_mapp);

	return isDirectory(sObjectDirectory);
}

BOOL doCreateObjectDataDirectory(LPCTSTR obj_mapp,LPCTSTR sub_dir)
{
	CString sObjectDirectory;
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (obj_mapp == _T("") || _tcslen(szDB) == 0) return FALSE;
	// We must to create the Inventory directory; 080204 p�d
	if (sub_dir == _T(""))
		sObjectDirectory.Format(_T("%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),checkDirectoryName(obj_mapp));
	else
		sObjectDirectory.Format(_T("%s\\%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),checkDirectoryName(obj_mapp),checkDirectoryName(sub_dir));
	if (!isDirectory(sObjectDirectory))
	{
		createDirectory(sObjectDirectory);
	}	// if (!isDirectory(sObjectDirectory))

	return TRUE;
}

BOOL doRenameObjectDataDirectory(LPCTSTR old_name,LPCTSTR new_name)
{
	TCHAR szDB[128];
	GetUserDBInRegistry(szDB);
	// Not a valid ObjectID; 080204 p�d
	if (old_name == _T("") || new_name == _T("") || _tcslen(szDB) == 0) return FALSE;
	CString sOldObjectDirectory;
	CString sNewObjectDirectory;
	sOldObjectDirectory.Format(_T("%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),old_name);
	sNewObjectDirectory.Format(_T("%s\\%s\\%s"),getRegisterObjectInventoryDir(),checkDirectoryName(szDB),new_name);

	int rValue=renameDirectory(sOldObjectDirectory,sNewObjectDirectory);

	if(rValue!=0)
	{ //rename failed
		CString sErrorMessage;

		if(rValue==EEXIST)
		{ //the folder/file already exists.
			sErrorMessage.Format(global_langMap[IDS_STRING6322],sNewObjectDirectory);
			::MessageBox(0,sErrorMessage,global_langMap[IDS_STRING6321],MB_OK);
		}
		else
		{ //the folder/file is locked, access denied.
			sErrorMessage.Format(global_langMap[IDS_STRING6324],sOldObjectDirectory);
			::MessageBox(0,sErrorMessage,global_langMap[IDS_STRING6323],MB_OK);
		}
		return FALSE;
	}
	return TRUE;
}

CString checkDirectoryName(LPCTSTR dir_name)
{
	CString sMap(dir_name);

	sMap.Replace(_T("\\"),_T("_"));
	sMap.Replace(_T("/"),_T("_"));
	sMap.Replace(_T(":"),_T("_"));
	sMap.Replace(_T("*"),_T("_"));
	sMap.Replace(_T("?"),_T("_"));
	sMap.Replace(_T("\""),_T("_"));
	sMap.Replace(_T("<"),_T("_"));
	sMap.Replace(_T(">"),_T("_"));
	sMap.Replace(_T("|"),_T("_"));

	return sMap;

}

BOOL getSTDReports(LPCTSTR fn,LPCTSTR add_to,int index,vecSTDReports &vec)
{
	vec.clear();
	if (fileExists(fn))
	{
		XMLShellData *pParser = new XMLShellData();
		if (pParser != NULL)
		{
			if (pParser->load(fn))
			{
				pParser->getReports(add_to,index,vec,getLangSet());
			}
			delete pParser;
		}
	}
	return (vec.size() > 0);
}

BOOL License(void)
{
	CString sLangFN;
	CString sHeadline;
	CString sNoLicenseMsg;

	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			sHeadline = xml.str(IDS_STRING10);
		}
		xml.clean();
	}

	TCHAR szBuf[MAX_PATH], szBuf2[MAX_PATH];
	GetModuleFileName(hInst, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szBuf2, NULL);

	// check if we have a valid license.
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H2202"), sHeadline, (bShowLicenseDialog ? _T("1") : _T("0")),&szBuf2);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	// Check if there's a license-dll present; 090119 p�d
	if (_tcscmp(msg.getFunc(),_T("CheckLicense")) == 0)
	{
		// Commented out 2009-04-02 P�D, NOT USED
		//AfxMessageBox(sNoLicenseMsg);
		return FALSE;
	}	// if (_tcscmp(msg.getFunc(),_T("CheckLicense")) == 0)

	if (bShowLicenseDialog)
		bShowLicenseDialog = FALSE;

	return (_tcscmp(msg.getFunc(), _T("1")) == 0);	// License ok; 081210 p�d

}


void setEditWindow(CMyExtEdit *ed,COLORREF disable_color,BOOL enabled)
{
	if (ed != NULL)
	{
		ed->SetDisabledColor(BLACK,disable_color);
		ed->EnableWindow(enabled);
		ed->SetReadOnly(!enabled);
	}
}

void setEditWindow(CMyMaskExtEdit *ed,COLORREF disable_color,BOOL enabled)
{
	if (ed != NULL)
	{
		ed->SetDisabledColor(BLACK,disable_color);
		ed->EnableWindow(enabled);
		ed->SetReadOnly(!enabled);
	}
}

void setEditWindowEx(CMyExtEdit *ed,COLORREF disable_color,BOOL enabled)
{
	if (ed != NULL)
	{
		ed->SetDisabledColor(BLACK,disable_color);
		ed->EnableWindow(enabled);
		ed->SetReadOnly(TRUE);
	}
}

void setCBoxWindow(CMyComboBox *cb,COLORREF disable_color,BOOL enabled)
{
	if (cb != NULL)
	{
		cb->SetDisabledColor(BLACK,disable_color);
		cb->EnableWindow(enabled);
		cb->SetReadOnly(!enabled);
	}
}

// Added 2009-05-06 P�D
void getTemplateMainSettings(TemplateParser *pars,int *hgt_over_sea,
																						 int *latitude,
																						 int* longitude,
																						 LPTSTR growth_area,
																						 LPTSTR si_h100,
																						 double *corr_fac,
																						 int *costs_id,
																						 LPTSTR costs_tmpl_name,
																						 bool *is_at_coast,
																						 bool *is_south_east,
																						 bool *is_region5,
																						 bool *is_part_of_plot,
																						 LPTSTR si_h100_pine)
{
	int nValue;
	if (pars != NULL)
	{
		pars->getTemplateHgtOverSea(hgt_over_sea);
		pars->getTemplateLatitude(latitude);
		pars->getTemplateLongitude(longitude);
		pars->getTemplateGrowthArea(growth_area);
		pars->getTemplateSI_H100(si_h100);
		pars->getTemplateCostsTmpl(costs_id,costs_tmpl_name);
		pars->getIsAtCoast(&nValue);
		*is_at_coast = (nValue == 1 ? TRUE : FALSE);

		pars->getIsSouthEast(&nValue);
		*is_south_east = (nValue == 1 ? TRUE : FALSE);

		pars->getIsRegion5(&nValue);
		*is_region5 = (nValue == 1 ? TRUE : FALSE);

		pars->getPartOfPlot(&nValue);
		*is_part_of_plot = (nValue == 1 ? TRUE : FALSE);

		pars->getSIH100_Pine(si_h100_pine);
	}
}

// Added 2009-05-06 P�D
void getTemplateSettings(TemplateParser *pars,int *exch_id,LPTSTR exch,int *prl_id,
												 int *prl_type_of,LPTSTR prl_name,double *dcls)
{
	if (pars != NULL)
	{
		pars->getTemplateExchange(exch_id,exch);
		pars->getTemplatePricelist(prl_id,prl_name);
		pars->getTemplatePricelistTypeOf(prl_type_of);
		pars->getTemplateDCLS(dcls);
	}
}

//----------------------------------------------------------------------------
// OBS! These methods also in UMTemplates; 070903 p�d
// Added 2009-05-06 P�D
void getTemplateSpecies(TemplateParser *pars,vecTransactionSpecies &vec)
{
	if (pars != NULL)
	{
		pars->getSpeciesInTemplateFile(vec);
	}
}
// Added 2009-05-06 P�D
void getTemplateFunctionsPerSpcecie(TemplateParser *pars,int spc_id,vecUCFunctionList &vec,double *m3fub_m3to,double *m3sk_m3ub_const)
{
	if (pars != NULL)
	{
		pars->getFunctionsForSpecie(spc_id,vec,m3fub_m3to,m3sk_m3ub_const);
	}
}

void getTemplateGrotDataPerSpcecie(TemplateParser *pars,int spc_id,int *func_id,double *percent,double *price,double *costs)
{
	if (pars != NULL)
	{
		pars->getGrotDataForSpecie(spc_id,func_id,percent,price,costs);
	}
}

// Added 2009-05-06 P�D
void getTemplateMiscFunctionsPerSpcecie(TemplateParser *pars,int spc_id,int *qdesc_index,LPTSTR qdesc,
																				double *sk_fub,double *fub_to,double *h25,int *transp_dist1,
																				int *transp_dist2,double *gcrown)
{
	if (pars != NULL)
	{
		pars->getMiscDataForSpecie(spc_id,qdesc_index,qdesc,sk_fub,fub_to,h25,transp_dist1,transp_dist2,gcrown);
	}
}

void getTemplateTransfersPerSpcecie(TemplateParser *pars,int spc_id,vecTransactionTemplateTransfers &vec)
{
	if (pars != NULL)
	{
		pars->getTransfersForSpecie(spc_id,vec);
	}
}

void parseTGL(LPCTSTR tgl,int* pine_percent,int* spruce_percent,int* birch_percent)
{
	CString sTGL(tgl),sToken;
	CStringArray sarrTokens;
	int nCurPos = 0;
	*pine_percent = 0;
	*spruce_percent = 0;
	*birch_percent = 0;
	if (!sTGL.IsEmpty())
	{
		if (sTGL.Left(sTGL.GetLength()) != _T(";")) sTGL += _T(";");
		sToken = sTGL.Tokenize(_T(";"),nCurPos);
		while (!sToken.IsEmpty())
		{
			sarrTokens.Add(sToken);
			sToken = sTGL.Tokenize(_T(";"),nCurPos);
		}
		if (sarrTokens.GetCount() >= 3)
		{
			*pine_percent = _tstoi(sarrTokens.GetAt(0));
			*spruce_percent = _tstoi(sarrTokens.GetAt(1));
			*birch_percent = _tstoi(sarrTokens.GetAt(2));
		}
	}
}

void calculateStormDry_1950(double pine_percent,
							double spruce_percent,
							double birch_percent,
							vecObjectTemplate_p30_table& p30,
							double volume_m3sk,
							LPCTSTR h100,
							LPCTSTR growth_area,
							double percent,
							double* spruce_mix,
							double* st_value,
							double* st_volume,
							double *fStormTorkInfo_SpruceMix,
							double *fStormTorkInfo_SumM3Sk_inside,
							double *fStormTorkInfo_AvgPriceFactor,
							double *fStormTorkInfo_TakeCareOfPerc,
							double *fStormTorkInfo_PineP30Price,
							double *fStormTorkInfo_SpruceP30Price,
							double *fStormTorkInfo_BirchP30Price,
							double *fStormTorkInfo_CompensationLevel,
							double *fStormTorkInfo_Andel_Pine,
							double *fStormTorkInfo_Andel_Spruce,
							double *fStormTorkInfo_Andel_Birch,
							double *fStormTorkInfo_WideningFactor)
{
// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(double pine_percent,
																		double spruce_percent,
																		double birch_percent,
																		vecObjectTemplate_p30_table&,
																		double,
																		LPCTSTR,
																		LPCTSTR,
																		double,
																		double*,
																		double*,
																		double*);
  DO_CALC doCalcProc;
	CString sFN;
			CString S;
	HINSTANCE hInst = NULL;
	double fSpruceMix = 0.0;
	double fSTValue = 0.0;
	double fSTVolume = 0.0;

	sFN.Format(_T("%s%s"),getModulesDir(),LANDVALUE_NORM_FN);
	if (fileExists(sFN))
	{
		hInst = AfxLoadLibrary(sFN);
		if (hInst)
		{

			doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hInst, LANDVALUE_CALC_1950);

			if (doCalcProc)
				doCalcProc(pine_percent,spruce_percent,birch_percent,p30,volume_m3sk,h100,growth_area,percent,&fSpruceMix,&fSTValue,&fSTVolume);

			::FreeLibrary(hInst);
		}
		*spruce_mix = fSpruceMix;
		*st_value = fSTValue;
		*st_volume = fSTVolume;


	}
}

void calculateStormDry_2018(double recalc_by,
														double pine_percent,
														double spruce_percent,
														double birch_percent,
														double volume_m3sk,
														vecObjectTemplate_p30_nn_table p30_nn,
														CString& h100,
														short wside,
														double present_width,
														double new_width,
														double* spruce_mix,
														double* st_value,
														double* st_volume,
														bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor)
{

// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(double, // Recalc by
																		double,// pine_percent,
																		double,// spruce_percent,
																		double,// birch_percent,
																		double,// volume_m3sk,
																		vecObjectTemplate_p30_nn_table,// p30_nn,
																		CString&,// h100,
																		short wside,
																	  double present_width,
																	  double new_width,
																		double*,// spruce_mix,
																		double*,// st_value,
																		double*,// st_volume
																		bool,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *);
  DO_CALC doCalcProc;
	CString sFN;
			CString S;
	HINSTANCE hInst = NULL;
	double fSpruceMix = 0.0;
	double fSTValue = 0.0;
	double fSTVolume = 0.0;

	sFN.Format(_T("%s%s"),getModulesDir(),LANDVALUE_NORM_FN);
	if (fileExists(sFN))
	{
		hInst = AfxLoadLibrary(sFN);
		if (hInst)
		{

			doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hInst, (LANDVALUE_CALC_2018));

			if (doCalcProc)
				doCalcProc(recalc_by,pine_percent,spruce_percent,birch_percent,volume_m3sk,p30_nn,h100,wside,present_width,new_width,&fSpruceMix,&fSTValue,&fSTVolume,is_widening,
																fStormTorkInfo_SpruceMix,
																fStormTorkInfo_SumM3Sk_inside,
																fStormTorkInfo_AvgPriceFactor,
																fStormTorkInfo_TakeCareOfPerc,
																fStormTorkInfo_PineP30Price,
																fStormTorkInfo_SpruceP30Price,
																fStormTorkInfo_BirchP30Price,
																fStormTorkInfo_CompensationLevel,
																fStormTorkInfo_Andel_Pine,
																fStormTorkInfo_Andel_Spruce,
																fStormTorkInfo_Andel_Birch,
																fStormTorkInfo_WideningFactor);	

			::FreeLibrary(hInst);
		}
		*spruce_mix = fSpruceMix;
		*st_value = fSTValue;
		*st_volume = fSTVolume;
	}
}

void calculateStormDry_2009(double recalc_by,
														double pine_percent,
														double spruce_percent,
														double birch_percent,
														double volume_m3sk,
														vecObjectTemplate_p30_nn_table p30_nn,
														CString& h100,
														short wside,
														double present_width,
														double new_width,
														double* spruce_mix,
														double* st_value,
														double* st_volume,
														bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor)
{

// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(double, // Recalc by
																		double,// pine_percent,
																		double,// spruce_percent,
																		double,// birch_percent,
																		double,// volume_m3sk,
																		vecObjectTemplate_p30_nn_table,// p30_nn,
																		CString&,// h100,
																		short wside,
																	  double present_width,
																	  double new_width,
																		double*,// spruce_mix,
																		double*,// st_value,
																		double*,// st_volume
																		bool,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *,
																double *);
  DO_CALC doCalcProc;
	CString sFN;
			CString S;
	HINSTANCE hInst = NULL;
	double fSpruceMix = 0.0;
	double fSTValue = 0.0;
	double fSTVolume = 0.0;

	sFN.Format(_T("%s%s"),getModulesDir(),LANDVALUE_NORM_FN);
	if (fileExists(sFN))
	{
		hInst = AfxLoadLibrary(sFN);
		if (hInst)
		{

			doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hInst, (LANDVALUE_CALC_2009));

			if (doCalcProc)
				doCalcProc(recalc_by,pine_percent,spruce_percent,birch_percent,volume_m3sk,p30_nn,h100,wside,present_width,new_width,&fSpruceMix,&fSTValue,&fSTVolume,is_widening,
																fStormTorkInfo_SpruceMix,
																fStormTorkInfo_SumM3Sk_inside,
																fStormTorkInfo_AvgPriceFactor,
																fStormTorkInfo_TakeCareOfPerc,
																fStormTorkInfo_PineP30Price,
																fStormTorkInfo_SpruceP30Price,
																fStormTorkInfo_BirchP30Price,
																fStormTorkInfo_CompensationLevel,
																fStormTorkInfo_Andel_Pine,
																fStormTorkInfo_Andel_Spruce,
																fStormTorkInfo_Andel_Birch,
																fStormTorkInfo_WideningFactor);	

			::FreeLibrary(hInst);
		}
		*spruce_mix = fSpruceMix;
		*st_value = fSTValue;
		*st_volume = fSTVolume;
	}
}


void calculateCorrFactor_1950(double volume,
															double pine_percent,
															double spruce_percent,
															double birch_percent,
															vecObjectTemplate_p30_table& p30,
															LPCTSTR h100,
															int age,
															int growth_area,
															double *corr_factor)
{
// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(double,			// Virkesf�rr�d
																		double,			// Andel Tall
																		double,			// Andel Gran
																		double,			// Andel Bj�rk (L�v)
																		vecObjectTemplate_p30_table&,		// P30-priser
																		LPCTSTR,		// SI
																		int,				// �lder
																		int,				// Tillv�xtomr�de
																		double*);	// corr_factor);
  DO_CALC doCalcProc;
	CString sFN;
	CString S;
	HINSTANCE hInst = NULL;
	double fCorrFactor = 0.0;

	sFN.Format(_T("%s%s"),getModulesDir(),LANDVALUE_NORM_FN);
	if (fileExists(sFN))
	{
		hInst = AfxLoadLibrary(sFN);
		if (hInst)
		{

			doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hInst, (LANDVALUE_CORR_FAC_1950));

			if (doCalcProc)
				doCalcProc(volume,pine_percent,spruce_percent,birch_percent,p30,h100,age,growth_area,&fCorrFactor);
			*corr_factor = fCorrFactor;

			::FreeLibrary(hInst);
		}
		*corr_factor = fCorrFactor;
	}
}

void calculateCorrFactor_2009(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor)
{
// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(double,		// volume
																		LPCTSTR,	// SI
																		int,			// age,
																		int,			// growth_area
																		double*);	// corr_factor);
  DO_CALC doCalcProc;
	CString sFN;
	CString S;
	HINSTANCE hInst = NULL;
	double fCorrFactor = 0.0;

	sFN.Format(_T("%s%s"),getModulesDir(),LANDVALUE_NORM_FN);
	if (fileExists(sFN))
	{
		hInst = AfxLoadLibrary(sFN);
		if (hInst)
		{

			doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hInst, (LANDVALUE_CORR_FAC_2009));

			if (doCalcProc)
				doCalcProc(volume,h100,age,growth_area,&fCorrFactor);	

			::FreeLibrary(hInst);
		}
		*corr_factor = fCorrFactor;
	}
}

void calculateCorrFactor_2018(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor)
{
// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(double,		// volume
																		LPCTSTR,	// SI
																		int,			// age,
																		int,			// growth_area
																		double*);	// corr_factor);
  DO_CALC doCalcProc;
	CString sFN;
	CString S;
	HINSTANCE hInst = NULL;
	double fCorrFactor = 0.0;

	sFN.Format(_T("%s%s"),getModulesDir(),LANDVALUE_NORM_FN);
	if (fileExists(sFN))
	{
		hInst = AfxLoadLibrary(sFN);
		if (hInst)
		{

			doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hInst, (LANDVALUE_CORR_FAC_2018));

			if (doCalcProc)
				doCalcProc(volume,h100,age,growth_area,&fCorrFactor);	

			::FreeLibrary(hInst);
		}
		*corr_factor = fCorrFactor;
	}
}

// Optimized version of canWeCalculateThisProp_cached by Peter
BOOL canWeCalculateThisProp_cached(int status, bool refresh /*=false*/)
{
	static vecTransaction_property_status vec;
	int nTest=0;
	if( refresh || vec.empty() ) // auto-refresh if we have no values
	{
		// refresh cached values from db
		vec.clear();
		if (global_pDB != NULL)
			global_pDB->getPropertyActionStatus(vec);
	}

	// check status
	if (vec.size() == 0) return TRUE;	// We can recalculate
	for (UINT i = 0;i < vec.size();i++)
	{
		if (vec[i].getPropStatusID_pk() == status)
		{
			nTest=vec[i].getPropStatusType();
			return (nTest == 1);
		}
	}

	return FALSE;
}

BOOL canWeCalculateThisProp(int status)
{	
	vecTransaction_property_status vec;
	if (global_pDB != NULL)
		global_pDB->getPropertyActionStatus(vec);
	if (vec.size() == 0) return TRUE;	// We can recalculate
	for (UINT i = 0;i < vec.size();i++)
	{
		if (vec[i].getPropStatusID_pk() == status)
			return (vec[i].getPropStatusType() == 1);
	}

	return FALSE;
}

void getPropActionStatusBkgndColor(int status,short* red,short* green,short* blue)
{
	vecTransaction_property_status vec;
	if (global_pDB != NULL)
		global_pDB->getPropertyActionStatus(vec);

	// Set default values = WHITE; 090916 p�d
	*red = 255;
	*green = 255;
	*blue = 255;

	if (vec.size() == 0) return ;

	for (UINT i = 0;i < vec.size();i++)
	{
		if (vec[i].getPropStatusID_pk() == status)
		{
			*red = vec[status].getPropStatusRCol();
			*green = vec[status].getPropStatusGCol();
			*blue = vec[status].getPropStatusBCol();
			break;
		}
	}
}



void setRegisterLastObjectVisited(int obj_id,LPCTSTR db_name)
{
	AfxGetApp()->WriteProfileInt(REG_LAST_OBJECT_VISITED_KEY,REG_LAST_OBJECT_VISITED_ITEM,obj_id);
	AfxGetApp()->WriteProfileString(REG_LAST_OBJECT_VISITED_DB_KEY,REG_LAST_OBJECT_VISITED_DB_ITEM,db_name);
}

void getRegisterLastObjectVisited(int *obj_id,CString& db_name)
{
	int nObjID = -1;
	CString sDBName;
	nObjID = AfxGetApp()->GetProfileInt(REG_LAST_OBJECT_VISITED_KEY,REG_LAST_OBJECT_VISITED_ITEM,-1);
	sDBName = AfxGetApp()->GetProfileString(REG_LAST_OBJECT_VISITED_DB_KEY,REG_LAST_OBJECT_VISITED_DB_ITEM,_T(""));
}


void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}


CString getDocumentsDir(int folder )
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
															 folder, 
															 NULL, 
															 0, 
															 szPath))) 
	{
		sPath.Format(_T("%s"),szPath);
		return sPath;
	}

	return _T("");
}


BOOL logThis(int item)
{
	CString sSettings = L"";
	BOOL sItems[5];
	BOOL bRet = FALSE;
	CString sPath = _T("");
	int nPos = 0;
	sPath.Format(L"%s\\%s",getDocumentsDir(CSIDL_COMMON_DOCUMENTS),LOG_SETTINGS_FILE);
	if (fileExists(sPath))
	{

		CStdioFile f;
		if (f.Open(sPath, CFile::modeRead | CFile::typeText))
		{
			f.ReadString(sSettings);
			if (!sSettings.IsEmpty())
			{
				sItems[0] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[1] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[2] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[3] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[4] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
			}
			f.Close();
		}
	}

	if (item >= 0 && item < 5)
		return sItems[item];
	else
		return FALSE;
}

int TwipsPerPixel()
{
	HWND hWnd = ::GetDesktopWindow();
	HDC hDC = ::GetDC(hWnd);
	const int logPixY = ::GetDeviceCaps(hDC, LOGPIXELSY);
	::ReleaseDC(hWnd, hDC);
	return 1440 / logPixY; 
}
