// HandleSetupFilesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HandleSetupFilesDlg.h"

#include "ResLangFileReader.h"

#include "StdioFileEx.h"

//#include <mapi.h>
//#include "IMapi.h"

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

	BOOL IsNatural(const CString Str)
	{
		// Check each character of the string
		// If a character at a certain position is not a digit,
		// then the string is not a valid natural number
		for(int i = 0; i < Str.GetLength(); i++)
		{
			if( Str[i] < '0' || Str[i] > '9' )
			return FALSE;
		}

		return TRUE;
	}

	BOOL IsNumeric(CString Str)
	{
		// Make a copy of the original string to test it
		CString WithoutSeparator = Str;
		// Prepare to test for a natural number
		// First remove the decimal separator, if any
		WithoutSeparator.Replace(L".", L"");

		// If this number were natural, test it
		// If it is not even a natural number, then it can't be valid
		if( IsNatural(WithoutSeparator) == FALSE )
			return FALSE; // Return Invalid Number

		// Set a counter to 0 to counter the number of decimal separators
		int NumberOfSeparators = 0;

		// Check each charcter in the original string
		for(int i = 0; i < Str.GetLength(); i++)
		{
			// If you find a decimal separator, count it
			if( Str[i] == '.' )
				NumberOfSeparators++;
		}

		// After checking the string and counting the decimal separators
		// If there is more than one decimal separator,
		// then this cannot be a valid number
		if( NumberOfSeparators > 1 )
			return FALSE; // Return Invalid Number
		else // Otherwise, this appears to be a valid decimal number
			return TRUE;
	}

// CHandleSetupFilesDlg dialog

IMPLEMENT_DYNAMIC(CHandleSetupFilesDlg, CDialog)

BEGIN_MESSAGE_MAP(CHandleSetupFilesDlg, CDialog)
// Uncomment these lines, to set Setup-filename in editbox; 080618 p�d
//	ON_NOTIFY(NM_CLICK, ID_SETUP_FILE, OnReportItemClick)
//	ON_NOTIFY(NM_KEYDOWN, ID_SETUP_FILE, OnReportKeyDown)
	ON_BN_CLICKED(IDC_BUTTON2, &CHandleSetupFilesDlg::OnBnClickedButton2)
//	ON_BN_CLICKED(IDC_BUTTON4, &CHandleSetupFilesDlg::OnBnClickedButton4)
//	ON_BN_CLICKED(IDC_BUTTON5, &CHandleSetupFilesDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON3, &CHandleSetupFilesDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON1, &CHandleSetupFilesDlg::OnBnClickedAddToListCtrl)
	ON_BN_CLICKED(IDC_BUTTON4, &CHandleSetupFilesDlg::OnBnClickedAddAllToListCtrl)
	ON_BN_CLICKED(IDC_BUTTON5, &CHandleSetupFilesDlg::OnBnClickedRemoveFromListCtrl)
	ON_BN_CLICKED(IDC_BUTTON6, &CHandleSetupFilesDlg::OnBnClickedRemoveAllFromListCtrl)
	ON_EN_CHANGE(IDC_EDIT1, &CHandleSetupFilesDlg::OnEnChangeSetupFNChange)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST2, &CHandleSetupFilesDlg::OnLvnItemchangedList2)
END_MESSAGE_MAP()

CHandleSetupFilesDlg::CHandleSetupFilesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHandleSetupFilesDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CHandleSetupFilesDlg::~CHandleSetupFilesDlg()
{
	m_vecTransaction_elv_properties.clear();
	m_sarrSetupFile.RemoveAll();
	m_sarrSetupFileList.RemoveAll();
	m_mapPropInSetupFile.clear();
	m_mapPropInSetupFileDateTime.clear();
}

void CHandleSetupFilesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL_SETUP1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_SETUP2, m_wndLbl2);

	DDX_Control(pDX, IDC_EDIT1, m_wndStupFileName);

	DDX_Control(pDX, IDC_LIST2, m_wndLCtrl);

	DDX_Control(pDX, IDC_BUTTON2, m_wndSave);
	DDX_Control(pDX, IDC_BUTTON3, m_wndRemoveSetup);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CHandleSetupFilesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING5820)));
			
			m_wndLbl1.SetWindowText((xml.str(IDS_STRING5821)));
			m_wndLbl2.SetWindowText((SETUP_FILE_DEF_EXT));

			m_wndSave.SetWindowText((xml.str(IDS_STRING5822)));
//			m_wndToCaliper.SetWindowText((xml.str(IDS_STRING5823)));
//			m_wndEMail.SetWindowText((xml.str(IDS_STRING5824)));
			m_wndRemoveSetup.SetWindowText((xml.str(IDS_STRING5826)));
			m_wndCancelBtn.SetWindowText((xml.str(IDS_STRING5825)));

			m_sMsgCap = (xml.str(IDS_STRING229));
			m_sMsgFileNameMissing = (xml.str(IDS_STRING58263));
			m_sMsgReplaceFile.Format(_T("%s\n\n%s\n"),
				(xml.str(IDS_STRING58264)),
				(xml.str(IDS_STRING58265)));
			m_sMsgRemoveSetupFile = (xml.str(IDS_STRING58266));
			m_sMsgEMailSubject = (xml.str(IDS_STRING58267));
			m_sMsgEMailText = (xml.str(IDS_STRING58268));

			m_sMsgDuplcatePropInSetup1.Format(_T("%s\n%s"),
				(xml.str(IDS_STRING5827)),
				(xml.str(IDS_STRING5828)));

			m_sMsgDuplcatePropInSetup2.Format(_T("%s\n\n%s"),
				(xml.str(IDS_STRING5829)),
				(xml.str(IDS_STRING5830)));

			xml.clean();
		}
	}

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndLCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_Header.SubclassWindow(hWndHeader);
	m_Header.SetTheme(new CXTHeaderCtrlThemeOffice2003());
	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	setupReport();
	getPropertiesInObject();
	getSetupFiles();
	populateData();

	m_wndStupFileName.SetDisabledColor(BLACK,COL3DFACE);
	m_wndStupFileName.SetEnabledColor(BLACK,WHITE);

	m_wndSave.EnableWindow(FALSE);

	m_wndStupFileName.EnableWindow(FALSE);
	m_wndStupFileName.SetReadOnly();

	CWnd *pWnd = GetDlgItem(IDC_BUTTON5);
	if (pWnd != NULL) pWnd->EnableWindow(FALSE);


	return TRUE;
}

void CHandleSetupFilesDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	// Need to set size of Report control; 051219 p�d
	RECT rect;
	GetClientRect(&rect);

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_SETUP_FILE, FALSE, FALSE))
		{
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			if (m_wndReport.GetSafeHwnd() == NULL)
			{
				return;
			}
			else
			{	
				m_wndReport.ShowWindow(SW_NORMAL);
				m_wndReport.ShowGroupBy( TRUE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING3020)), 80));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING3018)), 80));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING3001)), 80));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING58260)), 100));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING58261)), 100));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING58262)), 80));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				//#3725: Coordinate in a hidden column
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml.str(IDS_STRING58262)), 80));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetVisible(false);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.GetRecords()->SetCaseSensitive(FALSE);

				setResize(GetDlgItem(ID_SETUP_FILE),10,10,465,rect.bottom - 55);
			}


			if (m_wndLCtrl.GetSafeHwnd())
			{
				// Need to set size of Report control; 071109 p�d
				CWnd *pWnd = GetDlgItem(IDC_LIST2);
				if (pWnd != NULL)
				{
					int tpp = TwipsPerPixel();
					pWnd->SetWindowPos(&CWnd::wndTop,530*15/tpp,6*15/tpp,220*15/tpp,(rect.bottom-47)*15/tpp,SWP_SHOWWINDOW);
				}
				m_wndLCtrl.InsertColumn(0, (xml.str(IDS_STRING58269)), LVCFMT_LEFT, 220, 0);		// "Fastighet inkuderad i Setup-fil"; 090424 p�d
				m_wndLCtrl.InsertColumn(1, _T(""), LVCFMT_LEFT, 220, 1);		//#3725 Coordinate
			}	// if (m_wndLCtrl.GetSafeHwnd())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))
}

void CHandleSetupFilesDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	CXTPReportRow *pRow = NULL;
	CHandleSetupFileRec *pRec = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	CString	sPathAndFileName;
	CString sFileName;
	CString sObjMapp;
	if (pItemNotify != NULL)
	{
		pRow = pItemNotify->pRow;
		if (pRow != NULL)
		{	
			pRec = (CHandleSetupFileRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				sFileName = pRec->getColumnText(COLUMN_3);
				if (!sFileName.IsEmpty())
				{
					sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
					sObjMapp = checkDirectoryName(sObjMapp);
					sPathAndFileName.Format(_T("%s\\%s"),getObjectSetupDirectory(sObjMapp),sFileName);
					m_wndStupFileName.SetWindowText((extractFileNameNoExt(sPathAndFileName)));
				}
				else
					m_wndStupFileName.SetWindowText(_T(""));

				pRec = NULL;
			}	// if (pRec != NULL)
			pRow = NULL;
			pObj = NULL;
		}	// if (pRow != NULL)
	}	// if (pItemNotify != NULL)
}

void CHandleSetupFilesDlg::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CHandleSetupFileRec *pRec = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	CString	sPathAndFileName;
	CString sFileName;
	CString sObjMapp;
	// Only on up/down arrow keys; 080428 p�d
	if (lpNMKey->nVKey == VK_UP || lpNMKey->nVKey == VK_DOWN)
	{
		if (pRow != NULL)
		{	
			pRec = (CHandleSetupFileRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				sFileName = pRec->getColumnText(COLUMN_3);
				if (!sFileName.IsEmpty())
				{
					sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
					sObjMapp = checkDirectoryName(sObjMapp);
					sPathAndFileName.Format(_T("%s\\%s"),getObjectSetupDirectory(sObjMapp),sFileName);
					m_wndStupFileName.SetWindowText((extractFileNameNoExt(sPathAndFileName)));
				}
				else
					m_wndStupFileName.SetWindowText(_T(""));

				pRec = NULL;
			}	// if (pRec != NULL)
			pRow = NULL;
			pObj = NULL;
		}	// if (pRow != NULL)
	}	// if (lpNMKey->nVKey == VK_UP || lpNMKey->nVKey == VK_DOWN)
}

void CHandleSetupFilesDlg::getPropertiesInObject(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_nObjID,m_vecTransaction_elv_properties);
	}	// if (m_pDB != NULL)
}

void CHandleSetupFilesDlg::getSpeciesInPricelist(void)
{
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(m_sPricelistXML))
		{
			pPrlParser->getSpeciesInPricelistFile(vecSpc);
		}
		delete pPrlParser;
	}
/*
	PricelistParser prlPars;
	if (prlPars.LoadFromBuffer(m_sPricelistXML))
	{
			prlPars.getSpeciesInPricelistFile(vecSpc);
	}	// if (prlPars.LoadFromBuffer(m_sarrSetupFile))
*/
}

void CHandleSetupFilesDlg::populateData(void)
{
	int nStatusIndex = -1;
	CString sStatus;
	BOOL bIsNumeric = TRUE;
	BOOL bIsNumeric2 = TRUE;
	// set properties for Object; 080613 p�d
	if (m_vecTransaction_elv_properties.size() > 0)
	{
		// Determin if property-number is numeric of alphanumeric
		for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		{
			m_recElvProps = m_vecTransaction_elv_properties[i];
			if (!IsNumeric(m_recElvProps.getPropNumber()) || m_recElvProps.getPropNumber().IsEmpty())
			{
				bIsNumeric = FALSE;
				break;
			}

		}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)

		// Determin if GroupID-number is numeric of alphanumeric
		for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		{
			m_recElvProps = m_vecTransaction_elv_properties[i];
			if (!IsNumeric(m_recElvProps.getPropGroupID()) || m_recElvProps.getPropGroupID().IsEmpty())
			{
				bIsNumeric2 = FALSE;
				break;
			}

		}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)


		m_wndReport.ResetContent();
		readSetupFile();
		for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		{
			m_recElvProps = m_vecTransaction_elv_properties[i];

			m_wndReport.AddRecord(new CHandleSetupFileRec(i,m_recElvProps.getPropName(),
																										m_mapPropInSetupFile[m_recElvProps.getPropName()],
																										m_mapPropInSetupFileDateTime[m_recElvProps.getPropName()],
																										m_recElvProps,
																										bIsNumeric,
																										bIsNumeric2));
		}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
		m_wndReport.Populate();
//		m_wndReport.UpdateWindow();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
	}	// m_vecTransaction_elv_properties
}

BOOL CHandleSetupFilesDlg::createSetupFile(void)
{
	CString sDate;
	CString sSpcIDs;
	CHandleSetupFileRec *pRec = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	CXTPReportSelectedRows *pRows = m_wndReport.GetSelectedRows();
	CString sSetupFileName;
	CString sMsgProps;
	CString sMsg;
	BOOL bIsDonNotAdd = FALSE;
	vecTransactionSampleTreeCategory m_vecSmpTreeCategories;
	//�ndrat, kollar p� listan med valda fastigheter till h�ger ist�llet f�r att g� igenom urpsrungslistan...
	//20111005 J� Feature #2389
	if(m_wndLCtrl.GetItemCount()>=1 && pObj != NULL)
	{
		//�ndrat, kollar p� listan med valda fastigheter till h�ger ist�llet f�r att g� igenom urpsrungslistan...
		//20111005 J� Feature #2389
		if(m_wndLCtrl.GetItemCount()>=1)
		{
			// Create path and setup-filename; 080618 p�d
			sSetupFileName = m_wndStupFileName.getText();
			// Always add default fileextension to setup-file-name; 080618 p�d
			sSetupFileName += (SETUP_FILE_DEF_EXT);	// Default extension

			// Read setup-file(s) to have something to compare with; 080618 p�d
			readSetupFile();
			// Check if selected properties, already included in an other
			// Setup-file; 080618 p�d
			sMsgProps.Empty();

			//�ndrat, kollar p� listan med valda fastigheter till h�ger ist�llet f�r att g� igenom urpsrungslistan...
			//20111005 J� Feature #2389
			for (int i = 0;i < m_wndLCtrl.GetItemCount();i++)
			{
				//�ndrat, kollar p� listan med valda fastigheter till h�ger ist�llet f�r att g� igenom urpsrungslistan...
				//20111005 J� Feature #2389
				if (!m_mapPropInSetupFile[m_wndLCtrl.GetItemText(i,0)].IsEmpty() &&
						isInAddToSetupList(m_wndLCtrl.GetItemText(i,0)) &&
					  m_wndLCtrl.GetItemText(i,0).CompareNoCase(sSetupFileName) != 0)
				{
					sMsgProps += _T(" * ") + m_wndLCtrl.GetItemText(i,0) + _T("\n");
				}

			}	// for (int i = 0;i < pRows->GetCount();i++)
			// Selected properties already in other Setup-file; 080618 p�d
			if (!sMsgProps.IsEmpty())
			{
				sMsg = m_sMsgDuplcatePropInSetup1 + _T("\n\n");
				sMsg += sMsgProps + _T("\n\n");
				sMsg += m_sMsgDuplcatePropInSetup2 + _T("\n\n");
				if (::MessageBox(this->GetSafeHwnd(),(sMsg),(m_sMsgCap),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDNO)
				{
					m_wndSave.EnableWindow(FALSE);
					m_wndStupFileName.SetWindowText(_T(""));
					m_wndStupFileName.EnableWindow(FALSE);
					m_wndStupFileName.SetReadOnly(TRUE);
					m_wndLCtrl.DeleteAllItems();
					OnBnClickedRemoveAllFromListCtrl();
					return FALSE;
				}
			}
	
			// Read species from Pricelist in Object; 080617 p�d
			getSpeciesInPricelist();

			m_sSetupFile.Empty();

			// get first selected record, to get access to Propertyname; 080616 p�d
			if (m_wndLCtrl.GetItemCount() >= 1)
			{
				//-----------------------------------------------------------------------
				// Setup header data in setup-file; 080616 p�d

				CString csTmp, csLat, csLon;

				csTmp.Format(_T("1 3\nISO 8859-1~"));
				m_sSetupFile += csTmp;

				csTmp.Format(_T("12 4\n%s000000~"), getDate());
				m_sSetupFile += csTmp;

				if (m_wndLCtrl.GetItemCount() > 0)
				{
					for (int i = 0;i < m_wndLCtrl.GetItemCount();i++)
					{
						csTmp.Format(_T("2 1\n%s~"), m_wndLCtrl.GetItemText(i,0));
						m_sSetupFile += csTmp;

						SplitTextLatLon(m_wndLCtrl.GetItemText(i, 1), &csLat, &csLon);

						csTmp.Format(_T("2200 1 %s~"), csLat);
						m_sSetupFile += csTmp;

						csTmp.Format(_T("2201 1 %s~"), csLon);
						m_sSetupFile += csTmp;
					}
				}

				//L�gg till kategorinamn i setupfil med variabel 2206 typ 2 #4207 20141112 J�
				if (m_pDB != NULL)
				{
					m_pDB->GetCategories(m_vecSmpTreeCategories);
					if (m_vecSmpTreeCategories.size() > 0)
					{
						csTmp.Format(_T("2206 2"));
						m_sSetupFile += csTmp;
						for (UINT i = 0;i < m_vecSmpTreeCategories.size();i++)
						{
							//20141112 J� #4207 L�gg till kategorinamn till setupfil var 2206 typ 2

							csTmp.Format(_T("\n%s"),m_vecSmpTreeCategories[i].getCategory());
							m_sSetupFile += csTmp;
						}
						csTmp.Format(_T("~"));
						m_sSetupFile += csTmp;

						csTmp.Format(_T("2206 3"));
						m_sSetupFile += csTmp;
						for (UINT i = 0;i < m_vecSmpTreeCategories.size();i++)
						{
							//20141112 J� #4207 L�gg till kategorinamn till setupfil var 2206 typ 3

							csTmp.Format(_T(" %d"),m_vecSmpTreeCategories[i].getID());
							m_sSetupFile += csTmp;
						}
						csTmp.Format(_T("~"));
						m_sSetupFile += csTmp;

					}
				}

				m_sSetupFile += _T("\n");
			}
		}	// if (pRows->GetCount() > 0)

		pObj = NULL;
	}	// if (pRows != NULL && pObj != NULL)
	return TRUE;
}

BOOL CHandleSetupFilesDlg::isInAddToSetupList(LPCTSTR prop)
{
	CString sText,S;
	// Finish by adding Properties; 080616 p�d
	for (int i = 0;i < m_wndLCtrl.GetItemCount();i++)
	{
		sText = m_wndLCtrl.GetItemText(i,0);
		if (sText.CompareNoCase(prop) == 0) return TRUE;
	}	// for (int i = 0;i < pRows->GetCount();i++)
	return FALSE;
}

void CHandleSetupFilesDlg::getSetupFiles(void)
{
	CString sPath;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
		sPath = getObjectSetupDirectory(sObjMapp);
	}
	m_sarrSetupFileList.RemoveAll();
	getListOfFilesInDirectory(SETUP_FILE_WC,sPath,m_sarrSetupFileList);
}

void CHandleSetupFilesDlg::readSetupFile(void)
{
	CStdioFile file;
	CString sBuffer;
	CString sLine;
	CString sPathAndFileName;
	CString sSetupFileName;
	CString sSetupFileDateTime;
	BY_HANDLE_FILE_INFORMATION fileInfo;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
	}

	m_mapPropInSetupFile.clear();
	m_mapPropInSetupFileDateTime.clear();
	for (int i = 0;i < m_sarrSetupFileList.GetCount();i++)
	{
		sSetupFileName = m_sarrSetupFileList.GetAt(i);
		sPathAndFileName.Format(_T("%s\\%s"),getObjectSetupDirectory(sObjMapp),sSetupFileName);

		if (file.Open(sPathAndFileName,CFile::modeRead | CFile::typeText))
		{
			sBuffer.Empty();
			while (file.ReadString(sLine))
			{
				sBuffer += sLine;
			}

			file.Close();

			// Try to get some information on the SetupFile; 080617 p�d
			if (getFileInformation(sPathAndFileName,fileInfo))
			{
				convertFileDateTime(fileInfo.ftLastWriteTime,sSetupFileDateTime);
			}	// if (getFileInformation(sPathAndFileName,fileInfo))

			// Try to match Properties, in m_vecTransaction_elv_properties
			// to properties in SetupFile. Just do a find od propertyname+block+unit; 080617 p�d
			if (m_vecTransaction_elv_properties.size() > 0)
			{
				for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
				{
					m_recElvProps = m_vecTransaction_elv_properties[i];

					if (sBuffer.Find(m_recElvProps.getPropName()) > -1)
					{
						m_mapPropInSetupFile[m_recElvProps.getPropName()] = sSetupFileName;
						m_mapPropInSetupFileDateTime[m_recElvProps.getPropName()] = sSetupFileDateTime;
					}
				}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
			}	// if (m_vecTransaction_elv_properties.size() > 0)
		}	// if (f.is_open())
	}	// for (int i = 0;i < m_sarrSetupFileList.GetCount();i++)
}

BOOL CHandleSetupFilesDlg::saveSetupFile(void)
{
	CString sNameAndPath;
	CString sSetupFileName;
	CString sObjMapp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
		sObjMapp = checkDirectoryName(sObjMapp);
	}

	// Create path and setup-filename; 080616 p�d
	sSetupFileName = m_wndStupFileName.getText();
	if (sSetupFileName.IsEmpty())
	{
		::MessageBox(this->GetSafeHwnd(),(m_sMsgFileNameMissing),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
		m_wndStupFileName.SetSel(0,-1);
		m_wndStupFileName.SetFocus();
		return FALSE;
	}
	// Always add default fileextension to setup-file-name; 080617 p�d
	sSetupFileName += (SETUP_FILE_DEF_EXT);	// Default extension

	sNameAndPath.Format(_T("%s\\%s"),getObjectSetupDirectory(sObjMapp),sSetupFileName);

	// Check to see if there's already a Setup-file with this name; 080617 p�d
	if (fileExists(sNameAndPath))
	{
		if (::MessageBox(this->GetSafeHwnd(),(m_sMsgReplaceFile),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		{
			m_wndStupFileName.SetSel(0,-1);
			m_wndStupFileName.SetFocus();
			return FALSE;
		}
	}	// if (fileExists(sNameAndPath))

	CStdioFile file;
  // We can call Close() explicitly, but the destructor would have
  // also closed the file for us. Note that there's no need to
  // call the CloseHandle() on the handle returned by the API because
  // MFC will close it for us.
	if (file.Open(sNameAndPath,CFile::modeCreate | CFile::modeWrite)) // | CStdioFileEx::modeWriteUnicode))
	{
		file.WriteString(m_sSetupFile);
		file.Close();
		return TRUE;
	}
	
	return FALSE;
}

void CHandleSetupFilesDlg::OnBnClickedButton2()
{
	if (!createSetupFile()) return;
	if (saveSetupFile())
	{
		// Reload setupfiles; 080617 p�d
		getSetupFiles();
		// Repopulate report; 080617 p�d
		populateData();
		m_wndSave.EnableWindow(FALSE);
		m_wndStupFileName.SetWindowText(_T(""));
		m_wndStupFileName.EnableWindow(FALSE);
		m_wndStupFileName.SetReadOnly(TRUE);
		m_wndLCtrl.DeleteAllItems();
	}
}

void CHandleSetupFilesDlg::OnBnClickedButton3()
{
	CString sMsgRemove;
	CString sPathAndFileName;
	CString sObjMapp;
	CHandleSetupFileRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CTransaction_elv_object *pObj = getActiveObject();
	if (pRow != NULL && pObj != NULL)
	{
		pRec = (CHandleSetupFileRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			// No setupfile; 080617 p�d
			if (pRec->getColumnText(COLUMN_4).IsEmpty())
			{
				pObj = NULL;
				return;
			}	// if (pRec->getColumnText(COLUMN_3).IsEmpty())

			sMsgRemove.Format(_T("%s\n\n%s\n\n"),pRec->getColumnText(COLUMN_4),m_sMsgRemoveSetupFile);
			if (::MessageBox(this->GetSafeHwnd(),(sMsgRemove),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				sObjMapp.Format(_T("%s_%s"),pObj->getObjectName(),pObj->getObjIDNumber());
				sObjMapp = checkDirectoryName(sObjMapp);
				sPathAndFileName.Format(_T("%s\\%s"),getObjectSetupDirectory(sObjMapp),pRec->getColumnText(COLUMN_4));
				if (removeFile(sPathAndFileName))
				{
					// Reload setupfiles; 080617 p�d
					getSetupFiles();
					// Repopulate report; 080617 p�d
					populateData();
					// Clear name of setupfile; 090424 p�d
					m_wndStupFileName.SetWindowText(_T(""));		
				}	// if (removeFile(sPathAndFileName))
			}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sMsgReplaceFile),_T(m_sMsgCap),MB_ICONEXCLAMATION | | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
		}	// if (pRec != NULL)
		pObj = NULL;
	}	// if (pRow != NULL && pObj != NULL)
}
/*
void CHandleSetupFilesDlg::OnBnClickedButton3()
{
	// Create path and setup-filename; 080616 p�d
	CString	sPathAndFileName;
	CString	sSetupFileName;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		// First; Create setupfile: 080617 p�d
		if (!createSetupFile()) return;
		// Second; Save setupfile: 080617 p�d
		if (saveSetupFile())
		{
			// Create path and setup-filename; 080616 p�d
			sSetupFileName = m_wndStupFileName.getText();
			// Always add default fileextension to setup-file-name; 080617 p�d
			sSetupFileName += _T(SETUP_FILE_DEF_EXT);	// Default extension
			sPathAndFileName.Format("%s\\%s",getObjectSetupDirectory(m_nObjID),sSetupFileName);
			// Make sure the file actually exists
			if (fileExists(sPathAndFileName))
			{
				//	Tell the Communication-suite to send the file
				::SendMessage(GetParent()->GetSafeHwnd(),WM_USER_MSG_SUITE, WM_USER+4,
					(LPARAM)&_user_msg(401, _T("OpenSuiteEx"), _T("Communication.dll"), "", sPathAndFileName, ""));
				// Reload setupfiles; 080617 p�d
				getSetupFiles();
				// Repopulate report; 080617 p�d
				populateData();
			}	// if (fileExists(sPathAndFileName))
		}	// if (saveSetupFile())

		pObj = NULL;
	}	// if (pObj != NULL)
 OnOK();
}

// Send Setup-file via E-Mail; 080617 p�d
void CHandleSetupFilesDlg::OnBnClickedButton4()
{
	// Create path and setup-filename; 080616 p�d
	CString	sPathAndFileName;
	CString	sSetupFileName;
	CString sMsgText;
	// send the report
	CIMapi mail;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		if (mail.Error() == 0) //IMAPI_SUCCESS)
		{

			if (!createSetupFile()) return;
			if (saveSetupFile())
			{
				// Subject (�mne)	
				mail.Subject(_T(m_sMsgEMailSubject));

				// Create path and setup-filename; 080616 p�d
				sSetupFileName = m_wndStupFileName.getText();
				// Always add default fileextension to setup-file-name; 080617 p�d
				sSetupFileName += _T(SETUP_FILE_DEF_EXT);	// Default extension
				sPathAndFileName.Format("%s\\%s",getObjectSetupDirectory(m_nObjID),sSetupFileName);
				// Make sure the file actually exists
					AfxMessageBox(sPathAndFileName);
				if (fileExists(sPathAndFileName))
				{
					// Attached file(s)
					mail.Attach(sPathAndFileName);
				}	// if (fileExists(sPathAndFileName))			
				// "Br�dtext"
				sMsgText.Format("%s : %s",m_sMsgEMailText,getDBDateTime());
				mail.Text(_T(sMsgText));
				mail.Send();
				
				// Reload setupfiles; 080617 p�d
				getSetupFiles();
				// Repopulate report; 080617 p�d
				populateData();
			}	// if (saveSetupFile())
		}	// if (mail.Error() == 0)
		else
		{
			// Do something appropriate to the error...
			AfxMessageBox(_T("No E-Mail client")); //_T("No email-client installed!"));
		}
		pObj = NULL;
	}	// if (pObj != NULL)
}
*/

void CHandleSetupFilesDlg::OnBnClickedAddToListCtrl()
{
	LVFINDINFO info;
	int nNumOfItems = -1;
	CXTPReportSelectedRows *pRows = m_wndReport.GetSelectedRows();
	CHandleSetupFileRec *pRec = NULL;
	std::vector<CXTPReportRecord *> vecRecs;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CHandleSetupFileRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				// Check if property has Status <= STATUS_ADVICED; 090615 p�d
//				if (pRec->getRecord().getPropStatus() <= STATUS_ADVICED)
				if (canWeCalculateThisProp_cached(pRec->getRecord().getPropStatus()))
				{
					nNumOfItems =	m_wndLCtrl.GetItemCount();
					// Check to see if property already inserted; 090424 p�d
					info.flags = LVFI_STRING;
					info.psz = pRec->getColumnText(COLUMN_3);

					if (m_wndLCtrl.FindItem(&info) == -1)
					{
						InsertRow(m_wndLCtrl, nNumOfItems, 2, pRec->getColumnText(COLUMN_3), pRec->getColumnText(COLUMN_6));
					
						nNumOfItems =	m_wndLCtrl.GetItemCount();
						//Ta bort fastighet fr�n lista feature #2389 20111004 J�
						//m_wndReport.RemoveRecordEx(pRec);
						vecRecs.push_back(pRec);
					}
					//m_wndSave.EnableWindow(nNumOfItems > 0);
					m_wndStupFileName.EnableWindow(nNumOfItems > 0);
					m_wndStupFileName.SetReadOnly(!(nNumOfItems > 0));
				}	// if (pRec->getRecord().getPropStatus() <= STATUS_ADVICED)
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)

		if (vecRecs.size() > 0)
		{
			for (UINT j = 0;j < vecRecs.size();j++)
			{
				m_wndReport.RemoveRecordEx(vecRecs[j]);
			}
		}

	}
}

void CHandleSetupFilesDlg::OnBnClickedAddAllToListCtrl()
{
	int nNumOfItems = -1;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CHandleSetupFileRec *pRec = NULL;
	if (pRows != NULL )
	{
		if(pRows->GetCount()>0)
		{

			// Clear List before adding ALL items in list; 090424 p�d	
			//Ta inte bort alla utan f�r �ver de som �r kvar feature #2323 20111010 J�
			//m_wndLCtrl.DeleteAllItems();

			int i=0;
			//for (int i = 0;i < pRows->GetCount();i++)
			do
			{
				pRec = (CHandleSetupFileRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					// Check if property has Status <= STATUS_ADVICED; 090615 p�d
					//if (pRec->getRecord().getPropStatus() <= STATUS_ADVICED)
					if (canWeCalculateThisProp_cached(pRec->getRecord().getPropStatus()))
					{
						nNumOfItems =	m_wndLCtrl.GetItemCount();
						InsertRow(m_wndLCtrl, nNumOfItems, 2, pRec->getColumnText(COLUMN_3), pRec->getColumnText(COLUMN_6));

						nNumOfItems =	m_wndLCtrl.GetItemCount();
						//m_wndSave.EnableWindow(nNumOfItems > 0);
						m_wndStupFileName.EnableWindow(nNumOfItems > 0);
						m_wndStupFileName.SetReadOnly(!(nNumOfItems > 0));
						//Ta bort fastigheter fr�n lista feature #2389 20111004 J�
						m_wndReport.RemoveRecordEx(pRec);
					}
					else
						i++;
				}
			}while(pRows->GetCount()>0 && i < pRows->GetCount());
			//Inte t�mma hela listan feature #2389
			//m_wndReport.ClearReport();
			m_wndReport.Populate();
			m_wndReport.RedrawControl();
		}
	}
}

void CHandleSetupFilesDlg::OnBnClickedRemoveFromListCtrl()
{
	int nNumOfItems =	-1;
	int nSelectedItem = GetSelectedItem(m_wndLCtrl);
	BOOL bIsNumeric = TRUE;
	BOOL bIsNumeric2 = TRUE;
	CString sText;
	if (nSelectedItem > CB_ERR)
	{
		//L�gg till fastighet till lista feature #2389 20111004 J�
		sText = m_wndLCtrl.GetItemText(nSelectedItem,0);	
		for (UINT i1 = 0;i1 < m_vecTransaction_elv_properties.size();i1++)
		{
			if (m_vecTransaction_elv_properties[i1].getPropName().CompareNoCase(sText) == 0)
			{
				m_recElvProps = m_vecTransaction_elv_properties[i1];
				if (bIsNumeric && (!IsNumeric(m_recElvProps.getPropNumber()) || m_recElvProps.getPropNumber().IsEmpty()))
				{
					bIsNumeric = FALSE;
				}
				if (bIsNumeric2 && (!IsNumeric(m_recElvProps.getPropGroupID()) || m_recElvProps.getPropGroupID().IsEmpty()))
				{
					bIsNumeric2 = FALSE;
				}
				
				m_wndReport.AddRecord(new CHandleSetupFileRec(m_wndReport.GetSelectedRows()->GetCount()+1,
					m_recElvProps.getPropName(),
					m_mapPropInSetupFile[m_recElvProps.getPropName()],
					m_mapPropInSetupFileDateTime[m_recElvProps.getPropName()],
					m_recElvProps,
					bIsNumeric,
					bIsNumeric2));
				
			}
		}
		m_wndReport.Populate();
		m_wndReport.RedrawControl();

		m_wndLCtrl.DeleteItem(nSelectedItem);	
		nNumOfItems =	m_wndLCtrl.GetItemCount();
		//m_wndSave.EnableWindow(nNumOfItems > 0);
		m_wndStupFileName.SetWindowText(_T(""));
		m_wndStupFileName.EnableWindow(nNumOfItems > 0);
		m_wndStupFileName.SetReadOnly(!(nNumOfItems > 0));
	}
}

void CHandleSetupFilesDlg::OnBnClickedRemoveAllFromListCtrl()
{
	BOOL bIsNumeric = TRUE;
	BOOL bIsNumeric2 = TRUE;
	m_wndLCtrl.DeleteAllItems();

	// Determin if property-number is numeric of alphanumeric
	for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
	{
		m_recElvProps = m_vecTransaction_elv_properties[i];
		if (!IsNumeric(m_recElvProps.getPropNumber()) || m_recElvProps.getPropNumber().IsEmpty())
		{
			bIsNumeric = FALSE;
			break;
		}

	}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)

	// Determin if property-groupid is numeric of alphanumeric
	for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
	{
		m_recElvProps = m_vecTransaction_elv_properties[i];
		if (!IsNumeric(m_recElvProps.getPropGroupID()) || m_recElvProps.getPropGroupID().IsEmpty())
		{
			bIsNumeric2 = FALSE;
			break;
		}

	}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)

	m_wndReport.ResetContent();
	readSetupFile();
	for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
	{
		m_recElvProps = m_vecTransaction_elv_properties[i];

		m_wndReport.AddRecord(new CHandleSetupFileRec(i,m_recElvProps.getPropName(),
			m_mapPropInSetupFile[m_recElvProps.getPropName()],
			m_mapPropInSetupFileDateTime[m_recElvProps.getPropName()],
			m_recElvProps,
			bIsNumeric,
			bIsNumeric2));
	}	// for (UINT i = 0;i < m_vecTransaction_elv_properties.size();i++)
	m_wndReport.Populate();
	m_wndReport.RedrawControl();

	m_wndStupFileName.SetWindowText(_T(""));
	m_wndStupFileName.EnableWindow(FALSE);
	m_wndStupFileName.SetReadOnly(TRUE);
}

void CHandleSetupFilesDlg::OnEnChangeSetupFNChange()
{
	int nLength = m_wndStupFileName.GetWindowTextLengthW();
	m_wndSave.EnableWindow(nLength > 0);
}

void CHandleSetupFilesDlg::OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nSelectedItem = GetSelectedItem(m_wndLCtrl);
	// Need to set size of Report control; 071109 p�d
	CWnd *pWnd = GetDlgItem(IDC_BUTTON5);
	if (pWnd != NULL) pWnd->EnableWindow(nSelectedItem > -1);

	*pResult = 0;
}

void CHandleSetupFilesDlg::SplitTextLatLon(CString csOrig, CString *pcsLat, CString *pcsLon)
{
	int nFound;
	nFound = csOrig.Find(',');
	*pcsLat = csOrig.Left(nFound);
	*pcsLon = csOrig.Right(csOrig.GetLength() - nFound - 1);
}