#include "stdafx.h"
#include "ShowInformationDlgStormTork.h"


// CShowInformationDlgStormTork dialog
// HMS-50 2020-02-12
IMPLEMENT_DYNAMIC(CShowInformationDlgStormTork, CDialog)



BEGIN_MESSAGE_MAP(CShowInformationDlgStormTork, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CShowInformationDlgStormTork::CShowInformationDlgStormTork(CWnd* pParent)
	: CDialog(CShowInformationDlgStormTork::IDD, pParent)
{
	m_pDB = NULL;

	m_bInitialized = FALSE;
	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);

}

CShowInformationDlgStormTork::~CShowInformationDlgStormTork()
{
}

void CShowInformationDlgStormTork::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();

	m_xml.clean();

	CDialog::OnDestroy();
}

void CShowInformationDlgStormTork::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// CShowInformationDlgStormTork message handlers

BOOL CShowInformationDlgStormTork::OnInitDialog()
{
	CDialog::OnInitDialog();
	CWnd *pBtn = NULL;

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			m_xml.Load(m_sLangFN);

			SetWindowText((m_xml.str(IDS_STRING6401)));
			if ((pBtn = GetDlgItem(IDCANCEL)) != NULL)
				pBtn->SetWindowText((m_xml.str(IDS_STRING4245)));
		}
		m_bInitialized = TRUE;
	}		// if (!m_bInitialized)



	return TRUE;
}


// CShowInformationDlg message handlers

void CShowInformationDlgStormTork::OnPaint()
{
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-30,10,10);

	showStormTork(pDC);

	CDialog::OnPaint();

}


// PRIVATE

void CShowInformationDlgStormTork::showStormTork(CDC *dc)
{

	CString sName,sDoneBy;
	TEXTMETRIC tm;
	BOOL bFound = FALSE;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties *pProp = getActiveProperty();

	double fSpruceMix=m_ElvCruise.getStormTorkInfo_SpruceMix();
	double fSumM3Sk_inside=m_ElvCruise.getStormTorkInfo_SumM3Sk_inside();
	double fAvgPriceFactor=m_ElvCruise.getStormTorkInfo_AvgPriceFactor();
	double fTakeCareOfPerc=m_ElvCruise.getStormTorkInfo_TakeCareOfPerc();
	double fPineP30Price=m_ElvCruise.getStormTorkInfo_PineP30Price();
	double fSpruceP30Price=m_ElvCruise.getStormTorkInfo_SpruceP30Price();
	double fBirchP30Price=m_ElvCruise.getStormTorkInfo_BirchP30Price();
	double fCompensationLevel=m_ElvCruise.getStormTorkInfo_CompensationLevel();
	double fPinePerc=m_ElvCruise.getStormTorkInfo_Andel_Pine();
	double fSprucePerc=m_ElvCruise.getStormTorkInfo_Andel_Spruce();
	double fBirchPerc=m_ElvCruise.getStormTorkInfo_Andel_Birch();
	double fWideningFactor=m_ElvCruise.getStormTorkInfo_WideningFactor();
	
	double fErs_Pine=fSumM3Sk_inside*fPinePerc*fSpruceMix*fPineP30Price*fCompensationLevel;
	double fErs_Spruce=fSumM3Sk_inside*fSprucePerc*fSpruceMix*fSpruceP30Price*fCompensationLevel;
	double fErs_Birch=fSumM3Sk_inside*fBirchPerc*fSpruceMix*fBirchP30Price*fCompensationLevel;
	double fErs_Tot=fErs_Pine+fErs_Spruce+fErs_Birch;

	CString sSi=m_ElvCruise.getECruSI();

	CString sStr=_T("");
	int nLen=0,nLen1=0,nLen2=0,nLen3=0;

	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_hcost_table vecHCost;
	CObjectTemplate_hcost_table recHCost;
	if (pObj != NULL && pProp != NULL && m_pDB != NULL )
	{


		if(m_nNormId==ID_2018_FORREST_NORM || m_nNormId==ID_2009_FORREST_NORM)
		{
			nLen=0;
			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			// Volym x Tr�dslagsandel x Andel avv vol(graninblanding) x P30 Pris x Faktor
			dc->TextOut(10, tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53020))));
			// Tall Gran L�v Tot
			dc->TextOut(10, 3*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53021))));
			dc->TextOut(10, 4*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53022))));
			dc->TextOut(10, 5*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53023))));
			dc->TextOut(10, 6*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53024))));



			nLen1 = m_xml.str(IDS_STRING53021).GetLength();
			nLen2 = m_xml.str(IDS_STRING53022).GetLength();
			nLen3 = m_xml.str(IDS_STRING53023).GetLength();
			nLen = max(nLen1,nLen2);
			nLen = max(nLen,nLen3);

			dc->GetTextMetrics(&tm);
			dc->SelectObject(&m_fnt2);
			//fVol_Pine=fSumM3Sk_inside*
			dc->DrawText(formatData(_T("(%.3f x %.3f x %.2f) x (%.0f x %.2f) = %.2f"),fSumM3Sk_inside,fPinePerc  ,fSpruceMix,fPineP30Price  ,fCompensationLevel,fErs_Pine)		,CRect(50+nLen*tm.tmAveCharWidth,3*tm.tmHeight,330+nLen*tm.tmAveCharWidth,4*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			dc->DrawText(formatData(_T("(%.3f x %.3f x %.2f) x (%.0f x %.2f) = %.2f"),fSumM3Sk_inside,fSprucePerc,fSpruceMix,fSpruceP30Price,fCompensationLevel,fErs_Spruce)	,CRect(50+nLen*tm.tmAveCharWidth,4*tm.tmHeight,330+nLen*tm.tmAveCharWidth,5*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			dc->DrawText(formatData(_T("(%.3f x %.3f x %.2f) x (%.0f x %.2f) = %.2f"),fSumM3Sk_inside,fBirchPerc ,fSpruceMix,fBirchP30Price ,fCompensationLevel,fErs_Birch)		,CRect(50+nLen*tm.tmAveCharWidth,5*tm.tmHeight,330+nLen*tm.tmAveCharWidth,6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			dc->DrawText(formatData(_T("(%.2f + %.2f + %.2f) = %.0fkr"),fErs_Pine,fErs_Spruce,fErs_Birch,fErs_Tot)																,CRect(50+nLen*tm.tmAveCharWidth,6*tm.tmHeight,330+nLen*tm.tmAveCharWidth,7*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);


			if(!(fWideningFactor==1.0) && (fWideningFactor<1.0 || fWideningFactor>1.0) && fWideningFactor!=0.0)
			{
				nLen = m_xml.str(IDS_STRING53025).GetLength();

				dc->SelectObject(&m_fnt1);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

				dc->TextOut(10, 8*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53025))));
				dc->TextOut(11, 9*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53024))));

				dc->GetTextMetrics(&tm);
				dc->SelectObject(&m_fnt2);
				dc->DrawText(formatData(_T("= %.1f"),fWideningFactor)													,CRect(50+nLen*tm.tmAveCharWidth,8*tm.tmHeight,310+nLen*tm.tmAveCharWidth,9*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("(%.0f x %.1f) = %.0f"),fErs_Tot,fWideningFactor,fErs_Tot*fWideningFactor)	,CRect(50+nLen*tm.tmAveCharWidth,9*tm.tmHeight,310+nLen*tm.tmAveCharWidth,10*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			}
		}
		else	//1950 �rs norm annan ber�kning av storm o tork
		{
			nLen=0;
			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			
			//Medelprisfaktor = TallAndel x P30Tall  + GranAndel x P30Gran + L�vAndel x P30Bj�rk
			dc->TextOut(10, 1*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53031))));
			dc->TextOut(10, 2*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53032))));

			//Volym x Medelprisfaktor x Granandel x Faktor
			dc->TextOut(10, 4*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53030))));		
			dc->TextOut(10, 5*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53033))));		


			nLen = m_xml.str(IDS_STRING53032).GetLength();
			dc->GetTextMetrics(&tm);
			dc->SelectObject(&m_fnt2);
			//Medelprisfaktor = TallAndel x P30Tall  + GranAndel x P30Gran + L�vAndel x P30Bj�rk
			dc->DrawText(formatData(_T("(%.2f x %.0f) + (%.2f x %.0f) + (%.2f x %.0f) = %.2f"),fPinePerc,fPineP30Price,fSprucePerc,fSpruceP30Price,fBirchPerc,fBirchP30Price, fAvgPriceFactor)		,CRect(20+nLen*tm.tmAveCharWidth,2*tm.tmHeight,310+nLen*tm.tmAveCharWidth,3*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);

			nLen = m_xml.str(IDS_STRING53033).GetLength();
			//Volym x Medelprisfaktor x Granandel x Faktor
			fErs_Tot=fSumM3Sk_inside*fAvgPriceFactor*fSpruceMix*fTakeCareOfPerc;
			dc->DrawText(formatData(_T(" %.2f x %.2f x %.2f x %.2f = %.0f"),fSumM3Sk_inside,fAvgPriceFactor,fSpruceMix,fTakeCareOfPerc,fErs_Tot)													,CRect(20+nLen*tm.tmAveCharWidth,5*tm.tmHeight,310+nLen*tm.tmAveCharWidth,6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
		}
	}
	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	pProp = NULL;
}