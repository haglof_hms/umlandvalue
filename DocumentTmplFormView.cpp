// DocumentTmplFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DocumentTmplFormView.h"
#include "1950ForrestNormFrame.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

// CDocumentTmplFormView

IMPLEMENT_DYNCREATE(CDocumentTmplFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CDocumentTmplFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_WM_LBUTTONDOWN()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_TBTN_OPEN, OnTBtnOpen)
	ON_UPDATE_COMMAND_UI(ID_TBTN_OPEN, OnUpdateTBtnOpen)
	
	ON_COMMAND(ID_TBTN_PRINT_OUT, OnTBtnPrint)
	ON_UPDATE_COMMAND_UI(ID_TBTN_PRINT_OUT, OnUpdateTBtnPrint)

	ON_COMMAND(ID_TBTN_FONT, OnTBtnFormatFont)
	ON_UPDATE_COMMAND_UI(ID_TBTN_FONT, OnUpdateTBtnFont)

	ON_COMMAND(ID_TBTN_COLOR, OnTBtnColor)
	ON_UPDATE_COMMAND_UI(ID_TBTN_COLOR, OnUpdateTBtnColor)

	ON_COMMAND(ID_TBTN_CUT, OnTBtnCut)
	ON_UPDATE_COMMAND_UI(ID_TBTN_CUT, OnUpdateTBtnCut)

	ON_COMMAND(ID_TBTN_COPY, OnTBtnCopy)
	ON_UPDATE_COMMAND_UI(ID_TBTN_COPY, OnUpdateTBtnCopy)

	ON_COMMAND(ID_TBTN_PASTE, OnTBtnPaste)
	ON_UPDATE_COMMAND_UI(ID_TBTN_PASTE, OnUpdateTBtnPaste)

	ON_COMMAND(ID_TBTN_UNDO, OnTBtnUndo)
	ON_UPDATE_COMMAND_UI(ID_TBTN_UNDO, OnUpdateTBtnUndo)

	ON_COMMAND(ID_TBTN_REDO, OnTBtnRedo)
	ON_UPDATE_COMMAND_UI(ID_TBTN_REDO, OnUpdateTBtnRedo)

	ON_COMMAND(ID_TBTN_BOLD, OnTBtnBold)
	ON_UPDATE_COMMAND_UI(ID_TBTN_BOLD, OnUpdateTBtnBold)

	ON_COMMAND(ID_TBTN_ITALIC, OnTBtnItalic)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ITALIC, OnUpdateTBtnItalic)

	ON_COMMAND(ID_TBTN_UNDERLINE, OnTBtnUnderline)
	ON_UPDATE_COMMAND_UI(ID_TBTN_UNDERLINE, OnUpdateTBtnUnderline)

	ON_COMMAND(ID_TBTN_LEFT, OnTBtnParaLeft)
	ON_UPDATE_COMMAND_UI(ID_TBTN_LEFT, OnUpdateTBtnParaLeft)

	ON_COMMAND(ID_TBTN_CENTER, OnTBtnParaCenter)
	ON_UPDATE_COMMAND_UI(ID_TBTN_CENTER, OnUpdateTBtnParaCenter)

	ON_COMMAND(ID_TBTN_RIGHT, OnTBtnParaRight)
	ON_UPDATE_COMMAND_UI(ID_TBTN_RIGHT, OnUpdateTBtnParaRight)

	ON_COMMAND(ID_TBTN_BULLETS, OnTBtnParaBullets)
	ON_UPDATE_COMMAND_UI(ID_TBTN_BULLETS, OnUpdateTBtnParaBullets)

	ON_CBN_SELCHANGE(IDC_COMBO8_1, OnCbnSelchangeList81)

END_MESSAGE_MAP()

CDocumentTmplFormView::CDocumentTmplFormView()
	: CXTResizeFormView(CDocumentTmplFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
	m_enumACTION = UPD_ITEM;
	m_nDBIndex = -1;
}

CDocumentTmplFormView::~CDocumentTmplFormView()
{
}

void CDocumentTmplFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_sarrDocTypes.RemoveAll();

	CXTResizeFormView::OnDestroy();	
}

void CDocumentTmplFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL_DTMPL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_DTMPL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_DTMPL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_DTMPL4, m_wndLbl4);
	DDX_Control(pDX, IDC_COMBO8_1, m_wndCBox1);
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit4);
	DDX_Control(pDX, IDC_RICHEDIT21, m_wndRTF);
//	DDX_Control(pDX, IDC_RICHTEXTCTRL1, m_wndRTF);
	//}}AFX_DATA_MAP

}

BOOL CDocumentTmplFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;
}

BOOL CDocumentTmplFormView::PreTranslateMessage(MSG* pMsg) 
{
	CWnd *pWnd = GetFocus();
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_TAB))  
	{
		if (m_wndRTF.GetSafeHwnd() == pWnd->GetSafeHwnd())
		{
			// m_bTab=true; 
			m_wndRTF.m_bClick = false;
			// get the char index of the caret position
			CPoint nPos = m_wndRTF.GetCaretPos();  
			// select zero chars  
			//m_wndRTF.SetSel(nPos.x, nPos.x);  
			// then replace that selection with a TAB  
			m_wndRTF.ReplaceSel(_T("\t"), TRUE);
			return true;
		}
	}
	return CXTResizeFormView::PreTranslateMessage(pMsg);
}

void CDocumentTmplFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (!	m_bInitialized )
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit2.SetEnabledColor(BLACK,WHITE);
		m_wndEdit2.SetLimitText(49);	// Max number of characters in box; 091023 p�d

		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit3.SetEnabledColor(BLACK,WHITE);

		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit4.SetEnabledColor(BLACK,WHITE);
		m_wndEdit4.SetWindowText((getDateEx()));
		m_wndEdit4.SetReadOnly();

		m_wndEdit5.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit5.SetEnabledColor(BLACK,WHITE);

		if (fileExists(m_sLangFN))
		{
			if (m_xml.Load(m_sLangFN))
			{
				m_wndLbl1.SetWindowText((m_xml.str(IDS_STRING5600)));
				m_wndLbl2.SetWindowText((m_xml.str(IDS_STRING5601)));
				m_wndLbl3.SetWindowText((m_xml.str(IDS_STRING5602)));
				m_wndLbl4.SetWindowText((m_xml.str(IDS_STRING5603)));
				m_sarrDocTypes.Add((m_xml.str(IDS_STRING56050)));
				m_sarrDocTypes.Add((m_xml.str(IDS_STRING56051)));
				m_sarrDocTypes.Add((m_xml.str(IDS_STRING56052)));
				m_sarrDocTypes.Add((m_xml.str(IDS_STRING56053)));

				m_sMsgCap = (m_xml.str(IDS_STRING229));
				m_sMsgSave = m_xml.str(IDS_STRING5607);
				m_sMsgSave2.Format(_T("%s\n\n%s\n\n%s\n\n"),
					m_xml.str(IDS_STRING5607),
					m_xml.str(IDS_STRING2522),
					m_xml.str(IDS_STRING2504));
				m_sMsgRemove = (m_xml.str(IDS_STRING5608));
				m_sFileFilter = (m_xml.str(IDS_STRING5606));

				// Add 	m_sarrDocTypes data to m_wndCBox1; 080610 p�d
				for (int i = 0;i < m_sarrDocTypes.GetCount();i++)
					m_wndCBox1.AddString(m_sarrDocTypes.GetAt(i));

			}	// if (m_xml.Load(m_sLangFN))
			m_xml.clean();
		}	// if (fileExists(m_slangFN))

		m_enumAction = TEMPLATE_NONE;

		getDocuments();
		if (m_vecTemplate.size() > 0)
		{
			m_nDBIndex = m_vecTemplate.size()-1;
			m_enumACTION = UPD_ITEM;
		}
		else
		{
			m_nDBIndex = -1;
		}

		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < m_vecTemplate.size()-1);
		populateData();

		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

void CDocumentTmplFormView::OnSetFocus(CWnd* wnd)
{
	if (m_bInitialized )
	{
		if (m_nDBIndex > -1)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
		}
		else
		{
//			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
//			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
	}

	setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTemplate.size()-1));
		
	CXTResizeFormView::OnSetFocus(wnd);
}

void CDocumentTmplFormView::OnSize(UINT nType, int cx, int cy)
{
	if (m_wndRTF.GetSafeHwnd())
	{
		setResize(&m_wndRTF,10,80,cx-20,cy-100);
	}
}

BOOL CDocumentTmplFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CDocumentTmplFormView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	// if I have clicked in the window and immediately after the caret
	// is a bold,italic or underlined character, then show the Bold, 
	// Italic or Underlined button pressed

	    m_bClick=true;
			// get the caret position 
			point=m_wndRTF.GetCaretPos ();
			// if there is no text in the window, do not select 
			// anything
			if ( m_wndRTF.GetTextLength()==0 )
				m_wndRTF.SetSel(0 , 0);						
			else 
				// select 1 character and verify if the character is 
				// bold, italic or underlined
				m_wndRTF.SetSel(point.x , point.x+1);
			
			m_wndRTF.ShowCaret(); 
			// hide the selection. It is not importand for the user to 
			// see it
			m_wndRTF.HideSelection(true,false);

	CXTResizeFormView::OnLButtonDown(nFlags, point);
}

// CDocumentTmplFormView diagnostics

#ifdef _DEBUG
void CDocumentTmplFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDocumentTmplFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CDocumentTmplFormView::doSetNavigationBar()
{
	if (m_vecTemplate.size() > 0 || m_enumAction == TEMPLATE_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

// CDocumentTmplFormView message handlers

void CDocumentTmplFormView::populateData()
{
	CString sRTFString;
	if (m_vecTemplate.size() > 0 && 
			m_nDBIndex >= 0 && 
			m_nDBIndex < m_vecTemplate.size() )
	{
		m_enumAction = TEMPLATE_OPEN;

		setEnableWindows(TRUE);
		m_recTemplate = m_vecTemplate[m_nDBIndex];

		m_wndCBox1.SetCurSel(m_recTemplate.getTypeOf());
		m_wndEdit2.SetWindowText((m_recTemplate.getTemplateName()));
		m_wndEdit3.SetWindowText((m_recTemplate.getCreatedBy()));
		sRTFString = m_wndRTF.IsTextInRTF(m_recTemplate.getTemplateFile());
		m_wndRTF.SetRtf(sRTFString);

		m_wndRTF.SetFocus();

		m_enumACTION = UPD_ITEM;

	}	// if (m_vecTamplate.size() > 0)
	else
	{
		m_wndEdit2.SetWindowText(_T(""));
		m_wndEdit3.SetWindowText(_T(""));
		m_wndCBox1.SetCurSel(-1);

		m_wndRTF.SetWindowText(_T(""));
		m_wndRTF.SetFocus();

		setEnableWindows(FALSE);
		m_nDBIndex = -1;
		setNavigationButtons(FALSE,FALSE);
		m_enumACTION = NEW_ITEM;	// No template added yet; 080610 p�d
		m_enumAction = TEMPLATE_NONE;

	}

	doSetNavigationBar();
}

void CDocumentTmplFormView::getDocuments(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getObjectDocumentTemplates(m_vecTemplate);
	}	// if (m_pDB != NULL)
}

//-------------------------------------------------------------------------------
DWORD CALLBACK CDocumentTmplFormView::MyStreamOutCallback(DWORD dwCookie, 
								LPBYTE pbBuff, LONG cb, LONG *pcb)
{
	CFile* pFile = (CFile*) dwCookie;
  pFile->Write(pbBuff, cb);
  *pcb = cb;
  return 0;
}
//-------------------------------------------------------------------------------

/*
#define DOC_TMPL_ADVICE						0		// "Avisering"
#define DOC_TMPL_SALE_CONDITIONS	1		// "F�rs�ljningsvillkor"
#define DOC_TMPL_TAKT_CARE_COND		2		// "Villkor eget tillvaratagande"
*/
BOOL CDocumentTmplFormView::saveDocument(short action)
{
	CString sDocText;
	CTransaction_template rec;
	int nCBoxIndex = m_wndCBox1.GetCurSel();
	// Check that user have entered the nessary data; 080610 p�d
	if (nCBoxIndex == CB_ERR || m_wndEdit2.getText().IsEmpty())
	{
		if (m_nDBIndex > -1 || m_enumAction == TEMPLATE_NEW)
		{
			if (action == 1)
			{
				::MessageBox(this->GetSafeHwnd(),(m_sMsgSave),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
					return TRUE;
			}
			else if (action == 2)
			{
				if (::MessageBox(this->GetSafeHwnd(),(m_sMsgSave2),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
					return TRUE;
				else
					return FALSE;
			}
		}
	}	// if (nCBoxIndex == CB_ERR || m_wndEdit2.getText().IsEmpty())

	if (m_pDB != NULL)
	{
		if (!m_wndEdit2.getText().IsEmpty())
		{
			sDocText = m_wndRTF.GetRtf();
			rec =	CTransaction_template(m_recTemplate.getID(),m_wndEdit2.getText(),nCBoxIndex,sDocText,_T(""),m_wndEdit3.getText());

			if (!m_pDB->addObjectDocumentTemplate(rec))
				m_pDB->updObjectDocumentTemplate(rec);
			// Reload data
			getDocuments();

			if (m_enumACTION == NEW_ITEM)
			{
				if (m_vecTemplate.size() > 0)
				{
					m_nDBIndex = m_vecTemplate.size()-1;
					setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTemplate.size()-1));
					m_enumACTION = UPD_ITEM;
				}
				else
					setNavigationButtons(FALSE,FALSE);

				populateData();
			}
		}

	}	// if (m_pDB != NULL)

	m_enumACTION = UPD_ITEM;

	return TRUE;
}

void CDocumentTmplFormView::removeDocument(void)
{
	// Ask user, if he realy want's to remove this document-template; 080611 p�d
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemove),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (m_pDB->delObjectDocumentTemplate(m_recTemplate))
		{
			getDocuments();
			if (m_vecTemplate.size() > 0)
			{
				m_nDBIndex = m_vecTemplate.size()-1;
				setNavigationButtons(TRUE,FALSE);
				m_enumACTION = UPD_ITEM;
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
			}
			else
			{
				setNavigationButtons(FALSE,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
				AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
			}
		}	// if (m_pDB->delObjectDocumentTemplate(m_recTemplate))
		populateData();
	}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sMsgRemove),_T(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
}

void CDocumentTmplFormView::setNewDocument(void)
{
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText((getUserName().MakeUpper()));
	m_wndEdit4.SetWindowText((getDateEx()));
	m_wndRTF.SetWindowTextW(_T(""));
	m_wndCBox1.SetCurSel(-1);	// No selection
	m_enumACTION = NEW_ITEM;
	m_recTemplate = CTransaction_template();
	// Always enable on new document; 090519 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	m_enumAction = TEMPLATE_NEW;

}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CDocumentTmplFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			saveDocument(1);
			setNewDocument();
			setEnableWindows(TRUE);
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			saveDocument(1);
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			removeDocument();
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d

			if (saveDocument(-1))
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData();
			}

			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d

			if (saveDocument(-1))
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData();
			}

			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d

			if (saveDocument(-1))
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecTemplate.size() - 1))
					m_nDBIndex = (UINT)m_vecTemplate.size() - 1;
						
				if (m_nDBIndex == (UINT)m_vecTemplate.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData();
			}

			break;
		}
		case ID_DBNAVIG_END :
		{
			// Always save active object
			// before movin' on the the next one; 080401 p�d

			if (saveDocument(-1))
			{
				m_nDBIndex = (UINT)m_vecTemplate.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData();
			}

			break;
		}	// case ID_NEW_ITEM :
	}	// switch (wParam)
	return 0L;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CDocumentTmplFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

BOOL CDocumentTmplFormView::PrintRTF(HWND hwnd, HDC hdc)
{
    DOCINFO di = { sizeof(di) };
    di.lpszDocName = _T("ExternalDocument");
    if (!StartDoc(hdc, &di))
    {
        return FALSE;
    }

    int cxPhysOffset = GetDeviceCaps(hdc, PHYSICALOFFSETX);
    int cyPhysOffset = GetDeviceCaps(hdc, PHYSICALOFFSETY);
    int cxPhys = ::MulDiv(GetDeviceCaps(hdc,PHYSICALWIDTH), 1440, GetDeviceCaps(hdc,LOGPIXELSX));//GetDeviceCaps(hdc, PHYSICALWIDTH);
    int cyPhys = ::MulDiv(GetDeviceCaps(hdc,PHYSICALHEIGHT), 1440, GetDeviceCaps(hdc,LOGPIXELSY)); //GetDeviceCaps(hdc, PHYSICALHEIGHT);


// Commented out, this SendMessage'll change size of text formatting in RichText control.
// I'll not use this; 090817 p�d
		// Create "print preview". 
//		::SendMessage(hwnd, EM_SETTARGETDEVICE, (WPARAM)hdc, cxPhys/2);

    FORMATRANGE fr;
    fr.hdc = hdc;
    fr.hdcTarget = hdc;
    fr.rc.left = cxPhysOffset;
    fr.rc.top = cyPhysOffset;
    fr.rc.right = cxPhysOffset + cxPhys;
    fr.rc.bottom = cyPhysOffset + cyPhys;

    // Select the entire contents.
		::SendMessage(hwnd, EM_SETSEL, 0, (LPARAM)-1);
    // Get the selection into a CHARRANGE.
		::SendMessage(hwnd, EM_EXGETSEL, 0, (LPARAM)&fr.chrg);

    BOOL fSuccess = TRUE;

    // Use GDI to print successive pages.
    while (fr.chrg.cpMin < fr.chrg.cpMax && fSuccess) 
    {
        fSuccess = StartPage(hdc) > 0;
        if (!fSuccess) break;
				int cpMin = ::SendMessage(hwnd, EM_FORMATRANGE, TRUE, (LPARAM)&fr);
        if (cpMin <= fr.chrg.cpMin) 
        {
            fSuccess = FALSE;
            break;
        }
        fr.chrg.cpMin = cpMin;
        fSuccess = EndPage(hdc) > 0;
    }
		::SendMessage(hwnd, EM_FORMATRANGE, FALSE, NULL);
    if (fSuccess)
    {
			EndDoc(hdc);
    } 
    else 
    {
			AbortDoc(hdc);
    }

    return fSuccess;
}

void CDocumentTmplFormView::setEnableWindows(BOOL enable)
{
	m_bToolBarEnabled = enable;

	m_wndCBox1.EnableWindow(enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit3.EnableWindow(enable);
	m_wndEdit3.SetReadOnly(!enable);
	m_wndEdit4.EnableWindow(enable);
	m_wndEdit4.SetReadOnly(!enable);
	m_wndEdit5.EnableWindow(enable);
	m_wndEdit5.SetReadOnly(!enable);
	m_wndRTF.EnableWindow(enable);
}

// Open a document and add to m_wndEditOpenDoc
void CDocumentTmplFormView::OnTBtnOpen()
{
		CFileDialog fileDlg(TRUE, _T("rtf"), NULL, OFN_FILEMUSTEXIST,_T("RTF (*.rtf)|*.rtf|"));
		// display the file dialog
		// if I press the Cancel button
		if (fileDlg.DoModal () == IDCANCEL)
		{
			return;
		}
		// I obtain the file name
		m_strFilename = fileDlg.GetPathName ();
		CFile fis;
		fis.Open(m_strFilename, CFile::modeRead | CFile::shareExclusive   );
		m_wndRTF.SetRtf(&fis);
	  
		fis.Close();		
		// if I open a file, don't ask me if to save the 
		// content or not
		m_wndRTF.SetModify(FALSE); 
}

void CDocumentTmplFormView::OnUpdateTBtnOpen(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

// Open a document and add to m_wndEditOpenDoc
void CDocumentTmplFormView::OnTBtnPrint()
{
	PRINTDLG pd = { sizeof(pd) };
  pd.Flags = PD_RETURNDC;
  if (PrintDlg(&pd)) 
	{	
		if (PrintRTF(m_wndRTF.GetSafeHwnd(), pd.hDC))
		{ 
			GlobalFree(pd.hDevMode);
			GlobalFree(pd.hDevNames);
			DeleteDC(pd.hDC);
		}
	}
}

void CDocumentTmplFormView::OnUpdateTBtnPrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}


void CDocumentTmplFormView::OnTBtnFormatFont() 
{
	CFontDialog dlg( NULL , CF_BOTH | CF_EFFECTS );
	if ( dlg.DoModal() != IDOK )
		return;

	if ( dlg.IsUnderline() )
	{
		m_wndRTF.SetSelectionUnderline ();
	}
	
	CString faceName = dlg.GetFaceName();
	m_wndRTF.SetFontName( faceName ) ;
	
	COLORREF clr= dlg.GetColor ();
	m_wndRTF.SetColor(clr); 

	int nPointSize = dlg.GetSize();
	m_wndRTF.SetFontSize( nPointSize ) ;

}

void CDocumentTmplFormView::OnUpdateTBtnFont(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}


void CDocumentTmplFormView::OnTBtnColor() 
{
	m_wndRTF.SetColour();	
}

void CDocumentTmplFormView::OnUpdateTBtnColor(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

void CDocumentTmplFormView::OnTBtnCut()
{
	m_wndRTF.CutText();
}

void CDocumentTmplFormView::OnUpdateTBtnCut(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

void CDocumentTmplFormView::OnTBtnCopy()
{
	m_wndRTF.CopyText();
}

void CDocumentTmplFormView::OnUpdateTBtnCopy(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

void CDocumentTmplFormView::OnTBtnPaste()
{
	m_wndRTF.PasteText();
}

void CDocumentTmplFormView::OnUpdateTBtnPaste(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

void CDocumentTmplFormView::OnTBtnUndo()
{
	m_wndRTF.UndoLast();
}

void CDocumentTmplFormView::OnUpdateTBtnUndo(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

void CDocumentTmplFormView::OnTBtnRedo()
{
	m_wndRTF.Redo();
}

void CDocumentTmplFormView::OnUpdateTBtnRedo(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
}

void CDocumentTmplFormView::OnTBtnBold()
{
	m_wndRTF.SetSelectionBold();
}

void CDocumentTmplFormView::OnUpdateTBtnBold(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_BOLD);
		// get the sel type	
		WORD wSel = m_wndRTF.GetSelectionType ();
		// if sel is bold then show the Bold button pressed
		if ( (wSel == SEL_TEXT) &&  m_wndRTF.SelectionIsBold() )
			pCmdUI->SetCheck(1);  
		// if I have clicked in the window and immediately after the caret
		// is a bold character, then show the Bold button pressed
		else if (wSel == SEL_EMPTY)
		{
			if ( m_wndRTF.SelectionIsBold() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
		else
		{
			if ( m_wndRTF.SelectionIsBold() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
	}
}

void CDocumentTmplFormView::OnTBtnItalic()
{
	m_wndRTF.SetSelectionItalic();
}

void CDocumentTmplFormView::OnUpdateTBtnItalic(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_ITALIC);
		// get the sel type	
		WORD wSel= m_wndRTF.GetSelectionType ();
		// if sel is italic then show the Bold button pressed
		if ( (wSel == SEL_TEXT) &&  m_wndRTF.SelectionIsItalic() )
			pCmdUI->SetCheck(1);  
		// if I have clicked in the window and immediately after the caret
		// is a italic character, then show the Bold button pressed
		else if ( wSel == SEL_EMPTY)
		{
			if ( m_wndRTF.SelectionIsItalic() )
					pCmdUI->SetCheck(1);
			else 
					pCmdUI->SetCheck(0); 
		}
		else
		{
			if ( m_wndRTF.SelectionIsItalic() )
					pCmdUI->SetCheck(1);
			else 
					pCmdUI->SetCheck(0); 
		}
	}
}

void CDocumentTmplFormView::OnTBtnUnderline()
{
	m_wndRTF.SetSelectionUnderline();
}

void CDocumentTmplFormView::OnUpdateTBtnUnderline(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_UNDERLINE);
		WORD wSel= m_wndRTF.GetSelectionType ();
		if (   wSel & SEL_TEXT && m_wndRTF.SelectionIsUnderlined() )
			pCmdUI->SetCheck(1 );  
		else if (wSel == SEL_EMPTY)
		{
			 if ( m_wndRTF.SelectionIsUnderlined() )
				 pCmdUI->SetCheck(1);
			 else 
				 pCmdUI->SetCheck(0); 
		}
		else
		{
			 if ( m_wndRTF.SelectionIsUnderlined() )
				 pCmdUI->SetCheck(1);
			 else 
				 pCmdUI->SetCheck(0); 
		}
	}
}

// function added with classwizard on Saturday 21 september 2001
void CDocumentTmplFormView::OnTBtnParaLeft() 
{
	m_wndRTF.SetParagraphLeft();		
}

void CDocumentTmplFormView::OnUpdateTBtnParaLeft(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_LEFT);
		WORD wSel= m_wndRTF.GetSelectionType ();
		if (   wSel & SEL_TEXT && m_wndRTF.ParagraphIsLeft()) 
			pCmdUI->SetCheck(1 );  
		else if (wSel == SEL_EMPTY)
		{
			if ( m_wndRTF.ParagraphIsLeft() )
				pCmdUI->SetCheck(1);
			else 
			 pCmdUI->SetCheck(0); 
		}
		else
		{
			if ( m_wndRTF.ParagraphIsLeft() )
				pCmdUI->SetCheck(1);
			else 
			 pCmdUI->SetCheck(0); 
		}
	}
}

// function added with classwizard on Monday 24 september 2001
void CDocumentTmplFormView::OnTBtnParaCenter() 
{
	m_wndRTF.SetParagraphCenter();			
}

void CDocumentTmplFormView::OnUpdateTBtnParaCenter(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_CENTER);
		WORD wSel= m_wndRTF.GetSelectionType ();
		if (   wSel & SEL_TEXT && m_wndRTF.ParagraphIsCentered()) 
			pCmdUI->SetCheck(1 );  
		else if ( wSel == SEL_EMPTY)
		{
			if ( m_wndRTF.ParagraphIsCentered() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
		else
		{
			if ( m_wndRTF.ParagraphIsCentered() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
	}
}

void CDocumentTmplFormView::OnTBtnParaRight() 
{
	m_wndRTF.SetParagraphRight();			
}

void CDocumentTmplFormView::OnUpdateTBtnParaRight(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_RIGHT);
		WORD wSel= m_wndRTF.GetSelectionType ();
		if (   wSel & SEL_TEXT && m_wndRTF.ParagraphIsRight()) 
			pCmdUI->SetCheck(1 );  
		else if ( wSel == SEL_EMPTY)
		{
			if ( m_wndRTF.ParagraphIsRight() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
		else
		{
			if ( m_wndRTF.ParagraphIsRight() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
	}
}

void CDocumentTmplFormView::OnTBtnParaBullets() 
{
	m_wndRTF.SetParagraphBulleted();				
}

void CDocumentTmplFormView::OnUpdateTBtnParaBullets(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bToolBarEnabled);
	if (m_bToolBarEnabled)
	{
		// TODO: Add your command update UI handler code here
		ASSERT(pCmdUI->m_nID == ID_TBTN_BULLETS);
		WORD wSel= m_wndRTF.GetSelectionType ();
		if ( wSel & SEL_TEXT && m_wndRTF.ParagraphIsBulleted()) 
			pCmdUI->SetCheck(1 );  
		else if ( wSel == SEL_EMPTY)
		{
			if ( m_wndRTF.ParagraphIsBulleted() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
		else
		{
			if ( m_wndRTF.ParagraphIsBulleted() )
				pCmdUI->SetCheck(1);
			else 
				pCmdUI->SetCheck(0); 
		}
	}
}

void CDocumentTmplFormView::OnCbnSelchangeList81()
{
	int nIndex = m_wndCBox1.GetCurSel();
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,nIndex != CB_ERR);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,nIndex != CB_ERR);
}
