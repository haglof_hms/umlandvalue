#pragma once

#include "Resource.h"

// CPropertyStatusFormView form view

class CPropertyStatusFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPropertyStatusFormView)

	BOOL m_bInitialized;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgDelete;
	CString m_sMsgDelete2;
	
	CMyReportCtrl m_wndReport;
	CImageList m_ilIcons;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CPropertyStatusFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertyStatusFormView();

	void setupReport(void);

	void LoadReportState();
	void SaveReportState();

	vecTransaction_property_status m_vecPropStatus;
	void populateData(void);
	void getPropActionStatusFromDB(void);
	void newPropActionStatus(void);
	void savePropActionStatus(void);
	void delPropActionStatus(void);
	short getNextPropActionStatus_ordernum(void);

	mapBoolean m_mapPropStatusUsed;
	void getPropertyStatusUsedFromDB(void);
public:
	enum { IDD = IDD_FORMVIEW13 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


	void doSavePropActionStatus(void)	{ savePropActionStatus(); }

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


