#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////////
// CPropertiesOtherCompDataRec

class CPropertiesOtherCompDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_elv_properties_other_comp propOtherCompRec;
protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};


	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:
	CPropertiesOtherCompDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz2dec));
		AddItem(new CFloatItem(0.0,sz2dec));
		AddItem(new CTextItem(_T("")));
	}

	CPropertiesOtherCompDataRec(UINT index,CStringArray& types,CTransaction_elv_properties_other_comp data)	 
	{
		int nTypeOf = data.getPropOtherCompTypeOf();
		m_nIndex = index;
		propOtherCompRec = data;
		if (types.GetCount() > 0)
		{
			if (nTypeOf >= 0 && nTypeOf < types.GetCount())
			{
				AddItem(new CTextItem((types[nTypeOf])));
			}
			else
				AddItem(new CTextItem(_T("")));
		}
		else
			AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(data.getPropOtherCompValue_entered(),sz2dec));
		AddItem(new CFloatItem(data.getPropOtherCompValue_calc(),sz2dec));
		AddItem(new CTextItem((data.getPropOtherCompNote())));
	}

	CString getColumnText(int item)	{ return ((CTextItem*)GetItem(item))->getTextItem();	}
	void setColumnText(int item,LPCTSTR text)	{ ((CTextItem*)GetItem(item))->setTextItem(text);	}
	double getColumnFloat(int item)	{ return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	void setColumnFloat(int item,double value)	{ ((CFloatItem*)GetItem(item))->setFloatItem(value);	}


	UINT getIndex(void)	{	return m_nIndex;	}
	
	CTransaction_elv_properties_other_comp &getRecord(void)	{		return propOtherCompRec;	}

};


// CPropertyOtherCompDlg dialog

class CPropertyOtherCompDlg : public CDialog
{
	DECLARE_DYNAMIC(CPropertyOtherCompDlg)

	//private:
	BOOL m_bInitialized;

	// Setup language filename; 051214 p�d
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgRemoveOtherCompEntry;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;

	CButton m_wndAddRowBtn;
	CButton m_wndDelRowBtn;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CMyReportCtrl m_wndReport;
	void setupReport(void);

	CString m_sObjectName;
	CString m_sPropName;
	CString m_sPropNum;

	CStringArray m_sarrTypes;

	CString m_sLogAddOtherCompMsg;
	CString m_sLogDelOtherCompMsg;

	double m_fSumOtherComp;

	int m_nNumOfEntriesInLogBook;

	CTransaction_elv_object m_recObject;

	CTransaction_elv_properties m_recProperty;
	vecTransaction_elv_properties_other_comp m_vecOtherComp;
	CUMLandValueDB *m_pDB;

	void populateData();

	// Add "typ av ers�ttning"
	void addConstraints(void);
	int findTypeOf(LPCTSTR type_of);
public:
	CPropertyOtherCompDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPropertyOtherCompDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG11 };

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}

	void setObjectAndPropertyInfo(LPCTSTR obj_name,CTransaction_elv_properties &rec)
	{
		m_sObjectName = obj_name;
		m_sPropName = rec.getPropName();
		m_sPropNum = rec.getPropNumber();
		m_recProperty = rec;
	}

	double getSumOtherComp(void)	{ return m_fSumOtherComp; }

	void getPropertyOtherComp(void);
	void savePropertyOtherComp(void);
	void delEntryInPropertyOtherComp(CTransaction_elv_properties_other_comp &rec);
protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnReportValueChanged(NMHDR * pNotifyStruct, LRESULT * result);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
