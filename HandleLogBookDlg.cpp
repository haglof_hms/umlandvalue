// HandleLogBookDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HandleLogBookDlg.h"

#include "ResLangFileReader.h"

// CHandleLogBookDlg dialog

IMPLEMENT_DYNAMIC(CHandleLogBookDlg, CDialog)

BEGIN_MESSAGE_MAP(CHandleLogBookDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHandleLogBookDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CHandleLogBookDlg::CHandleLogBookDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHandleLogBookDlg::IDD, pParent),
	  m_sPath(_T(""))
{

}

CHandleLogBookDlg::~CHandleLogBookDlg()
{
}

void CHandleLogBookDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHandleLogBookDlg)
	DDX_Control(pDX, IDC_LBL30_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LIST30_1, m_listCtrl);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CHandleLogBookDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING8000));

			m_wndLbl1.SetWindowText(xml.str(IDS_STRING8001));

			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING22695));
			m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING22696));

			m_listCtrl.ModifyExtendedStyle(0,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

			m_listCtrl.InsertColumn(0,_T(""), LVCFMT_LEFT, 30);	 
			m_listCtrl.InsertColumn(1,_T(""), LVCFMT_LEFT, 250);

			InsertRow(m_listCtrl,0,2,_T(""), xml.str(IDS_STRING8002));
			InsertRow(m_listCtrl,1,2,_T(""), xml.str(IDS_STRING8003));
			InsertRow(m_listCtrl,2,2,_T(""), xml.str(IDS_STRING8004));
			InsertRow(m_listCtrl,3,2,_T(""), xml.str(IDS_STRING8005));
			InsertRow(m_listCtrl,4,2,_T(""), xml.str(IDS_STRING8006));

			m_sPath.Format(L"%s\\%s",getDocumentsDir(CSIDL_COMMON_DOCUMENTS),LOG_SETTINGS_FILE);
			int nPos = 0;
			CString sSettings = L"";
			if (fileExists(m_sPath))
			{
					CStdioFile f;
					if (f.Open(m_sPath, CFile::modeRead | CFile::typeText))
					{
						f.ReadString(sSettings);
						if (!sSettings.IsEmpty())
						{
							m_listCtrl.SetCheck(0,sSettings.Tokenize(_T(";"),nPos) == L"1");
							m_listCtrl.SetCheck(1,sSettings.Tokenize(_T(";"),nPos) == L"1");
							m_listCtrl.SetCheck(2,sSettings.Tokenize(_T(";"),nPos) == L"1");
							m_listCtrl.SetCheck(3,sSettings.Tokenize(_T(";"),nPos) == L"1");
							m_listCtrl.SetCheck(4,sSettings.Tokenize(_T(";"),nPos) == L"1");			
						}
						else
						{
							m_listCtrl.SetCheck(0,TRUE);
							m_listCtrl.SetCheck(1,TRUE);
							m_listCtrl.SetCheck(2,TRUE);
							m_listCtrl.SetCheck(3,TRUE);
							m_listCtrl.SetCheck(4,TRUE);
						}
						f.Close();
					}
			}
			else
			{
				m_listCtrl.SetCheck(0,TRUE);
				m_listCtrl.SetCheck(1,TRUE);
				m_listCtrl.SetCheck(2,TRUE);
				m_listCtrl.SetCheck(3,TRUE);
				m_listCtrl.SetCheck(4,TRUE);
			}	



			xml.clean();
		}
	}

	return TRUE;
}
// CHandleLogBookDlg message handlers

void CHandleLogBookDlg::OnBnClickedOk()
{
	CString sSettings = _T("");

	for (int i = 0;i < m_listCtrl.GetItemCount();i++)
	{
		if (m_listCtrl.GetCheck(i))
		{
			sSettings += _T("1;");
		}
		else
		{
			sSettings += _T("0;");
		}
	}

	CStdioFile f;
	if (f.Open(m_sPath, CFile::modeWrite | CFile::modeCreate | CFile::typeText))
	{
		f.WriteString(sSettings);
		f.Close();
	}


	OnOK();
}
