// PropertiesFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MDI1950ForrestNormFormView.h"
#include "PropertiesFormView.h"
#include "ObjectFormView.h"
#include "PropertyLogBookDlg.h"
#include "PropertyOtherCompDlg.h"
#include "PropertyOwnersDlg.h"
#include "StandsMatchedToPropDlg.h"
#include "LoggMessageDlg.h"
#include "ShowInformationDlg.h"
#include "ShowInformationDlg2.h"
#include "ObjectResultDlg.h"
#include "MyInputDlg.h"
#include "MyMsgBoxDlg.h"
#include "RemPropDlg.h"

/////////////////////////////////////////////////////////////////////////////
// Holds information on Property Status set in table "elv_property_status_table"; 090916 p�d
extern CUMLandValueDB *global_pDB;
extern StrMap global_langMap;
// CPropertiesFormView

IMPLEMENT_DYNCREATE(CPropertiesFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertiesFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnUserSuiteMessage)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL_2, OnSelectedChanged)
	ON_NOTIFY(NM_CLICK, ID_REPORT_PROPERTIES, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_PROPERTIES, OnReportChanged)
	ON_NOTIFY(NM_KEYDOWN, ID_REPORT_PROPERTIES, OnReportKeyDown)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, ID_REPORT_PROPERTIES, OnReportHeaderColumnRClick)
	ON_NOTIFY(NM_RCLICK, ID_REPORT_PROPERTIES, OnReportColumnRClick)
END_MESSAGE_MAP()

CPropertiesFormView::CPropertiesFormView()
	: CXTResizeFormView(CPropertiesFormView::IDD),
	m_wndSplitter(NUMBER_OF_PANES,SSP_HORZ,50,2)
{
	m_bInitialized = FALSE;
	m_bInitializedSplitter = FALSE;
	m_pDB = NULL;
	m_nWindowPosSet_y = 80;
	m_nWindowPos_y = 0;
	m_nWindowPos_x = 0;
	m_bDoCompleteReCalc = FALSE;	// Default do selective calculation; 090526 p�d
}

CPropertiesFormView::~CPropertiesFormView()
{
}


void CPropertiesFormView::OnDestroy()
{
	if (m_pDB != NULL)
	{
		delete m_pDB;
		m_pDB = NULL;
	}
	if( global_pDB )
	{
		// global_pDB is pointing to the same instance as m_pDB
		global_pDB = NULL;
	}

	m_ilIcons.DeleteImageList();

	m_xml.clean();

	SaveReportState();

	writeSplitBarSettingsToRegistry();

	CXTResizeFormView::OnDestroy();	
}

void CPropertiesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CPropertiesFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CPropertiesFormView::OnSetFocus(CWnd* pWnd)
{
//	OutputDebugString(_T("CPropertiesFormView::OnSetFocus\n"));

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);	

	CXTResizeFormView::OnSetFocus(pWnd);
}

int CPropertiesFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL_2);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	if (m_wndReportProperties.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReportProperties.Create(this, ID_REPORT_PROPERTIES, TRUE, FALSE))
//		if (!m_wndReportProperties.Create(WS_CHILD|WS_TABSTOP|WS_VISIBLE|WM_VSCROLL|WM_HSCROLL, CRect(0, 0, 0, 0), this, ID_REPORT_PROPERTIES))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return -1;
		}
	}

	m_wndSplitter.Create(this);
	m_wndSplitter.SetPane(PANE_1_INDEX,&m_wndReportProperties);
	m_wndSplitter.SetPane(PANE_2_INDEX,&m_wndTabControl);

	// Try to get info. on settings for SpliBar, from registry; 090122 p�d
	readSplitBarSettingsFromRegistry();
	m_wndSplitter.ChangeOrientation(m_nOrientation);

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		if (m_xml.Load(m_sLangFN))
		{
			// Setup error text used in ForrestNorm; 080509 p�d
			m_mapForrestNormErrText[IDS_STRING5000] = (m_xml.str(IDS_STRING5000));
			m_mapForrestNormErrText[IDS_STRING5001] = (m_xml.str(IDS_STRING5001));
			m_mapForrestNormErrText[IDS_STRING5002] = (m_xml.str(IDS_STRING5002));
			m_mapForrestNormErrText[IDS_STRING5101] = (m_xml.str(IDS_STRING5101));
			m_mapForrestNormErrText[IDS_STRING5102] = (m_xml.str(IDS_STRING5102));
			m_mapForrestNormErrText[IDS_STRING5103] = (m_xml.str(IDS_STRING5103));
			m_mapForrestNormErrText[IDS_STRING5104] = (m_xml.str(IDS_STRING5104));
			m_mapForrestNormErrText[IDS_STRING5105] = (m_xml.str(IDS_STRING5105));
			m_mapForrestNormErrText[IDS_STRING5106] = (m_xml.str(IDS_STRING5106));
			m_mapForrestNormErrText[IDS_STRING5107] = (m_xml.str(IDS_STRING5107));

			m_sObjTabCaption = (m_xml.str(IDS_STRING200));
			m_sPropTabCaption = (m_xml.str(IDS_STRING201));

			sEvalueTabCaption = (m_xml.str(IDS_STRING2010));
			sCruisingTabCaption = (m_xml.str(IDS_STRING2011));
			m_sMsgBatchStandTmpl = (m_xml.str(IDS_STRING4304));
			
			AddView(RUNTIME_CLASS(CEvaluatedData), sEvalueTabCaption, 3);
			AddView(RUNTIME_CLASS(CCruisingData), sCruisingTabCaption, 3);
		
		}
	}
	
	return 0;
}

void CPropertiesFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST1, &pWnd->m_wndFieldChooserDlg);
		m_wndReportProperties.GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (!	m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		// Added 2009-09-15 p�d
		// Holds Action status for properties; 090915 p�d
		getPropActionStatusFromDB();

		if (fileExists(m_sLangFN))
		{
			if (m_xml.Load(m_sLangFN))
			{
				// If there's no status set in db table, setup a default status setting; 091008 p�d
				if (m_vecPropStatus.size() == 0)
				{
					m_vecPropStatus.push_back(CTransaction_property_status(0,1,m_xml.str(IDS_STRING3100),1,255,255,255,_T("")));
/*	Only add one status as default; 091008 p�d
					m_vecPropStatus.push_back(CTransaction_property_status(1,2,m_xml.str(IDS_STRING3101),1,244,238,224,_T("")));
					m_vecPropStatus.push_back(CTransaction_property_status(2,3,m_xml.str(IDS_STRING3102),0,255,165,	 0,_T("")));
					m_vecPropStatus.push_back(CTransaction_property_status(3,4,m_xml.str(IDS_STRING3103),0, 95,158,160,_T("")));
					m_vecPropStatus.push_back(CTransaction_property_status(4,5,m_xml.str(IDS_STRING3104),0,205, 92, 92,_T("")));
					m_vecPropStatus.push_back(CTransaction_property_status(5,6,m_xml.str(IDS_STRING3105),0,255,255,	 0,_T("")));
					m_vecPropStatus.push_back(CTransaction_property_status(6,7,m_xml.str(IDS_STRING3106),0,154,205, 50,_T("")));
*/
				}


				if (m_vecPropStatus.size() > 0)
				{
					for (UINT i = 0;i < m_vecPropStatus.size();i++)
						m_arrStatus.Add(m_vecPropStatus[i].getPropStatusName());
				}
			}

			m_sarrOfferAccepted.Add(m_xml.str(IDS_STRING3103));
			m_sarrOfferAccepted.Add(m_xml.str(IDS_STRING3104));
			m_vecIconToogle.push_back(ICON_TOOGLE_STRUCT(14,m_xml.str(IDS_STRING3107)));
			m_vecIconToogle.push_back(ICON_TOOGLE_STRUCT(15,m_xml.str(IDS_STRING3103)));
			m_vecIconToogle.push_back(ICON_TOOGLE_STRUCT(16,m_xml.str(IDS_STRING3104)));
		}
		setupReportProperties();

		populateProperties();

		setActivePropertyInReport();

		LoadReportState();
		
	
		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

void CPropertiesFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	ShowScrollBar(SB_BOTH, FALSE); //hide the outer scrollbar
	SetScrollPos(SB_BOTH,0); //set outer scroll position to 0

	if (m_wndSplitter.GetSafeHwnd())
	{
		setResize(&m_wndSplitter,0,0,cx,cy);
		m_wndSplitter.SetPaneSizes(m_SplitterPaneSizes);
	}	
}

BOOL CPropertiesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
			global_pDB = m_pDB;
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


void CPropertiesFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
/*
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		int nIndex = pItem->GetIndex();
		if (nIndex > -1)
		{
			// Endable/Disable toolbar buttons; 080122 p�d
			CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pFrame != NULL)
			{
				pFrame->setExcludedToolItems(nIndex);
			}	// if (pFrame != NULL)
		}	// if (nIndex > -1)
	}	// if (pItem != NULL)
*/
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPropertiesFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{

	if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		// The return message holds a _user_msg structure, collected in the
		// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
		// In this case the szFileName item in _user_msg structure holds
		// the path and filename of the ShellData file used and the szArgStr
		// holds the Suite/UserModule name; 070410 p�d
		_user_msg *msg = (_user_msg*)lParam;
		if (msg != NULL)
		{
			CString sShellDataFile = msg->getFileName();
			int nIndex = msg->getIndex();
			getSTDReports(sShellDataFile,PROGRAM_NAME,nIndex,m_vecReports);
		}	// if (msg != NULL)
	}
	return 0L;
}


LRESULT CPropertiesFormView::OnUserSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == -1)
			{
				populateProperties();
			}
		}	// if (sizeof(*msg) = sizeof(_doc_identifer_msg))
	}	// if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	return 0L;
}

void CPropertiesFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	int nNumOfEntries = -1;
	CString sObjName;
	CTransaction_elv_object *pObj = getActiveObject();
	CObjectFormView *pObjView = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
			case COLUMN_0 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{

					CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						// Open logbook for selected property; 080417 p�d
						CPropertyLogBookDlg *pDlg = new CPropertyLogBookDlg();
						if (pDlg != NULL)
						{

							pDlg->setDBConnection(m_pDB);

							if (pObj != NULL)
								sObjName = pObj->getObjectName();
							else
								sObjName.Empty();

							pDlg->setObjectAndPropertyInfo(sObjName,pRec->getRecELVProp());
							pDlg->DoModal();
							// Set number of logitems added to this property; 080423 p�d
							nNumOfEntries = pDlg->getNumOfEntriesInLogBook();
							pRec->setColumnText(COLUMN_0,formatData(_T("%d st"),nNumOfEntries));
							delete pDlg;
						}	// if (pDlg != NULL)
					}	// if (pRec != NULL)
				}	// if (hitTest_X(pt.x,rect.left,13))
				break;
			}

			// Documents
			case COLUMN_1 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						// Get Module language filename; 080616 p�d
						CString	sLangFN(getLanguageFN(getLanguageDir(),(FORREST_PROGRAM_NAME),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
						showFormView(123,sLangFN);
						AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
								(LPARAM)&_doc_identifer_msg(_T("Module5200"),_T("Module123"),_T(""),
								pRec->getRecELVProp().getPropID_pk(),0,1));
					}	// if (pRec != NULL)
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}

			case COLUMN_5 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						// Open logbook for selected property; 080417 p�d
						CPropertyOwnersDlg *pDlg = new CPropertyOwnersDlg();
						if (pDlg != NULL)
						{
							if (pObj != NULL) {
								sObjName = pObj->getObjectName();
								pDlg->setObjectId(pObj->getObjID_pk());
							}
							else
								sObjName.Empty();

							pDlg->setDBConnection(m_pDB);
							pDlg->setObjectAndPropertyInfo(sObjName,pRec->getRecELVProp());
							pDlg->DoModal();
							delete pDlg;
						}	// if (pDlg != NULL)
					}	// if (pRec != NULL)
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}

			// "Visa inforamtion om Frivillig ers�ttning"; 080519 p�d
			case COLUMN_19 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CShowInformationDlg *pDlg = new CShowInformationDlg();
					if (pDlg != NULL)
					{
						pDlg->setDBConnection(m_pDB);
						pDlg->DoModal();

						delete pDlg;
					}	// if (pDlg != NULL)
					//populateProperties();
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}

			// "Visa inforamtion om F�rdyrad avverkning"; 080519 p�d
			case COLUMN_20 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CShowInformationDlg2 *pDlg = new CShowInformationDlg2();
					if (pDlg != NULL)
					{
						pDlg->setDBConnection(m_pDB);
						pDlg->DoModal();

						delete pDlg;
					}	// if (pDlg != NULL)
					//populateProperties();
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}

			// "Annan ers�ttning"; 080519 p�d
			case COLUMN_21 :
			{
				double fSumOtherComp = 0.0;
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						// Open other costs for selected property; 080516 p�d
						CPropertyOtherCompDlg *pDlg = new CPropertyOtherCompDlg();
						if (pDlg != NULL)
						{

							pDlg->setDBConnection(m_pDB);

							if (pObj != NULL)
								sObjName = pObj->getObjectName();
							else
								sObjName.Empty();

							pDlg->setObjectAndPropertyInfo(sObjName,pRec->getRecELVProp());

							if (pDlg->DoModal() == IDOK)
							{
								fSumOtherComp = pDlg->getSumOtherComp();
								if (m_pDB != NULL)
								{
									m_pDB->updObjPropTraktSUM_5(pRec->getRecELVProp().getPropID_pk(),
																							pRec->getRecELVProp().getPropObjectID_pk(),
																							fSumOtherComp);
								}
							}

							delete pDlg;
						}
						populateProperties();
					}	// if (pRec != NULL)
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}

			// "User reports"; 090528 p�d
			// "Grupp identitet"; 090528 p�d
			case COLUMN_22 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						CObjectResultDlg *pDlg = new CObjectResultDlg();
						if (pDlg != NULL)
						{
							pDlg->setLandOwnerNumber(pRec->getColumnIconText(COLUMN_22));
							pDlg->DoModal();

							delete pDlg;

						}	// if (pDlg != NULL)
					}	// if (pRec != NULL)
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}

			// "Typ av ers�ttning; f�rs�ljning el. egen regi"; 090528 p�d
			case COLUMN_23 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				if (hitTest_X(pt.x,rect.left,13)) 
				{
					CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						// Check if the user clicked on the Icon or not; 080513 p�d
						pRec->setColumnToogle(COLUMN_23);	// Toogle Icon item; 090930 p�d
					}	// if (pRec != NULL)
				}	// if (hitTest_X(pt.x,rect.left,13)) 
				break;
			}
/*
			case COLUMN_24 :
				CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
				if (pRec != NULL)
				{
					CString S;
					S.Format(_T("CPropertiesFormView::OnReportKeyDown\nSort order num %d\n"),pRec->getColumnInt(COLUMN_24));
					OutputDebugString(S);
				}
			break;
*/
	};
	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	pObjView = NULL;

	// Update report data; 080516 p�d
	setActivePropertyInReport();
}


void CPropertiesFormView::OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	//enum_sortOrderDirection sortOrderDir = SORT_NONE;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
		case COLUMN_2 :	// Status
		{
			int nStatusID = 0;
			CXTPReportColumn *pCol0 = pItemNotify->pColumn->GetColumns()->Find(COLUMN_2);
			if (pCol0 != NULL)
			{
				if (pItemNotify->pColumn->GetIndex() == pCol0->GetIndex())
				{
					CXTPReportRecordItem *pRecItem = pItemNotify->pItem;
					if (pRecItem != NULL)
					{
						CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRecItem->GetRecord();
						CXTPReportRecordItemEditOptions *pEdOptions = pItemNotify->pItem->GetEditOptions(pCol0);
						if (pEdOptions != NULL)
						{
							CXTPReportRecordItemConstraint *pConst = pEdOptions->FindConstraint(pRecItem->GetCaption(pCol0));
							if (pConst != NULL)
							{
								// Check if dwData is within limits; 090930 p�d
								if (pConst->m_dwData >= 0 && pConst->m_dwData <	m_vecPropStatus.size())
								{
									nStatusID = m_vecPropStatus[pConst->m_dwData].getPropStatusID_pk();
								}
								// Save the changed status to database and populate; 080417 p�d
								if (m_pDB != NULL && pRec != NULL)
								{
									
									CObjectFormView *pObj = NULL;
									// Save data before changing status data; 091005 p�d
									CMDI1950ForrestNormFormView *pView = (CMDI1950ForrestNormFormView*)getFormViewByID(IDD_FORMVIEW);
									if (pView != NULL)
									{
										if ((pObj = pView->getObjectFormView()) != NULL)
										pObj->saveObject(1);
									}	// if (pView != NULL)


									if (m_pDB->updPropertyStatus(pRec->getRecELVProp().getPropID_pk(),
																							 pRec->getRecELVProp().getPropObjectID_pk(),
																							 nStatusID))
									{
										// I think we could add a note to the logbook here; 080604 p�d
										// �ndra status

										CTransaction_elv_properties_logbook recLogBook = CTransaction_elv_properties_logbook(pRec->getRecELVProp().getPropID_pk(),
																																																				 pRec->getRecELVProp().getPropObjectID_pk(),
																																																				 getDateTimeEx2(),
																																																				 getUserName().MakeUpper() ,
																																																				 pRec->getColumnText(COLUMN_2),
																																																				 _T(""),
																																																				 nStatusID);
										if (m_pDB != NULL && logThis(2))
											m_pDB->addPropertyLogBook(recLogBook);

										// Repopulate; 080417 p�d
										populateProperties();
										// Update "V�rdering" and "Taxering"; 090507 p�d
										setActivePropertyInReport();
									}
								}	// if (m_pDB != NULL && pRec != NULL)
							}	// if (pConst != NULL)
						}	// if (pEdOptions != NULL)
					}	// if (pRecItem != NULL)
				}	// if (pItemNotify->pColumn->GetIndex() == pCol0->GetIndex())
			}	// if (pCol0 != NULL)

			break;
		}
		case COLUMN_24 :
		{
			int nChangedNumber = -1;
			int nCurrentNumber=0;
			int nOldNumber=0;
			int nDoSort=0;
			CString sMsg=_T("");

			//int nOrderNumber = -1;
			//int nNumber = -1;
			//CString sPropName;
			// Ask user if he really want to do this; 091014 p�d
			if (::MessageBox(this->GetSafeHwnd(),m_sMsgChangeSortOrder,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{

				CELVPropertyReportRec *pRecSelected = (CELVPropertyReportRec*)pItemNotify->pItem->GetRecord();
				//int nRowIndex = pItemNotify->pRow->GetIndex();
				if (pRecSelected != NULL)
				{
				
					//----------------------------------------------------------------------------------------------------
					//Added 20100305 by J� Shorten the function down to work more properly
					//Before the check that compared properties was made on the property name, but you can have 2 properties with the 
					//same name so now the comparison is made on the Object Id and property Id instead

					//Retreive new sort number
					nChangedNumber = pRecSelected->getColumnInt(COLUMN_24);
					nDoSort=0;
					//Retreive old sort number and check whether to sort or not
					for (UINT i = 0;i < m_vecELV_properties.size();i++)
					{
						CTransaction_elv_properties rec = m_vecELV_properties[i];

						if (pRecSelected->getPropId()==rec.getPropID_pk() && rec.getPropObjectID_pk()==pRecSelected->getObjId())
						{
							nOldNumber=m_vecELV_properties[i].getPropSortOrder();
							if(nOldNumber!=nChangedNumber)
								nDoSort=1;
							break;
						}
					}

					//Kolla hur m�nga fastigheter det finns f�r att kunna s�tta ett max nummer Bug #25008 20111125 J�
					int nMaxNum=m_vecELV_properties.size();
					//Inte kunna s�tta ett nummer st�rre �n max eller mindre �n 1.
					if(pRecSelected->getColumnInt(COLUMN_24)>nMaxNum || pRecSelected->getColumnInt(COLUMN_24)<1)
					{
						sMsg.Format(_T("%s%d"),m_sMsgNotallowedSortNr,nMaxNum);
						::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION);
						//S�tt ebax gamla sorteringsnumret om det nya �r f�r stort	Bug #25008 20111125 J�
						pRecSelected->setColumnInt(COLUMN_24,nOldNumber);
					}
					else
					{
						if (nDoSort==1)
						{
							for (UINT i = 0;i < m_vecELV_properties.size();i++)
							{
								CTransaction_elv_properties rec = m_vecELV_properties[i];
								nCurrentNumber=rec.getPropSortOrder();

								//Do not check the property with the changed sort order
								if(!(pRecSelected->getPropId()==rec.getPropID_pk() && rec.getPropObjectID_pk()==pRecSelected->getObjId()))
								{
									nCurrentNumber=rec.getPropSortOrder();
									//Check whether to change the sort order or not on the current property
									if(nChangedNumber<nOldNumber && nCurrentNumber<nOldNumber && nCurrentNumber>=nChangedNumber)
										m_vecELV_properties[i].setPropSortOrder(nCurrentNumber+1);
									else
									{
										if(nChangedNumber>nOldNumber && nCurrentNumber>nOldNumber && nCurrentNumber<=nChangedNumber)
											m_vecELV_properties[i].setPropSortOrder(nCurrentNumber-1);
									}
								}
								//Set the changed sort order to the new(edited) sort order
								else
								{
									m_vecELV_properties[i].setPropSortOrder(nChangedNumber);
								}
							}

							for (UINT i = 0;i < m_vecELV_properties.size();i++)
							{
								CTransaction_elv_properties rec = m_vecELV_properties[i];
								if (m_pDB) m_pDB->updPropertySortOrder(rec.getPropID_pk(),rec.getPropObjectID_pk(),rec.getPropSortOrder());
							}	// for (UINT i = 0;i < m_vecELV_properties.size();i++)
						}
						populateProperties();
					}
					//----------------------------------------------------------------------------------------------------

					/*
					//----------------------------------------------------------------------------------
						// We'll try to find out which direction we should change the sort order; 091014 p�d
						// If changed number is > previous number => SORT_GT_TO_LT
						// If changed number is < previous number => SORT_LT_TO_GT
						// If changed number is = previous number => SORT_NONE
					for (UINT i = 0;i < m_vecELV_properties.size();i++)
					{
						CTransaction_elv_properties rec = m_vecELV_properties[i];
						if (sPropName.CompareNoCase(rec.getPropName()) == 0)
						{
							if (nChangedNumber < m_vecELV_properties[i].getPropSortOrder())
								sortOrderDir = SORT_LT_TO_GT;
							else if (nChangedNumber > m_vecELV_properties[i].getPropSortOrder())
								sortOrderDir = SORT_GT_TO_LT;
							else sortOrderDir = SORT_NONE;
						break;
						}
					}
					if (sortOrderDir != SORT_NONE)
					{
						//----------------------------------------------------------------------------------
						// Do rearrange sortorder for Porperties; 091013 p�d
						for (UINT i = 0;i < m_vecELV_properties.size();i++)
						{
							CTransaction_elv_properties rec = m_vecELV_properties[i];

							if (sortOrderDir == SORT_LT_TO_GT)
							{
								if (rec.getPropSortOrder() >= nChangedNumber && sPropName.CompareNoCase(rec.getPropName()) != 0)
								{
									nNumber++;
									m_vecELV_properties[i].setPropSortOrder(nNumber);
								}
							}	// if (sortOrderDir == SORT_LT_TO_GT)
							if (sortOrderDir == SORT_GT_TO_LT)
							{
								if (rec.getPropSortOrder() <= nChangedNumber && sPropName.CompareNoCase(rec.getPropName()) != 0)
								{
									nOrderNumber++;
									m_vecELV_properties[i].setPropSortOrder(nOrderNumber);
								}
								else if (rec.getPropSortOrder() > nChangedNumber && sPropName.CompareNoCase(rec.getPropName()) != 0)
								{
									nNumber++;
									m_vecELV_properties[i].setPropSortOrder(nNumber);
								}

							}	// if (sortOrderDir == SORT_GT_TO_LT)
							if (sPropName.CompareNoCase(rec.getPropName()) == 0)
							{
								m_vecELV_properties[i].setPropSortOrder(nChangedNumber);
							}
						}	// for (UINT i = 0;i < m_vecELV_properties.size();i++)
						// Update sortorder in databasetable; 091014 p�d
						for (UINT i = 0;i < m_vecELV_properties.size();i++)
						{
							CTransaction_elv_properties rec = m_vecELV_properties[i];
							if (m_pDB) m_pDB->updPropertySortOrder(rec.getPropID_pk(),rec.getPropObjectID_pk(),rec.getPropSortOrder());
						}	// for (UINT i = 0;i < m_vecELV_properties.size();i++)
					}	// if (sortOrderDir != SORT_NONE)
					populateProperties();
					*/
					//----------------------------------------------------------------------------------
				}
			}	// if (::MessageBox(this->GetSafeHwnd(),m_sMsgChangeSortOrder,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			else // Just repopulate, no change; 091014 p�d
				populateProperties();
		}
		break;

	};
}

void CPropertiesFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();

	if (pRow == NULL) return;
	// Only on up/down arrow keys; 080428 p�d
	if (lpNMKey->nVKey == VK_UP || lpNMKey->nVKey == VK_DOWN)
	{
		setActivePropertyInReport();
	}
	else if (lpNMKey->nVKey == VK_DELETE && (GetKeyState(VK_CONTROL) & 0x8000))
	{
		if ((GetKeyState(VK_SHIFT) & 0x8000) == 0)
		{
			CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pWnd != NULL)
			{
				pWnd->doRemoveProperties(1);
				m_wndReportProperties.SetFocusedRow(pRow);
			}
		}
		else if ((GetKeyState(VK_SHIFT) & 0x8000) != 0)
		{
			CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pWnd != NULL)
			{
				pWnd->doRemoveProperties(2);
			}
		}
	}

}

void CPropertiesFormView::OnReportHeaderColumnRClick(NMHDR * pNotifyStruct, LRESULT * result)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�					
	//menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupBy);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);


	// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�					
	/*
	if (m_wndReportProperties.IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}*/

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELDCHOOSER:
		{
			CMDI1950ForrestNormFrame* pWnd = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
			if (pWnd != NULL)
			{
				BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
				pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
			}
		}
		break;

		/*
		// HMS-136 krash pga grupperingsf�lt s� tar ej hand om det eftersom det menyalternativet  plockats bort 20250204 J�					
		case ID_SHOW_GROUPBOX:
			BOOL test=!m_wndReportProperties.IsGroupByVisible();
			m_wndReportProperties.ShowGroupBy(test);
		break;
		*/

	}
	menu.DestroyMenu();
}

void CPropertiesFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result)
{
	CString S;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu main_menu;
	CMenu sub_menu;
	VERIFY(main_menu.CreatePopupMenu());
	VERIFY(sub_menu.CreatePopupMenu());

	sub_menu.AppendMenu(MF_STRING, ID_TOOLS_DO_PROP_GROUPINGS,global_langMap[IDS_STRING226540]);
	sub_menu.AppendMenu(MF_STRING, ID_TOOLS_DO_PROP_GROUPINGS2,global_langMap[IDS_STRING226541]);
	main_menu.AppendMenu(MF_POPUP, reinterpret_cast<UINT_PTR>(sub_menu.m_hMenu),m_sPopMenuCreateLOwnerGroups);
//	main_menu.AppendMenu(MF_STRING, ID_TOOLS_DO_PROP_GROUPINGS,(m_sPopMenuCreateLOwnerGroups));
// Commented out 2009-03-20 p�d  (not used PL)
//	main_menu.AppendMenu(MF_STRING, ID_TOOLS_SAVE_PROP_GROUPING,(m_sPopMenuSaveLOwnerGroups));
	main_menu.AppendMenu(MF_STRING, ID_TOOLS_DEL_PROP_GROUPING,(m_sPopMenuDelLOwnerGroups));
	main_menu.AppendMenu(MF_SEPARATOR);
	main_menu.AppendMenu(MF_STRING, ID_TOOLS_DO_PROP_SORT_ORDER,(m_sPopMenuCreateSortOrder));
/*	Commecnted out 2009-01-29 P�D
		Don't use this option (for now); 090129 p�d
	main_menu.AppendMenu(MF_SEPARATOR);
	// create main menu items
	if (m_vecReports.size() > 0)
	{
		for (UINT i = 0;i < m_vecReports.size();i++)
		{
			m_recReports = 	m_vecReports[i];
			sub_menu.AppendMenu(MF_STRING, i+1, m_recReports.getCaption());
		}
	}
	main_menu.AppendMenu(MF_POPUP, reinterpret_cast<UINT_PTR>(sub_menu.m_hMenu),(m_sPopMenuReportByGrpID));
*/
	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&main_menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// Handle menu command
	switch (nMenuResult)
	{
		case ID_TOOLS_DO_PROP_GROUPINGS :

			groupPropertiesOnLandOwners();
		break;

		case ID_TOOLS_DO_PROP_GROUPINGS2 :
			groupPropertiesOnPropertyNumbers();
		break;
/*
		case ID_TOOLS_SAVE_PROP_GROUPING :
			savePropertyGroupings();
		break;
*/
		case ID_TOOLS_DEL_PROP_GROUPING :
			removePropertyGroupings();
		break;

		case ID_TOOLS_DO_PROP_SORT_ORDER :
			setPropertySortOrder();
		break;
/*
		default:
			// Make sure we're within limits of m_vecReports; 081105 p�d
			if (nMenuResult-1 >= 0 && nMenuResult-1 < m_vecReports.size())
			{
				m_recReports = 	m_vecReports[nMenuResult-1];
				runPrintout(m_recReports);
			}
		break;
*/
	}
	main_menu.DestroyMenu();
	sub_menu.DestroyMenu();
}

// CPropertiesFormView diagnostics


// CPropertiesFormView message handlers

void CPropertiesFormView::setupReportProperties(void)
{
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		if (m_xml.Load(m_sLangFN))
		{
				if (m_wndReportProperties.GetSafeHwnd() != NULL)
				{
					m_sMsgCap = (m_xml.str(IDS_STRING229));
					m_sMsgPropertyName = (m_xml.str(IDS_STRING1140));
					m_sMsgRemoveProperty.Format(_T("%s\n%s\n\n%s\n\n"),
						(m_xml.str(IDS_STRING1141)),
						(m_xml.str(IDS_STRING1142)),
						(m_xml.str(IDS_STRING114)));

					m_sMsgRemoveProperty2.Format(_T("%s\n%s\n%s\n%s"),
						(m_xml.str(IDS_STRING1141)),
						(m_xml.str(IDS_STRING1142)),
						(m_xml.str(IDS_STRING1143)),
						(m_xml.str(IDS_STRING1144)));

					m_sMsgRemoveProperty3.Format(_T("%s\n%s\n%s\n\n%s"),
						(m_xml.str(IDS_STRING1141)),
						(m_xml.str(IDS_STRING1142)),
						(m_xml.str(IDS_STRING1145)),
						(m_xml.str(IDS_STRING114)));

					m_sLoggMsg1.Format(_T("%s\n%s\n\n%s\n\n"),
						(m_xml.str(IDS_STRING4050)),
						(m_xml.str(IDS_STRING4051)),
						(m_xml.str(IDS_STRING4052)));

					m_sMsgAddEvaluestand = (m_xml.str(IDS_STRING410));
					m_sMsgRemoveEvaluestand = (m_xml.str(IDS_STRING411));
					m_sMsgRemoveCruise = (m_xml.str(IDS_STRING412));

					m_sMsgRemoveTraktPropConenction.Format(_T("%s\n%s\n"),
						(m_xml.str(IDS_STRING3206)),
						(m_xml.str(IDS_STRING3207)));

					m_sMsgMatchAllStandsToProperties.Format(_T("%s\n\n%s\n%s\n\n%s\n\n"),
						(m_xml.str(IDS_STRING6008)),
						(m_xml.str(IDS_STRING6009)),
						(m_xml.str(IDS_STRING6010)),
						(m_xml.str(IDS_STRING6011)));

					m_sMsgChangeSortOrder = m_xml.str(IDS_STRING22688);
					m_sMsgNotallowedSortNr=m_xml.str(IDS_STRING22689);
				
					m_sStatusBarStatus = (m_xml.str(IDS_STRING2270));
					//m_sStatusBarShowLogBook = (m_xml.str(IDS_STRING2275));
					//m_sStatusBarProperty = (m_xml.str(IDS_STRING2277));
					m_sStatusBarSavingEvalStand = (m_xml.str(IDS_STRING5900));
					m_sStatusBarRemoveEvalStand = (m_xml.str(IDS_STRING5901));
					m_sStatusBarRecalcEvalStand = (m_xml.str(IDS_STRING5902));

					m_sLoggHeader = (m_xml.str(IDS_STRING4053));
					m_sLoggObject = (m_xml.str(IDS_STRING4054));
					m_sLoggProperty = (m_xml.str(IDS_STRING40540));
					m_sLoggCreated = (m_xml.str(IDS_STRING4055));
					m_sLoggPrlMatch = (m_xml.str(IDS_STRING4056));
					m_sLoggCostMatch = (m_xml.str(IDS_STRING4057));
					m_sLoggDCLSMatch = (m_xml.str(IDS_STRING4058));
					m_sLoggObjPrl = (m_xml.str(IDS_STRING4059));
					m_sLoggObjCost = (m_xml.str(IDS_STRING4060));
					m_sLoggObjDCLS = (m_xml.str(IDS_STRING4061));
					m_sLoggTraktPrl = (m_xml.str(IDS_STRING4062));
					m_sLoggTraktCost = (m_xml.str(IDS_STRING4063));
					m_sLoggTraktDCLS = (m_xml.str(IDS_STRING4064));
					m_sLogg_cm = (m_xml.str(IDS_STRING4065));

					m_sLogCap = (m_xml.str(IDS_STRING2300));
					m_sCancel = (m_xml.str(IDS_STRING2301));
					m_sPrintOut = (m_xml.str(IDS_STRING2302));
					m_sSaveToFile = (m_xml.str(IDS_STRING2303));
					m_sPopMenuCreateLOwnerGroups = (m_xml.str(IDS_STRING22654));
					m_sPopMenuSaveLOwnerGroups = (m_xml.str(IDS_STRING22655));
					m_sPopMenuDelLOwnerGroups = (m_xml.str(IDS_STRING22670));
					m_sPopMenuReportByGrpID = (m_xml.str(IDS_STRING2304));
					m_sPopMenuCreateSortOrder = (m_xml.str(IDS_STRING22687));

					// Add strings; 080416 p�d
					m_sFieldChooser = (m_xml.str(IDS_STRING105));
					
					// HMS-136 krash pga grupperingsf�lt s� plockatr bort det menyalternativet 20250204 J�	
					//m_sGroupBy = (m_xml.str(IDS_STRING1051));

					m_sNumOfLogEntrySort = (m_xml.str(IDS_STRING112));

					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					m_wndReportProperties.SetImageList(&m_ilIcons);

					// HMS-136 krash pga grupperingsf�lt s� s�tter det till false, 
					// eller nej det tar vi hand om i installation och nollar registret i installationen ist�llet 20250204 J�
					//m_wndReportProperties.ShowGroupBy( TRUE );


					m_wndReportProperties.ShowWindow( SW_NORMAL );

//					m_wndReportProperties.SetFreezeColumnsCount( 4 );	// Status and Propertyname

					// Set Dialog caption; 070219 p�d
					// Open property log
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_0, (m_xml.str(IDS_STRING106)), 50));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_CENTER);
					pCol->SetAlignment(DT_LEFT);
					// Document icon
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_1, (m_xml.str(IDS_STRING107)), 50));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_CENTER);
					pCol->SetAlignment(DT_LEFT);
					// Status
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_2, (m_xml.str(IDS_STRING3017)), 120));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();
					// "L�n"
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_3, (m_xml.str(IDS_STRING2311)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					// "Kommun"
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_4, (m_xml.str(IDS_STRING2312)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					// Property name
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_5, (m_xml.str(IDS_STRING3000)), 150));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					// Property number
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_6, (m_xml.str(IDS_STRING3001)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Volume m3sk
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_7, (m_xml.str(IDS_STRING3002)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Areal (ha)
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_8, (m_xml.str(IDS_STRING3003)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Num.of trees
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_9, (m_xml.str(IDS_STRING3004)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Num.of stands
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_10, (m_xml.str(IDS_STRING3005)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Wood volume (Gagnvirke)
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_11, (m_xml.str(IDS_STRING3006)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Wood value
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_12, (m_xml.str(IDS_STRING3007)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Costs
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_13, (m_xml.str(IDS_STRING3008)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// NETTO (Rotpost)
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_14, (m_xml.str(IDS_STRING3009)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Landvalue
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_15, (m_xml.str(IDS_STRING3010)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Early cutting
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_16, (m_xml.str(IDS_STRING3011)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Storm- and Drydamage
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_17, (m_xml.str(IDS_STRING3012)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Rand trees
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_18, (m_xml.str(IDS_STRING3013)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Voluntary deal
					CString sVolDeal;
					if (CTransaction_elv_object *pObj = getActiveObject())
					{
						int nTypeOfNet;
						double fPriceBase, fMaxPercent, fPercent, fPercentOfPriceBase;
						m_pDB->getObjVolDealValues(pObj->getObjID_pk(), &nTypeOfNet, &fPriceBase, &fMaxPercent, &fPercent, &fPercentOfPriceBase);
						if (nTypeOfNet == TYPEOFNET_LOCAL)
						{
							sVolDeal = m_xml.str(IDS_STRING3014) + _T(" ") + m_xml.str(IDS_STRING3038);
						}
						else if (nTypeOfNet == TYPEOFNET_SVENSKAKRAFTNAT)
						{
							sVolDeal = m_xml.str(IDS_STRING3036) + _T(" ") + m_xml.str(IDS_STRING3038);
						}
						else if (nTypeOfNet == TYPEOFNET_SWEDISHENERGY)	//Svensk energi 2014
						{
							sVolDeal = m_xml.str(IDS_STRING3037) + _T(" ") + m_xml.str(IDS_STRING3038);
						}
						else //Svensk energi 2019
						{
							sVolDeal = m_xml.str(IDS_STRING3037) + _T(" ") + m_xml.str(IDS_STRING3038);
						}
					}
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_19, sVolDeal, 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Higher cutting costs
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_20, (m_xml.str(IDS_STRING3015)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Other costs
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_21, (m_xml.str(IDS_STRING3016)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT);
					// Group identity
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_22, (m_xml.str(IDS_STRING3018)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_UPPERCASE; 
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT); 
					// Type of compenstion to LandOwner
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_23, (m_xml.str(IDS_STRING3019)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->SetAlignment(DT_LEFT); 
					// Sortorder
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_24, (m_xml.str(IDS_STRING3020)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT); 

					// Grot volume
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_25, (m_xml.str(IDS_STRING3021)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT); 

					// Grot value
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_26, (m_xml.str(IDS_STRING3022)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT); 

					// Grot cost
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_27, (m_xml.str(IDS_STRING3023)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
					pCol->SetAlignment(DT_RIGHT); 

					// Search ID; added 2012-08-29 p�d
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_28, (m_xml.str(IDS_STRING3024)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment(DT_CENTER); 

					// Mottagarreferens; added 2012-11-21 p�d
					pCol = m_wndReportProperties.AddColumn(new CXTPReportColumn(COLUMN_29, (m_xml.str(IDS_STRING3025)), 80));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment(DT_CENTER); 

					m_wndReportProperties.GetReportHeader()->AllowColumnRemove(TRUE);
					//Testar att s�tta att kunna v�lja flera feature 2456 20111101 J�, f�r att kunna radera flera fastigheter
					m_wndReportProperties.SetMultipleSelection( TRUE );
					m_wndReportProperties.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReportProperties.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReportProperties.FocusSubItems(TRUE);
					m_wndReportProperties.AllowEdit(TRUE);
					m_wndReportProperties.GetPaintManager()->SetFixedRowHeight(FALSE);

				}	// if (m_wndReportProperties.GetSafeHwnd() != NULL)
			}	// if (m_xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))
}

void CPropertiesFormView::addStatusConstraints(void)
{
	int nPropID = -1,nStatusNum = 0;
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReportProperties.GetColumns();
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	if (pColumns != NULL)
	{
		// Add Status-types; 080410 p�d
		CXTPReportColumn *pStatusCol = pColumns->Find(COLUMN_2);
		if (pStatusCol != NULL)
		{

			// Get constraints for Specie column and remove all items; 080410 p�d
			pCons = pStatusCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			if (m_arrStatus.GetCount() > 0)
			{
				for (int i = 0;i < m_arrStatus.GetCount();i++)
				{
					pStatusCol->GetEditOptions()->AddConstraint((m_arrStatus.GetAt(i)),i);
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)

}

void CPropertiesFormView::changeVoluntaryDealCaption(CString caption)
{
	if (CXTPReportColumns *pCols = m_wndReportProperties.GetColumns())
	{
		if (CXTPReportColumn *pCol = pCols->Find(COLUMN_19))
		{
			pCol->SetCaption(caption);
		}
	}
}

// Add to class data member: m_vecELV_properties; 080415 p�d
void CPropertiesFormView::getPropertiesFromDB(int obj_id)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(obj_id,m_vecELV_properties);
	}	// if (m_pDB != NULL)
}

void CPropertiesFormView::savePropertiesToDB(CTransaction_elv_properties &rec)
{
	if (m_pDB != NULL)
	{
		if (!m_pDB->addProperty(rec))
			m_pDB->updProperty(rec);
	}	// if (m_pDB != NULL)
}

void CPropertiesFormView::getNumOfEntriesInLogBookPerPropertyFromDB(int obj_id)
{
	CTransaction_elv_properties rec;
	int nNumOfEntries = -1;
	m_mapNumOfLogBookEntriesPerProperty.clear();
	/*
	if (m_pDB != NULL && m_vecELV_properties.size() > 0)
	{
		for (UINT i = 0;i < m_vecELV_properties.size();i++)
		{
			rec = m_vecELV_properties[i];
			//if (rec.getPropStatus() <= STATUS_ADVICED)
			//{
				nNumOfEntries = m_pDB->getNumOfEntriesForPropertyInLogBook(rec.getPropID_pk(),obj_id);
			 m_mapNumOfLogBookEntriesPerProperty[rec.getPropID_pk()] = nNumOfEntries;
			//}
		}	// for (UINT i = 0;i < m_vecELV_properties.size();i++)
	}	// if (m_pDB != NULL && m_vecELV_properties.size() > 0)
*/
	if (m_pDB)
		m_pDB->getNumOfEntriesForPropertyInLogBook(obj_id,m_mapNumOfLogBookEntriesPerProperty);
}

CEvaluatedData *CPropertiesFormView::getEvaluatedView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(TAB_EVALUATED);
	if (m_tabManager)
	{
		CEvaluatedData* pView = DYNAMIC_DOWNCAST(CEvaluatedData, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CEvaluatedData, pView);
		return pView;
	}
	return NULL;
}

CCruisingData *CPropertiesFormView::getCruisingView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(TAB_CRUISING);
	if (m_tabManager)
	{
		CCruisingData* pView = DYNAMIC_DOWNCAST(CCruisingData, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CCruisingData, pView);
		return pView;
	}
	return NULL;
}

// This method is checkin' all cruise stands added to Property in Object; 090421 p�d
BOOL CPropertiesFormView::isThereOnlyRandTrees(void)
{
	vecTransactionDCLSTree vecTrees;
	CTransaction_dcls_tree recTree;

	CTransaction_elv_cruise recCruiseID;
	vecTransaction_elv_cruise vecELVObjectCruise;

	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRec = (CELVPropertyReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				if (canWeCalculateThisProp_cached(pRec->getRecELVProp().getPropStatus()))
				{
					m_pDB->getObjectCruises(pRec->getRecELVProp().getPropID_pk(),pRec->getRecELVProp().getPropObjectID_pk(),vecELVObjectCruise);
					if (vecELVObjectCruise.size() > 0)
					{
						for (UINT i = 0;i < vecELVObjectCruise.size();i++)
						{
							recCruiseID = vecELVObjectCruise[i];
							if (m_pDB != NULL)
								m_pDB->getDCLSTrees(recCruiseID.getECruID_pk() /* TraktID */,vecTrees);
							
							// If there's no trees at all, we'll also return TRUE, i.e. only randtrees; 090330 p�d
							if (vecTrees.size() > 0)
							{
								for (UINT i = 0;i < vecTrees.size();i++)
								{
									recTree = vecTrees[i];
									// Check for "Tr�d i gatan"; 090330 p�d
									if (recTree.getNumOf() > 0)
									{
										vecTrees.clear();
										vecELVObjectCruise.clear();
										return FALSE;
									}	// if (recCruise.getECruNumOfTrees() > 0)
								}	// for (UINT i = 0;i < vecELVObjectCruise.size();i++)
							}	// if (vecELVObjectCruise.size() > 0)
						}	// if (canWeCalculateThisProp_cached(pRec->getRecELVProp().getPropStatus()))
					}	// for (int i = 0;i < pRows->GetCount();i++)
					vecTrees.clear();
					vecELVObjectCruise.clear();
				}	// if (vecELVObjectCruise_ids.size() > 0)
			}	// if ((pRec = (CELVPropertyReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	return TRUE;

}

BOOL CPropertiesFormView::isThereOnlyRandTreesInStand(CTransaction_elv_cruise &rec)
{
	BOOL bReturn = TRUE;
	vecTransactionDCLSTree vecTrees;
	CTransaction_dcls_tree recTree;

	if (m_pDB != NULL)
			m_pDB->getDCLSTrees(rec.getECruID_pk() /* TraktID */,vecTrees);
		
	// If there's no trees at all, we'll also return TRUE, i.e. only randtrees; 090330 p�d
	if (vecTrees.size() > 0)
	{
		for (UINT i = 0;i < vecTrees.size();i++)
		{
			recTree = vecTrees[i];
			// Check for "Tr�d i gatan"; 090330 p�d
			if (recTree.getNumOf() > 0)
			{
				bReturn = FALSE;
				break;
			}	// if (recCruise.getECruNumOfTrees() > 0)
		}	// for (UINT i = 0;i < vecELVObjectCruise.size();i++)
	}

	return bReturn;

}

void CPropertiesFormView::getObjectEvaluatedNotCalculated(void)	
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getObjectEvaluationNotCalculatesForObject(m_recActiveProperty.getPropObjectID_pk(),m_vecElvEvaluatedNotCalc);
		}
	}
}


BOOL CPropertiesFormView::calculateThisEvaluation(CTransaction_eval_evaluation& rec)
{
	// Do a complete recalculation, no matter what ...; 090525 p�d
	if (m_bDoCompleteReCalc) return TRUE;
	// No cruisees to calculate, already calculated; 090525 p�d
	if (m_vecElvEvaluatedNotCalc.size() == 0) return FALSE;

	// Cruisees to calculate; 090525 p�d
	if (m_vecElvEvaluatedNotCalc.size() > 0)
	{
		for (UINT i = 0;i < m_vecElvEvaluatedNotCalc.size();i++)
		{
			if (rec.getEValID_pk() == m_vecElvEvaluatedNotCalc[i].getEValID_pk() &&
					rec.getEValObjID_pk() == m_vecElvEvaluatedNotCalc[i].getEValObjID_pk() &&
					rec.getEValPropID_pk()  == m_vecElvEvaluatedNotCalc[i].getEValPropID_pk())
				return TRUE;
		}
	}

	return FALSE;
}

void CPropertiesFormView::getPropActionStatusFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getPropertyActionStatus(m_vecPropStatus);
	}	// if (m_pDB != NULL)
}


// S�tt P30 max SI fr�n 40 till v�rde enligt skogsnorm
void CPropertiesFormView::setP30NN_SIMax(vecObjectTemplate_p30_nn_table& vec)
{
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			if (vec[i].getAreaIdx() == 0 || vec[i].getAreaIdx() == 1 || vec[i].getAreaIdx() == 2)
			{
				if (vec[i].getSpcID() == 1 && vec[i].getTo() == 40) vec[i].setTo(30);
				else if (vec[i].getSpcID() == 2 && vec[i].getTo() == 40) vec[i].setTo(32);
				else if (vec[i].getSpcID() == 3 && vec[i].getTo() == 40) vec[i].setTo(30);
			}
			//Korrigerat kontroll av max SI f�r att matcha mot kanttr�sers�ttningstabellens "Ers�ttning i kr per ha f�r mark och f�rtidig avverkning vid ett rotv�rde av 10 kr per m3sk f�r 30 cm tr�det"
			//maxv�rden tabell 4 och 4a skall kontrolleras mot samma maxv�rden eftersom det bara finns en kanttr�dsers�ttningstabell f�r tillv�xtomr�de 4#4197 J� 20141107
			//Innan gjordes kontrollen f�r tillv�xtomr�de 1,2,3,4a mot samma maxv�rden vilket blir fel f�r om p30 prislista f�r omr�de 4A anv�nds och SI �r h�gre �n maxv�rden i kontrollen
			else if (vec[i].getAreaIdx() == 3 || vec[i].getAreaIdx() == 4)
			{
				if (vec[i].getSpcID() == 1 && vec[i].getTo() == 40) vec[i].setTo(30);
				else if (vec[i].getSpcID() == 2 && vec[i].getTo() == 40) vec[i].setTo(36);
				else if (vec[i].getSpcID() == 3 && vec[i].getTo() == 40) vec[i].setTo(30);
			}
			else if (vec[i].getAreaIdx() == 5)
			{
				if (vec[i].getSpcID() == 1 && vec[i].getTo() == 40) vec[i].setTo(32);
				else if (vec[i].getSpcID() == 2 && vec[i].getTo() == 40) vec[i].setTo(40);
				else if (vec[i].getSpcID() == 3 && vec[i].getTo() == 40) vec[i].setTo(30);
			}
		}
	}
}


void CPropertiesFormView::getObjectp30Speciesinfo(vecTransactionSpecies& vecP30Species,int nObjid)
{
	if (m_pDB != NULL)
	{
		m_pDB->getP30SpeciesInfo(vecP30Species,nObjid);
	}
}


BOOL CPropertiesFormView::calculateStandsForProperty(CTransaction_elv_properties& prop,BOOL use_dialog,CStringArray& error_log,short get_data_as, CTransaction_elv_object *pObj /*=NULL*/)
{
	CString S;
	int nReturn = -1;
	//int nPrlCnt = 1;
	//int nCostCnt = 1;
	//int nDCLSCnt = 1;
	int nObjID = -1;
	int nPropID = -1;
	int nTraktID = -1;
	int nNumOfStands = 0;
	double fAreal = 0.0;
	long lNumOfTrees = 0;
	double fVolumeM3Sk = 0.0;
	double fTimberVolume = 0.0;
	double fTimberValue = 0.0;
	double fTimberCost = 0.0;
	double fRotNetto = 0.0;
	double fStormDryValue = 0.0;
	double fRandTreesValue = 0.0;
	double fRandTreesVolume = 0.0;
	//CString sObjectName;
	//CString sPrlName = _T("");
	//int nPrlTypeOf = -1;
	//CString sPrlXML = _T("");
	//CString sCostName = _T("");
	//int nCostTypeOf = -1;
	//CString sCostXML = _T("");
	CString sName,sDoneBy,sNotes;
	//double fDiamClass = DEF_DIAMCLASS;	// Default diameterclass
	int nIsDoReduceRotpost = 1;	// 1 = No
	int nIsDontUseSampletrees = 1;	// 1 = Use sampletrees
	int nWSide = -1;	// Set Side on "breddning"; 080922 p�d
	CString sArea;				// 2009-09-21 p�d
	int nAreaIndex = -1;	// 2009-09-21 p�d
	double fCruiseWidth = 0.0;
	int nCruiseType = -1;
	//int nOnlyRotpost = -1;	//tagit bort igen #3260 reopen
	double fGrotVolume = 0.0;
	double fGrotValue = 0.0;
	double fGrotCost = 0.0;

	BOOL bIsLogBook = FALSE;
	BOOL bRemoveDeSelected = FALSE;
	BOOL bAddCruiseOK = FALSE;
	CStringArray arrLandValueLog;
	//CStringArray arrMatchLog;
	//CStringArray arrMatchLogPrl;
	//CStringArray arrMatchLogCost;
	//CStringArray arrMatchLogDCLS;
	CTransaction_elv_object recObject;
	CTransaction_trakt_misc_data recTraktMiscData;
	CTransaction_trakt recTrakt;
	CTransaction_elv_properties recElvProperty;
	CTransaction_elv_return_storm_dry recReturnStormDry;
	CTransaction_elv_return_land_and_precut recLandValueAndPreCut;
	CTransaction_elv_m3sk_per_specie recM3SkInOutPerSpc;

	vecTransactionTrakt vecTrakts;
	vecTransactionTrakt vecDeSelectedTrakts;
	vecTransactionTraktData vecTraktData;
	vecObjectTemplate_p30_table vecP30_table;
	vecObjectTemplate_p30_nn_table vecP30_nn_table;	// Added 2009-09-21 p�d
	vecTransaction_elv_m3sk_per_specie vecM3Sk_in_out;
	vecTransaction_elv_cruise vecELVCruise;
	vecTransactionSampleTree vecRandTrees;
	

	vecTransactionSpecies vecSpecies;	// Added 2008-11-07 P�D
	vecTransactionSpecies vecP30Species;	
	CTransaction_species recSpecie;		// Added 2008-11-07	P�D

//	vecReduceRotpost m_vecReduceRotpost;

	CTransaction_eval_evaluation recEvaluation;
	mapDouble mapRandTreesVolume;
	CString sMsgRemove;
	CStandsMatchedToPropDlg *pDlg = NULL;

	recElvProperty = prop;

	if (m_pDB != NULL)
	{
		if (use_dialog)
		{
			pDlg = new CStandsMatchedToPropDlg();
			if (pDlg != NULL)
			{
				pDlg->setDBConnection(m_pDB);
				pDlg->setELVPropertyRecord(recElvProperty);
				if ((nReturn = pDlg->DoModal()) == IDOK)
				{
					//-----------------------------------------------------------------
					// Get trakts from dialog; 080506 p�d
					vecTrakts = pDlg->getTrakts();
				}	// if ((nReturn = pDlg->DoModal()) == IDOK)
				delete pDlg;
			}	// if (pDlg != NULL)
			// Check if user Canceld dialog; 080604 p�d
			if (nReturn == IDCANCEL)
				return FALSE;
		}	// if (use_dialog)
		else
		{
			vecTrakts.clear();
			if (get_data_as == 1)
				m_pDB->getTrakts_match_stand_by_elv_cruise(recElvProperty.getPropObjectID_pk(),recElvProperty.getPropID_pk(),vecTrakts);
			else if (get_data_as == 2)
				m_pDB->getTrakts_match_stand(recElvProperty.getPropObjectID_pk(),recElvProperty.getPropID_pk(),vecTrakts);
		}
		// Try to collect some SUM. data for this property.
		// I.e. SUM data on Trakt(s) connected to this property; 080430 p�d
		nObjID = recElvProperty.getPropObjectID_pk();
		nPropID = recElvProperty.getPropID_pk();
		// Make sure we have the latest Object information; 080508 p�d
		if( pObj ) // Use cached value if set; Optimization by Peter
		{
			recObject = *pObj;
		}
		else
		{
			m_pDB->getObject(nObjID,recObject);
		}
		// Load P30 table for Object; 080508 p�d
		TemplateParser *parser = new TemplateParser();
		if (parser)
		{
			if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
			{
				if (parser->LoadFromBuffer(recObject.getObjP30XML()))
				{
					parser->getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
				}
			}
			else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
			{
				if (parser->LoadFromBuffer(recObject.getObjP30XML()))
				{
					parser->getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
					setP30NN_SIMax(vecP30_nn_table);
				}
			}
			else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
			{
				if (parser->LoadFromBuffer(recObject.getObjP30XML()))
				{
					parser->getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
					setP30NN_SIMax(vecP30_nn_table);
				}
			}
			delete parser;
		}

		// Set Object inforamtion; 080508 p�d
//		sObjectName = (recObject.getObjectName());
//		sPrlName = (recObject.getObjNameOfPricelist());
//		nPrlTypeOf = recObject.getObjTypeOfPricelist();
//		sPrlXML = (recObject.getObjPricelistXML());
//		sCostName = (recObject.getObjNameOfCosts());
//		nCostTypeOf = recObject.getObjTypeOfCosts();
//		sCostXML = (recObject.getObjCostsXML());
		// Make sure there's a diametercalss added in Object; 080507 p�d
		//if (recObject.getObjDCLS() > 0.0)
		//	fDiamClass = recObject.getObjDCLS();

		//****************************************************************************
		// Get cruises for property; 080922 p�d
		// Mostly used to get information on wich side the cruise
		// is on. I.e. match to Width1 or Width 2, for "breddning"; 080922 p�d
		// New; get information on CruiseWidth; 090625 p�d
		m_pDB->getObjectCruises(nPropID,nObjID,vecELVCruise);

		//----------------------------------------------------------------------------
		// Remove any ObjectCruises already entered; 080506 p�d
		m_pDB->delObjectCruise(nPropID,nObjID,CRUISE_TYPE_1);


		if (vecTrakts.size() > 0)
		{
			//----------------------------------------------------------------------------
			// Start reading trakt data from Trakts in CStandsMatchedToPropDlg; 080508 p�d
			for (UINT i = 0;i < vecTrakts.size();i++)
			{
				fTimberVolume = fTimberValue = fTimberCost = fRotNetto = fGrotVolume = fGrotValue = fGrotCost = 0.0;
				recTrakt = vecTrakts[i];

				// Try to match an read width set in cruise (ELV); 090625 p�d
				for (UINT i = 0;i < vecTrakts.size();i++)
				{
					// I'll just do a match here, for vecELVCruise and vecTrakts,
					// and set width1 and width2 "breddning ny morm"; 080922 p�d
					if (vecELVCruise.size() > 0)
					{
						for (UINT i1 = 0;i1 < vecELVCruise.size();i1++)
						{
							if (vecELVCruise[i1].getECruID_pk() == recTrakt.getTraktID())
							{
								fCruiseWidth = vecELVCruise[i1].getECruWidth();
								recTrakt.setCruiseWidth(fCruiseWidth);	// Use this to retrive value in UMLandValueNorm New Norm; 090625 p�d
								vecTrakts[i].setCruiseWidth(fCruiseWidth);
								nCruiseType = vecELVCruise[i1].getECruType();	//	Added 2009-09-07 P�D
								//nOnlyRotpost = vecELVCruise[i1].getECruUseForInfr();	// Added 2012-06-28 P�D		//tagit bort igen #3260 reopen
								break;
							}	// if (vecELVCruise[i1].getECruID_pk() == recTrakt.getTraktID() &&
						}	// for (UINT i1 = 0;i1 > vecELVCruise.size();i1++)
					}	// if (vecELVCruise.size() > 0)
				}	// for (UINT i = 0;i < vecTrakts.size();i++)
				if (nCruiseType <= CRUISE_TYPE_1)
				{
					// Set TraktID for add/update in DB table; 090626 p�d
					nTraktID = recTrakt.getTraktID();

					// It's ALWAYS the WSide set in Trakt that't the default; 090528 p�d
					nWSide = recTrakt.getWSide();

					// Get Traktdata from table "esti_trakt_data_table"; 080508 p�d
					// Info on e.g. "Tr�dslagsblandning"; 080508 p�d
					m_pDB->getTraktData(vecTraktData,nTraktID,1);	// 1 = "Uttag"
					// Get aggregate data for m3sk "I gatan" and "Kanttr�d", set per specie; 080508 p�d
					vecM3Sk_in_out.clear();
					m_pDB->getObjPropTrakt_m3sk_volumes_per_spc(nTraktID,recTrakt.getTraktAreal(),vecM3Sk_in_out);
					// Get aggregate data for "Kanttr�d" from "esti_trakt_sample_trees_table"; 080512 p�d
					vecRandTrees.clear();
					m_pDB->getLandValueRandTrees(nTraktID,vecRandTrees);

					//HMS-94 H�mtar p30 tr�dslagsinfo sparat per object och per tr�dslag
					getObjectp30Speciesinfo(vecP30Species,recObject.getObjID_pk());

					// Get aggregate information for Trakt; 080506 p�d				
					m_pDB->getObjPropTrakt_numof_volume(nTraktID,&lNumOfTrees,&fVolumeM3Sk);
					m_pDB->getObjPropTrakt_rotpost(nTraktID,&fTimberVolume,&fTimberValue,&fTimberCost,&fRotNetto);		
					m_pDB->getObjProp_grot(nTraktID,&fGrotVolume,&fGrotValue,&fGrotCost);
				
					// I think we should try to do some "Intr�ngsv�rdering", here. 
					// I.e. calculate "Storm- och Torkskador" and "Kanttr�d"; 080508 p�d
					arrLandValueLog.RemoveAll();
					
					doUMForrestNorm(1,	// "V�rdering"
													m_mapForrestNormErrText,
													recObject,						// In-data
													recTrakt,							// In-data not used
													vecP30_table,					// In-data
													vecP30_nn_table,			// In-data for new foest norm; 090421 p�d
													vecTraktData,					// In-data not used
													vecM3Sk_in_out,				// In-data not used
													vecRandTrees,					// In-data not used
													recEvaluation,				// In-data
													vecTrakts,
													vecP30Species,
													arrLandValueLog,			// Return data
													recReturnStormDry,		// Return data not used
													recLandValueAndPreCut,// Return data not used
													mapRandTreesVolume);	// Return data not used

					// Defult settings; 080624 p�d
					nIsDoReduceRotpost = 1; //(m_vecReduceRotpost[i].bIsYes ? 1 : 0);	// 1 = No
					nIsDontUseSampletrees = 0; // Use sampletrees (m_vecReduceRotpost[i].bDontUseSampleTrees ? 1 : 0);

					// Check that nWSide > 0; 080924 p�d
					if (nWSide <= 0) nWSide = 1;

					CTransaction_elv_cruise recCruise =	CTransaction_elv_cruise(
						nTraktID,
						nObjID,
						nPropID,
						recTrakt.getTraktNum(),
						recTrakt.getTraktName(),
						lNumOfTrees,
						recTrakt.getTraktAreal(),
						fVolumeM3Sk,
						fTimberVolume,
						fTimberValue,
						fTimberCost,
						fRotNetto,
						/*(nOnlyRotpost == 0 ?*/ recReturnStormDry.getStormDryVolume()/* : 0.0)*/,		//tagit bort igen #3260 reopen
						/*(nOnlyRotpost == 0 ?*/ recReturnStormDry.getStormDryValue()/* : 0.0)*/,
						/*(nOnlyRotpost == 0 ?*/ recReturnStormDry.getStormDrySpruceMix()/* : 0.0)*/,
						/*(nOnlyRotpost == 0 ?*/ recReturnStormDry.getAvgPriceFactor()/* : 0.0)*/,
						0.0,
						0.0,
						nWSide,	
						nIsDoReduceRotpost,
						0,
						fCruiseWidth,
						recTrakt.getTraktSIH100(),
						_T("0;0;0;"),
						fGrotVolume,
						fGrotValue,
						fGrotCost,
						recTrakt.getOnlyForStand(),
						recTrakt.getTraktTillfUtnyttj(),
						recReturnStormDry.getStormTorkInfo_SpruceMix(),	//HMS-50 Info om storm o tork 20200415 J�
						recReturnStormDry.getStormTorkInfo_SumM3Sk_inside(),
						recReturnStormDry.getStormTorkInfo_AvgPriceFactor(),
						recReturnStormDry.getStormTorkInfo_TakeCareOfPerc(),
						recReturnStormDry.getStormTorkInfo_PineP30Price(),
						recReturnStormDry.getStormTorkInfo_SpruceP30Price(),
						recReturnStormDry.getStormTorkInfo_BirchP30Price(),
						recReturnStormDry.getStormTorkInfo_CompensationLevel(),
						recReturnStormDry.getStormTorkInfo_Andel_Pine(),
						recReturnStormDry.getStormTorkInfo_Andel_Spruce(),
						recReturnStormDry.getStormTorkInfo_Andel_Birch(),
						recReturnStormDry.getStormTorkInfo_WideningFactor(),
						recLandValueAndPreCut.getMarkInfo_ValuePine(),
						recLandValueAndPreCut.getMarkInfo_ValueSpruce(),
						recLandValueAndPreCut.getMarkInfo_Andel_Pine(),
						recLandValueAndPreCut.getMarkInfo_Andel_Spruce(),
						recLandValueAndPreCut.getMarkInfo_P30_Pine(),
						recLandValueAndPreCut.getMarkInfo_P30_Spruce(),
						recLandValueAndPreCut.getMarkInfo_ReducedBy()
						); // TGL
					//------------------------------------------------------------------------
					bAddCruiseOK = m_pDB->addObjectCruise(recCruise);
					if (!bAddCruiseOK)	// Already in table, do update instead; 091016 p�d
						bAddCruiseOK =	m_pDB->updObjectCruise(recCruise);

					if (bAddCruiseOK)
					{
						//-------------------------------------------------------------------------------------
						// "H�mta tr�dslag fr�n Prislistan i Objektet"; 081107 p�d
						xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
						if (pPrlParser != NULL)
						{
							if (pPrlParser->loadStream(recObject.getObjPricelistXML()))
							{
								//---------------------------------------------------------------------------------
								// Get species in pricelist; 100104 p�d
								pPrlParser->getSpeciesInPricelistFile(vecSpecies);
							}
							delete pPrlParser;
						}
/*						
						PricelistParser *pPrlParser = new PricelistParser();
						if (pPrlParser != NULL)
						{
							if (pPrlParser->LoadFromBuffer(recObject.getObjPricelistXML()))
							{
								//---------------------------------------------------------------------------------
								// Get species in pricelist; 081107 p�d
								pPrlParser->getSpeciesInPricelistFile(vecSpecies);
							}
							delete pPrlParser;
						}
*/
						CTransaction_elv_cruise_randtrees rec;
						// Add volume and value to randtrees/specie
						// based on Species in vecM3Sk_in_out; 080512 p�d
						if (vecM3Sk_in_out.size() > 0)
						{
							for (UINT ii = 0;ii < vecM3Sk_in_out.size();ii++)
							{
								recM3SkInOutPerSpc = vecM3Sk_in_out[ii];
								// Only add "Kanttr�d" that has a volume; 080512 p�d
								if (recM3SkInOutPerSpc.getM3SkRandTrees() > 0.0)
								{
									
									// Try to find name of specie, by comparing 
									// recM3SkInOutPerSpc.getSpcID() to recSpecie.getSpcID()
									if (vecSpecies.size() > 0)
									{
										for (UINT iii = 0;iii < vecSpecies.size();iii++)
										{
											recSpecie = vecSpecies[iii];
											if (recSpecie.getSpcID() == recM3SkInOutPerSpc.getSpcID())
												break;
										}	// for (UINT iii = 0;iii < vecSpecies.size();iii++)
									}	// if (vecSpecies.size() > 0)

									//if (nOnlyRotpost == 0)	//tagit bort igen #3260 reopen
									{
										rec = CTransaction_elv_cruise_randtrees(recM3SkInOutPerSpc.getSpcID(),
																														nObjID,nPropID,nTraktID,
																														recM3SkInOutPerSpc.getSpcID(),
																														(recSpecie.getSpcName()),
																														recM3SkInOutPerSpc.getM3SkRandTrees(),
																														mapRandTreesVolume[recM3SkInOutPerSpc.getSpcID()]);
									}
									/*else if (nOnlyRotpost == 1)
									{
										rec = CTransaction_elv_cruise_randtrees(recM3SkInOutPerSpc.getSpcID(),
																														nObjID,nPropID,nTraktID,
																														recM3SkInOutPerSpc.getSpcID(),
																														(recSpecie.getSpcName()),
																														0.0, //recM3SkInOutPerSpc.getM3SkRandTrees(),
																														0.0); //mapRandTreesVolume[recM3SkInOutPerSpc.getSpcID()]);
									}*/
									m_pDB->addObjectCruiseRandtrees(rec);
								}	// if (recM3SkInOutPerSpc.getM3SkRandTrees() > 0.0)
							}	// for (UINT ii = 0;ii < vecM3Sk_in_out.size();ii++)
						}	// if (vecM3Sk_in_out.size() > 0)
					}	// if (m_pDB->updObjectCruise(recCruise)
					//------------------------------------------------------------------------

					// Do an update of "elv_cruise_table" on "Kanttr�d"; 080512 p�d
					m_pDB->getObjPropTraktCruise_randtrees(nTraktID,nObjID,nPropID,&fRandTreesValue,&fRandTreesVolume);
					m_pDB->updObjectCruise_randtrees(nTraktID,nPropID,nObjID,fRandTreesValue,fRandTreesVolume);
				}
			}	// for (UINT i = 0;i < vecTrakts.size();i++)

			//-----------------------------------------------------------------
			// Get aggregate information on Trakts per Property; 080430 p�d
			fStormDryValue = 0.0;
			fRandTreesValue = 0.0;
			m_pDB->getObjPropTraktSUM_stands_areal(nObjID,nPropID,&nNumOfStands,&fAreal);
			m_pDB->getObjPropTraktSUM_numof_volume(nObjID,nPropID,&lNumOfTrees,&fVolumeM3Sk);
			m_pDB->getObjPropTraktSUM_rotpost(nObjID,nPropID,&fTimberVolume,&fTimberValue,&fTimberCost,&fRotNetto);
			m_pDB->getObjPropTraktSUM_storm_dry_randtrees(nObjID,nPropID,&fStormDryValue,&fRandTreesValue);
			m_pDB->getObjPropTraktSUM_grot(nObjID,nPropID,&fGrotVolume,&fGrotValue,&fGrotCost);
			
			m_pDB->updObjPropTraktSUM_1(nPropID,nObjID,nNumOfStands,fAreal,lNumOfTrees,fVolumeM3Sk,fStormDryValue,fRandTreesValue);
			m_pDB->updObjPropTraktSUM_6(nPropID,nObjID,fTimberVolume,fTimberValue,fTimberCost,fRotNetto);
			m_pDB->updObjPropTraktSUM_grot(nPropID,nObjID,fGrotVolume,fGrotValue,fGrotCost);
			// Try to update Status2 in "elv_properties_table", to set selectect forrest norm; 080917 p�d
			if (recObject.getObjUseNormID() > -1 && recObject.getObjUseNormID() < 10)
				m_pDB->updPropertyStatus2(nPropID,nObjID,recObject.getObjUseNormID());
			else
				m_pDB->updPropertyStatus2(nPropID,nObjID,0);	// Set default forrest norm = "1950:�rs Skogsnorm"; 081103 p�d
			// Reload Object property data; 081110 p�d
			m_pDB->getObjectProperty(nPropID,nObjID,recElvProperty);
			// Add information on "Frivillig uppg�relse"; 080515 p�d
			caluclateVoluntaryDeal(recElvProperty);
			// Add information on "F�rdyrad avverkning"; 080515 p�d
			calculateHigherCosts(recElvProperty);

/*		***************************************************
				COMMENTED OUT 2010-06-15 P�D
				DON'T BOTHER CHECKIN' PRICELIST,COSTS AND DCLS
			***************************************************


			//------------------------------------------------------------------------------
			if (bIsLogBook)
			{

				//****************************************************************************
				// Checkout log-array. If there's any data in log-arrays
				// display them in a LogMessage; 080508 p�d
				arrMatchLog.RemoveAll();
				// Log-message(s) for Pricelist
				if (arrMatchLogPrl.GetCount() > 0)	
				{
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add(_T("A) ") + m_sLoggPrlMatch);
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add((formatData(_T("- %s %s"),m_sLoggObjPrl,sPrlName)));
					arrMatchLog.Add(_T(""));
					arrMatchLog.Append(arrMatchLogPrl);
				}
				// Log-message(s) for Costs
				if (arrMatchLogCost.GetCount() > 0)
				{
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add(_T("B) ") + m_sLoggCostMatch);
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add((formatData(_T("- %s %s"),m_sLoggObjCost,sCostName)));
					arrMatchLog.Add(_T(""));
					arrMatchLog.Append(arrMatchLogCost);
				}
				// Log-message(s) for Diameterclass(es)
				if (arrMatchLogDCLS.GetCount() > 0)
				{
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add(_T("C) ") + m_sLoggDCLSMatch);
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add((formatData(_T("- %s : %.1f %s"),m_sLoggObjDCLS,fDiamClass,m_sLogg_cm)));
					arrMatchLog.Add(_T(""));
					arrMatchLog.Append(arrMatchLogDCLS);
				}

				// Check if error-log from "Intr�ngsber�kning" holds any info. If so add to MatchLog; 080509 p�d
				if (arrLandValueLog.GetCount() > 0)
				{
					arrMatchLog.Add(_T(""));
					arrMatchLog.Add(_T("=========================================================="));
					arrMatchLog.Add(_T("=========================================================="));
					arrMatchLog.Append(arrLandValueLog);
				}

				error_log.Append(arrMatchLog);
				//****************************************************************************
			}	// if (bIsLogBook)
*/
		}	// if (vecTrakts.size() > 0)
		else
		{
			// Get aggregate information for Manually entered stand(s). I.e. there's no "Taxerade best�nd"; 090909 p�d
			fStormDryValue = 0.0;
			fRandTreesValue = 0.0;
			m_pDB->getObjPropTraktSUM_stands_areal(nObjID,nPropID,&nNumOfStands,&fAreal);
			m_pDB->getObjPropTraktSUM_numof_volume(nObjID,nPropID,&lNumOfTrees,&fVolumeM3Sk);
			m_pDB->getObjPropTraktSUM_rotpost(nObjID,nPropID,&fTimberVolume,&fTimberValue,&fTimberCost,&fRotNetto);
			m_pDB->getObjPropTraktSUM_storm_dry_randtrees(nObjID,nPropID,&fStormDryValue,&fRandTreesValue);

			m_pDB->updObjPropTraktSUM_1(nPropID,nObjID,nNumOfStands,fAreal,lNumOfTrees,fVolumeM3Sk,fStormDryValue,fRandTreesValue);
			m_pDB->updObjPropTraktSUM_6(nPropID,nObjID,fTimberVolume,fTimberValue,fTimberCost,fRotNetto);
		}
	}	// if (m_pDB != NULL)

	// Changed from "!bIsLogBook" to "TRUE"; 090924 p�d
	return TRUE; //!bIsLogBook;
}

void CPropertiesFormView::calculateEvaluationStandFromCruise(void)
{
	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();

	CString S;
	int nLastEvalID = -1;
	int nObjID = -1;
	int nPropID = -1;
	double fSUMLandValue = 0.0;
	double fSUMPreCut = 0.0;
	double fPinePercent = 0.0;	// "Tall"
	double fSprucePercent = 0.0;	// "Gran"
	double fBirchPercent = 0.0;	// "L�v"
	double fLarkPercent=0.0; //L�rk
	int nNumOfStands = 0;
	double fAreal = 0.0;
	CString sArea;				// 2009-09-21 p�d
	int nAreaIndex = -1;	// 2009-09-21 p�d
	CString sName,sDoneBy,sNotes;

	//HMS-108 kontrollera vilket p30 tr�dslag som �r satt och r�kna tr�dslagsandelen mha detta ist�llet f�r att s�tta allt annat �n tall o gran som l�v..??
	int nSpecId=0;								
	int getP30SpcID=0;

	CTransaction_elv_object recObject;
	CTransaction_trakt recTrakt;
	vecObjectTemplate_p30_table vecP30_table;
	vecObjectTemplate_p30_nn_table vecP30_nn_table;
	CTransaction_trakt_data recTraktData;
	vecTransactionTraktData vecTraktData;
	vecTransaction_elv_m3sk_per_specie vecM3Sk_in_out;
	vecTransactionSampleTree vecRandTrees;
	vecTransaction_eval_evaluation vecEvaluation;
	CTransaction_eval_evaluation recEvaluation;
	CStringArray arrLandValueLog;
	CTransaction_elv_return_storm_dry recReturnStormDry;
	CTransaction_elv_return_land_and_precut recLandValueAndPreCut;
	mapDouble mapRandTreesVolume;
	CTransaction_elv_properties recElvProperty;
	vecTransactionTrakt vecTrakts;
	vecTransaction_elv_cruise vecELVObjectCruise;
	CTransaction_elv_cruise recCruise;

	vecTransactionSpecies vecP30Species;	// Added 2008-11-07 P�D


	CCruisingData *pCruise = getCruisingView();
	if (pCruise != NULL)
	{
		pCruise->getCruises(vecELVObjectCruise);
	}	// if (pCruise != NULL)

	if (vecELVObjectCruise.size() == 0) 
	{
		return;
	}

	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow != NULL)
	{
		CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			recElvProperty = pRec->getRecELVProp();

			for (UINT i = 0;i < vecELVObjectCruise.size();i++)
			{
				recCruise = vecELVObjectCruise[i];
				if (!isThereOnlyRandTreesInStand(recCruise))
				{
					// Try to collect some SUM. data for this property.
					// I.e. SUM data on Trakt(s) connected to this property; 080430 p�d
					if (m_pDB != NULL)
					{
						nObjID = recElvProperty.getPropObjectID_pk();
						nPropID = recElvProperty.getPropID_pk();

						// Get Trakt data; 080619 p�d
						m_pDB->getTrakt(recCruise.getECruID_pk(),recTrakt);
						

						// Added 2009-05-04 P�D
						// Check age for Stand. If User's set Standage > 500 years, the stand can't be
						// used to create evaluated stand; 090504 p�d
						// Changed 100526 p�d
						if (recTrakt.getTraktAge() <= MAX_STAND_AGE_TO_CREATE_EVAL_NEW &&	recTrakt.getOnlyForStand() == 0)
						{
							// Get Traktdata from table "esti_trakt_data_table"; 080508 p�d
							// Info on e.g. "Tr�dslagsblandning"; 080508 p�d
							vecTraktData.clear();
							m_pDB->getTraktData(vecTraktData,recCruise.getECruID_pk(),1);	// 1 = "Uttag"

							// Get aggregate data for m3sk "I gatan" and "Kanttr�d", set per specie; 080508 p�d
							vecM3Sk_in_out.clear();
							m_pDB->getObjPropTrakt_m3sk_volumes_per_spc(recCruise.getECruID_pk(),recTrakt.getTraktAreal(),vecM3Sk_in_out);
							// Make sure we have the latest Object information; 080508 p�d
							m_pDB->getObject(nObjID,recObject);
							
							// Load P30 table for Object; 080508 p�d
							TemplateParser parser;
							if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
							{
								if (parser.LoadFromBuffer(recObject.getObjP30XML()))
								{
									parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
								}
							}
							else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
							{
								if (parser.LoadFromBuffer(recObject.getObjP30XML()))
								{
									parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
									setP30NN_SIMax(vecP30_nn_table);

								}
							}
							else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
							{
								if (parser.LoadFromBuffer(recObject.getObjP30XML()))
								{
									parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
									setP30NN_SIMax(vecP30_nn_table);

								}
							}
							
							m_pDB->getObjectEvaluation(nPropID,nObjID,vecEvaluation);

							// I think we should try to do some "Intr�ngsv�rdering", here. 
							// I.e. calculate "Storm- och Torkskador" and "Kanttr�d"; 080508 p�d
							arrLandValueLog.RemoveAll();

							// Get last created evaluation stand id; 080619 p�d
							m_pDB->getObjectEvaluation_last_id(nObjID,nPropID,&nLastEvalID);

							// Get aggregate data for "Kanttr�d" from "esti_trakt_sample_trees_table"; 080512 p�d
							vecRandTrees.clear();
							m_pDB->getLandValueRandTrees(recTrakt.getTraktID(),vecRandTrees);


							//HMS-94 H�mtar p30 tr�dslagsinfo sparat per object och per tr�dslag
							getObjectp30Speciesinfo(vecP30Species,recObject.getObjID_pk());


							// Calculate the "Tr�dslagsblandning", based on information in
							// table "esti_trakt_data_table".
							// SET: 1 = "Tall", 2 = "Gran" and rest is "L�v"; 080619 p�d

							//HMS-110 20231027 Nolla trslgsf�rd
							fPinePercent=0.0;
							fSprucePercent=0.0;
							fBirchPercent=0.0;
							if (vecTraktData.size() > 0)
							{
								for (UINT i = 0;i < vecTraktData.size();i++)
								{


									//HMS-51 Kontrollera om det finns l�rk i s� fall f�r �ver l�rk till Tall 20191125 J�
									recTraktData = vecTraktData[i];

									/*if (recTraktData.getSpecieID() == 1)	// "Tall"
										fPinePercent = recTraktData.getPercent();
									if (recTraktData.getSpecieID() == 2)	// "Gran"
										fSprucePercent = recTraktData.getPercent();
									if(recTraktData.getSpecieID() == 18)	//L�rk
										fLarkPercent=recTraktData.getPercent();*/

									nSpecId=recTraktData.getSpecieID();
									//HMS-108 kontrollera vilket p30 tr�dslag som �r satt och r�kna tr�dslagsandelen mha detta ist�llet f�r att s�tta allt annat �n tall o gran som l�v..??
									getP30SpcID=vecP30Species[nSpecId-1].getP30SpcID();
									switch(getP30SpcID)
									{
									case 1:
										fPinePercent += recTraktData.getPercent();
										break;
									case 2:
										fSprucePercent += recTraktData.getPercent();
										break;
									case 3:
										fBirchPercent += recTraktData.getPercent();
										break;
									}
									
								}
								/*
								//HMS-51 f�r �ver l�rk till Tall 20191125 J�
								fPinePercent = fPinePercent + fLarkPercent;
								// Set rest of "Tr�dslagsblandning" to "L�v"; 080619 p�d
								fBirchPercent = (100.0 - (fPinePercent+fSprucePercent));
								*/

							}	// if (vecTraktData.size() > 0)

							//Plocka ut volymen i gatan f�r att kunna s�tta volymen p� v�rderingsbest�ndet, J� 20121204 #3379
							double fTraktVol=0.0;
							if(vecM3Sk_in_out.size()>0)
							{
								for(UINT i=0;i<vecM3Sk_in_out.size();i++)
								{
									fTraktVol+=vecM3Sk_in_out[i].getM3SkInside();
								}
							}

							// Setup the "CTransaction_eval_evaluation" record; 080619 p�d
							recEvaluation = CTransaction_eval_evaluation(nLastEvalID+1,
								nObjID,
								nPropID,
								recTrakt.getTraktName(),
								recTrakt.getTraktAreal(),
								recTrakt.getTraktAge(),
								recTrakt.getTraktSIH100(),
								0.0,	// Correctionfactor'll be calculated on Cruise; 080619 p�d
								fPinePercent,
								fSprucePercent,
								fBirchPercent,
								0,		// "Skogsmark"
								EVAL_TYPE_FROM_TAXERING,		// "Taxeringsbest�nd"
								0.0,	// Calculated 
								0.0,	// Calculated 
								0.0,	// Calculated 
								0.0,	// Calculated 
								_T(""),
								fTraktVol,
								recTrakt.getTraktLength(),
								recTrakt.getTraktWidth(),
								1,
								recTrakt.getWSide(),
								false,
								recTrakt.getTraktTillfUtnyttj(),
								//HMS-49 Info om f�rtidig avverkning 20191126 J� beh�ver inte s�ttas upp h�r eftersom det ber�knas och sparas senare i  doUMForrestNorm/updObjectEvaluation_calculated_data
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0
								);
							// Create calculation to DB; 080619 p�d
							m_pDB->addObjectEvaluation_entered_data(recEvaluation);


							



							// Calculate "V�rderingsdata", from "Taxerat data"; 080619 p�d
							doUMForrestNorm(2, // "Taxering"
								m_mapForrestNormErrText,
								recObject,						// In-data
								recTrakt,							// In-data
								vecP30_table,					// In-data
								vecP30_nn_table,			// In-data; new forest norm; 090421 p�d
								vecTraktData,					// In-data
								vecM3Sk_in_out,				// In-data
								vecRandTrees,					// In-data
								recEvaluation,				// In-data
								vecTrakts,						// In-data; 080915 p�d
								vecP30Species,
								arrLandValueLog,			// Return data
								recReturnStormDry,		// Return data
								recLandValueAndPreCut,// Return data
								mapRandTreesVolume);	// Return data

							m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
								recEvaluation.getEValPropID_pk(),
								recEvaluation.getEValObjID_pk(),
								EVAL_TYPE_FROM_TAXERING, // "Taxeringsbest�nd"
								recLandValueAndPreCut.getLandValue_ha(),
								recLandValueAndPreCut.getPreCutValue_ha(),
								recLandValueAndPreCut.getLandValue(),
								recLandValueAndPreCut.getPreCutValue(),
								//HMS-49 Info om f�rtidig avverkning 20191126 J�
								recLandValueAndPreCut.getPreCutInfo_P30_Pine(),
								recLandValueAndPreCut.getPreCutInfo_P30_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_P30_Birch(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Pine(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Birch(),
								recLandValueAndPreCut.getPreCutInfo_CorrFact(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Pine(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Birch(),
								recLandValueAndPreCut.getPreCutInfo_ReducedBy(),
								//HMS-48 Info om markv�rde 20200427 J�
								recLandValueAndPreCut.getMarkInfo_ValuePine(),
								recLandValueAndPreCut.getMarkInfo_ValueSpruce(),
								recLandValueAndPreCut.getMarkInfo_Andel_Pine(),
								recLandValueAndPreCut.getMarkInfo_Andel_Spruce(),
								recLandValueAndPreCut.getMarkInfo_P30_Pine(),
								recLandValueAndPreCut.getMarkInfo_P30_Spruce(),
								recLandValueAndPreCut.getMarkInfo_ReducedBy()
								);

								// Also add calculated "Korrektions-faktor", from table in ForrestNorm; 080619 p�d
								m_pDB->updObjectEvaluation_calculated_corrfac(recEvaluation.getEValID_pk(),
																													 recEvaluation.getEValObjID_pk(),
																													 recEvaluation.getEValPropID_pk(),
																													 recLandValueAndPreCut.getCorrFactor());

								// Get aggregate information from "elv_evaluation_table" and add to "elv_properties_table"; 080515 p�d
								m_pDB->getObjPropTraktSUM_landvalue_precut(recEvaluation.getEValObjID_pk(),
																													 recEvaluation.getEValPropID_pk(),
																													 &fSUMLandValue,
																													 &fSUMPreCut);
								// Update table "elv_properties_table" with information on aggregate LandValue and PreCut; 080514 p�d
								m_pDB->updObjPropTraktSUM_2(recEvaluation.getEValPropID_pk(),
																						recEvaluation.getEValObjID_pk(),
																						fSUMLandValue,
																						fSUMPreCut);
			
								// Update Areal in Property table, because area is calculatecd from SUM of areal in elv_evaluation_table; 090914 p�d
								m_pDB->getObjPropTraktSUM_stands_areal(nObjID,nPropID,&nNumOfStands,&fAreal);
								m_pDB->updObjPropTraktSUM_7(nPropID,nObjID,fAreal);

								// Add information on "Frivillig uppg�relse"; 080515 p�d
								caluclateVoluntaryDeal(recElvProperty);

								// Add information on "F�rdyrad avverkning"; 080515 p�d
								calculateHigherCosts(recElvProperty);
						}	// if (recTrakt.getOnlyForStand() == 0)
					}	// if (m_pDB != NULL)
				}	// if (!isThereOnlyRandTreesInStand(recCruise))
			}	// for (UINT i = 0;i < vecELVObjectCruise.size();i++)
		}	// if (pRec != NULL && m_pDB != NULL)
	}	// if (pRow != NULL)
	

	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	//setActivePropertyInReport();

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);
}

// This is the bacch version, so we can calculate ALL evaluation-stands
// for an Object, based on cruesed stands/proeprty; 081022 p�d
void CPropertiesFormView::calculateAllEvaluationStandsFromCruises(CProgressDlg &prog_dlg)
{
	CString S;
	int nLastEvalID = -1;
	int nObjID = -1;
	int nPropID = -1;
	double fSUMLandValue = 0.0;
	double fSUMPreCut = 0.0;
	double fPinePercent = 0.0;	// "Tall"
	double fSprucePercent = 0.0;	// "Gran"
	double fBirchPercent = 0.0;	// "L�v"
	double fLarkPercent = 0.0;	// "L�rk"
	CString sArea;				// 2009-09-21 p�d
	int nAreaIndex = -1;	// 2009-09-21 p�d
	CString sName,sDoneBy,sNotes;
	//HMS-108 kontrollera vilket p30 tr�dslag som �r satt och r�kna tr�dslagsandelen mha detta ist�llet f�r att s�tta allt annat �n tall o gran som l�v..??
	int nSpecId=0;								
	int getP30SpcID=0;

	CTransaction_elv_object recObject;
	CTransaction_trakt recTrakt;
	vecObjectTemplate_p30_table vecP30_table;
	vecObjectTemplate_p30_nn_table vecP30_nn_table;
	CTransaction_trakt_data recTraktData;
	vecTransactionTraktData vecTraktData;
	vecTransaction_elv_m3sk_per_specie vecM3Sk_in_out;
	vecTransactionSampleTree vecRandTrees;
	CTransaction_eval_evaluation recEvaluation;
	CStringArray arrLandValueLog;
	CTransaction_elv_return_storm_dry recReturnStormDry;
	CTransaction_elv_return_land_and_precut recLandValueAndPreCut;
	mapDouble mapRandTreesVolume;
	CTransaction_elv_properties recElvProperty;
	vecTransactionTrakt vecTrakts;
	vecTransaction_elv_cruise vecELVObjectCruise;
	CTransaction_elv_cruise recCruise;
	vecTransactionSpecies vecP30Species;	// Added 2008-11-07 P�D
	

	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	if (pRows != NULL)
	{
		nObjID = m_recActiveProperty.getPropObjectID_pk();
		if (m_pDB != NULL)
		{
			// Make sure we have the latest Object information; 080508 p�d
			m_pDB->getObject(nObjID,recObject);
			// Load P30 table for Object; 090421 p�d
			TemplateParser parser;
			if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
			{
				if (parser.LoadFromBuffer(recObject.getObjP30XML()))
				{
					parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
				}
			}
			else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
			{
				if (parser.LoadFromBuffer(recObject.getObjP30XML()))
				{
					parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
					setP30NN_SIMax(vecP30_nn_table);
				}
			}
			else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
			{
				if (parser.LoadFromBuffer(recObject.getObjP30XML()))
				{
					parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
					setP30NN_SIMax(vecP30_nn_table);
				}
			}
		}
		// Try to get evaluated stands; from file or manually entered; 090526 p�d
		getObjectEvaluatedNotCalculated();

		if (prog_dlg.GetSafeHwnd())
		{
			prog_dlg.setProgressRange_props(pRows->GetCount()); 
		}

		for (int i = 0;i < pRows->GetCount();i++)
		{
			CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL && m_pDB != NULL)
			{
				recElvProperty = pRec->getRecELVProp();
				if (prog_dlg.GetSafeHwnd())
				{
					prog_dlg.setPropertyName(recElvProperty.getPropName());
					prog_dlg.setProgressPos_props(i+1);
				}

				//if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
				if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()))
				{
					if (m_pDB != NULL)
					{
						m_pDB->getObjectCruises(recElvProperty.getPropID_pk(),recElvProperty.getPropObjectID_pk(),vecELVObjectCruise);
					}	// if (m_pDB != NULL)

					if (vecELVObjectCruise.size() > 0)
					{
						if (prog_dlg.GetSafeHwnd())
						{
							prog_dlg.setProgressRange_stands(vecELVObjectCruise.size()); 
						}
						for (UINT ii = 0;ii < vecELVObjectCruise.size();ii++)
						{
							recCruise = vecELVObjectCruise[ii];
							if (prog_dlg.GetSafeHwnd())
							{
								prog_dlg.setStandName(recCruise.getECruName());
								prog_dlg.setProgressPos_stands(ii+1);
								doEvents();
							}
							if (!isThereOnlyRandTreesInStand(recCruise))
							{
								// Try to collect some SUM. data for this property.
								// I.e. SUM data on Trakt(s) connected to this property; 080430 p�d
								if (m_pDB != NULL)
								{
									nPropID = recElvProperty.getPropID_pk();

									// Get Trakt data; 080619 p�d
									m_pDB->getTrakt(recCruise.getECruID_pk(),recTrakt);

									recTrakt.setCruiseWidth(recCruise.getECruWidth());

									// Added 2009-05-04 P�D
									// Check age for Stand. If User's set Standage > 500 years, the stand can't be
									// used to create evaluated stand; 090504 p�d
									// Changed 100526 p�d
									if ((recTrakt.getTraktAge() <= MAX_STAND_AGE_TO_CREATE_EVAL_NEW && recTrakt.getOnlyForStand() == 0))
									{
										// Get Traktdata from table "esti_trakt_data_table"; 080508 p�d
										// Info on e.g. "Tr�dslagsblandning"; 080508 p�d
										vecTraktData.clear();
										m_pDB->getTraktData(vecTraktData,recCruise.getECruID_pk(),1);	// 1 = "Uttag"

										// Get aggregate data for m3sk "I gatan" and "Kanttr�d", set per specie; 080508 p�d
										vecM3Sk_in_out.clear();
										m_pDB->getObjPropTrakt_m3sk_volumes_per_spc(recCruise.getECruID_pk(),recTrakt.getTraktAreal(),vecM3Sk_in_out);

										// I think we should try to do some "Intr�ngsv�rdering", here. 
										// I.e. calculate "Storm- och Torkskador" and "Kanttr�d"; 080508 p�d
										arrLandValueLog.RemoveAll();

										// Get last created evaluation stand id; 080619 p�d
										m_pDB->getObjectEvaluation_last_id(nObjID,nPropID,&nLastEvalID);

										// Get aggregate data for "Kanttr�d" from "esti_trakt_sample_trees_table"; 080512 p�d
										vecRandTrees.clear();
										m_pDB->getLandValueRandTrees(recTrakt.getTraktID(),vecRandTrees);

										//HMS-94 H�mtar p30 tr�dslagsinfo sparat per object och per tr�dslag
										getObjectp30Speciesinfo(vecP30Species,recObject.getObjID_pk());

										// Calculate the "Tr�dslagsblandning", based on information in
										// table "esti_trakt_data_table".
										// SET: 1 = "Tall", 2 = "Gran" and rest is "L�v"; 080619 p�d
										//HMS-51 Kontrollera om det finns l�rk i s� fall f�r �ver l�rk till Tall 20191125 J�
										
										//HMS-110 20231027 Nolla trslgsf�rd
										fPinePercent=0.0;
										fSprucePercent=0.0;
										fBirchPercent=0.0;
										if (vecTraktData.size() > 0)
										{
											for (UINT i = 0;i < vecTraktData.size();i++)
											{

												/*
												//HMS-108 kontrollera vilket p30 tr�dslag som �r satt och r�kna tr�dslagsandelen mha detta ist�llet f�r att s�tta allt annat �n tall o gran som l�v..??
												if (recTraktData.getSpecieID() == 1)	// "Tall"
												fPinePercent = recTraktData.getPercent();
												if (recTraktData.getSpecieID() == 2)	// "Gran"
												fSprucePercent = recTraktData.getPercent();
												if (recTraktData.getSpecieID() == 18)	// "L�rk"
												fLarkPercent = recTraktData.getPercent();*/

												recTraktData = vecTraktData[i];
												nSpecId=recTraktData.getSpecieID();
												//HMS-108 kontrollera vilket p30 tr�dslag som �r satt och r�kna tr�dslagsandelen mha detta ist�llet f�r att s�tta allt annat �n tall o gran som l�v..??
												getP30SpcID=vecP30Species[nSpecId-1].getP30SpcID();
												switch(getP30SpcID)
												{
												case 1:
													fPinePercent += recTraktData.getPercent();
													break;
												case 2:
													fSprucePercent += recTraktData.getPercent();
													break;
												case 3:
													fBirchPercent += recTraktData.getPercent();
													break;
												}


											}											
											/*
											//HMS-51 Kontrollera om det finns l�rk i s� fall f�r �ver l�rk till Tall 20191125 J�
											fPinePercent = fPinePercent + fLarkPercent;
											// Set rest of "Tr�dslagsblandning" to "L�v"; 080619 p�d
											fBirchPercent = (100.0 - (fPinePercent+fSprucePercent));
											*/

										}	// if (vecTraktData.size() > 0)

										//Plocka ut volymen i gatan f�r att kunna s�tta volymen p� v�rderingsbest�ndet, J� 20121204 #3379
										double fTraktVol=0.0;
										if(vecM3Sk_in_out.size()>0)
										{
											for(UINT i=0;i<vecM3Sk_in_out.size();i++)
											{
												fTraktVol+=vecM3Sk_in_out[i].getM3SkInside();
											}
										}

										// Setup the "CTransaction_eval_evaluation" record; 080619 p�d
										recEvaluation = CTransaction_eval_evaluation(nLastEvalID+1,
											nObjID,
											nPropID,
											recTrakt.getTraktName(),
											recTrakt.getTraktAreal(),
											recTrakt.getTraktAge(),
											recTrakt.getTraktSIH100(),
											0.0,	// Correctionfactor'll be calculated on Cruise; 080619 p�d
											fPinePercent,
											fSprucePercent,
											fBirchPercent,
											0,		// "Skogsmark"
											EVAL_TYPE_FROM_TAXERING,		// "V�rdering fr�n Taxering"
											0.0,	// Calculated 
											0.0,	// Calculated 
											0.0,	// Calculated 
											0.0,	// Calculated 
											_T(""),
											fTraktVol,
											recTrakt.getTraktLength(),
											recTrakt.getTraktWidth(),
											1,
											recTrakt.getWSide(),
											false,
											recTrakt.getTraktTillfUtnyttj(),
											//HMS-49 In med info om f�rtidig avverkning �ven om typen �r taxering??
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0,
											0.0
											);


										// Create calculation to DB; 080619 p�d
										m_pDB->addObjectEvaluation_entered_data(recEvaluation);
										// Calculate "V�rderingsdata", from "Taxerat data"; 080619 p�d
										doUMForrestNorm(2, // "Taxering"
											m_mapForrestNormErrText,
											recObject,						// In-data
											recTrakt,							// In-data
											vecP30_table,					// In-data
											vecP30_nn_table,			// In-data; new forest norm; 090421 p�d
											vecTraktData,					// In-data
											vecM3Sk_in_out,				// In-data
											vecRandTrees,					// In-data
											recEvaluation,				// In-data
											vecTrakts,						// In-data; 080915 p�d
											vecP30Species,
											arrLandValueLog,			// Return data
											recReturnStormDry,		// Return data
											recLandValueAndPreCut,// Return data
											mapRandTreesVolume);	// Return data

										// Save calculation to DB; 080514 p�d
										m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
											recEvaluation.getEValPropID_pk(),
											recEvaluation.getEValObjID_pk(),
											EVAL_TYPE_FROM_TAXERING, // "Taxeringsbest�nd"
											recLandValueAndPreCut.getLandValue_ha(),
											recLandValueAndPreCut.getPreCutValue_ha(),
											recLandValueAndPreCut.getLandValue(),
											recLandValueAndPreCut.getPreCutValue(),
											//HMS-49 Info om f�rtidig avverkning 20191126 J�
											recLandValueAndPreCut.getPreCutInfo_P30_Pine(),
											recLandValueAndPreCut.getPreCutInfo_P30_Spruce(),
											recLandValueAndPreCut.getPreCutInfo_P30_Birch(),
											recLandValueAndPreCut.getPreCutInfo_Andel_Pine(),
											recLandValueAndPreCut.getPreCutInfo_Andel_Spruce(),
											recLandValueAndPreCut.getPreCutInfo_Andel_Birch(),
											recLandValueAndPreCut.getPreCutInfo_CorrFact(),
											recLandValueAndPreCut.getPreCutInfo_Ers_Pine(),
											recLandValueAndPreCut.getPreCutInfo_Ers_Spruce(),
											recLandValueAndPreCut.getPreCutInfo_Ers_Birch(),
											recLandValueAndPreCut.getPreCutInfo_ReducedBy(),
											//HMS-48 Info om markv�rde 20200427 J�
											recLandValueAndPreCut.getMarkInfo_ValuePine(),
											recLandValueAndPreCut.getMarkInfo_ValueSpruce(),
											recLandValueAndPreCut.getMarkInfo_Andel_Pine(),
											recLandValueAndPreCut.getMarkInfo_Andel_Spruce(),
											recLandValueAndPreCut.getMarkInfo_P30_Pine(),
											recLandValueAndPreCut.getMarkInfo_P30_Spruce(),
											recLandValueAndPreCut.getMarkInfo_ReducedBy()
											);
										// Also add calculated "Korrektions-faktor", from table in ForrestNorm; 080619 p�d
										m_pDB->updObjectEvaluation_calculated_corrfac(recEvaluation.getEValID_pk(),
																															 recEvaluation.getEValObjID_pk(),
																															 recEvaluation.getEValPropID_pk(),
																															 recLandValueAndPreCut.getCorrFactor());
			/*
									S.Format(_T("CPropertiesFormView::calculateEvaluationStandFromCruises\nrecEvaluation Corr %f"),
										recEvaluation.getEValCorrFactor());
									AfxMessageBox(S);
			*/
										// Get aggregate information from "elv_evaluation_table" and add to "elv_properties_table"; 080515 p�d
										m_pDB->getObjPropTraktSUM_landvalue_precut(recEvaluation.getEValObjID_pk(),
																															 recEvaluation.getEValPropID_pk(),
																															 &fSUMLandValue,
																															 &fSUMPreCut);
										// Update table "elv_properties_table" with information on aggregate LandValue and PreCut; 080514 p�d
										m_pDB->updObjPropTraktSUM_2(recEvaluation.getEValPropID_pk(),
																								recEvaluation.getEValObjID_pk(),
																								fSUMLandValue,
																								fSUMPreCut);

										// Add information on "Frivillig uppg�relse"; 080515 p�d
										caluclateVoluntaryDeal(recElvProperty);
										// Add information on "F�rdyrad avverkning"; 080515 p�d
										calculateHigherCosts(recElvProperty);
									}	// if (recTrakt.getOnlyForStand() == 0)
								}	// if (m_pDB != NULL)
							}	// if (!isThereOnlyRandTreesInStand(recCruise))
							else
							{
								// Update table "elv_properties_table" with information on aggregate LandValue and PreCut; 080514 p�d
								m_pDB->updObjPropTraktSUM_2(recElvProperty.getPropID_pk(),nObjID,0.0,0.0);
							}
						}	// for (UINT i = 0;i < vecELVObjectCruise.size();i++)
					}	// if (vecELVObjectCruise.size() > 0)
				}	// if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
			}	// if (pRec != NULL && m_pDB != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	setActivePropertyInReport();
}

void CPropertiesFormView::calculateEvaluationStand(void)
{
	CString S;
	int nObjID = -1;
	int nPropID = -1;
	double fSUMLandValue = 0.0;
	double fSUMPreCut = 0.0;
	int nNumOfStands = 0;
	double fAreal = 0.0;
	CString sArea;				// 2009-09-21 p�d
	int nAreaIndex = -1;	// 2009-09-21 p�d
	CString sName,sDoneBy,sNotes;

	CTransaction_elv_object recObject;
	CTransaction_trakt recTrakt;
	vecObjectTemplate_p30_table vecP30_table;
	vecObjectTemplate_p30_nn_table vecP30_nn_table;
	vecTransactionTraktData vecTraktData;
	vecTransaction_elv_m3sk_per_specie vecM3Sk_in_out;
	vecTransactionSampleTree vecRandTrees;
	vecTransaction_eval_evaluation vecEvaluation;
	CTransaction_eval_evaluation recEvaluation;
	CStringArray arrLandValueLog;
	CTransaction_elv_return_storm_dry recReturnStormDry;
	CTransaction_elv_return_land_and_precut recLandValueAndPreCut;
	mapDouble mapRandTreesVolume;
	CTransaction_elv_properties recElvProperty;
	vecTransactionTrakt vecTrakts;

	vecTransactionSpecies vecP30Species;	// Added 2008-11-07 P�D

	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow != NULL)
	{
		CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			recElvProperty = pRec->getRecELVProp();
			// Try to collect some SUM. data for this property.
			// I.e. SUM data on Trakt(s) connected to this property; 080430 p�d
			if (m_pDB != NULL)
			{
				nObjID = recElvProperty.getPropObjectID_pk();
				nPropID = recElvProperty.getPropID_pk();

				// Get Traktdata from table "esti_trakt_data_table"; 080508 p�d
				// Info on e.g. "Tr�dslagsblandning"; 080508 p�d
				
				// Make sure we have the latest Object information; 080508 p�d
				m_pDB->getObject(nObjID,recObject);
				// Load P30 table for Object; 090421 p�d
				TemplateParser parser;
				if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
				{
					if (parser.LoadFromBuffer(recObject.getObjP30XML()))
					{
						parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
					}
				}
				else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
				{
					if (parser.LoadFromBuffer(recObject.getObjP30XML()))
					{
						parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
						setP30NN_SIMax(vecP30_nn_table);
					}
				}
				else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
				{
					if (parser.LoadFromBuffer(recObject.getObjP30XML()))
					{
						parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
						setP30NN_SIMax(vecP30_nn_table);
					}
				}

				m_pDB->getObjectEvaluation(nPropID,nObjID,vecEvaluation);

				// I think we should try to do some "Intr�ngsv�rdering", here. 
				// I.e. calculate "Storm- och Torkskador" and "Kanttr�d"; 080508 p�d
				arrLandValueLog.RemoveAll();

				// Get aggregate data for "Kanttr�d" from "esti_trakt_sample_trees_table"; 080512 p�d
				m_pDB->getLandValueRandTrees(recTrakt.getTraktID(),vecRandTrees);

				m_pDB->getObjPropTrakt_m3sk_volumes_per_spc(recTrakt.getTraktID(),recTrakt.getTraktAreal(),vecM3Sk_in_out);

				// Check if there's any Evaluated stands to calculate; 080514 p�d
				if (vecEvaluation.size() > 0)
				{
					for (UINT i = 0;i < vecEvaluation.size();i++)
					{
						recEvaluation = vecEvaluation[i];
						if (recEvaluation.getEValType() == EVAL_TYPE_VARDERING)
						{


							//HMS-94 H�mtar p30 tr�dslagsinfo sparat per object och per tr�dslag
							getObjectp30Speciesinfo(vecP30Species,recObject.getObjID_pk());

							doUMForrestNorm(1,	// "V�rdering"
								m_mapForrestNormErrText,
								recObject,						// In-data
								recTrakt,							// In-data
								vecP30_table,					// In-data
								vecP30_nn_table,			// In-data, new forest norm; 090421 p�d
								vecTraktData,					// In-data
								vecM3Sk_in_out,				// In-data
								vecRandTrees,					// In-data
								recEvaluation,				// In-data
								vecTrakts,						// In-data; 080915 p�d
								vecP30Species,
								arrLandValueLog,			// Return data
								recReturnStormDry,		// Return data
								recLandValueAndPreCut,// Return data
								mapRandTreesVolume);	// Return data


							double preCutValue = recLandValueAndPreCut.getPreCutValue();
							double preCutValueHa = recLandValueAndPreCut.getPreCutValue_ha();

							if(recEvaluation.getEValForrestType()==1) // Obehandlad F�rtidig ==0
							{
								preCutValue=0;
								preCutValueHa=0;
							}

							// Save calculation to DB; 080514 p�d
							m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
																											 recEvaluation.getEValPropID_pk(),
																											 recEvaluation.getEValObjID_pk(),
																											 recEvaluation.getEValType(),
																											 recLandValueAndPreCut.getLandValue_ha(),
																											 preCutValueHa,
																											 recLandValueAndPreCut.getLandValue(),
								preCutValue,
								//HMS-49 Info om f�rtidig avverkning 20191126 J�
								recLandValueAndPreCut.getPreCutInfo_P30_Pine(),
								recLandValueAndPreCut.getPreCutInfo_P30_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_P30_Birch(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Pine(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Birch(),
								recLandValueAndPreCut.getPreCutInfo_CorrFact(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Pine(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Birch(),
								recLandValueAndPreCut.getPreCutInfo_ReducedBy(),
								//HMS-48 Info om markv�rde 20200427 J�
								recLandValueAndPreCut.getMarkInfo_ValuePine(),
								recLandValueAndPreCut.getMarkInfo_ValueSpruce(),
								recLandValueAndPreCut.getMarkInfo_Andel_Pine(),
								recLandValueAndPreCut.getMarkInfo_Andel_Spruce(),
								recLandValueAndPreCut.getMarkInfo_P30_Pine(),
								recLandValueAndPreCut.getMarkInfo_P30_Spruce(),
								recLandValueAndPreCut.getMarkInfo_ReducedBy()
								);
						}	// if (recEvaluation.getEValType() == EVAL_TYPE_1)
						else
						{	
							
							// Save calculation to DB; 080514 p�d
							/*
							m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
																											 recEvaluation.getEValPropID_pk(),
																											 recEvaluation.getEValObjID_pk(),
																											 recEvaluation.getEValType(),
																											 recEvaluation.getEValLandValue_ha(),
																											 recEvaluation.getEValPreCut_ha(), 
																											 recEvaluation.getEValLandValue(), 
								recEvaluation.getEValPreCut(),
								//HMS-49 Info om f�rtidig avverkning 20191126 J�
								recLandValueAndPreCut.getPreCutInfo_P30_Pine(),
								recLandValueAndPreCut.getPreCutInfo_P30_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_P30_Birch(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Pine(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_Andel_Birch(),
								recLandValueAndPreCut.getPreCutInfo_CorrFact(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Pine(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Spruce(),
								recLandValueAndPreCut.getPreCutInfo_Ers_Birch(),
								recLandValueAndPreCut.getPreCutInfo_ReducedBy(),
								//HMS-48 Info om markv�rde 20200427 J�
								recLandValueAndPreCut.getMarkInfo_ValuePine(),
								recLandValueAndPreCut.getMarkInfo_ValueSpruce(),
								recLandValueAndPreCut.getMarkInfo_Andel_Pine(),
								recLandValueAndPreCut.getMarkInfo_Andel_Spruce(),
								recLandValueAndPreCut.getMarkInfo_P30_Pine(),
								recLandValueAndPreCut.getMarkInfo_P30_Spruce(),
								recLandValueAndPreCut.getMarkInfo_ReducedBy()
								);*/

						 // #HMS-95 Inte spara ner eventuellt gammal info som inte beh�vs om annan mark, dvs nolla f�rtidig tex
					     // De v�rden som �r till�tna f�r annan mark �r 
							// Typ av v�rdering
							// Namn
							// Areal
							// L�ngd
							// Bredd
							// �lder
							// Markv�rde
							// Markv�rde per ha
							// Skikt
						 if(recEvaluation.getEValType() == EVAL_TYPE_ANNAN)
						 {
							m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
								recEvaluation.getEValPropID_pk(),
								recEvaluation.getEValObjID_pk(),
								recEvaluation.getEValType(),
								recEvaluation.getEValLandValue_ha(),
								0.0,	// #HMS-95 Nolla f�rtidig
								recEvaluation.getEValLandValue(), 
								0.0,	// #HMS-95 Nolla f�rtidig
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								//HMS-48 Info om markv�rde 20200427 J�
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0,
								0.0
								);
						 }
						 else
						 {


							// HMS-87 20220504 J� 
							// anv�nd recEvaluation ist�llet f�r som ovan recLandValueAndPreCut d� den s�tts vid manuella v�rderingar och ej uppdateras f�r v�rdering fr�n taxering
							// Dock kan man fr�ga sig om denna kod nedan som uppdaterar DB beh�vs d� jag inte vet om det faktiskt finns uppdaterat data eller ej i recEvaluation som beh�ver sparas??
							m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
								recEvaluation.getEValPropID_pk(),
								recEvaluation.getEValObjID_pk(),
								recEvaluation.getEValType(),
								recEvaluation.getEValLandValue_ha(),
								recEvaluation.getEValPreCut_ha(), 
								recEvaluation.getEValLandValue(), 
								recEvaluation.getEValPreCut(),
								//HMS-49 Info om f�rtidig avverkning 20191126 J�
								recEvaluation.getPreCutInfo_P30_Pine(),
								recEvaluation.getPreCutInfo_P30_Spruce(),
								recEvaluation.getPreCutInfo_P30_Birch(),
								recEvaluation.getPreCutInfo_Andel_Pine(),
								recEvaluation.getPreCutInfo_Andel_Spruce(),
								recEvaluation.getPreCutInfo_Andel_Birch(),
								recEvaluation.getPreCutInfo_CorrFact(),
								recEvaluation.getPreCutInfo_Ers_Pine(),
								recEvaluation.getPreCutInfo_Ers_Spruce(),
								recEvaluation.getPreCutInfo_Ers_Birch(),
								recEvaluation.getPreCutInfo_ReducedBy(),
								//HMS-48 Info om markv�rde 20200427 J�
								recEvaluation.getMarkInfo_ValuePine(),
								recEvaluation.getMarkInfo_ValueSpruce(),
								recEvaluation.getMarkInfo_Andel_Pine(),
								recEvaluation.getMarkInfo_Andel_Spruce(),
								recEvaluation.getMarkInfo_P30_Pine(),
								recEvaluation.getMarkInfo_P30_Spruce(),
								recEvaluation.getMarkInfo_ReducedBy()
								);
							}


						}
					}	// for (UINT i = 0;i < vecEvaluation.size();i++)
				}	// if (vecEvaluation.size() > 0)
				// If there's no Evaluation stands, reset "Mark- och Merv�rde" in database
				// for this property; 081022 p�d
				else
				{
					// If there's not evaluation stands, just reset "Mark- och Merv�rde"; 081022 p�d
					if (m_pDB != NULL)
					{
						m_pDB->updObjPropTraktSUM_2(recElvProperty.getPropID_pk(),recElvProperty.getPropObjectID_pk(),0.0,0.0);
					}
				}

				// Get aggregate information from "elv_evaluation_table" and add to "elv_properties_table"; 080515 p�d
				m_pDB->getObjPropTraktSUM_landvalue_precut(recEvaluation.getEValObjID_pk(),
																									 recEvaluation.getEValPropID_pk(),
																									 &fSUMLandValue,
																									 &fSUMPreCut);
				// Update table "elv_properties_table" with information on aggregate LandValue and PreCut; 080514 p�d
				m_pDB->updObjPropTraktSUM_2(recEvaluation.getEValPropID_pk(),
																		recEvaluation.getEValObjID_pk(),
																	  fSUMLandValue,
																	  fSUMPreCut);

				// Update Areal in Property table, because area is calculatecd from SUM of areal in elv_evaluation_table; 090914 p�d
				m_pDB->getObjPropTraktSUM_stands_areal(nObjID,nPropID,&nNumOfStands,&fAreal);
				m_pDB->updObjPropTraktSUM_7(nPropID,nObjID,fAreal);

				// I think we saould save, into table "elv_properties_table" on prop_status2
				// which forrest norm's selected; 080917 p�d
				if (recObject.getObjUseNormID() > -1 && recObject.getObjUseNormID() < 10)
					m_pDB->updPropertyStatus2(recEvaluation.getEValPropID_pk(),
																		recEvaluation.getEValObjID_pk(),
																		recObject.getObjUseNormID());
				else
					m_pDB->updPropertyStatus2(recEvaluation.getEValPropID_pk(),
																		recEvaluation.getEValObjID_pk(),
																		0);

				// Add information on "Frivillig uppg�relse"; 080515 p�d
				caluclateVoluntaryDeal(recElvProperty);

				// Add information on "F�rdyrad avverkning"; 080515 p�d
				calculateHigherCosts(recElvProperty);

			}	// if (m_pDB != NULL)
		}	// if (pRec != NULL && m_pDB != NULL)
	}	// if (pRow != NULL)

	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	setActivePropertyInReport();

}

void CPropertiesFormView::calculateAllEvaluationStands(CProgressDlg &prog_dlg)
{
	CString S;
	int nObjID = -1;
	int nPropID = -1;
	double fSUMLandValue = 0.0;
	double fSUMPreCut = 0.0;
	CString sArea;				// 2009-09-21 p�d
	int nAreaIndex = -1;	// 2009-09-21 p�d
	CString sName,sDoneBy,sNotes;

	CTransaction_elv_object recObject;
	CTransaction_trakt recTrakt;
	vecObjectTemplate_p30_table vecP30_table;
	vecObjectTemplate_p30_nn_table vecP30_nn_table;
	vecTransactionTraktData vecTraktData;
	vecTransaction_elv_m3sk_per_specie vecM3Sk_in_out;
	vecTransactionSampleTree vecRandTrees;
	vecTransaction_eval_evaluation vecEvaluation;
	CTransaction_eval_evaluation recEvaluation;
	CStringArray arrLandValueLog;
	CTransaction_elv_return_storm_dry recReturnStormDry;
	CTransaction_elv_return_land_and_precut recLandValueAndPreCut;
	mapDouble mapRandTreesVolume;
	CTransaction_elv_properties recElvProperty;
	vecTransactionTrakt vecTrakts;

	vecTransactionSpecies vecP30Species;	// Added 2008-11-07 P�D

	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	if (pRows == NULL || m_pDB == NULL) return;

	// If object contain no evaluations stand we don't need to run this function; Optimization by Peter
	if( !m_pDB->checkObjectEvaluationsExist(getActivePropertyRecord()->getPropObjectID_pk()) ) return;

	if (prog_dlg.GetSafeHwnd())
	{
		prog_dlg.setProgressRange_props(pRows->GetCount());
	}

	for (int i = 0;i < pRows->GetCount();i++)
	{

		CXTPReportRow *pRow = pRows->GetAt(i);
		if (pRow != NULL)
		{
			CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
			if (pRec != NULL)
			{
				recElvProperty = pRec->getRecELVProp();

				//if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
				if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()))
				{
					if (prog_dlg.GetSafeHwnd())
					{
						prog_dlg.setPropertyName(recElvProperty.getPropName());
						prog_dlg.setProgressPos_props(i+1);
					}
					// Try to collect some SUM. data for this property.
					// I.e. SUM data on Trakt(s) connected to this property; 080430 p�d
					nPropID = recElvProperty.getPropID_pk();
					// Make sure we have the latest Object information. Do this only once as object data won't change; Optimization by Peter
					if( nObjID == -1 )
					{
						nObjID = recElvProperty.getPropObjectID_pk();
						m_pDB->getObject(nObjID,recObject);
					}

					// Get Traktdata from table "esti_trakt_data_table"; 080508 p�d
					// Info on e.g. "Tr�dslagsblandning"; 080508 p�d
					vecTraktData.clear();
					vecM3Sk_in_out.clear();

					// Load P30 table for Object; 090421 p�d
					TemplateParser parser;
					if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
					{
						if (parser.LoadFromBuffer(recObject.getObjP30XML()))
						{
							parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
						}
					}
					else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
					{
						if (parser.LoadFromBuffer(recObject.getObjP30XML()))
						{
							parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
							setP30NN_SIMax(vecP30_nn_table);

						}
					}
					else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
					{
						if (parser.LoadFromBuffer(recObject.getObjP30XML()))
						{
							parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
							setP30NN_SIMax(vecP30_nn_table);

						}
					}

					m_pDB->getObjectEvaluation(nPropID,nObjID,vecEvaluation);

					// I think we should try to do some "Intr�ngsv�rdering", here. 
					// I.e. calculate "Storm- och Torkskador" and "Kanttr�d"; 080508 p�d
					arrLandValueLog.RemoveAll();

					// Check if there's any Evaluated stands to calculate; 080514 p�d
					if (vecEvaluation.size() > 0)
					{
						if (prog_dlg.GetSafeHwnd())
						{
							prog_dlg.setProgressRange_stands(vecEvaluation.size());
						}

						//HMS-94 H�mtar p30 tr�dslagsinfo sparat per object och per tr�dslag
						getObjectp30Speciesinfo(vecP30Species,recObject.getObjID_pk());

						for (UINT ii = 0;ii < vecEvaluation.size();ii++)
						{

							recEvaluation = vecEvaluation[ii];
							if (prog_dlg.GetSafeHwnd())
							{
								prog_dlg.setStandName(recEvaluation.getEValName());
								prog_dlg.setProgressPos_stands(ii+1);
								doEvents();
							}
							if (recEvaluation.getEValType() == EVAL_TYPE_VARDERING)
							{
								doUMForrestNorm(1,	// "V�rdering"
									m_mapForrestNormErrText,
									recObject,							// In-data
									recTrakt,								// In-data
									vecP30_table,						// In-data
									vecP30_nn_table,				// In-data, new forest norm; 090421 p�d
									vecTraktData,						// In-data
									vecM3Sk_in_out,					// In-data
									vecRandTrees,						// In-data
									recEvaluation,					// In-data
									vecTrakts,							// In-data; 080915 p�d
									vecP30Species,
									arrLandValueLog,				// Return data
									recReturnStormDry,			// Return data
									recLandValueAndPreCut,	// Return data
									mapRandTreesVolume);		// Return data

								double preCutValue = recLandValueAndPreCut.getPreCutValue();
								double preCutValueHa = recLandValueAndPreCut.getPreCutValue_ha();

								if(recEvaluation.getEValForrestType()==1) //Obehandlad = ingen f�rtidig ers
								{
									preCutValue=0;
									preCutValueHa=0;
								}


								// Save calculation to DB; 080514 p�d
								m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
																			 recEvaluation.getEValPropID_pk(),
																			 recEvaluation.getEValObjID_pk(),
																			 recEvaluation.getEValType(),
																			 recLandValueAndPreCut.getLandValue_ha(),
																			 preCutValueHa,
																			 recLandValueAndPreCut.getLandValue(),
									preCutValue,
									//HMS-49 Info om f�rtidig avverkning 20191126 J�
									recLandValueAndPreCut.getPreCutInfo_P30_Pine(),
									recLandValueAndPreCut.getPreCutInfo_P30_Spruce(),
									recLandValueAndPreCut.getPreCutInfo_P30_Birch(),
									recLandValueAndPreCut.getPreCutInfo_Andel_Pine(),
									recLandValueAndPreCut.getPreCutInfo_Andel_Spruce(),
									recLandValueAndPreCut.getPreCutInfo_Andel_Birch(),
									recLandValueAndPreCut.getPreCutInfo_CorrFact(),
									recLandValueAndPreCut.getPreCutInfo_Ers_Pine(),
									recLandValueAndPreCut.getPreCutInfo_Ers_Spruce(),
									recLandValueAndPreCut.getPreCutInfo_Ers_Birch(),
									recLandValueAndPreCut.getPreCutInfo_ReducedBy(),
									//HMS-48 Info om markv�rde 20200427 J�
									recLandValueAndPreCut.getMarkInfo_ValuePine(),
									recLandValueAndPreCut.getMarkInfo_ValueSpruce(),
									recLandValueAndPreCut.getMarkInfo_Andel_Pine(),
									recLandValueAndPreCut.getMarkInfo_Andel_Spruce(),
									recLandValueAndPreCut.getMarkInfo_P30_Pine(),
									recLandValueAndPreCut.getMarkInfo_P30_Spruce(),
									recLandValueAndPreCut.getMarkInfo_ReducedBy()
									);
							}	// if (recEvaluation.getEValType() == EVAL_TYPE_1)
							else
							{

								// Save calculation to DB; 080514 p�d
								m_pDB->updObjectEvaluation_calculated_data(recEvaluation.getEValID_pk(),
									recEvaluation.getEValPropID_pk(),
									recEvaluation.getEValObjID_pk(),
									recEvaluation.getEValType(),
									recEvaluation.getEValLandValue_ha(),
									recEvaluation.getEValPreCut_ha(),
									recEvaluation.getEValLandValue(),
									recEvaluation.getEValPreCut(),
									//HMS-49 In med info om f�rtidig avverkning �ven om typen �r taxering??
									recEvaluation.getPreCutInfo_P30_Pine(),
									recEvaluation.getPreCutInfo_P30_Spruce(),
									recEvaluation.getPreCutInfo_P30_Birch(),
									recEvaluation.getPreCutInfo_Andel_Pine(),
									recEvaluation.getPreCutInfo_Andel_Spruce(),
									recEvaluation.getPreCutInfo_Andel_Birch(),
									recEvaluation.getPreCutInfo_CorrFact(),
									recEvaluation.getPreCutInfo_Ers_Pine(),
									recEvaluation.getPreCutInfo_Ers_Spruce(),
									recEvaluation.getPreCutInfo_Ers_Birch(),
									recEvaluation.getPreCutInfo_ReducedBy(),
									//HMS-48 Info om markv�rde 20200427 J�
									recEvaluation.getMarkInfo_ValuePine(),
									recEvaluation.getMarkInfo_ValueSpruce(),
									recEvaluation.getMarkInfo_Andel_Pine(),
									recEvaluation.getMarkInfo_Andel_Spruce(),
									recEvaluation.getMarkInfo_P30_Pine(),
									recEvaluation.getMarkInfo_P30_Spruce(),
									recEvaluation.getMarkInfo_ReducedBy()
									);
							}
						}	// for (UINT i = 0;i < vecEvaluation.size();i++)

						// Get aggregate information from "elv_evaluation_table" and add to "elv_properties_table"; 080515 p�d
						m_pDB->getObjPropTraktSUM_landvalue_precut(recEvaluation.getEValObjID_pk(),
																	 recEvaluation.getEValPropID_pk(),
																	 &fSUMLandValue,
																	 &fSUMPreCut);
						// Update table "elv_properties_table" with information on aggregate LandValue and PreCut; 080514 p�d
						m_pDB->updObjPropTraktSUM_2(recEvaluation.getEValPropID_pk(),
													recEvaluation.getEValObjID_pk(),
													fSUMLandValue,
													fSUMPreCut);

						// I think we saould save, into table "elv_properties_table" on prop_status2
						// which forrest norm's selected; 080917 p�d
						if (recObject.getObjUseNormID() > -1 && recObject.getObjUseNormID() < 10)
						{
							m_pDB->updPropertyStatus2(recEvaluation.getEValPropID_pk(),
														recEvaluation.getEValObjID_pk(),
														recObject.getObjUseNormID());
						}
						else	// Set default Forrest norm = "1950:�rs skogsnorm"
						{
							m_pDB->updPropertyStatus2(recEvaluation.getEValPropID_pk(),
														recEvaluation.getEValObjID_pk(),
														0);
						}
					}	// if (vecEvaluation.size() > 0)

					/*
					// Add information on "Frivillig uppg�relse"; 080515 p�d
					caluclateVoluntaryDeal(recElvProperty);

					// Add information on "F�rdyrad avverkning"; 080515 p�d
					calculateHigherCosts(recElvProperty);
					*/
				}	// if (pRec != NULL)
			}	// if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
		}	// if (pRow != NULL)
	}	// for (int i = 0;i < pRows->GetCount();i++)

	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	setActivePropertyInReport();
}


// "Ber�kna frivillig uppg�relse"
// "Regler f�r ber�kning av frivillig uppg�relse
//	LOKALT N�T:
//	* Om ers�ttningen f�r intr�nget (Matkv�rde+F�rtidig avverkning+Storm- och Tork+Kanttr�d)
//		�r mindre �n 3% av ett basbelopp, blir ers�ttningen = 3% av basbeloppet
//	* Om ers�ttningen f�r intr�nget �r st�rre �n 3% av ett basbelopp, blir
//		ers�ttningen = 15% (angivet procenttal < max procent) av intr�ngsers�ttningen
//	REGIONAL N�T:
//	* Ers�ttningen ges enligt: 3% av ett basbelopp + 15% (angiven ers�ttning < max procent) f�r intr�nget
// Till�g #4835 L�gg till uppr�kning(expropriation p� intr�ngsers�ttningarna) 20160321 J� om inte underh�ll, om underh�ll s� inget till�gg
double CPropertiesFormView::caluclateVoluntaryDeal(CTransaction_elv_properties& prop)
{
	int nTypeOfNet = -1;	// Lokalt = 0,Regionalt = 1
	int nObjID = -1;
	int nPropID = -1;
	int nObjTyp=0;
	double fLandValue = 0.0;
	double fPreCutValue = 0.0;
	double fStormDryValue = 0.0;
	double fRandTreesValue = 0.0;
	double fPriceBase = 0.0;
	double fMaxPercent = 0.0;
	double fPercent = 0.0;
	double f3Percent = 0.0;
	double fSUMCompensation = 0.0;
	double fMaxCompensation = 0.0;
	double fActualCompensation = 0.0;
	double fCompensation = 0.0;
	double fPercentOfPriceBase = 0.0;
	double fExpropriationPerc=0.0;
    double fSUMExpropriation=0.0;
	double fTimberVolume = 0.0;
	double fTimberValue = 0.0;
	double fTimberCost = 0.0;
	double fTimberRotnetto = 0.0;
	double fGrotVolume = 0.0;
	double fGrotValue = 0.0;
	double fGrotCost = 0.0;

	// Get information on "Basbelopp","Procenttal"; 080515 p�d
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL && m_pDB != NULL)
	{
		// Setup data; 080515 p�d
		nObjID = pObj->getObjID_pk();
		nPropID = prop.getPropID_pk();
		/*nTypeOfNet = pObj->getObjTypeOfNet();
		fPriceBase = pObj->getObjPriceBase();
		fMaxPercent = pObj->getObjMaxPercent();
		fPercent = pObj->getObjPercent();*/
		m_pDB->getObjVolDealValues(nObjID, &nTypeOfNet, &fPriceBase, &fMaxPercent, &fPercent, &fPercentOfPriceBase);
/*
		S.Format("caluclateVoluntaryDeal\nObj %d\nProp %d\nNet %d",nObjID,nPropID,nTypeOfNet);
		AfxMessageBox(S);
*/
		// Get aggregate information from database, on Infring; 080515 p�d
		m_pDB->getObjProp_infr(nObjID,nPropID,&fLandValue,&fPreCutValue,&fStormDryValue,&fRandTreesValue);
		m_pDB->getObjPropTraktSUM_rotpost(nObjID,nPropID,&fTimberVolume,&fTimberValue,&fTimberCost,&fTimberRotnetto);
		m_pDB->getObjPropTraktSUM_grot(nObjID,nPropID,&fGrotVolume,&fGrotValue,&fGrotCost);
		fSUMCompensation = fLandValue + fPreCutValue + fStormDryValue + fRandTreesValue;


		// Till�g #4835 L�gg till uppr�kning(expropriation p� intr�ngsers�ttningarna) 20160321 J�
		nObjTyp=_tstoi(pObj->getObjTypeOfInfring());
		if(nObjTyp != 2) 
		{
			fExpropriationPerc=pObj->getObjRecalcBy();
			fExpropriationPerc/=100.0;
			fSUMExpropriation=fSUMCompensation*fExpropriationPerc;
			fSUMCompensation+=fSUMExpropriation;
			if(nTypeOfNet != TYPEOFNET_SVENSKAKRAFTNAT)	// Ej ha med p�slag p� virke om svenska kraftn�t vald som ers�ttningspolicy #5505 20180212 J�
			{

				//fSUMCompensation+=(fTimberRotnetto+fGrotValue)*fExpropriationPerc;
				//HMS-112 fra ifr�n grot kostnad fr�n v�rde innnan ber�kning av kompensation 20240213 J�
				fSUMCompensation+=(fTimberRotnetto+(fGrotValue-fGrotCost))*fExpropriationPerc;
			}
		}

		f3Percent = fPriceBase * fPercentOfPriceBase/100.0; // 0.03 = 3%
		// Max compansation
		fMaxCompensation = fPriceBase * (fMaxPercent/100.0);
		// Check which type of net we're dealing with; 080515 p�d
		if (nTypeOfNet == TYPEOFNET_LOCAL)	// "Lokalt n�t"
		{
			if (fSUMCompensation < f3Percent)
			{
				fCompensation = f3Percent;
			}
			else if (fSUMCompensation >= f3Percent)
			{
				// calculate the actual cxompensation; 080515 p�d
				fActualCompensation = fSUMCompensation * fPercent/100.0;
				// Compensation can't be > Max compensation; 080515 p�d
				if (fActualCompensation > fMaxCompensation)
					fCompensation = fMaxCompensation;
				else if (fActualCompensation > f3Percent)
					fCompensation = fActualCompensation;
				else
					fCompensation = f3Percent;
			}
		}
		else if (nTypeOfNet == TYPEOFNET_SVENSKAKRAFTNAT)	// Svenska kraftn�t
		{
			/*	Vid frivilligt godk�nnande av ers�ttningserbjudande utg�r
			� dels ett grundbelopp motsvarande 10% av prisbasbeloppet, vid v�rdetidpunkten,
			� dels 20% av intr�ngsers�ttningen (best�ende skada + 25% p�slaget enligt expr.lagen p� best�ende skada), dock max 20% av prisbasbeloppet. OBS! Virket (25% enl. expr.lagen) ska inte vara med vid ber�kning av den frivilliga ers�ttningen.

			Exempel 1
			F�ruts�ttningar :
			Intr�ngsers�ttning (best�ende skada) 10.000:-
			25% enligt expr.lagen p� best�ende skada 2.500:-

			Frivillig ers�ttning n�r 20% av intr�ngsers�ttningen �r max 20% av prisbasbeloppet:
			10% av prisbasbeloppet (45.500:- * 10%) = 4.550:-
			20% av intr�ngsers�ttningen (10.000:- + 2.500) * 20% = 2.500:-
			Summa frivillig ers�ttning = 7.050:-

			Exempel 2
			F�ruts�ttningar :
			Intr�ngsers�ttning (best�ende skada) 100.000:-
			25% enligt expr.lagen p� best�ende skada 25.000:-

			Frivillig ers�ttning n�r 20% av intr�ngsers�ttningen �verstiger 20% av prisbasbeloppet:
			10% av prisbasbeloppet (45.500:- * 10%) = 4.550:-
			20% av intr�ngsers�ttningen (100.000:- + 25.000) * 20% max 20% av prisbasbeloppet = 9.100:-
			Summa Max frivillig ers�ttning 2018. = 13.650:-   */

			double fSvenskaKraftnatIntrangsErsattning=fSUMCompensation * fPercent/100.0;
			double fSvenskaKraftnatGrundErsattning=f3Percent;
			if(fSvenskaKraftnatIntrangsErsattning<=fMaxCompensation) // Om Intr�ngsers�ttningen �r mindre �n max ers�ttning utg�r Intr�ngsers�ttningen samt Grunders�ttning(procent av prisbasbelopp)
				fCompensation=fSvenskaKraftnatGrundErsattning+fSvenskaKraftnatIntrangsErsattning;
			else
				fCompensation=fSvenskaKraftnatGrundErsattning+fMaxCompensation;

				/*
				// calculate the actual cxompensation; 080515 p�d
				fActualCompensation = f3Percent + fSUMCompensation * fPercent/100.0;
				// Compensation can't be > Max compensation; 080515 p�d
				if (fActualCompensation > fMaxCompensation)
				fCompensation = fMaxCompensation;
				else
				fCompensation = fActualCompensation;*/
		}
		else if (nTypeOfNet == TYPEOFNET_SWEDISHENERGY)	// "Svensk Energi 2014"
		{
			if (fSUMCompensation * (fPercent / 100.0 + 1.0) < f3Percent)
			{				
				fCompensation = f3Percent - fSUMCompensation; 

			}
			else
			{
				if (fSUMCompensation * fPercent > fPriceBase * fMaxPercent)
				{
					fCompensation = fPriceBase * fMaxPercent / 100.0;
				}
				else
				{
					fCompensation = fSUMCompensation * fPercent / 100.0;
				}
			}
		}
		else // "Svensk Energi 2019" HMS-75 20220208 J�
		{
			//Dels Grunders�ttning, 5% av det vid var tid g�llande prisbasbeloppet
			fCompensation=0.0;
			fCompensation=f3Percent;


			//Dels 20% av intr�ngsers�ttningen (inklusive p�slag med 25%) dock h�gst 20% av det vid var tid g�llande prisbasbeloppet
			if (fSUMCompensation * fPercent > fPriceBase * fMaxPercent)
			{
				fCompensation += fPriceBase * fMaxPercent / 100.0;
			}
			else
			{
				fCompensation += fSUMCompensation * fPercent / 100.0;
			}			
		}

		m_pDB->updObjPropTraktSUM_3(nPropID,nObjID,fCompensation);
	}	// if (pObj != NULL && m_pDB != NULL)


	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	return 0.0;
}

// "Ber�kna: F�rdyrad avverkning"
// "Ber�knas som: totala summan m3sk (I gatan och kanttr�d)
//  Kontrollera tabell f�r att f� kr/m3sk baserad p� sum m3sk
//	tag sum m3sk * tabellv�rdet; 080515 p�d
double CPropertiesFormView::calculateHigherCosts(CTransaction_elv_properties& prop)
{
	CString S;
	int nObjID = -1;
	int nPropID = -1;
	double fM3Sk = 0.0;
	double fM3Sk_last = 0.0;
	double fKrPerM3Sk = 0.0;
	double fBaseComp = 0.0;
	double fRotpost = 0.0;
	double fSUMHigherCosts = 0.0;
	int nTypeSet = -1;
	double fBreakValue = 0.0;
	double fFactor = 0.0;
	CString sName,sDoneBy;
	BOOL bFound = FALSE;
	CTransaction_elv_object recObject;
	vecObjectTemplate_hcost_table vecHCost;
	CObjectTemplate_hcost_table recHCost;
	if (m_pDB != NULL)
	{
		nObjID = prop.getPropObjectID_pk();
		nPropID = prop.getPropID_pk();

		// We need to reload the latest objectdata from DB; 080520 p�d
		m_pDB->getObject(nObjID,recObject);

		// Get Total volume in m3sk for property; 080515 p�d
		fM3Sk = prop.getPropM3Sk();
		fRotpost = prop.getPropRotpost();	// Added 090529 p�d


		TemplateParser *pars = new TemplateParser(); // Parser for P30 and HighCost; 080407 p�d
		if (pars != NULL)
		{
			if (pars->LoadFromBuffer(recObject.getObjHCostXML() ))
			{
				pars->getObjTmplHCost(&nTypeSet,&fBreakValue,&fFactor,vecHCost,sName,sDoneBy);
				if (nTypeSet != 1 && nTypeSet != 2) nTypeSet = 1;
			}
			delete pars;
		}
		if (nTypeSet == 1)
		{
			if (vecHCost.size() > 0)
			{
				// Try to run the vecHCost backwards to match the volume; 080515 p�d
				for (UINT i = 0;i < vecHCost.size();i++)
				{
					recHCost = vecHCost[i];
					if (fM3Sk >= fM3Sk_last && fM3Sk <= recHCost.getM3Sk())
					{
						fKrPerM3Sk = recHCost.getPriceM3Sk();
						fBaseComp = recHCost.getBaseComp();
						bFound = TRUE;
						break;
					}
					fM3Sk_last = recHCost.getM3Sk();
				}	// for (UINT i = vecHCost.size()-1;i <= 0;i--)
				// No value vas found, set Table value (kr/m3) to last entry
				// in table; 080515 p�d
				if (!bFound)
				{
					fKrPerM3Sk = vecHCost[vecHCost.size()-1].getPriceM3Sk();
					fBaseComp = vecHCost[vecHCost.size()-1].getBaseComp();
				}
				// Calculate SUM of Higher costs; 080515 p�d
				fSUMHigherCosts = (fM3Sk * fKrPerM3Sk) + fBaseComp;
				if (fSUMHigherCosts < 0.0) fSUMHigherCosts = 0.0;
			}	// if (vecHCost.size() > 0)
		}
		else
		{
			// Calculate Higher costs ("F�rdyrad avverkning") based on "Vattenfall"; 090529 p�d
			// Change calculation on (fRotpost <= fBreakValue) from: 
			//	fSUMHigherCosts = fRotpost to fSUMHigherCosts = fRotpost + (fM3Sk*fFactor): 090701 p�d
			if (fRotpost <= fBreakValue)
				fSUMHigherCosts = fRotpost + (fM3Sk*fFactor);
			else if (fRotpost > fBreakValue)
				fSUMHigherCosts = fBreakValue + (fM3Sk*fFactor);
			
			if (fSUMHigherCosts < 0.0) fSUMHigherCosts = 0.0;

		}
		m_pDB->updObjPropTraktSUM_4(nPropID,nObjID,fM3Sk,fSUMHigherCosts);
	}

	return 0.0;

}

// Change "Best�ndsmall"; 2009-05-06 p�d
// Method used to change (update) ALL stands in an Object,
// based on a "Best�ndsmall"; 090506 p�d
BOOL CPropertiesFormView::updateAllTraktTemplates(CTransaction_template xml_tmpl,vecTransaction_elv_properties &vec_props)
{
	int nHgtOverSea;
	int nLatitude;
	int nLongitude;
	TCHAR szGrowthArea[127];
	TCHAR szSI_H100[127];
	double fCorrFac;

	int nExchangeID;
	TCHAR szExchange[127];
	int nPricelistID;
	int nPricelistTypeOf;
	TCHAR szPricelist[127];
	double fDCLS;
	int nCostsTmplID;
	TCHAR szCostsTmplName[127];

	int nQualDescIndex;
	TCHAR szQualDesc[127];
	double fSkToFub;
	double fFubToTo;
	double fSkToUb;
	bool bIsAtCoast;
	bool bIsSouthEast;
	bool bIsRegion5;
	bool bIsPartOfPlot;
	TCHAR szSI_H100_Pine[127];
	double fH25;
	double fGreenCrown;
	int nTranspDist1;
	int nTranspDist2;

	int nGrotFuncID;
	double fGrotPercent;
	double fGrotPrice;
	double fGrotCosts;

	CString sExtraInfo,S;

	CTransaction_pricelist recPricelist;
	CTransaction_costtempl recCostsTmpl;

	CTransaction_trakt_set_spc tsetspc;

	vecTransactionSpecies vec1;
	vecUCFunctionList vec2;
	vecTransactionTemplateTransfers vec3;

	CTransaction_trakt recTrakt;
	vecTransactionTrakt vecTrakt;

	CTransaction_elv_properties recProp;


	CTransaction_elv_object *pObj = getActiveObject();
	//getPropertiesFromDB(pObj->getObjID_pk());
	// Check if there's any data do work with; 090506 p�d
	if (vec_props.size() == 0) return FALSE;

	TemplateParser *pars = new TemplateParser();
	if (pars == NULL) return FALSE;

	if (!pars->LoadFromBuffer(xml_tmpl.getTemplateFile())) return FALSE;
	
	getTemplateMainSettings(pars,&nHgtOverSea,&nLatitude,&nLongitude,szGrowthArea,szSI_H100,&fCorrFac,&nCostsTmplID,szCostsTmplName,
												 &bIsAtCoast,&bIsSouthEast,&bIsRegion5,&bIsPartOfPlot,szSI_H100_Pine);

	// Setup the "Extra information"; 070906 p�d
	// Extra info is set on Trakt level; 071207 p�d
	// OBS! Extra info isn't used. Not sure if we'll change this data; 090928 p�d
/*	Uncomment this, to set information on settings for "S�derbergs", from stand-template; 090928 p�d
	sExtraInfo.Format(_T("%d;%d;%d;%d;%s;"),
										bIsAtCoast,
										bIsSouthEast,
										bIsRegion5,
										bIsPartOfPlot,
										szSI_H100_Pine);
*/
		getTemplateSettings(pars,&nExchangeID,szExchange,&nPricelistID, &nPricelistTypeOf,szPricelist,&fDCLS);

		// Try to find pricelist in prl_pricelist_table, based on information in selected template; 070906 p�d
		getPricelistBasedOnTemplateInformation(nPricelistID,nPricelistTypeOf,recPricelist);
	
		getCostsTmplBasedOnTemplateInformation(nCostsTmplID,szCostsTmplName,recCostsTmpl);

		for (UINT prop_cnt = 0;prop_cnt < vec_props.size();prop_cnt++)
		{
			recProp = vec_props[prop_cnt];
			if (canWeCalculateThisProp_cached(recProp.getPropStatus()))	
			{
				if (m_pDB != NULL)
				{
					m_pDB->getTrakts_match_stand_by_elv_cruise(recProp.getPropObjectID_pk(),recProp.getPropID_pk(),vecTrakt);
					for (UINT trakt_cnt = 0;trakt_cnt < vecTrakt.size();trakt_cnt++)
					{
						recTrakt = vecTrakt[trakt_cnt];
						CTransaction_trakt_misc_data data = CTransaction_trakt_misc_data(recTrakt.getTraktID(),
																																						 szPricelist,
																																						 nPricelistTypeOf,
																																						 recPricelist.getPricelistFile(),
																																						 recCostsTmpl.getTemplateName(),
																																						 recCostsTmpl.getTypeOf(),
																																						 recCostsTmpl.getTemplateFile(),
																																						 fDCLS,
																																						 _T(""));
						m_pDB->updTraktMiscData_prl(data);
						m_pDB->updTraktMiscData_dcls(data);
						m_pDB->updTraktMiscData_costtmpl(data);

						// Special case, need to do this method, setShowSettingsPane()
						// after data has been saved. This is because if we run this
						// method (in saveTrakt()), populating Settings-grid in 
						// CMDIStandEntryFormFrame::setupTypeOfExchangefunctionsInPropertyGrid()
						// the information on Pricelist etc. isn't valid; 070904 p�d

						// get species in Template; 070905 p�d
						getTemplateSpecies(pars,vec1);

						// remove all the previous transfers
						m_pDB->delTraktTrans(recTrakt.getTraktID());


						// Make sure there's species; 070905 p�d
						if (vec1.size() > 0)
						{
							for (UINT i = 0;i < vec1.size();i++)
							{
								getTemplateFunctionsPerSpcecie(pars,vec1[i].getSpcID(),vec2,&fFubToTo,&fSkToUb);

								getTemplateMiscFunctionsPerSpcecie(pars,vec1[i].getSpcID(),&nQualDescIndex,szQualDesc,&fSkToFub,
																									 &fFubToTo,&fH25,&nTranspDist1,&nTranspDist2,&fGreenCrown);

								getTemplateGrotDataPerSpcecie(pars,vec1[i].getSpcID(),&nGrotFuncID,&fGrotPercent,&fGrotPrice,&fGrotCosts);

								// Check to see if we have functions specified for specie; 070905 p�d
								if (vec2.size() == 4)	// 4 equals Height,Volume,Bark and Volume under bark; 070905 p�d
								{
									CTransaction_trakt_set_spc tsetspc = CTransaction_trakt_set_spc(vec1[i].getSpcID(),
																																						 vec1[i].getSpcID(),
																																						 recTrakt.getTraktID(),
					  	 																															 STMP_LEN_WITHDRAW,
																																						 vec1[i].getSpcID(),
																																						 // Set Height-function for Specie
																																						 vec2[0].getID(),
																																						 vec2[0].getSpcID(),
																																						 vec2[0].getIndex(),
																																						 // Set Volume-function for Specie
																																						 vec2[1].getID(),
																																						 vec2[1].getSpcID(),
																																						 vec2[1].getIndex(),
																																						 // Set Bark-function for Specie
																																						 vec2[2].getID(),
																																						 vec2[2].getSpcID(),
																																						 vec2[2].getIndex(),
																																						 // Set Volume-function under bark for Specie
																																						 vec2[3].getID(),
																																						 vec2[3].getSpcID(),
																																						 vec2[3].getIndex(),																																					 
																																						 fSkToUb,
																																						 fSkToFub,
																																						 fFubToTo,
																																						 fDCLS,	// Diameterclass
																																						 nQualDescIndex,
																																						 sExtraInfo,
																																						 nTranspDist1,
																																						 nTranspDist2,
																																						 _T(""),
																																						 szQualDesc,
																																						 nGrotFuncID,
																																						 fGrotPercent,
																																						 fGrotPrice,
																																						 fGrotCosts,
																																						 fH25,
																																						 fGreenCrown);	// #4561

										// Update table "esti_trakt_set_spc_table"; 090928 p�d
										if (!m_pDB->addTraktSetSpc_hgtfunc(tsetspc)) 
											m_pDB->updTraktSetSpc_hgtfunc(tsetspc);

										//if (!m_pDB->addTraktSetSpc_volfunc(tsetspc)) 
											m_pDB->updTraktSetSpc_volfunc(tsetspc);						

										//if (!m_pDB->addTraktSetSpc_barkfunc(tsetspc))	
											m_pDB->updTraktSetSpc_barkfunc(tsetspc);							

										//if (!m_pDB->addTraktSetSpc_m3fub_m3to(tsetspc))	
											m_pDB->updTraktSetSpc_m3fub_m3to(tsetspc);

										//if (!m_pDB->addTraktSetSpc_m3sk_m3ub(tsetspc))	
											m_pDB->updTraktSetSpc_volfunc_ub(tsetspc);					
											m_pDB->updTraktSetSpc_m3sk_m3ub(tsetspc);

										//if (!m_pDB->addTraktSetSpc_rest_of_misc_data(tsetspc))	
											m_pDB->updTraktSetSpc_rest_of_misc_data(tsetspc);

										//if (!m_pDB->addTraktSetSpc_grot(tsetspc))	
											m_pDB->updTraktSetSpc_grot(tsetspc);

										//if (!m_pDB->addTraktSetSpc_default_h25(tsetspc))	
											m_pDB->updTraktSetSpc_default_h25(tsetspc);

										// Update table "esti_trakt_data_table"; 090928 p�d
										// OBS! Uncomment these two updates, to add h25 and "Gr�nkrona", from stand-template; 090928 p�d
										m_pDB->updTraktData_h25(recTrakt.getTraktID(),vec1[i].getSpcID(),fH25);
										m_pDB->updTraktData_gcrown(recTrakt.getTraktID(),vec1[i].getSpcID(),fGreenCrown);
								
								}	// if (vec2.size() == 4)

								//------------------------------------------------------------------
								// UNCOMMENT TO DO THIS, NOT SURE; 101208 p�d
								// Remove transfers before adding/update; 101208 p�d
								//m_pDB->delTraktTrans(recTrakt.getTraktID(),vec1[i].getSpcID());
								//------------------------------------------------------------------

								// Try to get transafers set in Template; 070907 p�d			
								getTemplateTransfersPerSpcecie(pars,vec1[i].getSpcID(),vec3);
								// Do we have any transfers for this specie; 070907 p�d
								if (vec3.size() > 0)
								{
									for (UINT tat_cnt = 0;tat_cnt < vec3.size();tat_cnt++)
									{
										CTransaction_trakt_trans recTTrans = CTransaction_trakt_trans(vec1[i].getSpcID(),
																																	recTrakt.getTraktID(),
																																	vec3[tat_cnt].getFromAssortID(),
																																	vec3[tat_cnt].getToAssortID(),
																																	vec1[i].getSpcID(),
																																	STMP_LEN_WITHDRAW,
																																	vec3[tat_cnt].getFromAssort(),
																																	vec3[tat_cnt].getToAssort(),
																																	vec1[i].getSpcName(),
																																	vec3[tat_cnt].getM3Fub(),
																																	vec3[tat_cnt].getPercent(),
																																	0.0,
																																	_T(""));
										if (!m_pDB->addTraktTrans(recTTrans))
											m_pDB->updTraktTrans(recTTrans);
									}										
								}
							}	// for (UINT i = 0;i < vec1.size();i++)
						}	// if (vec1.size() > 0			
					}	// for (UINT trakt_cnt = 0;trakt_cnt < veTrakt.size();trakt_cnt++)
				}	// if (m_pDB != NULL)
				//-------------------------------------------------------------------------
				// L�gg in information i loggen, om best�ndsmall,prislista och kostnadsmall
				// per fastighet; 2012-11-30 P�D #3384
				if (m_pDB != NULL && logThis(4))
				{
					CString sLogMsg = L"";
					sLogMsg.Format(L"%s; %s %s (%s; %s)",recProp.getPropName(),m_sMsgBatchStandTmpl,xml_tmpl.getTemplateName(),szPricelist,szCostsTmplName);

					CTransaction_elv_properties_logbook recLogBook = CTransaction_elv_properties_logbook(recProp.getPropID_pk(),
																																																pObj->getObjID_pk(),
																																																getDateTimeEx2(),
																																																getUserName().MakeUpper(),
																																																sLogMsg,
																																															  _T(""),
																																															  -1);

					m_pDB->addPropertyLogBook(recLogBook);
				}
				//-------------------------------------------------------------------------
			}	// if (m_bConnected)	
		}	// for (UINT prop_cnt = 0;prop_cnt < vec_props.size();prop_cnt++)
	

		if (pars) delete pars;

		return TRUE;
}



// This method tries to find a pricelist, in prl_priselist_table, based
// on information in selected template; 070906 p�d
BOOL CPropertiesFormView::getPricelistBasedOnTemplateInformation(int id,int type_of,CTransaction_pricelist& data)
{
	vecTransactionPricelist vecPricelist;

	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getPricelists(vecPricelist))
			{
				if (vecPricelist.size() > 0)
				{
					for (UINT i = 0;i < vecPricelist.size();i++)
					{
						data = vecPricelist[i];
						if (data.getID() == id && data.getTypeOf() == type_of)
						{
							return TRUE;
						}
					}
				}
			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
	return FALSE;
}

BOOL CPropertiesFormView::getCostsTmplBasedOnTemplateInformation(int id,LPCTSTR costs_tmpl_name,CTransaction_costtempl& data)
{
	CString S;
	vecTransaction_costtempl vecCostsTmpl;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCosts(vecCostsTmpl,COST_TYPE_2))
			{
				if (vecCostsTmpl.size() > 0)
				{
					for (UINT i = 0;i < vecCostsTmpl.size();i++)
					{
						data = vecCostsTmpl[i];
						// OBS! Check for "Kostnadsmallar f�r b�de Rotpost och Intr�ng"; 080306 p�d
						if (data.getID() == id && 
//							 (data.getTypeOf() == COST_TYPE_1 || data.getTypeOf() == COST_TYPE_2) && 
								data.getTemplateName().Compare(costs_tmpl_name) == 0)
						{
							return TRUE;
						}
					}
				}
			}
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
	return FALSE;
}


BOOL CPropertiesFormView::populateProperties(void)
{
	BOOL bIsOK = FALSE;
	CELVPropertyReportRec *pRec = NULL;
	CString sPropTabCaption,S;
	CString sStatusText;
	CString sNumOfEntriesText;
	CString sNumOfExtDocsText;
	int nIndexFocusedRow = -1;
	CTransaction_elv_object *pObj = getActiveObject();
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	vecTransaction_external_docs vecExternalDocs;
	short colRed;
	short colGreen;
	short colBlue;
	BOOL bFoundStatus = FALSE;

	if (pRow != NULL) nIndexFocusedRow = pRow->GetIndex();

	if (pObj != NULL)
	{
		m_wndReportProperties.BeginUpdate();
		m_wndReportProperties.ResetContent();

		// Always read the properties from DB; 080415 p�d
		getPropertiesFromDB(pObj->getObjID_pk());

		// No properties, quit and return FALSE; 090304 p�d
		if (m_vecELV_properties.size() > 0)
		{
			// Get number of entries in logbook per property for
			// this object; 080423 p�d
			getNumOfEntriesInLogBookPerPropertyFromDB(pObj->getObjID_pk());
			// We have data; 080415 p�d
			if (m_vecELV_properties.size() > 0)
			{
				
				for (UINT i = 0;i < m_vecELV_properties.size();i++)
				{
					m_recActiveProperty = m_vecELV_properties[i];

					addStatusConstraints();
				
					// Setup status text, based on value in rec.getPropStatus(); 080416 p�d
					if (m_recActiveProperty.getPropStatus() >= 0) // && m_recActiveProperty.getPropStatus() < m_arrStatus.GetCount())
					{
						bFoundStatus = FALSE;
						// Find property status by getting index of getPropStatusID_pk in m_vecPropStatus; 090930 p�d
						for (UINT i1 = 0;i1 < m_vecPropStatus.size();i1++)
						{
/*
							S.Format(_T("m_vecPropStatus[i1].getPropStatusID_pk() %d\nm_recActiveProperty.getPropStatus() %d"),
								m_vecPropStatus[i1].getPropStatusID_pk(),
								m_recActiveProperty.getPropStatus());
							AfxMessageBox(S);
*/
							if (m_vecPropStatus[i1].getPropStatusID_pk() == m_recActiveProperty.getPropStatus())
							{
								sStatusText = m_arrStatus[i1];
								colRed = m_vecPropStatus[i1].getPropStatusRCol();
								colGreen = m_vecPropStatus[i1].getPropStatusGCol();
								colBlue = m_vecPropStatus[i1].getPropStatusBCol();
								bFoundStatus = TRUE;

								//getPropActionStatusBkgndColor(m_vecPropStatus[i1].getPropStatusID_pk(),&colRed,&colGreen,&colBlue);
							}
						}	// for (UINT i1 = 0;i1 < m_vecPropStatus.size();i1++)
					}
					else
					{
						sStatusText = m_arrStatus[0];	// Set default to "Under utveckling"
					}

					if (!bFoundStatus)
					{
						sStatusText = m_arrStatus[0];	// Set default to "Under utveckling"
						// Set deafult color WHITE; 091009 p�d
						colRed = 255;
						colGreen = 255;
						colBlue = 255;
					}

					if (m_pDB != NULL)
						m_pDB->getExternalDocuments(TBL_ELV_OBJECT,m_recActiveProperty.getPropID_pk(),vecExternalDocs);


					sNumOfExtDocsText.Format(_T("%d %s"),vecExternalDocs.size(),m_sNumOfLogEntrySort);
					sNumOfEntriesText.Format(_T("%d %s"),m_mapNumOfLogBookEntriesPerProperty[m_recActiveProperty.getPropID_pk()],m_sNumOfLogEntrySort);
					m_wndReportProperties.AddRecord(pRec = new CELVPropertyReportRec(m_recActiveProperty.getPropID_pk(),
																																					 m_recActiveProperty.getPropObjectID_pk(),
																																					 (sStatusText),
																																					 (sNumOfEntriesText),
																																					  sNumOfExtDocsText,
																																						m_recActiveProperty,
																																						m_vecIconToogle));

					if (pRec != NULL)
					{
						pRec->setColumnBkgndColor(RGB(colRed,colGreen,colBlue));
						pRec = NULL;
					}
				}	// for (UINT i = 0;i < m_vecELV_properties.size();i++)
			}	// if (m_vecELV_properties.size() > 0)
			m_wndReportProperties.Populate();
			m_wndReportProperties.UpdateWindow();
			// Added 081006 p�d
	//		m_wndReportProperties.RedrawControl();
			// Set focused row; 081010 p�d
			if (pRow != NULL && pRows != NULL && nIndexFocusedRow > -1 && nIndexFocusedRow < pRows->GetCount())
			{
				m_wndReportProperties.SetFocusedRow(pRows->GetAt(nIndexFocusedRow));
			}	// if (pRow != NULL && pRows != NULL)
			bIsOK = TRUE;
		} // if (m_vecELV_properties.size() > 0)
	}

	// As an information to user, set number of Properties in the
	// tab caption; 080923 p�d
	CMDI1950ForrestNormFormView *pFormView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pFormView != NULL)
	{
		m_tabManager = pFormView->getTabCtrl().getTabPage(TAB_PROPERTIES);
		if (m_tabManager != NULL)
		{
			if (bIsOK)		
				sPropTabCaption.Format(_T("%s (%d)"),m_sPropTabCaption,m_wndReportProperties.GetRows()->GetCount());
			else
				sPropTabCaption.Format(_T("%s"),m_sPropTabCaption);
			m_tabManager->SetCaption(sPropTabCaption);
		}
	}
	m_wndReportProperties.EndUpdate();

	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	pRow = NULL;
	pRows = NULL;

	return bIsOK;
}

void CPropertiesFormView::runPrintout(CSTD_Reports &rec)
{
	CString sArgStr;
	CString sFileExtension;
	CString sReportPathAndFN;

	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow == NULL) return;

	CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
	if (pRec == NULL) return;

	CTransaction_elv_properties recELVProp = pRec->getRecELVProp();

	sArgStr.Format(_T("%d;%s;"),recELVProp.getPropObjectID_pk(),recELVProp.getPropGroupID());
	sReportPathAndFN.Format(_T("%s%s\\%s"),
													getReportsDir(),
													getLangSet(),
													rec.getFileName());

	if (fileExists(sReportPathAndFN))
	{
		// 081107 p�d
		// Try to get extension of file, to determin' if it's
		// a FastReport or Crystal Report.
		// I.e.
		//	.fr3 = FastReports
		//	.rpt = Crystal Reports
		sFileExtension = sReportPathAndFN.Right(3);

		if (sFileExtension.CompareNoCase(_T("fr3")) == 0)
		{
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
				(LPARAM)&_user_msg(300,	// ID = 333 for CrystalReport, ID = 300 for FastReports
				_T("OpenSuiteEx2"),			// Exported/Imported function
				_T("Reports.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports; 080214 p�d
				(sReportPathAndFN),		// Use this report
				_T(""),
				(sArgStr)));
		}
		else if (sFileExtension.CompareNoCase(_T("rpt")) == 0)
		{
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
				(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
				_T("OpenSuiteEx"),			// Exported/Imported function
				_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports; 080214 p�d
				(sReportPathAndFN),		// Use this report
				(sReportPathAndFN),
				(sArgStr)));
		}
	}
}

void CPropertiesFormView::setActivePropertyInReport(void)
{
	CString sTabCap,S;
	CTransaction_elv_properties recELVProp;
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_object recObj;
 	CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pFrame != NULL && pObj != NULL)
	{
		recObj = *pObj;
		if (pRow != NULL)
		{
			pRec = (CELVPropertyReportRec*)pRow->GetRecord();
			if (pRec != NULL)
			{
				recELVProp = pRec->getRecELVProp();

				pFrame->setActiveProperty(recELVProp,TRUE);

				getCruisingView()->populateReport(recELVProp.getPropID_pk(),recELVProp.getPropObjectID_pk(),recELVProp.getPropStatus(),recObj.getObjUseNormID());
				getEvaluatedView()->populateReport(recELVProp.getPropID_pk(),recELVProp.getPropObjectID_pk(),recELVProp.getPropStatus(),recObj.getObjUseNormID());
				// Set caption for Cruising tab, to display number of Cruising; 080923 p�d
				m_tabManager = m_wndTabControl.getTabPage(TAB_CRUISING);
				if (m_tabManager)
				{
					sTabCap.Format(_T("%s (%d)"),sCruisingTabCaption,getCruisingView()->getNumOfCruises());
					m_tabManager->SetCaption(sTabCap);
				}	// if (m_tabManager)
				// Set caption for Evalues tab, to display number of Evaluations; 080923 p�d
				m_tabManager = m_wndTabControl.getTabPage(TAB_EVALUATED);
				if (m_tabManager)
				{
					sTabCap.Format(_T("%s (%d)"),sEvalueTabCaption,getEvaluatedView()->getNumOfEvalues());
					m_tabManager->SetCaption(sTabCap);
				}	// if (m_tabManager)
				pRec = NULL;
			}
		}	// 	if (pRow != NULL)
		else
		{
			pFrame->setActiveProperty(CTransaction_elv_properties(),FALSE);
			// We need to populate here also, jisy to clear report; 080516 p�d
			getCruisingView()->populateReport(recELVProp.getPropID_pk(),recELVProp.getPropObjectID_pk(),recELVProp.getPropStatus(),recObj.getObjUseNormID());
			getEvaluatedView()->populateReport(recELVProp.getPropID_pk(),recELVProp.getPropObjectID_pk(),recELVProp.getPropStatus(),recObj.getObjUseNormID());
			// Set caption for Cruising tab, to display number of Cruising; 080923 p�d
			m_tabManager = m_wndTabControl.getTabPage(TAB_CRUISING);
			if (m_tabManager)
			{
				sTabCap.Format(_T("%s"),sCruisingTabCaption);
				m_tabManager->SetCaption(sTabCap);
			}	// if (m_tabManager)
			// Set caption for Evalues tab, to display number of Evaluations; 080923 p�d
			m_tabManager = m_wndTabControl.getTabPage(TAB_EVALUATED);
			if (m_tabManager)
			{
				sTabCap.Format(_T("%s"),sEvalueTabCaption);
				m_tabManager->SetCaption(sTabCap);
			}	// if (m_tabManager)
		}
	}
	pFrame = NULL;
	pRow = NULL;
}

#ifdef _DEBUG
void CPropertiesFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPropertiesFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// PUBLIC
void CPropertiesFormView::setSelectedProperties(vecTransactionProperty &vec)
{
	CString S;
	int nNormID = -1,nMaxSortNumber = 0;
	CTransaction_elv_properties recElvProp;
	CTransaction_property recFstProp;
	CTransaction_elv_object *pObj = getActiveObject();
	if (pObj != NULL)
	{
		nNormID = pObj->getObjUseNormID();
		m_vecSelectedProperties = vec;
		if (m_pDB != NULL) m_pDB->getPropertySortMax(pObj->getObjID_pk(),&nMaxSortNumber);
		// After this 'll add data from : m_vecSelectedProperties to m_vecELV_properties
		// and save to DB and Populate; 080415 p�d
		if (m_vecSelectedProperties.size() > 0)
		{
			for (UINT i = 0;i < m_vecSelectedProperties.size();i++)
			{			
				recFstProp = m_vecSelectedProperties[i];
				// Setup Propertyname adding "Block" and "Enhet"; 080416 p�d


				recElvProp = CTransaction_elv_properties(recFstProp.getID(),
																								 pObj->getObjID_pk(),
																								 _T(""),
																								 _T(""),
																								 _T(""),
																								 _T(""),
																								 0.0, 0.0, 0,0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0,
																								 nNormID,	// Set in status2; 080917 p�d
																								 _T(""),
																									 0, /* 0 = Ej satt */
																								 nMaxSortNumber+i+1 /* Set Sortorder */,
																								 0.0, 0.0, 0.0, L"", L"", L"", L"", L"", L"",
																								 recFstProp.getCoord());
				savePropertiesToDB(recElvProp);
			}	// for (UINT i = 0;i < m_vecSelectedProperties.size();i++)
			populateProperties();
		}	// if (m_vecSelectedProperties.size() > 0)
	}	// if (pObj != NULL)

	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
}

void CPropertiesFormView::getPropertiesInReport(vecTransaction_elv_properties &vec)
{
	vec.clear();
}

void CPropertiesFormView::getPropertiesInReport(libxl::Sheet* sheet,libxl::Format **fmt)
{
	CXTPReportRecords *pRecs = m_wndReportProperties.GetRecords();
	if (sheet != NULL && pRecs != NULL && fmt != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				// Status
				sheet->writeStr(i+6, 0,pRec->getColumnText(COLUMN_2),fmt[4]);
				// L�n
				sheet->writeStr(i+6, 1,pRec->getColumnText(COLUMN_3),fmt[4]);
				// Kommun
				sheet->writeStr(i+6, 2,pRec->getColumnText(COLUMN_4),fmt[4]);
				// Fastighetsnamn
				sheet->writeStr(i+6, 3,pRec->getColumnIconText(COLUMN_5),fmt[4]);
				// Fastighetsnummer
				sheet->writeStr(i+6, 4,pRec->getColumnText(COLUMN_6),fmt[1]);
				// Volym (m3sk)
				sheet->writeNum(i+6, 5,pRec->getColumnFloat(COLUMN_7),fmt[1]);
				// Areal (ha)
				sheet->writeNum(i+6, 6,pRec->getColumnFloat(COLUMN_8),fmt[1]);
				// Antal tr�d
				sheet->writeNum(i+6, 7,pRec->getColumnInt(COLUMN_9),fmt[4]);
				// Antal taxerade
				sheet->writeNum(i+6, 8,pRec->getColumnFloat(COLUMN_10),fmt[4]);
				// Volym gagnvirke (m3fub)
				sheet->writeNum(i+6, 9,pRec->getColumnFloat(COLUMN_11),fmt[1]);
				// Virkesv�rde (kr)
				sheet->writeNum(i+6, 10,pRec->getColumnFloat(COLUMN_12),fmt[4]);
				// Summa kostnad (kr)
				sheet->writeNum(i+6, 11,pRec->getColumnFloat(COLUMN_13),fmt[4]);
				// ROTNETTO (kr)
				sheet->writeNum(i+6, 12,pRec->getColumnFloat(COLUMN_14),fmt[4]);
				// Markv�rde (kr)
				sheet->writeNum(i+6, 13,pRec->getColumnFloat(COLUMN_15),fmt[4]);
				// F�rtidig avverkning (kr)
				sheet->writeNum(i+6, 14,pRec->getColumnFloat(COLUMN_16),fmt[4]);
				// Storm- och torkskador (kr)
				sheet->writeNum(i+6, 15,pRec->getColumnFloat(COLUMN_17),fmt[4]);
				// Kanttr�d (kr)
				sheet->writeNum(i+6, 16,pRec->getColumnFloat(COLUMN_18),fmt[4]);
				// Frivillig uppg�relse (kr)
				sheet->writeNum(i+6, 17,pRec->getColumnIconFloat(COLUMN_19),fmt[4]);
				// F�rdyrad avverkning (kr)
				sheet->writeNum(i+6, 18,pRec->getColumnIconFloat(COLUMN_20),fmt[4]);
				// Annan ers�ttning (kr)
				sheet->writeNum(i+6, 19,pRec->getColumnIconFloat(COLUMN_21),fmt[4]);
				// Gruppid
				
				// #HMS-91 Tillvaratagande l�gg till tillvaratagande info 20220825 J�
				sheet->writeStr(i+6, 20,pRec->getColumnIconText(COLUMN_23),fmt[4]);
				
				// Sorteringsordning
				// Skogsbr�nsle (ton TS)
				sheet->writeNum(i+6, 21,pRec->getColumnFloat(COLUMN_25),fmt[2]);
				// Skogsbr�nsle v�rde (kr)
				sheet->writeNum(i+6, 22,pRec->getColumnFloat(COLUMN_26),fmt[4]);
				// Skogsbr�nsle kostnad (kr)
				sheet->writeNum(i+6, 23,pRec->getColumnFloat(COLUMN_27),fmt[4]);
			}
		}
	}
}

BOOL CPropertiesFormView::removeProperty(short todo,bool bDoAll,bool *bToAll,bool *bYesToAll,bool *bNoToAll)
{
	BOOL bCanDelete = TRUE;
	BOOL bReturn = FALSE;
	CString sMsgRemove;
	vecFuncObjProp vecObjProp;
	vecFuncCruiseProp vecCruiseProp;
	vecTransaction_elv_cruise vecCruiseAllInObject;
	MATCH_OBJ_PROP_STRUCT rec1;
	MATCH_CRUISE_PROP_STRUCT rec2;
	int nDlgRet = 0;

	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow != NULL)
	{
		CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
		if (pRec != NULL && m_pDB != NULL)
		{
			// Remove only Property; 101115 p�d
			if (todo == 1)
			{
				sMsgRemove.Format(_T("%s%s\n\n%s"),m_sMsgPropertyName,pRec->getRecELVProp().getPropName(),m_sMsgRemoveProperty);
				// Ask user if he realy want's to remove this property; 080428 p�d
				/*if (::MessageBox(this->GetSafeHwnd(),(sMsgRemove),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDYES)
				{
					m_pDB->delProperty(pRec->getRecELVProp());
					populateProperties();
					setActivePropertyInReport();
					bReturn = TRUE;
				}*/

				//Lagt till dialog med checkbox f�r att valet skall anv�ndas f�r alla p�f�ljande fastighetsraderingar 20111128 J� feature #2456
								//(bool bToAll,bool bYesAll,bool bNoAll,CString strText)
				if(bDoAll && *bToAll)
				{
					if(*bYesToAll)
						bReturn =true;
					else
						bReturn =false;
				}
				else
				{
					CRemPropDlg *pDlg=new CRemPropDlg(NULL,bDoAll,*bToAll,sMsgRemove);
					nDlgRet=pDlg->DoModal();
					switch(nDlgRet)
					{
					case IDOK:
						if(pDlg->OnAll())
						{
							*bToAll=true;
							*bYesToAll=true;
							*bNoToAll=false;
						}
						bReturn =true;
						break;
					case IDCANCEL:
						if(pDlg->OnAll())
						{
							*bToAll=true;
							*bYesToAll=false;
							*bNoToAll=true;
						}
						bReturn =false;
						break;
					default:
							*bToAll=false;
							*bYesToAll=false;
							*bNoToAll=false;bReturn =false;
						break;

					}
					delete pDlg;
				}
				if(bReturn)
				{
					m_pDB->delProperty(pRec->getRecELVProp());
					populateProperties();
					setActivePropertyInReport();
				}

			}
			// Remove Property and stand(s); 101115 p�d
			// I.e. remove from "esti_trakt_table"; 101115 p�d
			else if (todo == 2)
			{
				m_pDB->matchPropertyInObjects(pRec->getRecELVProp().getPropObjectID_pk(),
					pRec->getRecELVProp().getPropID_pk(),
					vecObjProp,
					vecCruiseProp);
				m_pDB->getObjectCruises(pRec->getRecELVProp().getPropID_pk(),
					pRec->getRecELVProp().getPropObjectID_pk(),
					vecCruiseAllInObject);
				sMsgRemove.Format(_T("%s%s"),m_sMsgPropertyName,pRec->getRecELVProp().getPropName());
				
				if (vecObjProp.size() > 0)					// Property is included in other Objects than active; 101206 p�d
				{
					CMyMsgBoxDlg *pDlg = new CMyMsgBoxDlg();
					if (pDlg)
					{
						pDlg->setCaption(global_langMap[IDS_STRING22644]);

						pDlg->startHTML();
						//---------------------------------
						pDlg->addTable();
						pDlg->addTableCol();
						pDlg->addTableColText(m_sMsgPropertyName,10,false,10);
						pDlg->addTableColText(pRec->getRecELVProp().getPropName(),12,true);
						pDlg->endTableCol();
						pDlg->endTable();
						//---------------------------------
						pDlg->addTableHR(2,CMyMsgBoxDlg::HR_SOLID);
						//---------------------------------
						pDlg->addTable();
						pDlg->addTableText(m_sMsgRemoveProperty2,8);
						pDlg->addTableHR(1,CMyMsgBoxDlg::HR_SOLID);
						pDlg->addTableText(global_langMap[IDS_STRING22645]);
						if (vecCruiseProp.size() > 0)
							pDlg->addTableText(global_langMap[IDS_STRING22646]);
						pDlg->endTable();
						//---------------------------------
						pDlg->addTableHR(2,CMyMsgBoxDlg::HR_DOTTED);
						//---------------------------------
						for (UINT i1 = 0;i1 < vecObjProp.size();i1++)
						{
							rec1 = vecObjProp[i1];
							pDlg->addTable();
							pDlg->addTableCol();
							pDlg->addTableColText(global_langMap[IDS_STRING296]+L":",10,false,10);
							pDlg->addTableColText(rec1.sObjName,12,true);
							pDlg->endTableCol();
							pDlg->endTable();
							pDlg->addTable();
							for (UINT i2 = 0;i2 < vecCruiseProp.size();i2++)
							{
								rec2 = vecCruiseProp[i2];
								if (rec1.obj_id == rec2.nObjId)
								{
									pDlg->addTableCol();
									pDlg->addTableColText(global_langMap[IDS_STRING5420]+L":",10,false,10);
									pDlg->addTableColText(rec2.sStandName + L" ; " + rec2.sStandNumber,10,true);
									pDlg->endTableCol();
								}
							}
							pDlg->endTable();
							pDlg->addTableHR(1,CMyMsgBoxDlg::HR_DASHED);
						}
						if (pDlg->ShowDialog() == IDOK)
						{
							if (m_pDB->delProperty(pRec->getRecELVProp()))
							{
								// Remove Stand(s) not included in other Object(s)
								if (vecCruiseAllInObject.size() > 0 && vecCruiseProp.size() > 0)
								{
									for (UINT i2 = 0;i2 < vecCruiseAllInObject.size();i2++)
									{
										bCanDelete = TRUE;
										for (UINT i1 = 0;i1 < vecCruiseProp.size();i1++)
										{
											if (vecCruiseAllInObject[i2].getECruID_pk() == vecCruiseProp[i1].nStandId)
											{
												bCanDelete = FALSE;
												break;
											}	// if (vecCruisepAllInObject[i2].getECruID_pk() == vecCruiseProp[i1].nStandId)
										}	// for (UINT i2 = 0;i2 < vecCruisepAllInObject.size();i2++)
										// OK to delete Stand; 101203 p�d
										if (bCanDelete)
											m_pDB->delTrakt(vecCruiseAllInObject[i2].getECruID_pk());
									}	// for (UINT i1 = 0;i1 < vecCruiseProp.size();i1++)
								}	// if (vecCruisepAllInObject.size() > 0 && vecCruiseProp.size() > 0)
								// Remove Stand(s) only included in this Object
								else if (vecCruiseAllInObject.size() > 0 && vecCruiseProp.size() == 0)
								{
									for (UINT i2 = 0;i2 < vecCruiseAllInObject.size();i2++)
									{
										m_pDB->delTrakt(vecCruiseAllInObject[i2].getECruID_pk());
									}	// for (UINT i1 = 0;i1 < vecCruiseProp.size();i1++)
								}	// if (vecCruisepAllInObject.size() > 0 && vecCruiseProp.size() > 0)
							}	// if (m_pDB->delProperty(pRec->getRecELVProp()))
							bReturn = TRUE;
						}
						populateProperties();
						setActivePropertyInReport();

					}
					delete pDlg;
				}
					// Property only included in active Object; 101206 p�d
				else
				{
					sMsgRemove.Format(_T("%s%s\n\n%s"),m_sMsgPropertyName,pRec->getRecELVProp().getPropName(),m_sMsgRemoveProperty3);
					// Ask user if he realy want's to remove this property; 080428 p�d
					/*if (::MessageBox(this->GetSafeHwnd(),(sMsgRemove),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDYES)
					{
					if (m_pDB->delProperty(pRec->getRecELVProp()))
					{
					// Remove Stand(s)
					if (vecCruiseAllInObject.size() > 0)
					{
					for (UINT i = 0;i < vecCruiseAllInObject.size();i++)
					{
					m_pDB->delTrakt(vecCruiseAllInObject[i].getECruID_pk());
					}
					}
					}
					populateProperties();
					setActivePropertyInReport();
					bReturn = TRUE;
					}*/

					if(bDoAll && *bToAll)
					{
						if(*bYesToAll)
							bReturn =true;
						else
							bReturn =false;
					}
					else
					{
						CRemPropDlg *pDlg=new CRemPropDlg(NULL,bDoAll,*bToAll,sMsgRemove);
						nDlgRet=pDlg->DoModal();
						switch(nDlgRet)
						{
						case IDOK:
							if(pDlg->OnAll())
							{
								*bToAll=true;
								*bYesToAll=true;
								*bNoToAll=false;
							}
							bReturn =true;
							break;
						case IDCANCEL:
							if(pDlg->OnAll())
							{
								*bToAll=true;
								*bYesToAll=false;
								*bNoToAll=true;
							}
							bReturn =false;
							break;
						default:
							*bToAll=false;
							*bYesToAll=false;
							*bNoToAll=false;bReturn =false;
							break;

						}
						delete pDlg;
					}
					if(bReturn)
					{
						if (m_pDB->delProperty(pRec->getRecELVProp()))
						{
							// Remove Stand(s)
							if (vecCruiseAllInObject.size() > 0)
							{
								for (UINT i = 0;i < vecCruiseAllInObject.size();i++)
								{
									m_pDB->delTrakt(vecCruiseAllInObject[i].getECruID_pk());
								}
							}
						}
						populateProperties();
						setActivePropertyInReport();
						bReturn = TRUE;
					}
				}							
			}
		}
	}	// if (pRow != NULL)
	pRow = NULL;

	return bReturn;
}

// This method'll match the Focused properties stands; 080520 p�d
BOOL CPropertiesFormView::matchStandsForActiveProperty(bool use_dlg)
{

	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();

	BOOL bReturn = TRUE;
	int nOriginSizeOfLog = 0;
	CStringArray sarrLog;
	CString sMsg;
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	CTransaction_elv_object *pObj = getActiveObject();
	sarrLog.RemoveAll();
	if (pRow != NULL && pObj != NULL)
	{
		pRec = (CELVPropertyReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			// Add header information to Logbook; 080508 p�d
			sarrLog.Add((m_sLoggHeader));
			sarrLog.Add(_T(""));
			sarrLog.Add((formatData(_T("%s %s"),m_sLoggCreated,getDBDateTime())));
			sarrLog.Add(_T(""));
			sarrLog.Add((formatData(_T("%s %s"),m_sLoggObject,pObj->getObjectName())));
			sarrLog.Add((formatData(_T("%s %s"),m_sLoggProperty,pRec->getRecELVProp().getPropName())));
			sarrLog.Add(_T(""));
			nOriginSizeOfLog = sarrLog.GetCount();
		
			if (calculateStandsForProperty(pRec->getRecELVProp(),use_dlg /* TRUE = Use dialog */,sarrLog,1))
			{
				// Check if there's been any additions to the errorLog except header. 
				// If so, ask user if he like to see it; 080604 p�d
				if (nOriginSizeOfLog < sarrLog.GetCount())
				{
					if (::MessageBox(this->GetSafeHwnd(),(m_sLoggMsg1),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDYES)
					{
						sMsg.Empty();
						for (int i = 0;i < sarrLog.GetCount();i++)
							sMsg += sarrLog.GetAt(i) + _T("\r\n");

						CLoggMessageDlg *dlg = new CLoggMessageDlg();
						if (dlg != NULL)
						{
							dlg->setMessage(sMsg);
							dlg->setObjID(pObj->getObjID_pk());
							dlg->setupLanguage(m_sLogCap,m_sCancel,m_sPrintOut,m_sSaveToFile,
								formatData(_T("%s%s"),pObj->getObjectName(),LAND_VALUE_ROTPOST_LOG_FILE_NAME));
							dlg->DoModal();
					
							delete dlg;
						}	// if (dlg != NULL)
					}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sLoggMsg1),_T(m_sMsgCap),MB_YESNO | MB_DEFBUTTON2 | MB_ICONASTERISK) == IDYES)
				}	// if (nOriginSizeOfLog < sarrLog.GetCount())
				
				// Update report data; 080505 p�d
				populateProperties();
				// Setup data in Cruising and Evaluated; 080507 p�d
				//setActivePropertyInReport();

			}	// if (calculateStandsForProperty(pRec->getRecELVProp(),TRUE /* Use dialog */,sarrLog))
			else
				bReturn = FALSE;
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)

	pRow = NULL;
	pRec = NULL;
	pObj = NULL;

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);

	return bReturn;
}

// This method'll match all stands for all properties in Object, also in elv_cruise_table; 080520 p�d, 090505 p�d
void CPropertiesFormView::matchStandsPerPropertyForObject(CProgressDlg &prog_dlg)
{
	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();

	CStringArray sarrLog;
	CString sMsg;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	BOOL bIsErrorLog = FALSE;
	sarrLog.RemoveAll();
	CTransaction_elv_object recObj;
	if (pRows != NULL && pObj != NULL)
	{
		if (prog_dlg.GetSafeHwnd())
		{
			prog_dlg.setProgressRange_props(pRows->GetCount());
			doEvents();
		}
		// Cache up-to-date object data
		m_pDB->getObject(pObj->getObjID_pk(),recObj);
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
				if (pRec != NULL)
				{
					// Only add header information on first 
					if (i == 0)
					{
						// Add header information to Logbook; 080508 p�d
						sarrLog.Add((m_sLoggHeader));
						sarrLog.Add(_T(""));
						sarrLog.Add((formatData(_T("%s %s"),m_sLoggCreated,getDBDateTime())));
						sarrLog.Add(_T(""));
						sarrLog.Add((formatData(_T("%s %s"),m_sLoggObject,pObj->getObjectName())));
						sarrLog.Add(_T(""));
					}

					if (prog_dlg.GetSafeHwnd())
					{
						prog_dlg.setPropertyName(L"");
						prog_dlg.setProgressPos_props(i+1);
						doEvents();
					}

					//if (pRec->getRecELVProp().getPropStatus() <= STATUS_ADVICED)
					if (canWeCalculateThisProp_cached(pRec->getRecELVProp().getPropStatus()))
					{
						if (prog_dlg.GetSafeHwnd())
						{
							prog_dlg.setPropertyName(pRec->getRecELVProp().getPropName());
							doEvents();
						}
						// Add inforamtion on Property
						sarrLog.Add(_T("=========================================================="));
						sarrLog.Add((formatData(_T("%s %s"),m_sLoggProperty,pRec->getRecELVProp().getPropName())));
						if (!calculateStandsForProperty(pRec->getRecELVProp(),FALSE /* Don't use dialog */,sarrLog,1,&recObj))
							bIsErrorLog = TRUE;
					}
					pRec = NULL;
				}	// if (pRec != NULL)
				pRow = NULL;
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
		// Update report data; 080505 p�d
		populateProperties();
		// Setup data in Cruising and Evaluated; 080507 p�d
		setActivePropertyInReport();

		if (bIsErrorLog)
		{
			if (sarrLog.GetCount() > 6)
			{

//				if (::MessageBox(this->GetSafeHwnd(),_T(m_sLoggMsg1),_T(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDYES)
//				{
					sMsg.Empty();
					for (int i = 0;i < sarrLog.GetCount();i++)
						sMsg += sarrLog.GetAt(i) + _T("\r\n");

					CLoggMessageDlg *dlg = new CLoggMessageDlg();
					if (dlg != NULL)
					{
						dlg->setMessage(sMsg);
						dlg->setObjID(pObj->getObjID_pk());
						dlg->setupLanguage(m_sLogCap,m_sCancel,m_sPrintOut,m_sSaveToFile,
							formatData(_T("%s%s"),pObj->getObjectName(),LAND_VALUE_MATCH_LOG_FILE_NAME));
						dlg->DoModal();
				
						delete dlg;
					}	// if (dlg != NULL)
//				}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sLoggMsg1),_T(m_sMsgCap),MB_YESNO | MB_DEFBUTTON2 | MB_ICONASTERISK) == IDYES)
			}	// if (sarrLog.GetCount() > 0)
		}	// if (bIsErrorLog)
	}	// if (pRows != NULL)
	pRows = NULL;
	pObj = NULL;
	sarrLog.RemoveAll();

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);
}

// This method'll match all stands to properties in an Object; 090505 p�d
void CPropertiesFormView::matchAllStandsPerPropertyForObject()
{
	CStringArray sarrLog;
	CString sMsg;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	BOOL bIsErrorLog = FALSE;
	sarrLog.RemoveAll();
	CTransaction_elv_object recObj;

	if (::MessageBox(this->GetSafeHwnd(),m_sMsgMatchAllStandsToProperties,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		return;

	if (pRows != NULL && pObj != NULL)
	{
		// Cache up-to-date object data
		m_pDB->getObject(pObj->getObjID_pk(),recObj);
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
				if (pRec != NULL)
				{
					// Only add header information on first 
					if (i == 0)
					{
						// Add header information to Logbook; 080508 p�d
						sarrLog.Add((m_sLoggHeader));
						sarrLog.Add(_T(""));
						sarrLog.Add((formatData(_T("%s %s"),m_sLoggCreated,getDBDateTime())));
						sarrLog.Add(_T(""));
						sarrLog.Add((formatData(_T("%s %s"),m_sLoggObject,pObj->getObjectName())));
						sarrLog.Add(_T(""));
					}

					//if (pRec->getRecELVProp().getPropStatus() <= STATUS_ADVICED)
					if (canWeCalculateThisProp_cached(pRec->getRecELVProp().getPropStatus()))
					{
						// Add inforamtion on Property
						sarrLog.Add(_T("=========================================================="));
						sarrLog.Add((formatData(_T("%s %s"),m_sLoggProperty,pRec->getRecELVProp().getPropName())));
						if (!calculateStandsForProperty(pRec->getRecELVProp(),FALSE /* Don't use dialog */,sarrLog,2,&recObj))
							bIsErrorLog = TRUE;
					}
					pRec = NULL;
				}	// if (pRec != NULL)
				pRow = NULL;
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)

		// Update report data; 080505 p�d
		populateProperties();
		// Setup data in Cruising and Evaluated; 080507 p�d
		setActivePropertyInReport();

		if (bIsErrorLog)
		{
			if (sarrLog.GetCount() > 6)
			{
//				if (::MessageBox(this->GetSafeHwnd(),_T(m_sLoggMsg1),_T(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDYES)
//				{
					sMsg.Empty();
					for (int i = 0;i < sarrLog.GetCount();i++)
						sMsg += sarrLog.GetAt(i) + _T("\r\n");

					CLoggMessageDlg *dlg = new CLoggMessageDlg();
					if (dlg != NULL)
					{
						dlg->setMessage(sMsg);
						dlg->setObjID(pObj->getObjID_pk());
						dlg->setupLanguage(m_sLogCap,m_sCancel,m_sPrintOut,m_sSaveToFile,
							formatData(_T("%s%s"),pObj->getObjectName(),LAND_VALUE_MATCH_LOG_FILE_NAME));
						dlg->DoModal();
				
						delete dlg;
					}	// if (dlg != NULL)
//				}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sLoggMsg1),_T(m_sMsgCap),MB_YESNO | MB_DEFBUTTON2 | MB_ICONASTERISK) == IDYES)
			}	// if (sarrLog.GetCount() > 0)
		}	// if (bIsErrorLog)
	}	// if (pRows != NULL)
	pRows = NULL;
	pObj = NULL;
	sarrLog.RemoveAll();
}

void CPropertiesFormView::addNewVStandToActiveProperty(void)
{
	// Ask user if he realy want to add a new VStand; 080513 p�d
	// Commented out 2009-04-02 P�D, think we don't need to ask user here; 090402 p�d
	//if (::MessageBox(this->GetSafeHwnd(),(m_sMsgAddEvaluestand),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDNO)
	//	return;

	//HMS-16 Testar ta bort
	/*
	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();*/

	CEvaluatedData *pEval = getEvaluatedView();
	if (pEval != NULL)
	{
		pEval->addNewRecordToReport();
	}


	//HMS-16 Testar ta bort
	/*
	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);*/
}

void CPropertiesFormView::addNewVStandToActivePropertyWithDialog(void)
{
	// Ask user if he realy want to add a new VStand; 080513 p�d
	// Commented out 2009-04-02 P�D, think we don't need to ask user here; 090402 p�d
	//if (::MessageBox(this->GetSafeHwnd(),(m_sMsgAddEvaluestand),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDNO)
	//	return;

	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();

	CEvaluatedData *pEval = getEvaluatedView();
	if (pEval != NULL)
	{
		pEval->addNewRecordToReportWithDialog();
	}

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);
}

void CPropertiesFormView::saveAndCalculateVStandInActiveProperty(bool do_calc)
{
	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();


	CEvaluatedData *pEval = getEvaluatedView();
	if (pEval != NULL)
	{
		// Save evaluation data; 080514 p�d
		//pEval->saveEvalData(); Tagit bort, g�r �nd� ingenting i funktionen 20200428 J�

		// Do calculation of Evaluation-stand(s); 080514 p�d
		if (do_calc) 
			calculateEvaluationStand();
	}

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);
	
}

//PH #5234
int CPropertiesFormView::getSelectedPropertyId() {

	CELVPropertyReportRec* pRec = NULL;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CELVPropertyReportRec*)pRow->GetRecord();
		if(pRec!=NULL)
			return pRec->getPropId();
	}

	return -1;
}

//PH #5234
void CPropertiesFormView::setSelectedPropertyId(int id) {
	
	CXTPReportRows* rows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow;
	CELVPropertyReportRec* tempRec = NULL;

	if(id>0) {
		for(int i=0;i<rows->GetCount();i++) {
			pRow=rows->GetAt(i);
			tempRec=(CELVPropertyReportRec*)pRow->GetRecord();
			if(tempRec->getPropId()==id)
				break;
		}
		if(pRow!=NULL)
			m_wndReportProperties.SetFocusedRow(pRow);
	}

	setActivePropertyInReport();
}

void CPropertiesFormView::saveAndCalculateVStandInActiveProperty(CString best_num,CString best_name,CString si,CString tgl,double areal,double volume,int age,double corr_fac,BOOL do_update,int sida,BOOL bTillf)
{
	double fVolPerHA = 0.0;
	if (areal > 0.0 && volume > 0.0)
		fVolPerHA = volume/areal;
	else
		fVolPerHA = volume;


	CEvaluatedData *pEval = getEvaluatedView();
	if (pEval != NULL)
	{
		pEval->setDataInReport(best_num,best_name,si,tgl,areal,volume,age,corr_fac,do_update,fVolPerHA,sida,bTillf);

		// Save evaluation data; 080514 p�d
//		pEval->saveEvalData();	Don't save volume in this method
		pEval->saveEvalData(volume);
		// Do calculation of Evaluation-stand(s); 080514 p�d
		calculateEvaluationStand();
	}
}

void CPropertiesFormView::saveAndCalculateVStands(CProgressDlg &prog_dlg)
{
	CEvaluatedData *pEval = getEvaluatedView();
	if (pEval != NULL)
	{
		// Save evaluation data; 080514 p�d
		//pEval->saveEvalData(); Tagit bort, g�r �nd� ingenting i funktionen 20200428 J�
		// Do calculation of Evaluation-stand(s); 080514 p�d
		calculateAllEvaluationStands(prog_dlg);
	}
}

void CPropertiesFormView::removeCalculateVStandFromActiveProperty()
{
	// Ask user if he realy want to remove VStand; 080514 p�d
	if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveEvaluestand),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDNO)
		return;

	// Save selected property id, PH #5234
	int propertyID = getSelectedPropertyId();

	CTransaction_elv_object *pObj = getActiveObject();
	CEvaluatedData *pEval = getEvaluatedView();
	

	if (pEval != NULL && pObj != NULL)
	{
		CTransaction_elv_properties savedProp = m_recActiveProperty;

		pEval->removeSelectedEvaluation(m_recActiveProperty.getPropStatus(),pObj->getObjUseNormID());

		// Do calculation of Evaluation-stand(s); 080514 p�d
		calculateEvaluationStand();
		setActivePropertyInReport();

		CMDI1950ForrestNormFrame *pFrame = (CMDI1950ForrestNormFrame *)getFormViewParentByID(IDD_FORMVIEW);
		if (pFrame != NULL)
		{
			pFrame->setActiveProperty(savedProp,TRUE);
		}

	}

	// Restore selected property, PH #5234
	setSelectedPropertyId(propertyID);
}

void CPropertiesFormView::removeAllCalculateVStandsFromActiveProperty()
{
	vecTransaction_eval_evaluation vecELVObjectEvals;
	CTransaction_eval_evaluation recEval;
	vecTransaction_elv_cruise vecELVObjectCruise;
	CTransaction_elv_cruise recCruise;
	CTransaction_elv_properties recElvProperty;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	CEvaluatedData *pEval = getEvaluatedView();
	CTransaction_elv_object *pObj = getActiveObject();
	if (pRow != NULL && pObj != NULL)
	{
			CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRow->GetRecord();
			if (pRec != NULL && m_pDB != NULL)
			{
				recElvProperty = pRec->getRecELVProp();
//				if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
				if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()))
				{
					m_pDB->getObjectCruises(recElvProperty.getPropID_pk(),recElvProperty.getPropObjectID_pk(),vecELVObjectCruise);
					m_pDB->getObjectEvaluation(recElvProperty.getPropID_pk(),recElvProperty.getPropObjectID_pk(),vecELVObjectEvals);

					// Match each Evaluationstand to a Cruise stand. If not found, delete it; 090401 p�d
					if (vecELVObjectCruise.size() > 0 && vecELVObjectEvals.size() > 0)
					{
						for (UINT i1 = 0;i1 < vecELVObjectCruise.size();i1++)
						{
							recCruise = vecELVObjectCruise[i1];
							for (UINT i2 = 0;i2 < vecELVObjectEvals.size();i2++)
							{
								recEval = vecELVObjectEvals[i2];
								// Is this an evaluation stand based on Cruisedata; 090401 p�d
								if (recEval.getEValType() == EVAL_TYPE_FROM_TAXERING)
								{
									// Removes ALL evaluationstands from this object; 081022 p�d
									// Changed ti: removes evaluationstand from this object; 090401 p�d
									
									m_pDB->delObjectEvaluation(recEval.getEValID_pk(),recElvProperty.getPropObjectID_pk(),recElvProperty.getPropID_pk());
								}	// if (recEval.getEValType() == EVAL_TYPE_2)
							}	// for (UINT i1 = 0;i1 < vecELVObjectEvals.size();i1++)
						
						}	// for (UINT i1 = 0;i1 < vecELVObjectCruise.size();i1++)
						if (pEval != NULL)
							pEval->populateReport(-1,-1,recElvProperty.getPropStatus(),pObj->getObjUseNormID());
					}	// if (vecELVObjectCruise.size() > 0 && vecELVObjectEvals.size() > 0)
				}	// if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
			}	// if (pRec != NULL && m_pDB != NULL)
	}	// if (pRow != NULL)
}

// Change method 2009-04-01 P�D
// Check to see if Evaluated stand doesn't equal a cruised stand, check name etc.
// If not, don't remove that stand; 090401 p�d
void CPropertiesFormView::removeAllCalculateVStands(CProgressDlg &prog_dlg)
{
	vecTransaction_eval_evaluation vecELVObjectEvals;
	CTransaction_eval_evaluation recEval;
	vecTransaction_elv_cruise vecELVObjectCruise;
	CTransaction_elv_cruise recCruise;
	CTransaction_elv_properties recElvProperty;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CEvaluatedData *pEval = getEvaluatedView();
	CTransaction_elv_object *pObj = getActiveObject();
	if (pRows != NULL && pObj != NULL)
	{
		if (prog_dlg.GetSafeHwnd()) prog_dlg.setProgressRange_props(pRows->GetCount());
		for (int i = 0;i < pRows->GetCount();i++)
		{
			CELVPropertyReportRec *pRec = (CELVPropertyReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL && m_pDB != NULL)
			{
				recElvProperty = pRec->getRecELVProp();
				//if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
				if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()))
				{
					if (prog_dlg.GetSafeHwnd()) 
					{
						prog_dlg.setPropertyName(recElvProperty.getPropName());
						prog_dlg.setProgressPos_props(i+1);
					}

					m_pDB->getObjectCruises(recElvProperty.getPropID_pk(),recElvProperty.getPropObjectID_pk(),vecELVObjectCruise);
					m_pDB->getObjectEvaluation(recElvProperty.getPropID_pk(),recElvProperty.getPropObjectID_pk(),vecELVObjectEvals);

					// Match each Evaluationstand to a Cruise stand. If not found, delete it; 090401 p�d
					if (vecELVObjectCruise.size() > 0 && vecELVObjectEvals.size() > 0)
					{
						if (prog_dlg.GetSafeHwnd()) prog_dlg.setProgressRange_stands(vecELVObjectEvals.size());

						for (UINT i1 = 0;i1 < vecELVObjectCruise.size();i1++)
						{
							recCruise = vecELVObjectCruise[i1];
							for (UINT i2 = 0;i2 < vecELVObjectEvals.size();i2++)
							{
								recEval = vecELVObjectEvals[i2];
								if (prog_dlg.GetSafeHwnd()) 
								{
									prog_dlg.setStandName(recEval.getEValName());
									prog_dlg.setProgressPos_stands(i2+1);
									doEvents();
								}
								// Is this an evaluation stand based on Cruisedata; 090401 p�d
								if (recEval.getEValType() == EVAL_TYPE_FROM_TAXERING)
								{
									// Removes ALL evaluationstands from this object; 081022 p�d
									// Changed ti: removes evaluationstand from this object; 090401 p�d
									m_pDB->delObjectEvaluation(recEval.getEValID_pk(),recEval.getEValObjID_pk(),recEval.getEValPropID_pk());
									doEvents();
								}	// if (recEval.getEValType() == EVAL_TYPE_2)
							}	// for (UINT i1 = 0;i1 < vecELVObjectEvals.size();i1++)
						
						}	// for (UINT i1 = 0;i1 < vecELVObjectCruise.size();i1++)
						if (pEval != NULL)
							pEval->populateReport(-1,-1,recElvProperty.getPropStatus(),pObj->getObjUseNormID());

						if (prog_dlg.GetSafeHwnd()) 
						{
							prog_dlg.setPropertyName(L"");
							prog_dlg.setProgressPos_props(0);
							prog_dlg.setStandName(L"");
							prog_dlg.setProgressPos_stands(0);
						}
					}	// if (vecELVObjectCruise.size() > 0 && vecELVObjectEvals.size() > 0)
					//---------------------------------------------------------------
					// No Evaluated stands; 100707 p�d
					else if (vecELVObjectEvals.size() > 0)
					{
						// Set Property "Mark- och Merv�rde = 0.0"; 100707 p�d
						m_pDB->delObjectEvaluation_cruise(recElvProperty.getPropObjectID_pk(),recElvProperty.getPropID_pk());
						doEvents();
					}
				}	// if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
			}	// if (pRec != NULL && m_pDB != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

void CPropertiesFormView::groupPropertiesOnLandOwners(void)
{
	int nGroupIDNumber = 0;
	CString sSelectStr,sStr,sGrpStr;
	vecInt vecLandOwnerIDs;
	vecInt vecPropsPerLandOwnerIDs;
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	typedef std::map<CString, int> MapType;
	MapType mapGroupIDs;

	if (pRows != NULL && m_pDB != NULL)
	{
		nGroupIDNumber = 999;	// OBS! "Mark�garnummer startar p� 1000"; 080526 p�d

		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
					m_pDB->getObjProp_landowners_per_prop(pRec->getRecELVProp().getPropObjectID_pk(),
																								pRec->getRecELVProp().getPropID_pk(),
																								vecLandOwnerIDs);
					if (vecLandOwnerIDs.size() > 0)
					{
						sSelectStr = _T("(");
						sGrpStr.Empty();
						for (UINT i = 0;i < vecLandOwnerIDs.size();i++)
						{
							if (i < vecLandOwnerIDs.size()-1)
								sStr.Format(_T("a.contact_id=%d or "),vecLandOwnerIDs[i]);
							if (i == vecLandOwnerIDs.size()-1)
								sStr.Format(_T(" a.contact_id=%d"),vecLandOwnerIDs[i]);

							sGrpStr += formatData(_T("%d;"),vecLandOwnerIDs[i]);

							sSelectStr += sStr;
						}	// for (UINT i = 0;i < vecLandOwnersIDs.size();i++)
						sSelectStr += _T(")");

						//  get information on Landowner ids; 080516 p�d
						m_pDB->getObjProp_match_landowner_ids_per_properties(sSelectStr,vecLandOwnerIDs.size(),vecPropsPerLandOwnerIDs);
						if (vecPropsPerLandOwnerIDs.size() > 0)
						{
							for (UINT ii = 0;ii < vecPropsPerLandOwnerIDs.size();ii++)
							{
								if (pRec->getRecELVProp().getPropID_pk() == vecPropsPerLandOwnerIDs[ii])
								{
									MapType::iterator iter=mapGroupIDs.find(sGrpStr);
									int groupIdNumber=0;
									if(iter != mapGroupIDs.end())
									{ //This combination of Landowner id:s, already exists, use the same group id.
										groupIdNumber=iter->second;
									}
									else
									{ //The Land owner id:s doesn't exist, create a new groupIdNumber.
										mapGroupIDs.insert(std::map<CString,int>::value_type(sGrpStr,++nGroupIDNumber));
										groupIdNumber=nGroupIDNumber;
									}

									m_pDB->updPropertyGroupIdentity(pRec->getRecELVProp().getPropID_pk(),
																								  pRec->getRecELVProp().getPropObjectID_pk(),
																									formatData(_T("%d"),groupIdNumber));
//																									formatData(_T("%d%s"),pRec->getRecELVProp().getPropObjectID_pk(),sGrpStr));

								}	// if (pRec->getRecELVProp().getPropID_pk() == vecPropsPerLandOwnerIDs[ii])
							}	// for (UINT ii = 0;ii < vecPropsPerLandOwnerIDs.size();ii++)
						}	// if (vecPropsPerLandOwnerIDs.size() > 0)
					}	// if (vecLandOwnersIDs.size() > 0)
					else
					{
						m_pDB->updPropertyGroupIdentity(pRec->getRecELVProp().getPropID_pk(),
																					  pRec->getRecELVProp().getPropObjectID_pk(),
																						L"");
					}
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
		populateProperties();
		mapGroupIDs.clear();
	}	// if (pRows != NULL && m_pDB != NULL)
}


void CPropertiesFormView::groupPropertiesOnPropertyNumbers(void)
{
	int nPropNumber = 0;
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		nPropNumber = -1;	
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
						m_pDB->updPropertyGroupIdentity(pRec->getRecELVProp().getPropID_pk(),
																					  pRec->getRecELVProp().getPropObjectID_pk(),
																						pRec->getRecELVProp().getPropNumber());
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
		populateProperties();
	}	// if (pRows != NULL && m_pDB != NULL)
}


// Save groupings set for properties in Object; 080516 p�d
void CPropertiesFormView::savePropertyGroupings(bool populate)
{
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
					m_pDB->updPropertyGroupIdentity(pRec->getRecELVProp().getPropID_pk(),
																					pRec->getRecELVProp().getPropObjectID_pk(),
																					pRec->getColumnIconText(COLUMN_22));
//																					pRec->getColumnText(COLUMN_22));
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
		if (populate) populateProperties();
	}	// if (pRows != NULL && m_pDB != NULL)
}

// Set an auto sortorder
void CPropertiesFormView::setPropertySortOrder(bool populate)
{
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
					m_pDB->updPropertySortOrder(pRec->getRecELVProp().getPropID_pk(),
																			pRec->getRecELVProp().getPropObjectID_pk(),
																			i+1);
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
		if (populate) populateProperties();
	}	// if (pRows != NULL && m_pDB != NULL)
}
/*
void CPropertiesFormView::resetPropertySortOrder(bool populate)
{
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
					m_pDB->updPropertySortOrder(pRec->getRecELVProp().getPropID_pk(),
																			pRec->getRecELVProp().getPropObjectID_pk(),
																			pRec->getColumnInt(COLUMN_24));
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
		if (populate) populateProperties();
	}	// if (pRows != NULL && m_pDB != NULL)
}
*/
// Save groupings set for properties in Object; 080516 p�d
void CPropertiesFormView::savePropertyTypeOfAction(void)
{
	int nTypeOfAction = -1;
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
					nTypeOfAction = pRec->getColumnToogle(COLUMN_23);

					m_pDB->updPropertyTypeOfAction(pRec->getRecELVProp().getPropID_pk(),
																					pRec->getRecELVProp().getPropObjectID_pk(),
																					nTypeOfAction);
//																					pRec->getColumnText(COLUMN_22));
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
//		populateProperties();
	}	// if (pRows != NULL && m_pDB != NULL)
}

// Remove groupings set for properties in Object; 081110 p�d
void CPropertiesFormView::removePropertyGroupings(void)
{
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReportProperties.GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRow = pRows->GetAt(i)) != NULL)
			{			
				if ((pRec = (CELVPropertyReportRec*)pRow->GetRecord()) != NULL)
				{
					m_pDB->updPropertyGroupIdentity(pRec->getRecELVProp().getPropID_pk(),
																					pRec->getRecELVProp().getPropObjectID_pk(),
																					_T(""));	// Set column to empty; 081110 p�d
				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows.GetCount();i++)
		// Update report data; 080516 p�d
		populateProperties();
	}	// if (pRows != NULL && m_pDB != NULL)
}


void CPropertiesFormView::removeStandInProperty(void)
{
	int pId = this->getFocusedPropertyID();
	CStringArray sarrLog;
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	CTransaction_elv_object *pObj = getActiveObject();
	int nObjID = -1;
	int nPropID = -1;
	// Ask user if he realy want to remove VStand; 080514 p�d
	//if (::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveCruise),(m_sMsgCap),MB_YESNO | MB_ICONASTERISK | MB_DEFBUTTON2) == IDNO)
	//	return;

	if (pRow != NULL && pObj != NULL)
	{
		pRec = (CELVPropertyReportRec *)pRow->GetRecord();
		if (pRec != NULL)
		{
			nObjID = pRec->getRecELVProp().getPropObjectID_pk();
			nPropID = pRec->getRecELVProp().getPropID_pk();
			CCruisingData *pCruise = getCruisingView();
			if (pCruise != NULL)
			{
				if (pCruise->removeSelectedCruise(pRec->getRecELVProp().getPropStatus(),pObj->getObjUseNormID()))
				{
					calculateStandsForProperty(pRec->getRecELVProp(),FALSE /* TRUE = Use dialog */,sarrLog,1);
					// Update report data; 080516 p�d
					populateProperties();
					// Check if there's any data in 
					if (m_vecELV_properties.size() > 0)
					{
						if (pCruise->getNumOfCruises() == 0)
						{
							if (m_pDB != NULL)
							{
								if (m_pDB->resetProperty(nPropID,nObjID))
								{
									// Add information on "Frivillig uppg�relse"; 100224 p�d
									caluclateVoluntaryDeal(pRec->getRecELVProp());

									// Add information on "F�rdyrad avverkning"; 080515 p�d
									calculateHigherCosts(pRec->getRecELVProp());

									populateProperties();
								}	// if (m_pDB->resetProperty(nPropID,nObjID))
							}	// if (m_pDB != NULL)
						}	// if (pCruise->getNumOfCruises() == 0)
					 }	// if (m_vecELV_properties.size() > 0)
				}	// if (pCruise->removeSelectedCruise(pRec->getRecELVProp().getPropStatus(),pObj->getObjUseNormID()))
				pCruise = NULL;
			}	// if (pCruise != NULL)
			pRec = NULL;
		}	// if (pRec != NULL)
		pRow = NULL;
	}	// if (pRow != NULL && pObj != NULL)

	// Setup data in Cruising and Evaluated; 080515 p�d
	//setActivePropertyInReport();

	this->setSelectedPropertyId(pId);
}

CTransaction_elv_properties* CPropertiesFormView::getActivePropertyRecord(void)
{
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CELVPropertyReportRec *)pRow->GetRecord();
		if (pRec != NULL)
			return &(pRec->getRecELVProp());
	}

	return NULL;
}

// This method'll recalulate all "Frivillig ers�ttning", on active object; 080519 p�d
void CPropertiesFormView::recalulateAllVouluntaryDeals(void)
{
	int nNumOfEvals;

	CTransaction_elv_properties recElvProperty;
	if (m_vecELV_properties.size() > 0)
	{
		for (UINT i = 0;i < m_vecELV_properties.size();i++)
		{
			recElvProperty = m_vecELV_properties[i];

			//if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
			m_pDB->getNumOfEvaluations(recElvProperty.getPropObjectID_pk(), recElvProperty.getPropID_pk(), &nNumOfEvals);
			if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()) && (recElvProperty.getPropNumOfStands() || nNumOfEvals))
				caluclateVoluntaryDeal(recElvProperty);
		}	// for (UINT i = 0;i < m_vecELV_properties.szie();i++)
	}	// if (m_vecELV_properties.size() > 0)
	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	setActivePropertyInReport();
}

// This method'll recalulate all "F�rdyrad avverkning", on active object; 080519 p�d
void CPropertiesFormView::recalulateAllHigherCosts(void)
{
	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();

	int nNumOfEvals;

	CTransaction_elv_properties recElvProperty;
	if (m_vecELV_properties.size() > 0)
	{
		for (UINT i = 0;i < m_vecELV_properties.size();i++)
		{
			recElvProperty = m_vecELV_properties[i];
//			if (recElvProperty.getPropStatus() <= STATUS_ADVICED)
			m_pDB->getNumOfEvaluations(recElvProperty.getPropObjectID_pk(), recElvProperty.getPropID_pk(), &nNumOfEvals);
			if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()) && (recElvProperty.getPropNumOfStands() || nNumOfEvals))
				calculateHigherCosts(recElvProperty);
		}	// for (UINT i = 0;i < m_vecELV_properties.szie();i++)
	}	// if (m_vecELV_properties.size() > 0)
	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	//setActivePropertyInReport();

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);
}

// This method'll recalulate all "Annan ers�ttning", on active object; 080519 p�d
void CPropertiesFormView::recalulateAllOtherCosts(void)
{

	//Save selected property id, PH #5234
	int propertyId = getSelectedPropertyId();

	BOOL bIsOtherCosts = FALSE;
	double fValue = 0.0,fSumOtherComp = 0.0;
	CString S;
	CTransaction_elv_object recElvObject;
	CTransaction_elv_properties recElvProperty;
	CTransaction_elv_properties_other_comp recElvOtherComp;
	int nObjID = getActiveObject()->getObjID_pk();
	if (m_pDB != NULL)
	{
		m_pDB->getObject(nObjID,recElvObject);
		m_pDB->getPropertyOtherComp(nObjID,m_vecOtherComp);
	}

	if (m_vecELV_properties.size() > 0 && m_vecOtherComp.size() > 0)
	{
		for (UINT i = 0;i < m_vecELV_properties.size();i++)
		{
			fSumOtherComp = 0.0;
			fValue = 0.0;
			bIsOtherCosts = FALSE;

			recElvProperty = m_vecELV_properties[i];
			if (canWeCalculateThisProp_cached(recElvProperty.getPropStatus()))
			{
				for (UINT j = 0;j < m_vecOtherComp.size();j++)
				{
					recElvOtherComp = m_vecOtherComp[j];
					if (recElvOtherComp.getPropOtherCompObjectID_pk() == recElvProperty.getPropObjectID_pk() &&
						recElvOtherComp.getPropOtherCompPropID_pk() == recElvProperty.getPropID_pk())
					{
						if (recElvOtherComp.getPropOtherCompTypeOf() == 0)
						{
							fValue = recElvOtherComp.getPropOtherCompValue_calc();
							bIsOtherCosts = TRUE;
						}
						else if (recElvOtherComp.getPropOtherCompTypeOf() == 1)
						{
							fValue = recElvOtherComp.getPropOtherCompValue_entered() + recElvOtherComp.getPropOtherCompValue_entered()*(recElvObject.getObjVAT()/100.0);
							m_vecOtherComp[j].setPropOtherCompValue_calc(fValue);
							if (m_pDB != NULL)
								m_pDB->updPropertyOtherComp(m_vecOtherComp[j]);
							bIsOtherCosts = TRUE;

						}
						else if (recElvOtherComp.getPropOtherCompTypeOf() == 2)
						{
							fValue = recElvOtherComp.getPropOtherCompValue_entered()*(recElvObject.getObjPercent()/100.0);
							m_vecOtherComp[j].setPropOtherCompValue_calc(fValue);
							if (m_pDB != NULL)
								m_pDB->updPropertyOtherComp(m_vecOtherComp[j]);
							bIsOtherCosts = TRUE;

						}
						else
						{
							bIsOtherCosts = FALSE;
							fSumOtherComp = 0.0;
							m_vecOtherComp[j].setPropOtherCompValue_calc(0.0);
							if (m_pDB != NULL)
								m_pDB->updPropertyOtherComp(m_vecOtherComp[j]);
						}
						fSumOtherComp += fValue;
					}
				}	// for (UINT j = 0;j < m_vecOtherComp.size();j++)
			
				if (m_pDB != NULL)
				{
					if (!bIsOtherCosts)
						fSumOtherComp = 0.0;

					m_pDB->updObjPropTraktSUM_5(recElvProperty.getPropID_pk(),
																			recElvProperty.getPropObjectID_pk(),
																			fSumOtherComp);
				}
			}
		}	// for (UINT i = 0;i < m_vecELV_properties.szie();i++)
	}	// if (m_vecELV_properties.size() > 0)
	// Update report data; 080515 p�d
	populateProperties();
	// Setup data in Cruising and Evaluated; 080515 p�d
	//setActivePropertyInReport();

	//Restore selection, PH #5234
	setSelectedPropertyId(propertyId);
}

// Calculate "Storm- och Torkskador", for manually entered stands; 090907 p�d
void CPropertiesFormView::calculateStormDry(void)
{
	CString sName,sDoneBy,sNotes;
	CTransaction_elv_object recObject;
	vecTransaction_elv_cruise vecCruise;
	CTransaction_elv_cruise recCruise;

	vecObjectTemplate_p30_table vecP30_table;					// "1950:�rd Skogsnorm"
	vecObjectTemplate_p30_nn_table vecP30_nn_table;		// "Skogsnormen ver1.1"

	int nPinePercent,nSprucePercent,nBirchPercent;
	double fSpruceMix,fSTValue,fSTVolume;

	CString sArea,S;
	int nAreaIndex = 0;

	double fPresentWidth = 0.0;
	double fNewWidth = 0.0;

	nPinePercent = nSprucePercent = nBirchPercent = 0;
	fSpruceMix = fSTValue = fSTVolume = 0.0;


	double fStormTorkInfo_SpruceMix=0.0;
	double fStormTorkInfo_SumM3Sk_inside=0.0;
	double fStormTorkInfo_AvgPriceFactor=0.0;
	double fStormTorkInfo_TakeCareOfPerc=0.0;
	double fStormTorkInfo_PineP30Price=0.0;
	double fStormTorkInfo_SpruceP30Price=0.0;
	double fStormTorkInfo_BirchP30Price=0.0;
	double fStormTorkInfo_CompensationLevel=0.0;
	double fStormTorkInfo_Andel_Pine=0.0;
	double fStormTorkInfo_Andel_Spruce=0.0;
	double fStormTorkInfo_Andel_Birch=0.0;
	double fStormTorkInfo_WideningFactor=0.0;

	CCruisingData *pCruise = getCruisingView();
	if (pCruise != NULL)
	{
		pCruise->getCruises(vecCruise);
	}	// if (pCruise != NULL)

	// Collect fresh info. from DB; 090908 p�d
	if (m_pDB) 
	{
		m_pDB->getObject(m_recActiveProperty.getPropObjectID_pk(),recObject);
	}
	// Load P30 table for Object; 090909 p�d
	TemplateParser parser;
	if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
	{
		if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		{
			parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
		}
	}
	else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
	{
		if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		{
			parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
			setP30NN_SIMax(vecP30_nn_table);
		}
	}
	else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
	{
		if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		{
			parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
			setP30NN_SIMax(vecP30_nn_table);
		}
	}
	

	// Check if this is a "Breddning", if so set widths; 090909 p�d
	if (_tstoi(recObject.getObjTypeOfInfring()) == 1 /* "Breddning" */)
	{
		fPresentWidth = recObject.getObjPresentWidth1();
	}

	if (vecCruise.size() > 0)
	{
		for (UINT i = 0;i < vecCruise.size();i++)
		{
			recCruise = vecCruise[i];
			if (recCruise.getECruType() == CRUISE_TYPE_2)	// Manually entered; 090908 p�d
			{
				//-------------------------------------------------------------------------
				// Calculate "Storm- och Torkskador", acordingly to the "1950:�rs skogsnorm"; 090908 p�d
				//-------------------------------------------------------------------------
				if (recObject.getObjUseNormID() == ID_1950_FORREST_NORM)
				{
					parseTGL(recCruise.getECruTGL(),&nPinePercent,&nSprucePercent,&nBirchPercent);
					calculateStormDry_1950(nPinePercent,nSprucePercent,nBirchPercent,
																 vecP30_table,
																 recCruise.getECruM3Sk(),
																 recCruise.getECruSI(),
																 recObject.getObjGrowthArea(),
																 recObject.getObjPercent(),
																 &fSpruceMix,&fSTValue,&fSTVolume,
						&fStormTorkInfo_SpruceMix,
						&fStormTorkInfo_SumM3Sk_inside,
						&fStormTorkInfo_AvgPriceFactor,
						&fStormTorkInfo_TakeCareOfPerc,
						&fStormTorkInfo_PineP30Price,
						&fStormTorkInfo_SpruceP30Price,
						&fStormTorkInfo_BirchP30Price,
						&fStormTorkInfo_CompensationLevel,
						&fStormTorkInfo_Andel_Pine,
						&fStormTorkInfo_Andel_Spruce,
						&fStormTorkInfo_Andel_Birch,
						&fStormTorkInfo_WideningFactor);

					// Return-values; 090909 p�d
					if (pCruise != NULL)
						pCruise->setManuallyEnteredCalcData(recCruise,fSpruceMix,fSTValue,fSTVolume,fStormTorkInfo_SpruceMix,
						fStormTorkInfo_SumM3Sk_inside,
						fStormTorkInfo_AvgPriceFactor,
						fStormTorkInfo_TakeCareOfPerc,
						fStormTorkInfo_PineP30Price,
						fStormTorkInfo_SpruceP30Price,
						fStormTorkInfo_BirchP30Price,
						fStormTorkInfo_CompensationLevel,
						fStormTorkInfo_Andel_Pine,
						fStormTorkInfo_Andel_Spruce,
						fStormTorkInfo_Andel_Birch,
						fStormTorkInfo_WideningFactor);

				}
				//-------------------------------------------------------------------------
				// Calculate "Storm- och Torkskador", acordingly to the "Skogsnormen ver1.1"; 090908 p�d
				//-------------------------------------------------------------------------
				else if (recObject.getObjUseNormID() == ID_2009_FORREST_NORM)
				{
					parseTGL(recCruise.getECruTGL(),&nPinePercent,&nSprucePercent,&nBirchPercent);
					if (recCruise.getECruWidth() > 0.0)
					{
						fNewWidth = recCruise.getECruWidth();
					}
					else
					{
						if (recObject.getObjTypeOfInfring().Compare(_T("0")) == 0) // "Nybyggnation"
						{
							fNewWidth = recObject.getObjParallelWidth();
						}
						else if (recObject.getObjTypeOfInfring().Compare(_T("1")) == 0) // "Breddning"
						{
							if (recCruise.getECruUseSampleTrees() == 1)			// "Sida 1"
								fNewWidth = recObject.getObjAddedWidth1();
							else if (recCruise.getECruUseSampleTrees() == 2)	// "Sida 2"
								fNewWidth = recObject.getObjAddedWidth2();
						}
					}
					calculateStormDry_2009(recObject.getObjRecalcBy(),
																 nPinePercent,nSprucePercent,nBirchPercent,
																 recCruise.getECruM3Sk(),
																 vecP30_nn_table,
																 recCruise.getECruSI(),
																 recCruise.getECruUseSampleTrees(),
																 fPresentWidth,
																 fNewWidth,
																 &fSpruceMix,&fSTValue,&fSTVolume,
																 recObject.getObjTypeOfInfring() == L"1" ||
																 recObject.getObjTypeOfInfring() == L"0",
						&fStormTorkInfo_SpruceMix,
						&fStormTorkInfo_SumM3Sk_inside,
						&fStormTorkInfo_AvgPriceFactor,
						&fStormTorkInfo_TakeCareOfPerc,
						&fStormTorkInfo_PineP30Price,
						&fStormTorkInfo_SpruceP30Price,
						&fStormTorkInfo_BirchP30Price,
						&fStormTorkInfo_CompensationLevel,
						&fStormTorkInfo_Andel_Pine,
						&fStormTorkInfo_Andel_Spruce,
						&fStormTorkInfo_Andel_Birch,
						&fStormTorkInfo_WideningFactor);
					// Return-values; 090909 p�d
					if (pCruise != NULL)
						pCruise->setManuallyEnteredCalcData(recCruise,fSpruceMix,fSTValue,fSTVolume,
						fStormTorkInfo_SpruceMix,
						fStormTorkInfo_SumM3Sk_inside,
						fStormTorkInfo_AvgPriceFactor,
						fStormTorkInfo_TakeCareOfPerc,
						fStormTorkInfo_PineP30Price,
						fStormTorkInfo_SpruceP30Price,
						fStormTorkInfo_BirchP30Price,
						fStormTorkInfo_CompensationLevel,
						fStormTorkInfo_Andel_Pine,
						fStormTorkInfo_Andel_Spruce,
						fStormTorkInfo_Andel_Birch,
						fStormTorkInfo_WideningFactor);
				}	// else if (recObject.getObjUseNormID() == 1)

				//-------------------------------------------------------------------------
				// Calculate "Storm- och Torkskador", acordingly to the "Skogsnormen 2018";
				//-------------------------------------------------------------------------
				else if (recObject.getObjUseNormID() == ID_2018_FORREST_NORM)
				{
					parseTGL(recCruise.getECruTGL(),&nPinePercent,&nSprucePercent,&nBirchPercent);
					if (recCruise.getECruWidth() > 0.0)
					{
						fNewWidth = recCruise.getECruWidth();
					}
					else
					{
						if (recObject.getObjTypeOfInfring().Compare(_T("0")) == 0) // "Nybyggnation"
						{
							fNewWidth = recObject.getObjParallelWidth();
						}
						else if (recObject.getObjTypeOfInfring().Compare(_T("1")) == 0) // "Breddning"
						{
							if (recCruise.getECruUseSampleTrees() == 1)			// "Sida 1"
								fNewWidth = recObject.getObjAddedWidth1();
							else if (recCruise.getECruUseSampleTrees() == 2)	// "Sida 2"
								fNewWidth = recObject.getObjAddedWidth2();
						}
					}
					calculateStormDry_2018(recObject.getObjRecalcBy(),
																 nPinePercent,nSprucePercent,nBirchPercent,
																 recCruise.getECruM3Sk(),
																 vecP30_nn_table,
																 recCruise.getECruSI(),
																 recCruise.getECruUseSampleTrees(),
																 fPresentWidth,
																 fNewWidth,
																 &fSpruceMix,&fSTValue,&fSTVolume,
																 recObject.getObjTypeOfInfring() == L"1" ||
						recObject.getObjTypeOfInfring() == L"0",
						&fStormTorkInfo_SpruceMix,
						&fStormTorkInfo_SumM3Sk_inside,
						&fStormTorkInfo_AvgPriceFactor,
						&fStormTorkInfo_TakeCareOfPerc,
						&fStormTorkInfo_PineP30Price,
						&fStormTorkInfo_SpruceP30Price,
						&fStormTorkInfo_BirchP30Price,
						&fStormTorkInfo_CompensationLevel,
						&fStormTorkInfo_Andel_Pine,
						&fStormTorkInfo_Andel_Spruce,
						&fStormTorkInfo_Andel_Birch,
						&fStormTorkInfo_WideningFactor);
					// Return-values; 090909 p�d
					if (pCruise != NULL)
						pCruise->setManuallyEnteredCalcData(recCruise,fSpruceMix,fSTValue,fSTVolume,
						fStormTorkInfo_SpruceMix,
						fStormTorkInfo_SumM3Sk_inside,
						fStormTorkInfo_AvgPriceFactor,
						fStormTorkInfo_TakeCareOfPerc,
						fStormTorkInfo_PineP30Price,
						fStormTorkInfo_SpruceP30Price,
						fStormTorkInfo_BirchP30Price,
						fStormTorkInfo_CompensationLevel,
						fStormTorkInfo_Andel_Pine,
						fStormTorkInfo_Andel_Spruce,
						fStormTorkInfo_Andel_Birch,
						fStormTorkInfo_WideningFactor);
				}	// else if (recObject.getObjUseNormID() == 1)
			}	// if (recCruise.getECruType() == CRUISE_TYPE_2)
		}	// for (UINT i = 0;i < vecCruise.size();i++)
	}	// if (vecCruise.size() > 0)
	
}
// Calculate ALL Manually entered cruises, for an Object; 090910 p�d
void CPropertiesFormView::calculateStormDry(int obj_id,vecTransaction_elv_cruise& vec_cruise)
{
	CString sName,sDoneBy,sNotes;
	CTransaction_elv_object recObject;
	CTransaction_elv_cruise recCruise;

	vecObjectTemplate_p30_table vecP30_table;			// "1950:�rd Skogsnorm"
	vecObjectTemplate_p30_nn_table vecP30_nn_table;		// "Skogsnormen ver1.1"

	int nPinePercent,nSprucePercent,nBirchPercent;
	double fSpruceMix,fSTValue,fSTVolume;

	CString sArea,S;
	int nAreaIndex = 0;

	double fPresentWidth = 0.0;
	double fNewWidth = 0.0;

	double fStormTorkInfo_SpruceMix=0.0;
	double fStormTorkInfo_SumM3Sk_inside=0.0;
	double fStormTorkInfo_AvgPriceFactor=0.0;
	double fStormTorkInfo_TakeCareOfPerc=0.0;
	double fStormTorkInfo_PineP30Price=0.0;
	double fStormTorkInfo_SpruceP30Price=0.0;
	double fStormTorkInfo_BirchP30Price=0.0;
	double fStormTorkInfo_CompensationLevel=0.0;
	double fStormTorkInfo_Andel_Pine=0.0;
	double fStormTorkInfo_Andel_Spruce=0.0;
	double fStormTorkInfo_Andel_Birch=0.0;
	double fStormTorkInfo_WideningFactor=0.0;

	nPinePercent = nSprucePercent = nBirchPercent = 0;
	fSpruceMix = fSTValue = fSTVolume = 0.0;

	CCruisingData *pCruise = getCruisingView();

	// Collect fresh info. from DB; 090908 p�d
	if (m_pDB) 
	{
		m_pDB->getObject(obj_id,recObject);
	}
	// Load P30 table for Object; 090909 p�d
	TemplateParser parser;
	if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
	{
		if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		{
			parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
		}
	}
	else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_NEW_NORM)
	{
		if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		{
			parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
			setP30NN_SIMax(vecP30_nn_table);
		}
	}
	else if (recObject.getObjP30TypeOf() == TEMPLATE_P30_2018_NORM)
	{
		if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		{
			parser.getObjTmplP30_nn(vecP30_nn_table,sArea,&nAreaIndex);
			setP30NN_SIMax(vecP30_nn_table);
		}
	}
	// Check if this is a "Breddning", if so set widths; 090909 p�d
	if (_tstoi(recObject.getObjTypeOfInfring()) == 1 /* "Breddning" */)
	{
		fPresentWidth = recObject.getObjPresentWidth1();
	}

	if (vec_cruise.size() > 0)
	{
		for (UINT i = 0;i < vec_cruise.size();i++)
		{
			recCruise = vec_cruise[i];
			if (recCruise.getECruType() == CRUISE_TYPE_2)	// Manually entered; 090908 p�d
			{
				//-------------------------------------------------------------------------
				// Calculate "Storm- och Torkskador", acordingly to the "1950:�rs skogsnorm"; 090908 p�d
				//-------------------------------------------------------------------------
				if (recObject.getObjUseNormID() == ID_1950_FORREST_NORM)
				{
					parseTGL(recCruise.getECruTGL(),&nPinePercent,&nSprucePercent,&nBirchPercent);
					calculateStormDry_1950(nPinePercent,nSprucePercent,nBirchPercent,
																 vecP30_table,
																 recCruise.getECruM3Sk(),
																 recCruise.getECruSI(),
																 recObject.getObjGrowthArea(),
																 recObject.getObjPercent(),
						&fSpruceMix,&fSTValue,&fSTVolume,
						&fStormTorkInfo_SpruceMix,
						&fStormTorkInfo_SumM3Sk_inside,
						&fStormTorkInfo_AvgPriceFactor,
						&fStormTorkInfo_TakeCareOfPerc,
						&fStormTorkInfo_PineP30Price,
						&fStormTorkInfo_SpruceP30Price,
						&fStormTorkInfo_BirchP30Price,
						&fStormTorkInfo_CompensationLevel,
						&fStormTorkInfo_Andel_Pine,
						&fStormTorkInfo_Andel_Spruce,
						&fStormTorkInfo_Andel_Birch,
						&fStormTorkInfo_WideningFactor);

					// Return-values; 090909 p�d
					if (pCruise != NULL)
						pCruise->setManuallyEnteredCalcData(recCruise,fSpruceMix,fSTValue,fSTVolume,
						fStormTorkInfo_SpruceMix,
						fStormTorkInfo_SumM3Sk_inside,
						fStormTorkInfo_AvgPriceFactor,
						fStormTorkInfo_TakeCareOfPerc,
						fStormTorkInfo_PineP30Price,
						fStormTorkInfo_SpruceP30Price,
						fStormTorkInfo_BirchP30Price,
						fStormTorkInfo_CompensationLevel,
						fStormTorkInfo_Andel_Pine,
						fStormTorkInfo_Andel_Spruce,
						fStormTorkInfo_Andel_Birch,
						fStormTorkInfo_WideningFactor);
				}
				//-------------------------------------------------------------------------
				// Calculate "Storm- och Torkskador", acordingly to the "Skogsnormen ver1.1"; 090908 p�d
				//-------------------------------------------------------------------------
				else if (recObject.getObjUseNormID() == ID_2009_FORREST_NORM)
				{
					parseTGL(recCruise.getECruTGL(),&nPinePercent,&nSprucePercent,&nBirchPercent);
					if (recCruise.getECruWidth() > 0.0)
					{
						fNewWidth = recCruise.getECruWidth();
					}
					else
					{
						if (recObject.getObjTypeOfInfring().Compare(_T("0")) == 0) // "Nybyggnation"
						{
							fNewWidth = recObject.getObjParallelWidth();
						}
						else if (recObject.getObjTypeOfInfring().Compare(_T("1")) == 0) // "Breddning"
						{
							if (recCruise.getECruUseSampleTrees() == 1)			// "Sida 1"
								fNewWidth = recObject.getObjAddedWidth1();
							else if (recCruise.getECruUseSampleTrees() == 2)	// "Sida 2"
								fNewWidth = recObject.getObjAddedWidth2();
						}
					}
					calculateStormDry_2009(recObject.getObjRecalcBy(),
																 nPinePercent,nSprucePercent,nBirchPercent,
																 recCruise.getECruM3Sk(),
																 vecP30_nn_table,
																 recCruise.getECruSI(),
																 recCruise.getECruUseSampleTrees(),
																 fPresentWidth,
																 fNewWidth,
																 &fSpruceMix,&fSTValue,&fSTVolume,
																 recObject.getObjTypeOfInfring() == L"1" ||
																 recObject.getObjTypeOfInfring() == L"0",
						&fStormTorkInfo_SpruceMix,
						&fStormTorkInfo_SumM3Sk_inside,
						&fStormTorkInfo_AvgPriceFactor,
						&fStormTorkInfo_TakeCareOfPerc,
						&fStormTorkInfo_PineP30Price,
						&fStormTorkInfo_SpruceP30Price,
						&fStormTorkInfo_BirchP30Price,
						&fStormTorkInfo_CompensationLevel,
						&fStormTorkInfo_Andel_Pine,
						&fStormTorkInfo_Andel_Spruce,
						&fStormTorkInfo_Andel_Birch,
						&fStormTorkInfo_WideningFactor);
					// Return-values; 090909 p�d
					if (pCruise != NULL)
						pCruise->setManuallyEnteredCalcData(recCruise,fSpruceMix,fSTValue,fSTVolume,
						fStormTorkInfo_SpruceMix,
						fStormTorkInfo_SumM3Sk_inside,
						fStormTorkInfo_AvgPriceFactor,
						fStormTorkInfo_TakeCareOfPerc,
						fStormTorkInfo_PineP30Price,
						fStormTorkInfo_SpruceP30Price,
						fStormTorkInfo_BirchP30Price,
						fStormTorkInfo_CompensationLevel,
						fStormTorkInfo_Andel_Pine,
						fStormTorkInfo_Andel_Spruce,
						fStormTorkInfo_Andel_Birch,
						fStormTorkInfo_WideningFactor);
				}	// else if (recObject.getObjUseNormID() == 1)

				//-------------------------------------------------------------------------
				// Calculate "Storm- och Torkskador", acordingly to the "Skogsnormen 2018;
				//-------------------------------------------------------------------------
				else if (recObject.getObjUseNormID() == ID_2018_FORREST_NORM)
				{
					parseTGL(recCruise.getECruTGL(),&nPinePercent,&nSprucePercent,&nBirchPercent);
					if (recCruise.getECruWidth() > 0.0)
					{
						fNewWidth = recCruise.getECruWidth();
					}
					else
					{
						if (recObject.getObjTypeOfInfring().Compare(_T("0")) == 0) // "Nybyggnation"
						{
							fNewWidth = recObject.getObjParallelWidth();
						}
						else if (recObject.getObjTypeOfInfring().Compare(_T("1")) == 0) // "Breddning"
						{
							if (recCruise.getECruUseSampleTrees() == 1)			// "Sida 1"
								fNewWidth = recObject.getObjAddedWidth1();
							else if (recCruise.getECruUseSampleTrees() == 2)	// "Sida 2"
								fNewWidth = recObject.getObjAddedWidth2();
						}
					}
					calculateStormDry_2018(recObject.getObjRecalcBy(),
																 nPinePercent,nSprucePercent,nBirchPercent,
																 recCruise.getECruM3Sk(),
																 vecP30_nn_table,
																 recCruise.getECruSI(),
																 recCruise.getECruUseSampleTrees(),
																 fPresentWidth,
																 fNewWidth,
																 &fSpruceMix,&fSTValue,&fSTVolume,
																 recObject.getObjTypeOfInfring() == L"1" ||
																 recObject.getObjTypeOfInfring() == L"0",
						&fStormTorkInfo_SpruceMix,
						&fStormTorkInfo_SumM3Sk_inside,
						&fStormTorkInfo_AvgPriceFactor,
						&fStormTorkInfo_TakeCareOfPerc,
						&fStormTorkInfo_PineP30Price,
						&fStormTorkInfo_SpruceP30Price,
						&fStormTorkInfo_BirchP30Price,
						&fStormTorkInfo_CompensationLevel,
						&fStormTorkInfo_Andel_Pine,
						&fStormTorkInfo_Andel_Spruce,
						&fStormTorkInfo_Andel_Birch,
						&fStormTorkInfo_WideningFactor);
					// Return-values; 090909 p�d
					if (pCruise != NULL)
						pCruise->setManuallyEnteredCalcData(recCruise,fSpruceMix,fSTValue,fSTVolume,
						fStormTorkInfo_SpruceMix,
						fStormTorkInfo_SumM3Sk_inside,
						fStormTorkInfo_AvgPriceFactor,
						fStormTorkInfo_TakeCareOfPerc,
						fStormTorkInfo_PineP30Price,
						fStormTorkInfo_SpruceP30Price,
						fStormTorkInfo_BirchP30Price,
						fStormTorkInfo_CompensationLevel,
						fStormTorkInfo_Andel_Pine,
						fStormTorkInfo_Andel_Spruce,
						fStormTorkInfo_Andel_Birch,
						fStormTorkInfo_WideningFactor);
				}	// else if (recObject.getObjUseNormID() == 1)
			}	// if (recCruise.getECruType() == CRUISE_TYPE_2)
		}	// for (UINT i = 0;i < vecCruise.size();i++)
	}	// if (vecCruise.size() > 0)
}


void CPropertiesFormView::caclulateCorrFactor(void)
{
	CString sName,sDoneBy,sNotes;
	vecObjectTemplate_p30_table vecP30_table;					// "1950:�rd Skogsnorm"

	CTransaction_elv_object recObject;
	//vecTransaction_eval_evaluation vecEVal;
	CTransaction_eval_evaluation recEval;

	CString sSI;
	int nAge = 0;
	double fPinePerc = 0.0;
	double fSprucePerc = 0.0;
	double fBirchPerc = 0.0;

	double fCorrFactor = 0.0;
	double fVolume = 0.0;			// Virkesf�rr�d

	CString S;
	CEvaluatedData *pEVal = getEvaluatedView();
	if (pEVal != NULL)
	{
		pEVal->getSelectedEvaluated(recEval,sSI,&nAge,&fPinePerc,&fSprucePerc,&fBirchPerc);
	}	// if (pCruise != NULL)

	// Collect fresh info. from DB; 090908 p�d
	if (m_pDB) 
	{
		m_pDB->getObject(m_recActiveProperty.getPropObjectID_pk(),recObject);
	}

	//-------------------------------------------------------------------------
	// Calculate corr-factor, acordingly to the "1950:�rs skogsnorm"; 090911 p�d
	//-------------------------------------------------------------------------
	if (recObject.getObjUseNormID() == ID_1950_FORREST_NORM)
	{
		// Load P30 table for Object; 090909 p�d
		TemplateParser parser;
		if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
		{
			if (parser.LoadFromBuffer(recObject.getObjP30XML()))
			{
				parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
			}	// if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		}	// if (recObject.getObjP30TypeOf() == TEMPLATE_P30)

		CMyInputDlg *pDlg = new CMyInputDlg();
		if (pDlg != NULL)
		{
			if (pDlg->DoModal() == IDOK)
			{
				fVolume = pDlg->getVolumeEntered();

				calculateCorrFactor_1950(fVolume,
																 fPinePerc,
																 fSprucePerc,
																 fBirchPerc,
																 vecP30_table,
																 sSI,
																 nAge,
																 _tstoi(recObject.getObjGrowthArea()),
																 &fCorrFactor);
				if (pEVal != NULL)
					pEVal->setManuallyEnteredCorrFactor(recEval,fCorrFactor);
			}
			delete pDlg;
		}
	}
	//-------------------------------------------------------------------------
	// Calculate corr-factor, acordingly to the "Skogsnormen ver1.1"; 090911 p�d
	//-------------------------------------------------------------------------
	else if (recObject.getObjUseNormID() == ID_2009_FORREST_NORM)
	{
		CMyInputDlg *pDlg = new CMyInputDlg();
		if (pDlg != NULL)
		{
			if (pDlg->DoModal() == IDOK)
			{
				fVolume = pDlg->getVolumeEntered();
				calculateCorrFactor_2009(fVolume,
																 sSI,
																 nAge,
																 _tstoi(recObject.getObjGrowthArea()),
																 &fCorrFactor);
				if (pEVal != NULL)
					pEVal->setManuallyEnteredCorrFactor(recEval,fCorrFactor);

			}
			delete pDlg;
		}
	}	// else if (recObject.getObjUseNormID() == 1)
	//-------------------------------------------------------------------------
	// Calculate corr-factor, acordingly to the "Skogsnormen 2018";
	//-------------------------------------------------------------------------
	else if (recObject.getObjUseNormID() == ID_2018_FORREST_NORM)
	{
		CMyInputDlg *pDlg = new CMyInputDlg();
		if (pDlg != NULL)
		{
			if (pDlg->DoModal() == IDOK)
			{
				fVolume = pDlg->getVolumeEntered();
				calculateCorrFactor_2018(fVolume,
																 sSI,
																 nAge,
																 _tstoi(recObject.getObjGrowthArea()),
																 &fCorrFactor);
				if (pEVal != NULL)
					pEVal->setManuallyEnteredCorrFactor(recEval,fCorrFactor);

			}
			delete pDlg;
		}
	}	// else if (recObject.getObjUseNormID() == 1)
}


void CPropertiesFormView::caclulateCorrFactor(double fVolume,CString tgl,CString si,int age,double *corr)
{
	CString sName,sDoneBy,sNotes;
	vecObjectTemplate_p30_table vecP30_table;					// "1950:�rd Skogsnorm"

	CTransaction_elv_object recObject;
	vecTransaction_eval_evaluation vecEVal;
	CTransaction_eval_evaluation recEval;
	CString sT,sG,sL;

	int nPos = 0;
	if (tgl.Right(tgl.GetLength()) != L";")
			tgl += L";";		

	sT = tgl.Tokenize(L";",nPos);
	sG = tgl.Tokenize(L";",nPos);
	sL = tgl.Tokenize(L";",nPos);

	double fPine = _tstof(sT);
	double fSpruce = _tstof(sG);
	double fBirch = _tstof(sL);

	double fCorrFactor = 0.0;
	// Collect fresh info. from DB; 090908 p�d
	if (m_pDB) 
	{
		m_pDB->getObject(m_recActiveProperty.getPropObjectID_pk(),recObject);
	}

	//-------------------------------------------------------------------------
	// Calculate corr-factor, acordingly to the "1950:�rs skogsnorm"; 090911 p�d
	//-------------------------------------------------------------------------
	if (recObject.getObjUseNormID() == ID_1950_FORREST_NORM)
	{
		// Load P30 table for Object; 090909 p�d
		TemplateParser parser;
		if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
		{
			if (parser.LoadFromBuffer(recObject.getObjP30XML()))
			{
				parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
			}	// if (parser.LoadFromBuffer(recObject.getObjP30XML()))
		}	// if (recObject.getObjP30TypeOf() == TEMPLATE_P30)

		calculateCorrFactor_1950(fVolume,
														 fPine,
														 fSpruce,
														 fBirch,
														 vecP30_table,
														 si,
														 age,
														 _tstoi(recObject.getObjGrowthArea()),
														 &fCorrFactor);
		*corr = fCorrFactor;
	}
	//-------------------------------------------------------------------------
	// Calculate corr-factor, acordingly to the "Skogsnormen ver1.1"; 090911 p�d
	//-------------------------------------------------------------------------
	else if (recObject.getObjUseNormID() == ID_2009_FORREST_NORM)
	{
		calculateCorrFactor_2009(fVolume,
														 si,
														 age,
														 _tstoi(recObject.getObjGrowthArea()),
														 &fCorrFactor);

		*corr = fCorrFactor;

	}	// else if (recObject.getObjUseNormID() == 1)
	//-------------------------------------------------------------------------
	// Calculate corr-factor, acordingly to the "Skogsnormen 2018";
	//-------------------------------------------------------------------------
	else if (recObject.getObjUseNormID() == ID_2018_FORREST_NORM)
	{
		calculateCorrFactor_2018(fVolume,
														 si,
														 age,
														 _tstoi(recObject.getObjGrowthArea()),
														 &fCorrFactor);

		*corr = fCorrFactor;

	}	// else if (recObject.getObjUseNormID() == 1)
}

int CPropertiesFormView::getFocusedPropertyID(void)
{
	CELVPropertyReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReportProperties.GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CELVPropertyReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			return pRec->getRecELVProp().getPropID_pk();
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)

	return m_recActiveProperty.getPropID_pk();
}

void CPropertiesFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PROP_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_wndReportProperties.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
/*	
	// Get selected column index into registry; 070219 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);
*/
}

void CPropertiesFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_wndReportProperties.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PROP_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
/*
	// Set selected column index into registry; 070219 p�d
	AfxGetApp()->WriteProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);
*/
}

BOOL CPropertiesFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CPropertiesFormView::writeSplitBarSettingsToRegistry(void)
{
	int nPane1Size = 16384;
	int nPane2Size = 16384;
	int nOrientation = 0;
	if (m_wndSplitter.GetSafeHwnd())
	{
		nOrientation = m_wndSplitter.GetOrientation();
		m_wndSplitter.GetPaneSize(PANE_1_INDEX,&nPane1Size);
		m_wndSplitter.GetPaneSize(PANE_2_INDEX,&nPane2Size);
		if (((nPane1Size*1.0)/(nPane2Size*1.0)) < 0.3 || nPane1Size < 0 || nPane2Size < 0)
		{
			nPane1Size = 16384;
			nPane2Size = 16384;
		}
		// Add SplitBar settings to registry; 090122 p�d
		regSetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_SPLITBAR_ORIENTATION,nOrientation);
		regSetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_PANE_1_SIZE,nPane1Size);
		regSetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_PANE_2_SIZE,nPane2Size);
	
	}	// if (m_wndSplitter.GetSafeHwnd())
}

void CPropertiesFormView::readSplitBarSettingsFromRegistry(void)
{
	int nPane1Size = 16384;
	int nPane2Size = 16384;
	if (m_wndSplitter.GetSafeHwnd())
	{
		// Add SplitBar settings to registry; 090122 p�d
		m_nOrientation = regGetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_SPLITBAR_ORIENTATION,SSP_VERT);	// Default orientation HORZ; 090122 p�d
		nPane1Size = regGetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_PANE_1_SIZE,16384);	// Default value
		nPane2Size = regGetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_PANE_2_SIZE,16384);	// Default value
		if (((nPane1Size*1.0)/(nPane2Size*1.0)) < 0.3 || nPane1Size < 0 || nPane2Size < 0)
		{
			m_SplitterPaneSizes[PANE_1_INDEX] = 16384;
			m_SplitterPaneSizes[PANE_2_INDEX] = 16384;
		}
		else
		{
			m_SplitterPaneSizes[PANE_1_INDEX] = regGetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_PANE_1_SIZE,16384);	// Default value
			m_SplitterPaneSizes[PANE_2_INDEX] = regGetInt(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_PANE_2_SIZE,16384);	// Default value
		}
	}	// if (m_wndSplitter.GetSafeHwnd())
}

//Lagt till funktion f�r att kunna s�tta en rad fokuserad utanf�r formviewn, skickar ut pekare till rapportdelen, pga ta bort flera fastigheter feature #2456 20111111 J�
CMyReportCtrl* CPropertiesFormView::GetReport()
{
 return &m_wndReportProperties;
}