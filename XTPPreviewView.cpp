// XTPPreviewView.cpp : implementation file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// (c)1998-2006 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "XTPPreviewView.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

bool CDic::m_bLoaded = false;
CMapStringToString CDic::m_Map(20); // Increment by 20 elements

CDic::CDic()
{

}

CDic::~CDic()
{
	m_Map.RemoveAll();
}

CString CDic::GetText(LPCTSTR strSection, LPCTSTR strID, LPCTSTR strDefaultText, bool bTransform)
{
	// strSection: section of the text file. sections are enclosed by []
	// strID     : identifier to access the desired entry
	//             entry is left side of '=', text is right side
	// bTransform: if you'd like to transform string "\t" to a single character '\t'
	//             works with '\t', '\n', '\r'

	// example
	// 
	// [Preview]
	// PRINT = &Drucken...
	// NEXT = &N�chste
	// PREV = &Vorherige
	// 
	// CString strRet = CDic::GetText(_T("Preview"), _T("NEXT"), _T("&Next page"));

	if (m_bLoaded==false) // wasn't loaded, return default text
		return strDefaultText;

	CString strLookup, strRet;
	CString strPartSection(strSection);
	CString strPartID(strID);
	strPartSection.MakeUpper();
	strPartID.MakeUpper();

	// Inserting a "_" to get a difference between "Settings"+"11" and "Settings1"+"1"
	strLookup.Format(_T("%s_%s"), strPartSection, strPartID);

	if (m_Map.Lookup(strLookup, strRet)) {
		if (bTransform)
			TransformSpecChars(strRet);
		return strRet;
	}

	return strDefaultText;
}

bool CDic::LoadLanguage(LPCTSTR strFilepath)
{
	m_Map.RemoveAll();

	int iPos;
	CString strLine, strSection, strLookup, strLeft, strRight;
	CStdioFile file;
	if (file.Open(strFilepath, CFile::modeRead)) {
		while (file.ReadString(strLine)) {
			strLine.TrimLeft();
			strLine.TrimRight();
			// remove comment
			if (strLine.Left(2)==_T("//")) {
				strLine.Empty(); // entire row is a comment, ATTENTION with URLs !!
			} else if ((iPos = strLine.Find(TCHAR(';'))) >= 0) {
				strLine = strLine.Mid(iPos+1); // comment
			} 

			if (strLine.IsEmpty()==FALSE) {
				// new section?
				if (strLine.GetAt(0) == '[') {
					strLine = strLine.Mid(1);
					iPos = strLine.Find(TCHAR(']'));
					if (iPos>=0) {
						strLine = strLine.Left(iPos);
					}
					strSection = strLine;
					strSection.MakeUpper();
				} else {
					// create an entry
					iPos = strLine.Find(TCHAR('='));
					if (iPos > 0) {
						strLeft  = strLine.Left(iPos);
						strRight = strLine.Mid(iPos+1);
						strLeft.TrimRight();
						strRight.TrimLeft();
						strLeft.MakeUpper();

						strLookup.Format(_T("%s_%s"), strSection, strLeft);
						m_Map.SetAt(strLookup, strRight);
					}
				}
			}
		}

		file.Close();
		m_bLoaded = true;
		return true;
	}

	m_bLoaded = false;
	return false;
}

void CDic::TransformSpecChars(CString &strTransform)
{
	strTransform.Replace(_T("\\r"), _T("\r")); // old, new
	strTransform.Replace(_T("\\n"), _T("\n")); // old, new
	strTransform.Replace(_T("\\t"), _T("\t")); // old, new
}

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(CLangPreviewView, CPreviewView)

BEGIN_MESSAGE_MAP(CLangPreviewView, CPreviewView)
	//{{AFX_MSG_MAP(CLangPreviewView)
	ON_UPDATE_COMMAND_UI(AFX_ID_PREVIEW_NUMPAGE, OnUpdateNumPageChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CLangPreviewView::CLangPreviewView()
{
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{

			m_sPrintOut= xml->str(IDS_STRING236);		// "Skriv ut"
			m_sNextPage= xml->str(IDS_STRING237);		// "N�sta sida"
			m_sPrevPage= xml->str(IDS_STRING238);		// "F�reg. sida"
			m_sZoomIn= xml->str(IDS_STRING239);			// "Zooma in"
			m_sZoomOut= xml->str(IDS_STRING240);		// "Zooma ut"
			m_sClose= xml->str(IDS_STRING241);			// "St�ng"
			m_sOnePage = xml->str(IDS_STRING242);		// "En sida"
			m_sTwoPages = xml->str(IDS_STRING243);	// "Tv� sdidor"

		}
		delete xml;
	}

}

CLangPreviewView::~CLangPreviewView()
{

}

void CLangPreviewView::OnPreviewPrint()
{
	CPreviewView::OnPreviewPrint();
}

void CLangPreviewView::OnActivateView(BOOL bActivate, CView*, CView*)
{
	if (bActivate)
	{
		if (m_pToolBar) 
		{
			m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_PRINT)->SetWindowText(CDic::GetText(_T("Preview"), _T("PRINT"), (m_sPrintOut)));
			m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_NEXT)->SetWindowText(CDic::GetText(_T("Preview"), _T("NEXT"), (m_sNextPage)));
			m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_PREV)->SetWindowText(CDic::GetText(_T("Preview"), _T("PREV"), (m_sPrevPage)));
//					m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_NUMPAGE)->SetWindowText(CDic::GetText(_T("Preview"), _T("NUMPAGE"), _T("")));
			m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_ZOOMIN)->SetWindowText(CDic::GetText(_T("Preview"), _T("ZOOMIN"), (m_sZoomIn)));
			m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_ZOOMOUT)->SetWindowText(CDic::GetText(_T("Preview"), _T("ZOOMOUT"), (m_sZoomOut)));
			m_pToolBar->GetDlgItem(AFX_ID_PREVIEW_CLOSE)->SetWindowText(CDic::GetText(_T("Preview"), _T("CLOSE"), (m_sClose)));
	
		}	// if (m_pToolBar) 
	}	// if (bActivate)
}

void CLangPreviewView::OnUpdateNumPageChange(CCmdUI* pCmdUI)
{
	UINT nPages = (m_nZoomState == ZOOM_OUT ? m_nPages : m_nZoomOutPages);
	if (nPages == 1)
		pCmdUI->SetText(CDic::GetText(_T("Preview"), _T("TWOPAGE"), m_sTwoPages));
	else
		pCmdUI->SetText(CDic::GetText(_T("Preview"), _T("ONEPAGE"), m_sOnePage));

	// To avoid flicker (display default text); 070110 p�d
//	CPreviewView::OnUpdateNumPageChange(pCmdUI);
}

