// PricelistsListFrameView.cpp : implementation file
//

#include "stdafx.h"
#include "P30TemplateFormView.h"
#include "MDITemplateListFrameView.h"

#include "ResLangFileReader.h"

#include "UMLandValueDB.h"

// CTemplateListFrameView

IMPLEMENT_DYNCREATE(CTemplateListFrameView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTemplateListFrameView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_ERASEBKGND()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, ID_REPORT_P30_LIST_TEMPL, OnReportClick)
END_MESSAGE_MAP()

CTemplateListFrameView::CTemplateListFrameView()
	: CXTResizeFormView(CTemplateListFrameView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CTemplateListFrameView::~CTemplateListFrameView()
{
}

void CTemplateListFrameView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP

}

void CTemplateListFrameView::OnDestroy()
{
	m_vecTransactionTemplate.clear();

	m_wndTemplates.ResetContent();

	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();
}

void CTemplateListFrameView::OnClose()
{
	
	CXTResizeFormView::OnClose();
}

BOOL CTemplateListFrameView::OnEraseBkgnd(CDC *pDC)
{

	CRect clip;
	m_wndTemplates.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_BTNFACE));

	return FALSE;
}

void CTemplateListFrameView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndTemplates.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTemplates,1,1,rect.right - 1,rect.bottom - 2);
	}
	
}

BOOL CTemplateListFrameView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CTemplateListFrameView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (!	m_bInitialized )
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		CP30TemplateFormView *pView = (CP30TemplateFormView *)getFormViewByID(IDD_FORMVIEW2);
		if (pView)
		{
			m_nDBIndex = pView->getDBIndex();
			// Need to release ref. pointers; 080520 p�d
			pView = NULL;
		}

		setupReport();
		getTemplatesFromDB();
		populateData();

		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

BOOL CTemplateListFrameView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CTemplateListFrameView diagnostics

#ifdef _DEBUG
void CTemplateListFrameView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CTemplateListFrameView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// Protected

// Create and add PricelistsList reportwindow
BOOL CTemplateListFrameView::setupReport(void)
{

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndTemplates.GetSafeHwnd() == 0)
	{
		// Create the Reportcontrol
		if (!m_wndTemplates.Create(this, ID_REPORT_P30_LIST_TEMPL ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndTemplates.GetSafeHwnd() != NULL)
				{

					m_wndTemplates.ShowWindow( SW_NORMAL );
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING2500)), 100));
					pCol->AllowRemove(FALSE);
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING2501)), 100));
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(2, (xml->str(IDS_STRING2502)), 100));

					m_wndTemplates.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndTemplates.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndTemplates.GetReportHeader()->AllowColumnResize( TRUE );
					m_wndTemplates.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndTemplates.GetReportHeader()->SetAutoColumnSizing( TRUE );
					m_wndTemplates.SetMultipleSelection( FALSE );
					m_wndTemplates.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndTemplates.AllowEdit(FALSE);
					m_wndTemplates.FocusSubItems(TRUE);

					RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
					setResize(&m_wndTemplates,1,30,rect.right - 1,rect.bottom - 30);

				}	// if (m_wndTemplates.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;

	}	// if (fileExists(m_sLangFN))

	return TRUE;

}

BOOL CTemplateListFrameView::populateData(void)
{
	CTransaction_template tmplSel;
	CXTPReportRecord *pRec = NULL;
	if (m_vecTransactionTemplate.size() > 0)
	{
		if (m_nDBIndex >= 0 && m_nDBIndex < m_vecTransactionTemplate.size())
		{
			tmplSel = m_vecTransactionTemplate[m_nDBIndex];
		}
		for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
		{
			CTransaction_template tmpl = m_vecTransactionTemplate[i];
			if (tmpl.getID() == tmplSel.getID())
			{
				pRec = m_wndTemplates.AddRecord(new CTemplateListReportDataRec(i+1,tmpl.getTemplateName(),tmpl.getCreatedBy(),tmpl.getCreated()));
			}
			else
			{
				m_wndTemplates.AddRecord(new CTemplateListReportDataRec(i+1,tmpl.getTemplateName(),tmpl.getCreatedBy(),tmpl.getCreated()));
			}
		}
		m_wndTemplates.Populate();
//		m_wndTemplates.UpdateWindow();
		// Added 081006 p�d
		m_wndTemplates.RedrawControl();
		if (pRec)
		{
			CXTPReportRow *pRow = m_wndTemplates.GetRows()->Find(pRec);
			if (pRow)
			{
				m_wndTemplates.SetFocusedRow(pRow);
			}
		}

		return TRUE;
	}
	return FALSE;
}

void CTemplateListFrameView::getTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getObjectTemplates(m_vecTransactionTemplate,TEMPLATE_P30);
		}
	}
}

// CTemplateListFrameView message handlers

void CTemplateListFrameView::OnReportClick(NMHDR* pNMHDR, LRESULT* pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;

	if (m_wndTemplates.GetSafeHwnd() == NULL)
		return;

	if (pItemNotify->pRow)
	{
		CTemplateListReportDataRec *pRec = (CTemplateListReportDataRec *)pItemNotify->pItem->GetRecord();
		if (pRec)
		{
			m_nDBIndex = pRec->GetIndex();
			CP30TemplateFormView *pView = (CP30TemplateFormView *)getFormViewByID(IDD_FORMVIEW2);
			if (pView != NULL)
			{
				pView->doPopulateData(m_nDBIndex);
				// Need to release ref. pointers; 080520 p�d
				pView = NULL;
			}
		}

	}
	*pResult = 0;
}
