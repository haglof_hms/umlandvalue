#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////////
// CStandsMatchedToPropRec

class CStandsMatchedToPropRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_trakt propRec;
	CString sPropName;
protected:

public:
	CStandsMatchedToPropRec(void)
	{
		m_nIndex = -1;
		AddItem(new CExCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CTextItem(_T("")));
	}

	CStandsMatchedToPropRec(UINT index,BOOL is_used,CTransaction_trakt data,LPCTSTR obj)	 
	{
		CString S;
		m_nIndex = index;
		propRec = data;
		S.Format(L"%d",data.getWSide());
		AddItem(new CExCheckItem(is_used));
		AddItem(new CTextItem(data.getTraktName()));
		AddItem(new CTextItem((data.getTraktNum())));
//		AddItem(new CTextItem((data.getTraktType())));	// Not used "Region"; 080415 p�d
		AddItem(new CTextItem(S));	
		AddItem(new CFloatItem(data.getTraktAreal(),sz3dec));
		AddItem(new CTextItem(obj));
	
	}

	BOOL getColumnCheck(int item)
	{
		return ((CExCheckItem*)GetItem(item))->getChecked();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
	CTransaction_trakt &getRecord(void)
	{
		return propRec;
	}

};

// CStandsMatchedToPropDlg dialog

class CStandsMatchedToPropDlg : public CDialog
{
	DECLARE_DYNAMIC(CStandsMatchedToPropDlg)

	// Setup language filename; 080429 p�d
	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CMyReportCtrl m_wndReport;
	CImageList m_ilIcons;

	vecTransactionTrakt m_vecTrakt;
	vecTransactionTrakt m_vecTrakts_selected;
	vecTransactionTrakt m_vecTrakts_de_selected;

	vecInt m_vecTraktIDs;

	CTransaction_elv_properties m_recELVProp;
	CUMLandValueDB *m_pDB;

	void setupReport(void);

public:
	CStandsMatchedToPropDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CStandsMatchedToPropDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG9 };

	void setDBConnection(CUMLandValueDB *db)	{	m_pDB = db;	}

	void setELVPropertyRecord(CTransaction_elv_properties &rec)	{ m_recELVProp = rec; }

	vecInt& getTrakIDs(void)	{ return m_vecTraktIDs; }

	vecTransactionTrakt& getTrakts(void)  { return m_vecTrakts_selected; }
	vecTransactionTrakt& getDeSelectedTrakts(void)  { return m_vecTrakts_de_selected; }

	void populateData(void);

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
