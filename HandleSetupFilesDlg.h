#pragma once

#include "Resource.h"

extern CUMLandValueDB *global_pDB;

/////////////////////////////////////////////////////////////////////////////////
// CHandleSetupFileRec

class CHandleSetupFileRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_elv_properties recProp;
protected:

public:
	CHandleSetupFileRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		this->GetItem(0)->SetBackgroundColor(WHITE);
		this->GetItem(1)->SetBackgroundColor(WHITE);
		this->GetItem(2)->SetBackgroundColor(WHITE);
	}

	CHandleSetupFileRec(UINT index,LPCTSTR prop,LPCTSTR setup_file_name,LPCTSTR setup_created,CTransaction_elv_properties rec,BOOL is_numeric,BOOL is_numeric2)	 
	{
		m_nIndex = index;
		recProp = rec;
/*
		AddItem(new CTextItem(prop));
		AddItem(new CTextItem(setup_file_name));
		AddItem(new CTextItem(setup_created));

		if (rec.getPropStatus() <= STATUS_ADVICED)
		{
			this->GetItem(0)->SetBackgroundColor(WHITE);
			this->GetItem(1)->SetBackgroundColor(WHITE);
			this->GetItem(2)->SetBackgroundColor(WHITE);
		}
		else if (rec.getPropStatus() == STATUS_COMPENS_OFFER)
		{
			this->GetItem(0)->SetBackgroundColor(STATUS_COLOR_COMPENS_OFFER);
			this->GetItem(1)->SetBackgroundColor(STATUS_COLOR_COMPENS_OFFER);
			this->GetItem(2)->SetBackgroundColor(STATUS_COLOR_COMPENS_OFFER);
		}
		else if (rec.getPropStatus() == STATUS_RETURNED1) 
		{
			this->GetItem(0)->SetBackgroundColor(STATUS_COLOR_RETURNED1);
			this->GetItem(1)->SetBackgroundColor(STATUS_COLOR_RETURNED1);
			this->GetItem(2)->SetBackgroundColor(STATUS_COLOR_RETURNED1);
		}
		else if (rec.getPropStatus() == STATUS_RETURNED2)
		{
			this->GetItem(0)->SetBackgroundColor(STATUS_COLOR_RETURNED2);
			this->GetItem(1)->SetBackgroundColor(STATUS_COLOR_RETURNED2);
			this->GetItem(2)->SetBackgroundColor(STATUS_COLOR_RETURNED2);
		}
		else if (rec.getPropStatus() == STATUS_PAYED)
		{
			this->GetItem(0)->SetBackgroundColor(STATUS_COLOR_PAYED);
			this->GetItem(1)->SetBackgroundColor(STATUS_COLOR_PAYED);
			this->GetItem(2)->SetBackgroundColor(STATUS_COLOR_PAYED);
		}
		else if (rec.getPropStatus() == STATUS_DONE) 
		{
			this->GetItem(0)->SetBackgroundColor(STATUS_COLOR_DONE);
			this->GetItem(1)->SetBackgroundColor(STATUS_COLOR_DONE);
			this->GetItem(2)->SetBackgroundColor(STATUS_COLOR_DONE);
		}
*/
//*	THIS SET A STRIKEOUT Font; 090615 p�d
		//if (rec.getPropStatus() <= STATUS_ADVICED)

		AddItem(new CIntItem(recProp.getPropSortOrder()));

		if (is_numeric2)
			AddItem(new CIntItem(_tstoi(recProp.getPropGroupID())));
		else
			AddItem(new CTextItem(recProp.getPropGroupID()));

		if (is_numeric)
			AddItem(new CIntItem(_tstoi(recProp.getPropNumber())));
		else
			AddItem(new CTextItem(recProp.getPropNumber()));
		if (canWeCalculateThisProp_cached(rec.getPropStatus()))
		{
			AddItem(new CTextItem(prop));
		}
		else
		{
			AddItem(new CStrikeOutTextItem(prop));
		}

		AddItem(new CTextItem(setup_file_name));
		AddItem(new CTextItem(setup_created));
		AddItem( new CTextItem( rec.getCoord() ) );	//#3725
//*/
	}

	CTransaction_elv_properties& getRecord(void)	{ return recProp; }

	CString getColumnText(int item)	{ return ((CTextItem*)GetItem(item))->getTextItem();	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
};

// CHandleSetupFilesDlg dialog

class CHandleSetupFilesDlg : public CDialog
{
	DECLARE_DYNAMIC(CHandleSetupFilesDlg)

	// Setup language filename; 080429 p�d
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgFileNameMissing;
	CString m_sMsgReplaceFile;
	CString m_sMsgRemoveSetupFile;
	CString m_sMsgEMailSubject;
	CString m_sMsgEMailText;
	CString m_sMsgDuplcatePropInSetup1;
	CString m_sMsgDuplcatePropInSetup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;

	CMyExtEdit m_wndStupFileName;

	CButton m_wndSave;
	CButton m_wndRemoveSetup;
	CButton m_wndCancelBtn;

	int m_nObjID;
	CString m_sPricelistXML;
	CStringArray m_sarrSetupFileList;

	mapStrings m_mapPropInSetupFile;
	mapStrings m_mapPropInSetupFileDateTime;

	CTransaction_elv_properties m_recElvProps;
	vecTransaction_elv_properties m_vecTransaction_elv_properties;
	void getPropertiesInObject(void);

	CXTListCtrl m_wndLCtrl;
	CXTFlatHeaderCtrl m_Header;

	CUMLandValueDB *m_pDB;
	CMyReportCtrl m_wndReport;
	void setupReport(void);
	void populateData(void);

	void getSetupFiles(void);

	BOOL createSetupFile(void);
	void readSetupFile(void);

	BOOL saveSetupFile(void);

	vecTransactionSpecies vecSpc;
	void getSpeciesInPricelist(void);

	CStringArray m_sarrSetupFile;
	CString m_sSetupFile;

	BOOL isInAddToSetupList(LPCTSTR prop);
public:
	CHandleSetupFilesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHandleSetupFilesDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG17 };

	void setDBConnection(CUMLandValueDB *db)	{	m_pDB = db; global_pDB = m_pDB;	}
	void setObjectID(int id)		{ m_nObjID = id; }
	void setPricelistXML(LPCTSTR xml)		{ m_sPricelistXML = xml; }
	void SplitTextLatLon(CString csOrig, CString *pcsLat, CString *pcsLon);

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedAddToListCtrl();
	afx_msg void OnBnClickedAddAllToListCtrl();
	afx_msg void OnBnClickedRemoveFromListCtrl();
	afx_msg void OnBnClickedRemoveAllFromListCtrl();
	afx_msg void OnEnChangeSetupFNChange();
	afx_msg void OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult);
};
