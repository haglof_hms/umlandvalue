#include "StdAfx.h"

#include <time.h>

#include "UMLandValueDB.h"

//////////////////////////////////////////////////////////////////////////////////
// CUMLandValueDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CUMLandValueDB::CUMLandValueDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CUMLandValueDB::CUMLandValueDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CUMLandValueDB::CUMLandValueDB(DB_CONNECTION_DATA &db_connection, int nConType)
	: CDBBaseClass_SQLApi(db_connection, nConType)
{
}

// PRIVATE
BOOL CUMLandValueDB::traktDataExists(CTransaction_trakt_data &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdata_id=%d and tdata_trakt_id=%d and tdata_data_type=%d"),
		TBL_TRAKT_DATA,
		rec.getTDataID(),
		rec.getTDataTraktID(),
		rec.getTDataType());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDataExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdata_trakt_id=%d"),
		TBL_TRAKT_DATA,trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktSampleTreeExists(CTransaction_sample_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttree_id=%d and ttree_trakt_id=%d"),
		TBL_TRAKT_SAMPLE_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDCLSTreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_id=%d and tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDCLSTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDCLS1TreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_id=%d and tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDCLS1TreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDCLS2TreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_id=%d and tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktDCLS2TreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where trakt_id=%d"),
		TBL_TRAKT,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktAssTreeExists(CTransaction_tree_assort &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tassort_tree_id=%d and tassort_tree_trakt_id=%d and tassort_tree_dcls=%.0f"),
		TBL_TRAKT_TREES_ASSORT,
		rec.getTreeID(),
		rec.getTraktID(),
		rec.getDCLS());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktAssTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tassort_tree_trakt_id=%d"),
		TBL_TRAKT_TREES_ASSORT,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktAssExists(int trakt_id,int trakt_data_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_trakt_id=%d and tass_trakt_data_id=%d"),
		TBL_TRAKT_SPC_ASS,
		trakt_id,
		trakt_data_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktAssExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_trakt_id=%d"),
		TBL_TRAKT_SPC_ASS,
		trakt_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktAssExists(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_assort_id=%d and tass_trakt_id=%d and tass_trakt_data_id=%d and tass_data_type=%d"),
		TBL_TRAKT_SPC_ASS,
		rec.getTAssID(),
		rec.getTAssTraktID(),
		rec.getTAssTraktDataID(),
		rec.getTAssTraktDataType());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktTransExists(int trakt_data_id, int trakt_id, int nFrom_id, int nTo_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttrans_trakt_data_id=%d and ttrans_trakt_id=%d and ttrans_from_ass_id=%d and ttrans_to_ass_id=%d"),
		TBL_TRAKT_TRANS,
		trakt_data_id,
		trakt_id,
		nFrom_id,
		nTo_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktRotpostExists(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where rot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST,
		rec.getRotTraktID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::objectExists(CTransaction_elv_object &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where object_id=%d"),
		TBL_ELV_OBJECT,
		rec.getObjID_pk());
	return exists(sSQL);
}

BOOL CUMLandValueDB::objectExists(int obj_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where object_id=%d"),
		TBL_ELV_OBJECT,
		obj_id);
	return exists(sSQL);
}

//new #3385 tar �ven h�nsyn till status p� objektet
BOOL CUMLandValueDB::objectExists_status(int obj_id, int obj_status)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where object_id=%d and object_status=%d"),
		TBL_ELV_OBJECT,
		obj_id,
		obj_status);
	return exists(sSQL);
}

//new #3385 tittar om det finns n�gra object satta som p�g�ende
BOOL CUMLandValueDB::isAnyObjectOngoing()
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where object_status=%d"),
		TBL_ELV_OBJECT,
		0);
	return exists(sSQL);
}

//new #3385 tittar om det finns n�gra object satta som slutf�rda
BOOL CUMLandValueDB::isAnyObjectFinished()
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where object_status=%d"),
		TBL_ELV_OBJECT,
		1);
	return exists(sSQL);
}


BOOL CUMLandValueDB::objectTemplExist(CTransaction_template &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_ELV_TEMPLATE,rec.getID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::objectDocumentTemplExist(CTransaction_template &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_ELV_DOC_TEMPLATE,rec.getID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::propExists(CTransaction_elv_properties & rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_id=%d and prop_object_id=%d"),
		TBL_ELV_PROP,
		rec.getPropID_pk(),
		rec.getPropObjectID_pk());
	return exists(sSQL);
}

BOOL CUMLandValueDB::propExists(int prop_id,int obj_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_id=%d and prop_object_id=%d"),
		TBL_ELV_PROP,
		prop_id,
		obj_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::propExists(int obj_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_object_id=%d"),
		TBL_ELV_PROP,
		obj_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::propStatusExists(int id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_ELV_PROP_STATUS,
		id);
	return exists(sSQL);
}


BOOL CUMLandValueDB::cruiseExists(CTransaction_elv_cruise& rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ecru_id=%ld and ecru_object_id=%d and ecru_prop_id=%d"),TBL_ELV_CRUISE,rec.getECruID_pk(),rec.getECruObjectID_pk(),rec.getECruPropID_pk());
	return exists(sSQL);
}

BOOL CUMLandValueDB::cruiseExists(long ecru_id,int prop_id,int obj_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ecru_id=%ld and ecru_object_id=%d and ecru_prop_id=%d"),
		TBL_ELV_CRUISE,
		ecru_id,
		obj_id,
		prop_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::cruiseExists(int prop_id,int obj_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ecru_object_id=%d and ecru_prop_id=%d"),
		TBL_ELV_CRUISE,
		obj_id,
		prop_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::evaluationExists(CTransaction_eval_evaluation& rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where eval_id=%d and eval_object_id=%d and eval_prop_id=%d"),
		TBL_ELV_EVALUATION,
		rec.getEValID_pk(),
		rec.getEValObjID_pk(),
		rec.getEValPropID_pk());
	return exists(sSQL);
}


BOOL CUMLandValueDB::evaluationExists(int eval_id,int obj_id,int prop_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where eval_id=%d and eval_object_id=%d and eval_prop_id=%d"),
		TBL_ELV_EVALUATION,
		eval_id,
		obj_id,
		prop_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::evaluationExists(int obj_id,int prop_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where eval_object_id=%d and eval_prop_id=%d"),
		TBL_ELV_EVALUATION,
		obj_id,
		prop_id);
	return exists(sSQL);
}

BOOL CUMLandValueDB::cruiseRandTreesExists(CTransaction_elv_cruise_randtrees& rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where erand_id=%d and erand_object_id=%d and erand_prop_id=%d and erand_ecru_id=%d"),
		TBL_ELV_CRUISE_RANDTREES,
		rec.getEValRandID_pk(),
		rec.getEValRandObjectID_pk(),
		rec.getEValRandPropID_pk(),
		rec.getEValRandEvaluID_pk());
	
	return exists(sSQL);
}

BOOL CUMLandValueDB::propLogBookExists(CTransaction_elv_properties_logbook & rec)
{
	CString sSQL;

	sSQL.Format(_T("select 1 from %s where plog_id=%d and plog_object_id=%d and plog_log_date='%s'"),
		TBL_ELV_PROP_LOG,
		rec.getPropLogBookID_pk(),
		rec.getPropLogBookObjectID_pk(),
		rec.getPropLogBookDate());
	return exists(sSQL);
}

BOOL CUMLandValueDB::propOtherCompExists(CTransaction_elv_properties_other_comp& rec)
{
	CString sSQL;

	sSQL.Format(_T("select 1 from %s where ocomp_id =%d and ocomp_object_id=%d and ocomp_prop_id=%d"),
		TBL_ELV_PROP_OTHER_COMP,
		rec.getPropOtherCompID_pk(),
	rec.getPropOtherCompObjectID_pk(),
	rec.getPropOtherCompPropID_pk());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktMiscDataExists(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tprl_trakt_id=%d"),
		TBL_TRAKT_MISC_DATA,
		rec.getTSetTraktID());
	return exists(sSQL);
}

BOOL CUMLandValueDB::traktSetSpcExists(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tsetspc_id=%d and tsetspc_tdata_id=%d and tsetspc_tdata_trakt_id=%d and tsetspc_data_type=%d"),
		TBL_TRAKT_SET_SPC,
		rec.getTSetspcID(),
		rec.getTSetspcDataID(),
		rec.getTSetspcTraktID(),
		rec.getTSetspcDataType());
	return exists(sSQL);
}

BOOL CUMLandValueDB::updateP30SpeciesInfo(int nSpecId,int nP30SpecId,int nObjId)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (objectExists(obj_id))
		//{
		sSQL.Format(_T("update %s set p30_spec=:1 where spc_id=:2 and obj_id=:3"),
						TBL_P30SPECIES_OBJECT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= nP30SpecId;
			m_saCommand.Param(2).setAsLong()		= nSpecId;
			m_saCommand.Param(3).setAsLong()	= nObjId;
			m_saCommand.Execute();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

//#HMS-94 20220920 spara p30 tr�dslagsinfo f�r ett object
BOOL CUMLandValueDB::createP30SpeciesInfo(vecTransactionSpecies &vec,int nObj_Id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	SADateTime saDateTime;
	
	/*
const LPCTSTR table_ElvP30Spec_Obj= _T("CREATE TABLE dbo.%s (")
									_T("spc_id int NOT NULL,")
									_T("obj_id int NOT NULL,")
									_T("spc_name NVARCHAR(45),")
									_T("notes NVARCHAR(255),")
									_T("p30_spec int,")
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
*/
	try
	{
		if (vec.size()>0)
		{
			for(int i=0;i<vec.size();i++)
			{
				sSQL.Format(_T("insert into %s (spc_id,obj_id,spc_name,notes,p30_spec,created)")
					_T("values(:1,:2,:3,:4,:5,GETDATE())"),TBL_P30SPECIES_OBJECT);

				m_saCommand.setCommandText((SAString)sSQL);

				m_saCommand.Param(1).setAsLong()	= vec[i].getSpcID();
				m_saCommand.Param(2).setAsLong()	= nObj_Id;
				m_saCommand.Param(3).setAsString()	= vec[i].getSpcName();
				m_saCommand.Param(4).setAsString()	= vec[i].getNotes();
				m_saCommand.Param(5).setAsLong()	= vec[i].getP30SpcID();
				m_saCommand.Execute();
			}

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

//#HMS-94 20220920 h�mta p30 tr�dslagsinfo f�r ett object
BOOL CUMLandValueDB::getP30SpeciesInfo(vecTransactionSpecies &vec,int nObj_Id)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where obj_id=%d order by spc_id"),TBL_P30SPECIES_OBJECT,nObj_Id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			vec.push_back(CTransaction_species(-1,
				m_saCommand.Field(_T("spc_id")).asLong(),
				m_saCommand.Field(_T("p30_spec")).asLong(),
				m_saCommand.Field(_T("spc_name")).asString(),
				m_saCommand.Field(_T("notes")).asString(),
				getUserName()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// PUBLIC
// External database items.
BOOL CUMLandValueDB::getSpecies(vecTransactionSpecies &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			vec.push_back(CTransaction_species(-1,
				m_saCommand.Field(1).asLong(),
				m_saCommand.Field(_T("p30_spec")).asLong(),
				m_saCommand.Field(2).asString(),
				m_saCommand.Field(3).asString(),
				getUserName()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getPricelists(vecTransactionPricelist &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),TBL_PRICELISTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_pricelist(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asString(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLongChar(),
													m_saCommand.Field(5).asString(),
													convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

//Lagt till function f�r att plocka alla kostnader fr�n kostnadstabellen oavsett typ
// Bug #2367 20101011 J�
BOOL CUMLandValueDB::getAllCostTmpls(vecTransaction_costtempl &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getCosts(vecTransaction_costtempl &vec,int tmpl_type)
{
	CString sSQL;
	try
	{
		vec.clear();

		if (tmpl_type > -1)
		{
			sSQL.Format(_T("select * from %s where cost_type_of=:1"),
				TBL_COSTS_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= tmpl_type;
		}
		else
		{
			sSQL.Format(_T("select * from %s"),TBL_COSTS_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);
		}
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asLong(),
												m_saCommand.Field(4).asLongChar(),
												m_saCommand.Field(5).asLongChar(),
												m_saCommand.Field(6).asString(),
												convertSADateTime(saDateTime)));
		}

	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

std::vector<searchResult> CUMLandValueDB::getSearchList() {

	std::vector<searchResult> results;
	searchResult r;
	CString sSQL;

	sSQL = L"SELECT elv_object_table.object_id AS oID, elv_object_table.object_name AS oName,"
		L" fst_contacts_table.id AS cID, fst_contacts_table.name_of as cName,"
		L" fst_property_table.id AS pID, fst_property_table.prop_name as pName,"
		L" fst_property_table.block_number AS pBlockNumber, fst_property_table.unit_number AS pUnitNumber"
		L" FROM fst_contacts_table,elv_object_table,fst_property_table,fst_prop_owner_table,elv_properties_table"
		L" WHERE fst_prop_owner_table.prop_id=fst_property_table.id"
		L" AND fst_prop_owner_table.contact_id=fst_contacts_table.id"
		L" AND elv_properties_table.prop_id=fst_property_table.id"
		L" AND elv_properties_table.prop_object_id=elv_object_table.object_id";

	sSQL = L"SELECT elv_object_table.object_id AS oID, elv_object_table.object_name AS oName,"
		L" fst_contacts_table.id AS cID, fst_contacts_table.name_of as cName,"
		L" fst_property_table.id AS pID, fst_property_table.prop_name as pName,"
		L" fst_property_table.block_number AS pBlockNumber, fst_property_table.unit_number AS pUnitNumber"
		L" FROM fst_contacts_table"
		L" FULL JOIN fst_prop_owner_table"
		L" ON fst_contacts_table.id=fst_prop_owner_table.contact_id"
		L" FULL JOIN fst_property_table"
		L" ON fst_prop_owner_table.prop_id=fst_property_table.id"
		L" FULL JOIN elv_properties_table"
		L" ON fst_property_table.id=elv_properties_table.prop_id"
		L" FULL JOIN elv_object_table"
		L" ON elv_properties_table.prop_object_id=elv_object_table.object_id";


	try
	{	
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			r.objectName = m_saCommand.Field("oName").asString();
			r.objectId = m_saCommand.Field("oID").asShort();
			r.ownerName = m_saCommand.Field("cName").asString();
			r.ownerId = m_saCommand.Field("cID").asShort();
			r.propertyId = m_saCommand.Field("pID").asShort();

			r.propertyName = m_saCommand.Field("pName").asString();
			if(r.propertyName!=L"") {
				r.propertyName += L" ";
				r.propertyName += m_saCommand.Field("pBlockNumber").asString();
				r.propertyName += L":";
				r.propertyName += m_saCommand.Field("pUnitNumber").asString();
			}

			results.push_back(r);

		}
		
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
	}

	return results;
}


BOOL CUMLandValueDB::getProperties(CString sql,vecTransactionProperty &vec)
{
	CString sSQL = sql;
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asString(),
																					 m_saCommand.Field(3).asString(),
																					 m_saCommand.Field(4).asString(),
																					 m_saCommand.Field(5).asString(),
																					 m_saCommand.Field(6).asString(),
																					 m_saCommand.Field(7).asString(),
																					 m_saCommand.Field(8).asString(),
																					 m_saCommand.Field(9).asString(),
																					 m_saCommand.Field(10).asString(),
																					 m_saCommand.Field(11).asString(),
																					 m_saCommand.Field(12).asDouble(),
																					 m_saCommand.Field(13).asDouble(),
																					 m_saCommand.Field(14).asString(),
																					 m_saCommand.Field(15).asString(),
																					 m_saCommand.Field(16).asString(),
																					 m_saCommand.Field(17).asString(),
																					 m_saCommand.Field(18).asString(),
																					 m_saCommand.Field("prop_coord").asString()
																					 ));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// HANDLE OBJECTS; 2008-01-24 P�D

BOOL CUMLandValueDB::getObjectIndex(vecInt& vec, int object_status /*= -1*/)
{
	CString sSQL;
	try
	{
		vec.clear();
		//#3385 lagt till parameter f�r att v�lj ut p� objekt_status, 0=p�g�ende,1=slutf�rda,2=alla
		if(object_status == 0 || object_status == 1)
		{
			sSQL.Format(_T("select object_id from %s where object_status=%d "),	
			TBL_ELV_OBJECT, object_status);
		}
		else
		{
		sSQL.Format(_T("select object_id from %s"),	
			TBL_ELV_OBJECT);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back((int)m_saCommand.Field(_T("object_id")).asLong());
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObject(int obj_id,CTransaction_elv_object& rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where object_id=:1"),
			TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(48).asDateTime();
			rec = CTransaction_elv_object(m_saCommand.Field(1).asLong(),
											m_saCommand.Field(2).asString(),
											m_saCommand.Field(3).asString(),
											m_saCommand.Field(4).asString(),
											m_saCommand.Field(5).asString(),
											m_saCommand.Field(6).asString(),
											m_saCommand.Field(7).asDouble(),
											m_saCommand.Field(8).asLong(),
											m_saCommand.Field(9).asLong(),
											m_saCommand.Field(10).asString(),
											m_saCommand.Field(11).asDouble(),
											m_saCommand.Field(12).asDouble(),
											m_saCommand.Field(13).asDouble(),
											m_saCommand.Field(14).asDouble(),
											m_saCommand.Field("object_parallel_width").asDouble(),
											m_saCommand.Field(15).asLong(),
											m_saCommand.Field(16).asDouble(),
											m_saCommand.Field(17).asDouble(),
											m_saCommand.Field(18).asDouble(),
											m_saCommand.Field(19).asDouble(),
											m_saCommand.Field(20).asDouble(),
											m_saCommand.Field(21).asDouble(),
											m_saCommand.Field("object_perc_of_price_base").asDouble(),	// #4420 20150803 J�
											m_saCommand.Field(22).asLong(),
											m_saCommand.Field(23).asString(),
											m_saCommand.Field(24).asDouble(),
											m_saCommand.Field(25).asDouble(),
											m_saCommand.Field(26).asString(),
											m_saCommand.Field(27).asLong(),
											m_saCommand.Field(28).asLongChar(),
											m_saCommand.Field(29).asString(),
											m_saCommand.Field(30).asLong(),
											m_saCommand.Field(31).asLongChar(),
											m_saCommand.Field(32).asLong(),
											m_saCommand.Field(33).asDouble(),
											m_saCommand.Field(34).asString(),
											m_saCommand.Field(35).asString(),
											m_saCommand.Field(36).asLong(),
											m_saCommand.Field(37).asLongChar(),
											m_saCommand.Field(38).asString(),
											m_saCommand.Field(39).asLong(),
											m_saCommand.Field(40).asLongChar(),
											m_saCommand.Field(41).asShort(),
											m_saCommand.Field(42).asShort(),
											m_saCommand.Field(43).asShort(),
											m_saCommand.Field(44).asShort(),
											m_saCommand.Field(45).asString(),
											m_saCommand.Field(47).asLongChar(),
											m_saCommand.Field(46).asLong(),
											m_saCommand.Field(_T("object_logo_id")).asLong(),	// Added 100225 p�d
											m_saCommand.Field(_T("object_use_grot")).asShort(),
											m_saCommand.Field(_T("object_start_date")).asString(),
											m_saCommand.Field(_T("object_recalc_by")).asDouble(),
											m_saCommand.Field(_T("object_length")).asDouble(),
											convertSADateTime(saDateTime),
											m_saCommand.Field(_T("object_status")).asLong());		//new #3385
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObject(vecTransaction_elv_object &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(48).asDateTime();
			vec.push_back(CTransaction_elv_object(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asString(),
													m_saCommand.Field(3).asString(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asString(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asLong(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asString(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field("object_parallel_width").asDouble(),
													m_saCommand.Field(15).asLong(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asDouble(),
													m_saCommand.Field(20).asDouble(),
													m_saCommand.Field(21).asDouble(),
													m_saCommand.Field("object_perc_of_price_base").asDouble(), // #4420 20150803 J�													
													m_saCommand.Field(22).asLong(),
													m_saCommand.Field(23).asString(),
													m_saCommand.Field(24).asDouble(),
													m_saCommand.Field(25).asDouble(),
													m_saCommand.Field(26).asString(),
													m_saCommand.Field(27).asLong(),
													m_saCommand.Field(28).asLongChar(),
													m_saCommand.Field(29).asString(),
													m_saCommand.Field(30).asLong(),
													m_saCommand.Field(31).asLongChar(),
													m_saCommand.Field(32).asLong(),
													m_saCommand.Field(33).asDouble(),
													m_saCommand.Field(34).asString(),
													m_saCommand.Field(35).asString(),
													m_saCommand.Field(36).asLong(),
													m_saCommand.Field(37).asLongChar(),
													m_saCommand.Field(38).asString(),
													m_saCommand.Field(39).asLong(),
													m_saCommand.Field(40).asLongChar(),
													m_saCommand.Field(41).asShort(),
													m_saCommand.Field(42).asShort(),
													m_saCommand.Field(43).asShort(),
													m_saCommand.Field(44).asShort(),
													m_saCommand.Field(45).asString(),
													m_saCommand.Field(47).asLongChar(),
													m_saCommand.Field(46).asLong(),
													m_saCommand.Field(_T("object_logo_id")).asLong(),
													m_saCommand.Field(_T("object_use_grot")).asShort(),
													m_saCommand.Field(_T("object_start_date")).asString(),
													m_saCommand.Field(_T("object_recalc_by")).asDouble(),
													m_saCommand.Field(_T("object_length")).asDouble(),
													convertSADateTime(saDateTime),
													m_saCommand.Field(_T("object_status")).asLong()));		//new #3385
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

//new #3385
BOOL CUMLandValueDB::getObjectWithStatus(vecTransaction_elv_object &vec, int status)
{
	CString sSQL;
	try
	{
		vec.clear();

		if(status == 0 || status == 1)
			sSQL.Format(_T("select * from %s where object_status=%d"),TBL_ELV_OBJECT,status);
		else
			sSQL.Format(_T("select * from %s"),TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(48).asDateTime();
			vec.push_back(CTransaction_elv_object(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asString(),
													m_saCommand.Field(3).asString(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asString(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asLong(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asString(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field("object_parallel_width").asDouble(),
													m_saCommand.Field(15).asLong(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asDouble(),
													m_saCommand.Field(20).asDouble(),
													m_saCommand.Field(21).asDouble(),
													m_saCommand.Field("object_perc_of_price_base").asDouble(), // #4420 20150803 J�		
													m_saCommand.Field(22).asLong(),
													m_saCommand.Field(23).asString(),
													m_saCommand.Field(24).asDouble(),
													m_saCommand.Field(25).asDouble(),
													m_saCommand.Field(26).asString(),
													m_saCommand.Field(27).asLong(),
													m_saCommand.Field(28).asLongChar(),
													m_saCommand.Field(29).asString(),
													m_saCommand.Field(30).asLong(),
													m_saCommand.Field(31).asLongChar(),
													m_saCommand.Field(32).asLong(),
													m_saCommand.Field(33).asDouble(),
													m_saCommand.Field(34).asString(),
													m_saCommand.Field(35).asString(),
													m_saCommand.Field(36).asLong(),
													m_saCommand.Field(37).asLongChar(),
													m_saCommand.Field(38).asString(),
													m_saCommand.Field(39).asLong(),
													m_saCommand.Field(40).asLongChar(),
													m_saCommand.Field(41).asShort(),
													m_saCommand.Field(42).asShort(),
													m_saCommand.Field(43).asShort(),
													m_saCommand.Field(44).asShort(),
													m_saCommand.Field(45).asString(),
													m_saCommand.Field(47).asLongChar(),
													m_saCommand.Field(46).asLong(),
													m_saCommand.Field(_T("object_logo_id")).asLong(),
													m_saCommand.Field(_T("object_use_grot")).asShort(),
													m_saCommand.Field(_T("object_start_date")).asString(),
													m_saCommand.Field(_T("object_recalc_by")).asDouble(),
													m_saCommand.Field(_T("object_length")).asDouble(),
													convertSADateTime(saDateTime),
													m_saCommand.Field(_T("object_status")).asLong()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::addObject(CTransaction_elv_object &rec)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	SADateTime saDateTime;
	
	try
	{
		if (!objectExists(rec))
		{
			sSQL.Format(_T("insert into %s (object_errand_num,object_name,object_id_number,object_littra,object_growth_area,")
									_T("object_transport_dist,object_type_infring,object_width_present1,object_width_present2,object_width_added1,object_width_added2,")
									_T("object_type_of_net,object_price_div_1,object_price_div_2,object_price_base,object_max_percent,object_procent,object_VAT,")
									_T("object_use_norm_id,object_use_norm,object_take_care_of,object_corr_factor,object_contractor_id,object_dcls,object_extra_info,")
									_T("object_created_by,object_return_id,object_logo_id,object_notes,object_length,object_start_date,object_recalc_by,object_status,object_perc_of_price_base,created) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,")
									_T(":20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,GETDATE())"),TBL_ELV_OBJECT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getObjErrandNum();
			m_saCommand.Param(2).setAsString()	= rec.getObjectName();
			m_saCommand.Param(3).setAsString()	= rec.getObjIDNumber();
			m_saCommand.Param(4).setAsString()	= rec.getObjLittra();
			m_saCommand.Param(5).setAsString()	= rec.getObjGrowthArea();
			m_saCommand.Param(6).setAsDouble()	= rec.getObjTransportDist();
			if (rec.getObjTypeOfInfring().IsEmpty() || rec.getObjTypeOfInfring() == L" ")			
				m_saCommand.Param(7).setAsString()	= L"0";
			else
				m_saCommand.Param(7).setAsString()	= rec.getObjTypeOfInfring();
			m_saCommand.Param(8).setAsDouble()	= rec.getObjPresentWidth1();
			m_saCommand.Param(9).setAsDouble()	= rec.getObjPresentWidth2();
			m_saCommand.Param(10).setAsDouble()	= rec.getObjAddedWidth1();
			m_saCommand.Param(11).setAsDouble()	= rec.getObjAddedWidth2();
			m_saCommand.Param(12).setAsLong()	= rec.getObjTypeOfNet();
			m_saCommand.Param(13).setAsDouble()	= rec.getObjPriceDiv1();
			m_saCommand.Param(14).setAsDouble()	= rec.getObjPriceDiv2();
			m_saCommand.Param(15).setAsDouble()	= rec.getObjPriceBase();
			m_saCommand.Param(16).setAsDouble()	= rec.getObjMaxPercent();
			m_saCommand.Param(17).setAsDouble()	= rec.getObjPercent();
			m_saCommand.Param(18).setAsDouble()	= rec.getObjVAT();
			m_saCommand.Param(19).setAsLong()	= rec.getObjUseNormID();
			m_saCommand.Param(20).setAsString()	= rec.getObjUseNorm();
			m_saCommand.Param(21).setAsDouble()	= rec.getObjTakeCareOfPerc();
			m_saCommand.Param(22).setAsDouble()	= rec.getObjCorrFactor();
			m_saCommand.Param(23).setAsLong()	= rec.getObjContractorID();
			m_saCommand.Param(24).setAsDouble()	= rec.getObjDCLS();
			m_saCommand.Param(25).setAsString()	= rec.getObjExtraInfo();
			m_saCommand.Param(26).setAsString()	= rec.getObjCreatedBy();
			m_saCommand.Param(27).setAsLong()	= rec.getObjReturnAddressID();
			m_saCommand.Param(28).setAsLong()	= rec.getObjLogoID();
			m_saCommand.Param(29).setAsLongChar()= rec.getObjNotes();
			m_saCommand.Param(30).setAsDouble()	= rec.getObjLength();
			m_saCommand.Param(31).setAsString()	= rec.getObjStartDate();
			m_saCommand.Param(32).setAsDouble()	= rec.getObjRecalcBy();
			m_saCommand.Param(33).setAsShort() = rec.getObjStatus();
			m_saCommand.Param(34).setAsDouble()	= rec.getObjPercentOfPriceBase(); // Procent av prisbasbelopp #4420 20150803 J�
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObject(CTransaction_elv_object &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (objectExists(rec))
		//{
			sSQL.Format(_T("update %s set object_errand_num=:1,object_name=:2,object_id_number=:3,object_littra=:4,")
									_T("object_transport_dist=:5,object_width_present1=:6,object_width_present2=:7,object_width_added1=:8,object_width_added2=:9,")
									_T("object_price_div_1=:10,object_price_div_2=:11,object_price_base=:12,object_max_percent=:13,object_procent=:14,object_VAT=:15,")
									_T("object_use_norm_id=:16,object_use_norm=:17,object_take_care_of=:18,object_corr_factor=:19,object_contractor_id=:20,object_dcls=:21,")
									_T("object_extra_info=:22,object_created_by=:23,object_return_id=:24,object_logo_id=:25,object_notes=:26,object_start_date=:27,object_recalc_by=:28,object_length=:29,object_perc_of_price_base=:30 where object_id=:31"), //#4520 Tagit bort ", created=GETDATE()" s� att inte skapande datum uppdateras
									TBL_ELV_OBJECT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getObjErrandNum();
			m_saCommand.Param(2).setAsString()	= rec.getObjectName();
			m_saCommand.Param(3).setAsString()	= rec.getObjIDNumber();
			m_saCommand.Param(4).setAsString()	= rec.getObjLittra();
			m_saCommand.Param(5).setAsDouble()	= rec.getObjTransportDist();
			m_saCommand.Param(6).setAsDouble()	= rec.getObjPresentWidth1();
			m_saCommand.Param(7).setAsDouble()	= rec.getObjPresentWidth2();
			m_saCommand.Param(8).setAsDouble()	= rec.getObjAddedWidth1();
			m_saCommand.Param(9).setAsDouble()	= rec.getObjAddedWidth2();
			//m_saCommand.Param(10).setAsLong()	= rec.getObjTypeOfNet(); NOT USED; 091110 p�d
			m_saCommand.Param(10).setAsDouble()	= rec.getObjPriceDiv1();
			m_saCommand.Param(11).setAsDouble()	= rec.getObjPriceDiv2();
			m_saCommand.Param(12).setAsDouble()	= rec.getObjPriceBase();
			m_saCommand.Param(13).setAsDouble()	= rec.getObjMaxPercent();
			m_saCommand.Param(14).setAsDouble()	= rec.getObjPercent();
			m_saCommand.Param(15).setAsDouble()	= rec.getObjVAT();
			m_saCommand.Param(16).setAsLong()	= rec.getObjUseNormID();
			m_saCommand.Param(17).setAsString()	= rec.getObjUseNorm();
			m_saCommand.Param(18).setAsDouble()	= rec.getObjTakeCareOfPerc();
			m_saCommand.Param(19).setAsDouble()	= rec.getObjCorrFactor();
			m_saCommand.Param(20).setAsLong()	= rec.getObjContractorID();
			m_saCommand.Param(21).setAsDouble()	= rec.getObjDCLS();
			m_saCommand.Param(22).setAsString()	= rec.getObjExtraInfo();
			m_saCommand.Param(23).setAsString()	= rec.getObjCreatedBy();
			m_saCommand.Param(24).setAsLong()	= rec.getObjReturnAddressID();
			m_saCommand.Param(25).setAsLong()	= rec.getObjLogoID();
			m_saCommand.Param(26).setAsLongChar()= rec.getObjNotes();
			m_saCommand.Param(27).setAsString()	= rec.getObjStartDate();
			m_saCommand.Param(28).setAsDouble()	= rec.getObjRecalcBy();
			m_saCommand.Param(29).setAsDouble()	= rec.getObjLength();
			m_saCommand.Param(30).setAsDouble()	= rec.getObjPercentOfPriceBase(); // Procent av prisbasbelopp #4420 20150803 J�
			m_saCommand.Param(31).setAsLong()	= rec.getObjID_pk();
			m_saCommand.Execute();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::delObject(int obj_id)
{
	CString sSQL;
	try
	{
		if (objectExists(obj_id))
		{
			sSQL.Format(_T("delete from %s where object_id=:1"),
									TBL_ELV_OBJECT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= obj_id;
			m_saCommand.Execute();
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

// Added 070903 p�d
int CUMLandValueDB::getLastObjectIdentity(void)
{
	return last_identity(TBL_ELV_OBJECT,_T("object_id"));
}

short CUMLandValueDB::isObject(CTransaction_elv_object& rec,CString& change_by,CString& date_done)
{
	int nDateDiff = 0;
	CString sSQL;
	short nReturn = 1;	// 1 = OK
	try
	{

		if (objectExists(rec))
		{
			sSQL.Format(_T("select DATEDIFF(millisecond,\'%s\',created) as \'datediff\',object_created_by as \'created_by\',created from %s where object_id=%d"),
				rec.getObjDate(),
				TBL_ELV_OBJECT,
				rec.getObjID_pk());

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				nDateDiff = m_saCommand.Field(_T("datediff")).asLong();
				change_by = m_saCommand.Field(_T("created_by")).asString();
				SADateTime saDateTime = m_saCommand.Field(_T("created")).asDateTime();
				date_done = convertSADateTime(saDateTime);
			}

			if (nDateDiff <= 0) nReturn = 1; // OK
			else if (nDateDiff > 0) nReturn = 0; // NOT OK, post on DB is newer than yours; 090211 p�d
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return -1;	// NOT OK
	}

	return nReturn;
}

BOOL CUMLandValueDB::isObjectInDB(int obj_id)
{
	return objectExists(obj_id);
}

//new #3385
BOOL CUMLandValueDB::isObjectInDB_status(int obj_id, int obj_status)
{
	return objectExists_status(obj_id,obj_status);
}


// Only save pricelist data to Object table; 080331 p�d
BOOL CUMLandValueDB::addObject_prl(int obj_id,LPCTSTR prl_name,int prl_typeof,LPCTSTR prl_xml)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!objectExists(obj_id))
		{
			sSQL.Format(_T("insert into %s (object_pricelist_name,object_pricelist_typeof,object_pricelist_xml) values(:1,:2,:3)"),
							TBL_ELV_OBJECT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= prl_name;
			m_saCommand.Param(2).setAsLong()		= prl_typeof;
			m_saCommand.Param(3).setAsLongChar()	= prl_xml;

			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObject_prl(int obj_id,LPCTSTR prl_name,int prl_typeof,LPCTSTR prl_xml)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (objectExists(obj_id))
		//{
			sSQL.Format(_T("update %s set object_pricelist_name=:1,object_pricelist_typeof=:2,object_pricelist_xml=:3 where object_id=:4"),
						TBL_ELV_OBJECT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= prl_name;
			m_saCommand.Param(2).setAsLong()		= prl_typeof;
			m_saCommand.Param(3).setAsLongChar()	= prl_xml;
			m_saCommand.Param(4).setAsLong()		= obj_id;

			m_saCommand.Execute();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

// Only save cost data to Object table; 080331 p�d
BOOL CUMLandValueDB::addObject_cost(int obj_id,LPCTSTR cost_name,int cost_typeof,LPCTSTR cost_xml)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!objectExists(obj_id))
		{
			sSQL.Format(_T("insert into %s (object_costst_name,object_costst_typeof,object_costs_xml) values(:1,:2,:3)"),
							TBL_ELV_OBJECT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= cost_name;
			m_saCommand.Param(2).setAsLong()		= cost_typeof;
			m_saCommand.Param(3).setAsLongChar()	= cost_xml;

			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObject_cost(int obj_id,LPCTSTR cost_name,int cost_typeof,LPCTSTR cost_xml)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (objectExists(obj_id))
		//{
			sSQL.Format(_T("update %s set object_costst_name=:1,object_costst_typeof=:2,object_costs_xml=:3 where object_id=:4"),
							TBL_ELV_OBJECT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= cost_name;
			m_saCommand.Param(2).setAsLong()	= cost_typeof;
			m_saCommand.Param(3).setAsLongChar()= cost_xml;	
			m_saCommand.Param(4).setAsLong()	= obj_id;

			m_saCommand.Execute();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObject_price_div(int obj_id,double price_div_1,double price_div_2)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (objectExists(obj_id))
		//{
			sSQL.Format(_T("update %s set object_price_div_1=:1,object_price_div_2=:2 where object_id=:3"),
							TBL_ELV_OBJECT);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsDouble()	= price_div_1;
			m_saCommand.Param(2).setAsDouble()	= price_div_2;	
			m_saCommand.Param(3).setAsLong()	= obj_id;

			m_saCommand.Execute();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObject_typeof_infr(int obj_id,LPCTSTR typeof_infr)
{

	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_type_infring=:1 where object_id=:2"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		if (typeof_infr == L"" || typeof_infr == L" ")
			m_saCommand.Param(1).setAsString()	= L"0";	
		else
			m_saCommand.Param(1).setAsString()	= typeof_infr;	
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_norm(int obj_id,LPCTSTR norm_name,int norm_typeof)
{
	CString sSQL;
	try
	{
			sSQL.Format(_T("update %s set object_use_norm=:1,object_use_norm_id=:2 where object_id=:3"),
							TBL_ELV_OBJECT);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= norm_name;
			m_saCommand.Param(2).setAsLong()		= norm_typeof;
			m_saCommand.Param(3).setAsLong()		= obj_id;

			m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_dcls(int obj_id,double dcls)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_dcls=:1 where object_id=:2"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= dcls;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_extra_info(int obj_id,LPCTSTR extra_info)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_extra_info=:1 where object_id=:2"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= extra_info;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_recalc_by(int obj_id,double percent)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_recalc_by=:1 where object_id=:2"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= percent;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// "Objektets totala l�ngd"; 100909 p�d
BOOL CUMLandValueDB::updObject_length(int obj_id,double obj_length)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_length=:1 where object_id=:2"),TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= obj_length;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_curr_width(int obj_id,double preset_width)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_width_present1=:1 where object_id=:2"),TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= preset_width;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_added_widths(int obj_id,double added_width1,double added_width2)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_width_added1=:1,object_width_added2=:2 where object_id=:3"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= added_width1;
		m_saCommand.Param(2).setAsDouble()	= added_width2;
		m_saCommand.Param(3).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_parallel_width(int obj_id,double parallel_width)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_parallel_width=:1 where object_id=:2"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= parallel_width;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_p30(int obj_id,LPCTSTR p30_name,int p30_typeof,LPCTSTR p30_xml)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_p30_name=:1,object_p30_typeof=:2,object_p30_xml=:3 where object_id=:4"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= p30_name;
		m_saCommand.Param(2).setAsLong()		= p30_typeof;
		m_saCommand.Param(3).setAsLongChar()		= p30_xml;
		m_saCommand.Param(4).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_hcost(int obj_id,LPCTSTR hcost_name,int hcost_typeof,LPCTSTR hcost_xml)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_hcost_name=:1,object_hcost_typeof=:2,object_hcost_xml=:3	where object_id=:4"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= hcost_name;
		m_saCommand.Param(2).setAsLong()	= hcost_typeof;
		m_saCommand.Param(3).setAsLongChar()	= hcost_xml;
		m_saCommand.Param(4).setAsLong()	= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_growth_area(int obj_id,LPCTSTR growtharea)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_growth_area=:1 where object_id=:2"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= growtharea;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_latitude_altitude(int obj_id,int lat,int alt)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_latitude=:1,object_altitude=:2 where object_id=:3"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= lat;
		m_saCommand.Param(2).setAsLong()		= alt;
		m_saCommand.Param(3).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_include_exclude(int obj_id,int vat,int voluntary_deal,int higher_costs,int other_comp,int grot)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_use_vat=:1,object_use_voluntary_deal=:2,object_use_higher_costs=:3,object_use_other_comp=:4,object_use_grot=:5 where object_id=:6"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= vat;
		m_saCommand.Param(2).setAsShort()		= voluntary_deal;
		m_saCommand.Param(3).setAsShort()		= higher_costs;
		m_saCommand.Param(4).setAsShort()		= other_comp;
		m_saCommand.Param(5).setAsShort()		= grot;
		m_saCommand.Param(6).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_type_of_net(int obj_id,int type_of)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_type_of_net=:1 where object_id=:2"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= type_of;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}
																															// Procent av prisbasbelopp #4420 20150803 J�
BOOL CUMLandValueDB::updObject_price_max_percent(int obj_id,double price_base,double max_percent,double percent,double VAT,double fBindToPercentOfPriceBase)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_price_base=:1,object_max_percent=:2,object_procent=:3,object_VAT=:4,object_perc_of_price_base=:5 where object_id=:6"),
						TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= price_base;
		m_saCommand.Param(2).setAsDouble()	= max_percent;
		m_saCommand.Param(3).setAsDouble()	= percent;
		m_saCommand.Param(4).setAsDouble()	= VAT;
		m_saCommand.Param(5).setAsDouble()	= fBindToPercentOfPriceBase;
		m_saCommand.Param(6).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_vfall_break_factor(int obj_id,double vfall_break,double vfall_factor)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_vfall_break=:1,object_vfall_factor=:2 where object_id=:3"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= vfall_break;
		m_saCommand.Param(2).setAsDouble()	= vfall_factor;
		m_saCommand.Param(3).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_contractor_id(int obj_id,int id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_contractor_id=:1 where object_id=:2"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= id;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_retadr_id(int obj_id,int id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_return_id=:1 where object_id=:2"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= id;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObject_logo_id(int obj_id,int logo_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_logo_id=:1 where object_id=:2"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= logo_id;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

//new #3385
BOOL CUMLandValueDB::updObject_status(int obj_id,int status)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set object_status=:1 where object_id=:2"),
						TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= status;
		m_saCommand.Param(2).setAsLong()		= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}



// OBS! This function is SQL-server specific; 080204 p�d
int CUMLandValueDB::getLastObjectID(void)
{
	return last_identity(TBL_ELV_OBJECT,_T("object_id"));
}

BOOL CUMLandValueDB::matchPropertyInObjects(int obj_id,int prop_id,vecFuncObjProp &vec1,vecFuncCruiseProp &vec2)
{
	vecFuncCruiseProp vecCruiseProp1;
	vecFuncCruiseProp vecCruiseProp2;
	MATCH_CRUISE_PROP_STRUCT recCruiseProp1;
	MATCH_CRUISE_PROP_STRUCT recCruiseProp2;
	CString sPropName;
	CString sSQL,sUnit;
	try
	{
		vec1.clear();
		vec2.clear();

		//------------------------------------------------------------------------------------------------------------
		// Get information on which object(s) the property is included in; 101117 p�d
		//------------------------------------------------------------------------------------------------------------
		sSQL.Format(_T("select a.prop_object_id,b.prop_name,b.block_number,b.unit_number,c.object_name ")
								_T("from %s a,%s b,%s c ")
								_T("where prop_id=:1 and prop_object_id<>:2 and b.id=prop_id and c.object_id=a.prop_object_id"),
								 TBL_ELV_PROP,TBL_PROPERTY,TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= prop_id;
		m_saCommand.Param(2).setAsLong()	= obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec1.push_back(MATCH_OBJ_PROP_STRUCT(prop_id,
																	m_saCommand.Field(1).asLong(),
																	m_saCommand.Field(2).asString(),
																	m_saCommand.Field(3).asString(),
																	m_saCommand.Field(4).asString(),
																	m_saCommand.Field(5).asString()));
		}
		//------------------------------------------------------------------------------------------------------------
		// If the property is included in more than one object, we need to find out if there also are stands included
		// in more than one Object. Stand(s) included in more than one object can't be removed; 101117 p�d
		//------------------------------------------------------------------------------------------------------------
		if (vec1.size() > 0)
		{
			m_saCommand.Close();
			//---------------------------------------------------------------------------------
			// Find stands in property for active object; 101117 p�d
			//---------------------------------------------------------------------------------
			sSQL.Format(_T("select a.ecru_id,a.ecru_object_id,a.ecru_number,a.ecru_name ")
									_T("from %s a ")
									_T("where ecru_prop_id=:1 and ecru_object_id=:2"),
									 TBL_ELV_CRUISE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= prop_id;
			m_saCommand.Param(2).setAsLong()	= obj_id;
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				vecCruiseProp1.push_back(MATCH_CRUISE_PROP_STRUCT(m_saCommand.Field(1).asLong(),
																													m_saCommand.Field(2).asLong(),
																													m_saCommand.Field(3).asString(),
																													m_saCommand.Field(4).asString()));
			}
			//---------------------------------------------------------------------------------
			// Find stands in property for other objects; 101117 p�d
			//---------------------------------------------------------------------------------
			// Only need to do this, if there's stands for this property; 101117 p�d
			if (vecCruiseProp1.size() > 0)
			{
				m_saCommand.Close();
				sSQL.Format(_T("select a.ecru_id,a.ecru_object_id,a.ecru_number,a.ecru_name ")
										_T("from %s a ")
										_T("where ecru_prop_id=:1 and ecru_object_id<>:2"),
										 TBL_ELV_CRUISE);
				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsLong()	= prop_id;
				m_saCommand.Param(2).setAsLong()	= obj_id;
				m_saCommand.Execute();

				while(m_saCommand.FetchNext())
				{
					vecCruiseProp2.push_back(MATCH_CRUISE_PROP_STRUCT(m_saCommand.Field(1).asLong(),
																														m_saCommand.Field(2).asLong(),
																														m_saCommand.Field(3).asString(),
																														m_saCommand.Field(4).asString()));
				}
			}

			//---------------------------------------------------------------------------------
			// Try to match stands in Property to be removed, in active object, to
			// other Object(s) with stands included, for Property to be removed; 101117 p�d
			//---------------------------------------------------------------------------------
			if (vecCruiseProp1.size() > 0 && vecCruiseProp2.size() > 0)	// There are Stands for Other Object(s) and this Property; 101117 p�d
			{
				for (UINT i1 = 0;i1 < vecCruiseProp1.size();i1++)
				{
					recCruiseProp1 = vecCruiseProp1[i1];
					for (UINT i2 = 0;i2 < vecCruiseProp2.size();i2++)
					{
						recCruiseProp2 = vecCruiseProp2[i2];
						// Add Stand(s) that can't be removed; 101117 p�d
						if (recCruiseProp1.nStandId == recCruiseProp2.nStandId)
						{
							vec2.push_back(recCruiseProp2);
						}	// if (recCruiseProp1.nStandId == recCruiseProp2.nStandId)
					}	// for (UINT i2 = 0;i2 < vecCruiseProp2.size();i2++)
				}	// for (UINT i1 = 0;i1 < vecCruiseProp1.size();i1++)
			}	// if (vecCruiseProp1.size() > 0 && vecCruiseProp2.size())
		}

	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


// Handle properties; 080415 p�d
BOOL CUMLandValueDB::getProperties(int obj_id,vecTransaction_elv_properties &vec)
{
	CString sPropName;
	CString sSQL,sUnit;
	try
	{
		vec.clear();

		sSQL.Format(_T("select a.*, b.prop_name as bprop_name, b.block_number as bblock_number, b.unit_number as bunit_number, b.prop_number as bprop_number, b.county_name as bcounty_name, b.municipal_name as bmunicipal_name, b.obj_id as bobj_id, b.mottagarref as bmottagarref from %s a,%s b where prop_object_id=:1 and (a.prop_id=b.id)"),
								 TBL_ELV_PROP,TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			sUnit = m_saCommand.Field("bunit_number").asString();
			// Check if there's a UNIT in property; 090518 p�d
			if (!sUnit.IsEmpty())
			{
				sPropName.Format(_T("%s %s:%s"),
					m_saCommand.Field("bprop_name").asString(),
					m_saCommand.Field("bblock_number").asString(),
					sUnit);
			}
			else
			{
				sPropName.Format(_T("%s %s"),
					m_saCommand.Field("bprop_name").asString(),
					m_saCommand.Field("bblock_number").asString());
			}
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_elv_properties(m_saCommand.Field("prop_id").asLong(),
														m_saCommand.Field("prop_object_id").asLong(),
														m_saCommand.Field("bcounty_name").asString(),
														m_saCommand.Field("bmunicipal_name").asString(),
														sPropName, //m_saCommand.Field(3).asString(),
														m_saCommand.Field("bprop_number").asString(),
														m_saCommand.Field("prop_volume_m3sk").asDouble(),
														m_saCommand.Field("prop_areal").asDouble(),
														m_saCommand.Field("prop_numof_trees").asLong(),
														m_saCommand.Field("prop_numof_stands").asLong(),
														m_saCommand.Field("prop_wood_volume").asDouble(),
														m_saCommand.Field("prop_wood_value").asDouble(),
														m_saCommand.Field("prop_cost_value").asDouble(),
														m_saCommand.Field("prop_rotpost_value").asDouble(),
														m_saCommand.Field("prop_land_value").asDouble(),
														m_saCommand.Field("prop_early_cut_value").asDouble(),
														m_saCommand.Field("prop_storm_dry_value").asDouble(),
														m_saCommand.Field("prop_randtrees_value").asDouble(),
														m_saCommand.Field("prop_voluntary_deal_value").asDouble(),
														m_saCommand.Field("prop_high_cost_volume").asDouble(),
														m_saCommand.Field("prop_high_cost_value").asDouble(),
														m_saCommand.Field("prop_other_comp_value").asDouble(),
														m_saCommand.Field("prop_status").asShort(),
														m_saCommand.Field("prop_status2").asShort(),
														m_saCommand.Field("prop_group_id").asString(),
														m_saCommand.Field("prop_type_of_action").asShort(),
														m_saCommand.Field("prop_sort_order").asLong(),
														m_saCommand.Field("prop_grot_volume").asDouble(),
														m_saCommand.Field("prop_grot_value").asDouble(),
														m_saCommand.Field("prop_grot_cost").asDouble(),
														m_saCommand.Field("bobj_id").asString(),
														m_saCommand.Field("bmottagarref").asString(),
														m_saCommand.Field("prop_notify_date").asString(),
														m_saCommand.Field("prop_comp_offer_date").asString(),
														m_saCommand.Field("prop_pay_date").asString(),
														convertSADateTime(saDateTime),
														m_saCommand.Field("prop_coord").asString()
														));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getProperties_by_status(int obj_id,vecTransaction_elv_properties &vec,int prop_id)
{
	CString sPropName;
	CString sSQL;
	try
	{
		vec.clear();
		if (prop_id == -1)
		{
			//--------------------------------------------------------------------------------------------------------------------
			// We need to enter ALL, and check status in a diffierent way; 090915 p�d
			sSQL.Format(_T("select a.*, b.prop_name as bprop_name, b.block_number as bblock_number, b.unit_number as bunit_number, b.prop_number as bprop_number, b.county_name as bcounty_name, b.municipal_name as bmunicipal_name, b.obj_id as bobj_id from %s a,%s b where prop_object_id=:1 and a.prop_id=b.id"),
				TBL_ELV_PROP,TBL_PROPERTY);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()	= obj_id;
		}
		else
		{
			sSQL.Format(_T("select a.*, b.prop_name as bprop_name, b.block_number as bblock_number, b.unit_number as bunit_number, b.prop_number as bprop_number, b.county_name as bcounty_name, b.municipal_name as bmunicipal_name, b.obj_id as bobj_id from %s a,%s b ")
									_T(" where prop_object_id=:1 and a.prop_id=:2 and a.prop_id=b.id"),
				TBL_ELV_PROP,TBL_PROPERTY);
			
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()	= obj_id;
			m_saCommand.Param(2).setAsShort()	= prop_id;
		}

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			sPropName.Format(_T("%s %s:%s"),
				m_saCommand.Field("bprop_name").asString(),
				m_saCommand.Field("bblock_number").asString(),
				m_saCommand.Field("bunit_number").asString());
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_elv_properties(m_saCommand.Field("prop_id").asLong(),
														m_saCommand.Field("prop_object_id").asLong(),
														m_saCommand.Field("bcounty_name").asString(),
														m_saCommand.Field("bmunicipal_name").asString(),
														sPropName, //m_saCommand.Field(3).asString(),
														m_saCommand.Field("bprop_number").asString(),
														m_saCommand.Field("prop_volume_m3sk").asDouble(),
														m_saCommand.Field("prop_areal").asDouble(),
														m_saCommand.Field("prop_numof_trees").asLong(),
														m_saCommand.Field("prop_numof_stands").asLong(),
														m_saCommand.Field("prop_wood_volume").asDouble(),
														m_saCommand.Field("prop_wood_value").asDouble(),
														m_saCommand.Field("prop_cost_value").asDouble(),
														m_saCommand.Field("prop_rotpost_value").asDouble(),
														m_saCommand.Field("prop_land_value").asDouble(),
														m_saCommand.Field("prop_early_cut_value").asDouble(),
														m_saCommand.Field("prop_storm_dry_value").asDouble(),
														m_saCommand.Field("prop_randtrees_value").asDouble(),
														m_saCommand.Field("prop_voluntary_deal_value").asDouble(),
														m_saCommand.Field("prop_high_cost_volume").asDouble(),
														m_saCommand.Field("prop_high_cost_value").asDouble(),
														m_saCommand.Field("prop_other_comp_value").asDouble(),
														m_saCommand.Field("prop_status").asShort(),
														m_saCommand.Field("prop_status2").asShort(),
														m_saCommand.Field("prop_group_id").asString(),
														m_saCommand.Field("prop_type_of_action").asShort(),
														m_saCommand.Field("prop_sort_order").asLong(),
														m_saCommand.Field("prop_grot_volume").asDouble(),
														m_saCommand.Field("prop_grot_value").asDouble(),
														m_saCommand.Field("prop_grot_cost").asDouble(),
														m_saCommand.Field("bobj_id").asString(),
														m_saCommand.Field("prop_group_id").asString(),	// mottagatrref
														m_saCommand.Field("prop_notify_date").asString(),	// notify_date
														m_saCommand.Field("prop_comp_offer_date").asString(),	// comp_offer_date
														convertSADateTime(saDateTime),	// pay_date
														m_saCommand.Field("created").asString(),	// date (created)
														m_saCommand.Field("prop_coord").asString()	// coordinate
														));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addProperty(CTransaction_elv_properties &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propExists(rec))
		{
			sSQL.Format(_T("insert into %s (prop_id,prop_object_id,prop_name,prop_number,prop_volume_m3sk,prop_areal,prop_numof_trees,prop_numof_stands,")
									_T("prop_wood_volume,prop_wood_value,prop_cost_value,prop_rotpost_value,prop_land_value,prop_early_cut_value,prop_storm_dry_value,prop_randtrees_value,")
									_T("prop_voluntary_deal_value,prop_high_cost_volume,prop_high_cost_value,prop_other_comp_value,prop_status,prop_status2,prop_group_id,prop_type_of_action,prop_sort_order,")
									_T("prop_grot_volume,prop_grot_value,prop_grot_cost,prop_coord) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29)"), TBL_ELV_PROP);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()	= rec.getPropID_pk();
			m_saCommand.Param(2).setAsLong()	= rec.getPropObjectID_pk();
			m_saCommand.Param(3).setAsString()	= rec.getPropName();
			m_saCommand.Param(4).setAsString()	= rec.getPropNumber();
			m_saCommand.Param(5).setAsDouble()	= rec.getPropM3Sk();
			m_saCommand.Param(6).setAsDouble()	= rec.getPropAreal();
			m_saCommand.Param(7).setAsLong()		= rec.getPropNumOfTrees();
			m_saCommand.Param(8).setAsLong()		= rec.getPropNumOfStands();
			m_saCommand.Param(9).setAsDouble()	= rec.getPropWoodVolume();
			m_saCommand.Param(10).setAsDouble()	= rec.getPropWoodValue();
			m_saCommand.Param(11).setAsDouble()	= rec.getPropCostValue();
			m_saCommand.Param(12).setAsDouble()	= rec.getPropRotpost();
			m_saCommand.Param(13).setAsDouble()	= rec.getPropLandValue();
			m_saCommand.Param(14).setAsDouble()	= rec.getPropEarlyCutValue();
			m_saCommand.Param(15).setAsDouble()	= rec.getPropStromDryValue();
			m_saCommand.Param(16).setAsDouble()	= rec.getPropRandTreesValue();
			m_saCommand.Param(17).setAsDouble()	= rec.getPropVoluntaryDealValue();
			m_saCommand.Param(18).setAsDouble()	= rec.getPropHighCutVolume();
			m_saCommand.Param(19).setAsDouble()	= rec.getPropHighCutValue();
			m_saCommand.Param(20).setAsDouble()	= rec.getPropOtherCostValue();
			m_saCommand.Param(21).setAsShort()	= rec.getPropStatus();
			m_saCommand.Param(22).setAsShort()	= rec.getPropStatus2();
			m_saCommand.Param(23).setAsString()	= rec.getPropGroupID();
			m_saCommand.Param(24).setAsShort()	= rec.getPropTypeOfAction();
			m_saCommand.Param(25).setAsLong()	= rec.getPropSortOrder();
			m_saCommand.Param(26).setAsDouble()	= rec.getPropGrotVolume();
			m_saCommand.Param(27).setAsDouble()	= rec.getPropGrotValue();
			m_saCommand.Param(28).setAsDouble()	= rec.getPropGrotCost();
			m_saCommand.Param(29).setAsString()	= rec.getCoord();	// #3725

			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!propExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updProperty(CTransaction_elv_properties &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("update %s  set prop_name=:1,prop_number=:2,prop_volume_m3sk=:3,prop_areal=:4,prop_numof_trees=:5,prop_numof_stands=:6,")
									_T("prop_wood_volume=:7,prop_wood_value=:8,prop_cost_value=:9,prop_rotpost_value=:10,prop_land_value=:11,prop_early_cut_value=:12,")
									_T("prop_storm_dry_value=:13,prop_randtrees_value=:14,prop_voluntary_deal_value=:15,prop_high_cost_volume=:16,prop_high_cost_value=:17,prop_other_comp_value=:18,")
									_T("prop_status=:19,prop_status2=:20,prop_group_id=:21,prop_type_of_action=:22,prop_sort_order=:23,prop_grot_volume=:24,prop_grot_value=:25,prop_grot_cost=:26,prop_coord=:27 where prop_id=:28 and prop_object_id=:29"),
																	TBL_ELV_PROP);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getPropName();
			m_saCommand.Param(2).setAsString()	= rec.getPropNumber();
			m_saCommand.Param(3).setAsDouble()	= rec.getPropM3Sk();
			m_saCommand.Param(4).setAsDouble()	= rec.getPropAreal();
			m_saCommand.Param(5).setAsLong()	= rec.getPropNumOfTrees();
			m_saCommand.Param(6).setAsLong()	= rec.getPropNumOfStands();
			m_saCommand.Param(7).setAsDouble()	= rec.getPropWoodVolume();
			m_saCommand.Param(8).setAsDouble()	= rec.getPropWoodValue();
			m_saCommand.Param(9).setAsDouble()	= rec.getPropCostValue();
			m_saCommand.Param(10).setAsDouble()	= rec.getPropRotpost();
			m_saCommand.Param(11).setAsDouble()	= rec.getPropLandValue();
			m_saCommand.Param(12).setAsDouble()	= rec.getPropEarlyCutValue();
			m_saCommand.Param(13).setAsDouble()	= rec.getPropStromDryValue();
			m_saCommand.Param(14).setAsDouble()	= rec.getPropRandTreesValue();
			m_saCommand.Param(15).setAsDouble()	= rec.getPropVoluntaryDealValue();
			m_saCommand.Param(16).setAsDouble()	= rec.getPropHighCutVolume();
			m_saCommand.Param(17).setAsDouble()	= rec.getPropHighCutValue();
			m_saCommand.Param(18).setAsDouble()	= rec.getPropOtherCostValue();
			m_saCommand.Param(19).setAsShort()	= rec.getPropStatus();
			m_saCommand.Param(20).setAsShort()	= rec.getPropStatus2();
			m_saCommand.Param(21).setAsString()	= rec.getPropGroupID();
			m_saCommand.Param(22).setAsShort()	= rec.getPropTypeOfAction();
			m_saCommand.Param(23).setAsLong()	= rec.getPropSortOrder();
			m_saCommand.Param(24).setAsDouble()	= rec.getPropGrotVolume();
			m_saCommand.Param(25).setAsDouble()	= rec.getPropGrotValue();
			m_saCommand.Param(26).setAsDouble()	= rec.getPropGrotCost();
			m_saCommand.Param(27).setAsString()	= rec.getCoord();	// #3725
			m_saCommand.Param(28).setAsLong()	= rec.getPropID_pk();
			m_saCommand.Param(29).setAsLong()	= rec.getPropObjectID_pk();

			m_saCommand.Execute();

			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

// Set all values in Property to Zero; 080618 p�d
// OBS! Only values for the Cruise-stand is set; 081022 p�d
BOOL CUMLandValueDB::resetProperty(int prop_id,int obj_id)
{
	CString sSQL;
	try
	{
/*
		sSQL.Format(_T("update %s  set prop_volume_m3sk=:1,prop_numof_trees=:2,prop_numof_stands=:3,prop_wood_volume=:4,prop_wood_value=:5,prop_cost_value=:6,prop_rotpost_value=:7,")
								_T("prop_storm_dry_value=:8,prop_randtrees_value=:9,prop_voluntary_deal_value=:10,prop_high_cost_volume=:11,prop_high_cost_value=:12,prop_other_comp_value=:13,")
								_T("prop_status=:14,prop_status2=:15 where prop_id=:16 and prop_object_id=:17"),TBL_ELV_PROP);
*/
		sSQL.Format(_T("update %s  set prop_volume_m3sk=:1,prop_numof_trees=:2,prop_numof_stands=:3,prop_wood_volume=:4,prop_wood_value=:5,prop_cost_value=:6,prop_rotpost_value=:7,")
								_T("prop_storm_dry_value=:8,prop_randtrees_value=:9,")
								_T("prop_grot_volume=:10,prop_grot_value=:11,prop_grot_cost=:12 where prop_id=:13 and prop_object_id=:14"),TBL_ELV_PROP);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= 0.0;
		m_saCommand.Param(2).setAsLong()		= 0;
		m_saCommand.Param(3).setAsLong()		= 0;
		m_saCommand.Param(4).setAsDouble()	= 0.0;
		m_saCommand.Param(5).setAsDouble()	= 0.0;
		m_saCommand.Param(6).setAsDouble()	= 0.0;
		m_saCommand.Param(7).setAsDouble()	= 0.0;
		m_saCommand.Param(8).setAsDouble()	= 0.0;
		m_saCommand.Param(9).setAsDouble()	= 0.0;
		m_saCommand.Param(10).setAsDouble()	= 0.0;
		m_saCommand.Param(11).setAsDouble()	= 0.0;
		m_saCommand.Param(12).setAsDouble()	= 0.0;
		m_saCommand.Param(13).setAsLong()	= prop_id;
		m_saCommand.Param(14).setAsLong()	= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

//Added funkction for resetting the property before calculations, 20120120 J� Bug #2769
//Does not reset other costs or status values
BOOL CUMLandValueDB::resetPropertyBeforeCalc(int prop_id,int obj_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_volume_m3sk=:1,prop_areal=:2,prop_numof_trees=:3,prop_numof_stands=:4,prop_wood_volume=:5,prop_wood_value=:6,")
			_T("prop_cost_value=:7,prop_rotpost_value=:8,prop_land_value=:9,prop_early_cut_value=:10,prop_storm_dry_value=:11,prop_randtrees_value=:12,")
			_T("prop_voluntary_deal_value=:13,prop_high_cost_volume=:14,prop_high_cost_value=:15,prop_grot_volume=:16,prop_grot_value=:17,prop_grot_cost=:18 where prop_id=:19 and prop_object_id=:20"),TBL_ELV_PROP);
/*
prop_volume_m3sk	float
prop_areal			float
prop_numof_trees	int
prop_numof_stands	int
prop_wood_volume	float
prop_wood_value		float
prop_cost_value		float
prop_rotpost_value	float
prop_land_value		float
prop_early_cut_value	float
prop_storm_dry_value	float
prop_randtrees_value	float
prop_voluntary_deal_value	float
prop_high_cost_volume	float
prop_grot_volume	float
prop_grot_value		float
prop_grot_cost		float*/
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= 0.0;
		m_saCommand.Param(2).setAsDouble()	= 0.0;
		m_saCommand.Param(3).setAsShort()	= 0;
		m_saCommand.Param(4).setAsShort()	= 0;
		m_saCommand.Param(5).setAsDouble()	= 0.0;
		m_saCommand.Param(6).setAsDouble()	= 0.0;
		m_saCommand.Param(7).setAsDouble()	= 0.0;
		m_saCommand.Param(8).setAsDouble()	= 0.0;
		m_saCommand.Param(9).setAsDouble()	= 0.0;
		m_saCommand.Param(10).setAsDouble()	= 0.0;
		m_saCommand.Param(11).setAsDouble()	= 0.0;
		m_saCommand.Param(12).setAsDouble()	= 0.0;
		m_saCommand.Param(13).setAsDouble()	= 0.0;
		m_saCommand.Param(14).setAsDouble()	= 0.0;
		m_saCommand.Param(15).setAsDouble()	= 0.0;
		m_saCommand.Param(16).setAsDouble()	= 0.0;
		m_saCommand.Param(17).setAsDouble()	= 0.0;
		m_saCommand.Param(18).setAsDouble()	= 0.0;
		m_saCommand.Param(19).setAsShort()	= prop_id;
		m_saCommand.Param(20).setAsShort()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}

BOOL CUMLandValueDB::delProperty(CTransaction_elv_properties &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where prop_id=:1 and prop_object_id=:2"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getPropID_pk();
		m_saCommand.Param(2).setAsLong() = rec.getPropObjectID_pk();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updPropertyStatus(int prop_id,int obj_id,int status)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set prop_status=:1 where prop_id=:2 and prop_object_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= status;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updPropertyStatus2(int prop_id,int obj_id,int status)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set prop_status2=:1 where prop_id=:2 and prop_object_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= status;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updPropertyGroupIdentity(int prop_id,int obj_id,LPCTSTR group_id)
{
	CString sSQL;
	try
	{
			sSQL.Format(_T("update %s set prop_group_id=:1 where prop_id=:2 and prop_object_id=:3"),
							TBL_ELV_PROP);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= group_id;
			m_saCommand.Param(2).setAsLong()	= prop_id;
			m_saCommand.Param(3).setAsLong()	= obj_id;

			m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updPropertyTypeOfAction(int prop_id,int obj_id,int type_of_action)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_type_of_action=:1 where prop_id=:2 and prop_object_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= type_of_action;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updPropertyNotifyDate(int obj_id,LPCTSTR landowner_num)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_notify_date=:1 where prop_object_id=:2 and prop_group_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= getDateEx();	// Set date
		m_saCommand.Param(2).setAsLong()	= obj_id;
		m_saCommand.Param(3).setAsString()	= landowner_num;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updPropertyCompOfferDate(int obj_id,LPCTSTR landowner_num)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_comp_offer_date=:1 where prop_object_id=:2 and prop_group_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= getDateEx();	// Set date
		m_saCommand.Param(2).setAsLong()	= obj_id;
		m_saCommand.Param(3).setAsString()	= landowner_num;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updPropertyPayDate(int obj_id,LPCTSTR landowner_num)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_pay_date=:1 where prop_object_id=:2 and prop_group_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= getDateEx();	// Set date
		m_saCommand.Param(2).setAsLong()	= obj_id;
		m_saCommand.Param(3).setAsString()	= landowner_num;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updPropertySortOrder(int prop_id,int obj_id,int sort_number)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_sort_order=:1 where prop_id=:2 and prop_object_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= sort_number;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getPropertySortMax(int obj_id,int* sort_number)
{
	CString sSQL;
	int nMaxSort = 0;
	try
	{
		sSQL.Format(_T("select max(prop_sort_order) as 'max_sort' from %s where prop_object_id=:1"),TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nMaxSort = m_saCommand.Field(_T("max_sort")).asLong();
		}

		*sort_number = nMaxSort;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		*sort_number = 0;
		return FALSE;
	}

	return TRUE;
}
//#5010 PH 20160613
BOOL CUMLandValueDB::removePropOwner(int prop_id,int contact_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{

		sSQL.Format(_T("DELETE FROM %s WHERE prop_id=:1 AND contact_id=:2"),TBL_PROP_OWNER);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsShort() = contact_id;

		m_saCommand.Execute();	
		doCommit();

		return TRUE;

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	//return bReturn;
}

BOOL CUMLandValueDB::getPropertyOwners(int prop_id,vecTransactionContacts &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select a.prop_id,a.owner_share,b.* from %s a,%s b where a.prop_id=:1 and b.id=a.contact_id"),
						TBL_PROP_OWNER,TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asLong(),
				m_saCommand.Field(_T("type_of")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("owner_share")).asString())));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getPropertyActionStatus(vecTransaction_property_status &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by order_num"),TBL_ELV_PROP_STATUS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(8).asDateTime();
			vec.push_back(CTransaction_property_status(m_saCommand.Field(1).asLong(),
														m_saCommand.Field(2).asShort(),
														m_saCommand.Field(3).asString(),
														m_saCommand.Field(4).asShort(),
														m_saCommand.Field(5).asShort(),
														m_saCommand.Field(6).asShort(),
														m_saCommand.Field(7).asShort(),
														convertSADateTime(saDateTime)) );
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addPropertyActionStatus(CTransaction_property_status &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propStatusExists(rec.getPropStatusID_pk()))
		{
			sSQL.Format(_T("insert into %s (id,name,order_num,type_of,r_color,g_color,b_color) values(:1,:2,:3,:4,:5,:6,:7)"),TBL_ELV_PROP_STATUS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()		= rec.getPropStatusID_pk();
			m_saCommand.Param(2).setAsString()	= rec.getPropStatusName();
			m_saCommand.Param(3).setAsShort()		= rec.getPropStatusOrderNum();
			m_saCommand.Param(4).setAsShort()		= rec.getPropStatusType();
			m_saCommand.Param(5).setAsShort()		= rec.getPropStatusRCol();
			m_saCommand.Param(6).setAsShort()		= rec.getPropStatusGCol();
			m_saCommand.Param(7).setAsShort()		= rec.getPropStatusBCol();

			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updPropertyActionStatus(CTransaction_property_status &rec)
{
	CString sSQL;
	try
	{
			sSQL.Format(_T("update %s set name=:1,order_num=:2,type_of=:3,r_color=:4,g_color=:5,b_color=:6 where id=:7"),TBL_ELV_PROP_STATUS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getPropStatusName();
			m_saCommand.Param(2).setAsShort()		= rec.getPropStatusOrderNum();
			m_saCommand.Param(3).setAsShort()		= rec.getPropStatusType();
			m_saCommand.Param(4).setAsShort()		= rec.getPropStatusRCol();
			m_saCommand.Param(5).setAsShort()		= rec.getPropStatusGCol();
			m_saCommand.Param(6).setAsShort()		= rec.getPropStatusBCol();
			m_saCommand.Param(7).setAsLong()		= rec.getPropStatusID_pk();

			m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getNextPropertyActionStatus_id(short *id_num)
{
	short nIdNum = -1;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select max(id) as 'id' from %s"),TBL_ELV_PROP_STATUS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nIdNum = m_saCommand.Field(_T("id")).asLong();
		}

		nIdNum++;
	}
	catch(SAException &e)
	{
		*id_num = nIdNum;
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	*id_num = nIdNum;
	return TRUE;
}

BOOL CUMLandValueDB::getNextPropertyActionStatus_ordernum(short *order_num)
{
	short nOrderNum = 1;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select max(order_num) as 'order_num' from %s"),TBL_ELV_PROP_STATUS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nOrderNum = m_saCommand.Field(_T("order_num")).asShort();
		}
	}
	catch(SAException &e)
	{
		*order_num = nOrderNum;
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	*order_num = nOrderNum;
	return TRUE;
}

BOOL CUMLandValueDB::delPropertyActionStatus(CTransaction_property_status &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where id=:1"),TBL_ELV_PROP_STATUS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getPropStatusID_pk();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::resetPropetyActionStatusID(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_ELV_PROP_STATUS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Check if property action status is used in "elv_properties_table"; 090924 p�d
BOOL CUMLandValueDB::isPropertyActionStatusUsed(int status_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where prop_status=:1"),TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort() = status_id;
		m_saCommand.Execute();

		if (m_saCommand.FetchNext())
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::getPropertyActionStatusUsed(vecTransaction_property_status &vec,mapBoolean &map)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		map.clear();
		for (UINT i = 0;i < vec.size();i++)
		{
			sSQL.Format(_T("select * from %s where prop_status=:1"),TBL_ELV_PROP);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()		= i;
			m_saCommand.Execute();

			if (m_saCommand.FetchNext()) map[i] = true;
			else map[i] = false;
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getPropertyOwners(vecTransactionPropOwners &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_PROP_OWNER);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				vec.push_back(CTransaction_prop_owners(m_saCommand.Field(1).asLong(),
																						 m_saCommand.Field(2).asLong(),
   																					 (LPCTSTR)(m_saCommand.Field(3).asString()),
																						 m_saCommand.Field(4).asShort(),
																						 (LPCTSTR)(m_saCommand.Field(5).asString())) );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;}

BOOL CUMLandValueDB::getPropertyOwnersForProperty(int prop_id,vecTransactionPropOwners& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where prop_id=:1"),TBL_PROP_OWNER);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_prop_owners(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asString(),
													m_saCommand.Field(4).asShort(),
													m_saCommand.Field(5).asString()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::setPropertyOwnersForProperty_IsContact(int prop_id,int contact_id,int is_contact,LPCTSTR share)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set is_contact=:1,owner_share=:2 where prop_id=:3 and contact_id=:4"),TBL_PROP_OWNER);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= is_contact;
		m_saCommand.Param(2).setAsString()	= share;
		m_saCommand.Param(3).setAsLong()		= prop_id;
		m_saCommand.Param(4).setAsLong()		= contact_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getPropertyLogBook(int prop_id,int obj_id,vecTransaction_elv_properties_logbook &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where plog_id=:1 and plog_object_id=:2"),
						TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_elv_properties_logbook(m_saCommand.Field(1).asLong(),
																m_saCommand.Field(2).asLong(),
																m_saCommand.Field(3).asString(),
																m_saCommand.Field(4).asString(),
																m_saCommand.Field(5).asLongChar(),
																convertSADateTime(saDateTime),
																m_saCommand.Field(7).asShort()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addPropertyLogBook(CTransaction_elv_properties_logbook &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propLogBookExists(rec))
		{
			sSQL.Format(_T("insert into %s (plog_id,plog_object_id,plog_log_date,plog_added_by,plog_log_text,plog_type) values(:1,:2,:3,:4,:5,:6)"),
							TBL_ELV_PROP_LOG);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()		= rec.getPropLogBookID_pk();
			m_saCommand.Param(2).setAsLong()		= rec.getPropLogBookObjectID_pk();
			m_saCommand.Param(3).setAsString()	= rec.getPropLogBookDate();
			m_saCommand.Param(4).setAsString()	= rec.getPropLogBookAddedBy();
			m_saCommand.Param(5).setAsLongChar()	= rec.getPropLogBookNote();
			m_saCommand.Param(6).setAsShort()		= rec.getPropLogType();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!propLogBookExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updPropertyLogBook(CTransaction_elv_properties_logbook &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set plog_added_by=:1,plog_log_text=:2,plog_type=:3 where plog_id=:4 and plog_object_id=:5 and plog_log_date=:6"),
						TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getPropLogBookAddedBy();
		m_saCommand.Param(2).setAsLongChar()	= rec.getPropLogBookNote();
		m_saCommand.Param(3).setAsShort()		= rec.getPropLogType();
		m_saCommand.Param(4).setAsLong()		= rec.getPropLogBookID_pk();
		m_saCommand.Param(5).setAsLong()		= rec.getPropLogBookObjectID_pk();
		m_saCommand.Param(6).setAsString()	= rec.getPropLogBookDate();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delPropertyLogBook(CTransaction_elv_properties_logbook &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where plog_id=:1 and plog_object_id=:2 and plog_log_date=:3"),
						TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getPropLogBookID_pk();
		m_saCommand.Param(2).setAsLong()		= rec.getPropLogBookObjectID_pk();
		m_saCommand.Param(3).setAsString()	= rec.getPropLogBookDate();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delPropertyLogBook(int prop_id,int obj_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where plog_id=:1 and plog_object_id=:2"),
						TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= prop_id;
		m_saCommand.Param(2).setAsLong()		= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// OBS! This function is SQL-server specific; 080204 p�d
int CUMLandValueDB::getLastPropertyLogBookID(int obj_id)
{
	CString sSQL;
	int nMaxID = -1;
	try
	{
		sSQL.Format(_T("select max(plog_id) from %s where plog_object_id=:1"),
						TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			nMaxID = m_saCommand.Field(1).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return -1;
	}

	return nMaxID;
}

int CUMLandValueDB::getNumOfEntriesForPropertyInLogBook(int prop_id,int obj_id)
{
	CString sSQL;
	int nNumOf = -1;
	try
	{	
		sSQL.Format(_T("select count(plog_id) as 'numof' from %s where plog_id=:1 and plog_object_id=:2"),TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			nNumOf = m_saCommand.Field(_T("numof")).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return -1;
	}

	return nNumOf;
}

BOOL CUMLandValueDB::getNumOfEntriesForPropertyInLogBook(int obj_id,std::map<int,int>& map)
{
	CString sSQL;
	int nNumOf = -1;
	int nPlogId = -1;
	map.clear();
	try
	{	
		sSQL.Format(_T("select plog_id as 'plogid',count(plog_id) as 'numof' from %s where plog_object_id=:1 group by plog_id"),TBL_ELV_PROP_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			nPlogId = m_saCommand.Field(_T("plogid")).asLong();
			nNumOf = m_saCommand.Field(_T("numof")).asLong();
			map[nPlogId] = nNumOf;
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Handle Property Other costs ("Annan ers�ttning"); 080516 p�d
BOOL CUMLandValueDB::getPropertyOtherComp(int obj_id,vecTransaction_elv_properties_other_comp &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where ocomp_object_id=:1"),
						TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_properties_other_comp(m_saCommand.Field(1).asLong(),
																	m_saCommand.Field(2).asLong(),
																	m_saCommand.Field(3).asLong(),
																	m_saCommand.Field(4).asDouble(),
																	m_saCommand.Field(5).asDouble(),
																	m_saCommand.Field(6).asString(),
																	m_saCommand.Field(7).asShort()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Handle Property Other costs ("Annan ers�ttning"); 080516 p�d
BOOL CUMLandValueDB::getPropertyOtherComp(int obj_id,int prop_id,vecTransaction_elv_properties_other_comp &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where ocomp_object_id=:1 and ocomp_prop_id=:2"),
						TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_properties_other_comp(m_saCommand.Field(1).asLong(),
																	m_saCommand.Field(2).asLong(),
																	m_saCommand.Field(3).asLong(),
																	m_saCommand.Field(4).asDouble(),
																	m_saCommand.Field(5).asDouble(),
																	m_saCommand.Field(6).asString(),
																	m_saCommand.Field(7).asShort()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addPropertyOtherComp(CTransaction_elv_properties_other_comp &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propOtherCompExists(rec))
		{
			sSQL.Format(_T("insert into %s (ocomp_id,ocomp_object_id,ocomp_prop_id,ocomp_value_entered,ocomp_value_calc,ocomp_description,ocomp_typeof)	values(:1,:2,:3,:4,:5,:6,:7)"),
										TBL_ELV_PROP_OTHER_COMP);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getPropOtherCompID_pk();
			m_saCommand.Param(2).setAsLong()		= rec.getPropOtherCompObjectID_pk();
			m_saCommand.Param(3).setAsLong()		= rec.getPropOtherCompPropID_pk();
			m_saCommand.Param(4).setAsDouble()	= rec.getPropOtherCompValue_entered();
			m_saCommand.Param(5).setAsDouble()	= rec.getPropOtherCompValue_calc();
			m_saCommand.Param(6).setAsString()	= rec.getPropOtherCompNote();
			m_saCommand.Param(7).setAsShort()		= rec.getPropOtherCompTypeOf();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!propOtherCompExists(rec)
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updPropertyOtherComp(CTransaction_elv_properties_other_comp &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set ocomp_value_entered=:1,ocomp_value_calc=:2,ocomp_description=:3,ocomp_typeof=:4 where ocomp_id=:5 and ocomp_object_id=:6 and ocomp_prop_id=:7"),
								 TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getPropOtherCompValue_entered();
		m_saCommand.Param(2).setAsDouble()	= rec.getPropOtherCompValue_calc();
		m_saCommand.Param(3).setAsString()	= rec.getPropOtherCompNote();
		m_saCommand.Param(4).setAsShort()		= rec.getPropOtherCompTypeOf();
		m_saCommand.Param(5).setAsLong()		= rec.getPropOtherCompID_pk();
		m_saCommand.Param(6).setAsLong()		= rec.getPropOtherCompObjectID_pk();
		m_saCommand.Param(7).setAsLong()		= rec.getPropOtherCompPropID_pk();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delPropertyOtherComp(int obj_id,int prop_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where ocomp_object_id=:1 and ocomp_prop_id=:2"),
						TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		m_saCommand.Param(2).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::delPropertyOtherComp(int ocomp_id,int obj_id,int prop_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where ocomp_id=:1 and ocomp_object_id=:2 and ocomp_prop_id=:3"),
						TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= ocomp_id;
		m_saCommand.Param(2).setAsLong()		= obj_id;
		m_saCommand.Param(3).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

int CUMLandValueDB::getPropertyOtherCompNextIndex(int obj_id,int prop_id)
{
	int nNextIndex = 1;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select max(ocomp_id) from %s where ocomp_object_id=:1 and ocomp_prop_id=:2"),
							TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			nNextIndex = (m_saCommand.Field(1).asLong() + 1);
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return 1;
	}

	return nNextIndex;
}

// Update/remove information on connection between Trakt and Property/Object
BOOL CUMLandValueDB::removePropTraktConnection(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set trakt_prop_id=:1,trakt_prop_num=:2,trakt_prop_name=:3 where trakt_id=:4"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= -1;	// No Property
		m_saCommand.Param(2).setAsString()	= _T("");
		m_saCommand.Param(3).setAsString()	= _T("");
		m_saCommand.Param(4).setAsLong()	= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

// Handle Object cruise table; 080506 p�d
BOOL CUMLandValueDB::getObjectCruises(int obj_id,vecTransaction_elv_cruise &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

//		sSQL.Format(_T("select * from %s where ecru_prop_id=:1 and ecru_object_id=:2 and ecru_cruise_type=:3"),TBL_ELV_CRUISE);
		sSQL.Format(_T("select * from %s where ecru_object_id=:1"),TBL_ELV_CRUISE);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		//m_saCommand.Param(3).setAsLong() = CRUISE_TYPE_1;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_elv_cruise(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asLong(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asShort(),
													m_saCommand.Field(20).asShort(),
													m_saCommand.Field(22).asShort(),
													m_saCommand.Field(23).asDouble(),
													m_saCommand.Field(24).asString(),
													m_saCommand.Field(25).asString(),
													m_saCommand.Field(_T("ecru_grot_volume")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_value")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_cost")).asDouble(),
													m_saCommand.Field(_T("ecru_use_for_infr")).asShort(),
													m_saCommand.Field(_T("ecru_tillf_uttnyttjande")).asBool(),
													m_saCommand.Field(_T("ecru_sttork_fsprucemix")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsumm3sk_inside")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_favgpricefactor")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_ftakecareofperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpinep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsprucep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fbirchp30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fcompensationlevel")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpineperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_spruceperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_birchperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_widefactor")).asDouble(),
													//HMS-48 Info om markv�rde 20200427 J�
													m_saCommand.Field(_T("ecru_markinfo_valuepine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_valuespruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_reducedby")).asDouble()
													));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


// Handle Object cruise table; 080506 p�d
BOOL CUMLandValueDB::getObjectCruises(int prop_id,int obj_id,vecTransaction_elv_cruise &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

//		sSQL.Format(_T("select * from %s where ecru_prop_id=:1 and ecru_object_id=:2 and ecru_cruise_type=:3"),TBL_ELV_CRUISE);
		sSQL.Format(_T("select * from %s where ecru_prop_id=:1 and ecru_object_id=:2"),TBL_ELV_CRUISE);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		//m_saCommand.Param(3).setAsLong() = CRUISE_TYPE_1;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_elv_cruise(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asLong(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asShort(),
													m_saCommand.Field(20).asShort(),
													m_saCommand.Field(22).asShort(),
													m_saCommand.Field(23).asDouble(),
													m_saCommand.Field(24).asString(),
													m_saCommand.Field(25).asString(),
													m_saCommand.Field(_T("ecru_grot_volume")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_value")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_cost")).asDouble(),
													m_saCommand.Field(_T("ecru_use_for_infr")).asShort(),
													m_saCommand.Field(_T("ecru_tillf_uttnyttjande")).asBool(),
													m_saCommand.Field(_T("ecru_sttork_fsprucemix")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsumm3sk_inside")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_favgpricefactor")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_ftakecareofperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpinep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsprucep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fbirchp30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fcompensationlevel")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpineperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_spruceperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_birchperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_widefactor")).asDouble(),
													//HMS-48 Info om markv�rde 20200427 J�
													m_saCommand.Field(_T("ecru_markinfo_valuepine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_valuespruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_reducedby")).asDouble()
													));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Handle Object cruise table; 080506 p�d
BOOL CUMLandValueDB::getObjectCruises(vecTransaction_elv_cruise &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

//		sSQL.Format(_T("select * from %s where ecru_prop_id=:1 and ecru_object_id=:2 and ecru_cruise_type=:3"),TBL_ELV_CRUISE);
		sSQL.Format(_T("select * from %s"),TBL_ELV_CRUISE);
		
		m_saCommand.setCommandText((SAString)sSQL);
		//m_saCommand.Param(3).setAsLong() = CRUISE_TYPE_1;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_elv_cruise(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asLong(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asShort(),
													m_saCommand.Field(20).asShort(),
													m_saCommand.Field(22).asShort(),
													m_saCommand.Field(23).asDouble(),
													m_saCommand.Field(24).asString(),
													m_saCommand.Field(25).asString(),
													m_saCommand.Field(_T("ecru_grot_volume")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_value")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_cost")).asDouble(),
													m_saCommand.Field(_T("ecru_use_for_infr")).asShort(),
													m_saCommand.Field(_T("ecru_tillf_uttnyttjande")).asBool(),
													m_saCommand.Field(_T("ecru_sttork_fsprucemix")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsumm3sk_inside")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_favgpricefactor")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_ftakecareofperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpinep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsprucep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fbirchp30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fcompensationlevel")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpineperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_spruceperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_birchperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_widefactor")).asDouble(),
													//HMS-48 Info om markv�rde 20200427 J�
													m_saCommand.Field(_T("ecru_markinfo_valuepine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_valuespruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_reducedby")).asDouble()
													));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

long CUMLandValueDB::getObjectCruise_last_id(int obj_id,int prop_id,int cruise_type)
{
	CString sSQL;
	try
	{	
		sSQL.Format(_T("select max(ecru_id) as 'max_id' from %s where ecru_prop_id=:1 and ecru_object_id=:2 and ecru_cruise_type=:3"),TBL_ELV_CRUISE);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Param(3).setAsShort() = cruise_type;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			return m_saCommand.Field(_T("max_id")).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return -1;
}

long CUMLandValueDB::getObjectCruise_id_exists(long ecru_id,int obj_id,int prop_id)
{
	long nEcruID = ecru_id;
	if (cruiseExists(nEcruID,prop_id,obj_id))
	{
		nEcruID++;
		while (cruiseExists(nEcruID,prop_id,obj_id))
		{
			nEcruID++;
		}
	}
	return nEcruID;
}


// Added 2009-05-25 P�D
BOOL CUMLandValueDB::getObjectCruisesNotCalculatedForObject(int obj_id,vecTransaction_elv_cruise &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where ecru_object_id=:1 and ecru_numof_trees=0"),TBL_ELV_CRUISE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_elv_cruise(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asLong(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asShort(),
													m_saCommand.Field(20).asShort(),
													m_saCommand.Field(22).asShort(),
													m_saCommand.Field(23).asDouble(),
													m_saCommand.Field(24).asString(),																				 
													m_saCommand.Field(25).asString(),
													m_saCommand.Field(_T("ecru_grot_volume")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_value")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_cost")).asDouble(),
													m_saCommand.Field(_T("ecru_use_for_infr")).asShort(),
													m_saCommand.Field(_T("ecru_tillf_uttnyttjande")).asBool(),
													m_saCommand.Field(_T("ecru_sttork_fsprucemix")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsumm3sk_inside")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_favgpricefactor")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_ftakecareofperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpinep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsprucep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fbirchp30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fcompensationlevel")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpineperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_spruceperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_birchperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_widefactor")).asDouble(),
													//HMS-48 Info om markv�rde 20200427 J�
													m_saCommand.Field(_T("ecru_markinfo_valuepine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_valuespruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_reducedby")).asDouble()
													));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Added handling of Manually entered stands into elv_manual_cruise_table; 100503 p�d
BOOL CUMLandValueDB::addObjectCruise(CTransaction_elv_cruise &rec)
{
	CString sSQL;
	try
	{
		if (!cruiseExists(rec))
		{
			sSQL.Format(_T("insert into %s (ecru_id,ecru_object_id,ecru_prop_id,ecru_number,ecru_name,ecru_numof_trees,ecru_areal,ecru_m3sk_vol,ecru_timber_volume,ecru_timber_value,")
				_T("ecru_timber_cost,ecru_rotnetto,ecru_storm_dry_vol,ecru_storm_dry_value,ecru_storm_dry_spruce_mix,ecru_avg_price_factor,ecru_randtrees_volume,")
				_T("ecru_randtrees_value,ecru_use_sample_trees,ecru_do_reduce_rotpost,ecru_cruise_type,ecru_width,ecru_si,ecru_tgl,ecru_grot_volume,ecru_grot_value,ecru_grot_cost,ecru_use_for_infr,ecru_tillf_uttnyttjande,")
				_T("ecru_sttork_fsprucemix,")
				_T("ecru_sttork_fsumm3sk_inside,")
				_T("ecru_sttork_favgpricefactor,")
				_T("ecru_sttork_ftakecareofperc,")
				_T("ecru_sttork_fpinep30price,")
				_T("ecru_sttork_fsprucep30price,")
				_T("ecru_sttork_fbirchp30price,")
				_T("ecru_sttork_fcompensationlevel,")
				_T("ecru_sttork_fpineperc,")
				_T("ecru_sttork_spruceperc,")
				_T("ecru_sttork_birchperc,")
				_T("ecru_sttork_widefactor) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39,:40,:41)"),TBL_ELV_CRUISE);
			m_saCommand.setCommandText((SAString)sSQL);

				m_saCommand.Param(1).setAsLong()		= rec.getECruID_pk();
				m_saCommand.Param(2).setAsLong()		= rec.getECruObjectID_pk();
				m_saCommand.Param(3).setAsLong()		= rec.getECruPropID_pk();
				m_saCommand.Param(4).setAsString()	= rec.getECruNumber();
				m_saCommand.Param(5).setAsString()	= rec.getECruName();
				m_saCommand.Param(6).setAsLong()		= rec.getECruNumOfTrees();
				m_saCommand.Param(7).setAsDouble()	= rec.getECruAreal();
				m_saCommand.Param(8).setAsDouble()	= rec.getECruM3Sk();
				m_saCommand.Param(9).setAsDouble()	= rec.getECruTimberVol();
				m_saCommand.Param(10).setAsDouble()	= rec.getECruTimberValue();
				m_saCommand.Param(11).setAsDouble()	= rec.getECruTimberCost();
				m_saCommand.Param(12).setAsDouble()	= rec.getECruNetto();
				m_saCommand.Param(13).setAsDouble()	= rec.getECruStormDryVol();
				m_saCommand.Param(14).setAsDouble()	= rec.getECruStormDryValue();
				m_saCommand.Param(15).setAsDouble()	= rec.gteECruStormDrySpruceMix();
				m_saCommand.Param(16).setAsDouble()	= rec.getECruAvgPriceFactor();
				m_saCommand.Param(17).setAsDouble()	= rec.getECruRandTreesVol();
				m_saCommand.Param(18).setAsDouble()	= rec.getECruRandTreesValue();
				m_saCommand.Param(19).setAsShort()	= rec.getECruUseSampleTrees();
				m_saCommand.Param(20).setAsShort()	= rec.getECruDoReduceRotpost();
				m_saCommand.Param(21).setAsShort()	= rec.getECruType();
				m_saCommand.Param(22).setAsDouble()	= rec.getECruWidth();
				m_saCommand.Param(23).setAsString()	= rec.getECruSI();
				m_saCommand.Param(24).setAsString()	= rec.getECruTGL();
				m_saCommand.Param(25).setAsDouble()	= rec.getECruGrotVolume();
				m_saCommand.Param(26).setAsDouble()	= rec.getECruGrotValue();
				m_saCommand.Param(27).setAsDouble()	= rec.getECruGrotCost();
				m_saCommand.Param(28).setAsShort()	= (rec.getECruUseForInfr() == 0 ? 0 : 1);
				m_saCommand.Param(29).setAsBool()	= rec.getECruTillfUtnyttj();

				m_saCommand.Param(30).setAsDouble()	= rec.getStormTorkInfo_SpruceMix();
				m_saCommand.Param(31).setAsDouble()	= rec.getStormTorkInfo_SumM3Sk_inside();
				m_saCommand.Param(32).setAsDouble()	= rec.getStormTorkInfo_AvgPriceFactor();
				m_saCommand.Param(33).setAsDouble()	= rec.getStormTorkInfo_TakeCareOfPerc();
				m_saCommand.Param(34).setAsDouble()	= rec.getStormTorkInfo_PineP30Price();
				m_saCommand.Param(35).setAsDouble()	= rec.getStormTorkInfo_SpruceP30Price();
				m_saCommand.Param(36).setAsDouble()	= rec.getStormTorkInfo_BirchP30Price();
				m_saCommand.Param(37).setAsDouble()	= rec.getStormTorkInfo_CompensationLevel();
				m_saCommand.Param(38).setAsDouble()	= rec.getStormTorkInfo_Andel_Pine();
				m_saCommand.Param(39).setAsDouble()	= rec.getStormTorkInfo_Andel_Spruce();
				m_saCommand.Param(40).setAsDouble()	= rec.getStormTorkInfo_Andel_Birch();
				m_saCommand.Param(41).setAsDouble()	= rec.getStormTorkInfo_WideningFactor();


				m_saCommand.Execute();
		}	// if (!cruiseExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjectCruise(CTransaction_elv_cruise &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("update %s set ecru_number=:1,ecru_name=:2,ecru_numof_trees=:3,ecru_areal=:4,ecru_m3sk_vol=:5,ecru_timber_volume=:6,ecru_timber_value=:7,")
									_T("ecru_timber_cost=:8,ecru_rotnetto=:9,ecru_storm_dry_vol=:10,ecru_storm_dry_value=:11,ecru_storm_dry_spruce_mix=:12,ecru_avg_price_factor=:13,ecru_randtrees_volume=:14,")
									_T("ecru_randtrees_value=:15,ecru_use_sample_trees=:16,ecru_do_reduce_rotpost=:17,ecru_cruise_type=:18,ecru_width=:19,ecru_si=:20,ecru_tgl=:21,ecru_grot_volume=:22,ecru_grot_value=:23,ecru_grot_cost=:24,")
									_T("ecru_use_for_infr=:25,ecru_tillf_uttnyttjande=:26,")
									_T("ecru_sttork_fsprucemix=:27,")
									_T("ecru_sttork_fsumm3sk_inside=:28,")
									_T("ecru_sttork_favgpricefactor=:29,")
									_T("ecru_sttork_ftakecareofperc=:30,")
									_T("ecru_sttork_fpinep30price=:31,")
									_T("ecru_sttork_fsprucep30price=:32,")
									_T("ecru_sttork_fbirchp30price=:33,")
									_T("ecru_sttork_fcompensationlevel=:34,")
									_T("ecru_sttork_fpineperc=:35,")
									_T("ecru_sttork_spruceperc=:36,")
									_T("ecru_sttork_birchperc=:37,")
									_T("ecru_sttork_widefactor=:38 ")
									_T("where ecru_id=:39 and ecru_object_id=:40 and ecru_prop_id=:41"),TBL_ELV_CRUISE);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getECruNumber();
			m_saCommand.Param(2).setAsString()	= rec.getECruName();
			m_saCommand.Param(3).setAsLong()	= rec.getECruNumOfTrees();
			m_saCommand.Param(4).setAsDouble()	= rec.getECruAreal();
			m_saCommand.Param(5).setAsDouble()	= rec.getECruM3Sk();
			m_saCommand.Param(6).setAsDouble()	= rec.getECruTimberVol();
			m_saCommand.Param(7).setAsDouble()	= rec.getECruTimberValue();
			m_saCommand.Param(8).setAsDouble()	= rec.getECruTimberCost();
			m_saCommand.Param(9).setAsDouble()	= rec.getECruNetto();
			m_saCommand.Param(10).setAsDouble()	= rec.getECruStormDryVol();
			m_saCommand.Param(11).setAsDouble()	= rec.getECruStormDryValue();
			m_saCommand.Param(12).setAsDouble()	= rec.gteECruStormDrySpruceMix();
			m_saCommand.Param(13).setAsDouble()	= rec.getECruAvgPriceFactor();
			m_saCommand.Param(14).setAsDouble()	= rec.getECruRandTreesVol();
			m_saCommand.Param(15).setAsDouble()	= rec.getECruRandTreesValue();
			m_saCommand.Param(16).setAsShort()	= rec.getECruUseSampleTrees();
			m_saCommand.Param(17).setAsShort()	= rec.getECruDoReduceRotpost();
			m_saCommand.Param(18).setAsShort()	= rec.getECruType();
			m_saCommand.Param(19).setAsDouble()	= rec.getECruWidth();
			m_saCommand.Param(20).setAsString()	= rec.getECruSI();
			m_saCommand.Param(21).setAsString()	= rec.getECruTGL();
			m_saCommand.Param(22).setAsDouble()	= rec.getECruGrotVolume();
			m_saCommand.Param(23).setAsDouble()	= rec.getECruGrotValue();
			m_saCommand.Param(24).setAsDouble()	= rec.getECruGrotCost();
			m_saCommand.Param(25).setAsShort()	= (rec.getECruUseForInfr() == 0 ? 0 : 1);
			
			m_saCommand.Param(26).setAsBool()		= rec.getECruTillfUtnyttj();

			m_saCommand.Param(27).setAsDouble()	= rec.getStormTorkInfo_SpruceMix();
			m_saCommand.Param(28).setAsDouble()	= rec.getStormTorkInfo_SumM3Sk_inside();
			m_saCommand.Param(29).setAsDouble()	= rec.getStormTorkInfo_AvgPriceFactor();
			m_saCommand.Param(30).setAsDouble()	= rec.getStormTorkInfo_TakeCareOfPerc();
			m_saCommand.Param(31).setAsDouble()	= rec.getStormTorkInfo_PineP30Price();
			m_saCommand.Param(32).setAsDouble()	= rec.getStormTorkInfo_SpruceP30Price();
			m_saCommand.Param(33).setAsDouble()	= rec.getStormTorkInfo_BirchP30Price();
			m_saCommand.Param(34).setAsDouble()	= rec.getStormTorkInfo_CompensationLevel();
			m_saCommand.Param(35).setAsDouble()	= rec.getStormTorkInfo_Andel_Pine();
			m_saCommand.Param(36).setAsDouble()	= rec.getStormTorkInfo_Andel_Spruce();
			m_saCommand.Param(37).setAsDouble()	= rec.getStormTorkInfo_Andel_Birch();
			m_saCommand.Param(38).setAsDouble()	= rec.getStormTorkInfo_WideningFactor();


			m_saCommand.Param(39).setAsLong()		= rec.getECruID_pk();
			m_saCommand.Param(40).setAsLong()	= rec.getECruObjectID_pk();
			m_saCommand.Param(41).setAsLong()	= rec.getECruPropID_pk();
		m_saCommand.Execute();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObjectCruise_wside(long ecru_id,int prop_id,int obj_id,int value)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set ecru_use_sample_trees=:1 where ecru_id=:2 and ecru_object_id=:3 and ecru_prop_id=:4"),TBL_ELV_CRUISE);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort() = value;
		m_saCommand.Param(2).setAsLong() = ecru_id;
		m_saCommand.Param(3).setAsLong() = obj_id;
		m_saCommand.Param(4).setAsLong() = prop_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}


BOOL CUMLandValueDB::updObjectCruise_width(long ecru_id,int prop_id,int obj_id,double value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set ecru_width=:1 where ecru_id=:2 and ecru_object_id=:3 and ecru_prop_id=:4"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= value;
		m_saCommand.Param(2).setAsLong()		= ecru_id;
		m_saCommand.Param(3).setAsLong()		= obj_id;
		m_saCommand.Param(4).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjectCruise_do_reduce_rotpost(long ecru_id,int prop_id,int obj_id,int value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set ecru_do_reduce_rotpost=:1 where ecru_id=:2 and ecru_object_id=:3 and ecru_prop_id=:4"),TBL_ELV_CRUISE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= value;
		m_saCommand.Param(2).setAsLong()		= ecru_id;
		m_saCommand.Param(3).setAsLong()		= obj_id;
		m_saCommand.Param(4).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updObjectCruise_randtrees(long ecru_id,int prop_id,int obj_id,double randtrees_volume,double randtrees_value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set ecru_randtrees_volume=:1,ecru_randtrees_value=:2 where ecru_id=:3 and ecru_object_id=:4 and ecru_prop_id=:5"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= randtrees_volume;
		m_saCommand.Param(2).setAsDouble()	= randtrees_value;
		m_saCommand.Param(3).setAsLong()		= ecru_id;
		m_saCommand.Param(4).setAsLong()		= obj_id;
		m_saCommand.Param(5).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjectCruise_storm_and_dry(long ecru_id,int prop_id,int obj_id,double volume,double value,double mix)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set ecru_storm_dry_vol=:1,ecru_storm_dry_value=:2,ecru_storm_dry_spruce_mix=:3 where ecru_id=:4 and ecru_object_id=:5 and ecru_prop_id=:6"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= volume;
		m_saCommand.Param(2).setAsDouble()	= value;
		m_saCommand.Param(3).setAsDouble()	= mix;
		m_saCommand.Param(4).setAsLong()		= ecru_id;
		m_saCommand.Param(5).setAsLong()		= obj_id;
		m_saCommand.Param(6).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjectCruise_rotpost(long ecru_id,int prop_id,int obj_id,double volume,double value,double cost,double netto)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set ecru_timber_volume=:1,ecru_timber_value=:2,ecru_timber_cost=:3,ecru_rotnetto=:4 where ecru_id=:5 and ecru_object_id=:6 and ecru_prop_id=:7"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= volume;
		m_saCommand.Param(2).setAsDouble()	= value;
		m_saCommand.Param(3).setAsDouble()	= cost;
		m_saCommand.Param(4).setAsDouble()	= netto;
		m_saCommand.Param(5).setAsLong()		= ecru_id;
		m_saCommand.Param(6).setAsLong()		= obj_id;
		m_saCommand.Param(7).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjectCruise_use_for_infr(long ecru_id,int prop_id,int obj_id,int use)

{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set ecru_use_for_infr=:1 where ecru_id=:2 and ecru_object_id=:3 and ecru_prop_id=:4"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= use;
		m_saCommand.Param(2).setAsLong()		= ecru_id;
		m_saCommand.Param(3).setAsLong()		= obj_id;
		m_saCommand.Param(4).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjectProperty(int prop_id,int obj_id,CTransaction_elv_properties &rec)
{
	CString sPropName;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select a.*, b.prop_name as bprop_name, b.block_number as bblock_number, b.unit_number as bunit_number, b.prop_number as bprop_number, b.county_name as bcounty_name, b.municipal_name as bmunicipal_name, b.obj_id as bobj_id, b.mottagarref as bmottagarref ")
								_T("from %s a,%s b where a.prop_object_id=:1 and a.prop_id=:2 and a.prop_id=b.id"),TBL_ELV_PROP,TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			sPropName.Format(_T("%s %s:%s"),
								m_saCommand.Field("bprop_name").asString(),
								m_saCommand.Field("bblock_number").asString(),
								m_saCommand.Field("bunit_number").asString());
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_elv_properties(m_saCommand.Field("prop_id").asLong(),
												m_saCommand.Field("prop_object_id").asLong(),
												m_saCommand.Field("bcounty_name").asString(),
												m_saCommand.Field("bmunicipal_name").asString(),
												sPropName,
												m_saCommand.Field("bprop_number").asString(),
												m_saCommand.Field("prop_volume_m3sk").asDouble(),
												m_saCommand.Field("prop_areal").asDouble(),
												m_saCommand.Field("prop_numof_trees").asLong(),
												m_saCommand.Field("prop_numof_stands").asLong(),
												m_saCommand.Field("prop_wood_volume").asDouble(),
												m_saCommand.Field("prop_wood_value").asDouble(),
												m_saCommand.Field("prop_cost_value").asDouble(),
												m_saCommand.Field("prop_rotpost_value").asDouble(),
												m_saCommand.Field("prop_land_value").asDouble(),
												m_saCommand.Field("prop_early_cut_value").asDouble(),
												m_saCommand.Field("prop_storm_dry_value").asDouble(),
												m_saCommand.Field("prop_randtrees_value").asDouble(),
												m_saCommand.Field("prop_voluntary_deal_value").asDouble(),
												m_saCommand.Field("prop_high_cost_volume").asDouble(),
												m_saCommand.Field("prop_high_cost_value").asDouble(),
												m_saCommand.Field("prop_other_comp_value").asDouble(),
												m_saCommand.Field("prop_status").asShort(),
												m_saCommand.Field("prop_status2").asShort(),
												m_saCommand.Field("prop_group_id").asString(),
												m_saCommand.Field("prop_type_of_action").asShort(),
												m_saCommand.Field("prop_sort_order").asLong(),
												m_saCommand.Field("prop_grot_volume").asDouble(),
												m_saCommand.Field("prop_grot_value").asDouble(),
												m_saCommand.Field("prop_grot_cost").asDouble(),
												m_saCommand.Field("bobj_id").asString(),	// search id
												m_saCommand.Field("bmottagarref").asString(),	// mottagarref
												m_saCommand.Field("prop_notify_date").asString(),
												m_saCommand.Field("prop_comp_offer_date").asString(),
												m_saCommand.Field("prop_pay_date").asString(),
												convertSADateTime(saDateTime),
												m_saCommand.Field("prop_coord").asString()
												);
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::delObjectCruise(int prop_id,int obj_id,int cruise_type)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where ecru_prop_id=:1 and ecru_object_id=:2 and ecru_cruise_type=:3"),TBL_ELV_CRUISE);
	
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Param(3).setAsShort() = cruise_type;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}

BOOL CUMLandValueDB::delObjectCruise(long ecru_id,int prop_id,int obj_id,int cruise_type)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where ecru_id=:1 and ecru_prop_id=:2 and ecru_object_id=:3 and ecru_cruise_type=:4"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= ecru_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Param(3).setAsLong() = obj_id;
		m_saCommand.Param(4).setAsShort() = cruise_type;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}

// Handle Object cruise randtrees table; 080512 p�d
BOOL CUMLandValueDB::getObjectCruiseRandtrees(int cruise_id,int prop_id,int obj_id,vecTransaction_elv_cruise_randtrees &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where erand_prop_id=:1 and erand_object_id=:2 and erand_ecru_id=:3"),
						TBL_ELV_CRUISE_RANDTREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = cruise_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_elv_cruise_randtrees(m_saCommand.Field(1).asLong(),
															m_saCommand.Field(2).asLong(),
															m_saCommand.Field(3).asLong(),
															m_saCommand.Field(4).asLong(),
															m_saCommand.Field(5).asLong(),
															m_saCommand.Field(6).asString(),
															m_saCommand.Field(7).asDouble(),
															m_saCommand.Field(8).asDouble(),
															m_saCommand.Field(9).asString()) );
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addObjectCruiseRandtrees(CTransaction_elv_cruise_randtrees &rec)
{
	CString sSQL;
	try
	{
		if( !cruiseRandTreesExists(rec) )
		{
			sSQL.Format(_T("insert into %s (erand_id,erand_object_id,erand_prop_id,erand_ecru_id,erand_spc_id,erand_spc_name,erand_volume,erand_value) values(:1,:2,:3,:4,:5,:6,:7,:8)"),
											TBL_ELV_CRUISE_RANDTREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getEValRandID_pk();
			m_saCommand.Param(2).setAsLong()	= rec.getEValRandObjectID_pk();
			m_saCommand.Param(3).setAsLong()	= rec.getEValRandPropID_pk();
			m_saCommand.Param(4).setAsLong()	= rec.getEValRandEvaluID_pk();
			m_saCommand.Param(5).setAsLong()	= rec.getEValRandSpcID();
			m_saCommand.Param(6).setAsString()	= rec.getEValRandSpcName();
			m_saCommand.Param(7).setAsDouble()	= rec.getEValRandVolume();
			m_saCommand.Param(8).setAsDouble()	= rec.getEValRandValue();
			m_saCommand.Execute();
		}
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delObjectCruiseRandtrees(int cruise_id,int prop_id,int obj_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where erand_prop_id=:1 and erand_object_id=:2 and erand_ecru_id=:3"),
						TBL_ELV_CRUISE_RANDTREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= prop_id;
		m_saCommand.Param(2).setAsLong()		= obj_id;
		m_saCommand.Param(3).setAsLong()		= cruise_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}


// OBJECT EVALUATION TABLE HADLING; 080514 p�d

BOOL CUMLandValueDB::checkObjectEvaluationsExist(int obj_id)
{
	try
	{
		// Check if object contain any evaluation stands
		m_saCommand.setCommandText(_T("SELECT COUNT(*) FROM elv_evaluation_table WHERE eval_object_id = :1"));
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();
		if( m_saCommand.FetchNext() )
		{
			if( m_saCommand.Field(1).asLong() > 0 ) return TRUE;
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return FALSE;
}

// Object evaluation table; 080514 p�d
BOOL CUMLandValueDB::getObjectEvaluation(int prop_id,int obj_id,vecTransaction_eval_evaluation &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where eval_prop_id=:1 and eval_object_id=:2"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(19).asDateTime();
			vec.push_back(CTransaction_eval_evaluation(m_saCommand.Field(1).asLong(),
				m_saCommand.Field(2).asLong(),
				m_saCommand.Field(3).asLong(),
				m_saCommand.Field(4).asString(),
				m_saCommand.Field(5).asDouble(),
				m_saCommand.Field(6).asLong(),
				m_saCommand.Field(7).asString(),
				m_saCommand.Field(8).asDouble(),
				m_saCommand.Field(9).asDouble(),
				m_saCommand.Field(10).asDouble(),
				m_saCommand.Field(11).asDouble(),
				m_saCommand.Field(12).asShort(),
				m_saCommand.Field(14).asShort(),
				m_saCommand.Field(17).asDouble(),
				m_saCommand.Field(18).asDouble(),
				m_saCommand.Field(15).asDouble(),
				m_saCommand.Field(16).asDouble(),
				convertSADateTime(saDateTime),
				m_saCommand.Field(20).asDouble(),
				m_saCommand.Field(_T("eval_length")).asLong(),
				m_saCommand.Field(_T("eval_width")).asLong(),
				m_saCommand.Field(_T("eval_layer")).asLong(),
				m_saCommand.Field(_T("eval_side")).asLong(),
				m_saCommand.Field(_T("eval_overlapping_land")).asBool(),
				m_saCommand.Field(_T("eval_tillf_uttnyttjande")).asBool(),
				//HMS-49 Info om f�rtidig avverkning 20191126 J�
				m_saCommand.Field(_T("eval_info_precut_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_corrfact")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_reduced")).asDouble(),
				//HMS-48 Info om markv�rde 20200427 J�
				m_saCommand.Field(_T("eval_markinfo_valuepine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_valuespruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_reducedby")).asDouble()
				));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Object evaluation table; 080514 p�d
CTransaction_eval_evaluation* CUMLandValueDB::getObjectEvaluation(int prop_id,int obj_id,int eval_id)
{
	CString sSQL;
	try
	{

		sSQL.Format(_T("SELECT * FROM %s WHERE eval_prop_id=:1 AND eval_object_id=:2 AND eval_id=:3"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Param(3).setAsLong() = eval_id;
		m_saCommand.Execute();

		if(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(19).asDateTime();
			return new CTransaction_eval_evaluation(m_saCommand.Field(1).asLong(),
				m_saCommand.Field(2).asLong(),
				m_saCommand.Field(3).asLong(),
				m_saCommand.Field(4).asString(),
				m_saCommand.Field(5).asDouble(),
				m_saCommand.Field(6).asLong(),
				m_saCommand.Field(7).asString(),
				m_saCommand.Field(8).asDouble(),
				m_saCommand.Field(9).asDouble(),
				m_saCommand.Field(10).asDouble(),
				m_saCommand.Field(11).asDouble(),
				m_saCommand.Field(12).asShort(),
				m_saCommand.Field(14).asShort(),
				m_saCommand.Field(17).asDouble(),
				m_saCommand.Field(18).asDouble(),
				m_saCommand.Field(15).asDouble(),
				m_saCommand.Field(16).asDouble(),
				convertSADateTime(saDateTime),
				m_saCommand.Field(20).asDouble(),
				m_saCommand.Field(_T("eval_length")).asLong(),
				m_saCommand.Field(_T("eval_width")).asLong(),
				m_saCommand.Field(_T("eval_layer")).asLong(),
				m_saCommand.Field(_T("eval_side")).asLong(),
				m_saCommand.Field(_T("eval_overlapping_land")).asBool(),
				m_saCommand.Field(_T("eval_tillf_uttnyttjande")).asBool(),
				//HMS-49 Info om f�rtidig avverkning 20191126 J�
				m_saCommand.Field(_T("eval_info_precut_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_corrfact")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_reduced")).asDouble(),
				//HMS-48 Info om markv�rde 20200427 J�
				m_saCommand.Field(_T("eval_markinfo_valuepine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_valuespruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_reducedby")).asDouble()
				);
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return NULL;
	}

	return NULL;
}

int CUMLandValueDB::getObjectEvaluated_last_id(int obj_id,int prop_id)
{
	CString sSQL;
	try
	{	
		sSQL.Format(_T("select max(eval_id) as 'max_id' from %s where eval_prop_id=:1 and eval_object_id=:2"),TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			return m_saCommand.Field(_T("max_id")).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return -1;
}

// Object evaluation table, all evaluations not calculated; 080514 p�d
BOOL CUMLandValueDB::getObjectEvaluationNotCalculatesForObject(int obj_id,vecTransaction_eval_evaluation &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where eval_object_id=:1 and eval_land_value_stand=0 and eval_typeof_vstand=:2"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsShort()	= EVAL_TYPE_VARDERING;	// "V�rdebest�nd, inl�sat eller fr�n inv/hxl-fil"
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{

			SADateTime saDateTime = m_saCommand.Field(19).asDateTime();
			vec.push_back(CTransaction_eval_evaluation(m_saCommand.Field(1).asLong(),
				m_saCommand.Field(2).asLong(),
				m_saCommand.Field(3).asLong(),
				m_saCommand.Field(4).asString(),
				m_saCommand.Field(5).asDouble(),
				m_saCommand.Field(6).asLong(),
				m_saCommand.Field(7).asString(),
				m_saCommand.Field(8).asDouble(),
				m_saCommand.Field(9).asDouble(),
				m_saCommand.Field(10).asDouble(),
				m_saCommand.Field(11).asDouble(),
				m_saCommand.Field(12).asShort(),
				m_saCommand.Field(14).asShort(),
				m_saCommand.Field(17).asDouble(),
				m_saCommand.Field(18).asDouble(),
				m_saCommand.Field(15).asDouble(),
				m_saCommand.Field(16).asDouble(),
				convertSADateTime(saDateTime),
				m_saCommand.Field(20).asDouble(),
				m_saCommand.Field(_T("eval_length")).asLong(),
				m_saCommand.Field(_T("eval_width")).asLong(),
				m_saCommand.Field(_T("eval_layer")).asLong(),
				m_saCommand.Field(_T("eval_side")).asLong(),
				m_saCommand.Field(_T("eval_overlapping_land")).asBool(),
				m_saCommand.Field(_T("eval_tillf_uttnyttjande")).asBool(),
				//HMS-49 Info om f�rtidig avverkning 20191126 J�
				m_saCommand.Field(_T("eval_info_precut_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_corrfact")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_reduced")).asDouble(),
				//HMS-48 Info om markv�rde 20200427 J�
				m_saCommand.Field(_T("eval_markinfo_valuepine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_valuespruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_reducedby")).asDouble()
				));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addObjectEvaluation_entered_data(CTransaction_eval_evaluation &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!evaluationExists(rec))
		{
			sSQL.Format(_T("insert into %s (eval_id,eval_object_id,eval_prop_id,eval_name,eval_areal,eval_age,eval_si_h100,eval_corr_factor,")
				_T("eval_t_part,eval_g_part,eval_l_part,eval_forrest_type,eval_typeof_vstand,eval_volume,eval_length,eval_width,eval_layer,eval_side,eval_overlapping_land,eval_tillf_uttnyttjande) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20)"),TBL_ELV_EVALUATION);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getEValID_pk();
			m_saCommand.Param(2).setAsLong()		= rec.getEValObjID_pk();
			m_saCommand.Param(3).setAsLong()		= rec.getEValPropID_pk();
			m_saCommand.Param(4).setAsString()	= rec.getEValName();
			m_saCommand.Param(5).setAsDouble()	= rec.getEValAreal();
			m_saCommand.Param(6).setAsLong()		= rec.getEValAge();
			m_saCommand.Param(7).setAsString()	= rec.getEValSIH100();
			m_saCommand.Param(8).setAsDouble()	= rec.getEValCorrFactor();
			m_saCommand.Param(9).setAsDouble()	= rec.getEValPinePart();
			m_saCommand.Param(10).setAsDouble()	= rec.getEValSprucePart();
			m_saCommand.Param(11).setAsDouble()	= rec.getEValBirchPart();
			m_saCommand.Param(12).setAsShort()	= rec.getEValForrestType();
			m_saCommand.Param(13).setAsShort()	= rec.getEValType();
			m_saCommand.Param(14).setAsDouble()	= rec.getEValVolume();
			m_saCommand.Param(15).setAsLong()		= rec.getEValLength();
			m_saCommand.Param(16).setAsLong()		= rec.getEValWidth();
			m_saCommand.Param(17).setAsLong()		= rec.getEValLayer();
			m_saCommand.Param(18).setAsLong()		= rec.getEValSide();
			m_saCommand.Param(19).setAsBool()		= rec.getEValOverlappingLand();
			m_saCommand.Param(20).setAsBool()		= rec.getEValTillfUtnyttjande();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!evaluationExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObjectEvaluation_entered_data(CTransaction_eval_evaluation &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set eval_name=:1,eval_areal=:2,eval_age=:3,eval_si_h100=:4,eval_corr_factor=:5,eval_t_part=:6,eval_g_part=:7,eval_l_part=:8,")
			_T("eval_forrest_type=:9,eval_typeof_vstand=:10,eval_volume=:11,eval_length=:12,eval_width=:13,eval_layer=:14,eval_side=:15,eval_overlapping_land=:16,eval_tillf_uttnyttjande=:17  where eval_id=:18 and eval_object_id=:19 and eval_prop_id=:20"),TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getEValName();
		m_saCommand.Param(2).setAsDouble()	= rec.getEValAreal();
		m_saCommand.Param(3).setAsLong()	= rec.getEValAge();
		m_saCommand.Param(4).setAsString()	= rec.getEValSIH100();
		m_saCommand.Param(5).setAsDouble()	= rec.getEValCorrFactor();
		m_saCommand.Param(6).setAsDouble()	= rec.getEValPinePart();
		m_saCommand.Param(7).setAsDouble()	= rec.getEValSprucePart();
		m_saCommand.Param(8).setAsDouble()	= rec.getEValBirchPart();
		m_saCommand.Param(9).setAsShort()		= rec.getEValForrestType();
		m_saCommand.Param(10).setAsShort()	= rec.getEValType();
		m_saCommand.Param(11).setAsDouble()	= rec.getEValVolume();

		m_saCommand.Param(12).setAsLong()	= rec.getEValLength();
		m_saCommand.Param(13).setAsLong()	= rec.getEValWidth();
		m_saCommand.Param(14).setAsLong()	= rec.getEValLayer();
		m_saCommand.Param(15).setAsLong()	= rec.getEValSide();
		m_saCommand.Param(16).setAsBool()	= rec.getEValOverlappingLand();
		m_saCommand.Param(17).setAsBool()	= rec.getEValTillfUtnyttjande();

		m_saCommand.Param(18).setAsLong()	= rec.getEValID_pk();
		m_saCommand.Param(19).setAsLong()	= rec.getEValObjID_pk();
		m_saCommand.Param(20).setAsLong()	= rec.getEValPropID_pk();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addObjectEvaluation_other_data(CTransaction_eval_evaluation &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!evaluationExists(rec))
		{
			sSQL.Format(_T("insert into %s (eval_id,eval_object_id,eval_prop_id,eval_name,eval_areal,eval_age,eval_forrest_type,eval_typeof_vstand,")
				_T("eval_land_value_ha,eval_early_cutting_ha,eval_land_value_stand,eval_early_cutting_stand,eval_overlapping_land,eval_tillf_uttnyttjande) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)"),TBL_ELV_EVALUATION);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getEValID_pk();
			m_saCommand.Param(2).setAsLong()	= rec.getEValObjID_pk();
			m_saCommand.Param(3).setAsLong()	= rec.getEValPropID_pk();
			m_saCommand.Param(4).setAsString()	= rec.getEValName();
			m_saCommand.Param(5).setAsDouble()	= rec.getEValAreal();
			m_saCommand.Param(6).setAsLong()	= rec.getEValAge();
			m_saCommand.Param(7).setAsShort()	= rec.getEValForrestType();
			m_saCommand.Param(8).setAsShort()	= rec.getEValType();
			m_saCommand.Param(9).setAsDouble()	= rec.getEValLandValue_ha();
			m_saCommand.Param(10).setAsDouble()	= rec.getEValPreCut_ha();
			m_saCommand.Param(11).setAsDouble()	= rec.getEValLandValue();
			m_saCommand.Param(12).setAsDouble()	= rec.getEValPreCut();
			m_saCommand.Param(13).setAsBool()	= rec.getEValOverlappingLand();
			m_saCommand.Param(14).setAsBool()	= rec.getEValTillfUtnyttjande();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!evaluationExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObjectEvaluation_other_data(CTransaction_eval_evaluation &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set eval_name=:1,eval_areal=:2,eval_age=:3,eval_forrest_type=:4,eval_typeof_vstand=:5,")
			_T("eval_land_value_ha=:6,eval_early_cutting_ha=:7,eval_land_value_stand=:8,eval_early_cutting_stand=:9,eval_overlapping_land=:10,eval_tillf_uttnyttjande=:11 ")
								_T("where eval_id=:12 and eval_object_id=:13 and eval_prop_id=:14"),TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getEValName();
		m_saCommand.Param(2).setAsDouble()	= rec.getEValAreal();
		m_saCommand.Param(3).setAsLong()	= rec.getEValAge();
		m_saCommand.Param(4).setAsShort()	= rec.getEValForrestType();
		m_saCommand.Param(5).setAsShort()	= rec.getEValType();
		m_saCommand.Param(6).setAsDouble()	= rec.getEValLandValue_ha();
		m_saCommand.Param(7).setAsDouble()	= rec.getEValPreCut_ha();
		m_saCommand.Param(8).setAsDouble()	= rec.getEValLandValue();
		m_saCommand.Param(9).setAsDouble()	= rec.getEValPreCut();
		m_saCommand.Param(10).setAsBool()	= rec.getEValOverlappingLand();
		m_saCommand.Param(11).setAsBool()	= rec.getEValTillfUtnyttjande();
		m_saCommand.Param(12).setAsLong()	= rec.getEValID_pk();
		m_saCommand.Param(13).setAsLong()	= rec.getEValObjID_pk();
		m_saCommand.Param(14).setAsLong()	= rec.getEValPropID_pk();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updObjectEvaluation_calculated_data(int eval_id,
														 int prop_id,
														 int obj_id,
														 int eval_type,
														 double land_value_ha,
														 double early_cut_ha,
														 double land_value_stand,
														 double early_cut_stand,
														 //HMS-49 Info om f�rtidig avverkning 20191126 J�
														 double fPreCutInfo_P30_Pine,
														 double fPreCutInfo_P30_Spruce,
														 double fPreCutInfo_P30_Birch,
														 double fPreCutInfo_Andel_Pine,
														 double fPreCutInfo_Andel_Spruce,
														 double fPreCutInfo_Andel_Birch,
														 double fPreCutInfo_CorrFact,
														 double fPreCutInfo_Ers_Pine,
														 double fPreCutInfo_Ers_Spruce,
														 double fPreCutInfo_Ers_Birch,
														 double fPreCutInfo_ReducedBy,
														 //HMS-48 Info om markv�rde 20200427 J�
														 double fMarkInfo_ValuePine,
														 double fMarkInfo_ValueSpruce,
														 double fMarkInfo_Andel_Pine,
														 double fMarkInfo_Andel_Spruce,
														 double fMarkInfo_P30_Pine,
														 double fMarkInfo_P30_Spruce,
														 double fMarkInfo_ReducedBy
														 )
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set eval_land_value_ha=:1,eval_early_cutting_ha=:2,eval_land_value_stand=:3,eval_early_cutting_stand=:4,eval_typeof_vstand=:5,")
			_T("eval_info_precut_p30_pine=:6,eval_info_precut_p30_spruce=:7,eval_info_precut_p30_birch=:8,eval_info_precut_andel_pine=:9,eval_info_precut_andel_spruce=:10,")
			_T("eval_info_precut_andel_birch=:11,eval_info_precut_corrfact=:12,eval_info_precut_ers_pine=:13,eval_info_precut_ers_spruce=:14,eval_info_precut_ers_birch=:15,eval_info_precut_reduced=:16,")
			_T("eval_markinfo_valuepine=:17,eval_markinfo_valuespruce=:18,eval_markinfo_andel_pine=:19,eval_markinfo_andel_spruce=:20,eval_markinfo_p30_pine=:21,eval_markinfo_p30_spruce=:22,eval_markinfo_reducedby=:23  ")
			_T("where eval_id=:24 and eval_object_id=:25 and eval_prop_id=:26 "),TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= land_value_ha;
		m_saCommand.Param(2).setAsDouble()	= early_cut_ha;
		m_saCommand.Param(3).setAsDouble()	= land_value_stand;
		m_saCommand.Param(4).setAsDouble()	= early_cut_stand;
		m_saCommand.Param(5).setAsShort()	= eval_type;

		m_saCommand.Param(6).setAsDouble()	= fPreCutInfo_P30_Pine;
		m_saCommand.Param(7).setAsDouble()	= fPreCutInfo_P30_Spruce;
		m_saCommand.Param(8).setAsDouble()	= fPreCutInfo_P30_Birch;
		m_saCommand.Param(9).setAsDouble()	= fPreCutInfo_Andel_Pine;
		m_saCommand.Param(10).setAsDouble()	= fPreCutInfo_Andel_Spruce;
		m_saCommand.Param(11).setAsDouble()	= fPreCutInfo_Andel_Birch;
		m_saCommand.Param(12).setAsDouble()	= fPreCutInfo_CorrFact;
		m_saCommand.Param(13).setAsDouble()	= fPreCutInfo_Ers_Pine;
		m_saCommand.Param(14).setAsDouble()	= fPreCutInfo_Ers_Spruce;
		m_saCommand.Param(15).setAsDouble()	= fPreCutInfo_Ers_Birch;
		m_saCommand.Param(16).setAsDouble()	= fPreCutInfo_ReducedBy;

		//HMS-48 Info om markv�rde 20200427 J�
		m_saCommand.Param(17).setAsDouble()	= fMarkInfo_ValuePine;
		m_saCommand.Param(18).setAsDouble()	= fMarkInfo_ValueSpruce;
		m_saCommand.Param(19).setAsDouble()	= fMarkInfo_Andel_Pine;
		m_saCommand.Param(20).setAsDouble()	= fMarkInfo_Andel_Spruce;
		m_saCommand.Param(21).setAsDouble()	= fMarkInfo_P30_Pine;
		m_saCommand.Param(22).setAsDouble()	= fMarkInfo_P30_Spruce;
		m_saCommand.Param(23).setAsDouble()	= fMarkInfo_ReducedBy;


		m_saCommand.Param(24).setAsLong()	= eval_id;
		m_saCommand.Param(25).setAsLong()	= obj_id;
		m_saCommand.Param(26).setAsLong()	= prop_id;


		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjectEvaluation_calculated_corrfac(int eval_id,int obj_id,int prop_id,double corr_fac)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set eval_corr_factor=:1 where eval_id=:2 and eval_object_id=:3 and eval_prop_id=:4 and (eval_typeof_vstand=:5 or eval_typeof_vstand=:6)"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= corr_fac;
		m_saCommand.Param(2).setAsLong()	= eval_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;
		m_saCommand.Param(4).setAsLong()	= prop_id;
		m_saCommand.Param(5).setAsShort()	= EVAL_TYPE_VARDERING;
		m_saCommand.Param(6).setAsShort()	= EVAL_TYPE_FROM_TAXERING;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delObjectEvaluation(int eval_id,int obj_id,int prop_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where eval_id=:1 and eval_object_id=:2 and eval_prop_id=:3"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= eval_id;
		m_saCommand.Param(2).setAsLong()		= obj_id;
		m_saCommand.Param(3).setAsLong()		= prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}

BOOL CUMLandValueDB::delObjectEvaluation(int obj_id,int prop_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where eval_object_id=:1 and eval_prop_id=:2"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}


BOOL CUMLandValueDB::delObjectEvaluations(int obj_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where eval_object_id=:1"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}

BOOL CUMLandValueDB::getObjectEvaluation_last_id(int obj_id,int prop_id,int *id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select max(eval_id) from %s where eval_prop_id=:1 and eval_object_id=:2"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = prop_id;
		m_saCommand.Param(2).setAsLong() = obj_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*id = m_saCommand.Field(1).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delObjectEvaluation_cruise(int obj_id,int prop_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where eval_object_id=:1 and eval_prop_id=:2 and eval_typeof_vstand=:3"),
						TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Param(3).setAsShort() = EVAL_TYPE_FROM_TAXERING;	// Cruise stand
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}



// OBJECT TEMPLATES (P30,"F�rdyrad avverkning"); 080402 p�d

BOOL CUMLandValueDB::getObjectTemplates(vecTransactionTemplate &vec,int tmpl_type)
{

	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where type_of=:1 order by id"),TBL_ELV_TEMPLATE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = tmpl_type;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asLong(),
												m_saCommand.Field(4).asLongChar(),
												m_saCommand.Field(5).asLongChar(),
												m_saCommand.Field(6).asString(),
												convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addObjectTemplate(CTransaction_template &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!objectTemplExist(rec))
		{
			sSQL.Format(_T("insert into %s (name,type_of,template,notes,created_by) values(:1,:2,:3,:4,:5)"),
							TBL_ELV_TEMPLATE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
			m_saCommand.Param(2).setAsLong()	= rec.getTypeOf();
			m_saCommand.Param(3).setAsLongChar()	= rec.getTemplateFile();
			m_saCommand.Param(4).setAsLongChar()	= rec.getTemplateNotes();
			m_saCommand.Param(5).setAsString()	= rec.getCreatedBy();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!objectTemplExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObjectTemplate(CTransaction_template &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set name=:1,type_of=:2,created_by=:3,template=:4,notes=:5 where id=:6"),
						TBL_ELV_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
		m_saCommand.Param(2).setAsLong()	= rec.getTypeOf();
		m_saCommand.Param(3).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(4).setAsLongChar()	= rec.getTemplateFile();
		m_saCommand.Param(5).setAsLongChar()	= rec.getTemplateNotes();
		m_saCommand.Param(6).setAsLong()	= rec.getID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delObjectTemplate(CTransaction_template &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where id=:1"),
						TBL_ELV_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Object dokument templates; 080610 p�d
BOOL CUMLandValueDB::getObjectDocumentTemplates(vecTransactionTemplate &vec)
{

	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s order by id"),
						TBL_ELV_DOC_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asLong(),
												m_saCommand.Field(4).asLongChar(),
												_T(""),	// Not used
												m_saCommand.Field(5).asString(),
												convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addObjectDocumentTemplate(CTransaction_template &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!objectDocumentTemplExist(rec))
		{
			sSQL.Format(_T("insert into %s (name,type_of,template,created_by) values(:1,:2,:3,:4)"),
							TBL_ELV_DOC_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
			m_saCommand.Param(2).setAsLong()	= rec.getTypeOf();
			m_saCommand.Param(3).setAsLongChar()	= rec.getTemplateFile();
			m_saCommand.Param(4).setAsString()	= rec.getCreatedBy();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!objectDocumentTemplExist(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updObjectDocumentTemplate(CTransaction_template &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set name=:1,type_of=:2,created_by=:3,template=:4 where id=:5"),
						TBL_ELV_DOC_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
		m_saCommand.Param(2).setAsLong()	= rec.getTypeOf();
		m_saCommand.Param(3).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(4).setAsLongChar()	= rec.getTemplateFile();
		m_saCommand.Param(5).setAsLong()	= rec.getID();

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delObjectDocumentTemplate(CTransaction_template &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where id=:1"),
						TBL_ELV_DOC_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


// TRAKR

BOOL CUMLandValueDB::getTrakt(int trakt_id,CTransaction_trakt &rec)
{
	BOOL bReturnValue = FALSE;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where trakt_id=:1 order by trakt_id"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			bReturnValue = TRUE;
			rec = CTransaction_trakt(m_saCommand.Field(1).asLong(),
										m_saCommand.Field(2).asString(),
										m_saCommand.Field(3).asString(),
										m_saCommand.Field(4).asString(),
										m_saCommand.Field(5).asString(),
										m_saCommand.Field(6).asString(),
										m_saCommand.Field(7).asLong(),
										m_saCommand.Field(8).asDouble(),
										m_saCommand.Field(9).asDouble(),
										m_saCommand.Field(10).asDouble(),
										m_saCommand.Field(11).asLong(),
										m_saCommand.Field(12).asString(),
										m_saCommand.Field(13).asDouble(),
										m_saCommand.Field(14).asLong(),
										m_saCommand.Field(15).asLong(),
										m_saCommand.Field(16).asLong(),
										m_saCommand.Field(17).asLong(),
										m_saCommand.Field(18).asString(),
										m_saCommand.Field(19).asLong(),
										m_saCommand.Field(20).asLong(),
										m_saCommand.Field(21).asString(),
										m_saCommand.Field(22).asString(),
										m_saCommand.Field(23).asString(),
										m_saCommand.Field(24).asString(),
										m_saCommand.Field(25).asString(),
										m_saCommand.Field(26).asString(),
										m_saCommand.Field(27).asLong(),
										m_saCommand.Field(_T("trakt_near_coast")).asLong(),
										m_saCommand.Field(_T("trakt_southeast")).asLong(),
										m_saCommand.Field(_T("trakt_region5")).asLong(),
										m_saCommand.Field(_T("trakt_part_of_plot")).asLong(),
										m_saCommand.Field(_T("trakt_si_h100_pine")).asString(),
										m_saCommand.Field(_T("trakt_prop_num")).asString(),
										m_saCommand.Field(_T("trakt_prop_name")).asString(),
										m_saCommand.Field(_T("trakt_notes")).asLongChar(),
										m_saCommand.Field(_T("trakt_created_by")).asString(),
										m_saCommand.Field(_T("created")).asString(),
										m_saCommand.Field(_T("trakt_wside")).asShort(),
										m_saCommand.Field(_T("trakt_for_stand_only")).asShort(),
										m_saCommand.Field(_T("trakt_length")).asLong(),
										m_saCommand.Field(_T("trakt_width")).asLong(),
										m_saCommand.Field(_T("trakt_tillfutnyttj")).asBool(),
										m_saCommand.Field(_T("trakt_point")).asString(),
										m_saCommand.Field(_T("trakt_coordinates")).asString()
										);
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return 	bReturnValue;
}

BOOL CUMLandValueDB::getTrakts_match_stand(int obj_id,int prop_id,vecTransactionTrakt &vec)
{
	BOOL bReturnValue = FALSE;
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select b.* from %s a,%s b where a.prop_object_id=:1 and a.prop_id=:2 and b.trakt_prop_id=a.prop_id order by b.trakt_name"),
						TBL_ELV_PROP,TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		m_saCommand.Param(2).setAsLong()		= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			bReturnValue = TRUE;
			vec.push_back(CTransaction_trakt(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asString(),
												m_saCommand.Field(4).asString(),
												m_saCommand.Field(5).asString(),
												m_saCommand.Field(6).asString(),
												m_saCommand.Field(7).asLong(),
												m_saCommand.Field(8).asDouble(),
												m_saCommand.Field(9).asDouble(),
												m_saCommand.Field(10).asDouble(),
												m_saCommand.Field(11).asLong(),
												m_saCommand.Field(12).asString(),
												m_saCommand.Field(13).asDouble(),
												m_saCommand.Field(14).asLong(),
												m_saCommand.Field(15).asLong(),
												m_saCommand.Field(16).asLong(),
												m_saCommand.Field(17).asLong(),
												m_saCommand.Field(18).asString(),
												m_saCommand.Field(19).asLong(),
												m_saCommand.Field(20).asLong(),
												m_saCommand.Field(21).asString(),
												m_saCommand.Field(22).asString(),
												m_saCommand.Field(23).asString(),
												m_saCommand.Field(24).asString(),
												m_saCommand.Field(25).asString(),
												m_saCommand.Field(26).asString(),
												m_saCommand.Field(27).asLong(),
												m_saCommand.Field(_T("trakt_near_coast")).asLong(),
												m_saCommand.Field(_T("trakt_southeast")).asLong(),
												m_saCommand.Field(_T("trakt_region5")).asLong(),
												m_saCommand.Field(_T("trakt_part_of_plot")).asLong(),
												m_saCommand.Field(_T("trakt_si_h100_pine")).asString(),
												m_saCommand.Field(_T("trakt_prop_num")).asString(),
												m_saCommand.Field(_T("trakt_prop_name")).asString(),
												m_saCommand.Field(_T("trakt_notes")).asLongChar(),
												m_saCommand.Field(_T("trakt_created_by")).asString(),
												m_saCommand.Field(_T("created")).asString(),
												m_saCommand.Field(_T("trakt_wside")).asShort(),
												m_saCommand.Field(_T("trakt_for_stand_only")).asShort(),
												m_saCommand.Field(_T("trakt_length")).asLong(),
												m_saCommand.Field(_T("trakt_width")).asLong(),
												m_saCommand.Field(_T("trakt_tillfutnyttj")).asBool(),
												m_saCommand.Field(_T("trakt_point")).asString(),
												m_saCommand.Field(_T("trakt_coordinates")).asString()
												));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return 	bReturnValue;
}

// Method added 2008-11-03 p�d
// Only match srand in esti_trakt_table that's also in elv_cruise_table, for Object and Property; 081103 p�d
BOOL CUMLandValueDB::getTrakts_match_stand_by_elv_cruise(int obj_id,int prop_id,vecTransactionTrakt &vec)
{
	BOOL bReturnValue = FALSE;
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.* from %s a,%s b where b.ecru_object_id=:1 and b.ecru_prop_id=:2 and a.trakt_id=b.ecru_id order by a.trakt_name"),TBL_TRAKT,TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		m_saCommand.Param(2).setAsLong()		= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			bReturnValue = TRUE;
			vec.push_back(CTransaction_trakt(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asString(),
												m_saCommand.Field(4).asString(),
												m_saCommand.Field(5).asString(),
												m_saCommand.Field(6).asString(),
												m_saCommand.Field(7).asLong(),
												m_saCommand.Field(8).asDouble(),
												m_saCommand.Field(9).asDouble(),
												m_saCommand.Field(10).asDouble(),
												m_saCommand.Field(11).asLong(),
												m_saCommand.Field(12).asString(),
												m_saCommand.Field(13).asDouble(),
												m_saCommand.Field(14).asLong(),
												m_saCommand.Field(15).asLong(),
												m_saCommand.Field(16).asLong(),
												m_saCommand.Field(17).asLong(),
												m_saCommand.Field(18).asString(),
												m_saCommand.Field(19).asLong(),
												m_saCommand.Field(20).asLong(),
												m_saCommand.Field(21).asString(),
												m_saCommand.Field(22).asString(),
												m_saCommand.Field(23).asString(),
												m_saCommand.Field(24).asString(),
												m_saCommand.Field(25).asString(),
												m_saCommand.Field(26).asString(),
												m_saCommand.Field(27).asLong(),
												m_saCommand.Field(_T("trakt_near_coast")).asLong(),
												m_saCommand.Field(_T("trakt_southeast")).asLong(),
												m_saCommand.Field(_T("trakt_region5")).asLong(),
												m_saCommand.Field(_T("trakt_part_of_plot")).asLong(),
												m_saCommand.Field(_T("trakt_si_h100_pine")).asString(),
												m_saCommand.Field(_T("trakt_prop_num")).asString(),
												m_saCommand.Field(_T("trakt_prop_name")).asString(),
												m_saCommand.Field(_T("trakt_notes")).asLongChar(),
												m_saCommand.Field(_T("trakt_created_by")).asString(),
												m_saCommand.Field(_T("created")).asString(),
												m_saCommand.Field(_T("trakt_wside")).asShort(),
												m_saCommand.Field(_T("trakt_for_stand_only")).asShort(),
												m_saCommand.Field(_T("trakt_length")).asLong(),
												m_saCommand.Field(_T("trakt_width")).asLong(),
												m_saCommand.Field(_T("trakt_tillfutnyttj")).asBool(),
												m_saCommand.Field(_T("trakt_point")).asString(),
												m_saCommand.Field(_T("trakt_coordinates")).asString()
												));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return 	bReturnValue;
}


BOOL CUMLandValueDB::updTrakts_lat_long_hgt_over_sea(int trakt_id,int latitude,int longitude,int hgt_over_sea)
{
	CString sSQL,sNCoord;
	// Ber�kna NordKoordinat RT90; 100426 p�d
	CLatLongToRT90_data recRT90;
	SweRef99LatLongtoRT90xy(&(recRT90 = CLatLongToRT90_data(latitude*100,longitude*100)));
	sNCoord.Format(L"%.0f",recRT90.getX()/100000.0);

	try
	{
		sSQL.Format(_T("update %s set trakt_latitude=:1,trakt_longitude=:2,trakt_hgt_over_sea=:3,trakt_x_coord=:4 where trakt_id=:5"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= latitude;
		m_saCommand.Param(2).setAsLong()		= longitude;
		m_saCommand.Param(3).setAsLong()		= hgt_over_sea;
		m_saCommand.Param(4).setAsString()	= sNCoord;
		m_saCommand.Param(5).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTrakts_use_for_infr(int trakt_id,int use)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set trakt_for_stand_only=:1 where trakt_id=:2"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= use;
		m_saCommand.Param(2).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delTrakt(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where trakt_id=:1"),TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getPlots(int trakt_id,vecTransactionPlot &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tplot_trakt_id=:1"),
						TBL_TRAKT_PLOT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
				SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
				vec.push_back(CTransaction_plot(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asLong(),
												m_saCommand.Field(3).asString(),
												m_saCommand.Field(4).asLong(),
												m_saCommand.Field(5).asDouble(),
												m_saCommand.Field(6).asDouble(),
												m_saCommand.Field(7).asDouble(),
												m_saCommand.Field(8).asDouble(),
												m_saCommand.Field(9).asString(),
												m_saCommand.Field(10).asLong(),
												convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getLandValueTrees(int trakt_id,int tree_type,vecTransactionSampleTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (tree_type == SAMPLE_TREE ||
			  tree_type == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= SAMPLE_TREE;
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else if (tree_type == TREE_OUTSIDE_NO_SAMP)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
		}

		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_sample_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asLong(),
													m_saCommand.Field(12).asLong(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(13).asLong(),
													m_saCommand.Field(19).asString(),
													convertSADateTime(saDateTime),
													m_saCommand.Field(_T("ttree_coord")).asString(),
													m_saCommand.Field(_T("ttree_distcable")).asLong(),
													m_saCommand.Field(_T("ttree_category")).asLong(),
													m_saCommand.Field(_T("ttree_topcut")).asLong()
													));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getLandValueRandTrees(int trakt_id,vecTransactionSampleTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
						TBL_TRAKT_SAMPLE_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE;
		m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_sample_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asLong(),
													m_saCommand.Field(12).asLong(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(13).asLong(),
													m_saCommand.Field(19).asString(),
													convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getDCLSTreesforJsonExport(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1 order by tdcls_spc_id,tdcls_dcls_from"),
						TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asLong(),
													m_saCommand.Field(17).asLong(),
													0,
													convertSADateTime(saDateTime)));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getDCLSTrees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1"),
						TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asLong(),
													m_saCommand.Field(17).asLong(),
													0,
													convertSADateTime(saDateTime)));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getDCLS1Trees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls1_trakt_id=:1"),
						TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asLong(),
													m_saCommand.Field(17).asLong(),
													0,
													convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getDCLS2Trees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls2_trakt_id=:1"),
						TBL_TRAKT_DCLS2_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asLong(),
													m_saCommand.Field(17).asLong(),
													0,
													convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		 AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getTraktData(vecTransactionTraktData &vec,int trakt_id,int data_type)
{
	CString sSQL;
	try
	{
		vec.clear();
		// If trakt_id	> -1, use trakt_id to select
		// a specific trakt data; 070309 p�d
		if (trakt_id == -1)
			sSQL.Format(_T("select * from %s order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA);
		else if (trakt_id > -1 && data_type == -1)
			sSQL.Format(_T("select * from %s where tdata_trakt_id=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,trakt_id);
		else if (trakt_id > -1 && data_type > -1)
			sSQL.Format(_T("select * from %s where tdata_trakt_id=%d and tdata_data_type=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,trakt_id,data_type);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_trakt_data(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asShort(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asLong(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(22).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asDouble(),
													m_saCommand.Field(20).asDouble(),
													m_saCommand.Field(23).asDouble(),
													m_saCommand.Field(21).asString()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getTraktMiscData(int trakt_id,CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tprl_trakt_id=:1"),TBL_TRAKT_MISC_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_trakt_misc_data(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asLong(),
												m_saCommand.Field(4).asLongChar(),
												m_saCommand.Field(5).asString(),
												m_saCommand.Field(6).asLong(),
												m_saCommand.Field(7).asLongChar(),
												m_saCommand.Field(8).asDouble(),
												convertSADateTime(saDateTime));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Update information for a specific Trakt, for Pricelist,Costs and Diameterclass; 080507 p�d
BOOL CUMLandValueDB::updTraktMiscData(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tprl_name=:1,tprl_typeof=:2,tprl_pricelist=:3,tprl_costtmpl_name=:4,tprl_costtmpl_typeof=:5,tprl_costtmpl=:6 where tprl_trakt_id=:7"),
						TBL_TRAKT_MISC_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getName();
		m_saCommand.Param(2).setAsLong()	= rec.getTypeOf();
		m_saCommand.Param(3).setAsLongChar()	= rec.getXMLPricelist();
		m_saCommand.Param(4).setAsString()	= rec.getCostsName();
		m_saCommand.Param(5).setAsLong()	= rec.getCostsTypeOf();
		m_saCommand.Param(6).setAsLongChar()	= rec.getXMLCosts();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetTraktID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktDataExists(rec))
		{
			sSQL.Format(_T("insert into %s (tdata_id,tdata_trakt_id,tdata_data_type,tdata_spc_id,tdata_spc_name,tdata_percent,tdata_numof,")
									_T("tdata_da,tdata_dg,tdata_dgv,tdata_hgv,tdata_gy,tdata_avg_hgt,tdata_h25,tdata_m3sk_vol,tdata_m3fub_vol,tdata_m3ub_vol,")
									_T("tdata_avg_m3sk_vol,tdata_avg_m3fub_vol,tdata_avg_m3ub_vol) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20)"),TBL_TRAKT_DATA);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTDataID();
			m_saCommand.Param(2).setAsLong()	= rec.getTDataTraktID();
			m_saCommand.Param(3).setAsShort()	= rec.getTDataType();
			m_saCommand.Param(4).setAsLong()	= rec.getSpecieID();
			m_saCommand.Param(5).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(6).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(7).setAsLong()	= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()	= rec.getDA();
			m_saCommand.Param(9).setAsDouble()	= rec.getDG();
			m_saCommand.Param(10).setAsDouble()	= rec.getDGV();
			m_saCommand.Param(11).setAsDouble()	= rec.getHGV();
			m_saCommand.Param(12).setAsDouble()	= rec.getGY();
			m_saCommand.Param(13).setAsDouble()	= rec.getAvgHgt();
			m_saCommand.Param(14).setAsDouble()	= rec.getH25();
			m_saCommand.Param(15).setAsDouble()	= rec.getM3SK();
			m_saCommand.Param(16).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(17).setAsDouble()	= rec.getM3UB();
			m_saCommand.Param(18).setAsDouble()	= rec.getAvgM3SK();
			m_saCommand.Param(19).setAsDouble()	= rec.getAvgM3FUB();
			m_saCommand.Param(20).setAsDouble()	= rec.getAvgM3UB();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktDataExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tdata_spc_id=:1,tdata_spc_name=:2,tdata_percent=:3,tdata_numof=:4,tdata_da=:5,tdata_dg=:6,tdata_dgv=:7,tdata_hgv=:8,tdata_gy=:9,tdata_avg_hgt=:10,")
								_T("tdata_h25=:11,tdata_m3sk_vol=:12,tdata_m3fub_vol=:13,tdata_m3ub_vol=:14,tdata_avg_m3sk_vol=:15,tdata_avg_m3fub_vol=:16,tdata_avg_m3ub_vol=:17 ")
								_T("where tdata_id=:18 and tdata_trakt_id=:19 and tdata_data_type=:20"),TBL_TRAKT_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= rec.getSpecieID();
		m_saCommand.Param(2).setAsString()	= rec.getSpecieName();
		m_saCommand.Param(3).setAsDouble()	= rec.getPercent();
		m_saCommand.Param(4).setAsLong()	= rec.getNumOf();
		m_saCommand.Param(5).setAsDouble()	= rec.getDA();
		m_saCommand.Param(6).setAsDouble()	= rec.getDG();
		m_saCommand.Param(7).setAsDouble()	= rec.getDGV();
		m_saCommand.Param(8).setAsDouble()	= rec.getHGV();
		m_saCommand.Param(9).setAsDouble()	= rec.getGY();
		m_saCommand.Param(10).setAsDouble()	= rec.getAvgHgt();
		m_saCommand.Param(11).setAsDouble()	= rec.getH25();
		m_saCommand.Param(12).setAsDouble()	= rec.getM3SK();
		m_saCommand.Param(13).setAsDouble()	= rec.getM3FUB();
		m_saCommand.Param(14).setAsDouble()	= rec.getM3UB();
		m_saCommand.Param(15).setAsDouble()	= rec.getAvgM3SK();
		m_saCommand.Param(16).setAsDouble()	= rec.getAvgM3FUB();
		m_saCommand.Param(17).setAsDouble()	= rec.getAvgM3UB();
		m_saCommand.Param(18).setAsLong()	= rec.getTDataID();
		m_saCommand.Param(19).setAsLong()	= rec.getTDataTraktID();
		m_saCommand.Param(20).setAsShort()	= rec.getTDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktData_h25(int trakt_id,int spc_id,double h25)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	if (h25 == 0.0) return FALSE;

	try
	{
		sSQL.Format(_T("update %s set tdata_h25=:1 where tdata_trakt_id=:2 and tdata_data_type=:3 and tdata_spc_id=:4"),TBL_TRAKT_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= h25;
		m_saCommand.Param(2).setAsLong()	= trakt_id;
		m_saCommand.Param(3).setAsShort()	= STMP_LEN_WITHDRAW;	// "Uttag"
		m_saCommand.Param(4).setAsLong()	= spc_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktData_gcrown(int trakt_id,int spc_id,double gcrown)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tdata_gcrown=:1 where tdata_trakt_id=:2 and tdata_data_type=:3 and tdata_spc_id=:4"),TBL_TRAKT_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= gcrown;
		m_saCommand.Param(2).setAsLong()	= trakt_id;
		m_saCommand.Param(3).setAsShort()	= STMP_LEN_WITHDRAW;	// "Uttag"
		m_saCommand.Param(4).setAsLong()	= spc_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Only update information about "S�derbergs" and "SI H100"
BOOL CUMLandValueDB::updTraktData_soderbergs(int trakt_id,BOOL near_coast,BOOL southeast,BOOL region5,BOOL part_of_plot)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set trakt_near_coast=:1,trakt_southeast=:2,trakt_region5=:3,trakt_part_of_plot=:4 where trakt_id=:6"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()	= (near_coast ? 1 : 0);
		m_saCommand.Param(2).setAsShort()	= (southeast ? 1 : 0);
		m_saCommand.Param(3).setAsShort()	= (region5 ? 1 : 0);
		m_saCommand.Param(4).setAsShort()	= (part_of_plot ? 1 : 0);
		m_saCommand.Param(6).setAsLong()	= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::removeTraktDataSpecies(int trakt_id,LPCTSTR sql)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s %s"),
			TBL_TRAKT_DATA,sql);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getSampleTrees(int trakt_id,int tree_type,vecTransactionSampleTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (tree_type == SAMPLE_TREE ||
			  tree_type == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;
			m_saCommand.Param(2).setAsShort() = SAMPLE_TREE;
			m_saCommand.Param(3).setAsShort() = TREE_OUTSIDE;
		}
		else if (tree_type == TREE_OUTSIDE_NO_SAMP)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;
			m_saCommand.Param(2).setAsShort() = TREE_OUTSIDE_NO_SAMP;
		}
		else if (tree_type == TREE_SAMPLE_REMAINIG)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;
			m_saCommand.Param(2).setAsShort() = TREE_SAMPLE_REMAINIG;
		}
		else if (tree_type == TREE_SAMPLE_EXTRA)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;
			m_saCommand.Param(2).setAsShort() = TREE_SAMPLE_EXTRA;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;
		}
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_sample_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asLong(),
													m_saCommand.Field(12).asLong(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(13).asLong(),
													m_saCommand.Field(19).asString(),
													convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addSampleTrees(CTransaction_sample_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktSampleTreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (ttree_id,ttree_trakt_id,ttree_plot_id,ttree_spc_id,ttree_spc_name,ttree_dbh,ttree_hgt,ttree_gcrown,ttree_bark_thick,")
										_T("ttree_grot,ttree_age,ttree_growth,ttree_dcls_from,ttree_dcls_to,ttree_m3sk,ttree_m3fub,ttree_m3ub,ttree_tree_type,ttree_bonitet) ")
										_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19)"),TBL_TRAKT_SAMPLE_TREES);
				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
				_stprintf(tmp,_T("%.3f"),rec.getM3Sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3Fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3Ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------

				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDBH();
				m_saCommand.Param(7).setAsDouble()		= fHgt_rounded;
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(11).setAsLong()		= rec.getAge();
				m_saCommand.Param(12).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(13).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(14).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(15).setAsDouble()		= fM3Sk_rounded;
				m_saCommand.Param(16).setAsDouble()		= fM3Fub_rounded;
				m_saCommand.Param(17).setAsDouble()		= fM3Ub_rounded;
				m_saCommand.Param(18).setAsLong()		= rec.getTreeType();
				m_saCommand.Param(19).setAsString()		= rec.getBonitet();
				m_saCommand.Execute();
			}
			bReturn = TRUE;
		}	// if (!traktSampleTreeExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updSampleTrees(CTransaction_sample_tree &rec)
{
	CString sSQL;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{
			sSQL.Format(_T("update %s set ttree_plot_id=:1,ttree_spc_id=:2,ttree_spc_name=:3,ttree_dbh=:4,ttree_hgt=:5,ttree_gcrown=:6,ttree_bark_thick=:7,")
									_T("ttree_grot=:8,ttree_age=:9,ttree_growth=:10,ttree_dcls_from=:11,ttree_dcls_to=:12,ttree_m3sk=:13,ttree_m3fub=:14,ttree_m3ub=:15,ttree_tree_type=:16,ttree_bonitet=:17 ")
								  _T("where ttree_id=:18 and ttree_trakt_id=:19"),TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
			_stprintf(tmp,_T("%.3f"),rec.getM3Sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3Fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3Ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------

			m_saCommand.Param(1).setAsLong()		= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDBH();
			m_saCommand.Param(5).setAsDouble()		= fHgt_rounded;
			m_saCommand.Param(6).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(7).setAsDouble()		= fBark_rounded;
			m_saCommand.Param(8).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(9).setAsLong()		= rec.getAge();
			m_saCommand.Param(10).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(11).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(12).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(13).setAsDouble()		= fM3Sk_rounded;
			m_saCommand.Param(14).setAsDouble()		= fM3Fub_rounded;
			m_saCommand.Param(15).setAsDouble()		= fM3Ub_rounded;
			m_saCommand.Param(16).setAsLong()		= rec.getTreeType();
			m_saCommand.Param(17).setAsString()		= rec.getBonitet();
			m_saCommand.Param(18).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(19).setAsLong()		= rec.getTraktID();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::addDCLSTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLSTreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls_id,tdcls_trakt_id,tdcls_plot_id,tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,tdcls_hgt,tdcls_numof,tdcls_gcrown,tdcls_bark_thick,")
										_T("tdcls_m3sk,tdcls_m3fub,tdcls_m3ub,tdcls_grot,tdcls_age,tdcls_growth) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),
										TBL_TRAKT_DCLS_TREES);
				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------

				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Execute();
			}
			bReturn = TRUE;
		}	// if (!traktDCLSTreeExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updDCLSTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{

			sSQL.Format(_T("update %s set tdcls_plot_id=:1,tdcls_spc_id=:2,tdcls_spc_name=:3,tdcls_dcls_from=:4,tdcls_dcls_to=:5,tdcls_hgt=:6,tdcls_numof=:7,")
									_T("tdcls_gcrown=:8,tdcls_bark_thick=:9,tdcls_m3sk=:10,tdcls_m3fub=:11,tdcls_m3ub=:12,tdcls_grot=:13,tdcls_age=:14,tdcls_growth=:15 where tdcls_id=:16 and tdcls_trakt_id=:17"),
									 TBL_TRAKT_DCLS_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
			_stprintf(tmp,_T("%.3f"),rec.getM3sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------

			m_saCommand.Param(1).setAsLong()		= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(6).setAsDouble()		= fHgt_rounded;
			m_saCommand.Param(7).setAsLong()		= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(9).setAsDouble()		= fBark_rounded;
			m_saCommand.Param(10).setAsDouble()		= fM3Sk_rounded;
			m_saCommand.Param(11).setAsDouble()		= fM3Fub_rounded;
			m_saCommand.Param(12).setAsDouble()		= fM3Ub_rounded;
			m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(14).setAsLong()		= rec.getAge();
			m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(17).setAsLong()		= rec.getTraktID();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delDCLSTreesInTrakt(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls_trakt_id=:1"),
						TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}




BOOL CUMLandValueDB::addDCLS1Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLS1TreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls1_id,tdcls1_trakt_id,tdcls1_plot_id,tdcls1_spc_id,tdcls1_spc_name,tdcls1_dcls_from,tdcls1_dcls_to,")
										_T("tdcls1_hgt,tdcls1_numof,tdcls1_gcrown,tdcls1_bark_thick,tdcls1_m3sk,tdcls1_m3fub,tdcls1_m3ub,tdcls1_grot,tdcls1_age,tdcls1_growth) ")
										_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),TBL_TRAKT_DCLS1_TREES);
				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------

				m_saCommand.Param(1).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()		= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()		= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()		= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= fHgt_rounded;
				m_saCommand.Param(9).setAsLong()		= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Execute();
			}
		}	// if (!traktDCLS1TreeExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updDCLS1Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{

			sSQL.Format(_T("update %s set tdcls1_plot_id=:1,tdcls1_spc_id=:2,tdcls1_spc_name=:3,tdcls1_dcls_from=:4,tdcls1_dcls_to=:5,tdcls1_hgt=:6,tdcls1_numof=:7,")
									_T("tdcls1_gcrown=:8,tdcls1_bark_thick=:9,tdcls1_m3sk=:10,tdcls1_m3fub=:11,tdcls1_m3ub=:12,tdcls1_grot=:13,tdcls1_age=:14,tdcls1_growth=:15 where tdcls1_id=:16 and tdcls1_trakt_id=:17"),
									 TBL_TRAKT_DCLS1_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
			_stprintf(tmp,_T("%.3f"),rec.getM3sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------

			m_saCommand.Param(1).setAsLong()		= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(6).setAsDouble()		= fHgt_rounded;
			m_saCommand.Param(7).setAsLong()		= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(9).setAsDouble()		= fBark_rounded;
			m_saCommand.Param(10).setAsDouble()		= fM3Sk_rounded;
			m_saCommand.Param(11).setAsDouble()		= fM3Fub_rounded;
			m_saCommand.Param(12).setAsDouble()		= fM3Ub_rounded;
			m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(14).setAsLong()		= rec.getAge();
			m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(17).setAsLong()		= rec.getTraktID();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delDCLS1TreesInTrakt(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls1_trakt_id=:1"),
						TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::addDCLS2Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLS2TreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls2_id,tdcls2_trakt_id,tdcls2_plot_id,tdcls2_spc_id,tdcls2_spc_name,tdcls2_dcls_from,tdcls2_dcls_to,")
										_T("tdcls2_hgt,tdcls2_numof,tdcls2_gcrown,tdcls2_bark_thick,tdcls2_m3sk,tdcls2_m3fub,tdcls2_m3ub,tdcls2_grot,tdcls2_age,tdcls2_growth) ")
										_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),TBL_TRAKT_DCLS2_TREES);
				m_saCommand.setCommandText((SAString)sSQL);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------

				m_saCommand.Param(1).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()		= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()		= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()		= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= fHgt_rounded;
				m_saCommand.Param(9).setAsLong()		= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Execute();
			}
			bReturn = TRUE;
		}	// if (!traktDCLS2TreeExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updDCLS2Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	double fM3Sk_rounded = 0.0;
	double fM3Fub_rounded = 0.0;
	double fM3Ub_rounded = 0.0;
	double fBark_rounded = 0.0;
	double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{

			sSQL.Format(_T("update %s set tdcls2_plot_id=:1,tdcls2_spc_id=:2,tdcls2_spc_name=:3,tdcls2_dcls_from=:4,tdcls2_dcls_to=:5,tdcls2_hgt=:6,tdcls2_numof=:7,")
									_T("tdcls2_gcrown=:8,tdcls2_bark_thick=:9,tdcls2_m3sk=:10,tdcls2_m3fub=:11,tdcls2_m3ub=:12,tdcls2_grot=:13,tdcls2_age=:14,tdcls2_growth=:15 where tdcls2_id=:16 and tdcls2_trakt_id=:17"),
									 TBL_TRAKT_DCLS2_TREES);

			m_saCommand.setCommandText((SAString)sSQL);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
			_stprintf(tmp,_T("%.3f"),rec.getM3sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------

			m_saCommand.Param(1).setAsLong()		= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()		= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(6).setAsDouble()		= fHgt_rounded;
			m_saCommand.Param(7).setAsLong()		= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(9).setAsDouble()		= fBark_rounded;
			m_saCommand.Param(10).setAsDouble()		= fM3Sk_rounded;
			m_saCommand.Param(11).setAsDouble()		= fM3Fub_rounded;
			m_saCommand.Param(12).setAsDouble()		= fM3Ub_rounded;
			m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(14).setAsLong()		= rec.getAge();
			m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(17).setAsLong()		= rec.getTraktID();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delDCLS2TreesInTrakt(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls2_trakt_id=:1"),
						TBL_TRAKT_DCLS2_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getTraktSpcDistinct(int trakt_id,int type_of,vecInt& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		if (type_of == STMP_LEN_WITHDRAW)	// "Uttag"
			sSQL.Format(_T("select distinct tdcls_spc_id from %s where tdcls_trakt_id=:1"),TBL_TRAKT_DCLS_TREES);
		else if (type_of == STMP_LEN_TO_BE_LEFT)	// "Kvarl�mmnat"
			sSQL.Format(_T("select distinct tdcls1_spc_id from %s where tdcls1_trakt_id=:1"),TBL_TRAKT_DCLS1_TREES);
		else if (type_of == STMP_LEN_WITHDRAW_ROAD)	// "Uttag i stickv�g"
			sSQL.Format(_T("select distinct tdcls2_spc_id from %s where tdcls2_trakt_id=:1"),TBL_TRAKT_DCLS2_TREES);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getTraktLandOwnerNumberDistinct(int obj_id,vecString& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
	
		sSQL.Format(_T("select distinct prop_group_id from elv_properties_table where prop_object_id=:1"),
						TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CString(m_saCommand.Field(1).asString()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}



BOOL CUMLandValueDB::getTraktAss(vecTransactionTraktAss &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where tass_trakt_id=:1 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
						TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asShort(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asString(),
													m_saCommand.Field(13).asDouble()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addAssTree(CTransaction_tree_assort &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktAssTreeExists(rec))
		{
			sSQL.Format(_T("insert into %s (tassort_tree_trakt_id,tassort_tree_id,tassort_tree_dcls,tassort_spc_id,tassort_spc_name,tassort_assort_name,")
									_T("tassort_exch_volume,tassort_exch_price,tassort_price_in) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)"),TBL_TRAKT_TREES_ASSORT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()	= rec.getTreeID();
			m_saCommand.Param(3).setAsDouble()	= rec.getDCLS();
			m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(5).setAsString()	= rec.getSpcName();
			m_saCommand.Param(6).setAsString()	= rec.getAssortName();
			m_saCommand.Param(7).setAsDouble()	= rec.getExchVolume();
			m_saCommand.Param(8).setAsDouble()	= rec.getExchPrice();
			m_saCommand.Param(9).setAsLong()	= rec.getPriceIn();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktAssTreeExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktAssExists(rec))
		{
			sSQL.Format(_T("insert into %s (tass_assort_id,tass_trakt_id,tass_trakt_data_id,tass_data_type,tass_assort_name,tass_price_m3fub,tass_price_m3to,")
									_T("tass_m3fub,tass_m3to,tass_m3fub_value,tass_m3to_value) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11)"),TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTAssID();
			m_saCommand.Param(2).setAsLong()		= rec.getTAssTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTAssTraktDataID();
			m_saCommand.Param(4).setAsShort()		= rec.getTAssTraktDataType();
			m_saCommand.Param(5).setAsString()	= rec.getTAssName();
			m_saCommand.Param(6).setAsDouble()	= rec.getPriceM3FUB();
			m_saCommand.Param(7).setAsDouble()	= rec.getPriceM3TO();
			m_saCommand.Param(8).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(9).setAsDouble()	= rec.getM3TO();
			m_saCommand.Param(10).setAsDouble()	= rec.getValueM3FUB();
			m_saCommand.Param(11).setAsDouble()	= rec.getValueM3TO();
			m_saCommand.Execute();
		}	// if (!traktAssExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tass_assort_name=:1,tass_price_m3fub=:2,tass_price_m3to=:3,tass_m3fub=:4,tass_m3to=:5,tass_m3fub_value=:6,tass_m3to_value=:7  ")
								_T("where tass_assort_id=:8 and tass_trakt_id=:9 and tass_trakt_data_id=:10 and tass_data_type=:11"),TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getTAssName();
		m_saCommand.Param(2).setAsDouble()	= rec.getPriceM3FUB();
		m_saCommand.Param(3).setAsDouble()	= rec.getPriceM3TO();
		m_saCommand.Param(4).setAsDouble()	= rec.getM3FUB();
		m_saCommand.Param(5).setAsDouble()	= rec.getM3TO();
		m_saCommand.Param(6).setAsDouble()	= rec.getValueM3FUB();
		m_saCommand.Param(7).setAsDouble()	= rec.getValueM3TO();
		m_saCommand.Param(8).setAsLong()	= rec.getTAssID();
		m_saCommand.Param(9).setAsLong()	= rec.getTAssTraktID();
		m_saCommand.Param(10).setAsLong()	= rec.getTAssTraktDataID();
		m_saCommand.Param(11).setAsShort()	= rec.getTAssTraktDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delAssTree(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tassort_tree_trakt_id=:1"),
						TBL_TRAKT_TREES_ASSORT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktAss_prices(int trakt_id,int trakt_data_id,int data_type,int assort_id,double exch_price)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tass_price_m3to=:1 where tass_trakt_id=:2 and tass_trakt_data_id=:3 and tass_assort_id=:4 and tass_data_type=:5"),
						TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= exch_price;
		m_saCommand.Param(2).setAsLong()	= trakt_id;
		m_saCommand.Param(3).setAsLong()	= trakt_data_id;
		m_saCommand.Param(4).setAsLong()	= assort_id;
		m_saCommand.Param(5).setAsShort()	= data_type;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::resetTraktAss(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tass_m3fub=:1,tass_m3to=:2,tass_m3fub_value=:3,tass_m3to_value=:4 where tass_trakt_id=:5"),
						TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= 0.0;
		m_saCommand.Param(2).setAsDouble()	= 0.0;
		m_saCommand.Param(3).setAsDouble()	= 0.0;
		m_saCommand.Param(4).setAsDouble()	= 0.0;
		m_saCommand.Param(5).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::removeTraktAss(int trakt_id,int trakt_data_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where tass_trakt_id=:1 and tass_trakt_data_id=:2"),
						TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= trakt_data_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getTraktTrans(vecTransactionTraktTrans &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();

		if (trakt_id == -1)
			sSQL.Format(_T("select * from %s order by ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id"),
									TBL_TRAKT_TRANS);
		else
			sSQL.Format(_T("select * from %s where ttrans_trakt_id=:1 order by ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id"),
									TBL_TRAKT_TRANS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_trakt_trans(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asLong(),
													m_saCommand.Field(6).asShort(),
													m_saCommand.Field(7).asString(),
													m_saCommand.Field(8).asString(),
													m_saCommand.Field(9).asString(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(12).asString()));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::removeTraktTrans(int trakt_data_id,int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where ttrans_trakt_data_id=:1 and ttrans_trakt_id=:2"),
								TBL_TRAKT_TRANS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_data_id;
		m_saCommand.Param(2).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktRotpostExists(rec))
		{
			sSQL.Format(_T("insert into %s (rot_trakt_id,rot_post_volume,rot_post_value,rot_post_cost1,rot_post_cost2,rot_post_cost3,rot_post_cost4,")
									_T("rot_post_cost5,rot_post_cost6,rot_post_cost7,rot_post_cost8,rot_post_cost9,rot_post_netto,rot_post_origin) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)"),TBL_TRAKT_ROTPOST);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getRotTraktID();
			m_saCommand.Param(2).setAsDouble()	= rec.getVolume();
			m_saCommand.Param(3).setAsDouble()	= rec.getValue();
			m_saCommand.Param(4).setAsDouble()	= rec.getCost1();
			m_saCommand.Param(5).setAsDouble()	= rec.getCost2();
			m_saCommand.Param(6).setAsDouble()	= rec.getCost3();
			m_saCommand.Param(7).setAsDouble()	= rec.getCost4();
			m_saCommand.Param(8).setAsDouble()	= rec.getCost5();
			m_saCommand.Param(9).setAsDouble()	= rec.getCost6();
			m_saCommand.Param(10).setAsDouble()	= rec.getCost7();
			m_saCommand.Param(11).setAsDouble()	= rec.getCost8();
			m_saCommand.Param(12).setAsDouble()	= rec.getCost9();
			m_saCommand.Param(13).setAsDouble()	= rec.getNetto();
			m_saCommand.Param(14).setAsShort()	= rec.getOrigin();
			m_saCommand.Execute();
		}	// if (!traktRotpostExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set rot_post_volume=:1,rot_post_value=:2,rot_post_cost1=:3,rot_post_cost2=:4,rot_post_cost3=:5,rot_post_cost4=:6,")
								_T("rot_post_cost5=:7,rot_post_cost6=:8,rot_post_cost7=:9,rot_post_cost8=:10,rot_post_cost9=:11,rot_post_netto=:12,rot_post_origin=:13 where rot_trakt_id=:14"),
								TBL_TRAKT_ROTPOST);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getVolume();
		m_saCommand.Param(2).setAsDouble()	= rec.getValue();
		m_saCommand.Param(3).setAsDouble()	= rec.getCost1();
		m_saCommand.Param(4).setAsDouble()	= rec.getCost2();
		m_saCommand.Param(5).setAsDouble()	= rec.getCost3();
		m_saCommand.Param(6).setAsDouble()	= rec.getCost4();
		m_saCommand.Param(7).setAsDouble()	= rec.getCost5();
		m_saCommand.Param(8).setAsDouble()	= rec.getCost6();
		m_saCommand.Param(9).setAsDouble()	= rec.getCost7();
		m_saCommand.Param(10).setAsDouble()	= rec.getCost8();
		m_saCommand.Param(11).setAsDouble()	= rec.getCost9();
		m_saCommand.Param(12).setAsDouble()	= rec.getNetto();
		m_saCommand.Param(13).setAsShort()	= rec.getOrigin();
		m_saCommand.Param(14).setAsLong()	= rec.getRotTraktID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}
/*
// Special function combining 2 or more tables; 071024 p�d
BOOL CUMLandValueDB::getExchPriceCalulatedInExchangeModule(int trakt_id,vecExchCalcInfo &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format("select a.tass_trakt_id,a.tass_trakt_data_id,a.tass_assort_id,b.tassort_exch_price from %s a,%s b where b.tassort_tree_trakt_id=a.tass_trakt_id and a.tass_trakt_id=:1 \
								 and b.tassort_price_in=0 and b.tassort_assort_name=a.tass_assort_name and b.tassort_spc_id=a.tass_trakt_data_id",
			TBL_TRAKT_SPC_ASS,
			TBL_TRAKT_TREES_ASSORT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_exch_calc_info_struct(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asDouble()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}
*/

BOOL CUMLandValueDB::getContacts(vecTransactionContacts &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),
						TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()),
				m_saCommand.Field(_T("connect_id")).asLong(),
				m_saCommand.Field(_T("type_of")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString())	));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getContacts(CString sql,vecTransactionContacts &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where %s"),
						TBL_CONTACTS,sql);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asLong(),
				m_saCommand.Field(_T("type_of")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString())	));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getContacts(CString category,CString sql,vecTransactionContacts &vec)
{
	CString sSQL;
	int nCategoryID = -1;
	try
	{
		vec.clear();

		// Get id of selcted category; 080630 p�d
		sSQL.Format(_T("select distinct a.id from %s a,%s b	where a.id=b.category_id and a.category='%s'"),
						TBL_CATEGORY,TBL_CATEGORY_CONTACTS,category);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nCategoryID = m_saCommand.Field(1).asLong();
		}

		// Get selected Contacts; 080630 p�d
		// Get id of selcted category; 080630 p�d
		if (sql.IsEmpty())
		{
			sSQL.Format(_T("select a.* from %s a where exists (select b.contact_id from %s b where b.category_id=:1 and a.id=b.contact_id)"),
							TBL_CONTACTS,TBL_CATEGORY_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= nCategoryID;
		}
		else
		{
			sSQL.Format(_T("select a.* from %s a where exists (select b.contact_id from %s b where b.category_id=:1 and a.id=b.contact_id and %s)"),
							TBL_CONTACTS,TBL_CATEGORY_CONTACTS,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= nCategoryID;
		}
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asLong(),
				m_saCommand.Field(_T("type_of")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString())	));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	return TRUE;
}

BOOL CUMLandValueDB::getContact(int contact_id,CTransaction_contacts &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where id=:1"),
						TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = contact_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			rec = CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asLong(),
				m_saCommand.Field(_T("type_of")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString())	);
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getTemplates(vecTransactionTemplate &vec,int tmpl_type)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where tmpl_type_of=:1"),
						TBL_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = tmpl_type;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asLong(),
												m_saCommand.Field(4).asLongChar(),
												m_saCommand.Field(5).asLongChar(),
												convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


//*******************************************************************************************************
// Method SUM data for object and property from trakt(s). E.g. Areal,rotpost information etc.; 080430 p�d
BOOL CUMLandValueDB::getObjPropTraktSUM_stands_areal(int obj_id,int prop_id,int *numof_stands,double *areal)
{
	CString sSQL;
	try
	{
//		sSQL.Format(_T("select distinct count(b.trakt_areal),sum(b.trakt_areal)	from %s a,%s b \
//								where a.ecru_object_id=:1 and a.ecru_prop_id=:2 and b.trakt_prop_id=a.ecru_prop_id and a.ecru_id=b.trakt_id"),
//								TBL_ELV_CRUISE,TBL_TRAKT);
		sSQL.Format(_T("select distinct count(b.trakt_areal) from %s a,%s b where a.ecru_object_id=:1 and a.ecru_prop_id=:2 and b.trakt_prop_id=a.ecru_prop_id and a.ecru_id=b.trakt_id"),
								TBL_ELV_CRUISE,TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*numof_stands = m_saCommand.Field(1).asLong();
		}

		sSQL.Format(_T("select sum(a.eval_areal) from %s a	where a.eval_object_id=:1 and a.eval_prop_id=:2"),TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*areal = m_saCommand.Field(1).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}
BOOL CUMLandValueDB::getObjPropTraktSUM_numof_volume(int obj_id,int prop_id,long *numof_trees,double *m3sk)
{
	CString sSQL;
	double fM3Sk_cruised = 0.0;
	double fM3Sk_manually = 0.0;
	try
	{
		sSQL.Format(_T("select sum(c.tdata_numof),sum(c.tdata_m3sk_vol) from %s a,%s b,%s c where a.ecru_object_id=:1 and a.ecru_prop_id=:2 and ")
								_T("b.trakt_prop_id=a.ecru_prop_id and c.tdata_trakt_id=b.trakt_id and b.trakt_id=a.ecru_id and c.tdata_data_type=:3"),
								TBL_ELV_CRUISE,TBL_TRAKT,TBL_TRAKT_DATA);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Param(3).setAsShort() = STMP_LEN_WITHDRAW;	// "Uttag"
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*numof_trees = m_saCommand.Field(1).asLong();
			fM3Sk_cruised = m_saCommand.Field(2).asDouble();
		}

		sSQL.Format(_T("select sum(ecru_m3sk_vol) from %s  where ecru_object_id=:1 and ecru_prop_id=:2 and ecru_cruise_type=:3"),TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Param(3).setAsShort() = CRUISE_TYPE_2;	// "Manuellt inl�sta"
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			fM3Sk_manually = m_saCommand.Field(1).asDouble();
		}

		*m3sk = fM3Sk_cruised + fM3Sk_manually;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTraktSUM_rotpost(int obj_id,int prop_id,double *timber_volume,double *timber_value,double *timber_cost,double *rot_netto)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(c.rot_post_volume),sum(c.rot_post_value),")
								_T("sum(c.rot_post_cost1+c.rot_post_cost2+c.rot_post_cost3+c.rot_post_cost4+c.rot_post_cost5+c.rot_post_cost6+c.rot_post_cost7+c.rot_post_cost8),")
								_T("sum(c.rot_post_netto) ")
								_T("from %s a,%s b,%s c where a.ecru_object_id=:1 and a.ecru_prop_id=:2 and b.trakt_prop_id=a.ecru_prop_id and a.ecru_id=b.trakt_id and c.rot_trakt_id=b.trakt_id"),
								TBL_ELV_CRUISE,TBL_TRAKT,TBL_TRAKT_ROTPOST);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*timber_volume = m_saCommand.Field(1).asDouble();
			*timber_value = m_saCommand.Field(2).asDouble();
			*timber_cost = m_saCommand.Field(3).asDouble();
			*rot_netto = m_saCommand.Field(4).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTraktSUM_storm_dry_randtrees(int obj_id,int prop_id,double *storm_dry_value,double *randtree_value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(ecru_storm_dry_value),sum(ecru_randtrees_value) from %s where ecru_object_id=:1 and ecru_prop_id=:2"),
										TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		m_saCommand.Param(2).setAsLong()		= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*storm_dry_value = m_saCommand.Field(1).asDouble();
			*randtree_value = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTraktSUM_grot(int obj_id,int prop_id,double *grot_volume,double *grot_value,double *grot_cost)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(ecru_grot_volume),sum(ecru_grot_value),sum(ecru_grot_cost) from %s where ecru_object_id=:1 and ecru_prop_id=:2"),
										TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		m_saCommand.Param(2).setAsLong()		= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*grot_volume = m_saCommand.Field(1).asDouble();
			*grot_value = m_saCommand.Field(2).asDouble();
			*grot_cost = m_saCommand.Field(3).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// SUM Information in "elv_evaluation_table" to be added to "elv_properties_table"; 080515 p�d
BOOL CUMLandValueDB::getObjPropTraktSUM_landvalue_precut(int obj_id,int prop_id,double *land_value,double *precut_value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(eval_land_value_stand),sum(eval_early_cutting_stand) from %s where eval_object_id=:1 and eval_prop_id=:2"),
								TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*land_value = m_saCommand.Field(1).asDouble();
			*precut_value = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updObjPropTraktSUM_1(int prop_id,int obj_id,
											int numof_stands,double areal,
											long numof_trees,double m3sk,
											double storm_dry_value,double randtrees_value)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s  set prop_volume_m3sk=:1,prop_areal=:2,prop_numof_trees=:3,prop_numof_stands=:4,prop_storm_dry_value=:5,prop_randtrees_value=:6 ")
								_T("where prop_id=:7 and prop_object_id=:8"),TBL_ELV_PROP);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= m3sk;
		m_saCommand.Param(2).setAsDouble()	= areal;
		m_saCommand.Param(3).setAsLong()	= numof_trees;
		m_saCommand.Param(4).setAsLong()	= numof_stands;
		m_saCommand.Param(5).setAsDouble()	= storm_dry_value;
		m_saCommand.Param(6).setAsDouble()	= randtrees_value;
		m_saCommand.Param(7).setAsLong()	= prop_id;
		m_saCommand.Param(8).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}


BOOL CUMLandValueDB::updObjPropTraktSUM_2(int prop_id,int obj_id,double land_value,double precut_value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set prop_land_value=:1,prop_early_cut_value=:2 where prop_id=:3 and prop_object_id=:4"),TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= land_value;
		m_saCommand.Param(2).setAsDouble()	= precut_value;
		m_saCommand.Param(3).setAsLong()	= prop_id;
		m_saCommand.Param(4).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjPropTraktSUM_3(int prop_id,int obj_id,double voluntary_deal_value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_voluntary_deal_value=:1	where prop_id=:2 and prop_object_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= voluntary_deal_value;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updObjPropTraktSUM_4(int prop_id,int obj_id,double high_cost_volume,double higher_costs)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_high_cost_volume=:1,prop_high_cost_value=:2 where prop_id=:3 and prop_object_id=:4"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= high_cost_volume;
		m_saCommand.Param(2).setAsDouble()	= higher_costs;
		m_saCommand.Param(3).setAsLong()	= prop_id;
		m_saCommand.Param(4).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjPropTraktSUM_5(int prop_id,int obj_id,double other_comp)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_other_comp_value=:1	where prop_id=:2 and prop_object_id=:3"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= other_comp;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updObjPropTraktSUM_6(int prop_id,int obj_id,
											double timber_volume,double timber_value,
											double timber_cost,double rot_netto)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_wood_volume=:1,prop_wood_value=:2,prop_cost_value=:3,prop_rotpost_value=:4 where prop_id=:5 and prop_object_id=:6"),
						TBL_ELV_PROP);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= timber_volume;
		m_saCommand.Param(2).setAsDouble()	= timber_value;
		m_saCommand.Param(3).setAsDouble()	= timber_cost;
		m_saCommand.Param(4).setAsDouble()	= rot_netto;
		m_saCommand.Param(5).setAsLong()	= prop_id;
		m_saCommand.Param(6).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updObjPropTraktSUM_grot(int prop_id,int obj_id,double grot_volume,double grot_value,double grot_cost)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_grot_volume=:1,prop_grot_value=:2,prop_grot_cost=:3 where prop_id=:4 and prop_object_id=:5"),
						TBL_ELV_PROP);

		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsDouble()	= grot_volume;
		m_saCommand.Param(2).setAsDouble()	= grot_value;
		m_saCommand.Param(3).setAsDouble()	= grot_cost;
		m_saCommand.Param(4).setAsLong()	= prop_id;
		m_saCommand.Param(5).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Added 2009-09-14 p�d
BOOL CUMLandValueDB::updObjPropTraktSUM_7(int prop_id,int obj_id,double areal)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s  set prop_areal=:1 where prop_id=:2 and prop_object_id=:3"),TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= areal;
		m_saCommand.Param(2).setAsLong()	= prop_id;
		m_saCommand.Param(3).setAsLong()	= obj_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTrakt_numof_volume(int trakt_id,long *numof_trees,double *m3sk)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(tdata_numof),sum(tdata_m3sk_vol) from %s where tdata_trakt_id=:1 and tdata_data_type=:2"),
						TBL_TRAKT_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Param(2).setAsShort() = 1;	// "Uttag"
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*numof_trees = m_saCommand.Field(1).asLong();
			*m3sk = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTrakt_rotpost(int trakt_id,double *timber_volume,double *timber_value,double *timber_cost,double *rot_netto)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select (c.rot_post_volume),(c.rot_post_value),")
					_T("(c.rot_post_cost1+c.rot_post_cost2+c.rot_post_cost3+c.rot_post_cost4+c.rot_post_cost5+c.rot_post_cost6+c.rot_post_cost7+c.rot_post_cost8),")
					_T("(c.rot_post_netto) from %s c where c.rot_trakt_id=:1"),
					TBL_TRAKT_ROTPOST);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*timber_volume = m_saCommand.Field(1).asDouble();
			*timber_value = m_saCommand.Field(2).asDouble();
			*timber_cost = m_saCommand.Field(3).asDouble();
			*rot_netto = m_saCommand.Field(4).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTraktCruise_randtrees(int cruise_id,int obj_id,int prop_id,double *randtree_volume,double *randtree_value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(erand_volume),sum(erand_value) from elv_cruise_randtrees_table where erand_object_id=:1 and erand_prop_id=:2 and erand_ecru_id=:3"),
								TBL_ELV_CRUISE_RANDTREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Param(3).setAsLong() = cruise_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*randtree_volume = m_saCommand.Field(1).asDouble();
			*randtree_value = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjPropTrakt_m3sk_volumes_per_spc(int trakt_id,double trakt_areal,vecTransaction_elv_m3sk_per_specie& vec)
{
	CString sSQL;

	double fArea = 0.0,fArealCalculation = 1.0;
	try
	{
		// Get SUM area for Plots if we have "Cirkelytor"; 091019 p�d
		sSQL.Format(_T("select sum(tplot_area) as 'area' from esti_trakt_plot_table where (tplot_plot_type=2 or tplot_plot_type=3 or tplot_plot_type=4) and tplot_trakt_id=:1"),
						TBL_TRAKT_PLOT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			fArea = m_saCommand.Field(_T("area")).asDouble();
		}
		if (fArea > 0.0)
			fArealCalculation = ((10000.0/fArea)*trakt_areal);

		sSQL.Format(_T("update %s set tdcls_numof_randtrees=0 where tdcls_trakt_id=:1 and tdcls_numof_randtrees is NULL"),
								TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();

		sSQL.Format(_T("select tdcls_spc_id,sum((tdcls_m3sk/(tdcls_numof+tdcls_numof_randtrees))*tdcls_numof),sum((tdcls_m3sk/(tdcls_numof+tdcls_numof_randtrees))*tdcls_numof_randtrees) ")
								_T("from %s where tdcls_trakt_id=:1 group by tdcls_spc_id"),TBL_TRAKT_DCLS_TREES);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_m3sk_per_specie(m_saCommand.Field(1).asLong(),
																										 m_saCommand.Field(2).asDouble()*fArealCalculation,
																										 m_saCommand.Field(3).asDouble()*fArealCalculation));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getNumOfEvaluations(int obj_id,int prop_id,int *num_of)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select count(*) from %s where eval_object_id=:1 and eval_prop_id=:2"),
			TBL_ELV_EVALUATION);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		m_saCommand.Param(2).setAsLong()		= prop_id;
		
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*num_of = m_saCommand.Field(1).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


 BOOL CUMLandValueDB::getObjVolDealValues(int obj_id,int *type,double *price_base,double *max_perc,double *perc,double *perc_pricebase)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select object_type_of_net, object_price_base, object_max_percent, object_procent, object_perc_of_price_base from %s where object_id=:1"),
			TBL_ELV_OBJECT);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= obj_id;
		
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*type			= m_saCommand.Field(1).asLong();
			*price_base		= m_saCommand.Field(2).asDouble();
			*max_perc		= m_saCommand.Field(3).asDouble();
			*perc			= m_saCommand.Field(4).asDouble();
			*perc_pricebase	= m_saCommand.Field(5).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjProp_grot(int trakt_id,double *sum_grot_volume,double *sum_grot_value,double *sum_grot_cost)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select distinct b.tdata_grot/1000.0,c.tsetspc_grot_price,c.tsetspc_grot_cost from %s b,%s c ")
								_T("where b.tdata_trakt_id=%d and b.tdata_data_type=%d and c.tsetspc_tdata_trakt_id=b.tdata_trakt_id and c.tsetspc_specie_id=b.tdata_spc_id"),
			TBL_TRAKT_DATA,TBL_TRAKT_SET_SPC,trakt_id,STMP_LEN_WITHDRAW);

		m_saCommand.setCommandText((SAString)sSQL);
		
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*sum_grot_volume += m_saCommand.Field(1).asDouble();	// <= tonne
			*sum_grot_value += m_saCommand.Field(2).asDouble()*m_saCommand.Field(1).asDouble();
			*sum_grot_cost += m_saCommand.Field(3).asDouble()*m_saCommand.Field(1).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjProp_infr(int obj_id,int prop_id,double *sum_landvalue,double *sum_pre_cut,double *sum_storm_dry,double *sum_randtrees)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select prop_land_value,prop_early_cut_value,prop_storm_dry_value,prop_randtrees_value from %s where prop_object_id=:1 and prop_id=:2"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*sum_landvalue = m_saCommand.Field(1).asDouble();
			*sum_pre_cut = m_saCommand.Field(2).asDouble();
			*sum_storm_dry = m_saCommand.Field(3).asDouble();
			*sum_randtrees = m_saCommand.Field(4).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// "Metoder f�r att matcha mark�gare till fastigheter"; 080516 p�d

// "L�s in mark�garid f�r object och fastighet"; 080516 p�d
BOOL CUMLandValueDB::getObjProp_landowners_per_prop(int obj_id,int prop_id,vecInt& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select b.prop_id,c.id from %s a,%s b,%s c where a.prop_object_id=:1 and a.prop_id=b.prop_id and c.id=b.contact_id ")
					_T("and a.prop_id=:2 group by b.prop_id,c.id order by b.prop_id"),TBL_ELV_PROP,TBL_PROP_OWNER,TBL_CONTACTS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(m_saCommand.Field(2).asLong());
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getObjProp_landowner_id_numofs_per_property(int prop_id,int obj_id,int *num_of)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select a.prop_id,count(a.prop_id) from %s a,%s b,%s c where a.prop_object_id=:1 and a.prop_id=c.prop_id and b.id=c.contact_id and a.prop_id=:2 group by a.prop_id"),
								TBL_ELV_PROP,TBL_CONTACTS,TBL_PROP_OWNER);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Param(2).setAsLong() = prop_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*num_of = m_saCommand.Field(2).asLong();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


/* Matcha mark�garid(n) per fastighetsid */
BOOL CUMLandValueDB::getObjProp_match_landowner_ids_per_properties(LPCTSTR sql,int numof,vecInt& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.prop_id,count(a.prop_id) from %s a,%s b where %s and a.contact_id=b.id group by a.prop_id having count(a.prop_id)=:1"),
								TBL_PROP_OWNER,TBL_CONTACTS,sql);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = numof;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Handle Object cruise table; 080506 p�d
BOOL CUMLandValueDB::getObjectAllCruiseStands(int obj_id,vecTransaction_elv_cruise_id &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select ecru_object_id,ecru_prop_id,ecru_id from %s where ecru_object_id=:1"),
								TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_cruise_id(m_saCommand.Field(1).asLong(),
													 m_saCommand.Field(2).asLong(),
													 m_saCommand.Field(3).asLong()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getTraktsPerLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecInt& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select b.ecru_id from %s a,%s b where a.prop_object_id=:1 and a.prop_group_id=:2 and b.ecru_object_id=a.prop_object_id and b.ecru_prop_id=a.prop_id"),
						TBL_ELV_PROP,TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(m_saCommand.Field(1).asLong());
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Get Stands for a Landownerconstellation and Object; 080527 p�d
BOOL CUMLandValueDB::getSLenForTraktsPerLandOwnerNumber(LPCTSTR sql,vecTransaction_elv_slen_data& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		if( _tcslen(sql) > 0 )
		{
			sSQL.Format(_T("select distinct tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,sum(tdcls_numof),sum(tdcls_numof_randtrees) ")
									 _T("from %s where %s group by tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to order by tdcls_spc_id,tdcls_dcls_from"),
									 TBL_TRAKT_DCLS_TREES,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				vec.push_back(CTransaction_elv_slen_data(m_saCommand.Field(1).asLong(),
															m_saCommand.Field(2).asString(),
															m_saCommand.Field(3).asDouble(),
															m_saCommand.Field(4).asDouble(),
															m_saCommand.Field(5).asLong(),
															m_saCommand.Field(6).asLong()));
			}
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getSLenMinAndMaxDCLS(LPCTSTR sql,double *min_dcls,double *max_dcls)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select distinct min(tdcls_dcls_from),max(tdcls_dcls_from) from %s where %s"),TBL_TRAKT_DCLS_TREES,sql);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*min_dcls = m_saCommand.Field(1).asDouble();
			*max_dcls = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getSLenSummmarizedData(LPCTSTR sql,vecTransaction_elv_slen_sum_data& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		if( _tcslen(sql) > 0 )
		{
			sSQL.Format(_T("select tdcls_spc_id,tdcls_spc_name,sum(tdcls_numof),sum(tdcls_numof_randtrees),")
						_T("sum((tdcls_m3sk/(tdcls_numof+tdcls_numof_randtrees))*tdcls_numof),sum((tdcls_m3sk/(tdcls_numof+tdcls_numof_randtrees))*tdcls_numof_randtrees) ")
						_T("from %s where %s group by tdcls_spc_id,tdcls_spc_name order by tdcls_spc_id"),TBL_TRAKT_DCLS_TREES,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				vec.push_back(CTransaction_elv_slen_sum_data(m_saCommand.Field(1).asLong(),
																m_saCommand.Field(2).asString(),
																m_saCommand.Field(3).asLong(),
																m_saCommand.Field(4).asLong(),
																m_saCommand.Field(5).asDouble(),
																m_saCommand.Field(6).asDouble()));
			}
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getSLenDiameterclasses(LPCTSTR sql,vecInt& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		if( _tcslen(sql) > 0 )
		{
			sSQL.Format(_T("select distinct tdcls_dcls_from from %s where %s"),
									 TBL_TRAKT_DCLS_TREES,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				vec.push_back(m_saCommand.Field(1).asLong());
			}
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getPropertiesByLandOwnerNumber(int obj_id,LPCTSTR landowner_number,vecString& vec)
{
	CString sSQL,sProperty;
	try
	{
		vec.clear();
		sSQL.Format(_T("select b.prop_name,b.block_number,b.unit_number,b.prop_number from %s a,%s b where a.prop_object_id=:1 and a.prop_group_id=:2 and b.id=a.prop_id"),
								TBL_ELV_PROP,TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_number;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			sProperty.Format(_T("%s %d:%d"),
				m_saCommand.Field(1).asString(),
				m_saCommand.Field(2).asLong(),
				m_saCommand.Field(3).asLong());
			vec.push_back(sProperty);
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getEvaluationsPerLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_eval_evaluation& vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select b.* from %s a,%s b where a.prop_object_id=:1 and a.prop_group_id=:2 and b.eval_object_id=a.prop_object_id and b.eval_prop_id=a.prop_id"),
					TBL_ELV_PROP,TBL_ELV_EVALUATION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(19).asDateTime();
			vec.push_back(CTransaction_eval_evaluation(m_saCommand.Field(1).asLong(),
				m_saCommand.Field(2).asLong(),
				m_saCommand.Field(3).asLong(),
				m_saCommand.Field(4).asString(),
				m_saCommand.Field(5).asDouble(),
				m_saCommand.Field(6).asLong(),
				m_saCommand.Field(7).asString(),
				m_saCommand.Field(8).asDouble(),
				m_saCommand.Field(9).asDouble(),
				m_saCommand.Field(10).asDouble(),
				m_saCommand.Field(11).asDouble(),
				m_saCommand.Field(12).asShort(),
				m_saCommand.Field(13).asShort(),
				m_saCommand.Field(17).asDouble(),
				m_saCommand.Field(18).asDouble(),
				m_saCommand.Field(15).asDouble(),
				m_saCommand.Field(16).asDouble(),
				convertSADateTime(saDateTime),
				m_saCommand.Field(20).asDouble(),
				m_saCommand.Field(_T("eval_length")).asLong(),
				m_saCommand.Field(_T("eval_width")).asLong(),
				m_saCommand.Field(_T("eval_layer")).asLong(),
				m_saCommand.Field(_T("eval_side")).asLong(),
				m_saCommand.Field(_T("eval_overlapping_land")).asBool(),
				m_saCommand.Field(_T("eval_tillf_uttnyttjande")).asBool(),
				//HMS-49 Info om f�rtidig avverkning 20191126 J�
				m_saCommand.Field(_T("eval_info_precut_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_p30_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_andel_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_corrfact")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_pine")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_ers_birch")).asDouble(),
				m_saCommand.Field(_T("eval_info_precut_reduced")).asDouble(),
				//HMS-48 Info om markv�rde 20200427 J�
				m_saCommand.Field(_T("eval_markinfo_valuepine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_valuespruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_andel_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_pine")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_p30_spruce")).asDouble(),
				m_saCommand.Field(_T("eval_markinfo_reducedby")).asDouble()
				));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getStormDryForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,double *volume,double *value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(b.ecru_storm_dry_vol),sum(b.ecru_storm_dry_value) from elv_properties_table a,elv_cruise_table b ")
								_T("where a.prop_object_id=:1 and a.prop_group_id=:2 and b.ecru_object_id=a.prop_object_id and b.ecru_prop_id=a.prop_id"),
								TBL_ELV_PROP,TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*volume = m_saCommand.Field(1).asDouble();
			*value = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

BOOL CUMLandValueDB::getRandTreesForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_elv_cruise_randtrees& vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select b.* from %s a,%s b where a.prop_object_id=:1 and a.prop_group_id=:2 and b.erand_object_id=a.prop_object_id and b.erand_prop_id=a.prop_id"),
						TBL_ELV_PROP,TBL_ELV_CRUISE_RANDTREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_cruise_randtrees(m_saCommand.Field(1).asLong(),
															m_saCommand.Field(2).asLong(),
															m_saCommand.Field(3).asLong(),
															m_saCommand.Field(4).asLong(),
															m_saCommand.Field(5).asLong(),
															m_saCommand.Field(6).asString(),
															m_saCommand.Field(7).asDouble(),
															m_saCommand.Field(8).asDouble(),
															m_saCommand.Field(9).asString()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// "F�rdyrad avverkning"
BOOL CUMLandValueDB::getHighCostForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,double *volume,double *value)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(prop_high_cost_volume),sum(prop_high_cost_value) from %s where prop_object_id=:1 and prop_group_id=:2"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*volume = m_saCommand.Field(1).asDouble();
			*value = m_saCommand.Field(2).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

BOOL CUMLandValueDB::getOtherCompForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_elv_properties_other_comp& vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select b.* from %s a,%s b where a.prop_object_id=:1 and a.prop_group_id=:2 and b.ocomp_object_id=a.prop_object_id and b.ocomp_prop_id=a.prop_id"),
					TBL_ELV_PROP,TBL_ELV_PROP_OTHER_COMP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_properties_other_comp(m_saCommand.Field(1).asLong(),
																	m_saCommand.Field(2).asLong(),
																	m_saCommand.Field(3).asLong(),
																	m_saCommand.Field(4).asDouble(),
																	m_saCommand.Field(5).asDouble(),
																	m_saCommand.Field(6).asString(),
																	m_saCommand.Field(7).asLong()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getAssortmentsByTrakts(LPCTSTR sql,vecTransactionTraktAss &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		if( _tcslen(sql) > 0 )
		{
			sSQL.Format(_T("select distinct tass_trakt_id,tass_trakt_data_id,tass_assort_name,tass_price_m3fub,tass_price_m3to,tass_kr_m3_value,")
						_T("sum(tass_m3fub),sum(tass_m3to),sum(tass_m3fub_value),sum(tass_m3to_value) from %s where %s ")
						_T("group by tass_trakt_id,tass_trakt_data_id,tass_assort_name,tass_price_m3fub,tass_price_m3to,tass_kr_m3_value order by tass_trakt_data_id"),
						TBL_TRAKT_SPC_ASS,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();

			while(m_saCommand.FetchNext())
			{
				vec.push_back(CTransaction_trakt_ass(0,
														m_saCommand.Field(1).asLong(),
														m_saCommand.Field(2).asLong(),
														0,
														m_saCommand.Field(3).asString(),
														m_saCommand.Field(4).asDouble(),
														m_saCommand.Field(5).asDouble(),
														m_saCommand.Field(7).asDouble(),
														m_saCommand.Field(8).asDouble(),
														m_saCommand.Field(9).asDouble(),
														m_saCommand.Field(10).asDouble(),
														_T(""),
														m_saCommand.Field(6).asDouble()));
			}
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}
BOOL CUMLandValueDB::getRotpostDataForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,double *gagn_vol,double *value,double *cost,double *netto)
{

	CString sSQL;
	try
	{
		sSQL.Format(_T("select sum(prop_wood_volume),sum(prop_wood_value),sum(prop_cost_value),sum(prop_rotpost_value) from %s where prop_object_id=:1 and prop_group_id=:2"),
						TBL_ELV_PROP);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			*gagn_vol =	m_saCommand.Field(1).asDouble();
			*value = m_saCommand.Field(2).asDouble();
			*cost = m_saCommand.Field(3).asDouble();
			*netto = m_saCommand.Field(4).asDouble();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

BOOL CUMLandValueDB::getCruisesForLandOwnerNumber(int obj_id,LPCTSTR landowner_num,vecTransaction_elv_cruise &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select b.*	from %s a,%s b where a.prop_object_id=:1 and a.prop_group_id=:2 and b.ecru_object_id=a.prop_object_id and b.ecru_prop_id=a.prop_id order by b.ecru_prop_id"),
						TBL_ELV_PROP,TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= landowner_num;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_cruise(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asString(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asLong(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asDouble(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asDouble(),
													m_saCommand.Field(17).asDouble(),
													m_saCommand.Field(18).asDouble(),
													m_saCommand.Field(19).asShort(),
													m_saCommand.Field(20).asShort(),
													m_saCommand.Field(21).asShort(),
													m_saCommand.Field(22).asDouble(),
													m_saCommand.Field(23).asString(),
													m_saCommand.Field(_T("ecru_tgl")).asString(),
													m_saCommand.Field(_T("ecru_grot_volume")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_value")).asDouble(),
													m_saCommand.Field(_T("ecru_grot_cost")).asDouble(),
													m_saCommand.Field(_T("ecru_use_for_infr")).asShort(),
													m_saCommand.Field(_T("ecru_tillf_uttnyttjande")).asBool(),
													m_saCommand.Field(_T("ecru_sttork_fsprucemix")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsumm3sk_inside")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_favgpricefactor")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_ftakecareofperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpinep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fsprucep30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fbirchp30price")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fcompensationlevel")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_fpineperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_spruceperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_birchperc")).asDouble(),
													m_saCommand.Field(_T("ecru_sttork_widefactor")).asDouble(),
													//HMS-48 Info om markv�rde 20200427 J�
													m_saCommand.Field(_T("ecru_markinfo_valuepine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_valuespruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_andel_spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30pine")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_p30spruce")).asDouble(),
													m_saCommand.Field(_T("ecru_markinfo_reducedby")).asDouble()
													));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


// Metod f�r att r�kna upp/ned k3/m3 f�r Sortiment i tabell: "esti_trakt_spc_assort_table"; 080611 p�d
BOOL CUMLandValueDB::calculateK3PerM3OnObjectPropertyStands(int obj_id,double value,LPCTSTR assortment,int price_set_in)
{
	CString sSQL;
	try
	{
		// Price set in m3fub; 090615 p�d
		if (price_set_in == 1)
		{
			// First; reset values, i.e. calculate "original" values; 080611 p�d
			sSQL.Format(_T("update %s set tass_m3fub_value=tass_m3fub*tass_price_m3fub,tass_m3to_value=0.0 ")
						_T("where exists (select ecru_id from %s where ecru_object_id=:1 and tass_trakt_id=ecru_id and tass_assort_name=:2)"),
						TBL_TRAKT_SPC_ASS,TBL_ELV_CRUISE);
		}
		// Price set in m3to; 090615 p�d
		else if (price_set_in == 2)
		{
			// First; reset values, i.e. calculate "original" values; 080611 p�d
			sSQL.Format(_T("update %s set tass_m3fub_value=0.0,tass_m3to_value=tass_m3to*tass_price_m3to ")
						_T("where exists (select ecru_id from %s where ecru_object_id=:1 and tass_trakt_id=ecru_id and tass_assort_name=:2)"),
						TBL_TRAKT_SPC_ASS,TBL_ELV_CRUISE);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= assortment;

		// Second; set the value (kr/m3), to be used for up/down calculation
		// of price m3fub and m3to; 080611 p�d
		sSQL.Format(_T("update %s set tass_kr_m3_value=:1 where exists (select ecru_id from %s where ecru_object_id=:2 and tass_trakt_id=ecru_id and tass_assort_name=:3)"),
					TBL_TRAKT_SPC_ASS,TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= value;
		m_saCommand.Param(2).setAsLong()	= obj_id;
		m_saCommand.Param(3).setAsString()	= assortment;
		m_saCommand.Execute();

		// Price set in m3fub; 090615 p�d
		if (price_set_in == 1)
		{
			// Third; calculate new value for m3to and m3fub; 080611 p�d
			sSQL.Format(_T("update %s set tass_m3fub_value=tass_m3fub_value+(tass_m3fub*tass_kr_m3_value),tass_m3to_value=0.0 ")
						_T("where exists (select ecru_id from %s where ecru_object_id=:1 and tass_trakt_id=ecru_id and tass_assort_name=:2)"),
						TBL_TRAKT_SPC_ASS,TBL_ELV_CRUISE);
		}
		// Price set in m3to; 090615 p�d
		else if (price_set_in == 2)
		{
			// Third; calculate new value for m3to and m3fub; 080611 p�d
			sSQL.Format(_T("update %s set tass_m3fub_value=0.0,tass_m3to_value=tass_m3to_value+(tass_m3to*tass_kr_m3_value) ")
						_T("where exists (select ecru_id from %s where ecru_object_id=:1 and tass_trakt_id=ecru_id and tass_assort_name=:2)"),
						TBL_TRAKT_SPC_ASS,TBL_ELV_CRUISE);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= obj_id;
		m_saCommand.Param(2).setAsString()	= assortment;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getAssortmentsForTraktAndObject(int obj_id,vecTransaction_elv_ass_stands_obj &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select distinct a.tass_assort_id,a.tass_assort_name,a.tass_kr_m3_value from %s a,%s b ")
								_T("where b.ecru_object_id=:1 and a.tass_trakt_id=b.ecru_id and a.tass_kr_m3_value IS NOT NULL"),
								TBL_TRAKT_SPC_ASS,TBL_ELV_CRUISE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = obj_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_elv_ass_stands_obj(m_saCommand.Field(1).asLong(),																									
															m_saCommand.Field(2).asString(),
															m_saCommand.Field(3).asDouble()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::getCategories(vecTransactionCategory &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),
					TBL_CATEGORY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			vec.push_back(CTransaction_category(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asString(),
												getUserName()));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::getProperties(vecTransactionProperty &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(15).asDateTime();
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asLong(),
												 m_saCommand.Field(2).asString(),
												 m_saCommand.Field(3).asString(),
												 m_saCommand.Field(4).asString(),
												 m_saCommand.Field(5).asString(),
												 m_saCommand.Field(6).asString(),
												 m_saCommand.Field(7).asString(),
												 m_saCommand.Field(8).asString(),
												 m_saCommand.Field(9).asString(),
												 m_saCommand.Field(10).asString(),
												 m_saCommand.Field(11).asString(),
												 m_saCommand.Field(12).asDouble(),
												 m_saCommand.Field(13).asDouble(),
												 m_saCommand.Field(14).asString(),
												 convertSADateTime(saDateTime),
												 m_saCommand.Field(16).asString(),
												 m_saCommand.Field(18).asString(),
												 m_saCommand.Field(17).asString(),
												 m_saCommand.Field("prop_coord").asString()
												 ));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}




//**********************************************************************************************
// Added 2009-05-09 p�d
// Methods used e.g. update/change "Best�ndsmall"; 090506 p�d
BOOL CUMLandValueDB::updTraktMiscData_prl(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tprl_name=:1,tprl_typeof=:2,tprl_pricelist=:3 where tprl_trakt_id=:4"),
									TBL_TRAKT_MISC_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getName();
		m_saCommand.Param(2).setAsLong()	= rec.getTypeOf();
		m_saCommand.Param(3).setAsLongChar()	= rec.getXMLPricelist();
		m_saCommand.Param(4).setAsLong()	= rec.getTSetTraktID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}


BOOL CUMLandValueDB::updTraktMiscData_costtmpl(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tprl_costtmpl_name=:1,tprl_costtmpl_typeof=:2,tprl_costtmpl=:3 where tprl_trakt_id=:4"),
			TBL_TRAKT_MISC_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getCostsName();
		m_saCommand.Param(2).setAsLong()	= rec.getCostsTypeOf();
		m_saCommand.Param(3).setAsLongChar()	= rec.getXMLCosts();
		m_saCommand.Param(4).setAsLong()	= rec.getTSetTraktID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktMiscData_dcls(CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tprl_dcls=:1 where tprl_trakt_id=:2"),
					TBL_TRAKT_MISC_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getDiamClass();
		m_saCommand.Param(2).setAsLong()		= rec.getTSetTraktID();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktTransExists(rec.getTTransDataID(), rec.getTTransTraktID(), rec.getTTransFromID(), rec.getTTransToID() ))
		{
			if (rec.getTTransFromID() == -1 || rec.getTTransToID() == -1) return FALSE;

			sSQL.Format(_T("insert into %s (ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id,ttrans_to_spc_id,ttrans_data_type,ttrans_from_name,ttrans_to_name,ttrans_spc_name,ttrans_m3fub,ttrans_percent,ttrans_trans_m3) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)"),TBL_TRAKT_TRANS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTTransDataID();
			m_saCommand.Param(2).setAsLong()		= rec.getTTransTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTTransFromID();
			m_saCommand.Param(4).setAsLong()		= rec.getTTransToID();
			m_saCommand.Param(5).setAsLong()		= rec.getSpecieID();
			m_saCommand.Param(6).setAsShort()		= rec.getTTransDataType();
			m_saCommand.Param(7).setAsString()	= rec.getFromName();
			m_saCommand.Param(8).setAsString()	= rec.getToName();
			m_saCommand.Param(9).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(10).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(11).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(12).setAsDouble()	= rec.getM3Trans();

			m_saCommand.Execute();

			doCommit();

//		S.Format(L"addTraktTrans 1 Spc %s\n\nFrom %s\nTill %s",rec.getSpecieName(),rec.getFromName(),rec.getToName());
//		AfxMessageBox(S);

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set ttrans_from_name=:1,ttrans_to_name=:2,ttrans_spc_name=:3,ttrans_m3fub=:4,ttrans_percent=:5,ttrans_trans_m3=:6 ")
								_T("where ttrans_trakt_data_id=:7 and ttrans_trakt_id=:8 and ttrans_from_ass_id=:9 and ttrans_to_ass_id=:10 and ttrans_to_spc_id=:11 and ttrans_data_type=:12"),
								 TBL_TRAKT_TRANS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getFromName();
		m_saCommand.Param(2).setAsString()	= rec.getToName();
		m_saCommand.Param(3).setAsString()	= rec.getSpecieName();
		m_saCommand.Param(4).setAsDouble()	= rec.getM3FUB();
		m_saCommand.Param(5).setAsDouble()	= rec.getPercent();
		m_saCommand.Param(6).setAsDouble()	= rec.getM3Trans();
		m_saCommand.Param(7).setAsLong()		= rec.getTTransDataID();
		m_saCommand.Param(8).setAsLong()		= rec.getTTransTraktID();
		m_saCommand.Param(9).setAsLong()		= rec.getTTransFromID();
		m_saCommand.Param(10).setAsLong()	= rec.getTTransToID();
		m_saCommand.Param(11).setAsLong()	= rec.getSpecieID();
		m_saCommand.Param(12).setAsShort()	= rec.getTTransDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delTraktTrans(int trakt_id,int spc_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		//if (traktTransExists(trakt_id))
		//{

		sSQL.Format(_T("delete from %s where ttrans_trakt_id=:1 and ttrans_to_spc_id=:2"),
									TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= spc_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		//}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::delTraktTrans(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where ttrans_trakt_id=:1"),
			TBL_TRAKT_TRANS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


//-----------------------------------------------------------------------------------------
// Use these methods for changing template data; 090622 p�d

BOOL CUMLandValueDB::addTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
									_T("tsetspc_hgt_func_id,tsetspc_hgt_specie_id,tsetspc_hgt_func_index) values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getHgtFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getHgtFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getHgtFuncIndex();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_volfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
						_T("tsetspc_vol_func_id,tsetspc_vol_specie_id,tsetspc_vol_func_index) values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getVolFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getVolFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getVolFuncIndex();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
						_T("tsetspc_bark_func_id,tsetspc_bark_specie_id,tsetspc_bark_func_index) values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getBarkFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getBarkFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getBarkFuncIndex();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
						_T("tsetspc_vol_ub_func_id,tsetspc_vol_ub_specie_id,tsetspc_vol_ub_func_index,tsetspc_m3sk_m3ub) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getVolUBFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getVolUBFuncSpcID();
			m_saCommand.Param(8).setAsLong()	= rec.getVolUBFuncIndex();
			m_saCommand.Param(9).setAsDouble()	= rec.getM3SkToM3Ub();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}


BOOL CUMLandValueDB::addTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,tsetspc_m3sk_m3ub) values(:1,:2,:3,:4,:5,:6)"),
										TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsDouble()	= rec.getM3SkToM3Ub();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_m3fub_m3to(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
						_T("tsetspc_m3sk_m3fub,tsetspc_m3fub_m3to,tsetspc_extra_info,tsetspc_transp_dist,tsetspc_transp_dist2) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10)"),TBL_TRAKT_SET_SPC);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsDouble()	= rec.getM3SkToM3Fub();
			m_saCommand.Param(7).setAsDouble()	= rec.getM3FubToM3To();
			m_saCommand.Param(8).setAsString()	= rec.getExtraInfo();
			m_saCommand.Param(9).setAsLong()	= rec.getTranspDist1();	// Added 2008-03-06 P�D
			m_saCommand.Param(10).setAsLong()	= rec.getTranspDist2();	// Added 2008-03-14 P�D
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox((LPCWSTR)e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
									_T("tsetspc_dcls,tsetspc_qdesc_index,tsetspc_qdec_name) values(:1,:2,:3,:4,:5,:6,:7,:8)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsDouble()	= rec.getDiamClass();
			m_saCommand.Param(7).setAsLong()	= rec.getSelQDescIndex();
			m_saCommand.Param(8).setAsString()	= rec.getQDescName();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_grot(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
				_T("tsetspc_grot_func_id,tsetspc_grot_percent,tsetspc_grot_price,tsetspc_grot_cost) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getGrotFuncID();
			m_saCommand.Param(7).setAsLong()	= rec.getGrotPercent();
			m_saCommand.Param(8).setAsLong()	= rec.getGrotPrice();
			m_saCommand.Param(9).setAsLong()	= rec.getGrotCost();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::addTraktSetSpc_default_h25(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!traktSetSpcExists(rec))
		{
			sSQL.Format(_T("insert into %s (tsetspc_id,tsetspc_tdata_id,tsetspc_tdata_trakt_id,tsetspc_data_type,tsetspc_specie_id,")
				_T("tsetspc_default_h25) values(:1,:2,:3,:4,:5,:6)"),TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getTSetspcID();
			m_saCommand.Param(2).setAsLong()	= rec.getTSetspcDataID();
			m_saCommand.Param(3).setAsLong()	= rec.getTSetspcTraktID();
			m_saCommand.Param(4).setAsShort()	= rec.getTSetspcDataType();
			m_saCommand.Param(5).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(6).setAsLong()	= rec.getDefaultH25();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!traktSetSpcExists(rec))
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMLandValueDB::updTraktSetSpc_hgtfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_hgt_func_id=:1,tsetspc_hgt_specie_id=:2,tsetspc_hgt_func_index=:3 ")
					_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= rec.getHgtFuncID();
		m_saCommand.Param(2).setAsLong()	= rec.getHgtFuncSpcID();
		m_saCommand.Param(3).setAsLong()	= rec.getHgtFuncIndex();
		m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktSetSpc_volfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_vol_func_id=:1,tsetspc_vol_specie_id=:2,tsetspc_vol_func_index=:3 ")
					_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= rec.getVolFuncID();
		m_saCommand.Param(2).setAsLong()	= rec.getVolFuncSpcID();
		m_saCommand.Param(3).setAsLong()	= rec.getVolFuncIndex();
		m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updTraktSetSpc_barkfunc(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_bark_func_id=:1,tsetspc_bark_specie_id=:2,tsetspc_bark_func_index=:3 ")
								_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= rec.getBarkFuncID();
		m_saCommand.Param(2).setAsLong()	= rec.getBarkFuncSpcID();
		m_saCommand.Param(3).setAsLong()	= rec.getBarkFuncIndex();
		m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktSetSpc_volfunc_ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_vol_ub_func_id=:1,tsetspc_vol_ub_specie_id=:2,tsetspc_vol_ub_func_index=:3 ")
					_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= rec.getVolUBFuncID();
		m_saCommand.Param(2).setAsLong()	= rec.getVolUBFuncSpcID();
		m_saCommand.Param(3).setAsLong()	= rec.getVolUBFuncIndex();
		m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CUMLandValueDB::updTraktSetSpc_m3sk_m3ub(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_m3sk_m3ub=:1 where tsetspc_specie_id=:2 and tsetspc_id=:3 and tsetspc_tdata_id=:4 and tsetspc_tdata_trakt_id=:5 and tsetspc_data_type=:6"),
								 TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getM3SkToM3Ub();
		m_saCommand.Param(2).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(3).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(4).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(6).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktSetSpc_m3fub_m3to(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_m3sk_m3fub=:1,tsetspc_m3fub_m3to=:2,tsetspc_extra_info=:3,tsetspc_transp_dist=:4,tsetspc_transp_dist2=:5 ")
								_T("where tsetspc_specie_id=:6 and tsetspc_id=:7 and tsetspc_tdata_id=:8 and tsetspc_tdata_trakt_id=:9 and tsetspc_data_type=:10"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getM3SkToM3Fub();
		m_saCommand.Param(2).setAsDouble()	= rec.getM3FubToM3To();
		m_saCommand.Param(3).setAsString()	= rec.getExtraInfo();
		m_saCommand.Param(4).setAsLong()	= rec.getTranspDist1();
		m_saCommand.Param(5).setAsLong()	= rec.getTranspDist2();
		m_saCommand.Param(6).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(8).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(9).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(10).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktSetSpc_rest_of_misc_data(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_dcls=:1,tsetspc_qdesc_index=:2,tsetspc_qdesc_name=:3 ")
					_T("where tsetspc_specie_id=:4 and tsetspc_id=:5 and tsetspc_tdata_id=:6 and tsetspc_tdata_trakt_id=:7 and tsetspc_data_type=:8"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getDiamClass();
		m_saCommand.Param(2).setAsLong()	= rec.getSelQDescIndex();
		m_saCommand.Param(3).setAsString()	= rec.getQDescName();
		m_saCommand.Param(4).setAsLong()	= rec.getSpcID();
		m_saCommand.Param(5).setAsLong()	= rec.getTSetspcID();
		m_saCommand.Param(6).setAsLong()	= rec.getTSetspcDataID();
		m_saCommand.Param(7).setAsLong()	= rec.getTSetspcTraktID();
		m_saCommand.Param(8).setAsShort()	= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktSetSpc_grot(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_grot_func_id=:1,tsetspc_grot_percent=:2,tsetspc_grot_price=:3,tsetspc_grot_cost=:4 ")
					_T("where tsetspc_specie_id=:5 and tsetspc_id=:6 and tsetspc_tdata_id=:7 and tsetspc_tdata_trakt_id=:8 and tsetspc_data_type=:9"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getGrotFuncID();
		m_saCommand.Param(2).setAsDouble()	= rec.getGrotPercent();
		m_saCommand.Param(3).setAsDouble()	= rec.getGrotPrice();
		m_saCommand.Param(4).setAsDouble()	= rec.getGrotCost();
		m_saCommand.Param(5).setAsLong()		= rec.getSpcID();
		m_saCommand.Param(6).setAsLong()		= rec.getTSetspcID();
		m_saCommand.Param(7).setAsLong()		= rec.getTSetspcDataID();
		m_saCommand.Param(8).setAsLong()		= rec.getTSetspcTraktID();
		m_saCommand.Param(9).setAsShort()		= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::updTraktSetSpc_default_h25(CTransaction_trakt_set_spc &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tsetspc_default_h25=:1 ")
					_T("where tsetspc_specie_id=:2 and tsetspc_id=:3 and tsetspc_tdata_id=:4 and tsetspc_tdata_trakt_id=:5 and tsetspc_data_type=:6"),TBL_TRAKT_SET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsDouble()	= rec.getDefaultH25();
		m_saCommand.Param(2).setAsLong()		= rec.getSpcID();
		m_saCommand.Param(3).setAsLong()		= rec.getTSetspcID();
		m_saCommand.Param(4).setAsLong()		= rec.getTSetspcDataID();
		m_saCommand.Param(5).setAsLong()		= rec.getTSetspcTraktID();
		m_saCommand.Param(6).setAsShort()		= rec.getTSetspcDataType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMLandValueDB::addObjectLog(CElv_object_log &log)	// #3877
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("insert into %s (olog_object_id, olog_object_name, olog_type, olog_user, olog_db) values(:1,:2,:3,:4,:5)"), TBL_ELV_OBJECT_LOG);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= log.getObjectId();
		m_saCommand.Param(2).setAsString()	= log.getObjectName();
		m_saCommand.Param(3).setAsLong()	= log.getType();
		m_saCommand.Param(4).setAsString()	= log.getUser();
		m_saCommand.Param(5).setAsString()	= log.getDb();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------------------------


BOOL CUMLandValueDB::getExternalDocuments(LPCTSTR tbl_name,int link_id,vecTransaction_external_docs &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where extdoc_link_tbl=:1 and extdoc_link_id=:2"),TBL_EXTERNAL_DOCS);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsString()	= tbl_name;
		m_saCommand.Param(2).setAsLong()	= link_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_external_docs(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asString(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asShort(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asString(),
													m_saCommand.Field(7).asString(),
													m_saCommand.Field(8).asString()));
		}
	}
	catch(SAException &e)
	{
		 AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


unsigned int FromFileWriter(SAPieceType_t &ePieceType, void *pBuf, unsigned int nLen, void *pAddlData);
SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename);
void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData);


SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename)
{
	SAString s;
	//char sBuf[32*1024];
	TCHAR sBuf[32*1024];
	FILE *pFile = _tfopen(sFilename, _T("rb"));

	if(!pFile)
		SAException::throwUserException(-1, 
		(SAString)_T("Error opening file '%s'\n"), sFilename);
	do
	{
		unsigned int nRead = (unsigned int)fread(sBuf, 1*sizeof(TCHAR), sizeof(sBuf)/sizeof(TCHAR), pFile);
		s += SAString(sBuf, nRead);
	}
	while(!feof(pFile));
	
	fclose(pFile);
	
	return s;
}


static FILE *pFile = NULL;
static int nTotalBound;
static int nTotalRead;

unsigned int FromFileWriter(SAPieceType_t &ePieceType,	void *pBuf, unsigned int nLen, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece)
	{
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("rb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s'"), sFilename);
		
		nTotalBound = 0;
	}
	
	unsigned int nRead = (unsigned int)fread(pBuf, 1, nLen, pFile);		//TODO: might need to change this if using Unicode and TCHAR
	nTotalBound += nRead;
	// show progress
	//printf("%d bytes of file bound\n", nTotalBound);
	
	if(feof(pFile))
	{
		ePieceType = SA_LastPiece;
		fclose(pFile);
		pFile = NULL;
	}
	
	return nRead;
}

void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece || ePieceType == SA_OnePiece)
	{
		nTotalRead = 0;
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("wb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s' for writing"), sFilename);
	}

	CString csMsg;

	fwrite(pBuf, 1, nLen, pFile);
	nTotalRead += nLen;

//	csMsg.Format(_T("nLen %d\nnTotalRead %d"),nLen, nTotalRead);
//	AfxMessageBox(csMsg);

	// show progress
	//printf("%d bytes of %d read\n", nTotalRead, nBlobSize);

  if(ePieceType == SA_LastPiece || ePieceType == SA_OnePiece)
	{
		fclose(pFile);
		pFile = NULL;
	}
}

BOOL CUMLandValueDB::loadImage(CString path, int id)
{
	CString csSQL,S;
	BOOL bRet = TRUE;
	short nIsNullCounter = 0;

	try
	{
		csSQL.Format(_T("select picture from %s where id=%d"), TBL_CONTACTS, id);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		
		m_saCommand.Field(1).setLongOrLobReaderMode(SA_LongOrLobReaderManual);
		while(m_saCommand.FetchNext())
		{
				if (m_saCommand.Field(1).isNull()) 
				{
					bRet = FALSE;
				}
				else
				{
					m_saCommand.Field(1).ReadLongOrLob(IntoFileReader,	// our callback to read BLob content into file
																						 32*1024,		// our desired piece size
																						 (void*)/*(const char*)*/(LPCTSTR)path	// additional data, filename in our example
																						 );
					nIsNullCounter++;
				}
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nIsNullCounter > 0);
}

BOOL CUMLandValueDB::GetCategories(vecTransactionSampleTreeCategory &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"), TBL_SAMPLE_TREE_CATEGORY);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_sample_tree_category(m_saCommand.Field("id").asLong(),
				(LPCTSTR)(m_saCommand.Field("category").asString()),
				(LPCTSTR)(m_saCommand.Field("notes").asString()),
				_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox( e.ErrText().GetWideChars() );
		doRollback();
		return FALSE;
	}

	return TRUE;
}
//#5009 PH 20160616
BOOL CUMLandValueDB::getPropertyInfoForDialog(int id,LPCTSTR &objName,LPCTSTR &propName,LPCTSTR &propNumber,int &objId) {
	CString sSQL;
	try
	{		
		sSQL.Format(L"SELECT * FROM %s AS p,%s AS po,%s AS o "
			L"WHERE p.id=po.prop_id "
			L"AND po.prop_object_id=o.object_id "
			L"AND p.id=%d", TBL_PROPERTY,TBL_ELV_PROP,TBL_ELV_OBJECT,id);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		m_saCommand.FetchNext();

		objName=(LPCTSTR)m_saCommand.Field("object_name").asString();
		propName=(LPCTSTR)m_saCommand.Field("prop_name").asString();
		propNumber=(LPCTSTR)m_saCommand.Field("prop_number").asString();
		objId=(int)m_saCommand.Field("prop_object_id").asLong();
	
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox( e.ErrText().GetWideChars() );
		doRollback();
		return FALSE;
	}

	return TRUE;
}