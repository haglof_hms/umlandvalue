// ResultFormView.cpp : implementation file
//

#include "stdafx.h"
#include "ResultFormView.h"


// CResultFormView dialog

IMPLEMENT_DYNAMIC(CResultFormView, CDialog)

CResultFormView::CResultFormView(CWnd* pParent /*=NULL*/)
	: CDialog(CResultFormView::IDD, pParent)
{

}

CResultFormView::~CResultFormView()
{
}

void CResultFormView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CResultFormView, CDialog)
END_MESSAGE_MAP()


// CResultFormView message handlers
