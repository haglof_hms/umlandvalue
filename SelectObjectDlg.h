#pragma once

#include "Resource.h"

// CSelectObjectDlg dialog

class CSelectObjectDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CSelectObjectDlg)

	BOOL m_bIsSelectionOK;
	int m_nIndexLCtrl;

	CString m_sLangFN;
	vecTransaction_elv_object m_vecObjects;
	CTransaction_elv_object m_recSelectedObject;

	CXTResizeGroupBox m_wndGroup_object;

	CXTListCtrl m_wndLCtrl;
	CXTHeaderCtrl   m_Header;

	CMyExtStatic m_wndLblObjName;
	CMyExtStatic m_wndLblObjID;

	CMyExtEdit m_wndEdObjName;
	CMyExtEdit m_wndEdObjID;

	CButton m_wndOKBtn;
	CButton m_wndFromTmpl;
	CButton m_wndCancelBtn;

	CString m_sObjName;
	CString m_sObjID;

	CString m_sMsgCap;
	CString m_sMsgNoSelection;
	CString m_sMsgNameAndID;

	void setupList(void);
public:
	CSelectObjectDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectObjectDlg();

	void setObjects(vecTransaction_elv_object &vec)	{	m_vecObjects = vec;	}

	CTransaction_elv_object& getSelectedObject(void)	{ return m_recSelectedObject; }

	CString getObjName(void)	{ return m_sObjName; }
	CString getObjID(void)		{ return m_sObjID; }

// Dialog Data
	enum { IDD = IDD_DIALOG6 };

protected:
	//{{AFX_VIRTUAL(CSelectObjectDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	afx_msg void OnItemchangedLCtrl(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedFromTmpl();
	afx_msg void OnEdObjName();
	afx_msg void OnEdObjID();
};
