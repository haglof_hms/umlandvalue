#pragma once

#include "Resource.h"

#include "CruiseAndEvalueFormView.h"

#include "SimpleSplitter.h"

#include "ResLangFileReader.h"

#include "libxl.h"

// CPropertiesFormView form view

class CPropertiesFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPropertiesFormView)

	//Removed 2100305 not needed in sorting function
	//enum enum_sortOrderDirection { SORT_GT_TO_LT, SORT_LT_TO_GT, SORT_NONE };

	//private:
	BOOL m_bInitialized;
	BOOL m_bInitializedSplitter;

	RLFReader m_xml;

	int m_nWindowPos_y;
	int m_nWindowPos_x;
	int m_nWindowPosSet_y;

	CString m_sLangFN;

	CString sEvalueTabCaption;
	CString sCruisingTabCaption;
	CString m_sObjTabCaption;
	CString m_sPropTabCaption;

	CString m_sMsgCap;
	CString m_sMsgPropertyName;
	CString m_sMsgRemoveProperty;
	CString m_sMsgRemoveProperty2;
	CString m_sMsgRemoveProperty3;
	CString m_sMsgAddEvaluestand;
	CString m_sMsgRemoveEvaluestand;
	CString m_sMsgRemoveCruise;
	CString m_sMsgRemoveTraktPropConenction;
	CString m_sMsgMatchAllStandsToProperties;
	CString m_sMsgChangeSortOrder;
	CString m_sMsgNotallowedSortNr;
	CString m_sMsgBatchStandTmpl;

	CString m_sStatusBarStatus;
	CString m_sStatusBarShowLogBook;
	CString m_sStatusBarProperty;
	CString m_sStatusBarSavingEvalStand;
	CString m_sStatusBarRemoveEvalStand;
	CString m_sStatusBarRecalcEvalStand;

	CString m_sLoggMsg1;
	CString m_sLoggHeader;
	CString m_sLoggObject;
	CString m_sLoggProperty;
	CString m_sLoggCreated;
	CString m_sLoggPrlMatch;
	CString m_sLoggCostMatch;
	CString m_sLoggDCLSMatch;
	CString m_sLoggObjPrl;
	CString m_sLoggObjCost;
	CString m_sLoggObjDCLS;
	CString m_sLoggTraktPrl;
	CString m_sLoggTraktCost;
	CString m_sLoggTraktDCLS;
	CString m_sLogg_cm;

	CString m_sLogCap;
	CString m_sCancel;
	CString m_sPrintOut;
	CString m_sSaveToFile;

	CString m_sPopMenuCreateLOwnerGroups;
	CString m_sPopMenuSaveLOwnerGroups;
	CString m_sPopMenuDelLOwnerGroups;
	CString m_sPopMenuReportByGrpID;
	CString m_sPopMenuCreateSortOrder;

	CString m_sFieldChooser;
	CString m_sGroupBy;

	CString m_sNumOfLogEntrySort;	// st = styck

	mapString m_mapForrestNormErrText;

	CMyReportCtrl m_wndReportProperties;
//	CXTPReportControl m_wndReportProperties;
	CImageList m_ilIcons;

	CStringArray m_arrStatus;

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	vecTransactionProperty m_vecSelectedProperties;	// Selected from "fst_property_table"; 080415 p�d
	
	vecTransaction_elv_properties_other_comp m_vecOtherComp;

	vecTransaction_elv_properties m_vecELV_properties;
	CTransaction_elv_properties m_recActiveProperty;
	BOOL populateProperties(void);

	CXTPReportSubListControl m_wndSubList;

	CSTD_Reports m_recReports;
	vecSTDReports m_vecReports;
	void runPrintout(CSTD_Reports &);

	void LoadReportState();
	void SaveReportState();

	std::map<int,int> m_mapNumOfLogBookEntriesPerProperty;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);

	CSimpleSplitter m_wndSplitter;
	int m_nOrientation;
	int m_SplitterPaneSizes[NUMBER_OF_PANES];

	BOOL m_bDoCompleteReCalc;
	vecTransaction_eval_evaluation m_vecElvEvaluatedNotCalc;
	void getObjectEvaluatedNotCalculated(void);
	BOOL calculateThisEvaluation(CTransaction_eval_evaluation& rec);

	// Added 2009-09-15 p�d
	vecTransaction_property_status m_vecPropStatus;
	void getPropActionStatusFromDB(void);

	CStringArray m_sarrOfferAccepted;
	vecIconToogle m_vecIconToogle;

	// Added 110617 p�d
	void setP30NN_SIMax(vecObjectTemplate_p30_nn_table& vec);

	void changeVoluntaryDealCaption(CString caption);

protected:
	CPropertiesFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertiesFormView();

	void setupReportProperties(void);

	void addStatusConstraints(void);

	void getPropertiesFromDB(int obj_id);	
	void savePropertiesToDB(CTransaction_elv_properties &);
	void getNumOfEntriesInLogBookPerPropertyFromDB(int obj_id);

	void calculateEvaluationStand(void);
	void calculateAllEvaluationStands(CProgressDlg &prog_dlg);

	// "Ber�kna frivillig uppg�relse"
	double caluclateVoluntaryDeal(CTransaction_elv_properties& prop);
	// "Ber�kna f�rdyrad avverkning"
	double calculateHigherCosts(CTransaction_elv_properties& prop);

	BOOL getPricelistBasedOnTemplateInformation(int id,int type_of,CTransaction_pricelist& data);
	BOOL getCostsTmplBasedOnTemplateInformation(int id,LPCTSTR costs_tmpl_name,CTransaction_costtempl& data);

	void writeSplitBarSettingsToRegistry(void);
	void readSplitBarSettingsFromRegistry(void);

public:
	enum { IDD = IDD_FORMVIEW6 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	CMyReportCtrl *getReportProperties()
	{
		return &m_wndReportProperties;
	}

	CCruisingData *getCruisingView(void);
	CEvaluatedData *getEvaluatedView(void);

	// Added 2009-03-30 p�d
	// Check if Cruise stands only holds RandTrees.
	// If so, NO EVALUATIONSTANDS can be calculated; 090330 p�d
	BOOL isThereOnlyRandTrees(void);
	BOOL isThereOnlyRandTreesInStand(CTransaction_elv_cruise &);

	BOOL calculateStandsForProperty(CTransaction_elv_properties& prop,BOOL use_dialog,CStringArray& error_log,short get_data_as,CTransaction_elv_object *pObj=NULL);

    void getObjectp30Speciesinfo(vecTransactionSpecies& vecP30Species,int nP30DbId);

	void calculateEvaluationStandFromCruise(void);
	void calculateAllEvaluationStandsFromCruises(CProgressDlg &prog_dlg);

	void setSelectedProperties(vecTransactionProperty &);

	void getPropertiesInReport(vecTransaction_elv_properties &);

	void getPropertiesInReport(libxl::Sheet* sheet,libxl::Format **fmt);

	BOOL removeProperty(short todo,bool bDoAll,bool *bToAll,bool *bYesToAll,bool *bNoToAll);

	void setActivePropertyInReport(void);
   
	//Lagt till funktion f�r att kunna s�tta en rad fokuserad utanf�r formviewn, skickar ut pekare till rapportdelen, pga ta bort flera fastigheter feature #2456 20111111 J�
	CMyReportCtrl* GetReport();
	
	BOOL matchStandsForActiveProperty(bool use_dlg = TRUE);
	void matchStandsPerPropertyForObject(CProgressDlg &prog_dlg);
	void matchAllStandsPerPropertyForObject();

	void removeStandInProperty(void);

	void addNewVStandToActiveProperty(void);
	void addNewVStandToActivePropertyWithDialog(void);

	void saveAndCalculateVStandInActiveProperty(bool do_calc);
	void saveAndCalculateVStandInActiveProperty(CString best_num,CString best_name,CString si,CString tgl,double areal,double volume,int age,double corr_fac,BOOL do_update,int sida,BOOL bTillf);
	void saveAndCalculateVStands(CProgressDlg &prog_dlg);

	void removeCalculateVStandFromActiveProperty();
	void removeAllCalculateVStandsFromActiveProperty();
	void removeAllCalculateVStands(CProgressDlg &prog_dlg);

	void groupPropertiesOnLandOwners(void);
	void groupPropertiesOnPropertyNumbers(void);
	void savePropertyGroupings(bool populate = true);
	void removePropertyGroupings(void);
	void setPropertySortOrder(bool populate = true);

	void savePropertyTypeOfAction(void);

	CTransaction_elv_properties* getActivePropertyRecord(void);

	void recalulateAllVouluntaryDeals(void);
	void recalulateAllHigherCosts(void);
	void recalulateAllOtherCosts(void);

	int getSelectedPropertyId();
	void setSelectedPropertyId(int id);

	void calculateStormDry(void);	// 090908 p�d
	void calculateStormDry(int obj_id,vecTransaction_elv_cruise& vec_cruise);	// 090910 p�d

	void caclulateCorrFactor(void);
	void caclulateCorrFactor(double fVolume,CString tgl,CString si,int age,double *corr);

	int getFocusedPropertyID(void);

	inline void resetPropertyReport(void)
	{
		if (m_wndReportProperties.GetSafeHwnd())
			m_wndReportProperties.ResetContent();

	}
	inline void resetCruisingReport(void)
	{
		CCruisingData *pView = getCruisingView();
		if (pView != NULL) pView->resetReport();
	}
	inline void resetEvaluatedReport(void)
	{
		CEvaluatedData *pView = getEvaluatedView();
		if (pView != NULL) pView->resetReport();
	}
	inline BOOL isAnyProperties(void)
	{
		return (m_wndReportProperties.GetRows()->GetCount() > 0);
	}

	inline void setSplitBar(void)
	{
		if (m_wndSplitter.GetSafeHwnd())
		{
			if (m_wndSplitter.GetOrientation() == SSP_HORZ)
				m_wndSplitter.ChangeOrientation(SSP_VERT);
			else
				m_wndSplitter.ChangeOrientation(SSP_HORZ);

			m_wndSplitter.SetPaneSizes(m_SplitterPaneSizes);
		}
	}

	inline UINT getNumOfProperties(void) { return (UINT)m_vecELV_properties.size(); }

	inline void setDoComleteReCalc(BOOL action)		{ m_bDoCompleteReCalc = action; }

	BOOL updateAllTraktTemplates(CTransaction_template xml_tmpl,vecTransaction_elv_properties &vec_props);	// Added 90506 p�d, moved to PUBLIC; 090928 p�d

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_MSG

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnUserSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportHeaderColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
