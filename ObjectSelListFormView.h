#pragma once

#include "Resource.h"

#include "ResLangFileReader.h"

#include "UMLandValueDB.h"

//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CTraktSelListFrame; 070108 p�d

class CObjectReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CObjectReportFilterEditControl)
public:
	CObjectReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// CObjectSelListReportRec

class CObjectSelListReportRec : public CXTPReportRecord
{
	CTransaction_elv_object recObject;
	UINT m_nIndex;
protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{	return m_nValue; 	}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CObjectSelListReportRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));					
		AddItem(new CTextItem(_T("")));					
		AddItem(new CIntItem(0));						
		AddItem(new CIntItem(0));						
	    AddItem(new CTextItem(_T("")));					// �rendenummer
		AddItem(new CTextItem(_T("")));					// Datum
		AddItem(new CTextItem(_T("")));					// Utf�rd av
		AddItem(new CTextItem(_T("")));					// Noteringar
		AddItem(new CTextItem(_T("")));					//#3385 status p� objektet
		
	}

	CObjectSelListReportRec(int idx,CTransaction_elv_object& rec,CString net_infr,CString infr, CString status)
	{
		int nPos = 0;
		m_nIndex = idx;
		recObject = rec;
		AddItem(new CTextItem((rec.getObjectName())));		// "Objeksnamn"
		AddItem(new CTextItem((rec.getObjIDNumber())));		// "Objektid"
		AddItem(new CTextItem((rec.getObjLittra())));		// "Ledningslittra"
		AddItem(new CTextItem((rec.getObjGrowthArea())));	// "Tillv�xtomr�de"
		AddItem(new CTextItem(infr));						// "Typ av intr�ng"
		AddItem(new CTextItem((net_infr)));					// "Intr�nget avser; Lokalt n�t,Regionalt n�t"
		AddItem(new CIntItem(rec.getObjLatitude()));		// "Breddgrad"
		AddItem(new CIntItem(rec.getObjAltitude()));		// "H�jd �ver havet"
		//Lagt till �rendenummer, datum, utf�rd av, noteringar 20120531 J� #3116
		AddItem(new CTextItem(rec.getObjErrandNum()));		//�rendenummer
		AddItem(new CTextItem(rec.getObjStartDate()));		//Datum
		AddItem(new CTextItem(rec.getObjCreatedBy()));		//Utf�rd av
		AddItem(new CTextItem(rec.getObjNotes()));			//Noteringar
		AddItem(new CTextItem(status));						//#3385
	}

	CTransaction_elv_object& getRecord(void)
	{
		return recObject;
	}

	int getIndex(void)
	{
		return m_nIndex;
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

//////////////////////////////////////////////////////////////////////////////
// CObjectSelListFormView form view

class CObjectSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CObjectSelListFormView)

protected:
	CObjectSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CObjectSelListFormView();
/*
	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;
*/
	vecTransaction_elv_object m_vecObjectData;
	void getObject(void);

	CStringArray m_arrTypeOfNet;
	CStringArray m_arrObjectStatus;	//#3385

	StrMap m_mapTypeOfInfr;
	
	BOOL setupReport(void);
	void populateReport(void);

	void LoadReportState(void);
	void SaveReportState(void);

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif
	void Refresh(void);	//new #3385

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	afx_msg void OnRefresh();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
