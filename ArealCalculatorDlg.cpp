// ArealCalculatorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ArealCalculatorDlg.h"

#include "ResLangFileReader.h"

// CArealCalculatorDlg dialog

IMPLEMENT_DYNAMIC(CArealCalculatorDlg, CDialog)


BEGIN_MESSAGE_MAP(CArealCalculatorDlg, CDialog)
	ON_EN_CHANGE(IDC_EDIT_CALC1, OnEnChangeEditCalc1)
	ON_EN_CHANGE(IDC_EDIT_CALC2, OnEnChangeEditCalc2)
END_MESSAGE_MAP()

CArealCalculatorDlg::CArealCalculatorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CArealCalculatorDlg::IDD, pParent)
{

}

CArealCalculatorDlg::~CArealCalculatorDlg()
{
}

void CArealCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)

	DDX_Control(pDX, IDC_LBL_CALC1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_CALC2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_CALC3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_CALC5, m_wndLbl5);

	DDX_Control(pDX, IDC_EDIT_CALC1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT_CALC2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT_CALC4, m_wndEdit4);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP
}

BOOL CArealCalculatorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_wndEdit1.SetRange(0,10000);
	m_wndEdit2.SetRange(0,10000);

	m_wndEdit4.SetEnabledColor(BLACK,INFOBK);
	m_wndEdit4.SetDisabledColor(BLACK,INFOBK);
	m_wndEdit4.SetReadOnly();

	SetWindowText((m_mapDlgText[IDS_STRING420]));
	m_wndOKBtn.SetWindowText((m_mapDlgText[IDS_STRING421]));
	m_wndCancelBtn.SetWindowText((m_mapDlgText[IDS_STRING422]));

	m_wndLbl1.SetWindowText((m_mapDlgText[IDS_STRING4200]));
	m_wndLbl2.SetWindowText((m_mapDlgText[IDS_STRING4201]));
	m_wndLbl3.SetWindowText((m_mapDlgText[IDS_STRING4202]));
	m_wndLbl5.SetWindowText((m_mapDlgText[IDS_STRING4203]));

	m_fAreal = 0.0;

	return TRUE;
}

// CArealCalculatorDlg message handlers

// Calculate areal based on Length and Width; 080513 p�d

void CArealCalculatorDlg::OnEnChangeEditCalc1()
{

	double fLength = 0.0;
	double fWidth = 0.0;

	// Get values added for Length and Width; 080513 p�d
	fLength = m_wndEdit1.getFloat();
	fWidth = m_wndEdit2.getFloat();
	if (fLength > 0.0 && fWidth > 0.0)
	{
		m_fAreal = (fLength*fWidth)/10000.0;
		m_wndEdit4.setFloat(m_fAreal,3);
	}
	else
	{
		m_fAreal = 0.0;
		m_wndEdit4.setFloat(m_fAreal,3);
	}
}

void CArealCalculatorDlg::OnEnChangeEditCalc2()
{
	double fLength = 0.0;
	double fWidth = 0.0;

	// Get values added for Length and Width; 080513 p�d
	fLength = m_wndEdit1.getFloat();
	fWidth = m_wndEdit2.getFloat();
	if (fLength > 0.0 && fWidth > 0.0)
	{
		m_fAreal = (fLength*fWidth)/10000.0;
		m_wndEdit4.setFloat(m_fAreal,3);
	}
	else
	{
		m_fAreal = 0.0;
		m_wndEdit4.setFloat(m_fAreal,3);
	}
}

