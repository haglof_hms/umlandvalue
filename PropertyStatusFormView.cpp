// PropertyStatusFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PropertyStatusFormView.h"

#include "ResLangFileReader.h"

// CPropertyStatusFormView

IMPLEMENT_DYNCREATE(CPropertyStatusFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertyStatusFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_CLICK, ID_REPORT_PROP_STATUS, OnReportItemClick)
END_MESSAGE_MAP()

CPropertyStatusFormView::CPropertyStatusFormView()
	: CXTResizeFormView(CPropertyStatusFormView::IDD)
{
	m_bInitialized = FALSE;
}

CPropertyStatusFormView::~CPropertyStatusFormView()
{
}

void CPropertyStatusFormView::OnDestroy()
{
	//savePropActionStatus();

	if (m_pDB != NULL)
		delete m_pDB;

	m_ilIcons.DeleteImageList();

	SaveReportState();

	CXTResizeFormView::OnDestroy();	
}

void CPropertyStatusFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CPropertyStatusFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CPropertyStatusFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (!	m_bInitialized )
	{

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		getPropActionStatusFromDB();

		// Get a map of PropertyActionStatus used in "elv_properties_table"; 090924 p�d
		getPropertyStatusUsedFromDB();

		setupReport();

		populateData();

		LoadReportState();

		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

void CPropertyStatusFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType,cx,cy);

	if (m_wndReport.GetSafeHwnd())
	{
		setResize(&m_wndReport,2,2,cx-4,cy-4);
	}
}

BOOL CPropertyStatusFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CPropertyStatusFormView diagnostics

#ifdef _DEBUG
void CPropertyStatusFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPropertyStatusFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPropertyStatusFormView message handlers

void CPropertyStatusFormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	RLFReader xml;

	if (m_wndReport.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, ID_REPORT_PROP_STATUS, FALSE, FALSE))
		{
			TRACE0( "Failed to create m_wndReport\n" );
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == 0) return;

	if (fileExists(m_sLangFN))
	{
		if (xml.Load(m_sLangFN))
		{
				if (m_wndReport.GetSafeHwnd() != NULL)
				{
					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					m_sMsgCap = xml.str(IDS_STRING229);
					m_sMsgDelete = xml.str(IDS_STRING3034);
					m_sMsgDelete2 = xml.str(IDS_STRING3035);

					m_wndReport.SetImageList(&m_ilIcons);
				
					m_wndReport.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d
					// Checkbox; Not checked = No recalculation, Checked = Do recalculation
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING3030)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					// "S�tta en sorteringsordning"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING3031)), 50));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment(DT_CENTER);
					pCol->SetVisible(FALSE);
					// "Namn p� Status/Bakgrundsf�rg"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING3032)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					// "Visar on status anv�nds i ett eller flera objekt"
					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING3033)), 20));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment(DT_CENTER);

					m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndReport.SetMultipleSelection( FALSE );
					m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport.FocusSubItems(TRUE);
					m_wndReport.AllowEdit(TRUE);
					m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
					
					RECT rect;
					GetClientRect(&rect);
					setResize(&m_wndReport,2,2,rect.right-4,rect.bottom-4);

				}	// if (m_wndReport.GetSafeHwnd() != NULL)
				xml.clean();
			}	// if (xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPropertyStatusFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			newPropActionStatus();
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			savePropActionStatus();
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			delPropActionStatus();
			break;
		}	// case ID_DELETE_ITEM :
	}	// switch (wParam)
	return 0L;
}

void CPropertyStatusFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
			case COLUMN_2 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					if (pItemNotify->pRow)
					{
						CELVObjectPropStatusRec *pRec = (CELVObjectPropStatusRec*)pItemNotify->pItem->GetRecord();
						if (pRec != NULL)
						{
							CColorDialog dlg;
							if (dlg.DoModal() == IDOK)
							{
								pRec->setColumnBgndColor(COLUMN_2,dlg.GetColor());
								m_wndReport.Populate();
								m_wndReport.UpdateWindow();
							}
						}	// if (pRec != NULL)
					}	// if (pItemNotify->pRow)
					//populateProperties();
				}	// if (hitTest_X(pt.x,rect.left,13))
				break;
			}

	};
}

void CPropertyStatusFormView::populateData(void)
{
	CTransaction_property_status recPropStatus;

	m_wndReport.ResetContent();
	if (m_vecPropStatus.size() > 0)
	{
		for (UINT i = 0;i < m_vecPropStatus.size();i++)
		{
			recPropStatus = m_vecPropStatus[i];

			m_wndReport.AddRecord(new CELVObjectPropStatusRec(recPropStatus,m_mapPropStatusUsed[recPropStatus.getPropStatusID_pk()]));
		}	// for (UINT i = 0;i < m_vecPropStatus.size();i++)
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}	// if (m_vecPropStatus.size() > 0)
	else
	{
		if (m_pDB != NULL) m_pDB->resetPropetyActionStatusID();
	}
}

void CPropertyStatusFormView::getPropActionStatusFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getPropertyActionStatus(m_vecPropStatus);
	}	// if (m_pDB != NULL)
}

void CPropertyStatusFormView::getPropertyStatusUsedFromDB(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getPropertyActionStatusUsed(m_vecPropStatus,m_mapPropStatusUsed);
	}	// if (m_pDB != NULL)
}

void CPropertyStatusFormView::newPropActionStatus(void)
{
	if (m_wndReport.GetSafeHwnd())
	{
		// g�ra om id till max-id + 1
		short nIdNum = 0;
		if (m_pDB != NULL)
			m_pDB->getNextPropertyActionStatus_id(&nIdNum);

		m_wndReport.AddRecord(new CELVObjectPropStatusRec(getNextPropActionStatus_ordernum(), nIdNum));

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();

		savePropActionStatus();
	}
}

void CPropertyStatusFormView::savePropActionStatus(void)
{
	CTransaction_property_status recPropStatus;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CELVObjectPropStatusRec *pRec = NULL;
	if (pRows != NULL && m_pDB != NULL)
	{
		// Repopulate; 090915 p�d
		m_wndReport.Populate();

		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CELVObjectPropStatusRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				recPropStatus = CTransaction_property_status(pRec->getRecord().getPropStatusID_pk(),
																										 i+1,
																										 pRec->getIconColumnText(COLUMN_2),
																										(pRec->getColumnCheck(COLUMN_0) ? 1:0),
																										 pRec->getRecord().getPropStatusRCol(),
																										 pRec->getRecord().getPropStatusGCol(),
																										 pRec->getRecord().getPropStatusBCol(),
																										 _T(""));
				if (!m_pDB->addPropertyActionStatus(recPropStatus))
					m_pDB->updPropertyActionStatus(recPropStatus);
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
		getPropActionStatusFromDB();
		populateData();
	}	// if (pRows != NULL)
}

void CPropertyStatusFormView::delPropActionStatus(void)
{
	CString S;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CELVObjectPropStatusRec *pRec = NULL;
	if (pRow != NULL)
	{
		pRec = (CELVObjectPropStatusRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			// First we'll check if the status we're about to delete
			// is used. If so, we can't delete it; 090924 p�d
			if (m_pDB != NULL)
			{
				if (m_pDB->isPropertyActionStatusUsed(pRec->getRecord().getPropStatusID_pk()))
				{
					::MessageBox(this->GetSafeHwnd(),m_sMsgDelete,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				}
				else
				{
					if (::MessageBox(this->GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONSTOP | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
						m_pDB->delPropertyActionStatus(pRec->getRecord());
				}
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
		getPropActionStatusFromDB();
		populateData();
	}	// if (pRows != NULL)
}

short CPropertyStatusFormView::getNextPropActionStatus_ordernum(void)
{
	short nOrderNum = 0;
	if (m_pDB != NULL)
		m_pDB->getNextPropertyActionStatus_ordernum(&nOrderNum);

	return nOrderNum + 1;	// Add to last ordernum; 090915 p�d
}


void CPropertyStatusFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary(REG_WP_PROP_STATUS_REPORT_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_wndReport.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CPropertyStatusFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_wndReport.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_WP_PROP_STATUS_REPORT_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}
