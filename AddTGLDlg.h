#pragma once

#include "Resource.h"
#include "afxwin.h"

// CAddTGLDlg dialog

class CAddTGLDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddTGLDlg)

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;
	CMyExtStatic m_wndLblTslbl;

	CButton m_wndCheckBox;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CXTMaskEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;

	CButton m_wndOKBtn;
	CButton m_wndOKBtn2;
	CButton m_wndCancel;

	int m_nPine,m_nSpruce,m_nBirch,m_nAge,m_nSide;
	double m_fAreal,m_fVolym;
	CString m_sReturnTGL;
	CString m_sStandNum;
	CString m_sStandName;
	CString m_sStandSI;

	CString m_sMsgCap;
	CString m_sMsgSumError;
	CString m_sMsgSIError;

	BOOL m_bCreateVStand;
	BOOL m_bTillfUtnyttj;

public:
	CAddTGLDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddTGLDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG21 };


	void setLabels(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR lbl1,LPCTSTR lbl2,LPCTSTR lbl3);

	void setTGL(LPCTSTR tgl);
	void setStandNum(LPCTSTR stand_num)		{	m_sStandNum = stand_num; }
	void setStandName(LPCTSTR stand_name)	{	m_sStandName = stand_name; }
	void setStandSI(LPCTSTR stand_si)		{	m_sStandSI = stand_si; }
	void setStandAreal(double stand_areal)	{	m_fAreal = stand_areal; }
	void setStandVolume(double stand_vol)	{	m_fVolym = stand_vol; }
	void setStandAge(int stand_age)			{	m_nAge = stand_age; }
	void setStandSide(int stand_side)		{	m_nSide = stand_side; }
	void setTillfUtnyttj(BOOL tillf);

	CString& getTGL(void)	{ return m_sReturnTGL; }
	CString getStandNum()	{ return m_sStandNum; }
	CString getStandName()	{ return m_sStandName; }
	CString getStandSI()	{ return m_sStandSI; }
	double getStandAreal()	{ return m_fAreal; }
	double getStandVol()	{ return m_fVolym; }
	int getStandAge()		{ return m_nAge; }
	int getStandSide()		{ return m_nSide; }
	BOOL getTillfUtnyttj()	{ return m_bTillfUtnyttj;}

	BOOL getDoCreateVStand(){ 
		return m_bCreateVStand; 
	}

protected:
	//{{AFX_VIRTUAL(CAddTGLDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton210();
	afx_msg void OnBnClickedSparaVardering();
};
