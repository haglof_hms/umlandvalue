// MapTreeTypesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MapTreeTypesDlg.h"
#include "ResLangFileReader.h"

TREETYPE CMapTreeTypesDlg::vecTreeTypes[4] = {{_T(""), 3}, {_T(""), 5}, {_T(""), 0}, {_T(""), 7}};


// CMapTreeTypesDlg dialog

IMPLEMENT_DYNAMIC(CMapTreeTypesDlg, CDialog)

CMapTreeTypesDlg::CMapTreeTypesDlg(TreeTypeMap *pTreemap,
								   CWnd* pParent /*=NULL*/)
	: CDialog(CMapTreeTypesDlg::IDD, pParent),
	m_pTreemap(pTreemap)
{
}

CMapTreeTypesDlg::~CMapTreeTypesDlg()
{
	// Cleanup
	for( TreeTypeList::iterator iter = m_vals.begin(); iter != m_vals.end(); iter++ )
	{
		CString *pVal = *iter;
		if( pVal )
		{
			delete pVal;
			pVal = NULL;
		}
	}
}

void CMapTreeTypesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CMapTreeTypesDlg::OnInitDialog()
{
	CString curval;

	if( !CDialog::OnInitDialog() )
		return FALSE;

	// Load language
	CString	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			// Set up language
			SetWindowText(xml->str(IDS_STRING3292));

			vecTreeTypes[0].name = xml->str(IDS_STRING3295);
			vecTreeTypes[1].name = xml->str(IDS_STRING3296);
			vecTreeTypes[2].name = xml->str(IDS_STRING3297);
			vecTreeTypes[3].name = xml->str(IDS_STRING3298);

			// Create grid
			RECT rect;
			GetWindowRect(&rect);
			m_grid.Create(CRect(0, 0, rect.right - rect.left - 6, rect.bottom - rect.top - 70), this, ID_MAPTREETYPESGRID);
			m_grid.ShowHelp(FALSE);
			m_grid.SetTheme(xtpGridThemeOffice2003);
			if( m_grid.m_hWnd )
			{
				CXTPPropertyGridItem *pItem;
				CXTPPropertyGridItem *pTypes = m_grid.AddCategory(xml->str(IDS_STRING3293));
				pTypes->Expand();

				// List tree types from Excel
				for( TreeTypeMap::iterator iterm = m_pTreemap->begin(); iterm != m_pTreemap->end(); iterm++ )
				{
					pItem = new CXTPPropertyGridItem(iterm->first);
					curval.Empty();
					for( int i = 0; i < sizeof(vecTreeTypes) / sizeof(TREETYPE); i++ )
					{
						pItem->GetConstraints()->AddConstraint(vecTreeTypes[i].name);
						if( iterm->second == vecTreeTypes[i].type ) curval = vecTreeTypes[i].name; // Find current value
					}
					pItem->SetFlags(xtpGridItemHasComboButton);

					// Bind value to string
					CString *pBind = new CString;
					pItem->BindToString(pBind);
					m_vals.push_back(pBind);
					*pBind = curval;

					pTypes->AddChildItem(pItem);
				}
			}
		}
	}

	return TRUE;
}

void CMapTreeTypesDlg::OnOK()
{
	// Store values to tree type map
	TreeTypeList::iterator iterv = m_vals.begin();
	for( TreeTypeMap::iterator iterm = m_pTreemap->begin(); iterm != m_pTreemap->end(); iterm++ )
	{
		if( iterv != m_vals.end() && !(*iterv)->IsEmpty() )
		{
			// Go through array of tree types to find corresponding id
			for( int i = 0; i < sizeof(vecTreeTypes) / sizeof(TREETYPE); i++ )
			{
				if( vecTreeTypes[i].name == **iterv )
				{
					iterm->second = vecTreeTypes[i].type;
					AfxGetApp()->WriteProfileInt(REG_IMPORT_EXCEL_TREETYPES_KEY, iterm->first, vecTreeTypes[i].type); // Save to registry
					break;
				}
			}
		}

		iterv++;
	}

	CDialog::OnOK();
}


BEGIN_MESSAGE_MAP(CMapTreeTypesDlg, CDialog)
END_MESSAGE_MAP()


// CMapTreeTypesDlg message handlers
