#pragma once

#include "Resource.h"

#include "ObjectResultFormView.h"

// CObjectResultDlg dialog

class CObjectResultDlg : public CDialog
{
	DECLARE_DYNAMIC(CObjectResultDlg)

	CString m_sLangFN;
	RLFReader m_xml;

	CButton m_wndBtnCancel;
	CButton m_wndBtnPrintOut;

	CString m_sLandOwnerNumber;
	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
protected:
	// "St�mplingsl�ngd"
	CObjectResultFormView *getSLenTab(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(TAB_RESULT_SLEN_REPORT);
		if (m_tabManager)
		{
			CObjectResultFormView* pView = DYNAMIC_DOWNCAST(CObjectResultFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CObjectResultFormView, pView);
			return pView;
		}
		return NULL;
	}

	// "V�rdering"
	CObjectResultFormView *getEvalueTab(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(TAB_RESULT_EVAL_REPORT);
		if (m_tabManager)
		{
			CObjectResultFormView* pView = DYNAMIC_DOWNCAST(CObjectResultFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CObjectResultFormView, pView);
			return pView;
		}
		return NULL;
	}

	// "Rotpost"
	CObjectResultFormView *getRotpostTab(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(TAB_RESULT_NETTO_REPORT);
		if (m_tabManager)
		{
			CObjectResultFormView* pView = DYNAMIC_DOWNCAST(CObjectResultFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CObjectResultFormView, pView);
			return pView;
		}
		return NULL;
	}
public:
	CObjectResultDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CObjectResultDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG14 };

	// Setup Landownernumber for ALL tabs; 080603 p�d
	void setLandOwnerNumber(LPCTSTR lnum)
	{
		m_sLandOwnerNumber = lnum;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();

	//{{AFX_MSG(CTabbedViewView)
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};
