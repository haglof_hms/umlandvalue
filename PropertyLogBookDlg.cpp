// PropertyLogBookDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PropertyLogBookDlg.h"

#include "ResLangFileReader.h"

// CPropertyLogBookDlg dialog

IMPLEMENT_DYNAMIC(CPropertyLogBookDlg, CDialog)


BEGIN_MESSAGE_MAP(CPropertyLogBookDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON1, &CPropertyLogBookDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDOK, &CPropertyLogBookDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON2, &CPropertyLogBookDlg::OnBnClickedButton2)
END_MESSAGE_MAP()

CPropertyLogBookDlg::CPropertyLogBookDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropertyLogBookDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CPropertyLogBookDlg::~CPropertyLogBookDlg()
{
	m_vecLogBook.clear();
}

void CPropertyLogBookDlg::OnDestroy()
{
	// Save data on exit; 081014 p�d
	savePropertyLogBook();
	if (m_pDB != NULL)
	{
		// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
		CTransaction_elv_object *pObj = getActiveObject();
		m_nNumOfEntriesInLogBook = m_pDB->getNumOfEntriesForPropertyInLogBook(m_recProperty.getPropID_pk(),pObj->getObjID_pk());
		pObj = NULL;
	}
	
	CDialog::OnDestroy();
}

void CPropertyLogBookDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL_LOGBOOK1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_LOGBOOK2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_LOGBOOK3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_LOGBOOK4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL_LOGBOOK5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL_LOGBOOK6, m_wndLbl6);

	DDX_Control(pDX, IDC_BUTTON1, m_wndAddRowBtn);
	DDX_Control(pDX, IDC_BUTTON2, m_wndDelRowBtn);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	//}}AFX_DATA_MAP
}

BOOL CPropertyLogBookDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING3200)));

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING204)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING3203)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING3204)));
			m_wndLbl2.SetLblFontEx(-1,FW_BOLD);
			m_wndLbl4.SetLblFontEx(-1,FW_BOLD);
			m_wndLbl6.SetLblFontEx(-1,FW_BOLD);

			m_wndAddRowBtn.SetWindowText((xml->str(IDS_STRING3201)));
			m_wndDelRowBtn.SetWindowText((xml->str(IDS_STRING3202)));

			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING2604)));
		}
		delete xml;
	}

	setupReport();
	
	getPropertyLogBook();

	populateData();

	return TRUE;
}


// CPropertyLogBookDlg message handlers

// PRIVATE
void CPropertyLogBookDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_LOGBOOK_PROPS, FALSE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				m_sMsgCap = (xml->str(IDS_STRING229));
				m_sMsgRemoveLogEntry = (xml->str(IDS_STRING2605));

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING3210)), 60));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING3211)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING3212)), 200));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.AdjustScrollBars();


				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_REPORT_LOGBOOK_PROPS),10,80,rect.right - 20,rect.bottom - 120);
			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}
}

void CPropertyLogBookDlg::getPropertyLogBook(void)
{
	if (m_pDB != NULL)
	{
		// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
		CTransaction_elv_object *pObj = getActiveObject();

		m_pDB->getPropertyLogBook(m_recProperty.getPropID_pk(),
															pObj->getObjID_pk(),
															m_vecLogBook);
		pObj = NULL;
	}
}

void CPropertyLogBookDlg::savePropertyLogBook(void)
{
	int nLogType = -2;
	// Get information on active trakt, set in CPageOneFormView (a formview in TabbedView); 070515 p�d
	CTransaction_elv_object *pObj = getActiveObject();

	CTransaction_elv_properties_logbook recLogBook;
	CPropertiesLogBookDataRec *pRec = NULL;
	
	m_wndReport.Populate();// Added 090306 p�d

	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			pRec = (CPropertiesLogBookDataRec *)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				nLogType = pRec->getRecord().getPropLogType();
				recLogBook = CTransaction_elv_properties_logbook(m_recProperty.getPropID_pk(),
																												 pObj->getObjID_pk(),
																												 pRec->getLogDate(), // >getColumnText(COLUMN_0),
																												 pRec->getColumnText(COLUMN_1),
																												 pRec->getColumnText(COLUMN_2),
																												 _T(""),
																												 nLogType);

				if (m_pDB != NULL)
				{
					if (!m_pDB->addPropertyLogBook(recLogBook))
							 m_pDB->updPropertyLogBook(recLogBook);
				}

			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)
}

void CPropertyLogBookDlg::delEntryInPropertyLogBook(CTransaction_elv_properties_logbook &rec)
{
	if (m_pDB != NULL)
	{
		m_pDB->delPropertyLogBook(rec);
	}	// if (m_pDB != NULL)
}

void CPropertyLogBookDlg::OnBnClickedButton1()
{
	CPropertiesLogBookDataRec *pRecNew = NULL;
	CTransaction_elv_properties_logbook recLogBook;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (m_wndReport.GetSafeHwnd())
	{
		pRecNew = (CPropertiesLogBookDataRec *)m_wndReport.AddRecord(new CPropertiesLogBookDataRec());
		m_wndReport.Populate();
		// Added 081006 p�d
		m_wndReport.RedrawControl();
		m_wndReport.SetFocus();
		savePropertyLogBook();
		// Reload logbook; 081014 p�d	
		getPropertyLogBook();
		populateData();
	}
}

void CPropertyLogBookDlg::OnBnClickedButton2()
{
	// No entries to remove; 081006 p�d
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
		if (pRecs->GetCount() == 0)
			return;
	// First ask user if he realy want to remove entry; 080423 p�d
	if(::MessageBox(this->GetSafeHwnd(),(m_sMsgRemoveLogEntry),(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		return;


	//Gjort om funktion f�r att kunna radera fler loggboksinl�gg 20121128 J� #3432
	CPropertiesLogBookDataRec *pRec = NULL;
	CXTPReportSelectedRows *pRows = m_wndReport.GetSelectedRows();
	CXTPReportRow *pRow = NULL;

	if(pRows != NULL && pRecs != NULL)
	{
		int nSize=pRows->GetCount();
		//Struntar i att spara f�rst, det som h�nder om man f�rs�ker deleta en rad som inte finns sparad i databasen �r att
		//funktionen som tar bort den fr�n databasen misslyckas, eftersom den itne finns och den endast f�rsvinner fr�n formul�ret, vilket �r ok.
		//savePropertyLogBook();		

		for(int i = 0; i < nSize;i++)
		{
			pRow = pRows->GetAt(i);
			if(pRow != NULL)
			{
				pRec = (CPropertiesLogBookDataRec*)pRow->GetRecord();
				if (pRec != NULL)
				{
					delEntryInPropertyLogBook(pRec->getRecord());
				}
			}
		}
	}

	/*CPropertiesLogBookDataRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL && pRecs != NULL)
	{
		// Save data before deleting any; 090306 p�d
		savePropertyLogBook();

		pRec = (CPropertiesLogBookDataRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			delEntryInPropertyLogBook(pRec->getRecord());
		}
	}*/

	getPropertyLogBook();
	populateData();
}


void CPropertyLogBookDlg::populateData()
{
	m_wndLbl2.SetWindowText((m_sObjectName));
	m_wndLbl4.SetWindowText((m_sPropName));
	m_wndLbl6.SetWindowText((m_sPropNum));

	m_wndReport.ResetContent();
	if (m_vecLogBook.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogBook.size();i++)
		{
			m_wndReport.AddRecord(new CPropertiesLogBookDataRec(i,m_vecLogBook[i]));
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CPropertyLogBookDlg::OnBnClickedOk()
{
	OnOK();
}

