#pragma once


// CSeelctSpcDlg dialog

class CSeelctSpcDlg : public CDialog
{
	DECLARE_DYNAMIC(CSeelctSpcDlg)

public:
	CSeelctSpcDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSeelctSpcDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG18 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
