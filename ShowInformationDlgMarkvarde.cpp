#include "stdafx.h"
#include "ShowInformationDlgMarkvarde.h"


// CShowInformationDlgMarkvarde dialog
// HMS-49 2020-02-11
IMPLEMENT_DYNAMIC(CShowInformationDlgMarkvarde, CDialog)



BEGIN_MESSAGE_MAP(CShowInformationDlgMarkvarde, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CShowInformationDlgMarkvarde::CShowInformationDlgMarkvarde(CWnd* pParent)
	: CDialog(CShowInformationDlgMarkvarde::IDD, pParent)
{
	m_pDB = NULL;

	m_bInitialized = FALSE;
	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;	
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);


	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;	
	lfFont1.lfStrikeOut = TRUE;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1_strikeout.CreatePointFontIndirect(&lfFont1);




	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	lfFont1.lfStrikeOut = TRUE;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2_strikeout.CreatePointFontIndirect(&lfFont1);

	m_nEvalId=-1;
	m_nObjId=-1;
	m_nPropId=-1;

}

CShowInformationDlgMarkvarde::~CShowInformationDlgMarkvarde()
{
}

void CShowInformationDlgMarkvarde::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();
	m_fnt1_strikeout.DeleteObject();
	m_fnt2_strikeout.DeleteObject();

	m_xml.clean();

	CDialog::OnDestroy();
}

void CShowInformationDlgMarkvarde::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// CShowInformationDlg2 message handlers

BOOL CShowInformationDlgMarkvarde::OnInitDialog()
{
	CDialog::OnInitDialog();
	CWnd *pBtn = NULL;

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			m_xml.Load(m_sLangFN);

			SetWindowText((m_xml.str(IDS_STRING6402)));
			if ((pBtn = GetDlgItem(IDCANCEL)) != NULL)
				pBtn->SetWindowText((m_xml.str(IDS_STRING4245)));
		}
		m_bInitialized = TRUE;
	}		// if (!m_bInitialized)



	return TRUE;
}


// CShowInformationDlg message handlers

void CShowInformationDlgMarkvarde::OnPaint()
{
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-30,10,10);

	showMarkvarde(pDC);

	CDialog::OnPaint();

}


// PRIVATE

void CShowInformationDlgMarkvarde::showMarkvarde(CDC *dc)
{

	CString sName,sDoneBy;
	TEXTMETRIC tm;
	BOOL bFound = FALSE;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties *pProp = getActiveProperty();
	
	double fAndelPine=0.0;
	double fAndelSpruce=0.0;
	double fValuePine=0.0;
	double fValueSpruce=0.0;
	double fP30Pine=0.0;
	double fP30Spruce=0.0;
	double fPreCutInfo_ReducedBy=0.0;
	double fErs_Tot_Reduced=0.0;
	double fErs_Tot=0.0;
	double fAreal=0.0;

	CString sStr=_T("");
	int nLen=0,nLen1=0,nLen2=0,nLen3=0;

	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_hcost_table vecHCost;
	CObjectTemplate_hcost_table recHCost;
	if (pObj != NULL && pProp != NULL && m_pDB != NULL && m_nEvalId!=-1)
	{

		m_nObjId = pObj->getObjID_pk();
		m_nPropId= pProp->getPropID_pk();
		m_nNormId=pObj->getObjUseNormID();

		CTransaction_eval_evaluation *pEval = m_pDB->getObjectEvaluation(m_nPropId,m_nObjId,m_nEvalId);

		if(pEval->getEValType()==EVAL_TYPE_ANNAN)  // #HMS-95 Markv�rde satt av anv�ndare om annan mark �r satt som v�rderingstyp
		{
			dc->SelectObject(&m_fnt2);
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			dc->TextOut(10, 3*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53041)))); //Markv�rde  per ha
			nLen = m_xml.str(IDS_STRING53041).GetLength();

			dc->DrawText(formatData(_T(" = %.1f kr/ha"),pEval->getEValLandValue_ha())			,CRect(50+nLen*tm.tmAveCharWidth,3*tm.tmHeight,370+nLen*tm.tmAveCharWidth,4*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);

			//Markv�rde kr
			dc->SelectObject(&m_fnt1);			
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			dc->TextOut(10, 6*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53044))));
			nLen = m_xml.str(IDS_STRING53044).GetLength();
			dc->GetTextMetrics(&tm);
			dc->SelectObject(&m_fnt2);
			dc->DrawText(formatData(_T("(%.1f x %.3f) = %.0f kr"),pEval->getEValLandValue_ha(),pEval->getEValAreal(),pEval->getEValLandValue())					,CRect(50+nLen*tm.tmAveCharWidth,6*tm.tmHeight,240+nLen*tm.tmAveCharWidth,7*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
		
		}
		else
		{


			fPreCutInfo_ReducedBy=pEval->getMarkInfo_ReducedBy();

			// m_fLandValue = (m_fSharePine * m_fLandValueFromTableAPine * m_nP30Pine/10.0) + 
			// (m_fShareSpruce * m_fLandValueFromTableASpruce * m_nP30Spruce/10.0);
			fAndelPine = pEval->getMarkInfo_Andel_Pine();
			fAndelSpruce=pEval->getMarkInfo_Andel_Spruce();
			fValuePine=pEval->getMarkInfo_ValuePine();
			fValueSpruce=pEval->getMarkInfo_ValueSpruce();
			fP30Pine=pEval->getMarkInfo_P30_Pine();
			fP30Spruce=pEval->getMarkInfo_P30_Spruce();
			fErs_Tot=fAndelPine*fValuePine*fP30Pine/10.0 + fAndelSpruce*fValueSpruce*fP30Spruce/10.0;
			fAreal=pEval->getEValAreal();
			//fErs_Tot=pEval->getEValLandValue();

			nLen=0;
			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			dc->TextOut(10, tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53040))));

			if(fPreCutInfo_ReducedBy>0.0)
			{
				dc->SelectObject(&m_fnt1_strikeout);
				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
			}

			dc->TextOut(10, 3*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53041))));
			nLen = m_xml.str(IDS_STRING53041).GetLength();


			if(fPreCutInfo_ReducedBy>0.0)
			{
				dc->GetTextMetrics(&tm);
				dc->SelectObject(&m_fnt2_strikeout);			
			}
			else
			{
				dc->GetTextMetrics(&tm);
				dc->SelectObject(&m_fnt2);
			}

			// m_fLandValue = (m_fSharePine * m_fLandValueFromTableAPine * m_nP30Pine/10.0) + 
			// (m_fShareSpruce * m_fLandValueFromTableASpruce * m_nP30Spruce/10.0);
			dc->DrawText(formatData(_T("(%.2f x %.0f x %.1f) + (%.2f x %.0f x %.1f) = %.1f kr/ha"),fAndelPine,fValuePine,fP30Pine/10.0,fAndelSpruce,fValueSpruce,fP30Spruce/10.0,fErs_Tot)			,CRect(50+nLen*tm.tmAveCharWidth,3*tm.tmHeight,370+nLen*tm.tmAveCharWidth,4*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);

			dc->SelectObject(&m_fnt1);			
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			if(fPreCutInfo_ReducedBy>0.0)
			{

				fErs_Tot_Reduced=fErs_Tot*fPreCutInfo_ReducedBy;
				if(fPreCutInfo_ReducedBy>=0.3)	//Tillf�lligt 30%
				{						
					dc->TextOut(10, 4*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING42052))));
					dc->TextOut(10, 5*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53042))));
					nLen = m_xml.str(IDS_STRING53042).GetLength();
				}
				else	// �verlappande 25%
				{
					dc->TextOut(10, 4*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING42051))));
					dc->TextOut(10, 5*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53043))));
					nLen = m_xml.str(IDS_STRING53043).GetLength();
				}

				dc->GetTextMetrics(&tm);
				dc->SelectObject(&m_fnt2);

				dc->DrawText(formatData(_T("(%.1f x %.2f) = %.1f kr/ha"),fErs_Tot,fPreCutInfo_ReducedBy,fErs_Tot_Reduced)			,CRect(50+nLen*tm.tmAveCharWidth,5*tm.tmHeight,255+nLen*tm.tmAveCharWidth,6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				fErs_Tot=fErs_Tot_Reduced;
			}
			//Markv�rde kr
			dc->SelectObject(&m_fnt1);			
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			dc->TextOut(10, 6*tm.tmHeight,formatData(_T("%s"),(m_xml.str(IDS_STRING53044))));
			nLen = m_xml.str(IDS_STRING53044).GetLength();
			dc->GetTextMetrics(&tm);
			dc->SelectObject(&m_fnt2);
			dc->DrawText(formatData(_T("(%.1f x %.3f) = %.0f kr"),fErs_Tot,fAreal,fErs_Tot*fAreal)			,CRect(50+nLen*tm.tmAveCharWidth,6*tm.tmHeight,240+nLen*tm.tmAveCharWidth,7*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
		}
	}
	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	pProp = NULL;
}