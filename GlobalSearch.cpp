// GlobalSearch.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GlobalSearch.h"
#include "ResLangFileReader.h"
#include "ObjectFormView.h"
#include "MDI1950ForrestNormFormView.h"

// GlobalSearch

IMPLEMENT_DYNCREATE(GlobalSearch, CXTResizeFormView)

GlobalSearch::GlobalSearch(): CXTResizeFormView(GlobalSearch::IDD)
{

}

GlobalSearch::~GlobalSearch()
{
}

void GlobalSearch::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

	// Labels
	DDX_Control(pDX, IDC_STATIC_SEARCH, m_wndLblSearch);

	//EditText	
	DDX_Control(pDX, IDC_EDIT1,m_wndEditSearch);
	//DDX_Control(pDX, IDC_REPORT, m_wndReport);
}

BOOL GlobalSearch::OnInitDialog()
{
    CXTResizeFormView::OnInitDialog();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	results = m_pDB->getSearchList();

	setupReport();

	populateReport();

	//GetParentFrame()->ShowWindow(SW_SHOWMAXIMIZED);

	return TRUE;
}

BEGIN_MESSAGE_MAP(GlobalSearch, CFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_EN_CHANGE(IDC_EDIT1, &GlobalSearch::OnEnChangeEdit1)
	ON_NOTIFY(NM_CLICK, ID_SEARCH, OnReportItemClick)
END_MESSAGE_MAP()


// GlobalSearch diagnostics

#ifdef _DEBUG
void GlobalSearch::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void GlobalSearch::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// GlobalSearch message handlers

void GlobalSearch::OnEnChangeEdit1()
{
	populateReport();
}

void GlobalSearch::populateReport() 
{

	m_wndReport.GetRecords()->RemoveAll();

	CString searchText = m_wndEditSearch.getText();
	CString temp;

	std::vector<CString> splitString;
	CXTPReportRecord* pRecord;

	int nStart=0;
	int i=searchText.Find(' ');
	int found;
	while(i>-1) {
		temp = searchText.Mid(nStart,i-nStart);
		temp.MakeLower();
		splitString.push_back(temp);
		nStart=i+1;
		i=searchText.Find(' ',nStart);
	}
	i=searchText.GetLength();
	temp=searchText.Mid(nStart,i-nStart);
	temp.MakeLower();
		
	if(temp.Compare(L"")!=0)
		splitString.push_back(temp);

	for(i=0;i<results.size();i++) {
		temp = results[i].ownerName + L" " + results[i].propertyName + L" " + results[i].objectName;
		temp.MakeLower();
		for(int x=0;x<splitString.size();x++) {
			found = temp.Find(splitString[x]);
			if(found==-1)
				break;
		}
		if(found!=-1) {
			pRecord = new CXTPSearchReportRecord(results[i]);
			m_wndReport.AddRecord(pRecord);
		}
	}
	
	m_wndReport.GetPaintManager()->SetColumnStyle(xtpReportColumnFlat);
	m_wndReport.Populate();
	m_wndReport.RedrawControl();
}


BOOL GlobalSearch::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void GlobalSearch::setupReport() {

	CXTPReportColumn *pCol = NULL;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		
		if (!m_wndReport.Create(this,ID_SEARCH, FALSE, FALSE))
		{
			return;
		}
		
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING54113)), 250));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING5802)), 250,FALSE));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING1166)), 250,FALSE));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				
				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				//m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.GetRecords()->SetCaseSensitive(FALSE);
				
				xml.clean();

				CImageList m_ilIcons;
				VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				m_wndReport.SetImageList(&m_ilIcons);

				
				
			}
		}	// if (fileExists(sLangFN))
	}
}

void GlobalSearch::OnSize(UINT nType, int cx, int cy)
{
  CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	setResize(GetDlgItem(ID_SEARCH),2,35,rect.right - 4,rect.bottom - 35);
	setResize(GetDlgItem(IDC_EDIT1),40,7,rect.right - 40,20);

}

void GlobalSearch::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/) {
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	CXTPReportColumn *pCol = NULL;
	CXTPReportRow *pRow = NULL;
	CRect rect;
	POINT pt;
	CXTPSearchReportRecord * pRec;
	searchResult result;
	CString sLangFN(getLanguageFN(getLanguageDir(),_T("Forrest"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (pItemNotify != NULL)
	{
		pCol = pItemNotify->pColumn;
		pRow = pItemNotify->pRow;

		CMDI1950ForrestNormFormView *pFrame = NULL;
		CObjectFormView *pObjView = NULL;

		if (pCol != NULL && pRow != NULL)
		{
			rect = pCol->GetRect();		
			pt = pItemNotify->pt;
			pRec = (CXTPSearchReportRecord *)pRow->GetRecord();
			result = pRec->getSearchResult();

			if (hitTest_X(pt.x,rect.left,13)){

				switch (pItemNotify->pColumn->GetItemIndex())
				{
					case COLUMN_0 :
					{
						showFormView(113,sLangFN);
						AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
						(LPARAM)&_doc_identifer_msg(_T("Module113"),_T("Module113"),_T(""),3 ,result.ownerId,result.objectId));
						break;
					}
					case COLUMN_1 :
					{
						showFormView(118,sLangFN);
						AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
							(LPARAM)&_doc_identifer_msg(_T("Module118"),_T("Module118"),_T(""),10 ,result.propertyId,result.objectId));
						break;
					}
					case COLUMN_2 :
					{	
						showFormView(5200,m_sLangFN);
					
						if ((pFrame = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
						{
							if ((pObjView = pFrame->getObjectFormView()) != NULL)
							{
								//pObjView->doPopulateDataDBiD(result.objectId);
								pObjView->doPopulateObjectAndProperty(result.objectId,result.propertyId);
								pObjView = NULL;
							}					
						}
						pFrame = NULL;

						break;
					}
				}
			}
		}
	}
}

CXTPSearchReportRecord::CXTPSearchReportRecord(searchResult r) {
	result = r;
	if(r.ownerName!=L"")
		AddItem(new CExIconItemEx(r.ownerName,3));
	else
		AddItem(new CExTextItem(L""));
	if(r.propertyName!=L"")
		AddItem(new CExIconItemEx(r.propertyName,3));
	else
		AddItem(new CExTextItem(L""));
	if(r.objectName!=L"")
		AddItem(new CExIconItemEx(r.objectName,3));
	else
		AddItem(new CExTextItem(L""));
}
