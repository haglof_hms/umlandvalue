// SelectSpcDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectSpcDlg.h"

#include "ResLangFileReader.h"

// CSelectSpcDlg dialog

IMPLEMENT_DYNAMIC(CSelectSpcDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectSpcDlg, CDialog)
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

CSelectSpcDlg::CSelectSpcDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectSpcDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CSelectSpcDlg::~CSelectSpcDlg()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CSelectSpcDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST1, m_wndListCtrl);
	DDX_Control(pDX, IDC_LBL_SPC, m_wndLbl1);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

BOOL CSelectSpcDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CSelectSpcDlg::OnInitDialog()
{
	CString sMsg;
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setupListCtrl();
	setLanguage();

	return TRUE;
}

BOOL CSelectSpcDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CDialog::OnCopyData(pWnd, pData);
}


// PROTECTED
void CSelectSpcDlg::setupListCtrl(void)
{
	int nCnt = 0;
	CString sSpcID;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndListCtrl.InsertColumn(0, (xml->str(IDS_STRING22692)), LVCFMT_CENTER, 40);
			m_wndListCtrl.InsertColumn(1, (xml->str(IDS_STRING22693)), LVCFMT_LEFT, 30);
			m_wndListCtrl.InsertColumn(2, (xml->str(IDS_STRING22694)), LVCFMT_LEFT, 130);
		}
		delete xml;
	}

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_header.SubclassWindow(hWndHeader);

	m_header.SetTheme(new CXTHeaderCtrlThemeOfficeXP());

	m_wndListCtrl.ModifyExtendedStyle(0,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

	if (m_bConnected)
	{
		// Add species in Database to ListControl; 060328 p�d
		m_bConnected = m_pDB->getSpecies(m_vecSpecies);

		if (m_vecSpecies.size() > 0)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				if (!isSpecieUsed(m_vecSpecies[i].getSpcID()))
				{
					sSpcID.Format(_T("%d"),m_vecSpecies[i].getSpcID());
					m_wndListCtrl.InsertItem(nCnt, _T(""), 0);
					m_wndListCtrl.SetItem(nCnt,1, LVIF_TEXT, (sSpcID), 0, NULL, NULL, NULL);
					m_wndListCtrl.SetItem(nCnt,2, LVIF_TEXT, (m_vecSpecies[i].getSpcName()), 0, NULL, NULL, NULL);
					m_wndListCtrl.SetItemData(nCnt,m_vecSpecies[i].getSpcID());
					nCnt++;
				}
			}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
		}	// if (m_vecSpecies.size() > 0)
	}	// if (m_bConnected)

}

void CSelectSpcDlg::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING22690)));
			m_wndLbl1.SetWindowText((xml->str(IDS_STRING22691)));

			m_wndBtnOK.SetWindowText((xml->str(IDS_STRING22695)));
			m_wndBtnCancel.SetWindowText((xml->str(IDS_STRING22696)));
		}
		delete xml;
	}

}

BOOL CSelectSpcDlg::isSpecieUsed(int spc_id)
{
	if (m_vecSpeciesUsed.size() == 0)
		return FALSE;

	for (UINT i = 0;i < m_vecSpeciesUsed.size();i++)
	{
		if (m_vecSpeciesUsed[i] == spc_id)
			return TRUE;
	}

	return FALSE;
}


// PUBLIC
vecTransactionSpecies CSelectSpcDlg::getSpeciesSelected(void)
{
	return m_vecSpeciesSelected;
}

void CSelectSpcDlg::setSpeciesUsed(vecIntegers& spc_used)
{
	m_vecSpeciesUsed.clear();
	if (spc_used.size() > 0)
	{
		for (UINT i = 0;i < spc_used.size();i++)	
		m_vecSpeciesUsed.push_back((int)spc_used[i]);
	}	// if (spc_used.size() > 0)
}

// Return CTransaction_species from m_vecSpecies vector
// depending on specie id, selected; 070502 p�d
CTransaction_species* CSelectSpcDlg::getSpecie(int spc_id)
{
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == spc_id)
				return &m_vecSpecies[i];
		}
	}
	return NULL;
}

// CSelectSpcDlg message handlers

void CSelectSpcDlg::OnBnClickedOk()
{
	// Get selected species and add to m_vecSpeciesSelected; 060328 p�d
	CString S;
	int nSpcID;
	CTransaction_species *pRecSpc;
	m_vecSpeciesSelected.clear();

	if (m_wndListCtrl.GetItemCount() > 0)
	{
		for (int i = 0;i < m_wndListCtrl.GetItemCount();i++)
		{
			nSpcID = m_wndListCtrl.GetItemData(i);
			if (m_wndListCtrl.GetCheck(i))
			{
				pRecSpc = getSpecie(nSpcID);
				if (pRecSpc != NULL)
				{
					m_vecSpeciesSelected.push_back(*pRecSpc);
				}
			}
		}
	}

	OnOK();
}

// CSelectSpcDlg message handlers
