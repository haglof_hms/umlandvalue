#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////////
// CChangeStatusRec

class CChangeStatusRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_elv_properties recProp;
protected:

public:
	CChangeStatusRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CChangeStatusRec(UINT index,LPCTSTR prop,LPCTSTR landowner,LPCTSTR status,CTransaction_elv_properties rec,LPCTSTR PropNr,LPCTSTR PropLan,LPCTSTR PropKommun)	 
	{
		m_nIndex = index;
		recProp = rec;
		AddItem(new CTextItem((prop)));
		AddItem(new CTextItem((landowner)));
		AddItem(new CTextItem((status)));
		AddItem(new CTextItem((PropNr)));
		AddItem(new CTextItem((PropLan)));
		AddItem(new CTextItem((PropKommun)));

		
	}

	BOOL getColumnCheck(int item)	{	return ((CExCheckItem*)GetItem(item))->getChecked();	}

	CString getColumnText(int item) {	return ((CTextItem*)GetItem(item))->getTextItem();	}

	CTransaction_elv_properties& getRecord(void)	{ return recProp; }

	UINT getIndex(void)	{	return m_nIndex;	}
};

// CChangePropStatusDlg dialog

class CChangePropStatusDlg : public CDialog
{
	DECLARE_DYNAMIC(CChangePropStatusDlg)

	// Setup language filename; 080429 p�d
	CString m_sLangFN;

	CString m_sBatchStr;
	CString m_sStatusSet;
	int m_nStatusIndex;

	CMyReportCtrl m_wndReport;

	CXTListCtrl m_wndLCtrl;
	CXTFlatHeaderCtrl m_Header;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CComboBox m_wndCBox1;
	CMyExtEdit m_wndEdit1;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CStringArray m_sarrStatus;
	int m_nObjID;
	CUMLandValueDB *m_pDB;
	void setupReport(void);

	CTransaction_elv_properties m_recElvProps;
	vecTransaction_elv_properties m_vecTransaction_elv_properties;
	vecTransaction_elv_properties m_vecTransaction_elv_properties_return;
	void getPropertiesInObject(void);

	// Added 2009-09-15 p�d
	vecTransaction_property_status m_vecPropStatus;
	void getPropActionStatusFromDB(void);

	void populateData(void);
public:
	CChangePropStatusDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangePropStatusDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG16 };

	void setDBConnection(CUMLandValueDB *db)	{	m_pDB = db;	}
	void setObjectID(int id)		{ m_nObjID = id; }

	void getReturnData(vecTransaction_elv_properties &vec,CString& status,int *index)
	{
		CString sStatus;
		vec = m_vecTransaction_elv_properties_return;
		if (m_sBatchStr.IsEmpty())
			status = m_sStatusSet;
		else
			status.Format(_T("%s , %s"),m_sStatusSet,m_sBatchStr);
		*index = m_nStatusIndex;
	}

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddToListCtrl();
	afx_msg void OnBnClickedAddAllToListCtrl();
	afx_msg void OnBnClickedRemoveFromListCtrl();
	afx_msg void OnBnClickedRemoveAllFromListCtrl();
	afx_msg void OnBnClickedOk();
};
