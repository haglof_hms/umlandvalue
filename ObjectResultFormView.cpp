// ObjectResultFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ObjectResultFormView.h"
#include "1950ForrestNormFrame.h"

#include "XTPPreviewView.h"

IMPLEMENT_DYNCREATE(CObjectReportView, CXTPReportView)

BEGIN_MESSAGE_MAP(CObjectReportView, CXTPReportView)
	ON_COMMAND(ID_FILE_PRINT, &CObjectReportView::OnFilePrint)
END_MESSAGE_MAP()

CObjectReportView::CObjectReportView()
{
}

CObjectReportView::~CObjectReportView()
{
}

void CObjectReportView::doPrintReport(void)
{
	::SendMessage(this->GetSafeHwnd(),WM_COMMAND,ID_FILE_PRINT,0);
}


///////////////////////////////////////////////////////////////////////
// CObjectResultFormView

IMPLEMENT_DYNCREATE(CObjectResultFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CObjectResultFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CObjectResultFormView::CObjectResultFormView()
	: CXTResizeFormView(CObjectResultFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pReportView = NULL;
	m_pDB = NULL;

	// Default settings of reports in Summarized data tab; 080527 p�d
	m_nActiveReport = TAB_RESULT_SLEN_REPORT;
}

CObjectResultFormView::~CObjectResultFormView()
{
}

void CObjectResultFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;

	m_xml.clean();

	if (m_pReportView != NULL)
		m_pReportView->DestroyWindow();

	CXTResizeFormView::OnDestroy();
}

void CObjectResultFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}

int CObjectResultFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pReportView = (CObjectReportView*)(RUNTIME_CLASS(CObjectReportView)->CreateObject());
	return 0;
}

BOOL CObjectResultFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMLandValueDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


void CObjectResultFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

//		m_wndLB.SetLblFont(20,FW_BOLD);

		CCreateContext context;
		context.m_pCurrentDoc = NULL;

		if (m_pReportView != NULL)
		{
			m_pReportView->Create(NULL,NULL,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),this,ID_LANDOWNERS_REP,&context);

			m_pReportView->GetReportCtrl().AddColumn(new CXTPReportColumn(0,_T(""),100));
			m_pReportView->GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
			m_pReportView->GetReportCtrl().SetMultipleSelection( FALSE );
			m_pReportView->GetReportCtrl().SetGridStyle( TRUE, xtpReportGridNoLines );
			m_pReportView->GetReportCtrl().SetGridStyle( FALSE, xtpReportGridNoLines );
			m_pReportView->GetReportCtrl().AllowEdit(FALSE);
			m_pReportView->GetReportCtrl().ShowHeader( FALSE );
			m_pReportView->GetReportCtrl().FocusSubItems(FALSE);
			m_pReportView->GetReportCtrl().ModifyStyle(WS_TABSTOP,0);
			m_pReportView->GetReportCtrl().GetPaintManager()->m_bHideSelection = TRUE;
			m_pReportView->GetReportCtrl().GetPaintManager()->m_clrHighlightText = RGB(0,0,0);

			m_pReportView->GetReportCtrl().GetPaintManager()->m_clrHighlight = WHITE;
			m_pReportView->GetReportCtrl().GetPaintManager()->m_clrControlBack = WHITE;
			m_pReportView->GetReportCtrl().GetPaintManager()->m_clrHeaderControl = WHITE;
			m_pReportView->GetReportCtrl().GetPaintManager()->m_clrControlLightLight = WHITE;
			m_pReportView->GetReportCtrl().GetPaintManager()->m_clrControlDark = WHITE;

			m_pReportView->GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
			m_pReportView->GetPrintOptions()->m_rcMargins = CRect(2,2,2,2);
		}	// if (m_pReportView != NULL)

		m_bInitialized = TRUE;
	}
}

BOOL CObjectResultFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CObjectResultFormView::OnSize(UINT nType,int cx,int cy)
{

	if (m_pReportView->GetSafeHwnd())
	{
		setResize(m_pReportView,2,2,cx-4,cy-4);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

// CObjectResultFormView diagnostics

#ifdef _DEBUG
void CObjectResultFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CObjectResultFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CObjectResultFormView message handlers

// PUBLIC

// PROTECTED

void CObjectResultFormView::setupSQLTraktIDs(LPCTSTR lnum)
{
	vecInt vecTraktID;
	CString sSQLStr;
	CString sSQLStr_assorts;
	CTransaction_elv_object *pObj = getActiveObject();
	m_sSQLTraktIDs.Empty();
	m_sSQLTraktIDs_assorts.Empty();
	m_sSelectedLandOwnerNumber = lnum;

	//-----------------------------------------------------------------------------------------
	// "H�mta in alla Trakter (ID), som h�r ihop med"
	// "aktuellt Objekt och Mark�garnummer"
	// "L�gg in informationen i m_sSQLTraktIDs"; 080526 p�d
	m_pDB->getTraktsPerLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecTraktID);
	if (vecTraktID.size() == 1)
	{
		m_sSQLTraktIDs.Format(_T("(tdcls_trakt_id=%d)"),vecTraktID[0]);
		m_sSQLTraktIDs_assorts.Format(_T("(tass_trakt_id=%d)"),vecTraktID[0]);
	}
	else if (vecTraktID.size() == 2)
	{
		m_sSQLTraktIDs.Format(_T("(tdcls_trakt_id=%d or tdcls_trakt_id=%d)"),vecTraktID[0],vecTraktID[1]);
		m_sSQLTraktIDs_assorts.Format(_T("(tass_trakt_id=%d or tass_trakt_id=%d)"),vecTraktID[0],vecTraktID[1]);
	}
	else if (vecTraktID.size() > 2)
	{
		sSQLStr.Empty();
		sSQLStr_assorts.Empty();
		for (UINT i = 0;i < vecTraktID.size();i++)
		{
			if (i == 0)
			{
				sSQLStr += formatData(_T("(tdcls_trakt_id=%d or"),vecTraktID[i]);
				sSQLStr_assorts += formatData(_T("(tass_trakt_id=%d or"),vecTraktID[i]);
			}
			else if (i > 0 && i < vecTraktID.size()-1)
			{
				sSQLStr += formatData(_T(" tdcls_trakt_id=%d or "),vecTraktID[i]);
				sSQLStr_assorts += formatData(_T(" tass_trakt_id=%d or "),vecTraktID[i]);
			}
			else if (i == vecTraktID.size()-1)
			{
				sSQLStr += formatData(_T("tdcls_trakt_id=%d)"),vecTraktID[i]);
				sSQLStr_assorts += formatData(_T("tass_trakt_id=%d)"),vecTraktID[i]);
			}
		}
		m_sSQLTraktIDs = sSQLStr;
		m_sSQLTraktIDs_assorts = sSQLStr_assorts;
	}
	//-----------------------------------------------------------------------------------------
	setupSLenReport();
}

void CObjectResultFormView::printReport(void)
{
	if (m_pReportView->GetSafeHwnd())
		m_pReportView->doPrintReport();
}

// Report "St�mplingsl�ngd"; 080526 p�d
void CObjectResultFormView::setupSLenReport()
{
	vecString vecProperties;
	vecInt vecDCLS;
	std::map<int,int> mapColums;
	vecTransaction_elv_slen_data vecSLenData;
	CTransaction_elv_slen_data recSLenData;
	vecTransaction_elv_slen_sum_data vecSLenSumData;
	CTransaction_elv_slen_sum_data recSLenSumData;
	int nSpcWidth = 0;
	TCHAR szBuffer[255];
	CELVObjectSumRec *pRec = NULL;
	CString sRowStr;
	CString sStr;
	double fSumM3Sk = 0.0;
	long nSumNumOf = 0;
	double fSumM3Sk_randtrees = 0.0;
	long nSumNumOf_randtrees = 0;
	CTransaction_elv_object *pObj = getActiveObject();

	// Start collecting data for "St�mplingsl�ngd"; 080526 p�d

	if (m_pDB != NULL && pObj != NULL)
	{

		m_nActiveReport = TAB_RESULT_SLEN_REPORT;

		//-----------------------------------------------------------------------------------------
		// "H�mta in st�mplingsl�ngd, f�r samtliga Trakter, f�r Objekt och Mark�garnummer"; 080526 p�d
		m_pDB->getSLenForTraktsPerLandOwnerNumber(m_sSQLTraktIDs,vecSLenData);	
		// "H�mta in summering av Object trakter f�r mark�garkonstellationen"; 080527 p�d
		m_pDB->getSLenSummmarizedData(m_sSQLTraktIDs,vecSLenSumData);
		// "H�mta in diameterklasser oberoende av tr�dslag"; 080527 p�d
		m_pDB->getSLenDiameterclasses(m_sSQLTraktIDs,vecDCLS);
		// "H�mta in fastighet(er) f�r Mark�garnummer och Objekt"; 080528 p�d
		m_pDB->getPropertiesByLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecProperties);
		//-----------------------------------------------------------------------------------------
		if (fileExists(m_sLangFN))
		{
			if (m_xml.Load(m_sLangFN))
			{
				//*****************************************************************************************
				//								START: Add header information
				//*****************************************************************************************
				m_pReportView->GetReportCtrl().ResetContent();
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((getDBDateTime()))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL); pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54100)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_HEADER);
				pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%s : %s"),
																																				m_xml.str(IDS_STRING5403),
																																				m_sSelectedLandOwnerNumber)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);
				pRec = NULL;

				// Empty row
				// Add properties for this Landownernumber and Object; 080528 p�d
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING5405)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
				if (vecProperties.size() > 0)
				{
					for (UINT i = 0;i < vecProperties.size();i++)
					{
						m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((vecProperties[i]))));
						if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);
						pRec = NULL;
					}	// for (UINT i = 0;i < vecProperties.size();i++)
				}	// if (vecProperties.size() > 0)

				//*****************************************************************************************
				//								END: Add header information
				//*****************************************************************************************
				// Empty row
				m_pReportView->GetReportCtrl().AddRecord(new CELVObjectSumRec(_T("")));
				//*****************************************************************************************
				//								START: Adding diameterclasses per specie; 080528 p�d
				//*****************************************************************************************
				// Setup dclsColumns i.e. adding number of colums per diameterclass, OBS same number of
				// columns equals number of species; 080527 p�d
				if (vecDCLS.size() > 0)
				{
					vecDclsColumns.clear();
					for (UINT i1 = 0;i1 < vecDCLS.size();i1++)
					{
						recDclsColumns._dcls = vecDCLS[i1];
						recDclsColumns._vec_str.clear();
						// Add number of columns (species); 080527 p�d
						// OBS! Columns equals species; 080528 p�d
						for (UINT i2 = 0;i2 < vecSLenSumData.size();i2++)
						{
							// Add a default value to each column (i.e. specie); 080527 p�d
							recDclsColumns._vec_str.push_back(_T(""));
						}	// for (UINT i2 = 0;i2 < vecSLenSumData.size();i2++)
						vecDclsColumns.push_back(recDclsColumns);
					}	// for (UINT i = 0;i < vecColumnsPerDCLS.size();i++)

				}	// if (vecColumnsPerDCLS.size() > 0)
				//-----------------------------------------------------------------------------------------
				// We need to format strings for each row; 080527 p�d
				// Calculate where column-items should be placed, based on
				// size of Speciename and placement in list vecSLenSumData; 080527 p�d
				mapColums.clear();
				_vec_spc.clear();
				for (UINT i = 0;i < vecSLenSumData.size();i++)
				{
					recSLenSumData = vecSLenSumData[i];
					nSpcWidth = recSLenSumData.getSpcName().GetLength();
					// We need to check that length of speciename >= 6 chanracters
					// if not we'll add up to min width; 080528 p�d
					if (nSpcWidth < 6) nSpcWidth = 6;
					mapColums[i] = nSpcWidth+3;
					_vec_spc.push_back(recSLenSumData.getSpcID());
				}
				//----------------------------------------------------------------------------------
				//	Add headline for diameterclasses (species etc.); 080527 p�d
				// Format string for species; 080527 p�d
				sRowStr.Empty();
				_stprintf(szBuffer,_T("%-6s"),(m_xml.str(IDS_STRING5404)));
				sRowStr = szBuffer;
				for (UINT i = 0;i < vecSLenSumData.size();i++)
				{
					// Only add specie, if there's volume; 080527 p�d
					//if (vecSLenSumData[i].getM3sk() > 0.0)
						_stprintf(szBuffer,_T("%*s"),mapColums[i],vecSLenSumData[i].getSpcName());
						sRowStr += szBuffer;
				}
				if (!sRowStr.IsEmpty())
				{
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
					pRec->setColumnFont(FNT_SMALL_UNDERLINE);
					pRec = NULL;
				}
				//----------------------------------------------------------------------------------
				if (vecDclsColumns.size() > 0)
				{
						// Row in this case equals Diameterclass in the order set by vecSLenSumData; 080528 p�d
						for (UINT row = 0;row < vecDclsColumns.size();row++)
						{
							// Column in this case equals Specie in the order set by vecSLenSumData; 080528 p�d
							// OBS! _vec_spc size must be ecual to number of columns (i.e.) species,
							// and should be, becuse we setup from same vector vecSLenSumData; 080528 p�d
							for (UINT col = 0;col < vecDclsColumns[row]._vec_str.size();col++)
							{
								// vecSLenData holds information, among others, on diamterclass per specie
								// and number of trees per dcls and specie; 080528 p�d
								for (UINT row1 = 0;row1 < vecSLenData.size();row1++)
								{
									recSLenData = vecSLenData[row1];
									if (recSLenData.getDclsFrom() == vecDclsColumns[row]._dcls && 
											recSLenData.getSpcID() == _vec_spc[col])
									{
										// OBS! Number of trees in each diameterclass includes "Kanttr�d"; 080528 p�d
										vecDclsColumns[row]._vec_str[col] = formatData(_T("%d/%d"),recSLenData.getNumOf(),recSLenData.getNumOfRandTrees());
									}	// if (recSLenData.getDclsFrom() == vecDCLS[row2] && recSLenData.getSpcID() == recSLenSumData.getSpcID())
								}	// for (UINT row1 = 0;row1 < vecSLenData.size();row1++)						
						
						}	// for (UINT col = 0;col < vecDclsColumns[row]._vec.size();col++)
					}	// for (UINT row = 0;row < vecDclsColumns.size();row++)
					//----------------------------------------------------------------------------------
					// Setup and add rows (number of trees per diematerclass and specie); 080528 p�d
					for (UINT row = 0;row < vecDclsColumns.size();row++)
					{
						// Add first column equals diameterclass; 080527 p�d
						sRowStr = formatData(_T("%3.0f%3s"),vecDclsColumns[row]._dcls,_T(":"));
						for (UINT col = 0;col < vecDclsColumns[row]._vec_str.size();col++)
						{
							_stprintf(szBuffer,_T("%*s"),mapColums[col],vecDclsColumns[row]._vec_str[col]);
							sRowStr += szBuffer;						

						} // for (UINT i2 = 0;i2 < vecDclsColumns[i1]._vec.size();i2++)
						m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
						pRec->setColumnFont(FNT_SMALL);
						pRec = NULL;
					}	// for (UINT i1 = 0;i1 < vecDclsColumns.size();i1++)
				}	// if (vecDclsColumns.size() > 0)
				sRowStr = formatData(_T("%6s"),_T(""));
				for (UINT col = 0;col < vecSLenSumData.size();col++)
				{
					_stprintf(szBuffer,_T("%*s"),mapColums[col],_T(""));
					sRowStr += szBuffer;
				}	// for (UINT i = 0;i < vecSLenSumData.size();i++)
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				pRec->setColumnFont(FNT_SMALL_UNDERLINE);
				pRec = NULL;

				sRowStr = formatData(_T("%5s:"),(m_xml.str(IDS_STRING54102)));
				for (UINT col = 0;col < vecSLenSumData.size();col++)
				{
					recSLenSumData = vecSLenSumData[col];
					sStr.Format(_T("%d/%d"),recSLenSumData.getNumOf(),recSLenSumData.getNumOfRandTrees());
					_stprintf(szBuffer,_T("%*s"),mapColums[col],sStr);
					sRowStr += szBuffer;
				}	// for (UINT i = 0;i < vecSLenSumData.size();i++)
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				pRec->setColumnFont(FNT_SMALL);
				pRec = NULL;
				//----------------------------------------------------------------------------------
				// Setup and add sum. of number of trees/specie; 080528 p�d
				//*****************************************************************************************
				//								END: Adding diameterclasses per specie; 080528 p�d
				//*****************************************************************************************
				// Empty row
				m_pReportView->GetReportCtrl().AddRecord(new CELVObjectSumRec(_T("")));
				//*****************************************************************************************
				//								START: Adding sumarized data; 080528 p�d
				//*****************************************************************************************
				// Headeline for summerized data; 080528 p�d
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54103)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);
				pRec = NULL;
				sRowStr = formatData(_T("%25s%21s"),(m_xml.str(IDS_STRING54104)),(m_xml.str(IDS_STRING54105)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);
				pRec = NULL;
				sRowStr = formatData(_T("%20s%10s%10s%10s"),
					(m_xml.str(IDS_STRING54106)),
					(m_xml.str(IDS_STRING54107)),
					(m_xml.str(IDS_STRING54106)),
					(m_xml.str(IDS_STRING54107)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
				pRec = NULL;
				// Add sum. data to report. Also sum total; 080528 p�d
				fSumM3Sk = 0.0;
				nSumNumOf = 0;
				fSumM3Sk_randtrees = 0.0;
				nSumNumOf_randtrees = 0;
				for (UINT col = 0;col < vecSLenSumData.size();col++)
				{
					recSLenSumData = vecSLenSumData[col];
					fSumM3Sk += recSLenSumData.getM3sk();
					nSumNumOf += recSLenSumData.getNumOf();
					fSumM3Sk_randtrees += recSLenSumData.getM3SkRandTrees();
					nSumNumOf_randtrees += recSLenSumData.getNumOfRandTrees();
					sStr.Format(_T("%-15s%5d%10.2f%10d%10.2f"),
								recSLenSumData.getSpcName(),
								recSLenSumData.getNumOf(),
								recSLenSumData.getM3sk(),
								recSLenSumData.getNumOfRandTrees(),
								recSLenSumData.getM3SkRandTrees());
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sStr))));
					pRec->setColumnFont(FNT_SMALL);
					pRec = NULL;
				}	// for (UINT i = 0;i < vecSLenSumData.size();i++)
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%50s"),_T(""))))));
				pRec->setColumnFont(FNT_SMALL_UNDERLINE);
				pRec = NULL;
				sStr.Format(_T("%12s  :%5d%10.2f%10d%10.2f"),
								(m_xml.str(IDS_STRING54102)),
								nSumNumOf,
								fSumM3Sk,
								nSumNumOf_randtrees,
								fSumM3Sk_randtrees);
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sStr))));
					pRec->setColumnFont(FNT_SMALL);
					pRec = NULL;

				//*****************************************************************************************
				//								END: Adding sumarized data; 080528 p�d
				//*****************************************************************************************
			}
		}
		m_xml.clean();
	}	// if (m_pDB != NULL)

	m_pReportView->GetReportCtrl().Populate();
	m_pReportView->GetReportCtrl().UpdateWindow();

	_vec_spc.clear();
	vecDCLS.clear();
	mapColums.clear();
	vecSLenData.clear();
	vecSLenSumData.clear();
	for (UINT row = 0;row < vecDclsColumns.size();row++)
		vecDclsColumns[row]._vec_str.clear();
	vecDclsColumns.clear();
	vecProperties.clear();
	pObj = NULL;

}

void CObjectResultFormView::setupEvaluationReport()
{
	double fSumLandValue = 0.0;
	double fSumEarlyCutting = 0.0;
	double fStormDryVolume = 0.0;
	double fStormDryValue = 0.0;
	double fHighCostVolume = 0.0;
	double fHighCostValue = 0.0;
	double fSumCompensation = 0.0;
	CString sRowStr;
	CString sSpecieName;
	CString sOtherCompNote;
	CString sTGL;
	CString sWidthCap;
	CString sWidths;
	CString sEValueName;
	vecString vecProperties;
	CTransaction_eval_evaluation recEvaluation;
	vecTransaction_eval_evaluation vecEvaluations;
	vecTransaction_elv_cruise_randtrees vecCruiseRandTrees;
	CTransaction_elv_cruise_randtrees recCruiseRandTrees;
	vecTransaction_elv_properties_other_comp vecOtherComp;
	CTransaction_elv_properties_other_comp recOtherComp;
	vecTransactionSpecies vecSpecies;
	CTransaction_species recSpecie;
	CELVObjectSumRec *pRec = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	vecUCFunctions funcs;
	std::map<short,double> mapSpcVolume;
	std::map<short,double> mapSpcValue;
	int nTypeOfInfringIndex = -1;

	// Start collecting data for "St�mplingsl�ngd"; 080526 p�d

	if (m_pDB != NULL && pObj != NULL)
	{
		m_nActiveReport = TAB_RESULT_EVAL_REPORT;
		//-----------------------------------------------------------------------------------------
		// "L�s in typ av intr�ng, fr�n UMLandValueNorm"; 080922 p�d
		getForrestNormTypeOfInfring(funcs);
		//-----------------------------------------------------------------------------------------
		// "H�mta in v�rderingsbest�nd f�r Objekt och Mark�garkonstellation"; 080528 p�d
		m_pDB->getEvaluationsPerLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecEvaluations);
		// "H�mta in storm- och torkskador"; 080528 p�d
		m_pDB->getStormDryForLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,&fStormDryVolume,&fStormDryValue);
		// "H�mta in f�rdyrad avverkning"; 080528 p�d
		m_pDB->getHighCostForLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,&fHighCostVolume,&fHighCostValue);
		// "H�mta in storm- och torkskador"; 080528 p�d
		m_pDB->getRandTreesForLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecCruiseRandTrees);
		// "H�mta in annan ers�ttning"; 080528 p�d
		m_pDB->getOtherCompForLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecOtherComp);
		// "H�mta in fastighet(er) f�r Mark�garnummer och Objekt"; 080528 p�d
		m_pDB->getPropertiesByLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecProperties);
		// "H�mta in Tr�dslag fr�n fst_species_table"; 080528 p�d
		//m_pDB->getSpecies(vecSpecies);
		//-------------------------------------------------------------------------------------
		// "H�mta tr�dslag fr�n Prislistan i Objektet"; 081028 p�d	
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(pObj->getObjPricelistXML()))
			{
				//---------------------------------------------------------------------------------
				// Get species in pricelist; 100104 p�d
				pPrlParser->getSpeciesInPricelistFile(vecSpecies);
			}
			delete pPrlParser;
		}
/*
		PricelistParser *pPrlParser = new PricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->LoadFromBuffer(pObj->getObjPricelistXML()))
			{
				//---------------------------------------------------------------------------------
				// Get species in pricelist; 081028 p�d
				pPrlParser->getSpeciesInPricelistFile(vecSpecies);
			}
			delete pPrlParser;
		}
*/
		if (fileExists(m_sLangFN))
		{
			if (m_xml.Load(m_sLangFN))
			{
				//*****************************************************************************************
				//								START: Add header information
				//*****************************************************************************************
				m_pReportView->GetReportCtrl().ResetContent();
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((getDBDateTime()))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL); pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54110)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_HEADER);pRec = NULL;

				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%s : %s"),
																																				m_xml.str(IDS_STRING5403),
																																				m_sSelectedLandOwnerNumber)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);	pRec = NULL;
				

				nTypeOfInfringIndex = _tstoi(pObj->getObjTypeOfInfring());
				if (nTypeOfInfringIndex > -1 && nTypeOfInfringIndex < funcs.size())
				{
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%s : %s"),
																																					m_xml.str(IDS_STRING214),
																																					funcs[nTypeOfInfringIndex].getName())))));
				}
				else
				{
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%s : <%s>"),
																																					m_xml.str(IDS_STRING214),
																																					m_xml.str(IDS_STRING5504))))));
																																					
				}

				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);	pRec = NULL;
				// Add properties for this Landownernumber and Object; 080528 p�d
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING5405)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
				if (vecProperties.size() > 0)
				{
					for (UINT i = 0;i < vecProperties.size();i++)
					{
						m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((vecProperties[i]))));
						if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);
						pRec = NULL;
					}	// for (UINT i = 0;i < vecProperties.size();i++)
				}	// if (vecProperties.size() > 0)
				m_pReportView->GetReportCtrl().AddRecord(pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54133))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord(pRec = new CELVObjectSumRec((pObj->getObjUseNorm())));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);pRec = NULL;
				// If "Breddning", add information on widths; 080924 p�d
				if (_tstoi(pObj->getObjTypeOfInfring()) == 1)
				{
					sWidthCap.Format(_T("%-18s%-12s%-11s"),
						(m_xml.str(IDS_STRING208)),
						(m_xml.str(IDS_STRING2080)),
						(m_xml.str(IDS_STRING2081)));
					m_pReportView->GetReportCtrl().AddRecord(pRec = new CELVObjectSumRec(sWidthCap));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);pRec = NULL;
					sWidths.Format(_T("   %-18.0f%-12.0f%-11.0f"),
						pObj->getObjPresentWidth1(),
						pObj->getObjAddedWidth1(),
						pObj->getObjAddedWidth2());

					m_pReportView->GetReportCtrl().AddRecord(pRec = new CELVObjectSumRec(sWidths));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);pRec = NULL;
				}

				//*****************************************************************************************
				//								END: Add header information
				//*****************************************************************************************
				//*****************************************************************************************
				//								START: Add evaluation(s) for this landownerconstellation; 080528 p�d
				//*****************************************************************************************
				if (vecEvaluations.size() > 0)
				{
					// First, we'll check if there's a "V�rdering startar"; 080528 p�d
					for (UINT i1 = 0;i1 < vecEvaluations.size();i1++)
					{
						recEvaluation = vecEvaluations[i1];
						if (recEvaluation.getEValType() == EVAL_TYPE_ANNAN)	// "V�rdering startar"; 080528 p�d
						{
							m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54111)))));
							if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
							pRec = NULL;
							m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((recEvaluation.getEValName()))));
							if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);
							pRec = NULL;
						}										
						if (recEvaluation.getEValType() == EVAL_TYPE_4)	// "V�rdering slutar"; 080528 p�d
						{
							m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54112)))));
							if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
							pRec = NULL;
							m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((recEvaluation.getEValName()))));
							if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);
							pRec = NULL;
						}										
					}	// for (UINT i1 = 0;i1 < vecEvaluations.size();i1++)
					// Empty row
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec()));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);
					pRec = NULL;

					// Setup headline for evaluation data; 080528 p�d
					// Order of columns: "Namn,Areal,�lder,TGL,H100(dm),Korr.fak.,Markv�rde,F�rt.avv."; 080529 p�d
					sTGL.Format(_T("%s  "),(m_xml.str(IDS_STRING54132)));
					sRowStr = formatData(_T("%-20s%10s%6s%12s%8s%10s%12s%12s"),
									(m_xml.str(IDS_STRING54113)),
									(m_xml.str(IDS_STRING54114)),
									(m_xml.str(IDS_STRING54115)),
									sTGL,
									(m_xml.str(IDS_STRING54116)),
									(m_xml.str(IDS_STRING54117)),
									(m_xml.str(IDS_STRING54118)),
									(m_xml.str(IDS_STRING54120)));
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
					pRec = NULL;

					// Add evaluations for this Landownernumber and Object; 080528 p�d
					fSumLandValue = 0.0;
					fSumEarlyCutting = 0.0;
					for (UINT i1 = 0;i1 < vecEvaluations.size();i1++)
					{
						recEvaluation = vecEvaluations[i1];
						if (recEvaluation.getEValType() == EVAL_TYPE_VARDERING || recEvaluation.getEValType() == EVAL_TYPE_FROM_TAXERING)	// "V�rderings- eller Taxeringsbest�nd"; 080528 p�d
						{
							fSumLandValue += recEvaluation.getEValLandValue();
							fSumEarlyCutting += recEvaluation.getEValPreCut();
							sTGL.Format(_T("%2.0f;%2.0f;%2.0f"),
										recEvaluation.getEValPinePart(),
										recEvaluation.getEValSprucePart(),
										recEvaluation.getEValBirchPart());
							sEValueName = recEvaluation.getEValName();
							if (sEValueName.GetLength() > 16)
							{
								sEValueName.Delete(16,sEValueName.GetLength());
								sEValueName += _T("...");
							}
/*
							sRowStr = formatData(_T("%-19s%10.3f%6d%11s%10s%10.2f%12.2f%12.2f"),
								sEValueName,
								recEvaluation.getEValAreal(),
								recEvaluation.getEValAge(),
								sTGL,
								recEvaluation.getEValSIH100(),
								recEvaluation.getEValCorrFactor(),
								recEvaluation.getEValLandValue(),
								recEvaluation.getEValPreCut());
*/
							sRowStr = formatData(_T("%-19s%10.3f%6d%11s%10s%10.2f%12s%12s"),
								sEValueName,
								recEvaluation.getEValAreal(),
								recEvaluation.getEValAge(),
								sTGL,
								recEvaluation.getEValSIH100(),
								recEvaluation.getEValCorrFactor(),
								formatNumber(recEvaluation.getEValLandValue(),0),
								formatNumber(recEvaluation.getEValPreCut(),0));

							m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
							if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);
							pRec = NULL;
						}

					}	// for (UINT i1 = 0;i1 < vecEvaluations.size();i1++)
				}	// if (vecEvaluations.size() > 0)
				//*****************************************************************************************
				//								END: Add evaluation(s) for this landownerconstellation; 080528 p�d
				//*****************************************************************************************
				// Empty row
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec()));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);
				pRec = NULL;
				//*****************************************************************************************
				//								START: Add summarized data for Evaluation; 080528 p�d
				//*****************************************************************************************
				fSumCompensation = 0;	// "Start to set zero"; 080528 p�d
				// Headline for Summarized data; 080528 p�d
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec(((m_xml.str(IDS_STRING54122))))));
				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);	pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec()));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);	pRec = NULL;
				sRowStr = formatData(_T("%30s%15s%25s"),_T(""),(m_xml.str(IDS_STRING54130)),(m_xml.str(IDS_STRING54131)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);pRec = NULL;

				// "Markv�rde"
//				sRowStr = formatData(_T("%-30s%40.2f"),(m_xml.str(IDS_STRING54123)),fSumLandValue);
				sRowStr = formatData(_T("%-30s%40s"),(m_xml.str(IDS_STRING54123)),formatNumber(fSumLandValue,0));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				fSumCompensation += fSumLandValue;
				// "F�rtida avverkning"
//				sRowStr = formatData(_T("%-30s%40.2f"),(m_xml.str(IDS_STRING54124)),fSumEarlyCutting);
				sRowStr = formatData(_T("%-30s%40s"),(m_xml.str(IDS_STRING54124)),formatNumber(fSumEarlyCutting,0));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				fSumCompensation += fSumEarlyCutting;
				// "Storm- och torkskador"
//				sRowStr = formatData(_T("%-30s%15.2f%25.2f"),(m_xml.str(IDS_STRING54125)),fStormDryVolume,fStormDryValue);
				sRowStr = formatData(_T("%-30s%15.2f%25s"),(m_xml.str(IDS_STRING54125)),fStormDryVolume,formatNumber(fStormDryValue,0));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				fSumCompensation += fStormDryValue;
				// "Kanttr�d"
				if (vecCruiseRandTrees.size() > 0)
				{
					sRowStr = formatData(_T("%-30s"),(m_xml.str(IDS_STRING54126)));
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
								
					for (UINT i = 0;i < vecCruiseRandTrees.size();i++)
					{
						recCruiseRandTrees = vecCruiseRandTrees[i];
						// Find name of specie from vecSpecie; 080528 p�d
						sSpecieName.Empty();
						// Calculate on aggregate/specie; 080916 p�d
						if (vecSpecies.size() > 0)
						{
							for (UINT ii = 0;ii < vecSpecies.size();ii++)
							{
								recSpecie = vecSpecies[ii];
								if (recSpecie.getSpcID() == recCruiseRandTrees.getEValRandSpcID())
								{
									sSpecieName = _T("- ") + recSpecie.getSpcName();
									mapSpcVolume[recSpecie.getSpcID()] += recCruiseRandTrees.getEValRandVolume();
									mapSpcValue[recSpecie.getSpcID()] += recCruiseRandTrees.getEValRandValue();
									//break;
								}	// if (recSpecie.getSpcID() == recCruiseRandTrees.getEValRandSpcID())
							}	// for (UINT ii = 0;ii < vecSpecies.size();ii++)
						}	// if (vecSpcies.size() > 0)
						fSumCompensation += recCruiseRandTrees.getEValRandValue();
					}	// for (UINT i = 0;i < vecCruiseRandTrees.size();i++)				
				}	// if (vecCruiseRandTrees.size() > 0)
				// Display data in report; 080916 p�d
				if (vecSpecies.size() > 0)
				{
					for (UINT ii = 0;ii < vecSpecies.size();ii++)
					{
						recSpecie = vecSpecies[ii];
						if (mapSpcVolume[recSpecie.getSpcID()] > 0.0 &&
							  mapSpcValue[recSpecie.getSpcID()] > 0.0)
						{
							sSpecieName = _T("- ") + recSpecie.getSpcName();
//							sRowStr = formatData(_T("%-15s%-15s%15.2f%25.2f"),_T(""),sSpecieName,mapSpcVolume[recSpecie.getSpcID()],mapSpcValue[recSpecie.getSpcID()]);
							sRowStr = formatData(_T("%-15s%-15s%15.2f%25s"),_T(""),sSpecieName,mapSpcVolume[recSpecie.getSpcID()],formatNumber(mapSpcValue[recSpecie.getSpcID()],0));
							m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
							if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
						}	// if (mapSpcVolume[recSpecie.getSpcID()] > 0.0 &&
					}	// for (UINT ii = 0;ii < vecSpecies.size();ii++)
				}	// if (vecSpcies.size() > 0)
				// Clear map data; 080916 p�d
				mapSpcVolume.clear();
				mapSpcValue.clear();
				// "F�rdyrad avverkning"
//				sRowStr = formatData(_T("%-30s%15.2f%25.2f"),(m_xml.str(IDS_STRING54128)),fHighCostVolume,fHighCostValue);
				sRowStr = formatData(_T("%-30s%15.2f%25s"),(m_xml.str(IDS_STRING54128)),fHighCostVolume,formatNumber(fHighCostValue,0));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				fSumCompensation += fHighCostValue;

				// "Annan ers�ttning"
				if (vecOtherComp.size() > 0)
				{
					sRowStr = formatData(_T("%-30s"),(m_xml.str(IDS_STRING54127)));
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
					for (UINT i = 0;i < vecOtherComp.size();i++)
					{
						recOtherComp = vecOtherComp[i];
						sOtherCompNote = recOtherComp.getPropOtherCompNote();
						if (sOtherCompNote.GetLength() > 40)
						{
							sOtherCompNote.Delete(41,sOtherCompNote.GetLength());
							sOtherCompNote += _T("...");
						}
//						sRowStr = formatData(_T("- %-58s%10.2f"),sOtherCompNote,recOtherComp.getPropOtherCompValue_calc());
						sRowStr = formatData(_T("- %-58s%10s"),sOtherCompNote,formatNumber(recOtherComp.getPropOtherCompValue_calc(),0));
						m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
						if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
						fSumCompensation += recOtherComp.getPropOtherCompValue_calc();
					}	// for (UINT i = 0;i < vecOtherCOmp.size();i++)				
				}	// if (vecOtherComp.size() > 0)
				
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%70s"),_T(""))))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);pRec = NULL;

//				sRowStr = formatData(_T("%-60s%10.2f"),(m_xml.str(IDS_STRING54129)),fSumCompensation);
				sRowStr = formatData(_T("%-60s%10s"),(m_xml.str(IDS_STRING54129)),formatNumber(fSumCompensation,0));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				//*****************************************************************************************
				//								END: Add summarized data for Evaluation; 080528 p�d
				//*****************************************************************************************
			}	// if (m_xml.Load(m_sLangFN))
			m_xml.clean();
		}	// if (fileExists(m_sLangFN))

	}
	m_pReportView->GetReportCtrl().Populate();
	m_pReportView->GetReportCtrl().UpdateWindow();

	vecProperties.clear();
	vecEvaluations.clear();
	vecCruiseRandTrees.clear();
	vecOtherComp.clear();
	vecSpecies.clear();
	pObj = NULL;

}

void CObjectResultFormView::setupRotpostReport()
{
	CString S;
	CString sRowStr;
	CString sM3fub;
	CString sM3to;
	CString sM3fub_price;
	CString sM3to_price;
	CString sM3fub_value;
	CString sM3to_value;
	double fGagnVirke = 0.0;
	double fValue = 0.0;
	double fCost = 0.0;
	double fNetto = 0.0;

	vecString vecProperties;
	vecTransactionTraktAss vecAssortByTrakts;
	CTransaction_trakt_ass recAssorts;
	vecTransactionSpecies vecSpecies;
	CTransaction_species recSpecie;
	CELVObjectSumRec *pRec = NULL;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_object recObject;
	BOOL bIsSpecieNameAdded = FALSE;
	//BOOL bIsPriceReduction = FALSE;
	//BOOL bIsShowComments = FALSE;
	vecTransaction_elv_m3sk_per_specie vecInsideOutsideVolumes;
	CTransaction_elv_m3sk_per_specie recTransaction_elv_m3sk_per_specie;
	vecTransaction_elv_cruise vecCruisePerLandowner;
	CTransaction_elv_cruise recCruisePerLandowner;

	CStringArray sarrMsg;

	// Start collecting data for "St�mplingsl�ngd"; 080526 p�d

	if (m_pDB != NULL && pObj != NULL)
	{
		m_nActiveReport = TAB_RESULT_NETTO_REPORT;
		//-----------------------------------------------------------------------------------------
		// "H�mta in aktuellt objekt"; 080605 p�d
		m_pDB->getObject(pObj->getObjID_pk(),recObject);
		// "H�mta in fastighet(er) f�r Mark�garnummer och Objekt"; 080529 p�d
		m_pDB->getPropertiesByLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecProperties);
		// "H�mta in sortimet f�r trakter som h�r till mark�garkonstellation och Objekt"; 080529 p�d
		m_pDB->getAssortmentsByTrakts(m_sSQLTraktIDs_assorts,vecAssortByTrakts);
		// "H�mta in gangvirke,virkesv�rde,kostnader och rotnetto"; 080529 p�d
		m_pDB->getRotpostDataForLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,&fGagnVirke,&fValue,&fCost,&fNetto);
		// "H�mta in information om trakter(best�nd), f�r Object och Mark�garnummer"; 080605 p�d
		m_pDB->getCruisesForLandOwnerNumber(pObj->getObjID_pk(),m_sSelectedLandOwnerNumber,vecCruisePerLandowner);
		// "H�mta in Tr�dslag fr�n fst_species_table"; 080528 p�d
		//m_pDB->getSpecies(vecSpecies);
		// "H�mta tr�dslag fr�n Prislistan i Objektet"; 081028 p�d
		xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
		if (pPrlParser != NULL)
		{
			if (pPrlParser->loadStream(pObj->getObjPricelistXML()))
			{
				//---------------------------------------------------------------------------------
				// Get species in pricelist; 100104 p�d
				pPrlParser->getSpeciesInPricelistFile(vecSpecies);
			}
			delete pPrlParser;
		}
		if (fileExists(m_sLangFN))
		{
			if (m_xml.Load(m_sLangFN))
			{
				//*****************************************************************************************
				//								START: Add header information
				//*****************************************************************************************
				m_pReportView->GetReportCtrl().ResetContent();
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((getDBDateTime()))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL); pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54200)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_HEADER);pRec = NULL;
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((formatData(_T("%s : %s"),
																																				m_xml.str(IDS_STRING5403),
																																				m_sSelectedLandOwnerNumber)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);
				pRec = NULL;
				// Add properties for this Landownernumber and Object; 080528 p�d
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING5405)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);
				if (vecProperties.size() > 0)
				{
					for (UINT i = 0;i < vecProperties.size();i++)
					{
						m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((vecProperties[i]))));
						if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);
						pRec = NULL;
					}	// for (UINT i = 0;i < vecProperties.size();i++)
				}	// if (vecProperties.size() > 0)

				//*****************************************************************************************
				//								END: Add header information
				//*****************************************************************************************
				//	Empty row
				m_pReportView->GetReportCtrl().AddRecord(new CELVObjectSumRec());

				//*****************************************************************************************
				//								START: Add information on Assortments per specie; 080529 p�d
				//*****************************************************************************************
				if (vecAssortByTrakts.size() > 0)
				{
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54201)))));
					if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);pRec = NULL;
					//	Empty row
					m_pReportView->GetReportCtrl().AddRecord(new CELVObjectSumRec());

					// Setup and add headline for assortments; 080529 p�d
					sRowStr = formatData(_T("%25s%10s%10s%10s%10s%14s%14s"),
							_T(""),
							(m_xml.str(IDS_STRING54202)),
							(m_xml.str(IDS_STRING54203)),
							(m_xml.str(IDS_STRING54204)),
							(m_xml.str(IDS_STRING54205)),
							(m_xml.str(IDS_STRING54206)),
							(m_xml.str(IDS_STRING54207)));
					m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
					if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);pRec = NULL;

					if (vecSpecies.size() > 0)
					{
						for (UINT i1 = 0;i1 < vecSpecies.size();i1++)
						{
							bIsSpecieNameAdded = FALSE;

							recSpecie = vecSpecies[i1];	
							for (UINT i2 = 0;i2 < vecAssortByTrakts.size();i2++)
							{
								recAssorts = vecAssortByTrakts[i2];
								sM3fub.Empty();
								sM3to.Empty();
								sM3fub_price.Empty();
								sM3to_price.Empty();
								sM3fub_value.Empty();
								sM3to_value.Empty();
								if (recAssorts.getTAssTraktDataID() == recSpecie.getSpcID()) // && recAssorts.getValueM3FUB() > 0)
								{
									// Only add name of specie one time for each assortments; 080529 p�d
									if (!bIsSpecieNameAdded)
									{
										m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((recSpecie.getSpcName()))));
										if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_BOLD);pRec = NULL;
										bIsSpecieNameAdded = TRUE;
									}	// if (!bIsSpecieNameAdded)

									// We'll setup values, so if value  = 0, the string = ""; 080529 p�d
									if (recAssorts.getM3FUB() > 0) sM3fub = formatData(_T("%.2f"),recAssorts.getM3FUB());
									if (recAssorts.getM3TO() > 0) sM3to = formatData(_T("%.2f"),recAssorts.getM3TO());
//									if (recAssorts.getPriceM3FUB() > 0) sM3fub_price = formatData(_T("%.0f"),recAssorts.getPriceM3FUB());
									if (recAssorts.getPriceM3FUB() > 0) sM3fub_price = formatNumber(recAssorts.getPriceM3FUB(),0);
//									if (recAssorts.getPriceM3TO() > 0) sM3to_price = formatData(_T("%.0f"),recAssorts.getPriceM3TO());
									if (recAssorts.getPriceM3TO() > 0) sM3to_price = formatNumber(recAssorts.getPriceM3TO(),0);
//									if (recAssorts.getValueM3FUB() > 0) sM3fub_value = formatData(_T("%.0f"),recAssorts.getValueM3FUB());
									if (recAssorts.getValueM3FUB() > 0) sM3fub_value = formatNumber(recAssorts.getValueM3FUB(),0);
//									if (recAssorts.getValueM3TO() > 0) sM3to_value = formatData(_T("%.0f"),recAssorts.getValueM3TO());
									if (recAssorts.getValueM3TO() > 0) sM3to_value = formatNumber(recAssorts.getValueM3TO(),0);

									sRowStr = formatData(_T("%04d - %-18s%10s%10s%10s%10s%14s%14s"),
										recAssorts.getTAssTraktID(),
										recAssorts.getTAssName(),
										sM3fub,
										sM3to,
										sM3fub_price,
										sM3to_price,
										sM3fub_value,
										sM3to_value);

										m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
										if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
							
								}	// if (recAssorts.getTAssTraktDataID() == recSpecie.getSpcID())
							}	// for (UINT i2 = 0;i2 < vecAssortByTrakts.size();i2++)
						}	// for (UINT i1 = 0;i1 < vecSpecies.size();i1++)
					}	// if (vecSpecies.size() > 0)	
				}	// if (vecAssortByTrakts.size() > 0)
				//*****************************************************************************************
				//								END: Add information on Assortments per specie; 080529 p�d
				//*****************************************************************************************
				//	Empty row
				m_pReportView->GetReportCtrl().AddRecord(new CELVObjectSumRec());
				// We'll setup the sarrMsg header lines here; 080605 p�d
				sarrMsg.Add((m_xml.str(IDS_STRING54220)));
	
				//*****************************************************************************************
				//								START: Add information on rotpost; 080529 p�d
				//*****************************************************************************************
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((m_xml.str(IDS_STRING54208)))));
				if (pRec != NULL) pRec->setColumnFont(FNT_NORMAL_BOLD);pRec = NULL;
				//	Empty row
				m_pReportView->GetReportCtrl().AddRecord(new CELVObjectSumRec());
				// "Gagnvirke"
				sRowStr = formatData(_T("%-15s:%10s%5s"),(m_xml.str(IDS_STRING54209)),formatNumber(fGagnVirke,0),(m_xml.str(IDS_STRING54213)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				// "Virkesv�rde"
				sRowStr = formatData(_T("%-15s:%10s%5s"),(m_xml.str(IDS_STRING54210)),formatNumber(fValue,0),(m_xml.str(IDS_STRING54214)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				// "Total kostnad"
				sRowStr = formatData(_T("%-15s:%10s%5s"),(m_xml.str(IDS_STRING54211)),formatNumber(fCost,0),(m_xml.str(IDS_STRING54214)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				// Underline row
				sRowStr = formatData(_T("%32s"),_T(""));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL_UNDERLINE);pRec = NULL;
				// "NETTO"
				sRowStr = formatData(_T("%-15s:%10s%5s"),(m_xml.str(IDS_STRING54212)),formatNumber(fNetto,0),(m_xml.str(IDS_STRING54214)));
				m_pReportView->GetReportCtrl().AddRecord((pRec = new CELVObjectSumRec((sRowStr))));
				if (pRec != NULL) pRec->setColumnFont(FNT_SMALL);pRec = NULL;
				//*****************************************************************************************
				//								END: Add information on rotpost; 080529 p�d
				//*****************************************************************************************
			}	// if (m_xml.Load(m_sLangFN))
			m_xml.clean();
		}	// if (fileExists(m_sLangFN))

	}
	m_pReportView->GetReportCtrl().Populate();
	m_pReportView->GetReportCtrl().UpdateWindow();

	vecProperties.clear();
	vecAssortByTrakts.clear();
	vecSpecies.clear();
	vecCruisePerLandowner.clear();
	sarrMsg.RemoveAll();
	pObj = NULL;
}