#pragma once


#include "Resource.h"

#include "htmlctrl.h"

// CHtmlMsgDlg dialog

class CHtmlMsgDlg : public CDialog
{
	DECLARE_DYNAMIC(CHtmlMsgDlg)

	CHTMLCtrl m_cHTML;

	CString m_sHTMLText;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
	CButton m_wndBtnPrintOut;
public:
	CHtmlMsgDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHtmlMsgDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG26 };

	void startHTML(void);
	void addHeader(LPCTSTR text,short font_size=10,bool bold=false);
	void addTable();
	void endTable();
	void addTableBR(short font_size=10);
	void addTableHR(short font_size=10);
	void addTableText(LPCTSTR text,short font_size=10,bool bold=false);

	int ShowDialog()	{ endHTML(); showHTML(); return DoModal(); }

	inline void print()	{ m_cHTML.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER, NULL, NULL); }
protected:
	void endHTML(void);
	void showHTML(void);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};
