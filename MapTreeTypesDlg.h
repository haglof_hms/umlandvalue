#pragma once

#include "Resource.h"
#include <list>
#include <map>

typedef std::map<CString, int> TreeTypeMap;
typedef std::list<CString*> TreeTypeList;

struct TREETYPE
{
	CString name;
	int type;
};


// CMapTreeTypesDlg dialog

class CMapTreeTypesDlg : public CDialog
{
	DECLARE_DYNAMIC(CMapTreeTypesDlg)

public:
	CMapTreeTypesDlg(TreeTypeMap *pTreemap,
					 CWnd* pParent = NULL);
	virtual ~CMapTreeTypesDlg();

	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG33 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

	static TREETYPE vecTreeTypes[4];

	CXTPPropertyGrid m_grid;
	TreeTypeList m_vals;
	TreeTypeMap *m_pTreemap;

	DECLARE_MESSAGE_MAP()
};
