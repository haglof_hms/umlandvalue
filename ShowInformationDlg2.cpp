// ShowInformationDlg2.cpp : implementation file
//

#include "stdafx.h"
#include "ShowInformationDlg2.h"


// CShowInformationDlg2 dialog

IMPLEMENT_DYNAMIC(CShowInformationDlg2, CDialog)



BEGIN_MESSAGE_MAP(CShowInformationDlg2, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

CShowInformationDlg2::CShowInformationDlg2(CWnd* pParent /*=NULL*/)
	: CDialog(CShowInformationDlg2::IDD, pParent)
{
	m_pDB = NULL;

	m_bInitialized = FALSE;
	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 90;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_BOLD;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);
}

CShowInformationDlg2::~CShowInformationDlg2()
{
}

void CShowInformationDlg2::OnDestroy()
{
	m_fnt1.DeleteObject();
	m_fnt2.DeleteObject();

	m_xml.clean();

	CDialog::OnDestroy();
}

void CShowInformationDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// CShowInformationDlg2 message handlers

BOOL CShowInformationDlg2::OnInitDialog()
{
	CDialog::OnInitDialog();
	CWnd *pBtn = NULL;

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			m_xml.Load(m_sLangFN);

			SetWindowText((m_xml.str(IDS_STRING5300)));
			if ((pBtn = GetDlgItem(IDCANCEL)) != NULL)
				pBtn->SetWindowText((m_xml.str(IDS_STRING4245)));
		}
		m_bInitialized = TRUE;
	}		// if (!m_bInitialized)



	return TRUE;
}


// CShowInformationDlg message handlers

void CShowInformationDlg2::OnPaint()
{
	int nTextInX_object = 0;
	int nTextInX_object_name = 0;
	int nTextInX_prop = 0;
	int nTextInX_prop_name = 0;
	CDC *pDC = GetDC();
	CRect clip;
	GetClientRect(&clip);		// get rect of the control

//	clip.DeflateRect(1,1);
	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(1,1,clip.right,clip.bottom,INFOBK);
	pDC->RoundRect(clip.left+1,clip.top+2,clip.right-2,clip.bottom-30,10,10);

	showHigherCosts(pDC);

	CDialog::OnPaint();

}


// PRIVATE

void CShowInformationDlg2::showHigherCosts(CDC *dc)
{
	int nLen;
	int nLen1;
	int nLen2;
	int nLen3;
	int nLen4;
	int nLen5;
	int nObjID = -1;
	int nNumOfEvals = 0;
	double fM3Sk = 0.0;
	double fM3Sk_last = 0.0;
	double fKrPerM3Sk = 0.0;
	double fBaseComp = 0.0;
	double fSUMHigherCosts = 0.0;
	double fRotpost = 0.0;
	int nTypeSet = -1;
	double fBreakValue = 0.0;
	double fFactor = 0.0;
	CString sName,sDoneBy;
	TEXTMETRIC tm;
	BOOL bFound = FALSE;
	CTransaction_elv_object *pObj = getActiveObject();
	CTransaction_elv_properties *pProp = getActiveProperty();
	TemplateParser pars; // Parser for P30 and HighCost; 080407 p�d
	vecObjectTemplate_hcost_table vecHCost;
	CObjectTemplate_hcost_table recHCost;
	if (pObj != NULL && pProp != NULL && m_pDB != NULL)
	{
		nObjID = pObj->getObjID_pk();

		// Get Total volume in m3sk for property; 080515 p�d
		fM3Sk = pProp->getPropM3Sk();
		fRotpost = pProp->getPropRotpost();

		m_pDB->getNumOfEvaluations(nObjID, pProp->getPropID_pk(), &nNumOfEvals);

		if (pars.LoadFromBuffer(pObj->getObjHCostXML() ))
		{
			vecHCost.clear();
			pars.getObjTmplHCost(&nTypeSet,&fBreakValue,&fFactor,vecHCost,sName,sDoneBy);
			if (nTypeSet != 1 && nTypeSet != 2) nTypeSet = 1;

		}	// if (pars.LoadFromBuffer(pObj->getObjHCostXML() ))
		if (nTypeSet == 1)
		{
			if (vecHCost.size() > 0)
			{
				// Try to run the vecHCost backwards to match the volume; 080515 p�d
				for (UINT i = 0;i < vecHCost.size();i++)
				{
					recHCost = vecHCost[i];
					if (fM3Sk >= fM3Sk_last && fM3Sk <= recHCost.getM3Sk())
					{
						fKrPerM3Sk = recHCost.getPriceM3Sk();
						fBaseComp = recHCost.getBaseComp();
						bFound = TRUE;
						break;
					}
					fM3Sk_last = recHCost.getM3Sk();
				}	// for (UINT i = vecHCost.size()-1;i <= 0;i--)
				// No value vas found, set Table value (kr/m3) to last entry
				// in table; 080515 p�d
				if (!bFound)
				{
					fKrPerM3Sk = vecHCost[vecHCost.size()-1].getPriceM3Sk();
					fBaseComp = vecHCost[vecHCost.size()-1].getBaseComp();
				}
				// Calculate SUM of Higher costs; 080515 p�d
				fSUMHigherCosts = (fM3Sk * fKrPerM3Sk) + fBaseComp;
				// Can't be less then 0.0; 090617 p�d
				if (fSUMHigherCosts < 0.0) fSUMHigherCosts = 0.0;
			}	// if (vecHCost.size() > 0)

			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d

			dc->TextOut(10, tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53003))));
			nLen1 = m_xml.str(IDS_STRING53003).GetLength();
			dc->TextOut(10, 3*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53000))));
			nLen2 = m_xml.str(IDS_STRING53000).GetLength();
			dc->TextOut(10, 5*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53001))));
			nLen3 = m_xml.str(IDS_STRING53001).GetLength();
			dc->TextOut(10, 9*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53002))));
			nLen4 = m_xml.str(IDS_STRING53002).GetLength();

			nLen = max(nLen1,nLen2);
			nLen = max(nLen,nLen3);
			nLen = max(nLen,nLen4);
	
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
			dc->SelectObject(&m_fnt2);
			dc->DrawText(formatData(_T("%.2f"),fBaseComp),CRect(50+nLen*tm.tmAveCharWidth,tm.tmHeight+10,110+nLen*tm.tmAveCharWidth, 3*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			dc->DrawText(formatData(_T("%.2f"),fM3Sk),CRect(50+nLen*tm.tmAveCharWidth,3*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,4*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			dc->DrawText(formatData(_T("%.2f"),fKrPerM3Sk),CRect(50+nLen*tm.tmAveCharWidth,5*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			if (pProp->getPropNumOfStands() || nNumOfEvals)
			{
				if (fSUMHigherCosts > 0.0)
					dc->DrawText(formatData(_T("%.2f + (%.2fx%.2f) = %.2f"),fBaseComp,fM3Sk,fKrPerM3Sk,fSUMHigherCosts),CRect(20, 11*tm.tmHeight+10,150+nLen*tm.tmAveCharWidth, 12*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				else
					dc->DrawText(formatData(_T("(%.2f + (%.2fx%.2f)) : %.2f"),fBaseComp,fM3Sk,fKrPerM3Sk,fSUMHigherCosts),CRect(20, 11*tm.tmHeight+10,150+nLen*tm.tmAveCharWidth, 12*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			}
			else
			{
				dc->DrawText(formatData(_T("%.2f"), 0.0),CRect(20,12*tm.tmHeight+10,200+9*tm.tmAveCharWidth,13*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
			}
		}
		else if (nTypeSet == 2)
		{
			dc->SelectObject(&m_fnt1);
			dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
			dc->TextOut(10, tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53004))));
			// Calculate Higher costs ("F�rdyrad avverkning") based on "Vattenfall"; 090529 p�d
			// Change calculation on (fRotpost <= fBreakValue) from: 
			//	fSUMHigherCosts = fRotpost to fSUMHigherCosts = fRotpost + (fM3Sk*fFactor): 090701 p�d
			if (fRotpost <= fBreakValue)
			{
				fSUMHigherCosts = fRotpost + (fM3Sk*fFactor);

				if (fSUMHigherCosts < 0.0) fSUMHigherCosts = 0.0;

				dc->TextOut(10, 3*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53005))));
				
				dc->TextOut(10, 5*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53007))));
				nLen1 = m_xml.str(IDS_STRING53007).GetLength();
				dc->TextOut(10, 7*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53008))));
				nLen2 = m_xml.str(IDS_STRING53008).GetLength();			
				dc->TextOut(10, 9*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53009))));
				nLen3 = m_xml.str(IDS_STRING53008).GetLength();			
				dc->TextOut(10, 11*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53000))));
				nLen4 = m_xml.str(IDS_STRING53000).GetLength();			
				dc->TextOut(10, 13*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53002))));
				nLen5 = m_xml.str(IDS_STRING53002).GetLength();
			
				nLen = max(nLen1,nLen2);
				nLen = max(nLen,nLen3);
				nLen = max(nLen,nLen4);
				nLen = max(nLen,nLen5);

				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
				dc->SelectObject(&m_fnt2);
				dc->DrawText(formatData(_T("%.2f"),fRotpost),CRect(50+nLen*tm.tmAveCharWidth,5*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth, 6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fBreakValue),CRect(50+nLen*tm.tmAveCharWidth,7*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,8*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fFactor),CRect(50+nLen*tm.tmAveCharWidth,9*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,10*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fM3Sk),CRect(50+nLen*tm.tmAveCharWidth,11*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,12*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				if (pProp->getPropNumOfStands() || nNumOfEvals)
				{
					if (fSUMHigherCosts > 0.0)
						dc->DrawText(formatData(_T("%.2f + (%.2fx%.2f) = %.2f"),fRotpost,fFactor,fM3Sk,fSUMHigherCosts),CRect(20,15*tm.tmHeight+10,200+nLen*tm.tmAveCharWidth,16*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
					else
						dc->DrawText(formatData(_T("(%.2f + (%.2fx%.2f)) : %.2f"),fRotpost,fFactor,fM3Sk,fSUMHigherCosts),CRect(20,15*tm.tmHeight+10,200+nLen*tm.tmAveCharWidth,16*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				}
				else
				{
					dc->DrawText(formatData(_T("%.2f"), 0.0),CRect(20,12*tm.tmHeight+10,200+9*tm.tmAveCharWidth,13*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				}
			}
			else if (fRotpost > fBreakValue)
			{
				fSUMHigherCosts = fBreakValue + (fM3Sk*fFactor);

				if (fSUMHigherCosts < 0.0) fSUMHigherCosts = 0.0;

				dc->TextOut(10, 3*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53006))));
			
				dc->TextOut(10, 5*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53007))));
				nLen1 = m_xml.str(IDS_STRING53007).GetLength();
				dc->TextOut(10, 7*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53008))));
				nLen2 = m_xml.str(IDS_STRING53008).GetLength();			
				dc->TextOut(10, 9*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53009))));
				nLen3 = m_xml.str(IDS_STRING53008).GetLength();			
				dc->TextOut(10, 11*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53000))));
				nLen4 = m_xml.str(IDS_STRING53000).GetLength();			
				dc->TextOut(10, 13*tm.tmHeight+10,formatData(_T("%s"),(m_xml.str(IDS_STRING53002))));
				nLen5 = m_xml.str(IDS_STRING53002).GetLength();
			
				nLen = max(nLen1,nLen2);
				nLen = max(nLen,nLen3);
				nLen = max(nLen,nLen4);
				nLen = max(nLen,nLen5);

				dc->GetTextMetrics(&tm);	// Get metrics for selected font; 080519 p�d
				dc->SelectObject(&m_fnt2);
				dc->DrawText(formatData(_T("%.2f"),fRotpost),CRect(50+nLen*tm.tmAveCharWidth,5*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth, 6*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fBreakValue),CRect(50+nLen*tm.tmAveCharWidth,7*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,8*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fFactor),CRect(50+nLen*tm.tmAveCharWidth,9*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,10*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				dc->DrawText(formatData(_T("%.2f"),fM3Sk),CRect(50+nLen*tm.tmAveCharWidth,11*tm.tmHeight+10,110+nLen*tm.tmAveCharWidth,12*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				if (pProp->getPropNumOfStands() || nNumOfEvals)
				{
					if (fSUMHigherCosts > 0.0)
						dc->DrawText(formatData(_T("%.2f + (%.2fx%.2f) = %.2f"),fBreakValue,fFactor,fM3Sk,fSUMHigherCosts),CRect(20,15*tm.tmHeight+10,200+nLen*tm.tmAveCharWidth,16*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
					else
						dc->DrawText(formatData(_T("(%.2f + (%.2fx%.2f)) : %.2f"),fBreakValue,fFactor,fM3Sk,fSUMHigherCosts),CRect(20,15*tm.tmHeight+10,200+nLen*tm.tmAveCharWidth,16*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				}
				else
				{
					dc->DrawText(formatData(_T("%.2f"), 0.0),CRect(20,12*tm.tmHeight+10,200+9*tm.tmAveCharWidth,13*tm.tmHeight+10),DT_RIGHT|DT_SINGLELINE);
				}
			}

		}
	}
	// Need to release ref. pointers; 080520 p�d
	pObj = NULL;
	pProp = NULL;
}
