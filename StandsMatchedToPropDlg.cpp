// StandsMatchedToPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "StandsMatchedToPropDlg.h"

#include "ResLangFileReader.h"

// CStandsMatchedToPropDlg dialog

IMPLEMENT_DYNAMIC(CStandsMatchedToPropDlg, CDialog)

BEGIN_MESSAGE_MAP(CStandsMatchedToPropDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CStandsMatchedToPropDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CStandsMatchedToPropDlg::CStandsMatchedToPropDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStandsMatchedToPropDlg::IDD, pParent)
{
	m_pDB = NULL;
}

CStandsMatchedToPropDlg::~CStandsMatchedToPropDlg()
{
	m_ilIcons.DeleteImageList();
	m_vecTrakt.clear();
	m_vecTrakts_selected.clear();
	m_vecTraktIDs.clear();

}

void CStandsMatchedToPropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL_MATCH1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_MATCH2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_MATCH3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_MATCH4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL_MATCH5, m_wndLbl5);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

BOOL CStandsMatchedToPropDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING116)));

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING3203)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING3204)));
			m_wndLbl5.SetWindowText((xml.str(IDS_STRING3205)));
			m_wndLbl5.SetBkColor(INFOBK);
			m_wndLbl5.SetTextColor(BLUE);
			m_wndLbl3.SetWindowText(m_recELVProp.getPropName());
			m_wndLbl4.SetWindowText(m_recELVProp.getPropNumber());
			m_wndLbl3.SetLblFontEx(-1,FW_BOLD);
			m_wndLbl4.SetLblFontEx(-1,FW_BOLD);

			m_wndOKBtn.SetWindowText((xml.str(IDS_STRING1160)));
			m_wndCancelBtn.SetWindowText((xml.str(IDS_STRING1161)));
			xml.clean();
		}
	}

	setupReport();

	populateData();

	return TRUE;
}

void CStandsMatchedToPropDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_MATCHING_STANDS, FALSE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
		m_ilIcons.Add(&bmp, RGB(255, 0, 255));

		m_wndReport.SetImageList(&m_ilIcons);

		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 20,FALSE,12));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING1162)), 150));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING1163)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING40120)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING1165)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING1166)), 120));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_LEFT);

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.GetRecords()->SetCaseSensitive(FALSE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_MATCHING_STANDS),5,70,rect.right - 10,rect.bottom - 110);
				xml.clean();
			}
		}	// if (fileExists(sLangFN))
	}
}

void CStandsMatchedToPropDlg::populateData(void)
{
	CString S;
	vecTransaction_elv_cruise vecCruise;
	vecTransaction_elv_cruise vecCruise2;
	vecTransaction_elv_object vecObj;
	m_vecTrakt.clear();
	BOOL bIsInCruiseTable = FALSE;
	CString sObjects;
	if (m_pDB != NULL)
	{
		m_pDB->getTrakts_match_stand(m_recELVProp.getPropObjectID_pk(),m_recELVProp.getPropID_pk(),m_vecTrakt);
		m_pDB->getObjectCruises(m_recELVProp.getPropID_pk(),m_recELVProp.getPropObjectID_pk(),vecCruise);
//		m_pDB->getObjectCruises(m_recELVProp.getPropObjectID_pk(),vecCruise);
		m_pDB->getObjectCruises(vecCruise2);
		m_pDB->getObject(vecObj);
	}	// if (m_pDB != NULL)

	m_wndReport.ResetContent();
	m_vecTraktIDs.clear();

	if (m_vecTrakt.size() > 0)
	{
		for (UINT i = 0;i < m_vecTrakt.size();i++)
		{
			bIsInCruiseTable = FALSE;
			// Do a check, to see if one or more Stands already
			// is in the "elv_cruise_table". If so set the
			// checkbok in Report; 080604 p�d
			if (vecCruise.size() > 0)
			{
				for (UINT i1 = 0;i1 < vecCruise.size();i1++)
				{
					if (vecCruise[i1].getECruID_pk() == m_vecTrakt[i].getTraktID())
					{
						bIsInCruiseTable = TRUE;
						break;
					}	// if (vecCruise[i1].getEcruID_pk() == m_vecTrakt[i].getTraktID())
				}

				// Try to find out if stand is included in other objects; 101118 p�d
				for (UINT i2 = 0;i2 < vecObj.size();i2++)
				{
					for (UINT i3 = 0;i3 < vecCruise2.size();i3++)
					{
						if (vecObj[i2].getObjID_pk() == vecCruise2[i3].getECruObjectID_pk() && 
								m_vecTrakt[i].getTraktID() == vecCruise2[i3].getECruID_pk())
						{
							sObjects += vecObj[i2].getObjectName() + L" ; " + vecObj[i2].getObjIDNumber() + '\n';
						}				
					}	// for (UINT i2 = 0;i2 < vecObj.size();i2++)
				}	// for (UINI i1 = 0;i1 < vecCruise.size();i1++)

			}	// if (vecCruise.size() > 0)
			sObjects = sObjects.Left(sObjects.GetLength()-1);
			m_wndReport.AddRecord(new CStandsMatchedToPropRec(i,bIsInCruiseTable,m_vecTrakt[i],sObjects));
			m_vecTraktIDs.push_back(m_vecTrakt[i].getTraktID());
			sObjects.Empty();
		}	// for (UINT i = 0;i < m_vecTrakt.size();i++)
	}	// if (vecTrakt.size() > 0)
	m_wndReport.Populate();
//	m_wndReport.UpdateWindow();
	// Added 081006 p�d
	m_wndReport.RedrawControl();

	vecCruise.clear();
}

void CStandsMatchedToPropDlg::OnBnClickedOk()
{
	// Try to match stands selected by user; 080604 p�d
	CStandsMatchedToPropRec *pRec = NULL;
	m_vecTrakts_selected.clear();
	m_vecTrakts_de_selected.clear();
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{	
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CStandsMatchedToPropRec*)pRows->GetAt(i)->GetRecord();
			// Only add Stands that are checked; 080604 p�d
			if (pRec->getColumnCheck(COLUMN_0))
				m_vecTrakts_selected.push_back(pRec->getRecord());
			else 
				m_vecTrakts_de_selected.push_back(pRec->getRecord());
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	OnOK();
}
