#pragma once

#include "Resource.h"

// CInfoDlgBar dialog

class CInfoDlgBar : public CDialogBar
{
	DECLARE_DYNAMIC(CInfoDlgBar)

	BOOL m_bIsPropData;

	BOOL m_bClearWindow;

	CString m_sObjID;
	CString m_sTimeStamp;
	CString m_sObjCap;
	CString m_sPropCap;
	CString m_sPropAreal;

	CFont m_fnt1;
	CFont m_fnt2;
	CTransaction_elv_object recObject;
	CTransaction_elv_properties recProp;
public:
	CInfoDlgBar(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInfoDlgBar();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR };

	void setObjID(LPCTSTR str)
	{
		m_sObjID = str;
	}
	void setTimeStamp(LPCTSTR str)
	{
		m_sTimeStamp = str;
	}

	void setObjCap(LPCTSTR str)
	{
		m_sObjCap = str;
	}
	void setPropCap(LPCTSTR str,LPCTSTR str1)
	{
		m_sPropCap = str;
		m_sPropAreal.Format(_T("%s : "),str1) ;
	}

	void setObjectRecord(CTransaction_elv_object &obj_rec,BOOL is_clear)
	{
		recObject = obj_rec;
		m_bClearWindow = is_clear;
		Invalidate();	// Init. OnPaint(); 080428 p�d
		UpdateWindow();
	}
	void setPropertyRecord(CTransaction_elv_properties &prop_rec,BOOL is_data)
	{
		recProp = prop_rec;
		m_bIsPropData = is_data;
		m_bClearWindow = FALSE;
		Invalidate();	// Init. OnPaint(); 080428 p�d
		UpdateWindow();
	}
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//{{AFX_MSG(CInfoDlgBar)
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
