#pragma once

#include "Resource.h"


/////////////////////////////////////////////////////////////////////////////////
// CSelectedPropertiesDataRec

class CSelectedPropertiesDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_property propRec;
	CString sPropName;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

public:
	CSelectedPropertiesDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CSelectedPropertiesDataRec(UINT index,CTransaction_property data)	 
	{
		m_nIndex = index;
		propRec = data;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem((data.getObjectID())));
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
//		AddItem(new CTextItem(_T(data.getParishName())));	// Not used "Region"; 080415 p�d
		AddItem(new CTextItem((data.getPropertyNum())));
//		sPropName.Format(_T("%s %s:%s"),
//			data.getPropertyName(),
//			data.getBlock(),
//			data.getUnit());
		AddItem(new CTextItem((data.getPropertyName())));
		AddItem(new CTextItem((data.getBlock())));
		AddItem(new CTextItem((data.getUnit())));
		
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	void setColumnCheck(int item,BOOL bChecked)
	{
		((CCheckItem*)GetItem(item))->setChecked(bChecked);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
	CTransaction_property &getRecord(void)
	{
		return propRec;
	}

};

// CSearchPropertiesDlg dialog

class CSearchPropertiesDlg : public CDialog
{
	DECLARE_DYNAMIC(CSearchPropertiesDlg)

	// Setup language filename; 051214 p�d
	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;

	CButton m_wndSearchBtn;
	CButton m_wndClearBtn;
	CButton m_wndCheckAll;
	CButton m_wndUncheckAll;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CMyReportCtrl m_wndReport;
	CImageList m_ilIcons;

	void setupReport(void);

	void populateData(void);

	void setupSQLFromSelections(void);

	BOOL isTableSelected(void);

	vecTransactionProperty m_vecSelectedProperties;
	vecTransactionProperty m_vecTransactionProperty;
	CUMLandValueDB *m_pDB;
public:
	CSearchPropertiesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSearchPropertiesDlg();

	void setDBConnection(CUMLandValueDB *db)
	{
		m_pDB = db;
	}
	
	void getSelectedProperties(vecTransactionProperty& vec)
	{
		vec = m_vecSelectedProperties;
	}


// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBnClickedBtnSearch();
	afx_msg void OnBnClickedBtnClear();
	afx_msg void OnBnClickedBtnOK();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
