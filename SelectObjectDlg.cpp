// SelectObjectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectObjectDlg.h"


#include "ResLangFileReader.h"

// CSelectObjectDlg dialog

IMPLEMENT_DYNAMIC(CSelectObjectDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CSelectObjectDlg, CXTResizeDialog)
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(IDOK, &CSelectObjectDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_FROM_TMPL, &CSelectObjectDlg::OnBnClickedFromTmpl)
	ON_EN_CHANGE(IDC_ED_OBJ_NAME, OnEdObjName)
	ON_EN_CHANGE(IDC_ED_OBJ_ID, OnEdObjID)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST1, OnItemchangedLCtrl)
END_MESSAGE_MAP()

CSelectObjectDlg::CSelectObjectDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CSelectObjectDlg::IDD, pParent)
{
	m_bIsSelectionOK = FALSE;
}

CSelectObjectDlg::~CSelectObjectDlg()
{
}

void CSelectObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL_OBJ_NAME, m_wndLblObjName);
	DDX_Control(pDX, IDC_LBL_OBJ_ID, m_wndLblObjID);
	DDX_Control(pDX, IDC_ED_OBJ_NAME, m_wndEdObjName);
	DDX_Control(pDX, IDC_ED_OBJ_ID, m_wndEdObjID);

	DDX_Control(pDX, IDC_LIST1, m_wndLCtrl);

	DDX_Control(pDX, IDC_GROUP_OBJECT, m_wndGroup_object);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDC_BTN_FROM_TMPL, m_wndFromTmpl);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

BOOL CSelectObjectDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING2610)));
			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING2611)));
			m_wndOKBtn.EnableWindow(m_bIsSelectionOK);

			m_wndCancelBtn.SetWindowText((xml->str(IDS_STRING2612)));
			m_wndLblObjName.SetWindowText(xml->str(IDS_STRING204));
			m_wndLblObjID.SetWindowText(xml->str(IDS_STRING205));
			m_wndFromTmpl.SetWindowText(xml->str(IDS_STRING2614));

			m_sMsgCap = (xml->str(IDS_STRING229));
			m_sMsgNoSelection = (xml->str(IDS_STRING2613));

			m_sMsgNameAndID.Format(_T("%s\n\n%s\n"),
				xml->str(IDS_STRING6006),
				xml->str(IDS_STRING6007));

			m_wndLCtrl.InsertColumn(0,(xml->str(IDS_STRING203)),LVCFMT_LEFT,100);		// �rendenummer
			m_wndLCtrl.InsertColumn(1,(xml->str(IDS_STRING204)),LVCFMT_LEFT,100);		// Objektsnamn
			m_wndLCtrl.InsertColumn(2,(xml->str(IDS_STRING205)),LVCFMT_LEFT,100);		// Objektsnummer
			m_wndLCtrl.InsertColumn(3,(xml->str(IDS_STRING212)),LVCFMT_LEFT,100);		// Intr�nget avser
			m_wndLCtrl.InsertColumn(4,(xml->str(IDS_STRING210)),LVCFMT_LEFT,100);		// Skapad av
			m_wndLCtrl.InsertColumn(5,(xml->str(IDS_STRING211)),LVCFMT_LEFT,100);		// Datum
		}
		delete xml;
	}

	m_wndLCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	m_wndEdObjName.SetLimitText(50);
	m_wndEdObjID.SetLimitText(10);

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndLCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_Header.SubclassWindow(hWndHeader);
	m_Header.SetTheme(new CXTHeaderCtrlThemeOffice2003());
	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	m_nIndexLCtrl = -1;

	setupList();

	return TRUE;
}

void CSelectObjectDlg::OnShowWindow(BOOL bShow,UINT nStatus)
{
	m_wndEdObjName.SetFocus();
	CXTResizeDialog::OnShowWindow(bShow,nStatus);
}

void CSelectObjectDlg::OnSetFocus(CWnd* wnd)
{
	CXTResizeDialog::OnSetFocus(wnd);
}

// CSelectObjectDlg message handlers

void CSelectObjectDlg::setupList(void)
{
	CString sTypeOfInfr;
	CTransaction_elv_object rec;
	vecUCFunctions funcs;
	int nIndex = -1;

	m_wndLCtrl.DeleteAllItems();

	getForrestNormTypeOfInfring(funcs);

	// Make sure there's any data to work with; 080404 p�d
	if (m_vecObjects.size() > 0)
	{	
		for (UINT i = 0;i < m_vecObjects.size();i++)
		{
			rec = m_vecObjects[i];
			nIndex = _tstoi(rec.getObjTypeOfInfring());
			sTypeOfInfr.Empty();
			if (funcs.size() > 0 && nIndex > -1 && nIndex < funcs.size())
			{
				sTypeOfInfr = funcs[nIndex].getName();
			}

			InsertRow(m_wndLCtrl,i,6,(rec.getObjErrandNum()),
															 (rec.getObjectName()),
															 (rec.getObjIDNumber()),
															 sTypeOfInfr,
															 (rec.getObjCreatedBy()),
															 (rec.getObjDate()));
		}	// for (UINT i = 0;i < m_vecObjects.size();i++)
	}	// if (m_vecObjects.size() > 0)
}

void CSelectObjectDlg::OnEdObjName()
{
	if (m_wndEdObjName.getText().IsEmpty())
	{
		m_nIndexLCtrl = -1;
		m_wndLCtrl.SetHotItem(m_nIndexLCtrl);
	}

	m_sObjName = m_wndEdObjName.getText();
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjID.IsEmpty() && m_nIndexLCtrl > -1);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);
}

void CSelectObjectDlg::OnEdObjID()
{
	if (m_wndEdObjID.getText().IsEmpty())
	{
		m_nIndexLCtrl = -1;
		m_wndLCtrl.SetHotItem(m_nIndexLCtrl);
	}

	m_sObjID = m_wndEdObjID.getText();
	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjID.IsEmpty() && m_nIndexLCtrl > -1);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);
}


void CSelectObjectDlg::OnItemchangedLCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	m_nIndexLCtrl = -1;

	if( pNMListView->uNewState & LVIS_SELECTED )
	{
		m_nIndexLCtrl = pNMListView->iItem;
	}

	m_bIsSelectionOK = (!m_sObjName.IsEmpty() && !m_sObjID.IsEmpty() && m_nIndexLCtrl > -1);
	m_wndOKBtn.EnableWindow(m_bIsSelectionOK);

	*pResult = 0;
}

void CSelectObjectDlg::OnBnClickedOk()
{
	BOOL bObjectNameAndIDUsed = FALSE;
	CTransaction_elv_object rec;
	int nSelectedItem = GetSelectedItem(m_wndLCtrl);
	if (nSelectedItem > -1)
	{
		m_recSelectedObject = m_vecObjects[nSelectedItem];
		// Check if Objectname and ObjectID is unique; 090309 p�d
		if (m_vecObjects.size() > 0)
		{
			for (UINT i = 0;i < m_vecObjects.size();i++)
			{
				rec = m_vecObjects[i];
				//S.Format(_T("Name %s == %s\nID %s == %s"),rec.getObjectName(),m_sObjName,rec.getObjIDNumber(),m_sObjNumber);
				//AfxMessageBox(S);
				if (m_recSelectedObject.getObjectName().CompareNoCase(m_sObjName) == 0 &&	m_recSelectedObject.getObjIDNumber().CompareNoCase(m_sObjID) == 0)
				{
					bObjectNameAndIDUsed = TRUE;
				}	// if (rec.getObjectName().CompareNoCase(m_sObjName) == 0 ||
			}	// for (UINT i 0 0;i < m_vecObjects.size();i++)
		}	// if (m_vecObjects.size() > 0)

		// Objectname and ObjectID unique, quit ok; 090309 p�d
		if (!bObjectNameAndIDUsed)	OnOK();
		else
		{
			::MessageBox(this->GetSafeHwnd(),m_sMsgNameAndID,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			m_sObjName.Empty();
			m_sObjID.Empty();
			m_wndEdObjName.SetWindowTextW(_T(""));
			m_wndEdObjID.SetWindowTextW(_T(""));
			m_wndEdObjName.SetFocus();
		}
	}	// if (nSelectedItem > -1)
	else
		::MessageBox(this->GetSafeHwnd(),(m_sMsgNoSelection),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
}

void CSelectObjectDlg::OnBnClickedFromTmpl()
{
	EndDialog(ID_BTN_FROM_TMPL_RETURN);
	CloseWindow();
}