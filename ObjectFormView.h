#pragma once

#include "Resource.h"

#include "UMLandValueDB.h"

#include "picturectrl.h"

// CObjectFormView form view
#define LOG_CHANGE_OBJECT 1

class CObjectFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CObjectFormView)

	BOOL m_bInitialized;
	BOOL m_bLogObject;	// #3877
	BOOL m_bSetFocus;

	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sCreateNewObjectFromTmpl;
	CString m_sCreateNewObjectFromOtherObject;
	CString m_sMandatoryDataMissing;
	CString m_sQuitAnyway;
	CString m_sRemoveObject;
	CString m_sSearchDlgTitle1;
	CString m_sSearchDlgTitle2;
	CString m_sSearchDlgTitle3;
	CString m_sNoObjectMsg;
	CString m_sRemoveLogoMsg;
	CString m_sRemoveContractorMsg;
	CString m_sRemoveRetAddressMsg;

	CString m_sGoToLastObjMsg;

	CString m_sGrowthAreaLbl;

	CString m_sOldObjName;
	CString m_sOldObjID;

	CString m_sPRelErrMsg;

	CString m_sPropTabCaption;
	CString m_sStatusLblFinished;	//new #3385
	CString m_sStatusLblOngoing;	//new #3385

	CXTResizeGroupBox m_wndGroup1_1;
	CXTResizeGroupBox m_wndGroup1_2;
	CXTResizeGroupBox m_wndGroup1_6;
	CXTResizeGroupBox m_wndGroup1_7;
	CXTResizeGroupBox m_wndGroup1_8;
	CXTResizeGroupBox m_wndGroup1_9;

	CMyExtStatic m_wndLbl1_1;
	CMyExtStatic m_wndLbl1_2;
	CMyExtStatic m_wndLbl1_3;
	CMyExtStatic m_wndLbl1_4;
	CMyExtStatic m_wndLbl1_5;
	CMyExtStatic m_wndLbl1_6;
	CMyExtStatic m_wndLbl1_7;
//	CMyExtStatic m_wndLbl1_8;
//	CMyExtStatic m_wndLbl1_9;
//	CMyExtStatic m_wndLbl1_10;
	//CMyExtStatic m_wndLbl1_11;
	CMyExtStatic m_wndLbl1_12;
	CMyExtStatic m_wndLbl1_13;
	CMyExtStatic m_wndLbl1_14;
	//CMyExtStatic m_wndLbl1_15;
	//CMyExtStatic m_wndLbl1_16;
	CMyExtStatic m_wndLbl1_17;
	//CMyExtStatic m_wndLbl1_18;
	CMyExtStatic m_wndLbl1_19;
	CMyExtStatic m_wndLbl1_20;
	CMyExtStatic m_wndLbl1_21;
	CMyExtStatic m_wndLbl1_22;
	CMyExtStatic m_wndLbl1_23;
	CMyExtStatic m_wndLbl1_24;
	CMyExtStatic m_wndLbl1_25;
	CMyExtStatic m_wndLbl2_1;	//new #3385

	CMyExtEdit m_wndEdit1_1;
	CMyExtEdit m_wndEdit1_2;
	CMyExtEdit m_wndEdit1_3;
	CMyExtEdit m_wndEdit1_4;
//	CMyExtEdit m_wndEdit1_5;
	CMyExtEdit m_wndEdit1_6;
	CMyExtEdit m_wndEdit1_7;
	CMyExtEdit m_wndEdit1_8;
	//CMyExtEdit m_wndEdit1_9;
	//CMyExtEdit m_wndEdit1_15;
	//CMyExtEdit m_wndEdit1_24;
	CMyExtEdit m_wndEdit1_25;
	CMyExtEdit m_wndEdit1_26;
	CMyExtEdit m_wndEdit1_27;
	CMyExtEdit m_wndEdit1_10;
	CMyMaskExtEdit m_wndEdit1_11;
//	CMyExtEdit m_wndEdit1_12;
//	CMyExtEdit m_wndEdit1_13;
//	CMyExtEdit m_wndEdit1_14;
	CMyExtEdit m_wndEdit1_21;
	CMyExtEdit m_wndEdit1_16;
	CMyExtEdit m_wndEdit1_17;
	CMyExtEdit m_wndEdit1_18;
	CMyExtEdit m_wndEdit1_19;

	CMyExtEdit m_wndEdit1_29;
	CMyExtEdit m_wndEdit1_30;

	CMyExtStatic m_wndEdit2_1;
	
	CButton m_wndAddContractor;
	CButton m_wndDelContractor;
	CButton m_wndShowContractor;
	
	CButton m_wndAddRetAdr;
	CButton m_wndDelRetAdr;

	CMyReportCtrl m_wndReport;
	CMyReportCtrl m_wndReport1;

	CButton m_wndAddLogo;
	CButton m_wndDelLogo;
	CPictureCtrl m_wndPicCtrl;

	int m_nShowObjects;	//new #3385, statusflagga f�r vilka objekt som ska visas, 0=endast p�g�ende, 1=endast slutf�rda, 2=visa alla

	vecInt m_vecObjectIndex;
	void getObjectIndexFromDB(void);

	CTransaction_elv_object m_recActiveObject;
	void getObjectFromDB(int obj_id,CTransaction_elv_object &);

	CTransaction_contacts m_recActiveContractor;
	void getContractorFromDB(int contact_id,CTransaction_contacts &);

	// Holds Trakt-templates; 080407 p�d
	vecTransactionTemplate m_vecTraktTemplate;
	BOOL getTraktTemplatesFromDB(void);

	// Holds P30-templates; 080407 p�d
	vecTransactionTemplate m_vecObjectP30Template;
	vecTransactionTemplate m_vecObjectP30TemplateNN;
	vecTransactionTemplate m_vecObjectP30Template2018;
	BOOL getObjectP30TemplatesFromDB(void);

	// Holds Higher cost-templates; 080407 p�d
	vecTransactionTemplate m_vecObjectHCostTemplate;
	BOOL getObjectHCostTemplatesFromDB(void);

	UINT m_nDBIndex;
	CUMLandValueDB *m_pDB;
	CUMLandValueDB *m_pDBadmin;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void populateData(void);

	void checkP30SpeciesSettings(int nObjId);

	void populateP30Table(void);
	void populateHCostTable(void);

	BOOL newObject(void);
	void createNewObjectFromTemplates(CTransaction_template&,CTransaction_template&,CTransaction_template&,CString obj_name,CString obj_number,int forest_norm_index,int growth_area);
	void createNewObjectFromOtherObject(CTransaction_elv_object& rec,CString obj_name,CString obj_id);
	BOOL saveObject(int show_msg_level);
	void removeObject(void);

	void setPopulationOfObject(void);

	void setUpdateInfoDlgBarInNormFrame(void);

	int m_nLastVisitedObjectID;
	BOOL m_bIsLastVisitedObjectID_Valid;
	void writeLastObjectIDToRegistry(void);
	BOOL readLastObjectIDFromRegistry(void);

	BOOL m_bIsPRelOKInP30Table;
protected:
	CObjectFormView();           // protected constructor used by dynamic creation
	virtual ~CObjectFormView();

	BOOL setupReport(void);
	void setupLanguage(void);

	//vecTransactionSpecies m_vecSpecies;
	//BOOL getSpeciesFromDB(void);

	// TypeOfInfr in propertyGrid; 090403 P�D
	//void addLandValueTypeOfInfring(void);

	void clearContractor(void);
	void clearObject(void);

	void setEnableWindows(BOOL enable,BOOL is_empty);

public:
	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	// Contractor ...
	int getContractorID_pk(void)		{ return m_wndEdit1_6.getItemIntData(); }
	void setContractorID_pk(int id)	{ m_wndEdit1_6.setItemIntData(id); }
	CString getPersOrgNum(void)			{ return m_wndEdit1_6.getText(); }
	void setPersOrgNum(LPCTSTR v)		{ m_wndEdit1_6.SetWindowText(v); }
	CString getContactPers(void)		{ return m_wndEdit1_7.getText(); }
	void setContactPers(LPCTSTR v)		{ m_wndEdit1_7.SetWindowText(v); }
	CString getCompany(void)				{ return m_wndEdit1_8.getText(); }
	void setCompany(LPCTSTR v)				{ m_wndEdit1_8.SetWindowText(v); }
//	CString getAddress(void)				{ return m_wndEdit1_15.getText(); }
//	void setAddress(LPCTSTR v)				{ m_wndEdit1_15.SetWindowText(v); }
//	CString getPostnum(void)				{ return m_wndEdit1_9.getText(); }
//	void setPostnum(LPCTSTR v)				{ m_wndEdit1_9.SetWindowText(v); }
//	CString getPostAddress(void)		{ return m_wndEdit1_24.getText(); }
//	void setPostAddress(LPCTSTR v)		{ m_wndEdit1_24.SetWindowText(v); }
	CString getPhoneWork(void)			{ return m_wndEdit1_25.getText(); }
	void setPhoneWork(LPCTSTR v)			{ m_wndEdit1_25.SetWindowText(v); }
	CString getPhoneHome(void)			{ return m_wndEdit1_26.getText(); }
	void setPhoneHome(LPCTSTR v)			{ m_wndEdit1_26.SetWindowText(v); }
	CString getMobile(void)					{ return m_wndEdit1_27.getText(); }
	void setMobile(LPCTSTR v)				{ m_wndEdit1_27.SetWindowText(v); }
	// General Object info...
	CString getObjectName(void)			{	return m_wndEdit1_2.getText(); }
	void setObjectName(LPCTSTR v)		{	m_wndEdit1_2.SetWindowText(v); }
	CString getObjectID(void)				{ return m_wndEdit1_3.getText(); }
	void setObjectID(LPCTSTR v)			{ m_wndEdit1_3.SetWindowText(v); }
	CString getErrandNum(void)			{ return m_wndEdit1_1.getText(); }
	void setErrandNum(LPCTSTR v)			{ m_wndEdit1_1.SetWindowText(v); }
	CString getLineLittra(void)			{ return m_wndEdit1_4.getText(); }
	void setLineLittra(LPCTSTR v)		{ m_wndEdit1_4.SetWindowText(v); }
//	CString getGrowthArea(void)			{ return m_wndEdit1_5.getText(); }
//	void setGrowthArea(LPCTSTR v)		{ m_wndEdit1_5.SetWindowText(v); }
//	double getTranspDist(void)			{ return 0.0; } //NOT USED 2008-04-09 P�D m_wndEdit1_12.getFloat(); }
//	void setTranspDist(double v)		{ m_wndEdit1_12.setFloat(v,0); }
//	int getLatitude(void)						{ return m_wndEdit1_13.getInt(); }
//	void setLatitude(int v)					{ m_wndEdit1_13.setInt(v); }
//	int getAltitude(void)						{ return m_wndEdit1_14.getInt(); }
//	void setAltitude(int v)					{ m_wndEdit1_14.setInt(v); }
/* Commented out 2009-04-03 P�D
	Not used, uses setting in PropertyGrid; 090403 p�d
	CString getTypeOfInfring(void)	
	{ 
		int nIndex = m_wndCBox1.GetCurSel();
		CString sIndex;
		if (nIndex != CB_ERR)
		{
			sIndex.Format(_T("%d"),nIndex);
			return sIndex;
		}
		return _T("-1");
	}
	void setTypeOfInfring(LPCTSTR v)	
	{ 
		int nIndex = atoi(v);
		m_wndCBox1.SetCurSel(nIndex);
	}
*/
	CString getDoneBy(void)					{ return m_wndEdit1_10.getText(); }
	void setDoneBy(LPCTSTR v)				{ m_wndEdit1_10.SetWindowText(v); }
	CString getStartDate(void)			{ return m_wndEdit1_11.getText(); }
	void setStartDate(LPCTSTR v)		{ m_wndEdit1_11.SetWindowText(v); }

	CString getReturnName()								{ return m_wndEdit1_16.getText(); }
	void setReturnName(LPCTSTR v)					{ m_wndEdit1_16.SetWindowText(v); }
	int getReturnContactID(void)					{ return m_wndEdit1_16.getItemIntData(); }
	void setReturnContactID(int id)				{ m_wndEdit1_16.setItemIntData(id); }

	CString getReturnAddress()						{ return m_wndEdit1_17.getText(); }
	void setReturnAddress(LPCTSTR v)				{ m_wndEdit1_17.SetWindowText(v); }
	CString getReturnPostNumber()					{ return m_wndEdit1_18.getText(); }
	void setReturnPostNumber(LPCTSTR v)		{ m_wndEdit1_18.SetWindowText(v); }
	CString getReturnPostAddress()				{ return m_wndEdit1_19.getText(); }
	void setReturnPostAddress(LPCTSTR v)		{ m_wndEdit1_19.SetWindowText(v); }

	// "Noteringar"; 080204 p�d
	CString getNotes(void)				{ return m_wndEdit1_21.getText(); }

	int getActiveObjectID_pk(void)	{		return m_recActiveObject.getObjID_pk();	}

	CTransaction_elv_object &getActiveObjectRecord(void)	{		return m_recActiveObject;	}

	void doPopulateData(UINT idx)
	{
		int nOldId = m_recActiveObject.getObjID_pk();

		m_nDBIndex = idx;
		populateData();

		// create a log entry that we changed object (#3877)
		if( nOldId != m_recActiveObject.getObjID_pk() )
		{
			if(m_bLogObject == TRUE && m_pDBadmin != NULL && m_pDB != NULL)
			{
				TCHAR szDb[128];
				GetUserDBInRegistry(szDb);

				CElv_object_log log = CElv_object_log();
				log.setObjectId( m_recActiveObject.getObjID_pk() );
				log.setObjectName( m_recActiveObject.getObjectName() );
				log.setType( LOG_CHANGE_OBJECT );
				log.setUser( getUserName() );
				log.setDb( szDb );
				m_pDBadmin->addObjectLog(log);
			}
		}
	}

	void doPopulateDataDBiD(UINT idx)
	{
		for(int i=0;i<m_vecObjectIndex.size();i++) {
			if(m_vecObjectIndex[i]==idx) {
				doPopulateData(i);
				break;
			}
		}
	}

	void doPopulateLastObject(void)
	{
		// Always reload data from database, before running this method; 090330 p�d
		getObjectIndexFromDB();

		BOOL bFound = FALSE;
		// Find ObjectID match; 090122 p�d
		if (m_vecObjectIndex.size() > 0)
		{
			for (UINT i = 0;i < m_vecObjectIndex.size();i++)
			{
				if (m_vecObjectIndex[i] == m_nLastVisitedObjectID)
				{
					m_nDBIndex = i;
					bFound = TRUE;
					break;
				}
			}
		}
		if (bFound)	populateData();
	}

	void doPopulateLastObject(int obj_id)
	{
		BOOL bFound = FALSE;
		// Find ObjectID match; 090122 p�d
		if (m_vecObjectIndex.size() > 0)
		{
			for (UINT i = 0;i < m_vecObjectIndex.size();i++)
			{
				if (m_vecObjectIndex[i] == obj_id)
				{
					m_nDBIndex = i;
					bFound = TRUE;
					break;
				}
			}
		}
		if (bFound)	populateData();
	}

	//Lagt till funktion f�r att �pnna ett objekt och visa en fastighet id et objektet Feature #2590,#2529 20111125 J�
	void doPopulateObjectAndProperty(int obj_id,int prop_id);

	void doPopulateFromLastAddedObject(void)
	{	
		// Always reload data from database, before running this method; 100922 p�d
		getObjectIndexFromDB();
		if (m_vecObjectIndex.size() > 0)
		{
			m_nDBIndex = m_vecObjectIndex.size() - 1;
			populateData();
		}		
	}

	void doSetNavigationButtons();

	int getObjIndex(void)	{ return m_nDBIndex; }

	BOOL doSaveObject(int show_msg_level)	{	return saveObject(show_msg_level);	}

	int getNumOfItems(void) { return (int)m_vecObjectIndex.size(); }

	// Added 2009-03-31 P�D
	void createObjectFromGIS(int *properties);

	void _populateP30Table()	{ populateP30Table(); }

	void setObjectStatusLbl(int status);	//new #3385
	void setObjectStatus(int status);	//new #3385
	void setShowObjectStatus(int status);	//new #3385
	int getShowObjects(void) { return m_nShowObjects; }	//new #3385

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageTwoFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDI1950ForrestNormFrame)
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd *);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnAddContractor();
	afx_msg void OnBnClickedBtnShowContractor();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedBtnAddLogo();
	afx_msg void OnBnClickedBtnDelLogo();
	afx_msg void OnBnClickedBtnDelContractor();
	afx_msg void OnBnClickedBtnP30Settings();
	afx_msg void OnBnClickedBtnDelRetadr();
};


