#pragma once

#include "Resource.h"

// CHandleLogBookDlg dialog

class CHandleLogBookDlg : public CDialog
{
	DECLARE_DYNAMIC(CHandleLogBookDlg)

	CMyExtStatic m_wndLbl1;
	CXTListCtrl m_listCtrl;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CString m_sPath;
public:
	CHandleLogBookDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHandleLogBookDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG30 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
