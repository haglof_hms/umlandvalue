#if !defined(AFX_CREATETABLES_H)
#define AFX_CREATETABLES_H

#include "StdAfx.h"
// CreateTables in Database; 080630 p�d

// "Ers�tter sql-script"
//***************************************************************************************************
//�ndrat �rendenummer till 50 tecken 20120601 J� #3116
const LPCTSTR table_ElvObject = _T("CREATE TABLE dbo.%s (\
													object_id int NOT NULL IDENTITY(1,1),\
													object_errand_num nvarchar(50),		/* �rendenummer */\
													object_name nvarchar(50),			/* Objektsnamn */\
													object_id_number nvarchar(10),		/* Objektid */\
													object_littra nvarchar(10),			/* Ledningslittra */\
													object_growth_area nvarchar(5),		/* Tillv�xtomr�de */\
													object_transport_dist float,		/* Transportavst�nd i m */\
													object_latitude int,				/* Breddgrad */\
													object_altitude int,				/* H�jd �ver havet i meter */\
													object_type_infring nvarchar(30),	/* Typ av intr�n (ex. breddning) */\
													object_width_present1 float,		/* Nuvarande bredd 1 */\
													object_width_present2 float,		/* Nuvarande bredd 2 */\
													object_width_added1 float,			/* Tillkommande bredd 1 */\
													object_width_added2 float,			/* Tillkommande bredd 2 */\
													object_type_of_net int,				/* Typ av Regionalt eller Lokalt n�t */\
													object_price_div_1 float,			/* Prisf�rdelning i gatan */\
													object_price_div_2 float,			/* Prisf�rdelning kanttr�d */\
													object_price_base float,			/* Prisbasbelopp;ers�ttning frivillig uppg�relse */\
													object_max_percent float,			/* Max procent; ers�ttning frivillig uppg�relse */\
													object_procent float,				/* Procentsata f�r; ers�ttning frivillig uppg�relse */\
													object_VAT float,					/* MOMS */\
													object_use_norm_id int,				/* ID F�r f�r ber�kning intr�ngsers�ttning */\
													object_use_norm nvarchar(40),		/* Anv�nd norm f�r ber�kning ex. 1950-�rs skogsnorm */\
													object_take_care_of float,			/* Tillvaratagande procent */\
													object_corr_factor float,				/* Correktionsfaktor (v�rdering) */\
													object_pricelist_name nvarchar(50),	/* Name of pricelist */\
													object_pricelist_typeof int,		/* Typ av prislista 1=Estimate prislista,2=Medelpris */\
													object_pricelist_xml ntext,			/* Prislista i XML-format (BLOB) */\
													object_costst_name nvarchar(50),		/* Name of cost taemplate */\
													object_costst_typeof int,			/* Typ av kostnad 1=Estimate,2=Intr�ng */\
													object_costs_xml ntext,				/* Kostnader i XML-format (BLOB) */\
													object_contractor_id int,			/* ID (Primarykey) f�r uppdragsgivare i kontakter */\
													object_dcls float,					/* Diameterklass */\
													object_extra_info nvarchar(50),		/* Extra info. ex. inst�llningar S�derbergs */\
													object_p30_name nvarchar(50),		/* Namn p� P30-mallen */\
													object_p30_typeof int,				/* Typ av mall (1=P30) */\
													object_p30_xml ntext,				/* p30 pris xml-mall */\
													object_hcost_name nvarchar(50),		/* Namn p� P30-mallen */\
													object_hcost_typeof int,			/* Typ av mall (2=F�rdyrad avverkning) */\
													object_hcost_xml ntext,				/* F�rdyrad avverkning xml-mall */\
													object_use_vat smallint,			/* TRUE/FALSE 1/0 Anv�nd Moms f�r Rotpost */\
													object_use_voluntary_deal smallint,	/* TRUE/FALSE 1/0 Anv�nd Frivillig uppg�relse */\
													object_use_higher_costs smallint,	/* TRUE/FALSE 1/0 Anv�nd F�rdyrad avverkning */\
													object_use_other_comp smallint,		/* TRUE/FALSE 1/0 Anv�nd Annan ers�ttning */\
													object_created_by nvarchar(20),		/* Skapad av */\
													object_return_id int,				/* ID f�r kontakt, som returadress */\
													object_notes ntext,					/* Noteringar */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													object_length float,			/* L�ngd p� str�cka (km) */\
													object_img varbinary(MAX),\
													object_logo_id int, /* ID f�r logo, h�mtas fr�n kontakter*/\
													object_use_grot smallint,		/* TRUE/FALSE 1/0 Anv�nd GROT */\
													object_start_date nvarchar(15),		/* Startdatum f�r Objekt */\
													object_recalc_by float,			/* R�kna upp Intr�ng procent */\
													object_status int,				/* Status p� objektet, 0=p�g�ende, 1=slutf�rd, #3385*/\
													object_perc_of_price_base float,		/* Procent av prisbasbelopp,frivillig uppg�relse #4420 20150803 J� */\
													object_parallel_width float,			/* Parallell bredd #4829 PL */\
													PRIMARY KEY (object_id));");
//***************************************************************************************************
const LPCTSTR table_ElvProperties = _T("CREATE TABLE dbo.%s (\
													prop_id int NOT NULL,\
													prop_object_id int NOT NULL,\
													prop_name nvarchar(50) NULL,\
													prop_number nvarchar(30) NULL,\
													prop_volume_m3sk float NULL,\
													prop_areal float NULL,\
													prop_numof_trees int NULL,\
													prop_numof_stands int NULL,\
													prop_wood_volume float NULL,\
													prop_wood_value float NULL,\
													prop_cost_value float NULL,\
													prop_rotpost_value float NULL,\
													prop_land_value float NULL,\
													prop_early_cut_value float NULL,\
													prop_storm_dry_value float NULL,\
													prop_randtrees_value float NULL,\
													prop_voluntary_deal_value float NULL,\
													prop_high_cost_volume float NULL,\
													prop_high_cost_value float NULL,\
													prop_other_comp_value float NULL,\
													prop_status tinyint NULL,\
													prop_status2 tinyint NULL,\
													prop_group_id nvarchar(20),			/* Identitet f�r att grupera fastigheter, ex. mark�gare */\
													prop_notify_date nvarchar(15),		/* Datum f�r Avisering */\
													prop_comp_offer_date nvarchar(15),	/* Datum f�r Ers�ttningserbjudande */\
													prop_pay_date nvarchar(15),			/* Datum f�r Utbetalning */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													prop_type_of_action tinyint NULL,	/* Markerar TRUE = F�rs�ljning, FALSE = Tillvaratagande */\
													prop_sort_order int NULL,	/* L�gga upp en sorteringsordning */\
													prop_grot_volume float NULL,	/* Grot volym (ton) */\
													prop_grot_value float NULL,	/* Grot v�rde (kr) */\
													prop_grot_cost float NULL,	/* Grot kostnad (kr) */\
													prop_coord NVARCHAR(80), /* #3734 koordinat */ \
													PRIMARY KEY (prop_id,prop_object_id),\
													CONSTRAINT fk_elv_prop\
													FOREIGN KEY (prop_object_id)\
													REFERENCES elv_object_table (object_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
//***************************************************************************************************
const LPCTSTR table_ElvPropertiesLog = _T("CREATE TABLE dbo.%s (\
													plog_id int NOT NULL,				/* Point to ID in fst_property table */\
													plog_object_id int NOT NULL,\
													plog_log_date nvarchar(25) NOT NULL,	/* Datum f�r log (ing�r i primary key) */\
													plog_added_by nvarchar(40),			/* Signatur (namn) */\
													plog_log_text ntext,				/* Log text */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													plog_type smallint,\
													PRIMARY KEY (plog_id,plog_object_id,plog_log_date),\
													CONSTRAINT fk_elv_plog\
													FOREIGN KEY (plog_id,plog_object_id)\
													REFERENCES elv_properties_table (prop_id,prop_object_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
//***************************************************************************************************
const LPCTSTR table_ElvPropertiesOtherComp = _T("CREATE TABLE dbo.%s (\
													ocomp_id int NOT NULL,				/* Point to ID in fst_property table */\
													ocomp_object_id int NOT NULL,\
													ocomp_prop_id int NOT NULL,			/* Fastighetens id */\
													ocomp_value_entered float,			/* Pris, kompensation inl�st */\
													ocomp_value_calc float,				/* Pris, kompensation utr�knat (Intr�ngsers�ttning) */\
													ocomp_description nvarchar(255),		/* Anteckning, f�rklaring av kompensation */\
													ocomp_typeof smallint,				/* Typ av kompensation; Fast ers�ttning,Intr�ngsers�ttning */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													PRIMARY KEY (ocomp_id,ocomp_object_id,ocomp_prop_id),\
													CONSTRAINT fk_elv_ocomp\
													FOREIGN KEY (ocomp_prop_id,ocomp_object_id)\
													REFERENCES elv_properties_table (prop_id,prop_object_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
//***************************************************************************************************
const LPCTSTR table_ElvEvaluation = _T("CREATE TABLE dbo.%s (\
													eval_id int NOT NULL,				/*  */\
													eval_object_id int NOT NULL,		/* Points to Object */\
													eval_prop_id int NOT NULL,			/* Points to Property */\
													eval_name nvarchar(50),				/* Namn p� V�rdering */\
													eval_areal float,					/* Areal (ha) */\
													eval_age int,						/* �lder */\
													eval_si_h100 nvarchar(5),			/* SI H100 (dm) */\
													eval_corr_factor float,				/* Korrektionsfaktor */\
												/* TGL (Tr�dslagsf�rdelning) */\
													eval_t_part float,					/* F�rdelning Tall */\
													eval_g_part float,					/* F�rdelning Gran */\
													eval_l_part float,					/* F�rdelning L�v */\
												/* Typ av skog; ex. Skogsmark,Kalmark */\
													eval_forrest_type smallint,			/* Index f�r skogstyp 0=Skogsmark (Default), 1=Kalmark */\
													eval_forrest_type_name nvarchar(15),	/* Namn p� skogstyp */\
												/* Typ av best�nd; V�rdering,NOLL-v�rdering */\
													eval_typeof_vstand smallint,		/* Index f�r typ; 0=V�rdering, 1=NOLL-best�nd */\
												/* Summa av v�rdering */\
													eval_land_value_ha float,			/* Markv�rde per ha */\
													eval_early_cutting_ha float,		/* F�rtidig avverkning per ha */\
													eval_land_value_stand float,		/* Markv�rde per best�nd */\
													eval_early_cutting_stand float,		/* F�rtidig avverkning per best�nd */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													eval_volume float,					/* Kan anv�ndas f�r snabbv�rdering dvs. ingen taxering */\
													eval_overlapping_land bit,			/* �verlappande mark */\
													eval_tillf_uttnyttjande bit,			/* Tillf�lligt utnyttjande */\
													eval_info_precut_p30_pine float,\
													eval_info_precut_p30_spruce float,\
													eval_info_precut_p30_birch float,\
													eval_info_precut_andel_pine float,\
													eval_info_precut_andel_spruce float,\
													eval_info_precut_andel_birch float,\
													eval_info_precut_ers_pine float,\
													eval_info_precut_ers_spruce float,\
													eval_info_precut_ers_birch float,\
													eval_info_precut_reduced float,\
													eval_info_precut_corrfact float\
													PRIMARY KEY (eval_id,eval_object_id,eval_prop_id),\
													CONSTRAINT fk_elv_eval\
													FOREIGN KEY (eval_prop_id,eval_object_id)\
													REFERENCES elv_properties_table (prop_id,prop_object_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
//***************************************************************************************************
//
const LPCTSTR table_ElvCruise = _T("CREATE TABLE dbo.%s (\
													ecru_id int NOT NULL,				/* Pekar p� Trakt ID i esti_trakt_table */\
													ecru_object_id int NOT NULL,		/* Points to Object */\
													ecru_prop_id int NOT NULL,			/* Points to Property */\
													ecru_number nvarchar(20),			/* Trakt-nummer */\
													ecru_name nvarchar(50),				/* Namn p� Trakt */\
													ecru_numof_trees int,				/* Summa; Antal tr�d p� trakt */\
													ecru_areal float,					/* Areal i ha */\
													ecru_m3sk_vol float,				/* Summa; Volym m3sk p� trakt */\
													ecru_timber_volume float,			/* Gagnvirkesvolym m3fub */\
													ecru_timber_value float,			/* Virkesv�rde */\
													ecru_timber_cost float,				/* Kostnader */\
													ecru_rotnetto float,				/* ROTNETTO */\
													ecru_storm_dry_vol float,			/* Volym; storm och tork */\
													ecru_storm_dry_value float,			/* V�rde; storm och tork */\
													ecru_storm_dry_spruce_mix float,	/* Graninblandning i Procent */\
													ecru_avg_price_factor float,		/* Medelprisfaktor */\
													ecru_randtrees_volume float,		/* SUMMA Volym; kanttr�d */\
													ecru_randtrees_value float,			/* SUMMA V�rde; kanttr�d */\
													ecru_use_sample_trees smallint default(0),	/* Anv�nds f�r att indikera vilken skogsnorm som valts */\
													ecru_do_reduce_rotpost smallint default(1),	/* Ja/Nej om reducering av rotpostpriset */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													ecru_cruise_type smallint, /* Type of Cruise 0 = From rotpost ( 1 = Manually entered - NOT USED from 2010-05-03 P�D) */\
													ecru_width float, /* Other width than on Object; for 'Breddning' */\
													ecru_si nvarchar(5),				/* St�ndortsindex; samma som p� Trakt */\
													ecru_tgl nvarchar(20),				/* Tr�dslagsblandning f�r manuellt inl�sta best�nd */\
													ecru_grot_volume float,			/* Grot volym (ton) */\
													ecru_grot_value float,			/* Grot v�rde (kr) */\
													ecru_grot_cost float,			/* Grot kostnad (kr) */\
													ecru_use_for_infr smallint,		/* Ber�kning av Intr�ng;  1 = Ing�r ej i intr�ng, 0 = Ing�r i intr�ng */\
													ecru_tillf_uttnyttjande bit 	/* f�r ber�kning av markv�rde ja 30% av markv�rde */ \
													PRIMARY KEY (ecru_id,ecru_object_id,ecru_prop_id),\
													CONSTRAINT fk_elv_ecru\
													FOREIGN KEY (ecru_prop_id,ecru_object_id)\
													REFERENCES elv_properties_table (prop_id,prop_object_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
//***************************************************************************************************
//
/*
const LPCTSTR table_ElvMCruise = _T("CREATE TABLE dbo.%s (\
													mecru_id int NOT NULL,					/* Pekar p� Trakt ID i esti_trakt_table * /\
													mecru_object_id int NOT NULL,		/* Points to Object * /\
													mecru_prop_id int NOT NULL,			/* Points to Property * /\
													mecru_number nvarchar(20),			/* Trakt-nummer * /\
													mecru_name nvarchar(50),				/* Namn p� Trakt * /\
													mecru_m3sk_vol float,						/* Summa; Volym m3sk p� trakt * /\
													mecru_storm_dry_vol float,			/* Volym; storm och tork * /\
													mecru_storm_dry_value float,		/* V�rde; storm och tork * /\
													mecru_storm_dry_spruce_mix float,	/* Graninblandning i Procent * /\
													mecru_si nvarchar(5),						/* St�ndortsindex; samma som p� Trakt * /\
													mecru_tgl nvarchar(20),					/* Tr�dslagsblandning f�r manuellt inl�sta best�nd * /\
													mecru_cruise_type smallint,			/* Type of Cruise 0 = Manually entered * /\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													updated datetime NOT NULL default CURRENT_TIMESTAMP,\
													PRIMARY KEY (mecru_id,mecru_object_id,mecru_prop_id),\
													CONSTRAINT fk_elv_mecru\
													FOREIGN KEY (mecru_prop_id,mecru_object_id)\
													REFERENCES elv_properties_table (prop_id,prop_object_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
*/
//***************************************************************************************************
/* LandValue evaluation rand trees */
const LPCTSTR table_ElvCruiseRandtrees = _T("CREATE TABLE dbo.%s (\
													erand_id int NOT NULL,				/*  */\
													erand_object_id int NOT NULL,		/* Points to Object */\
													erand_prop_id int NOT NULL,			/* Points to Property */\
													erand_ecru_id int NOT NULL,		/* Points to Cruise table */\
													erand_spc_id int,					/* ID f�r tr�dslag */\
													erand_spc_name nvarchar(50),			/* Namn p� Tr�dslag */\
													erand_volume float,					/* Volym f�r kanttr�d/tr�dslag */\
													erand_value float,					/* V�rde f�r kanttr�d/tr�dslag */\
													created datetime NOT NULL default CURRENT_TIMESTAMP,\
													PRIMARY KEY (erand_id,erand_object_id,erand_prop_id,erand_ecru_id),\
													CONSTRAINT fk_elv_erand\
													FOREIGN KEY (erand_ecru_id,erand_object_id,erand_prop_id)\
													REFERENCES elv_cruise_table (ecru_id,ecru_object_id,ecru_prop_id)\
													ON DELETE CASCADE\
													ON UPDATE CASCADE);");
//***************************************************************************************************
/* Tabell f�r mallar till intr�ngsv�rdering */
/* Mall f�r P30 priser och mall f�r "F�rdyrad avverkning" */
/* OBS! Frist�ende tabll, inga relationer till ex. elv_object_table */
const LPCTSTR table_ElvTemplate = _T("CREATE TABLE dbo.%s (\
												 id INT NOT NULL IDENTITY,\
												 name nvarchar(50),\
												 type_of int,				/* Idetifies type of tamplate ex. 1 = P30, 2 = F�rdyrad avverkning */\
												 template NTEXT,			/* Template in xml format */\
												 notes NTEXT,				/* Notes */\
												 created_by nvarchar(20),\
												 created datetime NOT NULL default CURRENT_TIMESTAMP,\
												 PRIMARY KEY (ID));");
//***************************************************************************************************
/* Tabell f�r dokumant-mallar */
/* Ex. dokumant-mall f�r text till Aviseringar */
const LPCTSTR table_ElvDocumentTemplate = _T("CREATE TABLE dbo.%s (\
												 id INT NOT NULL IDENTITY,\
												 name nvarchar(50),\
												 type_of int,				/* Idetifies type of tamplate ex. 1 = P30, 2 = F�rdyrad avverkning */\
												 template NTEXT,			/* Template in xml format */\
												 created_by nvarchar(20),\
												 created datetime NOT NULL default CURRENT_TIMESTAMP,\
												 PRIMARY KEY (ID));");

//***************************************************************************************************
/* Tabell f�r dokumant-mallar */
/* Ex. dokumant-mall f�r text till Aviseringar */
const LPCTSTR table_ElvPropertyStatusSVE = _T("CREATE TABLE dbo.%s (\
												 id INT NOT NULL,\
												 order_num tinyint,			/* Ordernummer */\
												 name nvarchar(50),			/* Name of Status */\
												 type_of tinyint,				/* Idetifies type of status 1 = Can recalculate, 0 = No recalclate */\
												 r_color tinyint,				/* Red intensity 0 - 255 */\
												 g_color tinyint,				/* Green intensity 0 - 255 */\
												 b_color tinyint,				/* Blue intensity 0 - 255 */\
												 created datetime NOT NULL default CURRENT_TIMESTAMP,\
												 PRIMARY KEY (ID));\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(0,1,'Under behandling',1,255,255,255);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(1,2,'Aviserad',1,244,238,224);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(2,3,'Ers�ttningserbjudande',0,255,165,	0);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(3,4,'F�rs�ljning',0,95,158,160);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(4,5,'Egen regi',0,205, 92, 92);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(5,6,'Utbetalning',0,255,255,0);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(6,7,'KLAR',0,154,205, 50);");

const LPCTSTR table_ElvPropertyStatusENU = _T("CREATE TABLE dbo.%s (\
												 id INT NOT NULL,\
												 order_num tinyint,			/* Ordernummer */\
												 name nvarchar(50),			/* Name of Status */\
												 type_of tinyint,				/* Idetifies type of status 1 = Can recalculate, 0 = No recalclate */\
												 r_color tinyint,				/* Red intensity 0 - 255 */\
												 g_color tinyint,				/* Green intensity 0 - 255 */\
												 b_color tinyint,				/* Blue intensity 0 - 255 */\
												 created datetime NOT NULL default CURRENT_TIMESTAMP,\
												 PRIMARY KEY (ID));\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(0,1,'Working',1,255,255,255);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(1,2,'Announced',1,244,238,224);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(2,3,'Compensation offer',0,255,165,	0);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(3,4,'Selling',0,95,158,160);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(4,5,'Own handling',0,205, 92, 92);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(5,6,'Payment',0,255,255,0);\
INSERT INTO elv_property_status_table (id,order_num,name,type_of,r_color,g_color,b_color) values(6,7,'DONE',0,154,205, 50);");

// #3877
const LPCTSTR table_ElvObjectLog = _T("CREATE TABLE dbo.%s (")
 _T("olog_created datetime NOT NULL default CURRENT_TIMESTAMP, ")
 _T("olog_object_id int NOT NULL, ")
 _T("olog_object_name nvarchar(50), ")
 _T("olog_type int NOT NULL, ")
 _T("olog_user nvarchar(50), ")
 _T("olog_db nvarchar(50), ")
 _T("PRIMARY KEY (olog_created) ")
_T(");");


//***************************************************************************************************
/* Alter Table scripts */
//***************************************************************************************************
// This script also does a check to see if column already exists; 090423 p�d
const LPCTSTR alter_ElvPropertyLog = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'plog_type')  \
																				 alter table %s add plog_type smallint");
//***************************************************************************************************
/* Alter Table scripts */
//***************************************************************************************************


const LPCTSTR alter_ElvObjectTable_ParallelWidth = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'object_parallel_width') \
																				 alter table %s add object_parallel_width float");

const LPCTSTR alter_ElvObjectTable_PerOfPriceBase_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'object_perc_of_price_base') \
																				 alter table %s add object_perc_of_price_base float");

const LPCTSTR alter_ElvObjectTable_PerOfPriceBase_2 = _T("update %s set %s.object_perc_of_price_base = 3 where object_perc_of_price_base is null");



// This script also does a check to see if column already exists; 090423 p�d
const LPCTSTR alter_ElvObjectTable = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'object_img')  \
																				 alter table %s add object_img varbinary(MAX)");

// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvObjectTable090528_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'object_vfall_break')  \
																							alter table %s add object_vfall_break float");
// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvObjectTable090528_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'object_vfall_factor')  \
																							alter table %s add object_vfall_factor float");

// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvCruiseTable090625_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ecru_cruise_type')  \
																							alter table %s add ecru_cruise_type smallint");

// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvCruiseTable090625_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'ecru_width')  \
																							alter table %s add ecru_width float");

// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvCruiseTable090904_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_si')  \
																								 alter table %s add ecru_si nvarchar(5)");
// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvCruiseTable090908_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_tgl')  \
																								 alter table %s add ecru_tgl nvarchar(20)");
// This script also does a check to see if column already exists; 090914 p�d
const LPCTSTR alter_ElvPropertyTable090914_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'prop_type_of_action')  \
																								 alter table %s add prop_type_of_action tinyint");
// This script also does a check to see if column already exists; 091009 p�d
const LPCTSTR alter_ElvPropertyTable091009_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'prop_sort_order')  \
																								 alter table %s add prop_sort_order int");

// This script also does a check to see if column already exists; 100225 p�d
const LPCTSTR alter_ElvObjectTable100225_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'object_logo_id')  \
																								 alter table %s add object_logo_id int");

// This script also does a check to see if column already exists; 100225 p�d
const LPCTSTR alter_ElvObjectTable100323_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'object_use_grot')  \
																								 alter table %s add object_use_grot smallint");

// This script also does a check to see if column already exists; 100323 p�d
const LPCTSTR alter_ElvCruiseTable100323_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_grot_volume')  \
																								 alter table %s add ecru_grot_volume float");

// This script also does a check to see if column already exists; 100323 p�d
const LPCTSTR alter_ElvCruiseTable100323_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_grot_value')  \
																								 alter table %s add ecru_grot_value float");

// This script also does a check to see if column already exists; 100323 p�d
const LPCTSTR alter_ElvPropertyTable100323_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'prop_grot_volume')  \
																									 alter table %s add prop_grot_volume float");

// This script also does a check to see if column already exists; 100323 p�d
const LPCTSTR alter_ElvPropertyTable100323_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'prop_grot_value')  \
																									 alter table %s add prop_grot_value float");

// This script also does a check to see if column already exists; 100323 p�d
const LPCTSTR alter_ElvPropertyTable100517_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'prop_grot_cost')  \
																									 alter table %s add prop_grot_cost float");

// This script also does a check to see if column already exists; 100323 p�d
const LPCTSTR alter_ElvCruiseTable100517_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_grot_cost')  \
																								 alter table %s add ecru_grot_cost float");

// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvObjectTable100610_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'object_start_date')  \
																							alter table %s add object_start_date nvarchar(15)");

// This script also does a check to see if column already exists; 090528 p�d
const LPCTSTR alter_ElvObjectTable100615_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'object_recalc_by')  \
																							alter table %s add object_recalc_by float");

// This script also does a check to see if column already exists; 100702 p�d
const LPCTSTR alter_ElvCruiseTable100702_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_use_for_infr')  \
																								 alter table %s add ecru_use_for_infr smallint");

const LPCTSTR alter_ElvCruiseTable190327 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_tillf_uttnyttjande')  \
																								 alter table %s add ecru_tillf_uttnyttjande bit");


const LPCTSTR alter_ElvEvalutationTable191126_01= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_p30_pine')  \
													alter table %s add eval_info_precut_p30_pine float");
const LPCTSTR alter_ElvEvalutationTable191126_02= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_p30_spruce')  \
													alter table %s add eval_info_precut_p30_spruce float");
const LPCTSTR alter_ElvEvalutationTable191126_03= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_p30_birch')  \
													alter table %s add eval_info_precut_p30_birch float");
const LPCTSTR alter_ElvEvalutationTable191126_04= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_andel_pine')  \
													alter table %s add eval_info_precut_andel_pine float");
const LPCTSTR alter_ElvEvalutationTable191126_05= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_andel_spruce')  \
													alter table %s add eval_info_precut_andel_spruce float");
const LPCTSTR alter_ElvEvalutationTable191126_06= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_andel_birch')  \
													alter table %s add eval_info_precut_andel_birch float");
const LPCTSTR alter_ElvEvalutationTable191126_07= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_ers_pine')  \
													alter table %s add eval_info_precut_ers_pine float");
const LPCTSTR alter_ElvEvalutationTable191126_08= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_ers_spruce')  \
													alter table %s add eval_info_precut_ers_spruce float");
const LPCTSTR alter_ElvEvalutationTable191126_09= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_ers_birch')  \
													alter table %s add eval_info_precut_ers_birch float");
const LPCTSTR alter_ElvEvalutationTable191126_10= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_reduced')  \
													alter table %s add eval_info_precut_reduced float");
const LPCTSTR alter_ElvEvalutationTable191126_11= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_info_precut_corrfact')  \
													alter table %s add eval_info_precut_corrfact float");


const LPCTSTR alter_ElvEvalutationTable200427_01= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_valuepine')  \
													alter table %s add eval_markinfo_valuepine float");
const LPCTSTR alter_ElvEvalutationTable200427_02= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_valuespruce')  \
													alter table %s add eval_markinfo_valuespruce float");
const LPCTSTR alter_ElvEvalutationTable200427_03= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_andel_pine')  \
													alter table %s add eval_markinfo_andel_pine float");
const LPCTSTR alter_ElvEvalutationTable200427_04= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_andel_spruce')  \
													alter table %s add eval_markinfo_andel_spruce float");

const LPCTSTR alter_ElvEvalutationTable200427_05= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_p30_pine')  \
													alter table %s add eval_markinfo_p30_pine float");
const LPCTSTR alter_ElvEvalutationTable200427_06= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_p30_spruce')  \
													alter table %s add eval_markinfo_p30_spruce float");
const LPCTSTR alter_ElvEvalutationTable200427_07= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
													where table_name = '%s' and column_name = 'eval_markinfo_reducedby')  \
													alter table %s add eval_markinfo_reducedby float");






//�ndrat �rendenummer till 50 tecken 20120601 J� #3116
const LPCTSTR alter_ElvObjectTable120601_1 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
												where table_name = '%s' and column_name = 'object_errand_num')  \
																								 alter table %s alter column object_errand_num nvarchar(50)");

// #3385 l�gger till kolumnen object_status
const LPCTSTR alter_ElvObjectTable121011_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'object_status')  \
																							alter table %s add object_status int");
//#3385 �ndrar v�rdena i kolumnen object_status fr�n NULL till 0
const LPCTSTR alter_ElvObjectTable121011_2 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																							where table_name = '%s' and column_name = 'object_status')  \
																							update %s set object_status = 0 where object_status is NULL");

const LPCTSTR alter_ElvObjectTable151210 = _T("ALTER TABLE elv_object_table ALTER COLUMN object_img varbinary(MAX)");

//HMS-119 ut�ka tecken p� object littra till 50
const LPCTSTR alter_ElvObjectTable20241210 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
												where table_name = '%s' and column_name = 'object_littra')  \
																								 alter table %s alter column object_littra nvarchar(50)");

// #3734 l�gga till koordinat i fastighetstabellen
const LPCTSTR alter_ElvPropertyTable120905_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'prop_coord')  \
																									 alter table %s add prop_coord NVARCHAR(80)");

const LPCTSTR alter_ElvEvalTable20150112_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_length')  \
																									 alter table %s add eval_length int");
const LPCTSTR alter_ElvEvalTable20150112_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_width')  \
																									 alter table %s add eval_width int");

const LPCTSTR alter_ElvEvalTable20160616 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_overlapping_land')  \
																									 alter table %s add eval_overlapping_land bit");

const LPCTSTR alter_ElvEvalTable20190326 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_tillf_uttnyttjande')  \
																									 alter table %s add eval_tillf_uttnyttjande bit");


		
const LPCTSTR alter_ElvEvalTable20160817_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_layer')  \
																									 alter table %s add eval_layer int");
const LPCTSTR alter_ElvEvalTable20160817_2 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_layer')  \
																									 update %s set eval_layer = 1 where eval_layer is null");
const LPCTSTR alter_ElvEvalTable20161005_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_side')  \
																									 alter table %s add eval_side int");
const LPCTSTR alter_ElvEvalTable20161005_2 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																									 where table_name = '%s' and column_name = 'eval_side')  \
																									 update %s set eval_side = 1 where eval_side is null");


const LPCTSTR alter_ElvCruiseTable20200414_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fsprucemix')  \
																								 alter table %s add ecru_sttork_fsprucemix float");

const LPCTSTR alter_ElvCruiseTable20200414_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fsumm3sk_inside')  \
																								 alter table %s add ecru_sttork_fsumm3sk_inside float");
const LPCTSTR alter_ElvCruiseTable20200414_3 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_favgpricefactor')  \
																								 alter table %s add ecru_sttork_favgpricefactor float");
const LPCTSTR alter_ElvCruiseTable20200414_4 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_ftakecareofperc')  \
																								 alter table %s add ecru_sttork_ftakecareofperc float");
const LPCTSTR alter_ElvCruiseTable20200414_5 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fpinep30price')  \
																								 alter table %s add ecru_sttork_fpinep30price float");
const LPCTSTR alter_ElvCruiseTable20200414_6 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fsprucep30price')  \
																								 alter table %s add ecru_sttork_fsprucep30price float");
const LPCTSTR alter_ElvCruiseTable20200414_7 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fbirchp30price')  \
																								 alter table %s add ecru_sttork_fbirchp30price float");

const LPCTSTR alter_ElvCruiseTable20200414_8 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fcompensationlevel')  \
																								 alter table %s add ecru_sttork_fcompensationlevel float");

const LPCTSTR alter_ElvCruiseTable20200414_9 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_fpineperc')  \
																								 alter table %s add ecru_sttork_fpineperc float");

const LPCTSTR alter_ElvCruiseTable20200414_10 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_spruceperc')  \
																								 alter table %s add ecru_sttork_spruceperc float");

const LPCTSTR alter_ElvCruiseTable20200414_11 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_birchperc')  \
																								 alter table %s add ecru_sttork_birchperc float");

const LPCTSTR alter_ElvCruiseTable20200414_12 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_sttork_widefactor')  \
																								 alter table %s add ecru_sttork_widefactor float");


const LPCTSTR alter_ElvCruiseTable20200427_01 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_valuepine')  \
																								 alter table %s add ecru_markinfo_valuePine float");


const LPCTSTR alter_ElvCruiseTable20200427_02 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_valuespruce')  \
																								 alter table %s add ecru_markinfo_valuespruce float");

const LPCTSTR alter_ElvCruiseTable20200427_03 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_andel_pine')  \
																								 alter table %s add ecru_markinfo_andel_pine float");

const LPCTSTR alter_ElvCruiseTable20200427_04 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_andel_spruce')  \
																								 alter table %s add ecru_markinfo_andel_spruce float");

const LPCTSTR alter_ElvCruiseTable20200427_05 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_p30pine')  \
																								 alter table %s add ecru_markinfo_p30pine float");

const LPCTSTR alter_ElvCruiseTable20200427_06 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_p30spruce')  \
																								 alter table %s add ecru_markinfo_p30spruce float");
const LPCTSTR alter_ElvCruiseTable20200427_07 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																								 where table_name = '%s' and column_name = 'ecru_markinfo_reducedby')  \
																								 alter table %s add ecru_markinfo_reducedby float");

#endif