#pragma once

#include "Resource.h"

// CProgressDlg dialog

class CProgressDlg : public CDialog
{
	DECLARE_DYNAMIC(CProgressDlg)

	CString m_s;
	CString m_sNumOfItemsTxt;
	CString m_sPropertyName;
	CString m_sStandName;
	CString m_sActiveAction;

	CStringArray m_sarrCaps;

	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;

	CProgressCtrl m_wndProgress_props;
	CProgressCtrl m_wndProgress_stand;

	CBrush m_brush;

	int m_nIsShowDlg;
public:
	CProgressDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDlg();

	inline void setCaption(LPCTSTR txt)	{ SetWindowText(txt);	}
	inline void setLbl2(LPCTSTR txt)	{	m_wndLbl2.SetWindowText(txt);	}
	inline void setLbl3(LPCTSTR txt)	{	m_wndLbl3.SetWindowText(txt);	}

	inline void setPropertyName(LPCTSTR txt)
	{
		m_sPropertyName = txt;
		m_wndLbl5.SetWindowTextW(m_sPropertyName);
		m_wndLbl6.SetWindowTextW(L"");
		//AfxMessageBox(L"Action " + m_sActiveAction + L"\n Propname " + m_sPropertyName);
	}

	inline void setStandName(LPCTSTR txt)
	{
		m_sStandName = txt;
		m_wndLbl6.SetWindowTextW(m_sStandName);
		//AfxMessageBox(L"Action " + m_sActiveAction + L"\nPropname " + m_sPropertyName + L"\n" + L"Standname " + m_sStandName);
	}

	inline void setActiveAction(LPCTSTR txt)
	{
		m_sActiveAction = txt;
		m_wndLbl7.SetWindowTextW(m_sActiveAction);
		m_wndLbl5.SetWindowTextW(L"");
		m_wndLbl6.SetWindowTextW(L"");
	}

	inline void setProgressPos_props(int step)
	{
		if (m_wndProgress_props.GetSafeHwnd() != NULL)
		{
			m_wndProgress_props.SetPos(step);
			m_wndProgress_props.Invalidate();
		}
	}

	inline void setProgressRange_props(int range)
	{
		if (m_wndProgress_props.GetSafeHwnd() != NULL)
		{
			m_wndProgress_props.SetRange(0,range);
			m_wndProgress_props.Invalidate();
		}
	}

	inline void setProgressPos_stands(int step)
	{
		if (m_wndProgress_stand.GetSafeHwnd() != NULL)
		{
			m_wndProgress_stand.SetPos(step);
		}
	}

	inline void setProgressRange_stands(int range)
	{
		if (m_wndProgress_stand.GetSafeHwnd() != NULL)
		{
			m_wndProgress_stand.SetRange(0,range);
		}
	}

	inline void hideStandInformation(void)
	{
		if (m_wndProgress_stand.GetSafeHwnd() != NULL)
		{
			m_wndProgress_stand.ShowWindow(SW_HIDE);
		}
		m_wndLbl3.ShowWindow(SW_HIDE);
		m_wndLbl6.ShowWindow(SW_HIDE);
		m_sPropertyName.Empty();
		m_sStandName.Empty();
	}

	inline void showStandInformation(void)
	{
		if (m_wndProgress_stand.GetSafeHwnd() != NULL)
		{
			m_wndProgress_stand.ShowWindow(SW_NORMAL);
		}
		m_wndLbl3.ShowWindow(SW_NORMAL);
		m_wndLbl6.ShowWindow(SW_NORMAL);
	}

	inline void show(int cmd_show)
	{
		if (GetSafeHwnd())
		{
			m_nIsShowDlg = cmd_show;
			ShowWindow(m_nIsShowDlg);
		}
		else
			m_nIsShowDlg = SW_HIDE;
	}

	inline BOOL isVisible(void)	
	{
		return (m_nIsShowDlg > 0);
	}

// Dialog Data
	enum { IDD = IDD_DIALOG23 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	DECLARE_MESSAGE_MAP()
};
