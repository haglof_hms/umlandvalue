#if !defined(__1950FORRESTNORMFRAME_H__)
#define __1950FORRESTNORMFRAME_H__

#include "UMLandValueDB.h"

#include "InfoDlgBar.h"

#include "ProgressDlg.h"

#include "HtmlCtrl.h"
#include "libxl.h"
using namespace libxl;
/////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 090129 p�d

class CACBox : public CComboBox
{
public:
	CACBox();

	void SetLblFont(int size,int weight,LPCTSTR font_name = _T("Arial"));

	vecSTDReports m_vecReports;
	void setSTDReportsInCBox(LPCTSTR shell_data_file,LPCTSTR add_to,int index);
	void setLanguageFN(LPCTSTR lng_fn);

protected:
	CFont *m_fnt1;
	CString m_sLangFN;

	CString getLangStr(int id);

	//{{AFX_MSG(CACBox)
	afx_msg void OnDestroy();
	afx_msg BOOL OnCBoxChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////
// CMyControlPopup
class CMenuItemsEnabled
{
public:
	short m_nItemIndex;
	CString m_sItemName;
	bool m_bEnabled;

	CMenuItemsEnabled()
	{
		m_nItemIndex = -1;
		m_sItemName = L"";
		m_bEnabled = true;
	}
	CMenuItemsEnabled(short index,LPCTSTR name,bool enabled)
	{
		m_nItemIndex = index;
		m_sItemName = name;
		m_bEnabled = enabled;
	}
};

typedef std::vector<CMenuItemsEnabled> vecMenuItemsEnabled;

//new #3385
class CSubMenuItemsEnabled
{
public:
	int m_nId;
	int m_nItemIndex;
	bool m_bEnabled;

	CSubMenuItemsEnabled()
	{
		m_nId = -1;
		m_nItemIndex = -1;
		m_bEnabled = true;
	}
	CSubMenuItemsEnabled(int id,int index,bool enabled)
	{
		m_nId = id;
		m_nItemIndex = index;
		m_bEnabled = enabled;
	}
};

typedef std::vector<CSubMenuItemsEnabled> vecSubMenuItemsEnabled;


class CMyControlPopup : public CXTPControlPopup
{
	DECLARE_XTP_CONTROL(CMyControlPopup)

	struct _menu_items
	{
		int m_nID;
		CString m_sText;
		int m_nSubMenuID;

		_menu_items(int id,LPCTSTR text,int sub_menu_id = 0 )
		{
			m_nID = id;
			m_sText = (text);
			m_nSubMenuID = sub_menu_id;
		}
	};

	std::vector<_menu_items > vecMenuItems;
	std::vector<_menu_items > vecSubMenuItems;
	vecMenuItemsEnabled m_vecMenuItemsEnabled;
	vecSubMenuItemsEnabled m_vecSubMenuItemsEnabled;	//new #3385

	BOOL isEnabled(LPCTSTR item_text,short *idx);
	void setSubMenuEnabled(int idx, CMenu* menu);	//new #3385
	void setSubMenuChecked(int idx, CMenu* menu);	//new #3385
protected:
	virtual BOOL OnSetPopup(BOOL bPopup);
public:
	CMyControlPopup(void);
	virtual ~CMyControlPopup(void);
	void addMenuIDAndText(int id = 0,LPCTSTR text = _T(""),int sub_menu_id = -999);
	void setSubMenuIDAndText(int id = 0,LPCTSTR text = _T(""),int sub_menu_id = -900);
	void setExcludedItems(vecMenuItemsEnabled &);
	void setExcludedSubMenuItems(vecSubMenuItemsEnabled &);	//new #3385
};

//////////////////////////////////////////////////////////////////////////
// CCustomItemChilds
class CCustomItemChilds : public CXTPPropertyGridItem
{
	class CCustomItemChildsPad;

	friend class CCustomItemChildsPad;

public:
	CCustomItemChilds(CString strCaption,CString childCap1,CString childCap2,double *bind_to1,double *bind_to2);

	void setValues(double *bind_to1,double *bind_to2);

	CCustomItemChildsPad GetItemWidth1();
	CCustomItemChildsPad GetItemWidth2();
	virtual void SetReadOnly(BOOL bReadOnly /* = TRUE */);

protected:
	virtual void OnAddChildItem();
	virtual void SetValue(CString strValue);

private:
	void UpdateChilds();
	CString ValueToString(double val1,double val2);

private:
	CCustomItemChildsPad* m_itemWidth1;
	CCustomItemChildsPad* m_itemWidth2;
	double &m_fBindTo1;
	double &m_fBindTo2;

	CString m_sChildCap1;
	CString m_sChildCap2;
	CString m_sFormat;
};


class CCustomItemChilds_2 : public CXTPPropertyGridItem
{
	class CMyPropGridItemBool;
	friend class CMyPropGridItemBool;
	class CMyPropGridItem;
	friend class CMyPropGridItem;

public:
	CCustomItemChilds_2(CString strCaption,
											CString childCap1,
											CString childCap2,
											CString childCap3,
											CString childCap4,
											CString childCap5,
											BOOL *bind_to1,
											BOOL *bind_to2,
											BOOL *bind_to3,
											BOOL *bind_to4,
											CString *bind_to5,
											CString bool_yes,
											CString bool_no);

	void setValues(BOOL *bind_to1,BOOL *bind_to2,BOOL *bind_to3,BOOL *bind_to4,CString *bind_to5);

protected:
	virtual void OnAddChildItem();
	virtual void SetValue(CString strValue);

private:
	void UpdateChilds();
	CString ValueToString(BOOL val1,BOOL val2,BOOL val3,BOOL val4,CString val5);

private:
	CMyPropGridItemBool* m_item1;
	CMyPropGridItemBool* m_item2;
	CMyPropGridItemBool* m_item3;
	CMyPropGridItemBool* m_item4;
	CMyPropGridItem* m_item5;

	BOOL &m_bBindTo1;
	BOOL &m_bBindTo2;
	BOOL &m_bBindTo3;
	BOOL &m_bBindTo4;
	CString &m_sBindTo5;

	CString m_sChildCap1;
	CString m_sChildCap2;
	CString m_sChildCap3;
	CString m_sChildCap4;
	CString m_sChildCap5;
	CString m_sFormat;
	CString m_sBoolYes;
	CString m_sBoolNo;
};



class CCustomItemButton;

class CInplaceButton : public CXTButton
{
public:
	DECLARE_MESSAGE_MAP()
protected:
	CCustomItemButton* m_pItem;
	void OnClicked();

	friend class CCustomItemButton;
};

class CCustomItemButton : public CXTPPropertyGridItem
{
protected:

public:
	CCustomItemButton(CString strCaption,CWnd *parent);

protected:
	virtual void SetVisible(BOOL bVisible);
	BOOL OnDrawItemValue(CDC& dc, CRect rcValue);
	void CreateButton();
	virtual void OnValueChanged(CString strValue);

private:
	CInplaceButton m_wndButton;
	CWnd *m_pParent;

	friend class CInplaceButton;
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc
/*
class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

*/

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CMDI1950ForrestNormFrame

class CMDI1950ForrestNormFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDI1950ForrestNormFrame)
		

//private:
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	//CProgressCtrl m_wndProgressCtrl;
	CInfoDlgBar m_wndInfoDlgBar;
	CProgressDlg m_wndProgressDlg;	// Added 100203 p�d

	CXTResizeGroupBox m_wndGeneralInfoGroup;

	CString m_sLangFN;

	CString m_sPropGridCap1;
	CString m_sPropGridCap2;
	CString m_sPropGridCap3;

	CString m_sSettingGridCap1;
	CString m_sSettingGridCap2; 
	CString m_sSettingGridCap3;
	CString m_sSettingGridCap4;

	CString m_sYes;
	CString m_sNo;

	CString m_sOK;
	CString m_sCancelDlg;

	CString m_sChangeTemplateMsg1;
	CString m_sChangeTemplateMsg2;
	
	CString m_sMissingFileForPrintOutMsg;

	CString m_sCalculateRotpost;
	CString m_sPricelist;
	CString m_sCosts;
	CString m_sDiameterClass;
	CString m_sSoderbergs;
	CString m_sIsAtCoast;
	CString m_sIsSouthEast;
	CString m_sIsRegion5;
	CString m_sIsPartOfPlot;
	CString m_sSIH100_pine;
	CString m_sCalculateInfring;
	
	CString m_sTypeOfInfring;
	CString m_sForrestNorm;
	CString m_sP30Price;
	CString m_sGrowthArea;
	CString m_sLatitude;
	CString m_sAltitude;
	CString m_sHighCost;
	CString m_sTakeCareOfPercent;
	CString m_sCorrectionFactor;
	CString m_sLengthOfObject;
	CString m_sCurrentWidth;
	CString m_sCurrentWidth1;
	CString m_sCurrentWidth2;
	CString m_sAddedWidth;
	CString m_sAddedWidth1;
	CString m_sAddedWidth2;
	CString m_sParallelWidth;
	CString m_sTypeOfNet;				// "Lokal n�t","Regionalt n�t"
	CString m_sPriceDeviation;	// "Prisf�rdelning"
	CString m_sPriceDeviation1;	// "I skogsgatan"
	CString m_sPriceDeviation2;	// "Kanttr�d"
	CString m_sVFallBreak;
	CString m_sVFallFactor;
	CString m_sInfrPercent;		// "Uppr�kning av Intr�ng i procent"; 100615 p�d

	CString m_sPriceBase;		// "Prisbasbelopp"
	CString m_sMaxPerc;			// "Max procent"
	CString m_sPercOfPriceBase;  // "Procent av prisbasbelopp"	#4420 20150803 J�
	CString m_sCurrPercent;	// "Procentsats"
	CString m_sVAT;	// "Moms"

	CString m_sVATForRotpost;			// "Inkludera/Exkludera i MOMS f�r rotpost"
	CString m_sVoluntaryDeal;			// "Inkludera/Exkludera i Frivillig uppg�relse"
	CString m_sHigherCosts;				// "Inkludera/Exkludera i F�rdyrad avverkning"
	CString m_sOtherCompensation;	// "Inkludera/Exkludera i Annan ers�ttning"
	CString m_sGROT;							// "Inkludera/Exkludera i GROT"

	CString m_sDataDirectory;
	CString m_sActiveDataDirectory;

	CString m_sStatus;
	CString m_sStatusOK;
	CString m_sStatusWorking;

	CString m_sMsgReplaceUpdTemplate;
	CString m_sMsgReCalcUpdCruise;
	CString m_sMsgReCalcUpdEval;
	CString m_sMsgFinishOff;
	CString m_sMsgProperty;
	CString m_sMsgStand;
	CString m_sMsgObject;

	CString m_sMsgUpdP30;
	CString m_sMsgUpdHigherCosts;
	CString m_sMsgP30NotFound;
	CString m_sMsgHigherCostsNotFound;

	CString m_sLoggMsg1;
	CString m_sLoggMsg2;
	CString m_sLogErrorMsg2;
	CString m_sLogErrorMsgSoderberg;
	CString m_sLogErrorHeader;

	CString m_sLogRecalcMsg;

	CString m_sPricelistCompareMsg;

	CString m_sRecalcVStandMsg;

	CString m_sMsgCap;
	CString m_sMsgEMailSubject;
	CString m_sMsgEMailText;
	CString m_sMsgNoObjects;
	CString m_sMsgNoProperties;
	CString m_sMsgNoProperties_eval;
	CString m_sMsgNoProperties_cruise;
	CString m_sMsgNoPropertiesToCalc1;
	CString m_sMsgNoPropertiesToCalc2;

	CString m_sMsgBarkFuncErrSoderbergs;
	CString m_sMsgHgtFuncErrSoderbergs;

	CString m_sHeightForH25Unreasonable;
	CString m_sHeightForH25Unreasonable2;


	CString m_sMsgPrintOut1;
	CString m_sMsgPrintOut2;
	CString m_sMsgPrintOut3;
	CString m_sMsgPrintOut4;
	CString m_sMsgPrintOut5;
	CString m_sMsgPrintOut6;
	CString m_sMsgPrintOut7;
	CString m_sMsgPrintOut8;
	CString m_sMsgPrintOut9;

	CString m_sMsgGrowthAreaError;

	CString m_sSearchDlgTitle;

	CString m_sLogAddOtherCompMsg;

	CString m_sVoluntaryDealLocal;
	CString m_sVoluntaryDealRegional;
	CString m_sVoluntaryDealSwedishEnergy;
	CString m_sVoluntaryDealCurrency;

	CString m_sLogCap;
	CString m_sCancel;
	CString m_sPrintOut;
	CString m_sSaveToFile;
	CString m_sRestrictionOnReCalc;
	CString m_sDoRecalculation;
	CString m_sDoCompleteReCalc;
	CString m_sMsgCouldNotSaveObject;
		// .. bind to
	CString m_sBinToExchPricelistStr;
	CString m_sBindToPricelistSelected;
	int m_nPriceListTypeOf;
	CString m_sPriceListXML;
	CString m_sBindToP30Selected;
	CString m_sBindToGrowthAreaSelected;
	long m_nBindToLatitude;	// Breddgrad
	long m_nBindToAltitude;	 
	double m_fBindToVFallBreak;		// Added 090528 p�d
	double m_fBindToVFallFactor;	// Added 090528 p�d
	int m_nP30TypeOf;
	int m_nP30SelIndex;
	int m_nGrowthAreaSelIndex;
	CString m_sBindToHighCostSelected;

	CString m_sBindToCosts;
	int m_nCostsTypeOf;
	CString m_sCostsXML;

	CString m_sSetAsFinished;		//new #3385
	CString m_sSetAsOngoing;		//new #3385
	BOOL m_bObjectsIsFinished;		//new #3385
	
	double m_fBindToDiameterClass;
	// .. for S�derbergs
	BOOL m_bBindToIsAtCoast;
	BOOL m_bBindToIsSouthEast;
	BOOL m_bBindToIsRegion5;
	BOOL m_bBindToIsPartOfPlot;
	CString m_sBindToSIH100_pine;	// "Tall S�derbergs funktion"

	// ... for Include/Exclude in repoprts
	BOOL m_bBindToIsVAT;						// "Moms (rotpost)"
	BOOL m_bBindToIsVoluntaryDeal;	// "Frivillig uppg�relse"
	BOOL m_bBindToIsHigherCosts;		// "F�rdyrad avverkning"
	BOOL m_bBindToIsOtherCompens;		// "Annan ers�ttning"
	BOOL m_bBindToIsGrot;						// "Grot"

	CString m_sBindToTypeOfInfr;
	CString m_sBindToTypeOfInfrIndex;
	CString m_sBindToForrestNorm;
	int m_nNormTypeOf;
	CString m_sBindToTypeOfNet;
	double m_fBindToTakeCareOfPercent;
	double m_fBindToCorrectionFactor;
	double m_fBindToObjLength;
	double m_fBindToCurrentWidth1;
	double m_fBindToCurrentWidth2;
	double m_fBindToAddedWidth1;
	double m_fBindToAddedWidth2;
	double m_fBindToParallelWidth;
	double m_fBindToPriceDev1;	// "I skogsgatan"
	double m_fBindToPriceDev2;	// "Kanttr�d"

	double m_fBindToInfrPercent;	// "Procenttal f�r upp�kning av intr�ng"; 100615 p�d

	CString m_sBindToDataDirectory;
	double m_fBindToPriceBase;
	double m_fBindToMaxPercent;
	double m_fBindToCurrPercent;
	double m_fBindToPercentOfPriceBase; // Procent av prisbasbelopp #4420 20150803 J�
	double m_fBindToVAT;

	CStringArray m_arrTypeOfNet;

	//CXTPPropertyGrid m_wndPropertyGrid;
	CXTPPropertyGrid m_wndSettingsGrid;

	HICON m_hIcon;
	BOOL m_bFirstOpen;

	CString m_sMsgCreateEvaluetionsFromStands;
	CString m_sMsgNoEvaluationStandsOnRandTrees;
	
	BOOL m_bCalculateEvalFromCruise;

	vecUCFunctions m_vecExchangeFunc;
	vecUCFunctions m_vecForrestNormFunc;

		// Holds Trakt-templates; 090506 p�d
	vecTransactionTemplate m_vecTraktTemplate;
	BOOL getTraktTemplatesFromDB(void);

	vecTransactionPricelist m_vecTransactionPricelist;
	void getPricelistsFromDB(void);

	vecTransaction_costtempl m_vecTransaction_costtempl;
	void getCostTemplateFromDB(void);

	vecTransactionTemplate vecTemplate_high_cost; 
	void getHighCostTemplateFromDB(void);

	// Holds P30-templates; 090417 p�d
	vecTransactionTemplate m_vecObjectP30Template;
	void getObjectP30TemplatesFromDB(short type_id);

	BOOL m_bAlreadySaidOkToClose;
	BOOL okToClose(void);

	CUMLandValueDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL m_bEnableContractorTBtn;
	BOOL m_bEnableImportPropertiesTBtn;
	BOOL m_bEnableCalculateTBtn;
	BOOL m_bEnableUpdateTBtn;
	BOOL m_bEnableRemoveTBtn;
	BOOL m_bEnableToolTBtn;
	BOOL m_bEnableExportTBtn;
	BOOL m_bEnableExtDocTBtn;

	BOOL m_bEnablePrintOutTBtn;

	BOOL m_bDoCheckPricelist;

	BOOL m_bInitReports;
	CString m_sShellDataFile;
	int m_nShellDataIndex;

	CTransaction_elv_object m_recActiveObject;
	int m_nActiveObjectID;
	int m_nPrlTypeOf;	// "Estimate prislista","Medelprislista"

	CDialogBar m_wndFieldChooserDlg;		// Sample Field chooser window

	CDialogBar m_wndFieldChooserDlg_evalue;		// Sample Field chooser window for Evaluated stands; 090610 p�d
	CDialogBar m_wndFieldChooserDlg_cruise;		// Sample Field chooser window for Crusin's; 090610 p�d

	CMyControlPopup *m_pToolsPopup;

	vecSTDReports m_vecReports;
	CACBox m_cbPrintOuts;
	int m_nSelPrintOut;

	vecMenuItemsEnabled m_vecExcludedItemsOnObjectInfoTabSelected;
	vecMenuItemsEnabled m_vecExcludedItemsOnPropertyTabSelected;
	vecSubMenuItemsEnabled m_vecExcludedSubMenuItems;	//new #3385

	vecTransaction_elv_cruise m_vecElvCruiseNotCalc; // Added 090525 p�d
	void getObjectCruisesNotCalculated(void);
	BOOL calculateThisCruise(CTransaction_elv_cruise& rec);

	BOOL m_bDoCompleteReCalc;	// Added 2009-05-25 P�D

	bool m_dForceCompleteReCalc;

	bool m_dNeedToRecalcAll;
	int m_nProdIDRecalc;

	bool m_bShowMsg_OnSaveCalculateVStandInAvtiveProperty;
	BOOL m_bGISInstalled;
	BOOL m_bIsGISModule;

	short m_nDoCacluateAction;

	CHTMLCtrl m_cHTML;

	// Annan kostnad; Added 2011-08-22 p�d
	// R�knar �ven om v�rde f�r "Intr�ng"
	void recalcOtherCosts();

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}
	void GetSoderbergFromNewTemplate(TemplateParser pars,BOOL *bBindToIsAtCoast,BOOL *bBindToIsSouthEast,BOOL *bBindToIsRegion5,BOOL *bBindToIsPartOfPlot,CString *sBindToSIH100_pine);

	void setupTypeOfExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupExchangefunctionsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupTypeOfCostsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupOtherInfoInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupP30PricelistsInPropertyGrid(CXTPPropertyGridItem *pItem,bool reset = false);
	void setupGrowthAreaInPropertyGrid(CXTPPropertyGridItem *pItem,bool reset = false);
	void setupTypeOfHighCostTablesInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupLatitudeAndAltitudeInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupVFallHighCostDataInPropertyGrid(CXTPPropertyGridItem *pItem);

	void setupTypeOfInfringInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupTypeOfInfringNormInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupTypeOfMiscInfringInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupWoluntaryDealSettingsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupMiscSettingsInPropertyGrid(CXTPPropertyGridItem *pItem);
	void setupReportSettingsInPropertyGrid(CXTPPropertyGridItem *pItem);

	void setLanguage(void);

	void getSelectedPricelistInformation(CXTPPropertyGridItem *pItem);
	BOOL saveObject_prl(CXTPPropertyGridItem *pItem);
	
	void getSelectedCostInformation(CXTPPropertyGridItem *pItem);
	BOOL saveObject_cost(CXTPPropertyGridItem *pItem);

	BOOL saveObject_price_deviation(CXTPPropertyGridItem *pItem);

	BOOL saveObject_typeof_infr(CXTPPropertyGridItem *pItem);

	void updateCurrentWidth();

	BOOL setBroadening(void);

	int getTypeOfInfrIndex(void);

	BOOL saveObject_high_cost(CXTPPropertyGridItem *pItem);

	BOOL saveObject_latitude_altitude(CXTPPropertyGridItem *pItem);

	BOOL saveObject_length(CXTPPropertyGridItem *pItem);

	BOOL saveObject_curr_width(CXTPPropertyGridItem *pItem);
	BOOL saveObject_added_widths(CXTPPropertyGridItem *pItem);
	BOOL saveObject_parallel_width(CXTPPropertyGridItem *pItem);

	BOOL saveObject_vfall_break_factor(CXTPPropertyGridItem *pItem);

	void getSelectedNormInformation(CXTPPropertyGridItem *pItem);
	BOOL saveObject_norm(CXTPPropertyGridItem *pItem);
	
	void getSelectedP30Information(CXTPPropertyGridItem *pItem);
	BOOL saveObject_p30(CXTPPropertyGridItem *pItem,bool recalc = true);
	
	void getSelectedGrowthAreaInformation(CXTPPropertyGridItem *pItem);
	BOOL saveObject_growth_area(CXTPPropertyGridItem *pItem,bool recalc = true);


	BOOL setGrowthAreaOnSelectedP30Price(void);


	BOOL saveObject_dcls(CXTPPropertyGridItem *pItem);

	BOOL saveObject_recalc_by(CXTPPropertyGridItem *pItem);

	BOOL saveObject_include_exclude(CXTPPropertyGridItem *pItem);

	BOOL saveObject_type_of_net(CXTPPropertyGridItem *pItem);

	BOOL saveObject_pricebase_max_percent(CXTPPropertyGridItem *pItem);

	void recalulateVouluntaryDeals(void);

	BOOL updateTraktMiscDataPerProperty(vecTransaction_elv_properties& vec);

	void cleanUpTraktOnUpdatePerProperty(CTransaction_elv_properties& prop,CTransaction_elv_object &rec_obj,int trakt_id);

	int checkPricelistForSpecies(CTransaction_elv_object &rec_obj,int trakt_id);

	BOOL doesStatusAllowChange(BOOL only_msg);

	BOOL isCalculationdataOK();

	BOOL isThereOnlyRandTreesInStand(CTransaction_elv_cruise &rec);

	//Lagt till funktion f�r att ta bort flera fastigheter feature #2456 20111101 J�
	// nRemoveRotpost = 1 Ta bara bort fr�n intr�ng
	// nRemoveRotpost = 2 Ta �ven bort rotpost
	BOOL RemoveProperties(int nRemoveRotpost);
	
	void setObjectStatus(int status);	//new #3385
	void showObjectStatus(int status);	//new #3385
	BOOL getIsAnyObjectOngoing(void);	//new #3385
	BOOL getIsAnyObjectFinished(void);	//new #3385
public:
	CMDI1950ForrestNormFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CXTPTabClientWnd m_MTIClientWnd;

	void setPopulateSettings(void);

	void setSubMenuItemsExcluded(int obj_status);	//new #3385
	
	void setActiveObject(CTransaction_elv_object& rec,BOOL is_clear)
	{
		m_recActiveObject = rec;
		if (m_wndInfoDlgBar.GetSafeHwnd() != NULL)
		{
			m_wndInfoDlgBar.setObjectRecord(m_recActiveObject,is_clear);
		}
		canWeCalculateThisProp_cached(0,true); // refresh cached status table; Optimization by Peter
	}

	void setActiveProperty(CTransaction_elv_properties& rec,BOOL is_data)
	{
		if (m_wndInfoDlgBar.GetSafeHwnd() != NULL)
		{
			m_wndInfoDlgBar.setPropertyRecord(rec,is_data);
		}
	}

	void setStatusBarText(int index,LPCTSTR text)
	{
		if (m_wndStatusBar.GetSafeHwnd() != NULL)
		{
			m_wndStatusBar.SetPaneText(index,text);
		}
	}
/*
	void setStatusBarProgress(void)
	{
		if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.StepIt();
		}
	}
	void setShowHideBarProgress(int cmd )
	{
		if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.ShowWindow( cmd );
		}
	}
*/

	void setContractorTBtn(BOOL enabled)	{	m_bEnableContractorTBtn = enabled;	}

	void setImportPropertiesTBtn(BOOL enabled)	{	m_bEnableImportPropertiesTBtn = enabled;	}

	void setCalculateTBtn(BOOL enabled)	{	m_bEnableCalculateTBtn = enabled;	}

	void setUpdateTBtn(BOOL enabled)	{	m_bEnableUpdateTBtn = enabled;	}

	void setRemoveTBtn(BOOL enabled)	{	m_bEnableRemoveTBtn = enabled;	}

	void setToolTBtn(BOOL enabled)	{	m_bEnableToolTBtn = enabled;	}

	void setExportTBtn(BOOL enabled)	{	m_bEnableExportTBtn = enabled;	} //HMS-76 20220128 J�

	void setExtDocTBtn(BOOL enabled)	{	m_bEnableExtDocTBtn = enabled;	}

	void setPrintoutsTBtn(BOOL enabled)
	{
		m_bEnableExtDocTBtn = enabled;
		m_cbPrintOuts.EnableWindow(enabled);

	}

	void setPrintoutsCBox(BOOL enabled)	{	m_cbPrintOuts.EnableWindow(enabled);	}

	void setGISButton(BOOL enabled)	{	m_bGISInstalled = enabled;	}

	void setExcludedToolItems(int tab_selected);

	void changeInventoryDirectory(void);

	CString getTypeOfInfrSelected(void)		{ return m_sBindToTypeOfInfrIndex; }

	CString getPricelistSelected(void)		{ return m_sBindToPricelistSelected; }
	int getPricelistTypeOf(void)					{ return m_nPriceListTypeOf; }
	CString getPricelistXML(void)					{ return m_sPriceListXML; }

	CString getCostsSelected(void)				{ return m_sBindToCosts; }
	int getCostsTypeOf(void)							{ return m_nCostsTypeOf; }
	CString getCostsXML(void)							{ return m_sCostsXML;	}

	CString getForrestNormName(void)	{	return m_sBindToForrestNorm; 	}
	void setForrestNormName(LPCTSTR v)	{	m_sBindToForrestNorm = v; 	}
	int getForrestNormIndex(void);
	int getTypeOfNet(void);

	double getDiameterClass(void)			{ return m_fBindToDiameterClass; }
	void setDiameterClass(double v)		{ m_fBindToDiameterClass = v; }

	// Create a semicolon limited string, based
	// on data entered for "S�derbergs"; 080408 p�d
	CString getExtraInfo(void);

	double getTakeCareOfPerc(void)		{ return m_fBindToTakeCareOfPercent; }
	void setTakeCareOfPerc(double v)	{ m_fBindToTakeCareOfPercent = v; }
	double getCorrFactor(void)				{ return m_fBindToCorrectionFactor; }
	void setCorrFactor(double v)			{ m_fBindToCorrectionFactor = v; }

	double getObjLength(void)					{ return m_fBindToObjLength; }
	void setObjLength(double v)				{ m_fBindToObjLength = v; }

	double getObjReclacBy(void)				{ return m_fBindToInfrPercent; }

	double getCurrentWidth1(void)			{ return m_fBindToCurrentWidth1; }
	void setCurrentWidth1(double v)		{ m_fBindToCurrentWidth1 = v; }
	double getCurrentWidth2(void)			{ return m_fBindToCurrentWidth2; }
	void setCurrentWidth2(double v)		{ m_fBindToCurrentWidth2 = v; }
	double getAddedWidth1(void)				{ return m_fBindToAddedWidth1; }
	void setAddedWidth1(double v)			{ m_fBindToAddedWidth1 = v; }
	double getAddedWidth2(void)				{ return m_fBindToAddedWidth2; }
	void setAddedWidth2(double v)			{ m_fBindToAddedWidth2 = v; }
	double getParallelWidth(void)				{ return m_fBindToParallelWidth; }
	void setParallelWidth(double v)			{ m_fBindToParallelWidth = v; }

	double getPriceDiv1(void)					{ return m_fBindToPriceDev1;	}	// "I skogsgatan"
	void setPriceDiv1(double v)				{ m_fBindToPriceDev1 = v;	}	// "I skogsgatan"
	double getPriceDiv2(void)					{ return m_fBindToPriceDev2;	}	// "Kanttr�d"
	void setPriceDiv2(double v)				{ m_fBindToPriceDev2 = v;	}	// "Kanttr�d"

	double getPriceBase(void)					{ return m_fBindToPriceBase; }
	void setPriceBase(double v)				{ m_fBindToPriceBase = v; }
	double getMaxPercent(void)				{ return m_fBindToMaxPercent; }
	void setMaxPercent(double v)			{ m_fBindToMaxPercent = v; }
	double getCurrPercent(void)				{ return m_fBindToCurrPercent; }
	void setCurrPercent(double v)			{ m_fBindToCurrPercent = v; }
	double getVAT(void)								{ return m_fBindToVAT; }
	void setVAT(double v)							{ m_fBindToVAT = v; }

	double getPercentOfPriceBase(void)		{ return m_fBindToPercentOfPriceBase;}
	void setPercentOfPriceBase(double p)		{ m_fBindToPercentOfPriceBase = p;}

	BOOL getIsVAT(void)								{ return m_bBindToIsVAT; }
	void setIsVAT(int v)							{ m_bBindToIsVAT = (v == 1 ? TRUE : FALSE); }

	BOOL getIsVoluntaryDeal(void)			{ return m_bBindToIsVoluntaryDeal; }
	void setIsVoluntaryDeal(int v)		{ m_bBindToIsVoluntaryDeal = (v == 1 ? TRUE : FALSE); }

	BOOL getIsHigherCosts(void)				{ return m_bBindToIsHigherCosts;	}
	void setIsHigherCosts(int v)			{ m_bBindToIsHigherCosts = (v == 1 ? TRUE : FALSE);	}

	BOOL getIsOtherCompens(void)			{ return m_bBindToIsOtherCompens;	}
	void setIsOtherCompens(int v)			{ m_bBindToIsOtherCompens = (v == 1 ? TRUE : FALSE);	}

	BOOL getIsGROT(void)							{ return m_bBindToIsGrot;	}
	void setIsGROT(int v)							{ m_bBindToIsGrot = (v == 1 ? TRUE : FALSE);	}


	void clearSettingsPropertyGrid(void);
	void enablePropertyGrid(BOOL enable);

	// Handle on Evaluated
	inline void doAddNewVStandToAvtiveProperty()						{ OnAddNewVStandToAvtiveProperty(); }
	inline void doSaveCalculateVStandInAvtiveProperty(bool msg)			
	{ 
		m_bShowMsg_OnSaveCalculateVStandInAvtiveProperty = msg;
		OnSaveCalculateVStandInAvtiveProperty(); 
	}
	inline void doRemoveCalculateVStandFromAvtiveProperty() { OnRemoveCalculateVStandFromAvtiveProperty(); }

	// Handle on Cruising
	inline void doCalculateManuallyEnteredCruises()		{	OnCalculateManuallyEnteredCruises();	}
	
	BOOL doMatchStandsToAvtiveProperty(bool use_dlg = TRUE);
	inline void doOnRemoveSelStandFromProperty()			{	OnRemoveSelStandFromProperty();	}
//	inline void doCreateEvaluationFromCruise()				{	OnCreateEvaluationFromCruise();	}
	void doCreateEvaluationFromCruise(bool show_msg = TRUE);

	void runCalulation(int prop_id)	{ m_dNeedToRecalcAll = FALSE; m_nProdIDRecalc = prop_id; OnCalculateTBtn(); m_dNeedToRecalcAll = TRUE; m_nProdIDRecalc = -1;}


	void doRemoveProperties(int nRemoveRotpost) { RemoveProperties(nRemoveRotpost); }

	CString FormatShareToDecimal(CString cShare);
	float FormatShareToPercent(CString cShare);

	enum {VP_EXCELVERSION_2020,VP_EXCELVERSION_2021,VP_EXCELVERSION_20220113,VP_EXCELVERSION_20220119,VP_EXCELVERSION_20230125};

	enum {ERSERBJ_EXCELVERSION_2019,ERSERBJ_EXCELVERSION_2023};

	int CheckVersionOfExcelVp(void);
	int CheckVersionOfExcelErsErbj(void);
	void ExportPropertiesExcelrapportErsErbj(int nVersion);
	void ExportPropertiesExcelrapportVp_2020(void);
	void ExportPropertiesExcelrapportVp_2021(void);
	void ExportPropertiesExcelrapportVp_2022_01_19(void);
	void ExportPropertiesExcelrapportVp_2022_01_13(void);
	void ExportPropertiesExcelrapportEllevio2025_01_22(void);
	
	void Split1(const CString strIn, const CString delim, CStringArray &a);

	// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CMDI1950ForrestNormFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CMDI1950ForrestNormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);


//	afx_msg void OnImportPropertiesTBtn(void);
	afx_msg void OnContractorTBtn(void);
	afx_msg void OnMenuCalculateTBtn(void);
	afx_msg BOOL OnCalculateTBtn(void);
	afx_msg void OnImportPropertiesTBtn(void);
	afx_msg void OnImportPropertiesFromFileTBtn(void); //#5008 PH 20160615
	afx_msg void OnHandleDocumentTBtn(void);
	afx_msg void OnPrintOutTBtn(void);

	afx_msg void OnUpdateGotoGIS(CCmdUI* pCmdUI);
	afx_msg void OnUpdateContractorTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateImportPropertiesTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCalculateTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUpdateTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoveTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExportTBtn(CCmdUI* pCmdUI); //HMS-76
	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExtDocTBtn(CCmdUI* pCmdUI);

	afx_msg void OnUpdatePricelistTemplate(void);
	afx_msg void OnUpdatePricelistInfoTemplate(void);
	afx_msg void OnUpdateCostTemplate(void);
	afx_msg void OnChangeTemplate(void);
	afx_msg void OnUpdatePropertiesTBtn(void);
	afx_msg void OnRemovePropertyTBtn(void);
	afx_msg void OnRemovePropertyTBtn2(void);	// Added 101115 p�d
	afx_msg void OnExportPropertiesTBtn(void);
	afx_msg void OnExportPropertiesExcelrapportErsErbjTBtn(void);
	afx_msg void OnExportPropertiesExcelrapportVpTBtn(void);
		

	afx_msg void OnExportPropertiesXmlTBtn(void);
	afx_msg void OnExportPropertiesJsonTBtn(void);

	afx_msg void OnExportPropData(void);


	afx_msg void OnLogBookSettingsTBtn(void);
	afx_msg void OnCalculateManuallyEnteredCruises(void);
	afx_msg void OnMatchStandsToAvtiveProperty(void);
	afx_msg void OnRemoveSelStandFromProperty(void);

	//#5273 PH 2017-02-13
	afx_msg void OnOpenSelStand(void);
	afx_msg void OnCreateNewStand(void);
	afx_msg void OnShowPriceList(void);
	afx_msg void OnShowCostTemplate(void);

	afx_msg void OnMatchStandsToProperties(void);
	afx_msg void OnAddNewVStandToAvtiveProperty(void);
	afx_msg void OnSaveCalculateVStandInAvtiveProperty(void);
	afx_msg void OnSaveCalculateVStands(void);
	afx_msg void OnRemoveCalculateVStandFromAvtiveProperty(void);
	afx_msg void OnGroupPropertiesOnLandOwners(void);
	afx_msg void OnGroupPropertiesOnPropertyNumbers(void);
	afx_msg void OnSavePropertyGroupings(void);
	afx_msg void OnDelPropertyGroupings(void);
	afx_msg void OnRecalculateK3M3ForStands(void);
	afx_msg void OnChangeStatusForStands(void);
	afx_msg void OnCreateSetupFileForCaliper(void);
	afx_msg void OnSendSetupToCaliper(void);
	afx_msg void OnSendSetupToDPII(void);
	afx_msg void OnSendSetupByEMail(void);
	afx_msg void OnReciveINVDataFromCaliperDP(void);
	afx_msg void OnReciveINVDataFromCaliperDPII(void);
	afx_msg void OnCreateFormINVDataOnObjectAndProperties(void);
	afx_msg void OnCreateExcelData(void);
	afx_msg void OnCreateEvaluationFromCruise(void);
	afx_msg void OnCreateAllEvaluationsFromCruise(void);
	afx_msg void OnChangeSplitBarOrientation(void);
	afx_msg void OnGotoGIS(void);	// Added 090929 p�d
	afx_msg void OnUpdateP30(void);	// Added 100303 p�d
	afx_msg void OnUpdateHigherCosts(void);	// Added 100303 p�d
	afx_msg void OnAddOtherCosts(void); // Added 100611 p�d
	afx_msg void OnListPropertyOwners(void); //Added 160609 PH

	//new #3385
	afx_msg void OnSetObjectFinished(void);
	afx_msg void OnSetObjectOngoing(void);
	afx_msg void OnShowObjectOngoing(void);
	afx_msg void OnShowObjectFinished(void);
	afx_msg void OnShowObjectAll(void);
	
	//}}AFX_MSG
DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDI1950ForrestNormListFrame frame

class CMDI1950ForrestNormListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDI1950ForrestNormListFrame)

//private:
	BOOL m_bFirstOpen;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDI1950ForrestNormListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDI1950ForrestNormListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDIP30TemplateFrame frame

class CMDIP30TemplateFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIP30TemplateFrame)

//private:
	BOOL m_bFirstOpen;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	CXTPToolBar m_wndToolBar;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

	BOOL m_bEnableTBtns;

	BOOL m_bAlreadySaidOkToClose;
	BOOL okToClose(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:

	CMDIP30TemplateFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIP30TemplateFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

		void setEnableToolbar(BOOL enable)	{ m_bEnableTBtns = enable; }
protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);

	afx_msg	void OnAddSpecieTBtnClick(void);
	afx_msg void OnUpdateSpcTBtnClick(CCmdUI* pCmdUI);

	afx_msg	void OnRemoveSpecieTBtnClick(void);
	afx_msg void OnUpdateRemoveSpcTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnImportTBtnClick(void);
	afx_msg void OnUpdateImportTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnExportTBtnClick(void);
	afx_msg void OnUpdateExportTBtnClick(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



///////////////////////////////////////////////////////////////////////////////////////////
// CMDIP30NewNormTemplateFrame frame

class CMDIP30NewNormTemplateFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIP30NewNormTemplateFrame)

//private:
	BOOL m_bFirstOpen;
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sMainWindowCaption;

	CXTPDockingPaneManager m_paneManager;

	CXTPToolBar m_wndToolBar;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

	BOOL m_bEnableTBtns;

	BOOL m_bAlreadySaidOkToClose;
	BOOL okToClose(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:

	CMDIP30NewNormTemplateFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIP30NewNormTemplateFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

		void setEnableToolbar(BOOL enable)	{ m_bEnableTBtns = enable; }
protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);

	afx_msg	void OnAddSpecieTBtnClick(void);
	afx_msg void OnUpdateSpcTBtnClick(CCmdUI* pCmdUI);

	afx_msg	void OnRemoveSpecieTBtnClick(void);
	afx_msg void OnUpdateRemoveSpcTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnImportTBtnClick(void);
	afx_msg void OnUpdateImportTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnExportTBtnClick(void);
	afx_msg void OnUpdateExportTBtnClick(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



///////////////////////////////////////////////////////////////////////////////////////////
// CMDIP30Norm2018TemplateFrame frame

class CMDIP30Norm2018TemplateFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIP30Norm2018TemplateFrame)

//private:
	BOOL m_bFirstOpen;
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sMainWindowCaption;

	CXTPDockingPaneManager m_paneManager;

	CXTPToolBar m_wndToolBar;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

	BOOL m_bEnableTBtns;

	BOOL m_bAlreadySaidOkToClose;
	BOOL okToClose(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:

	CMDIP30Norm2018TemplateFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIP30Norm2018TemplateFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

		void setEnableToolbar(BOOL enable)	{ m_bEnableTBtns = enable; }
protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);

	afx_msg	void OnAddSpecieTBtnClick(void);
	afx_msg void OnUpdateSpcTBtnClick(CCmdUI* pCmdUI);

	afx_msg	void OnRemoveSpecieTBtnClick(void);
	afx_msg void OnUpdateRemoveSpcTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnImportTBtnClick(void);
	afx_msg void OnUpdateImportTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnExportTBtnClick(void);
	afx_msg void OnUpdateExportTBtnClick(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDIHigherCostsFrame frame

class CMDIHigherCostsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIHigherCostsFrame)

//private:
	BOOL m_bFirstOpen;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	CXTPToolBar m_wndToolBar;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

	BOOL m_bEnableTBtns0;
	BOOL m_bEnableTBtns1;

	BOOL m_bAlreadySaidOkToClose;
	BOOL okToClose(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:

	CMDIHigherCostsFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIHigherCostsFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void setEnableToolbar0(BOOL enable)	{ m_bEnableTBtns0 = enable; }
	void setEnableToolbar1(BOOL enable)	{ m_bEnableTBtns1 = enable; }

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);

	afx_msg void OnAddRowTBtnClick(void);
	afx_msg void OnUpdateAddTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnRemoveRowTBtnClick(void);
	afx_msg void OnUpdateRemoveRowTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnImportTBtnClick(void);
	afx_msg void OnUpdateImportTBtnClick(CCmdUI* pCmdUI);

	afx_msg void OnExportTBtnClick(void);
	afx_msg void OnUpdateExportTBtnClick(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


class CMDITemplateListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDITemplateListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDITemplateListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDITemplateListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
//	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CMDITemplate2ListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDITemplate2ListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDITemplate2ListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDITemplate2ListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
//	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


class CMDITemplate2018ListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDITemplate2018ListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDITemplate2018ListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDITemplate2018ListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
//	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



class CMDITemplate3ListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDITemplate3ListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDITemplate3ListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDITemplate3ListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
//	afx_msg void OnClose(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
///////////////////////////////////////////////////////////////////////////////////////////
// CDocumentTmplFrame frame

class CDocumentTmplFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CDocumentTmplFrame)

//private:
	BOOL m_bFirstOpen;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;
	CXTPToolBar m_wndToolBar;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:

	CDocumentTmplFrame();           // protected constructor used by dynamic creation
	virtual ~CDocumentTmplFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


	void setEnableToolBar(BOOL enable) { m_wndToolBar.EnableWindow(enable); }

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CDocumentTmplListFrame frame

class CDocumentTmplListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CDocumentTmplListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CDocumentTmplListFrame();           // protected constructor used by dynamic creation
	virtual ~CDocumentTmplListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CPropertyStatusFrame frame

class CPropertyStatusFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CPropertyStatusFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CPropertyStatusFrame();           // protected constructor used by dynamic creation
	virtual ~CPropertyStatusFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDISpeciesFrame frame

class CMDISpeciesFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDISpeciesFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	CString m_sMsg1;
	CString m_sMsgCap;
	CString m_sLangFN;

	BOOL m_bFirstOpen;

	HICON m_hIcon;
public:

	CMDISpeciesFrame();           // protected constructor used by dynamic creation
	virtual ~CMDISpeciesFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



/*
//////////////////////////////////////////////////////////////////////////////////////////
// CPropertyRemoveSelListFrame frame

class CPropertyRemoveSelListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CPropertyRemoveSelListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	//CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CPropertyRemoveSelListFrame();           // protected constructor used by dynamic creation
	virtual ~CPropertyRemoveSelListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	//CDialogBar m_wndFieldChooser;   // Sample Field chooser window


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
*/

#endif