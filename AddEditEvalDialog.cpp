// AddEditEvalDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AddEditEvalDialog.h"
#include "ResLangFileReader.h"
#include "MyInputDlg.h"
#include "MDI1950ForrestNormFormView.h"


// CAddEditEvalDialog dialog

IMPLEMENT_DYNAMIC(CAddEditEvalDialog, CDialog)

CAddEditEvalDialog::CAddEditEvalDialog(CWnd* pParent /*=NULL*/) : CDialog(CAddEditEvalDialog::IDD, pParent)
{

}

CAddEditEvalDialog::CAddEditEvalDialog(int objId,int propertyId,CUMLandValueDB * db,int evaluationId) : CDialog(CAddEditEvalDialog::IDD, NULL) {
	objectId=objId;
	propId=propertyId;
	evalId=evaluationId;
	m_pDB=db;

	if(evalId==-1) {
		record = new CTransaction_eval_evaluation();
		record->setObjectId(objectId);
		record->setPropId(propId);
	}
	else {
		record = db->getObjectEvaluation(propId,objectId,evalId);
		
	}
}

CAddEditEvalDialog::~CAddEditEvalDialog()
{
}

BOOL CAddEditEvalDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			//Set Labels
			m_wndLblType.SetWindowText(xml.str(IDS_STRING4109));
			m_wndLblName.SetWindowText(xml.str(IDS_STRING4100));
			m_wndLblArea.SetWindowText(xml.str(IDS_STRING4101));
			m_wndLblAge.SetWindowText(xml.str(IDS_STRING4102));
			m_wndLblH100.SetWindowText(xml.str(IDS_STRING4103));
			m_wndLblCorrFactor.SetWindowText(xml.str(IDS_STRING4104));
			m_wndLblPartPine.SetWindowText(xml.str(IDS_STRING4105));
			m_wndLblPartSpruce.SetWindowText(xml.str(IDS_STRING4106));
			m_wndLblPartBirch.SetWindowText(xml.str(IDS_STRING4107));
			m_wndLblVolume.SetWindowText(xml.str(IDS_STRING4114));
			m_wndLblVolumePerHa.SetWindowText(xml.str(IDS_STRING4115));
			m_wndLblLength.SetWindowText(xml.str(IDS_STRING4201));
			m_wndLblWidth.SetWindowText(xml.str(IDS_STRING4202));
			m_wndLblLandType.SetWindowText(xml.str(IDS_STRING4108));
			m_wndLblLandValue.SetWindowText(xml.str(IDS_STRING4110));
			m_wndLblLandValuePerHa.SetWindowText(xml.str(IDS_STRING4112));
			m_wndLblLayer.SetWindowText(xml.str(IDS_STRING4154));

			m_wndLblReducMark.SetWindowText(xml.str(IDS_STRING4205));

			//Set Buttons
			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING40707));
			m_wndCancel.SetWindowText(xml.str(IDS_STRING22696));
			m_wndCalculateCorrFactor.SetWindowText(xml.str(IDS_STRING4116));

			//m_wndOverlappingLand.SetWindowText(xml.str(IDS_STRING4205));

			//Set Dialog title
			SetWindowText(xml.str(IDS_STRING413));

			//Dropdown
			m_wndComboType.AddString(xml.str(IDS_STRING4150)); //V�rderad
			m_wndComboType.AddString(xml.str(IDS_STRING4152)); //Annan mark

			m_wndComboLandType.AddString(xml.str(IDS_STRING4160)); //Behandlad 
			m_wndComboLandType.AddString(xml.str(IDS_STRING4161)); //Obehandlad

			//#HMS-41 Tillf�lligt utnyttjande 190327 J�
			m_wndComboReducMark.AddString(xml.str(IDS_STRING42050));	//Inget, tomt
			m_wndComboReducMark.AddString(xml.str(IDS_STRING42051));	//�verlappande Mark
			m_wndComboReducMark.AddString(xml.str(IDS_STRING42052));	//Tillf�lligt utnyttjande

			//Set text masks
			m_wndEditName.SetLimitText(50);
			m_wndEditArea.SetAsNumeric();
			m_wndEditAge.SetLimitText(3);
			m_wndEditH100.SetEditMask(_T(">00"), _T("___"));
			m_wndEditCorrFactor.SetAsNumeric();
			m_wndEditCorrFactor.SetLimitText(4);
			m_wndEditPartPine.SetLimitText(3);
			m_wndEditPartSpruce.SetLimitText(3);
			m_wndEditPartBirch.SetLimitText(3);
			m_wndEditVolume.SetAsNumeric();
			m_wndEditVolume.SetLimitText(10);
			m_wndEditVolumePerHa.SetAsNumeric();
			m_wndEditVolumePerHa.SetLimitText(10);
			m_wndEditLength.SetLimitText(10);
			m_wndEditWidth.SetLimitText(10);
			m_wndEditLandValue.SetAsNumeric();
			m_wndEditLandValue.SetLimitText(10);
			m_wndEditLandValuePerHa.SetAsNumeric();
			m_wndEditLandValuePerHa.SetLimitText(10);
			m_wndEditLayer.SetAsNumeric();

			if(evalId!=-1) {

				updateLandValue=false;
				updateLandValuePerHa=false;
				updateVolume=false;
				updateVolumePerHa=false;

				//Get values from database
				m_wndEditName.SetWindowTextW(record->getEValName());
				m_wndEditArea.setFloat(abs(record->getEValAreal()),3);
				m_wndEditAge.setInt(abs(record->getEValAge()));
				m_wndEditH100.SetWindowTextW(record->getEValSIH100());
				m_wndEditCorrFactor.setFloat(abs(record->getEValCorrFactor()),2);
				m_wndEditPartPine.setFloat(abs(record->getEValPinePart()),0);
				m_wndEditPartSpruce.setFloat(abs(record->getEValSprucePart()),0);
				m_wndEditPartBirch.setFloat(abs(record->getEValBirchPart()),0);
				m_wndEditLandValue.setFloat(abs(record->getEValLandValue()),3);
				m_wndEditLandValuePerHa.setFloat(abs(record->getEValLandValue_ha()),3);
				m_wndEditVolume.setFloat(abs(record->getEValVolume()),3);
				m_wndEditLength.setInt(abs(record->getEValLength()));
				m_wndEditWidth.setInt(abs(record->getEValWidth()));
				//m_wndOverlappingLand.SetCheck(record->getEValOverlappingLand());
				m_wndEditLayer.setInt(record->getEValLayer());

				if(record->getEValVolume()>0&&record->getEValAreal()>0) {
					m_wndEditVolumePerHa.setFloat(record->getEValVolume()/record->getEValAreal(),3);
				}
				else
					m_wndEditVolumePerHa.setFloat(0,3);

				if(record->getEValType()==EVAL_TYPE_VARDERING)
					m_wndComboType.SelectString(0,xml.str(IDS_STRING4150));//V�lj v�rderad
				else if(record->getEValType()==EVAL_TYPE_ANNAN)
					m_wndComboType.SelectString(0,xml.str(IDS_STRING4152));//Annan mark

				//#HMS-41 Tillf�lligt utnyttjande 190327 J�
				if(record->getEValOverlappingLand()==false && record->getEValTillfUtnyttjande()==false)
					m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42050));//V�lj tomt
				else if(record->getEValOverlappingLand())
					m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42051));//V�lj �verlappande mark
				else if(record->getEValTillfUtnyttjande())
					m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42052));//V�lj tillf�lligt utnyttjande
			}
			else 
			{
				m_wndComboType.SelectString(0,xml.str(IDS_STRING4150));//V�lj v�rderad
				m_wndEditLayer.setInt(1);
				//#HMS-41 Tillf�lligt utnyttjande 190327 J�
				m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42050));//V�lj tomt
			}

		}
	}

	updateLandValue=true;
	updateLandValuePerHa=true;
	updateVolumePerHa=true;
	updateVolume=true;

	enableDisableFields();

	return TRUE;
}

void CAddEditEvalDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	// Labels
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_TYPE_LABEL, m_wndLblType);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_NAME_LABEL, m_wndLblName);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_AREA_LABEL, m_wndLblArea);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_AGE_LABEL, m_wndLblAge);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_H100_LABEL, m_wndLblH100);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_CORR_FACTOR_LABEL, m_wndLblCorrFactor);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_PART_PINE_LABEL, m_wndLblPartPine);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_PART_SPRUCE_LABEL, m_wndLblPartSpruce);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_PART_BIRCH_LABEL, m_wndLblPartBirch);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_VOLUME_LABEL, m_wndLblVolume);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_VOLUME_PER_HA_LABEL, m_wndLblVolumePerHa);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LENGTH_LABEL, m_wndLblLength);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_WIDTH_LABEL, m_wndLblWidth);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LAND_TYPE_LABEL, m_wndLblLandType);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LANDVALUE_LABEL, m_wndLblLandValue);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LANDVALUE_PER_HA_LABEL, m_wndLblLandValuePerHa);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LAYER, m_wndLblLayer);

	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_REDUCERADMARK_LABEL, m_wndLblReducMark);

	

	// Buttons
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_OK_BTN, m_wndOKBtn);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_CANCEL_BUTTON, m_wndCancel);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_CORR_FACTOR_BUTTON, m_wndCalculateCorrFactor);
	
	//DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_OVERLAPPING_LAND, m_wndOverlappingLand);

	//EditText	
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_NAME_EDIT,m_wndEditName);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_AREA_EDIT,m_wndEditArea);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_AGE_EDIT,m_wndEditAge);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_H100_EDIT,m_wndEditH100);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_CORR_FACTOR_EDIT,m_wndEditCorrFactor);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_PART_PINE_EDIT,m_wndEditPartPine);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_PART_SPRUCE_EDIT,m_wndEditPartSpruce);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_PART_BIRCH_EDIT,m_wndEditPartBirch);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_VOLUME_EDIT,m_wndEditVolume);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_VOLUME_PER_HA_EDIT,m_wndEditVolumePerHa);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LENGTH_EDIT,m_wndEditLength);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_WIDTH_EDIT,m_wndEditWidth);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LANDVALUE_EDIT,m_wndEditLandValue);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LANDVALUE_PER_HA_EDIT,m_wndEditLandValuePerHa);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LAYER_EDIT,m_wndEditLayer);

	//Dropdown
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_TYPE_COMBOO,m_wndComboType);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_LAND_TYPE_COMBO,m_wndComboLandType);
	DDX_Control(pDX, IDC_ADD_EVAL_DIALOG_REDUCERADMARK_COMBO,m_wndComboReducMark); //#HMS-41 Tillf�lligt utnyttjande 190327 J�
}


BEGIN_MESSAGE_MAP(CAddEditEvalDialog, CDialog)
	ON_BN_CLICKED(IDC_ADD_EVAL_DIALOG_OK_BTN, &CAddEditEvalDialog::OnBnClickedAddEvalDialogOkBtn)
	ON_CBN_SELCHANGE(IDC_ADD_EVAL_DIALOG_TYPE_COMBOO, &CAddEditEvalDialog::OnCbnSelchangeAddEvalDialogTypeComboo)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_LENGTH_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogLengthEdit)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_WIDTH_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogWidthEdit)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_AREA_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogAreaEdit)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_VOLUME_PER_HA_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogVolumePerHaEdit)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_VOLUME_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogVolumeEdit)
	ON_BN_CLICKED(IDC_ADD_EVAL_DIALOG_CANCEL_BUTTON, &CAddEditEvalDialog::OnBnClickedAddEvalDialogCancelButton)
	ON_BN_CLICKED(IDC_ADD_EVAL_DIALOG_CORR_FACTOR_BUTTON, &CAddEditEvalDialog::OnBnClickedAddEvalDialogCalculateButton)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_LANDVALUE_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogLandvalueEdit)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_LANDVALUE_PER_HA_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogLandvaluePerHaEdit)
	ON_EN_CHANGE(IDC_ADD_EVAL_DIALOG_CORR_FACTOR_EDIT, &CAddEditEvalDialog::OnEnChangeAddEvalDialogCorrFactorEdit)
END_MESSAGE_MAP()


// CAddEditEvalDialog message handlers
void CAddEditEvalDialog::OnBnClickedAddEvalDialogOkBtn()
{

	int typeOf = 0;
	int forrestType = -1;

	CString typeOfString;
	CString forrestTypeString;
	RLFReader xml;
	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		if (!xml.Load(sLangFN))
			return;
	}
	else
		return;

	m_wndComboType.GetLBText(m_wndComboType.GetCurSel(),typeOfString);

	if(m_wndComboLandType.GetCurSel() >= 0) 
	{
		m_wndComboLandType.GetLBText(m_wndComboLandType.GetCurSel(),forrestTypeString);
	}

	if(xml.str(IDS_STRING4150).Compare(typeOfString) == 0) 
	{ //V�rderad

		if(xml.str(IDS_STRING4160).Compare(forrestTypeString) == 0) 
		{ //Behandlat
			forrestType=0;
		}
		else if(xml.str(IDS_STRING4161).Compare(forrestTypeString) == 0) { //Obehandlat
			forrestType=1;
		}

		if(m_wndEditName.getText()==L"") {
			showMessage(IDS_STRING8010);
			return;
		}

		if(!(_tstof(m_wndEditArea.getText())>0)) {
			showMessage(IDS_STRING8011);
			return;
		}

		if(!(_tstof(m_wndEditAge.getText())>=0)) {
			showMessage(IDS_STRING8012);
			return;
		}

		if(m_wndEditH100.getText()==L"") {
			showMessage(IDS_STRING8013);
			return;
		}

		if(m_wndEditH100.getText().Left(1).FindOneOf(_T("TGBFCE"))<0) {
			showMessage(IDS_STRING8016);
			return;
		}

		if(m_wndEditCorrFactor.getText()==L"") {
			showMessage(IDS_STRING8014);
			return;
		}

		if(!checkTreePercent()&&forrestType==0) {
			showMessage(IDS_STRING4074);
			return;
		}

		int layer = _tstoi(m_wndEditLayer.getText());
		if(layer<1||layer>9) {
			showMessage(IDS_STRING4076);
			return;
		}

		typeOf=EVAL_TYPE_VARDERING;
	}
	else if(xml.str(IDS_STRING4152).Compare(typeOfString) == 0) { //Annan mark
		typeOf=EVAL_TYPE_ANNAN;
		//HMS-67 20210204 J� Kontrollera s� att namn �r satt vid annan mark
		if(m_wndEditName.getText()==L"") 
		{
			showMessage(IDS_STRING8010);
			return;
		}
	}

	//Get all values
	record->setName(m_wndEditName.getText());
	record->setAreal(_tstof(m_wndEditArea.getText()));
	record->setAge(_tstoi(m_wndEditAge.getText()));
	record->setH100(m_wndEditH100.getText());
	record->setEValCorrFactor(_tstof(m_wndEditCorrFactor.getText()));
	record->setEValPinePart(_tstof(m_wndEditPartPine.getText()));
	record->setEValSprucePart(_tstof(m_wndEditPartSpruce.getText()));
	record->setEValBirchPart(_tstof(m_wndEditPartBirch.getText()));
	record->setForrestType(forrestType);
	record->setEvalType(typeOf);
	record->setEValLandValue(_tstof(m_wndEditLandValue.getText()));
	record->setEValLandValue_ha(_tstof(m_wndEditLandValuePerHa.getText()));
	record->setVolume(_tstof(m_wndEditVolume.getText()));
	record->setLength(_tstoi(m_wndEditLength.getText()));
	record->setWidth(_tstoi(m_wndEditWidth.getText()));


	//#HMS-41 Tillf�lligt utnyttjande 190327 J�
	if(m_wndComboReducMark.GetCurSel()==EVAL_REDUCMARK_INGET)
	{
		record->setEValOverlappingLand(false);
		record->setEValTillfUtnyttjande(false);
	}
	else if(m_wndComboReducMark.GetCurSel()==EVAL_REDUCMARK_OVERLAPPANDE)
	{
		record->setEValOverlappingLand(true);
		record->setEValTillfUtnyttjande(false);
	}
	else if(m_wndComboReducMark.GetCurSel()==EVAL_REDUCMARK_TILLFALLIGT)
	{

		record->setEValOverlappingLand(false);
		record->setEValTillfUtnyttjande(true);
	}

	//record->setEValOverlappingLand(m_wndOverlappingLand.GetCheck()==1);
	record->setEvalLayer(_tstoi(m_wndEditLayer.getText()));

	if(record->getEValID_pk()==-1) {
		int newId;
		m_pDB->getObjectEvaluation_last_id(record->getEValObjID_pk(),record->getEValPropID_pk(),&newId);
		record->setEvalId(newId+1);
		m_pDB->addObjectEvaluation_entered_data(*record);
		m_pDB->updObjectEvaluation_other_data(*record);
	}
	else {
		m_pDB->updObjectEvaluation_entered_data(*record);
		m_pDB->updObjectEvaluation_other_data(*record);
	}

	CMDI1950ForrestNormFormView *pView = NULL;
	CPropertiesFormView *pPropView = NULL;
	if ((pView = (CMDI1950ForrestNormFormView *)getFormViewByID(IDD_FORMVIEW)) != NULL)
	{
		if ((pPropView = (CPropertiesFormView*)pView->getPropertiesFormView()) != NULL)
		{
			pPropView->saveAndCalculateVStandInActiveProperty(true);
		}
	}

	OnOK();
}

void CAddEditEvalDialog::OnCbnSelchangeAddEvalDialogTypeComboo()
{
	enableDisableFields();
}

void CAddEditEvalDialog::enableDisableFields() {
	CString selectedString;
	RLFReader xml;

	
	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	if (fileExists(sLangFN))
	{
		if (!xml.Load(sLangFN))
			return;
	}
	else
		return;
	
	m_wndComboType.GetLBText(m_wndComboType.GetCurSel(),selectedString);

	if(xml.str(IDS_STRING4150).Compare(selectedString) == 0) { //V�rderad
		enableDisableEdit(&m_wndEditName,true);
		enableDisableEdit(&m_wndEditArea,true);
		enableDisableEdit(&m_wndEditAge,true);
		enableDisableEdit(&m_wndEditH100,true);
		enableDisableEdit(&m_wndEditCorrFactor,true);
		enableDisableEdit(&m_wndEditPartPine,true);
		enableDisableEdit(&m_wndEditPartSpruce,true);
		enableDisableEdit(&m_wndEditPartBirch,true);
		enableDisableEdit(&m_wndComboLandType,true);
		enableDisableEdit(&m_wndEditLandValue,false);
		enableDisableEdit(&m_wndEditLandValuePerHa,false);
		enableDisableEdit(&m_wndEditVolume,true);
		enableDisableEdit(&m_wndEditVolumePerHa,true);
		enableDisableEdit(&m_wndEditLength,true);
		enableDisableEdit(&m_wndEditWidth,true);

		m_wndCalculateCorrFactor.EnableWindow(true);
		//m_wndOverlappingLand.EnableWindow(true);

		switch(record->getEValForrestType())
		{
		case 0://Behandlad
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4160));//Behandlad";
			break;
		case 1:// Obehandlad
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4161));//Obehandlad
			break;
		case -1://Ej satt s�tt som behandlad
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4160));//Behandlad";
			record->setForrestType(0);
			break;
		}

		if(record->getEValForrestType()==0)
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4160));//Behandlad"
		else
		{
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4161));//Obehandlad
		}

		//#HMS-41 Tillf�lligt utnyttjande 190327 J�
		m_wndComboReducMark.EnableWindow(true);
		if(record->getEValOverlappingLand()==false && record->getEValTillfUtnyttjande()==false)
			m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42050));//V�lj tomt
		else if(record->getEValOverlappingLand())
			m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42051));//V�lj �verlappande mark
		else if(record->getEValTillfUtnyttjande())
			m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42052));//V�lj tillf�lligt utnyttjande

		//Empty disabled fields
		m_wndComboLandType.SetWindowTextW(L"");
		m_wndEditLandValue.SetWindowTextW(L"");
		m_wndEditLandValuePerHa.SetWindowTextW(L"");
	}
	else if(xml.str(IDS_STRING4152).Compare(selectedString) == 0) { //Annan mark
		enableDisableEdit(&m_wndEditName,true);
		enableDisableEdit(&m_wndEditArea,true);
		enableDisableEdit(&m_wndEditAge,true);
		enableDisableEdit(&m_wndEditH100,false);
		enableDisableEdit(&m_wndEditCorrFactor,false);
		enableDisableEdit(&m_wndEditPartPine,false);
		enableDisableEdit(&m_wndEditPartSpruce,false);
		enableDisableEdit(&m_wndEditPartBirch,false);
		enableDisableEdit(&m_wndComboLandType,false);
		enableDisableEdit(&m_wndEditLandValue,true);
		enableDisableEdit(&m_wndEditLandValuePerHa,true);
		enableDisableEdit(&m_wndEditVolume,false);
		enableDisableEdit(&m_wndEditVolumePerHa,false);
		enableDisableEdit(&m_wndEditLength,true);
		enableDisableEdit(&m_wndEditWidth,true);

		m_wndCalculateCorrFactor.EnableWindow(false);

		//#HMS-41 Tillf�lligt utnyttjande 190327 J�
		m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42050));//V�lj tomt
		m_wndComboReducMark.EnableWindow(false);

		//m_wndOverlappingLand.EnableWindow(false);

		/*
		if(record->getEValForrestType()==0)
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4160));//Choose "Skogsmark"
		else
			m_wndComboLandType.SelectString(0,xml.str(IDS_STRING4161));//Choose "Kalmark"
			*/

		//Empty disabled fields
		m_wndEditH100.SetWindowTextW(L"");
		m_wndEditCorrFactor.SetWindowTextW(L"");
		m_wndEditPartPine.SetWindowTextW(L"");
		m_wndEditPartSpruce.SetWindowTextW(L"");
		m_wndEditPartBirch.SetWindowTextW(L"");
		m_wndEditVolume.SetWindowTextW(L"");
		m_wndEditVolumePerHa.SetWindowTextW(L"");
	}
	else {
		enableDisableEdit(&m_wndEditName,false);
		enableDisableEdit(&m_wndEditArea,false);
		enableDisableEdit(&m_wndEditAge,false);
		enableDisableEdit(&m_wndEditH100,false);
		enableDisableEdit(&m_wndEditCorrFactor,false);
		enableDisableEdit(&m_wndEditPartPine,false);
		enableDisableEdit(&m_wndEditPartSpruce,false);
		enableDisableEdit(&m_wndEditPartBirch,false);
		enableDisableEdit(&m_wndComboLandType,false);
		enableDisableEdit(&m_wndEditLandValue,false);
		enableDisableEdit(&m_wndEditLandValuePerHa,false);
		enableDisableEdit(&m_wndEditVolume,false);
		enableDisableEdit(&m_wndEditVolumePerHa,false);
		enableDisableEdit(&m_wndEditLength,false);
		enableDisableEdit(&m_wndEditWidth,false);

		m_wndCalculateCorrFactor.EnableWindow(false);

		//#HMS-41 Tillf�lligt utnyttjande 190327 J�
		m_wndComboReducMark.SelectString(0,xml.str(IDS_STRING42050));//V�lj tomt
		m_wndComboReducMark.EnableWindow(false);

		//m_wndOverlappingLand.EnableWindow(false);
	}
	calculateArea();
}

void CAddEditEvalDialog::enableDisableEdit(CMyExtEdit * edit,bool enabled) {
		edit->EnableWindow(enabled);
		edit->SetReadOnly(!enabled);
}

void CAddEditEvalDialog::enableDisableEdit(CWnd * edit,bool enabled) {
		edit->EnableWindow(enabled);
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogLengthEdit()
{
	calculateArea();
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogWidthEdit()
{
	calculateArea();
}

void CAddEditEvalDialog::calculateArea() {
	double length = _tstof(m_wndEditLength.getText());
	double width = _tstof(m_wndEditWidth.getText());
	if(length>0&&width>0) {
		m_wndEditArea.setFloat((length*width)/(float)10000,3);
		m_wndEditArea.SetReadOnly(true);
		return;
	}
	m_wndEditArea.SetReadOnly(false);
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogAreaEdit() {
	calculateVolume();
	calculateLandValue();
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogVolumePerHaEdit()
{
	if(updateVolumePerHa) {
		calculateVolume();
	}
	updateVolumePerHa=true;
}

void CAddEditEvalDialog::calculateVolume() {
	if(!m_wndEditVolume.IsWindowEnabled())
		return;

	double volumePerHa = _tstof(m_wndEditVolumePerHa.getText());
	double area = _tstof(m_wndEditArea.getText());
	updateVolume=false;
	m_wndEditVolume.setFloat(volumePerHa*area,3);
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogVolumeEdit()
{
	if(updateVolume) {
		double area = _tstof(m_wndEditArea.getText());
		double volume = _tstof(m_wndEditVolume.getText());
		
		updateVolumePerHa=false;

		if(area>0&&volume>0)
			m_wndEditVolumePerHa.setFloat(volume/area,3);
		else
			m_wndEditVolumePerHa.setFloat(0,3);
	}
	updateVolume=true;

}

void CAddEditEvalDialog::OnBnClickedAddEvalDialogCancelButton()
{
	EndDialog(0);
	
}

void CAddEditEvalDialog::OnBnClickedAddEvalDialogCalculateButton() {
	calculateCorrFactor();
}

void CAddEditEvalDialog::calculateCorrFactor()
{
	CString sName,sDoneBy,sNotes;
	vecObjectTemplate_p30_table vecP30_table;

	CTransaction_elv_object recObject;

	CString sSI = m_wndEditH100.getText();
	int nAge = _tstoi(m_wndEditAge.getText());

	double fCorrFactor = _tstof(m_wndEditCorrFactor.getText());
	double fVolume = _tstof(m_wndEditVolume.getText());

	if(m_wndEditAge.getText()==L"") {
		showMessage(IDS_STRING8007);
		return;
	}

	if(m_wndEditH100.getText()==L"") {
		showMessage(IDS_STRING8008);
		return;
	}

	if(m_wndEditH100.getText().Left(1).FindOneOf(_T("TGBFCE"))<0) {
		showMessage(IDS_STRING8016);
		return;
	}

	if (m_pDB) 
	{
		m_pDB->getObject(objectId,recObject);
	}

	CMyInputDlg *pDlg = new CMyInputDlg();
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{ 
			fVolume = pDlg->getVolumeEntered();

			int normId = recObject.getObjUseNormID();

			if (normId == ID_1950_FORREST_NORM) {

				double fPinePerc = _tstof(m_wndEditPartPine.getText());
				double fSprucePerc = _tstof(m_wndEditPartSpruce.getText());
				double fBirchPerc = _tstof(m_wndEditPartBirch.getText());

				CString sName,sDoneBy,sNotes;
				vecObjectTemplate_p30_table vecP30_table;					// "1950:�rd Skogsnorm"

				// Load P30 table for Object;
				TemplateParser parser;
				if (recObject.getObjP30TypeOf() == TEMPLATE_P30)
				{
					if (parser.LoadFromBuffer(recObject.getObjP30XML()))
					{
						parser.getObjTmplP30(vecP30_table,sName,sDoneBy,sNotes);
					}	// if (parser.LoadFromBuffer(recObject.getObjP30XML()))
				}	// if (recObject.getObjP30TypeOf() == TEMPLATE_P30)

				calculateCorrFactor_1950(fVolume,
												 fPinePerc,
												 fSprucePerc,
												 fBirchPerc,
												 vecP30_table,
												 sSI,
												 nAge,
												 _tstoi(recObject.getObjGrowthArea()),
												 &fCorrFactor);
			}
			else if (normId == ID_2009_FORREST_NORM) {

				calculateCorrFactor_2009(fVolume,
										 sSI,
										 nAge,
										 _tstoi(recObject.getObjGrowthArea()),
										 &fCorrFactor);
			}
			else if (normId == ID_2018_FORREST_NORM) {

				calculateCorrFactor_2018(fVolume,
										 sSI,
										 nAge,
										 _tstoi(recObject.getObjGrowthArea()),
										 &fCorrFactor);
			}

		}
		delete pDlg;
	}

	m_wndEditCorrFactor.setFloat(fCorrFactor,2);
}

bool CAddEditEvalDialog::checkTreePercent() {
	double fPinePerc = _tstof(m_wndEditPartPine.getText());
	double fSprucePerc = _tstof(m_wndEditPartSpruce.getText());
	double fBirchPerc = _tstof(m_wndEditPartBirch.getText());

	if((fPinePerc+fSprucePerc+fBirchPerc)==100)
		return true;
	return false;
}

void CAddEditEvalDialog::showMessage(int id) {
	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			::MessageBox(this->GetSafeHwnd(),xml.str(id),xml.str(IDS_STRING229),MB_ICONASTERISK | MB_OK);
		}
	}
}

void CAddEditEvalDialog::showMessage(CString msg) {
	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			::MessageBox(this->GetSafeHwnd(),msg,xml.str(IDS_STRING229),MB_ICONASTERISK | MB_OK);
		}
	}
}

void CAddEditEvalDialog::calculateLandValue() {
	double landValuePerHa = _tstof(m_wndEditLandValuePerHa.getText());
	double area = _tstof(m_wndEditArea.getText());

	updateLandValue=false;
	if(m_wndEditLandValue.IsWindowEnabled()) {
		m_wndEditLandValue.setFloat(landValuePerHa*area,3);
	}
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogLandvalueEdit()
{
	if(updateLandValue)
	{
		double landValue = _tstof(m_wndEditLandValue.getText());
		double area = _tstof(m_wndEditArea.getText());

		updateLandValuePerHa=false;
		
		if(area>0)
			m_wndEditLandValuePerHa.setFloat(landValue/area,3);
		else
			m_wndEditLandValuePerHa.setFloat(0,3);
	}

	updateLandValue=true;
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogLandvaluePerHaEdit()
{
	if(updateLandValuePerHa) {
		calculateLandValue();
	}
	updateLandValuePerHa=true;
}

void CAddEditEvalDialog::OnEnChangeAddEvalDialogCorrFactorEdit()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	double cFactor = _tstof(m_wndEditCorrFactor.getText());

	if(!(cFactor>0))
		return;

	if(!(cFactor>=MIN_VALUE_FOR_CORRFACTOR&&cFactor<=MAX_VALUE_FOR_CORRFACTOR)) {
		CString sMsg=L"";
		CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

		if (fileExists(sLangFN))
		{
			RLFReader xml;
			if (xml.Load(sLangFN))
			{
				sMsg.Format(xml.str(IDS_STRING6320),MIN_VALUE_FOR_CORRFACTOR,MAX_VALUE_FOR_CORRFACTOR);
			}
		}

		showMessage(sMsg);
	}
}
